using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Sylvia.Common
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
  public class Util
  {
    public Util()
    {
      //
      // TODO: Add constructor logic here
      //
    }




    static public DateTime GoToStartOfDay(DateTime dt)
    {
      DateTime cdt = new DateTime(dt.Year, dt.Month,dt.Day, 0, 0, 0, 0);
      return cdt;
    }    
    static public DateTime GoToEndOfDay(DateTime dt)
    {
      DateTime cdt = new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59, 0);
      return cdt;
    }
    static public string DateToSQLString(DateTime dt)
    {
      // this patterns return date like: 01-JAN-2005 02:30:45 PM
      string sSQLDatePatter = "dd-MMM-yyyy hh:mm:ss tt";

      return dt.ToString(sSQLDatePatter);
    }

    static public void LogFatal(string sLog)
    {
        Console.WriteLine( sLog );
    }

    static public void LogError(string sLog)
    {
      Console.WriteLine( sLog );
    }
    static public void LogWarning(string sLog)
    {
      Console.WriteLine( sLog );
    }
    static public void LogInfo(string sLog)
    {
      Console.WriteLine( sLog );
    }

    static public object DeserializeFromBase64String(string sBase64)
    {
      byte [] bytes = Convert.FromBase64String( sBase64 );
      IFormatter formatter = new BinaryFormatter( );
      MemoryStream stream = new MemoryStream( bytes );
      object obj = formatter.Deserialize( stream );
      return obj;
    }

    static public string SerializeToBase64String(object obj)
    {
      IFormatter formatter = new BinaryFormatter( );
      MemoryStream stream = new MemoryStream( );
      formatter.Serialize( stream, obj );
      byte [] bytes  = stream.ToArray( );
      return Convert.ToBase64String( bytes );
    }

	}  // end of Util class
}
