﻿using AspNetCore.Reporting;
using Sylvia.Common.Reports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Sylvia.Common.RDLCReport
{
    public class ReportByteData
    {
        
        public byte[] ReportData(rptLeaveDS leaveDS)
        {
            //var xxxx;
            //string[] resourceNames = this.GetType().Assembly.GetManifestResourceNames();


            //var assembly = Assembly.GetExecutingAssembly();

            //var stream = assembly.GetManifestResourceStream("Test.rdlc");
            // var path = _appEnvironment.ContentRootPath.ToString();
            // var path2 = _appEnvironment.;
            //      string fileDirPath = Assembly.GetExecutingAssembly().Location.Replace("ReportDesign.dll", string.Empty);
            //      string rdlcFilePath = string.Format("{0}\\{1}.rdlc", fileDirPath, "Test");
            Dictionary<string, string> parameters = new Dictionary<string, string>();

            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Encoding.GetEncoding("windows-1252");
            string rdlcPath = (@"D:\SYLVIA-RND-ASP-DOT-DOT-CORE\asp-dot-net-core-sylvia\Sylvia\Sylvia.Common\RDLCReport\rptLeaveSummery.rdlc");

            //Step 1 : Create a Local Report.
            LocalReport localReport = new LocalReport(rdlcPath);
            parameters.Add("p_CompanyName", "JAPAN TOBACCO INTERNATIONAL");
            parameters.Add("p_Period", "01‎-‎Jan‎-‎2020 ‎To ‎31‎-‎Dec‎-‎2020");
            parameters.Add("p_LeaveType", "‎All");
            parameters.Add("p_LeaveStatus", "Approved");
           // parameters.Add("p_Image", "Approved");
            string logoPath = @"D:\SYLVIA-RND-ASP-DOT-DOT-CORE\asp-dot-net-core-sylvia\Sylvia\Presentation.Web.AspDotNetCore.Client\wwwroot\img\JTI_Logo.jpg";

            byte[] imgBinary = File.ReadAllBytes(logoPath);
            string image = Convert.ToBase64String(imgBinary);
            parameters.Add("p_Image", image);


            ////LocalReport localReport = new LocalReport(rdlcPath);

            localReport.AddDataSource("leaveData", leaveDS.Leave);
           // localReport.AddDataSource("img", leaveDS.Image);

            var result = localReport.Execute(RenderType.Pdf, 1, parameters);
            return result.MainStream;
            //  return File(stream, "application/pdf", "Report.pdf");

        }
    }
}
