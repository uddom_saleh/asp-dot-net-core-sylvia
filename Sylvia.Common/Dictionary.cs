using System;

namespace Sylvia.Common
{
    public enum SalaryHead
    {
        PAYSLIP_CONST_BASIC_SALARY = -101,
        PAYSLIP_CONST_OVER_TIME_HOURS = -102,
        PAYSLIP_CONST_OVER_TIME_AMOUNT = -103,

        PAYSLIP_CONST_ORD_HOURS = -104,
        PAYSLIP_CONST_ORD_AMOUNT = -105,

        PAYSLIP_CONST_ADDITIONAL_HOURS = -106,
        PAYSLIP_CONST_ADDITIONAL_AMOUNT = -107,

        PAYSLIP_DSHIFT_HOURS = -108,
        PAYSLIP_DSHIFT_AMOUNT = -109,
        PAYSLIP_TSHIFT_HOURS = -110,
        PAYSLIP_TSHIFT_AMOUNT = -111,

        PAYSLIP_CONST_MONTHLY_ATTENDANCE = -112,
        PAYSLIP_CONST_REGULAR_ALLOWANCE = -113,


        //PAYSLIP_CONST_ONEOFF_ALLOWANCE = -114,
        //PAYSLIP_CONST_ACTING_ALLOWANCE = -115,

        PAYSLIP_CONST_REGULAR_DEDUCTION = -116,
        PAYSLIP_CONST_ADVANCE_DEDUCTION = -117,
        PAYSLIP_CONST_ARREAR = -118,

        PAYSLIP_CONST_LOAN_MONTHLY_ADJASTMENT = -120,
        //  PAYSLIP_CONST_GEN_LOAN_MONTHLY_INTEREST = -121,
        //  PAYSLIP_CONST_PF_LOAN_MONTHLY_INSTALLMENT = -122,
        //  PAYSLIP_CONST_PF_LOAN_MONTHLY_INTEREST = -123,

        PAYSLIP_CONST_GEN_LOAN_REMAIN_INSTALLMENT = -124,
        PAYSLIP_CONST_GEN_LOAN_REMAIN_INTEREST = -125,
        PAYSLIP_CONST_PF_LOAN_REMAIN_INSTALLMENT = -126,
        PAYSLIP_CONST_PF_LOAN_REMAIN_INTEREST = -127,

        PAYSLIP_CONST_PF_CONTRIBUTION = -128,
        PAYSLIP_CONST_PF_COMPANY_CONTRIBUTION = -121,
        PAYSLIP_CONST_INC_TAX_DEDUCTION = -129,
        PAYSLIP_CONST_INC_TAX_TOT_TAXABLE = -130,
        PAYSLIP_CONST_INC_TAX_YEARLY_AMOUNT = -131,

        PAYSLIP_CONST_NET_PAYABLE = -132,
        PAYSLIP_CONST_TOT_UNAUTHLEAVE_DAYS = -133,
        PAYSLIP_CONST_TOT_ARREAR_DAYS = -134,

        PAYSLIP_CONST_TOT_ATTEND_DAYS = -135,
        PAYSLIP_CONST_CONV_DAYS = -136,
        PAYSLIP_CONST_PERSONAL_PAYMENT = -137,
        PAYSLIP_CONST_TOTAL_REMITTED = -138,
        PAYSLIP_CONST_SCI_ADJUSTMENT = -139,

        PAYSLIP_CONST_COIN_ADD = -140,
        PAYSLIP_CONST_COIN_DED = -141,

        PAYSLIP_CONST_LATE_HOUR_HOURS = -142,
        PAYSLIP_CONST_LATE_HOUR_AMOUNT = -143,
        PAYSLIP_CONST_SHORT_LEAVE_HOURS = -144,
        PAYSLIP_CONST_SHORT_LEAVE_AMOUNT = -145,

        PAYSLIP_CONST_MEAL_DAYS = -146,
        PAYSLIP_CONST_TEA_DAYS = -147,

        PAYSLIP_CONST_BONUS = -150,

    }
    public enum AdType
    {
        ALLOWANCE = 1,
        DEDUCTION = 2,
        BONUS = 3,
    }
    public enum PaySlipGroup
    {
        RegularTaxableGross = 1,
        IregularTaxableGross = 2,
        NonTaxableGross = 3,
        LFAamount = 4,

        RegularTaxbleDeductions = 11,
        IregularTaxbleDeductions = 12,
        NonTaxableDeductions = 13,

        UnauthLeave = 33,
        Miscellaneous = 4,
        OtherItem = 5,
        Arrear = 8,
    }
    public enum EmployeeCategory
    {
        XCom = 0,
        Management = 1,
        NonManagement = 2,
        Regular = 3,
        Project = 4,
        Service = 5,
        Contractual = 6,
    }
    public enum DiscontinueType
    {
        Continue = 0,
        Permanently = 1,
        Temporarily = 2,
        LeaveWithOutPay = 3,
        PermanentlyAndViewInPaySheet = 4,
        Suspend = 5,
        Suspension_Withdraw = 6,
        Recontract = 7,
        Resignation = 200,
    }
    public enum EmployeeType
    {
        Management = 0,
        NonManagement = 1,
        Contractual = 2,
        XCom = 3,
        XPat = 4,
        Regular = 5,
        Project = 6,
        Service = 7,
    }
    public enum EventCode
    {
        JOIN = 1,
        UPDATE = 2,
        #region Update By Jarif(12-Sep-11)
        //PROMOTION = 3,
        INCREMENT = 3,
        #endregion
        TRANSFER = 4,
        CHANGE_STATUS = 5,
        JOIN_PAYROLL = 6,
        CONFIRM_EMPLOYEE = 7,
        PF_MEMBERSHIP = 8,
        DISCONTINUE = 9,
        LEAVE = 10,//Leave without pay
        #region Update By Jarif(12-Sep-11)
        PROMOTION = 11,
        #endregion
        CONTINUE = 12,
        TEMPORARILY_DISCONTINUE = 13,
        LEAVE_WITHOUT_PAY = 14,
        PERMANENTLY_AND_VIEW_IN_PAYSHEET = 15,
        SUSPEND = 16,
        DEMOTION = 17,
        DECREMENT = 18,
        SUSPENSION_WITHDRAW = 19,
        LIABILITIES = 20, 
        TRAINING_REQUIRED = 21,
        OBSERVATION_WITHDRAW = 22,
        OBSERVATION = 23,
        RE_CONTRACT = 24,

    }
    public enum SalaryHeadConfig
    {
        SalaryReviewSalaryIncrement = 0,
        SalaryReviewBonusIncrement = 1,
        FinalSattlement = 2,
        LFA = 3,
    }
    public enum LeaveType
    {
        LeaveWithoutPay = 1,
        LeaveWithPay = 2,
    }
    public enum FIXEDACCOUNTHEAD
    {
        SALARY_ADMIN = 1,
        SALARY_FIN = 2,
        SALARY_LEGAL = 3,
        SALARY_HR = 4,
        SALARY_COM = 5,
        SALARY_PRO = 6,
        SALARY_MDSEC = 7,
        SALARY_TECHNICAL = 8,
        SALARY_CTI = 9,
        SALARY_OPG = 10,
        PF_ADMIN = 11,
        PF_FIN = 12,
        PF_LEGAL = 13,
        PF_HR = 14,
        PF_COM = 15,
        PF_PRO = 16,
        PF_MDSEC = 17,
        PF_OPG = 18,
        PF_TECHNICAL = 19,

    }

    public class HEADCODE
    {
        public static readonly string HOUSE_RENT = "A000001";
        public static readonly string MEDICAL = "A000006";
        public static readonly string CONVEYENCE = "A000002";
        public static readonly string PF_COMPANY = "PFCOMP";
        public static readonly string PF_SELF = "PFSELF";
        //mislbd.Jarif Update Start(20-July-2009)
        public static readonly string LFA = "A000005";
        //public static readonly string LFA = "A000018";
        //mislbd.Jarif Update End(20-July-2009)
        public static readonly string PERSONAL_ALLOWNCE = "A000003";
        public static readonly string FESTIBAL_ALLOWNCE = "A000011";
        public static readonly string BASIC_SALARY = "BASIC";
        public static readonly string AREAR = "AREAR";

        // New code Added by Zakaria  

        public static readonly string LOCATION_ALLOWNCE = "A000004";
        public static readonly string COMMISSION_ALLOWNCE = "A000012";
        public static readonly string VARIABLE_SALARY = "A000013";
        public static readonly string EID_SPECIAL_ALLOWNCE = "A000014";
        public static readonly string UTILITY_CHARGE = "DED01";
        public static readonly string MEAL_CHARGE = "DED03";
        public static readonly string TAX = "TAX";
        public static readonly string BONUS = "A000009";

        //mislbd.Jarif Update Start(20-July-2009)
        public static readonly string OVER_TIME = "A000016";
        public static readonly string SHIFT_ALLOWANCE = "A000017";
        //mislbd.Jarif Update End(20-July-2009)

        //mislbd.Jarif Update Start(04-August-2009)
        public static readonly string CONVEYENCE_FACILITY = "CONFAC";
        //mislbd.Jarif Update End(04-August-2009)

        //mislbd.Jarif Update Start(08-March-2010)
        public static readonly string LEA = "DED05";
        //mislbd.Jarif Update End(08-March-2010)
        #region Created By Jarif(09 Oct 11)...
        public static readonly string LFA_REQUEST = "A000018";
        #endregion
        public static readonly string LFA_ARREAR = "A000025";
        //Jarif, 02Sep12...
        public static readonly string HouseMaint = "A000020";
        public static readonly string Utilities = "A000019";
        public static readonly string BF_SS = "DED14";
        public static readonly string SHBS_Gen = "DED06";
        public static readonly string SHBS_Com = "DED07";
        public static readonly string AIBOC_Savings = "DED08";
        public static readonly string AIBOC_InvtReco = "DED09";
        public static readonly string CIS_DDIS = "DED10";
        public static readonly string QardAgtSS_BF = "DED11";
        public static readonly string QardAgtCar = "DED12";
        public static readonly string QardAgtPF = "DED13";
        public static readonly string IT_Allowance = "A000022";
        public static readonly string FestivalBonusF = "B000002";
        public static readonly string FestivalBonusA = "B000003";
        //
        public static readonly string LOAN = "LOAN";

    }
    public class DEPARTMENTCODE
    {
        public static readonly string ADMIN = "DEPT0001";
        public static readonly string FIN = "DEPT0006";
        public static readonly string LEGAL = "DEPT0011";
        public static readonly string HR = "DEPT0008";
        public static readonly string COM = "Commercial";
        public static readonly string PRO = "Production";
        public static readonly string MDSEC = "";
        public static readonly string TECHNICAL = "DEPT0023";
        public static readonly string CTI = "";
        public static readonly string OPG = "";

    }

    // For LMS
    public enum LeaveActivities
    {
        Approve = 101,
        Recommend = 102,
        NotRecommend = 103,
        Reject = 104,
        Cancel = 105,
        PendingApproval = 106,
        Taken = 107,
        Weekend = 108,
        Holiday = 109,
        Transfer = 110,
    }

    public enum Weekend
    {
        FullDay = 103,
        HalfDay = 102,
        Weekend = 101,
    }

    public enum DaysName
    {
        Saturday = 101,
        Sunday = 102,
        Monday = 103,
        Tuesday = 104,
        Wednesday = 105,
        Thursday = 106,
        Friday = 107,
    }

    public enum DayLength
    {
        FullDay = 8,
        HalfDay = 4,
    }

    public enum DayLengthIDs
    {
        FullDayLeave = 101,
        HalfDayLeave = 102,
        NoLeave = 103,
    }

    public class LeaveStatus
    {
        public static readonly string STATUS_DIFFER = "Status Differ";
        public static readonly string STATUS_APPROVED_DIFFER = "Partly Approved/ Status Differ";
    }

    public enum LeaveEncashmentStatus
    {
        Verified = 0,
        Approved = 1,
        Rejeceted = 2,
        Paid = 3
    }
    //Ene Lms

    #region Recruitment...
    public enum JobApplicationActivities
    {
        Approve = 101,
        SeekApproval = 102,
        OfferJob = 103,
        OfferDecline = 104,
        Reject = 105,
        Interview = 106,
        ApplicationSubmit = 107,
        ShortListed = 111,
        WrittenTest = 108,
        Reject_Preli = 109,
        
    }
    #endregion Recruitment

    #region LFA...
    public enum LFARequestActivities
    {
        Approve = 101,
        Reject = 104,
        Cancel = 105,
        PendingApproval = 106,
    }
    #endregion LFA

    #region Employee Advance...
    public enum MailDepartment
    {
        User = -101,
        Supervisor = -102,
    }
    public enum Task
    {
        Advance = 201,
    }
    #endregion

    public enum RequisitionActivities
    {
        Approved = 101,
        Recommend = 102,
        NotRecommend = 103,
        Reject = 104,
        Cancel = 105,
        PendingApproval = 106,
    }

    public class RequisitionStatus
    {
        public static readonly string STATUS_DIFFER = "Status Differ";
        public static readonly string STATUS_APPROVED_DIFFER = "Partly Approved/ Status Differ";
    }

    public enum TrainingType
    {
        Local = 1,
        Foreign = 2
    }

    public enum AssetActivities
    {
        Approved = 101,
        Recommend = 102,
        NotRecommend = 103,
        Reject = 104,
        Cancel = 105,
        PendingApproval = 106,
        Settlement = 107,
        Assigned = 108,       // WALI :: 30-Mar-2015
    }

    public enum HouseLocation
    {
        City_Corporation = 1,
        District = 2,
        Upozila = 3,
        City_Corporation_Other = 4,
    }

    public enum Position
    {
        ALLOWANCE = 2,
        DEDUCTION = 105,
        BONUS = 3,
        LOAN_ADJUSTMENT = 103,
        BENEVOLENT_FUND = 106,
        LEAVE_ENCASHMENT = 107,
        LEAVE_WITHOUT_PAY = 108,
        OVERTIME = 109,
        ATTENDANCE_PENALTY = 110,
    }


    // For Training
    public enum ModuleID
    {
        TMS = 101,
    }

    public enum TrainingActivities
    {
        SendTo = 1001,
        SendBack = 1002,
        Received = 1003,
        Reject = 1004,
        ForwardBypassed = 1005,
        BackwardBypassed = 1006,
        MakeToFinal = 1007,
    }

    public enum TrainingConfJustification
    {
        Present = 1,
        On_Leave = 2,
        Absent = 3,
    }

    public enum TrainingAttendanceStatus
    {
        Present = 1,
        Late = 2,
        Absent = 3,
    }

    public enum Bypass
    {
        Forward = 1,
        Backward = 0,
    }

    public enum Miscellaneous
    {
        Comments = 101,
        Reason = 102,
        Mail_Subject = 103,
        Mail_Body = 104,
    }
    // End - Training

    public enum AppraisalType
    {
        Employee = 1,
        Department = 2,
        Branch = 3,
        Zone = 4,
        Institution = 5,
    }

    public enum GetEmpByAnyCriteriaFrmNo
    {
        QualificationAssessmentReport = 1,
        frmEmployeeAssessmentForSuccession = 2,
    }

    //WPPWF_PF  Start.........
    public enum SettlementMode
    {
        Early = 1,
        Regular = 2,
        Late = 3,
        Freeze = -1,
    }
    public enum Transaction_Type
    {
        Principal = 0,
        Profit = 1,
    }
    //WPPWF_PF  End...........

    public enum TransferActivity
    {
        Recommend = 101,
        Not_Recommend = 102,
        Approve = 103,
        Reject = 104,
        Receive = 105,
        Reverse = 106,
        Make_To_Final = 107,
        Release = 108,
    }

    public enum AttandanceActivity
    {
        Recommend = 102,
        Not_Recommend = 103,
        Approve = 101,
        Reject = 104,
        Cancel = 105,
        PendingApproval = 106,
    }

    public enum FinalSettlementActivity
    {
        Recommend = 1,
        Not_Recommend = 2,
        Approve = 3,
        Reject = 4,
        Waiting = 5,
        Release = 100,
        Send_Back = -1,
        //Discontinue = 9,
        Inform_LiabilityCollector = 103,
        AcknowledgeEmployee = 104,
        Liability_Collection = 105,
        PendingApproval = 106,
        ExitInterViewCall = 107,
        ExitInterViewSubmission = 108,
    }

    public enum AdministrativeArea
    {
        Own_Site_Branch = 0,
        All_Site_Branch = 1,
        UnAuthorized = 2,
    }

    // WALI :: 24-Jun-2015
    public enum JobApplicantUserActivity
    {
        Save_Applicant = 1,
        Update_Applicant = 2,
        Apply_For_Job = 3,
        Applicant_Create_And_Job_Apply = 4,
        Applicant_Update_And_Job_Apply = 5,
    }
    // WALI :: End

    public enum WP_PF_HEAD
    {
        PFCOMP = 101,
        PFSELF = 102,
        APPF_COMP = 103,
        APPF_SELF = 104,
        LF = 105,
        URS = 106,
        CaB = 107,
        PLSC = 108,
        CHRG = 109,
        Investment = 201,
        Accrued_Profit_on_Investment = 202,
        Bank_Profit = 203,
    }

    public enum GratuityHead
    {
        Gratuity_Contribution = 101,
        Lapses_Forfeiture = 105,
        Payable_to_Donor = 108,
        Undistributed_Revenue_Surplus = 106,
        Accrued_Profit_on_Investment = 102,
        Cash_at_Bank = 107,
        Investment = 201
    }


    public enum BenevolentFundHead
    {
        BenevolentFund_Contribution = 101,
        Lapses_Forfeiture = 105,
        Payable_to_Donor = 108,
        Undistributed_Revenue_Surplus = 106,
        Accrued_Profit_on_Investment = 102,
        Cash_at_Bank = 107,
        Investment = 201
    }

    #region Basic Accounting
    public enum SegmentType
    {
        Controlled, Dynamic
    }
    public enum Acc_ApprovalPath
    {
        Receive = 155,
        Payment = 156,
        Journal = 157,
        Sales_Purchase = 164,
        Contra = 165,
        Debit_Credit_Note = 166
    }
    #endregion

    public enum CertificateVerificationStatus
    {
        Not_Verified = 1,
        Under_Process = 2,
        Genuine = 3,
        Fake = 4,
    }

    public enum NomineeType
    {
        All             = 100,
        Provident_Fund  = 101,
        Gratuity        = 102,
        Employee_Welfare_Fund =103,
        Group_Insurance = 104,
        EHBLSS          = 105,
        WPPWF           = 106,
    }

    public enum EmpStatus
    {
        Regular = 0,
        Discontinue_Permanently = 1,
        Discontinue_Temporarily = 2,
        Suspended = 5,
    }
	
	public enum Organogram_Type
    {
        Branch = 101,
        Division = 102,
        Department = 103,
        Designation = 104,
        Function = 105,
        Func_Desig = 106,
        Report_Func_Desig = 107,
    }

    public enum Liabilities_Nature
    {
        Managing_Director = 999,
        Head_of_HR = 1000,
        Head_of_Organization = 1001,
        Branch_Head = 1002,
        Division_Head = 1003,
        Department_Head = 1004,
        Functional_Head = 1005,
        General = 1006,
    }

    public enum Supervisor_SearchParam
    {
        General = -1,
        Leave = 0,
        Loan = 1,
    }

    public enum Approval_Activity
    {
        PendingApproval = 0,
        Approved = 1,
        Rejected = 2,
    }

    public enum Withhold_ActionType
    {
        SALARY = 0,
        INCREMENT = 1,
        PROMOTION = 2,
        TRANSFER = 3,
    }

    public enum EmpProfile_ApprovalActivity
    {
        PendingApproval = 100,
        Approve = 101,
        Recommend = 102,
        Not_Recommend = 103,
        Reject = 104,
        Cancel = 105,
    }

    public enum Requisition_ApprovalActivity
    {
        PendingApproval = 100,
        Approve = 101,
        Recommend = 102,
        Not_Recommend = 103,
        Reject = 104,
        Cancel = 105,
    }

    public enum Priority
    {
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
    }
    public enum WorkFlowBasedActivity
    {
        PendingApproval = 100,
        Approve = 101,
        Recommend = 102,
        Not_Recommend = 103,
        Reject = 104,
        Cancel = 105,
    }

    //// For Separation
    //public enum SeparationActivities
    //{
    //    Approve = 101,
    //    Recommend = 102,
    //    NotRecommend = 103,
    //    Reject = 104,
    //    Cancel = 105,
    //    PendingApproval = 106,
    //    ExitInterViewCall = 107,
    //    ExitInterViewSubmission = 108,
    //}
}
