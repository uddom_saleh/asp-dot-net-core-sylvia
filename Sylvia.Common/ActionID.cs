using System;

namespace Sylvia.Common
{


    /// <summary>
    /// Summary description for Action.
    /// </summary>
    public enum ActionID
    {
        //NA = Not Applicable for Role. NA_


        #region ALL VIEW ACTIONS FOR VIEWING PERMISSION OF USER INTERFACE  ::  ORGANIZED BY RONY :: 25 JUN 2015
        ACTION_VIEW_DepartmentWiseMonthlySalary_UI,
        ACTION_VIEW_AccountHead_UI,
        ACTION_VIEW_AccountHeads_UI,
        ACTION_VIEW_Activity_UI,
        ACTION_VIEW_Activities_UI,
        ACTION_VIEW_Allowance_UI,
        ACTION_VIEW_Allowances_UI,
        ACTION_VIEW_AllowanceEmpolyees_UI,
        ACTION_VIEW_AllowanceGradeParam_UI,
        ACTION_VIEW_AllowanceGradeParams_UI,
        ACTION_VIEW_AttendanceDays_UI,
        ACTION_VIEW_AttendanceMonth_UI,
        ACTION_VIEW_AttendanceMonths_UI,
        ACTION_VIEW_Bonus_UI,
        ACTION_VIEW_Bonuses_UI,
        ACTION_VIEW_BankBranch_UI,
        ACTION_VIEW_BankBranches_UI,
        ACTION_VIEW_BonusEmployees_UI,
        ACTION_VIEW_BonusGradeParam_UI,
        ACTION_VIEW_BonusGradeParams_UI,
        ACTION_VIEW_Children_UI,
        ACTION_VIEW_Company_UI,
        ACTION_VIEW_Companies_UI,
        ACTION_VIEW_CompanyPerfBonus_UI,
        ACTION_VIEW_Configuration_UI,
        ACTION_VIEW_Configurations_UI,
        ACTION_VIEW_CostCenter_UI,
        ACTION_VIEW_CostCenters_UI,
        ACTION_VIEW_Currency_UI,
        ACTION_VIEW_Currencies_UI,
        ACTION_VIEW_CurrencyConversion_UI,
        ACTION_VIEW_CurrencyConversions_UI,
        ACTION_VIEW_DataImport_UI,
        ACTION_VIEW_Deduction_UI,
        ACTION_VIEW_Deductions_UI,
        ACTION_VIEW_DeductionEmployess_UI,
        ACTION_VIEW_DeductionGradeParam_UI,
        ACTION_VIEW_DeductionGradeParams_UI,
        ACTION_VIEW_Department_UI,
        ACTION_VIEW_Departments_UI,
        ACTION_VIEW_Designation_UI,
        ACTION_VIEW_Designations_UI,
        ACTION_VIEW_DiscontinueEmployee_UI,
        ACTION_VIEW_Education_UI,
        ACTION_VIEW_EmailPassword_UI,
        ACTION_VIEW_Employee_UI,
        ACTION_VIEW_Employees_UI,
        ACTION_VIEW_EmployeeCurrency_UI,
        ACTION_VIEW_EmployeeCurrencies_UI,
        ACTION_VIEW_EmployeeFCSalary_UI,
        ACTION_VIEW_EmployeeFCSalaries_UI,
        ACTION_VIEW_EmployeeHist_UI,
        ACTION_VIEW_EmployeeHistory_UI,
        ACTION_VIEW_EmployeeJoinPFMembership_UI,
        ACTION_VIEW_EmployeePFMembers_UI,
        ACTION_VIEW_EmployeePerformRange_UI,
        ACTION_VIEW_Employment_UI,
        ACTION_VIEW_FinalSettlement_UI,
        ACTION_VIEW_FinalSettlementEdit_UI,
        ACTION_VIEW_Function_UI,
        ACTION_VIEW_Functions_UI,
        ACTION_VIEW_Grade_UI,
        ACTION_VIEW_Grades_UI,
        ACTION_VIEW_GratuityParameter_UI,
        ACTION_VIEW_INcomeTaxParam_UI,
        ACTION_VIEW_IncrementProvision_UI,
        ACTION_VIEW_JobConfirmation_UI,
        ACTION_VIEW_KPI_UI,
        ACTION_VIEW_KPIMappingWithDepartment_UI,
        ACTION_VIEW_KPIScoring_UI,
        ACTION_VIEW_KPITarget_UI,
        ACTION_VIEW_LayOffEmployee_UI,
        ACTION_VIEW_LayOffParameter_UI,
        ACTION_VIEW_LayOffParameters_UI,
        ACTION_VIEW_LeaveWithoutPay_UI,
        ACTION_VIEW_Loan_UI,
        ACTION_VIEW_LoanIssue_UI,
        ACTION_VIEW_LoanIssues_UI,
        ACTION_VIEW_Loans_UI,
        ACTION_VIEW_LoanType_UI,
        ACTION_VIEW_LoanTypes_UI,
        ACTION_VIEW_Location_UI,
        ACTION_VIEW_Locations_UI,
        ACTION_VIEW_LoginUserList_UI,
        ACTION_VIEW_MasterCompany_UI,
        ACTION_VIEW_MasterCompanies_UI,
        ACTION_VIEW_ModifyGratuity_UI,
        ACTION_VIEW_ModifySalary_UI,
        ACTION_VIEW_MonthEndProcess_UI,
        ACTION_VIEW_PARating_UI,
        ACTION_VIEW_PARatingEmployee_UI,
        ACTION_VIEW_PARatings_UI,
        ACTION_VIEW_PasswordChange_UI,
        ACTION_VIEW_PayProcess_UI,
        ACTION_VIEW_PayProcesses_UI,
        ACTION_VIEW_PayrollSite_UI,
        ACTION_VIEW_PayrollSites_UI,
        ACTION_VIEW_PFParameter_UI,
        ACTION_VIEW_PFParameters_UI,
        ACTION_VIEW_Promotion_UI,
        ACTION_VIEW_ResponsibilityCenter_UI,
        ACTION_VIEW_ResponsibilityCenters_UI,
        ACTION_VIEW_Role_UI,
        ACTION_VIEW_Roles_UI,
        ACTION_VIEW_SalaryDetail_UI,
        ACTION_VIEW_SalaryHeadConfig_UI,
        ACTION_VIEW_SalaryReview_UI,
        ACTION_VIEW_SetPARatingtoEmp_UI,
        ACTION_VIEW_Site_UI,
        ACTION_VIEW_Sites_UI,
        ACTION_VIEW_Spouse_UI,
        ACTION_VIEW_SystemInformation_UI,
        ACTION_VIEW_TaxCertificate_UI,
        ACTION_VIEW_Training_UI,
        ACTION_VIEW_Transfer_UI,
        ACTION_VIEW_User_UI,
        ACTION_VIEW_Users_UI,
        ACTION_VIEW_AllocateLeaveQuota_UI,
        ACTION_VIEW_ApplyLeave_UI,
        ACTION_VIEW_AppoveLeave_UI,
        ACTION_VIEW_ApproveLeaveDatails_UI,
        ACTION_VIEW_AssignGrade_UI,
        ACTION_VIEW_AssignLeave_UI,
        ACTION_VIEW_ConfigLeaveType_UI,
        ACTION_VIEW_Holiday_UI,
        ACTION_VIEW_LeaveActivities_UI,
        ACTION_VIEW_LeaveCategories_UI,
        ACTION_VIEW_LeaveCategory_UI,
        ACTION_VIEW_LeaveCounter_UI,
        ACTION_VIEW_LeaveType_UI,
        ACTION_VIEW_LeaveTypes_UI,
        ACTION_VIEW_ModifyLeaveQuota_UI,
        ACTION_VIEW_Weekend_UI,
        ACTION_VIEW_AccumulatedGratuityAnalysis_PUI,
        ACTION_VIEW_ArrearInformation_PUI,
        ACTION_VIEW_BankAdvice_PUI,
        ACTION_VIEW_BankDeposite_PUI,
        ACTION_VIEW_CertificateOfTax_PUI,
        ACTION_VIEW_ChtPerformanceRecord_PUI,
        ACTION_VIEW_CumulativePF_PUI,
        ACTION_VIEW_DesignationReport_PUI,
        ACTION_VIEW_EmpInfo_PUI,
        ACTION_VIEW_Fluctuation_PUI,
        ACTION_VIEW_JobHistory_PUI,
        ACTION_VIEW_LEAStatement_PUI,
        ACTION_VIEW_LeaveReport_PUI,
        ACTION_VIEW_LFAReport_PUI,
        ACTION_VIEW_MonthlyReconciliation_PUI,
        ACTION_VIEW_MonthlyTaxReport_PUI,
        ACTION_VIEW_PaySheet_PUI,
        ACTION_VIEW_PaySheetByParam_PUI,
        ACTION_VIEW_PaySlip_PUI,
        ACTION_VIEW_PerformanceRecord_PUI,
        ACTION_VIEW_PerquisiteReport_PUI,
        ACTION_VIEW_PfAnalysis_PUI,
        ACTION_VIEW_Promotion_PUI,
        ACTION_VIEW_ProvisionedGratuityAnalysis_PUI,
        ACTION_VIEW_SalaryAnalysis_PUI,
        ACTION_VIEW_SalaryStatementSummaryReport_PUI,
        ACTION_VIEW_Section108_PUI,
        ACTION_VIEW_TaxCard_PUI,
        ACTION_VIEW_TaxCardWithCertificate_PUI,
        ACTION_VIEW_TaxStatement_PUI,
        ACTION_VIEW_Transfer_PUI,
        ACTION_VIEW_BANK_UI,
        ACTION_VIEW_BANKS_UI,
        ACTION_VIEW_LeaveHistory_UI,
        ACTION_VIEW_ModifyIncomeTax_UI,
        ACTION_VIEW_ApproveLFA_UI,
        ACTION_VIEW_LFARequest_UI,
        ACTION_VIEW_BasicSalaryReport_PUI,
        ACTION_VIEW_PaySheetDynamic_PUI,
        ACTION_VIEW_ReconcileReportHeadwise_PUI,
        ACTION_VIEW_Delegate_UI,
        ACTION_VIEW_MailUserConfiguration_UI,
        ACTION_VIEW_RuleConfiguration_UI,
        ACTION_VIEW_MailSendDateConfig_UI,
        ACTION_VIEW_MailBody_UI,
        ACTION_VIEW_EmployeeAdvanceReport_PUI,
        ACTION_VIEW_ASSIGN_SUPERVISOR_UI,

        // Shakir StArt...
        ACTION_VIEW_REQUISITION_APPROVE_UI,
        ACTION_VIEW_REQUISITION_LIMIT_CONFIG_UI,
        ACTION_VIEW_REQUISITION_APPLY_UI,
        ACTION_VIEW_REQUISITION_CATEGORIES_UI,
        ACTION_VIEW_ASSIGN_GRADE_CATEGORIES_UI,
        ACTION_VIEW_REQUISITIONCATEGORY_UI,
        ACTION_VIEW_REQ_CATEGORY_CONFIG_UI,
        ACTION_VIEW_RIF_CONFIG_UI,
        ACTION_VIEW_INPUT_FIELD_UI,
        ACTION_VIEW_INPUT_FIELDS_UI,
        ACTION_VIEW_REQUISITION_TYPE_UI,
        ACTION_VIEW_REQUISITION_TYPES_UI,
        //Shakir End
        ACTION_VIEW_MARKING_SHIFT_EMPLOYEE_UI,
        ACTION_VIEW_SITE_CCMAIL_USER_UI,
        ACTION_VIEW_LUMPLPaySlipUpload_UI,
        ACTION_VIEW_SalaryApproval_UI,
        ACTION_VIEW_SCALE_OF_PAY_CREATION_UI,
        ACTION_VIEW_PayFixation_UI,
        ACTION_VIEW_ProfitPosting_UI,
        ACTION_VIEW_SFund_PUI,
        ACTION_VIEW_AuthorisedSignatory_UI,
        ACTION_VIEW_PowerOfAttorney_UI,
        ACTION_VIEW_BalanceSheet_PF_PUI,
        ACTION_VIEW_PF_Separation_Report,
        

        #region For Viewing permissions for Training
        ACTION_VIEW_TR_Activity_UI,
        ACTION_VIEW_TR_Designations_UI,
        ACTION_VIEW_TR_Employee_UI,
        ACTION_VIEW_TR_EmployeeHistory_UI,
        ACTION_VIEW_TR_ExternalTainer_List_UI,
        ACTION_VIEW_TR_ExternalTainer_UI,
        ACTION_VIEW_TR_Category_List_UI,
        ACTION_VIEW_TR_Category_UI,
        ACTION_VIEW_TR_Codeprefix_List_UI,
        ACTION_VIEW_TR_Codeprefix_UI,
        ACTION_VIEW_TR_Training_List_UI,
        ACTION_VIEW_TR_Training_UI,
        ACTION_VIEW_TR_Type_UI,
        ACTION_VIEW_TR_DepartmentalMatrix_UI,
        ACTION_VIEW_TR_Miscellaneous_UI,
        ACTION_VIEW_TR_WeightageSettings_UI,
        ACTION_VIEW_TR_MatrixMapping_UI,
        ACTION_VIEW_TR_VersionGenerate_UI,
        ACTION_VIEW_TR_VersionList_UI,
        ACTION_VIEW_TR_PathCreation_UI,
        ACTION_VIEW_TR_BypassConfiguration_UI,
        ACTION_VIEW_TR_NeedAnalysis_UI,
        ACTION_VIEW_TR_AnalysisApproval_UI,
        ACTION_VIEW_TR_Calendar_UI,
        ACTION_VIEW_TR_CalendarSummary_UI,
        ACTION_VIEW_TR_MonthlyScheduling_UI,
        ACTION_VIEW_TR_Reminder_UI,
        ACTION_VIEW_TR_Confirmation_UI,
        ACTION_VIEW_TR_Attendance_UI,
        ACTION_VIEW_TR_Evaluation_UI,
        ACTION_VIEW_TR_NoticeBoard_UI,
        ACTION_VIEW_KPIMarginBasedInc_UI,
        ACTION_VIEW_KPIScoring_Branch_UI,
        ACTION_VIEW_KPIScoring_Department_UI,
        ACTION_VIEW_KPIScoring_Institution_UI,
        ACTION_VIEW_KPIScoring_Zone_UI,
        ACTION_VIEW_KPITarget_Branch_UI,
        ACTION_VIEW_KPITarget_Department_UI,
        ACTION_VIEW_KPITarget_Institution_UI,
        ACTION_VIEW_KPITarget_Zone_UI,
        #endregion

        ACTION_View_ATTENDANCE_REPORT_UI,       // WALI :: 21-Apr-2015
        ACTION_VIEW_EmploymentTermination_UI,   // WALI :: 20-Jun-2014
        ACTION_VIEW_Guarantor_UI,               // WALI :: 22-Jun-2014
        ACTION_View_University_Entry_UI,
        ACTION_View_University_List_UI,
        ACTION_VIEW_LeaveAdjustmentHeadConfig_UI,
        ACTION_View_Employee_Type_Entry_UI,
        ACTION_View_Employee_Type_List_UI,
        ACTION_View_Job_Details_UI,
        ACTION_VIEW_JobDetails_List_UI,
        ACTION_VIEW_JobDetailsGrade_UI,
        ACTION_VIEW_Verification_For_Encashment_UI,
        ACTION_VIEW_Zone_UI,
        ACTION_VIEW_ZoneList_UI,
        ACTION_VIEW_LeaveEncashApprove_UI,
        ACTION_VIEW_AuthorityCreate_UI,
        ACTION_VIEW_AuthorityList_UI,
        ACTION_View_Punishment_nature_List_UI,
        ACTION_View_Punishment_nature_Entry_UI,
        ACTION_VIEW_Qualification_UI,
        ACTION_VIEW_ServiceAgreement_UI,
        ACTION_View_Reward_Type_Entry_UI,
        ACTION_View_Reward_Type_List_UI,
        ACTION_VIEW_RewardRecognition_UI,
        ACTION_VIEW_SocietyMembership_UI,
        ACTION_VIEW_CompanyDivisionList_UI,
        ACTION_VIEW_CompanyDivisionEntry_UI,
        ACTION_VIEW_ReportSignatory_UI,

        #region View Actions for Provident Fund :: WALI :: 29-Dec-2014
        ACTION_View_PF_Modify_Fund_Balance_UI,      // WALI :: 05-Mar-2015
        ACTION_View_PF_Modify_Employee_Balance_UI,      // WALI :: 05-Mar-2015
        ACTION_View_PF_Distribute_ProvisionedProfit_UI,
        ACTION_View_PF_Employee_JournalEntry_UI,
        ACTION_View_PF_Employee_List_For_Payment_UI,
        ACTION_View_PF_Employee_List_For_Seperation_UI,
        ACTION_View_PF_Employee_Payment_UI,
        ACTION_View_PF_Employee_Seperation_UI,
        ACTION_View_PF_Fund_Distribution_UI,
        ACTION_View_PF_Investment_UI,
        ACTION_View_PF_Investment_List_UI,
        ACTION_View_PF_Investment_Settlement_UI,
        ACTION_View_PF_Investment_Settlement_List_UI,
        ACTION_View_PF_Journal_Entry_UI,
        ACTION_View_PF_Investment_Provision_UI,
        ACTION_View_PF_Terms_Of_Separation_UI,
        ACTION_View_PF_PFFundRecieve_UI,

        // Reports        
        ACTION_VIEW_PFStatement_PUI,
        ACTION_VIEW_InvestmentRegister_PF_PUI,
        ACTION_VIEW_InvEncashmentRegister_PF_PUI,
        ACTION_VIEW_InvestmentRegister_PUI,
        #endregion

        #region View Actions for WPPWF :: WALI :: 29-Dec-2014
        ACTION_View_WPPWF_Deposit_UI,
        ACTION_View_WPPWF_Profit_Distribution_UI,
        ACTION_View_WPPWF_Employee_Journal_Entry_UI,
        ACTION_View_WPPWF_Employee_Seperation_UI,
        ACTION_View_WPPWF_Fund_Allocation_UI,
        ACTION_View_WPPWF_Fund_Heads_UI,
        ACTION_View_WPPWF_Fund_Journal_Entry_UI,
        ACTION_View_WPPWF_Fund_Payment_UI,
        ACTION_View_WPPWF_Investment_UI,
        ACTION_View_WPPWF_Investment_List_UI,
        ACTION_View_WPPWF_Investment_Settlement_UI,
        ACTION_View_WPPWF_Investment_Settlement_List_UI,
        ACTION_View_WPPWF_Organization_Entry_UI,
        ACTION_View_WPPWF_Organization_List_UI,
        ACTION_View_WPPWF_Product_Entry_UI,
        ACTION_View_WPPWF_Provision_Process_UI,
        ACTION_View_WPPWF_Terms_Of_Seperation_UI,
        ACTION_VIEW_DueCalculation_WPPWF_UI,
        ACTION_View_WPPWF_Modify_Employee_Balance_UI,
        ACTION_View_WPPWF_Modify_Fund_Balance_UI,
        #endregion

        #region RONY :: View Separation Type
        ACTION_VIEW_SeparationTypeMapping_UI,
        ACTION_VIEW_SEPARATION_TYPE_UI,
        #endregion

        //Shakir start :: Final Settlement
        ACTION_VIEW_FINAL_SETT_APPROVAL_AUTHORITY_CONFIG_UI,
        ACTION_VIEW_FINAL_SETT_APPROVAL_PROCESS_UI,
        //Shakir End :: Final Settlement

        //Shakir Start :: Transfer Bulk
        ACTION_VIEW_TRANSFER_APPROVAL_PATH_CONFIG_UI,
        ACTION_VIEW_TRANSFER_APPROVAL_PROCESS_UI,
        ACTION_VIEW_TRANSFER_AUTHORITY_CONFIG_UI,
        ACTION_VIEW_TRANSFER_BULK_UI,
        ACTION_VIEW_TRANSFER_AUTHORITY_CONFIG_LIST_UI,
        //Shakir End :: Transfer Bulk

        //Start :: ~~
        ACTION_VIEW_REQUISITION_APPROVAL_PATH_CONFIG_UI,
        ACTION_VIEW_REQUISITION_APPROVAL_PROCESS_UI,
        ACTION_VIEW_REQUISITION_AUTHORITY_CONFIG_UI,
        ACTION_VIEW_REQUISITION_BULK_UI,
        ACTION_VIEW_REQUISITION_AUTHORITY_CONFIG_LIST_UI,
        //

        //Rony :: Transfer Bulk List
        ACTION_VIEW_TRANSFER_BULK_LIST_UI,
        //End


        // Update by Asif, 11-mar-12
        ACTION_VIEW_JOB_SPECIFICATION_UI,
        ACTION_VIEW_JOB_SPECIFICATIONS_UI,
        // Update end

        // Update by Asif, 11-mar-12
        ACTION_VIEW_JOB_TITLE_UI,
        ACTION_VIEW_JOB_TITLES_UI,
        // Update end

        // Update by Asif, 11-mar-12
        ACTION_VIEW_JOB_VACANCIES_UI,
        ACTION_VIEW_JOB_VACANCY_UI,
        // Update end

        // Update by Asif, 11-mar-12
        ACTION_VIEW_APPLICANTS_UI,
        // Update end

        //Requisition
        ACTION_VIEW_AppoveRequisition_UI,
        //End


        ACTION_VIEW_SHIFTING_UI,
        ACTION_VIEW_SHIFTINGHIST_UI,
        ACTION_VIEW_SHIFTINGPLAN_UI,
        ACTION_VIEW_ATTENDANCE_LOGIN_UI,
        ACTION_VIEW_EXCEPTIONAL_LOGIN_UI,
        ACTION_VIEW_ATTREPORT_UI,
        ACTION_VIEW_ATTREPORT_ADMIN_UI,
        ACTION_VIEW_ATTREGISTER_UI,
        ACTION_VIEW_ATTSUMMARY_UI,
        ACTION_VIEW_SHIFTINGPLANREPORT_UI,
        ACTION_VIEW_LFAAssign_UI,
        ACTION_VIEW_LeavePlanRuleCreation_UI,
        ACTION_VIEW_LeavePlanSubmission_UI,
        ACTION_VIEW_LeavePlanApproval_UI,
        ACTION_VIEW_EmployeeOCSMembers_UI,
        ACTION_VIEW_EmployeeOCSMembership_UI,
        ACTION_VIEW_OverTime_Category_UI,
        ACTION_VIEW_Employee_Accommodation_Plan_UI,

        //////// view
        ACTION_VIEW_DISCIPLINARYTYPE,
        ACTION_VIEW_DISCIPLINARY,
        ACTION_VIEW_DISCIPLINARYTYPEGRID_UI,
        ACTION_VIEW_EmployeeCost_PUI,

        // Competency Profile :: WALI       21-Apr-2014
        ACTION_VIEW_Competency_Profile_UI,

        ACTION_VIEW_DESEASES_LIST_UI,
        ACTION_VIEW_DESEASES_ENTRY_UI,
        ACTION_VIEW_AREA_LIST_UI,
        ACTION_VIEW_AREA_ENTRY_UI,
        ACTION_VIEW_DEGREE_LIST_UI,
        ACTION_VIEW_DEGREE_ENTRY_UI,
        ACTION_VIEW_MAJOR_SUBJECT_LIST_UI,
        ACTION_VIEW_MAJOR_SUBJECT_ENTRY_UI,
        ACTION_VIEW_GROUP_LIST_UI,
        ACTION_VIEW_GROUP_ENTRY_UI,
        ACTION_VIEW_EDUCATION_BOARD_ENTRY_UI,
        ACTION_VIEW_EDUCATION_BOARD_LIST_UI,
        ACTION_VIEW_TR_ExternalTainer_ENTRY_UI,
        ACTION_VIEW_FILE_LIST_UI,
        ACTION_TR_UPLOADEDFILE_ENTRY_UI,
        ACTION_VIEW_instantTrainingRecord_UI,
        ACTION_VIEW_TRAINING_SUMMARY_RECORD_REPORT,
        ACTION_VIEW_TR_CV_REPORT,
        ACTION_VIEW_INDIBIDUAL_EMPLOYEE_TRAINING_REPORT,
        ACTION_VIEW_INDIVIDUAL_TRAINING_NEED_ANALYSISI_REPORT,
        ACTION_VIEW_INSTANT_TRAINING_REPORT,
        ACTION_VIEW_JOB_DESCRIPTION_REPORT,
        ACTION_VIEW_TRAINING_NEED_ANALYSISI_REPORT,
        ACTION_VIEW_SIMarginParameter_UI,


        //New----For training report.----- SHAKIR 14.12.2014
        ACTION_VIEW_TR_PERSONAL_DETAILS,
        ACTION_VIEW_TR_CURRICULUM_VITAE_REPORT,
        ACTION_VIEW_SOP_TRAINING_NEED_ANALYSISI_REPORT,
        ACTION_VIEW_TR_SOP_TRAINING_RECORD,
        ACTION_VIEW_ANNUAL_TNA_REPORT,
        ACTION_VIEW_ANNUAL_TRAINING_RECORD,
        ACTION_VIEW_ANNUAL_TRAINING_REPORT,
        ACTION_VIEW_TR_SOP_TRAINING_REPORT,
        ACTION_VIEW_TR_TRAINER_LIST_REPORT,
        ACTION_VIEW_TR_TRAINING_SCHEDULE_REPORT,

        ACTION_VIEW_PaySlip_WPPWF_PUI,

        ACTION_View_WPPWF_Employee_List_For_Seperation_UI,
        ACTION_View_WPPWF_Employee_Payment_UI,

        ACTION_View_PF_Fund_Recieve_Exceptional_UI,
        ACTION_View_PF_BalanceSheet_PUI,

        #region View HRAS :: Rony
        ACTION_VIEW_TaskType_UI,
        ACTION_VIEW_Vendor_UI,
        ACTION_View_Service_Type_Entry_UI,
        ACTION_View_Service_Type_List_UI,
        ACTION_View_CheckList_Entry_UI,
        ACTION_View_CheckList_List_UI,
        ACTION_View_Task_Entry_UI,
        ACTION_View_Task_List_UI,
        ACTION_View_Allocation_Task_UI,
        ACTION_View_Allocation_Execution_UI,
        ACTION_View_Job_Status_Task_UI,
        #endregion

        ACTION_VIEW_LETTER_SUBJECT_UI,   //RONY :: 21-JAN-2015
        ACTION_View_LETTER_SUBJECT_LIST_UI,
        ACTION_VIEW_Generate_Letter_UI,
        ACTION_VIEW_Template_Of_Letter_UI,

        // Termination Type :: RONY :: 10 MAR 2015
        ACTION_View_Termination_TYPE_UI,
        ACTION_View_Termination_TYPE_LIST_UI,
        //end

        //GL Mapping for Salary Transaction Report:: Rony
        ACTION_VIEW_GL_MAPPING_FOR_SALARY_TRANSACTION_UI,
        //end

        ACTION_View_Assign_Asset_UI,


        //shakir start
        ACTION_VIEW_EMP_REGISTRATION_BULK_UI,
        ACTION_VIEW_FILE_BANK_UI,
        ACTION_VIEW_HSM_REPORT_UI,
        ACTION_VIEW_REPORTING_FOR_HSM,
        ACTION_VIEW_LEAVE_SANCTION_ADVICE_UI,
        ACTION_VIEW_TR_BUDGET_GENERATE_UI,
        ACTION_VIEW_TR_COST_HEAD_LIST,
        ACTION_VIEW_TR_COST_SETUP_BUDGET_UI,
        ACTION_VIEW_TR_NEED_ASSESSMENT_UI,
        ACTION_VIEW_TR_ADVICE_LIST_UI,
        ACTION_VIEW_TR_COST_AND_PAYMENT_RULES_UI,
        ACTION_VIEW_COM_ASSETS_APPROVE_UI,
        ACTION_VIEW_APPLY_FOR_ASSETS_UI,
        ACTION_VIEW_ATTENDANCE_MONTHS_UI,
        ACTION_VIEW_SALARY_CERTIFICATE_UI,
        ACTION_VIEW_LOCAL_POSTING_UI,
        ACTION_VIEW_LAY_OFF_PARAMS_UI,
        ACTION_VIEW_PayFixation_Report_UI,
        ACTION_VIEW_Increment_Report_UI,
        ACTION_VIEW_EMP_ASS_FOR_SUCCESSION_UI,
        ACTION_VIEW_EMP_INFO_REPORT_UI,
        ACTION_VIEW_BENEFIT_RULE_MAINTAINANCE_UI,
        ACTION_VIEW_OTHERS_BENEFIT_LIST_UI,
        ACTION_VIEW_ASSETS_LIMIT_CONFIG_GRDEWISE_UI,
        ACTION_VIEW_ASSET_CATEGORY_CONFIG_UI,
        ACTION_VIEW_ASSETS_CATEGORY_LIST_UI,
        ACTION_VIEW_ASSETS_CAT_CREATION_UI,
        ACTION_VIEW_ASSETES_TYPE_LIST_UI,
        ACTION_VIEW_ASSETS_TYPE_CREATION_UI,
        ACTION_VIEW_CERTIFICATE_LIST_UI,
        ACTION_VIEW_CERTTIFICATE_CREATION_UI,
        ACTION_VIEW_QUALIFICATION_LIST_UI,
        ACTION_VIEW_QUALIFICATION_CREATION_UI,
        ACTION_VIEW_SUBJECT_LIST_UI,
        ACTION_VIEW_MAJOR_SUB_CREATION_UI,
        ACTION_VIEW_EDU_BOARD_LIST_UI,
        ACTION_VIEW_EDU_BOARD_CREATION_UI,
        ACTION_VIEW_COMPETENCY_TYPE_LIST_UI,
        ACTION_VIEW_LIABILITY_LIST_UI,
        ACTION_VIEW_LIABILITY_CREATION_UI,
        ACTION_VIEW_GROUP_CREATION_UI,
        ACTION_VIEW_DISCIPLINARY_REPORT_UI,
        ACTION_VIEW_LOAN_ELIGIBLE_GRADES_UI,
        ACTION_VIEW_ASSETS_INPUT_FIELDS_UI,
        ACTION_VIEW_HEALTH_INPUT_FIELDS_UI,
        ACTION_VIEW_BENEFIT_CATEGORIES_UI,
        ACTION_VIEW_BENEFIT_IF_CONFIG_UI,
        ACTION_VIEW_BENEFIT_CATEGORY_CONFIG_UI,
        ACTION_VIEW_BENEFIT_APPLY_UI,
        ACTION_VIEW_BENEFIT_APPROVE_UI,
        ACTION_VIEW_ASSETS_REPORT,
        ACTION_VIEW_HEAD_WISE_BALANCE_STATUS_UI,
        ACTION_VIEW_BENEFIT_INPUT_FIELD_UI,
        ACTION_VIEW_BENEFIT_CATEGORY_UI,
        //shakir End


        ACTION_VIEW_HSM_INPUT_FIELD_UI,
        ACTION_VIEW_HSM_INPUT_FIELDS_UI,
        ACTION_VIEW_HSM_TYPE_UI,
        ACTION_VIEW_HSM_TYPES_UI,

        ACTION_VIEW_Requisition_Report_UI,

        ACTION_VIEW_Recruitment_Job_Title_UI,  // Recruitment :: WALI :: 09-Jul-2015

        ACTION_VIEW_GratuitySlip_PUI, // Payroll :: WALI :: 16-Aug-2015

        ACTION_View_Employee_Category_List_UI,
        ACTION_View_Employee_Category_Entry_UI,

        ACTION_View_Country_Division_List_UI,
        ACTION_View_Country_Division_Entry_UI,
        ACTION_View_Country_District_List_UI,
        ACTION_View_Country_District_Entry_UI,
        ACTION_View_Country_Thana_List_UI,
        ACTION_View_Country_Thana_Entry_UI,

        ACTION_VIEW_AssignLeaveMultipleEmployee_UI,

        ACTION_VIEW_Attendance_Login_WithoutCalculation_UI,

        ACTION_VIEW_EMPLOYEE_REGISTERED_REPORT_UI,
        ACTION_VIEW_LFASummary_PUI,
        ACTION_VIEW_Cost_And_Payment_role,
        ACTION_VIEW_COST_HEAD_Creation_UI,
        ACTION_VIEW_Increment_UI,
        ACTION_VIEW_Promotion_Emp_Approval_UI,
        ACTION_VIEW_SUSPEND_RULE_UI,
        ACTION_View_PF_Employee_Payment_List,
        ACTION_View_PF_Employee_Payment,
        ACTION_VIEW_SALARY_ANOMALIES,
        ACTION_VIEW_RESOURCE_TARGET_PLANING,
        ACTION_VIEW_Leave_Governance_UI,
        ACTION_VIEW_TransferAuthorityList,

        ACTION_VIEW_RELATION_ENTRY_UI,
        ACTION_VIEW_RELATION_LIST_UI,

        ACTION_VIEW_ORGANIZATION_TYPE_ENTRY_UI,
        ACTION_VIEW_ORGANIZATION_TYPE_LIST_UI,


        ACTION_VIEW_BF_FUND_TYPE_UI,
        ACTION_VIEW_BF_FUND_TYPE_LIST_UI,
        ACTION_VIEW_BF_SHARE_RATE_UI,
        ACTION_VIEW_BF_MEMBER_UI,
        ACTION_VIEW_BF_MEMBER_LIST_UI,
        ACTION_VIEW_BF_GRADEWISE_UI,

        ACTION_VIEW_INCREMENT_BANGLA_REPORT_UI,
        ACTION_VIEW_BalanceSheet_WPPWF_UI,
        ACTION_VIEW_Salary_Account_Report_UI,

        //Benevolent Fund Start
        ACTION_View_BF_Distribute_ProvisionedProfit_UI,
        ACTION_View_BF_Employee_JournalEntry_UI,
        ACTION_View_BF_Employee_List_For_Payment_UI,
        ACTION_View_BF_Employee_List_For_Seperation_UI,
        ACTION_View_BF_Employee_Payment_UI,
        ACTION_View_BF_Employee_Seperation_UI,
        ACTION_View_BF_BFFundRecieve_UI,
        ACTION_View_BF_Fund_Recieve_Exceptional_UI,
        ACTION_View_BF_Investment_UI,
        ACTION_View_BF_Investment_List_UI,
        ACTION_View_BF_Investment_Settlement_UI,
        ACTION_View_BF_Investment_Settlement_List_UI,
        ACTION_View_BF_Journal_Entry_UI,
        ACTION_View_BF_Modify_Employee_Balance_UI,
        ACTION_View_BF_Modify_Fund_Balance_UI,
        ACTION_View_BF_Investment_Provision_UI,
        ACTION_View_BF_Terms_Of_Separation_UI,


        ACTION_VIEW_InvEncashmentRegister_BF_PUI,
        ACTION_VIEW_InvestmentRegister_BF_PUI,
        ACTION_VIEW_BalanceSheet_BF_PUI,
        ACTION_VIEW_BF_Ledger_PUI,
        ACTION_VIEW_BF_Separation_Report,
        ACTION_VIEW_BFStatement_PUI,
        //Benevolent Fund End


        //Basic Accounting - Start
        ACTION_View_Acc_Supplier_List_UI,
        ACTION_View_Acc_Supplier_Entry_UI,
        ACTION_View_Acc_Customer_List_UI,
        ACTION_View_Acc_Customer_Entry_UI,
        ACTION_View_Acc_Reference_List_UI,
        ACTION_View_Acc_Reference_Entry_UI,
        ACTION_View_Acc_Report_Specification_UI,
        ACTION_View_ChartOfAccounts_UI,
        ACTION_View_Acc_Bank_Info_UI,
        ACTION_View_Acc_Voucher_Entry_UI,
        ACTION_View_Acc_Voucher_Approve_UI,
        ACTION_View_Acc_Journal_Voucher_Entry_UI,
        ACTION_View_Acc_ApprovalPath_UI,
        ACTION_View_Acc_MemoFixation_UI,
        ACTION_View_Acc_VoucherTemplate_UI,
        ACTION_View_Acc_VoucherTemplate_List_UI,
        ACTION_View_Acc_VoucherTemplate_Process_UI,
        ACTION_View_Acc_JournalTemplate_UI,
        ACTION_View_Acc_MonthEnd_Process_UI,

        ACTION_View_Acc_VoucherReport_UI,
        ACTION_View_Acc_TrialBalanceReport_UI,  // UI for 'Trial Balance', 'Balance Sheet'
        //Basic Accounting - End

        ACTION_VIEW_Common_Message_UI,

        ACTION_View_Separation_Approval_Path_Config_UI,
        ACTION_View_ResignationReason_Entry_UI,
        ACTION_View_ResignationReason_List_UI,
        ACTION_View_Resignation_Apply_UI,

        ACTION_View_Salary_Withhold_UI,

        ACTION_VIEW_AdvanceTax_PUI,
        ACTION_VIEW_Relative_Information_UI,
        ACTION_VIEW_Employee_Nominee_UI,
        ACTION_VIEW_Occupation_UI,
        ACTION_VIEW_SelfServiceEmployee_UI,

        ACTION_VIEW_Employee_Approval_UI,
        ACTION_VIEW_SelfEntry_Approval_UI,
        ACTION_VIEW_EmpProfile_SelfEntry_Approval_UI,

        ACTION_VIEW_Employee_Profile_Report_UI,

        ACTION_VIEW_Major_Topics_UI,
        ACTION_VIEW_Training_Organizer_UI,

        ACTION_VIEW_Gradewise_TaxRebate_PUI,
        ACTION_VIEW_Reference_Employee_UI,
        ACTION_VIEW_SelfServicePersonalHist_UI,

        ACTION_VIEW_Reference_UI,
        ACTION_VIEW_COMPETENCY_PROFILE_UI,

        ACTION_VIEW_SelfServiceJobHistory_UI,

        ACTION_VIEW_Organogram_UI,

        ACTION_VIEW_Additional_Job_History_UI,
        ACTION_VIEW_LeaveBalanceUpdate_UI,
        ACTION_VIEW_AllowanceBranches_UI,

        ACTION_VIEW_Employee_Salary_Arrear_UI, 
        
        ACTION_VIEW_Discontinue_Type_UI,
        ACTION_VIEW_Transfer_Type_UI,

        ACTION_VIEW_CBS_Upload_Data_UI,

        ACTION_VIEW_OCS_REPORT_UI,

        ACTION_VIEW_OCS_SHARE_LIST_UI,
        ACTION_VIEW_LFAAnalysis_PUI,
        ACTION_View_BF_Fund_Payment,
        ACTION_View_BF_Fund_Payment_List,

        ACTION_VIEW_OCS_SUMMARY_REPORT_UI,

        ACTION_VIEW_Reference_Employee_List_UI,
        ACTION_VIEW_Occupation_List_UI,
        ACTION_VIEW_Training_Organizer_List_UI,
        ACTION_VIEW_Major_Topics_List_UI,
        ACTION_VIEW_Transfer_Type_List_UI,
        ACTION_VIEW_Discontinue_Type_List_UI,
        ACTION_VIEW_LEAVE_APPROVAL_POLICY_UI,
        ACTION_VIEW_New_EmployeeList_Report_UI,
        ACTION_VIEW_BenefitPaidInCash_PUI,
        ACTION_VIEW_RETIREMENT_SALARY_POLICY_UI,

        ACTION_VIEW_WITHHOLD_ENTRY_UI,
        ACTION_VIEW_Organogram_Manpower_UI,
        ACTION_VIEW_ARREAR_LIST_UI,

        ACTION_VIEW_QUESTION_ENTRY_UI,
        ACTION_VIEW_QUESTION_LIST_UI,
        ACTION_VIEW_SEP_LIABILITY_QUESTION_UI,
        ACTION_VIEW_SEP_LIABILITY_ANSWER_UI,

        ACTION_VIEW_EMPWISE_SALARY_PROCESS_UI,
        ACTION_VIEW_RANKING_UI,
        ACTION_VIEW_KPI_MAPPING_UI,
        ACTION_VIEW_KPI_MAPPING_EMPWISE_UI,

        ACTION_View_KPI_Approval_Path_Config_UI,
        ACTION_VIEW_KPI_ITEM_CONFIG_UI,

        ACTION_VIEW_KPI_APPROVAL_UI,

        ACTION_VIEW_KPIScoring_Recommendation_UI,
        ACTION_View_KPI_Scoring_Authority_UI,
        ACTION_VIEW_AllocateLeaveQuota_WithoutPolicy_UI,
        ACTION_VIEW_SITE_Category_UI,
        ACTION_VIEW_SITE_Category_List_UI,
        ACTION_VIEW_KPI_PromotionPolicyList_UI,

        ACTION_VIEW_Promotion_Policy_UI,
        ACTION_VIEW_Benefit_Approval_Authority_UI,

        ACTION_View_Bonus_Posting_UI,
        ACTION_View_Training_Report_UI,
        ACTION_VIEW_CBS_Bonus_Upload_Data_UI,

        ACTION_VIEW_PF_LoanDisbursementReport_UI,
        ACTION_View_Bonus_Modification_UI,

        ACTION_VIEW_Termination_Approve_UI,
        ACTION_View_HEALTHSAFTY_VERIFIER,
        ACTION_View_HEALTHSAFTY_APPROVAL,
        ACTION_VIEW_Disciplinary_Report_UI,
        ACTION_VIEW_Recontract_Employee_UI,
        ACTION_VIEW_ReNew_Employee_UI,
        ACTION_View_Relative_Information_Report_UI,

        ACTION_View_JOB_EXAM_ORGANIZER_UI,
        ACTION_View_JOB_EXAM_RESULT_UPLOAD,
        ACTION_VIEW_AbsenceEmpolyees_UI,
        ACTION_VIEW_GroupWiseSalaryReport_UI,
        ACTION_VIEW_YearWisePerformance_PUI,

        ACTION_VIEW_AllowanceLiability_UI,
        ACTION_VIEW_GRADE_CATEGORY_UI,
        ACTION_VIEW_Account_Statement_UI,
        ACTION_VIEW_LFA_SUMMARY_UI,
        ACTION_VIEW_LFA_REPORT_UI,
        ACTION_VIEW_LEA_STATEMENT_UI,
        ACTION_VIEW_LFA_ANYLYSIS_UI,
        ACTION_VIEW_EMPLOYEE_SUMMARY_UI,
        ACTION_VIEW_EMPLOYEE_ALL_INFO_REPORT_UI,
        ACTION_VIEW_MANPOWER_DETAIL_REPORT_UI,
        ACTION_VIEW_EMPLOYEE_COUNT_REPORT_UI,
        ACTION_VIEW_RELATIVE_INFORMATION_REPORT_UI,
        ACTION_VIEW_DISCIPLINARY_RECORD_REPORT_UI,
        ACTION_VIEW_NOC_REPORT_UI,


       ACTION_VIEW_Internal_Memo_UI,
       ACTION_VIEW_Earning_Report_UI,
       ACTION_VIEW_Headcount_Variance_UI,
       ACTION_VIEW_SHIFTINGGROUP_UI,
       ACTION_VIEW_SHIFTINGGROUP_ENTRY_UI,
       ACTION_VIEW_BULK_ATTENDANCE_UPLOAD_UI,
       ACTION_VIEW_BULK_ATTENDANCE_UPLOAD_APPROVAL_UI,
       ACTION_VIEW_ATTANDANCE_SUMMARY_UI,
       ACTION_VIEW_ATTANDANCE_SUMMARY_APPROVAL_UI,
       ACTION_VIEW_ATTENDANCE_SPAM_UI,
       ACTION_VIEW_HolidayType_UI,
       ACTION_VIEW_Overtime_Calculation_UI,
       ACTION_VIEW_HolidayType_Create_UI,
       ACTION_VIEW_BranchWise_Holidys_UI,

       ACTION_VIEW_Promotion_Pending_UI,
       ACTION_VIEW_Increment_Pending_UI,
       ACTION_VIEW_Outside_Payroll_Process_UI,
       ACTION_VIEW_ATTENDANCE_SPAM_APPROVAL_UI,
 //      ACTION_VIEW_SPAM_LIST_UI,
       ACTION_VIEW_ATTENDANCE_MODIFICATION_UI,
       ACTION_VIEW_ATTENDANCE_MODIFICATION_APPROVAL_UI,
       ACTION_VIEW_ATTENDANCE_REGULATORY_UI,
       ACTION_VIEW_ADDITIONAL_SHIFTINGPLAN_UI,
       ACTION_VIEW_ADDITIONAL_SHIFTINGPLAN_MODIFY_UI,
        #endregion


        // system action IDs: unknown ID
        ACTION_UNKOWN_ID,

        NA_ACTION_SYSTEM_DATETIME_GET,

        // system action IDs: DB connection related action IDs
        ACTION_DB_CONNECTION_GET,
        ACTION_DB_CONNECTION_CLOSE,
        ACTION_DB_TRANSACTION_GET,
        ACTION_DB_TRANSACTION_COMMIT,
        ACTION_DB_TRANSACTION_ROLLBACK,

        //user authentication related action IDs
        ACTION_USER_AUTHENTICATE,
        ACTION_USER_PWD_CHANGE,

        // system action IDs:  primary key an ID related action IDs
        ACTION_NEXT_PK_GET,
        ACTION_NEXT_ID_GET,
        ACTION_DOES_ID_EXIST,
        // end of system action IDs //

        // system Information action IDs: 
        ACTION_SYSTEMINFO_ADD,
        ACTION_SYSTEMINFO_GET,




        // Department related action IDs
        ACTION_DEPARTMENT_ADD,
        NA_ACTION_DEPARTMENT_GET,
        ACTION_DEPARTMENT_DEL,
        ACTION_DEPARTMENT_UPD,
        ACTION_DOES_DEPARTMENT_EXIST,
        NA_ACTION_GET_DEPARTMENT_LIST,
        NA_ACTION_GET_DEPARTMENT,
        NA_ACTION_GET_DEPARTMENTLISTFORDELEGATEUSER,        

        //
        // Responsibility related action IDs
        ACTION_RESPONSIBILITYCENTER_ADD,
        NA_ACTION_RESPONSIBILITYCENTER_GET,
        ACTION_RESPONSIBILITYCENTER_DEL,
        ACTION_RESPONSIBILITYCENTER_UPD,
        NA_ACTION_Q_RESPONSIBILITYCENTER_ALL,
        ACTION_DOES_RESPONSIBILITYCENTER_EXIST,

        //
        // Salary Review  related action IDs
        NA_ACTION_SALARYREVIEW_PROCESS,
        ACTION_PARATING_ADD,
        ACTION_PARATING_DEL,
        ACTION_PARATING_UPD,
        NA_ACTION_Q_ALL_PARATING,
        ACTION_DOES_PARATING_EXIST,
        ACTION_PAPARFORANGE_ADD,
        ACTION_PAPARFORANGE_UPD,
        NA_ACTION_Q_ALL_PAPARFORANGE,
        ACTION_DOES_PAPARFORANGE_EXIST,
        NA_ACTION_Q_ALL_PAREMPLOYEE,
        ACTION_PAREMPLOYEE_ADD,
        ACTION_PAREMPLOYEE_UPD,
        ACTION_DOES_PAREMPLOYEE_EXIST,
        ACTION_COMPERBONUS_ADD,
        ACTION_COMPERBONUS_UPD,
        NA_ACTION_Q_ALL_COMPERBONUS,
        ACTION_DOES_COMPERBONUS_EXIST,
        //
        // Bank related action IDs
        ACTION_BANK_ADD,
        NA_ACTION_BANK_GET,
        ACTION_BANK_DEL,
        ACTION_BANK_UPD,
        NA_ACTION_GET_BANK_LIST,
        NA_ACTION_GET_BANK,
        ACTION_DOES_BANK_EXIST,

        //
        // Eployee related action IDs Update by Asif, 08-mar-12
        NA_ACTION_GET_EMPLOYEE_BY_EMPLOYEECODE,
        // Update end

        //
        // PFParameter related action IDs
        ACTION_PFPARAMETER_ADD,
        ACTION_PFPARAMETER_DEL,
        ACTION_DOES_PFPARAMETER_EXIST,
        NA_ACTION_Q_ALL_PFPARAMETER,
        NA_ACTION_Q_ALL_PFVALUE_FOR_EMPLOYEE,

        /// Salary Head Configaration

        ACTION_SALARYHEADCONFIG_ADD,
        NA_ACTION_Q_ALL_SALARYHEADCONFIG,

        //Batch Process related action Ids
        NA_ACTION_BATCH_PFPARAMETER_GET,
        NA_ACTION_Q_ALL_EMPLOYEE_HAVE_LOAN,
        ACTION_BATCH_SALARY_PROCESS_INFO_SAVE,
        ACTION_BATCH_SALARY_PROCESS,
        ACTION_BATCH_MONTH_END_PROCESS,
        ACTION_DOES_SALARY_PROCESS_EXIST,
        ACTION_DOES_SALARY_PROCESS_MONTH_EXIST,
        ACTION_SALARYPROCESS_DEL,
        ACTION_DOES_MONTH_END_PROCESS_EXIST,
        ACTION_BATCH_CHANGED_SP_SAVE,
        NA_ACTION_BATCH_Q_ALL_SALARY_PROCESS,
        NA_ACTION_BATCH_Q_ALL_PROCESS_DATA,
        NA_ACTION_BATCH_Q_ALL_ALLOWDEDUCTGRADES,
        NA_ACTION_Q_ALL_EMPLOYEE_ARREAR_INFO,
        // GratuityParameter related action IDs
        ACTION_GRATUITYPARAMETER_ADD,
        ACTION_GRATUITYPROVISION_ADD,
        ACTION_DOES_GRATUITYPARAMETER_EXIST,
        ACTION_GRATUITYPARAMETER_UPD,
        NA_ACTION_Q_ALL_GRATUITYPARAMETER,
        NA_ACTION_BATCH_PREV_MONTH_PROCESS_INFO_GET,
        //
        // Laid Off Parameter related action IDs

        ACTION_LAYOFFPARAMETER_ADD,
        ACTION_DOES_LAYOFFPARAMETER_EXIST,
        ACTION_LAYOFFPARAMETER_UPD,
        ACTION_LAYOFFPARAMETER_DEL,
        NA_ACTION_Q_ALL_LAYOFFPARAMETER,
        NA_ACTION_Q_ALL_LAYOFFRULE,

        ACTION_LAYOFFEMPLOYEE_ADD,
        ACTION_LAYOFFEMPLOYEE_UPD,
        ACTION_LAYOFFEMPLOYEE_DEL,
        NA_ACTION_Q_ALL_LAYOFFEMPLOYEE,

        // Final Satelment related action IDs
        ACTION_FINAL_SATELMENT_PROCESS,

        // User related action IDs
        NA_ACTION_SEC_USER_LOGOUT,
        ACTION_SEC_ROLE_ADD,
        ACTION_SEC_ROLE_DEL,
        ACTION_SEC_ROLE_UPD,
        NA_ACTION_Q_ALL_ROLE,
        ACTION_DOES_ROLE_EXIST,
        ACTION_SEC_USER_ADD,
        NA_ACTION_SEC_PWD_CHANGE,
        ACTION_SEC_USER_UPD,
        ACTION_SEC_USER_LOCKED_SET,
        ACTION_SEC_USER_LOCKED_GET,
        ACTION_SEC_CONCURRENCE_LOGIN_GET,
        ACTION_SEC_CONCURRENCE_LOGIN_SET,
        ACTION_SEC_FAILED_LOGIN_GET,
        ACTION_SEC_FAILED_LOGIN_SET,
        ACTION_SEC_MAXCONCURRENCE_LOGIN_GET,
        ACTION_SEC_MAXFAILED_LOGIN_GET,
        NA_ACTION_Q_ALL_USER,
        NA_ACTION_Q_ALL_LOGIN_USER,
        NA_ACTION_SEC_USER_GET,
        ACTION_DOES_USER_EXIST,
        ACTION_DOES_PREVILAGE_EXIST,
        ACTION_DOES_EMPLOYEE_EXIST_IN_USER,
        NA_ACTION_Q_ALL_PREVILAGE,
        NA_ACTION_Q_ALL_ROLE_BY_USER,
        NA_ACTION_Q_ALL_EMPLOYEE_BY_USER,
        NA_ACTION_Q_ALL_PREVILAGE_BY_ROLE,

        //
        ACTION_EMPLOYEE_ALLOWANCE_ADD,
        NA_ACTION_EMPLOYEE_ALLOWANCE_GET_ALL,

        // Bank Branch related action IDs
        ACTION_BANKBRANCH_ADD,
        ACTION_BANKBRANCH_GET,
        ACTION_BANKBRANCH_DEL,
        ACTION_BANKBRANCH_UPD,
        NA_ACTION_GET_BANKBRANCH_LIST,
        NA_ACTION_GET_BANKBRANCH,
        ACTION_DOES_BANKBRANCH_EXIST,

        //
        // location related action ids
        ACTION_LOCATION_ADD,
        ACTION_LOCATION_DEL,
        ACTION_LOCATION_UPD,
        NA_ACTION_GET_LOCATION_LIST,
        ACTION_DOES_LOCATION_EXIST,

        //company related
        ACTION_COMPANY_ADD,
        ACTION_COMPANY_DEL,
        ACTION_COMPANY_UPD,
        ACTION_DOES_COMPANY_EXIST,
        NA_ACTION_GET_COMPANY_LIST,

        // Designation related action IDs
        ACTION_DESIGNATION_ADD,
        NA_ACTION_DESIGNATION_GET,
        ACTION_DESIGNATION_DEL,
        ACTION_DESIGNATION_UPD,
        ACTION_DOES_DESIGNATION_EXIST,
        NA_ACTION_GET_DESIGNATION_LIST,
        NA_ACTION_GET_DESIGNATION,
        // Employee related action IDs
        ACTION_EMPLOYEE_ADD,
        NA_ACTION_EMPLOYEE_GET,
        ACTION_EMPLOYEE_DEL,
        ACTION_EMPLOYEE_UPD,
        ACTION_DOES_EMPLOYEE_EXIST,
        NA_ACTION_GET_EMPLOYEE_LIST,
        NA_ACTION_GET_EMPLOYEE_DETAIL,
        NA_ACTION_GET_EMPLOYEE,
        NA_ACTION_GET_EMPLOYEE_HIST,
        ACTION_EMPLOYEE_TRANSFER,
        ACTION_EMPLOYEE_PROMOTION,
        ACTION_EMPLOYEE_DISCONTINUE,
        ACTION_EMPLOYEE_LEAVE_WITHOUT_PAY,
        ACTION_EMPLOYEE_JOB_CONFIRMATION,
        ACTION_EMPLOYEE_JOIN_PFMEMBERSHIP,
        ACTION_EMPLOYEE_JOIN_PFMEMBERSHIP_UPD,
        ACTION_EMPLOYEE_CANCEL_LAST_CHANGE,



        // Function related action IDs
        ACTION_FUNCTION_ADD,
        NA_ACTION_FUNCTION_GET,
        ACTION_FUNCTION_DEL,
        ACTION_FUNCTION_UPD,
        ACTION_DOES_FUNCTION_EXIST,
        NA_ACTION_GET_FUNCTION_LIST,
        NA_ACTION_GET_FUNCTION,

        //costcenter related
        ACTION_COSTCENTER_ADD,
        ACTION_COSTCENTER_DEL,
        ACTION_COSTCENTER_UPD,
        NA_ACTION_GET_COSTCENTER_LIST,
        ACTION_DOES_COSTCENTER_EXIST,
        //Account Head related
        ACTION_ACCOUNTHEAD_ADD,
        ACTION_ACCOUNTHEAD_DEL,
        ACTION_ACCOUNTHEAD_UPD,
        NA_ACTION_GET_ACCOUNTHEAD_LIST,
        ACTION_DOES_ACCOUNTHEAD_EXIST,
        ACTION_DOES_FIXEDACCOUNT_EXIST,
        NA_ACTION_GET_ACCOUNTHEAD_ID,
        //Activity
        ACTION_ACTIVITY_ADD,
        ACTION_ACTIVITY_DEL,
        ACTION_ACTIVITY_UPD,
        NA_ACTION_GET_ACTIVITY_LIST,
        ACTION_DOES_ACTIVITY_EXIST,
        //Attendance Month
        ACTION_ATTENDANCE_DAY_ADD,
        ACTION_DOES_ATTENDANCE_DAY_EXIST,
        NA_ACTION_Q_ALL_ATTENDANCE_DAY,
        ACTION_ATTENDANCEMONTH_ADD,
        ACTION_ATTENDANCEMONTH_DEL,
        ACTION_ATTENDANCEMONTH_UPD,
        NA_ACTION_GET_ATTENDANCEMONTH_LIST,
        ACTION_DOES_ATTENDANCEMONTH_EXIST,

        //Site
        ACTION_SITE_ADD,
        ACTION_SITE_DEL,
        ACTION_SITE_UPD,
        NA_ACTION_GET_SITE_LIST,
        ACTION_DOES_SITE_EXIST,
        //PayrollSite
        ACTION_PAYROLLSITE_ADD,
        ACTION_PAYROLLSITE_DEL,
        ACTION_PAYROLLSITE_UPD,
        NA_ACTION_GET_PAYROLLSITE_LIST,
        ACTION_DOES_PAYROLLSITE_EXIST,


        //bonus related
        ACTION_BONUS_UPD,
        ACTION_BONUS_ADD,
        ACTION_BONUS_DEL,
        ACTION_DOES_BONUS_EXIST,
        NA_ACTION_Q_BONUS_ALL,

        ACTION_GRADE_UPD,
        ACTION_GRADE_ADD,
        ACTION_GRADE_DEL,
        ACTION_DOES_GRADE_EXIST,
        NA_ACTION_GET_GRADE_LIST,
        ACTION_DOES_GRADE_USED_IN_ALLOWDEDUCT,
        ACTION_DOES_GRADE_EXIST_IN_ALLOWDEDUCT,

        //Income Tax Parameter related
        ACTION_ITPARAMETER_ADD,
        ACTION_ITPARAMETER_UPD,
        ACTION_ITPARAMETER_DEL,

        NA_ACTION_Q_ITPARAMETER_ALL,
        ACTION_ITSLAB_ADD,
        NA_ACTION_Q_ITSLAB_ALL,
        ACTION_DOES_ITPARAMETER_EXIST,
        NA_ACTION_GET_TAX_STATEMENT,
        NA_ACTION_Q_TAXHISTORY_BY_SALARYYEAR,
        //currency related
        ACTION_CURRENCY_ADD,
        ACTION_CURRENCY_DEL,
        ACTION_CURRENCY_UPD,
        ACTION_DOES_CURRENCY_EXIST,
        NA_ACTION_GET_CURRENCY_LIST,
        ACTION_CURRENCY_CONV_ADD,
        ACTION_CURRENCY_CONV_DEL,
        ACTION_CURRENCY_CONV_UPD,
        NA_ACTION_Q_ALL_CURRENCY_CONV,
        ACTION_DOES_CURRENCY_CONV_EXIST,

        ACTION_EMPLOYEE_CURRENCY_ADD,
        ACTION_EMPLOYEE_CURRENCY_DEL,
        ACTION_EMPLOYEE_CURRENCY_UPD,
        NA_ACTION_Q_ALL_EMPLOYEE_CURRENCY,
        ACTION_DOES_EMPLOYEE_CURRENCY_EXIST,

        ACTION_EMPLOYEE_FC_SALARY_ADD,
        ACTION_EMPLOYEE_FC_SALARY_DEL,
        ACTION_EMPLOYEE_FC_SALARY_UPD,
        NA_ACTION_Q_ALL_EMPLOYEE_FC_SALARY,
        ACTION_DOES_EMPLOYEE_FC_SALARY_EXIST,
        // Allowance Deduction related action ids
        ACTION_ALLOWDEDUCT_ADD,
        NA_ACTION_ALLOWDEDUCT_GET,
        ACTION_ALLOWDEDUCT_UPD,
        ACTION_ALLOWDEDUCT_DEL,
        ACTION_DOES_ALLOWDEDUCT_EXIST,
        NA_ACTION_GET_ALLOWDEDUCT_LIST,

        ACTION_ALLOWDEDUCTPARAM_ADD,
        NA_ACTION_ALLOWDEDUCTPARAM_GET,
        ACTION_ALLOWDEDUCTPARAM_UPD,
        ACTION_ALLOWDEDUCTPARAM_DEL,
        NA_ACTION_Q_ALLOWDEDUCTPARAM_ALL,

        ACTION_LOANTYPE_ADD,
        ACTION_LOANTYPE_UPD,
        ACTION_LOANTYPE_DEL,
        ACTION_DOES_LOANTYPE_EXIST,
        NA_ACTION_Q_LOANTYPE_ALL,

        ACTION_LOAN_ADD,
        ACTION_LOAN_UPD,
        ACTION_LOAN_DEL,
        ACTION_DOES_LOAN_EXIST,
        NA_ACTION_Q_LOAN_ALL,
        NA_ACTION_LOAN_GET,

        ACTION_LOANISSUE_ADD,
        ACTION_LOANISSUE_UPD,
        ACTION_LOANISSUE_DEL,
        ACTION_DOES_LOANISSUE_EXIST,
        NA_ACTION_Q_LOANISSUE_ALL,
        NA_ACTION_LOANISSUE_GET,

        ACTION_LOANSCHEDULE_ADD,
        ACTION_LOANSCHEDULE_UPD,
        ACTION_LOANSCHEDULE_DEL,
        ACTION_DOES_LOANSCHEDULE_EXIST,
        NA_ACTION_Q_LOANSCHEDULE_ALL,
        NA_ACTION_LOANSCHEDULE_GET,

        NA_ACTION_GET_COUNTRY_LIST,
        ACTION_DOES_COUNTRY_EXIST,
        NA_ACTION_GET_SALARY_HEAD_LIST,


        //Master Company Related
        ACTION_MASTERCOMPANY_ADD,
        ACTION_MASTERCOMPANY_DEL,
        ACTION_MASTERCOMPANY_UPD,
        ACTION_DOES_MASTERCOMPANY_EXIST,
        NA_ACTION_GET_MASTERCOMPANY_LIST,
        // ------------------------Report Action Start---------------------
        NA_ACTION_GET_ARREARINFO_DETAIL,
        NA_ACTION_GET_ARREARINFO_SUMMERY,
        NA_ACTION_GET_PAYSLIP,       // Pay Slip related action ID
        NA_ACTION_GET_PAYSHEET,       // Pay Sheet related action ID
        NA_ACTION_GET_SALARYSHEET,       // Salary Sheet related action ID
        NA_ACTION_GET_MONTHLY_RECONCILIATION, // Monthly Reconciliation  related action ID
        NA_ACTION_GET_TAX_CARD,
        NA_ACTION_BATCH_Q_SALARYDETAILS_EMPLOYEE_MONTH,
        NA_ACTION_GET_MONTHLY_TAX_LIST,
        NA_ACTION_GET_PERQUISITE,
        NA_ACTION_GET_SECTION108, 
        NA_ACTION_GET_SECTION108_EXCEL,
        NA_ACTION_GET_LFAREPORT,
        NA_ACTION_GET_FLUCTUATION_RPT,
        NA_ACTION_GET_BANK_DEPOSITE_RPT,
        NA_ACTION_GET_EMPLOYEE_PAID_AMOUNT,
        NA_ACTION_GET_SALARY_STATEMENT_SUMMARY,
        NA_ACTION_GET_SALARY_ANALYSIS_RPT,
        NA_ACTION_GET_PF_ANALYSIS_RPT,     //PF Analysis

        NA_ACTION_GET_BP_VOUCHER_RPT,      //BP Voucher
        // ------------------------Report Action End---------------------
        NA_ACTION_Q_ALL_EMPLOYEE_ARREAR_INFO_SUMMARY,

        //mislbd.Jarif Update Start(10-Apr-2008)
        NA_ACTION_GET_BASIC_SALARY,
        //mislbd.Jarif Update End(10-Apr-2008)

        //mislbd.Jarif Update Start(24-Feb-2009)
        //Configuration_Promotion related
        ACTION_CONFIGURATIONPROMOTION_ADD,
        ACTION_CONFIGURATIONPROMOTION_DEL,
        ACTION_CONFIGURATIONPROMOTION_UPD,
        ACTION_DOES_CONFIGURATIONPROMOTION_EXIST,
        NA_ACTION_GET_CONFIGURATIONPROMOTION_LIST,
        //mislbd.Jarif Update End(24-Feb-2009)

        //mislbd.Jarif Update Start(04-Mar-2009)
        //Spouse related
        ACTION_SPOUSE_ADD,
        ACTION_SPOUSE_DEL,
        ACTION_SPOUSE_UPD,
        ACTION_DOES_SPOUSE_EXIST,
        NA_ACTION_GET_SPOUSE_LIST,

        //Children related
        ACTION_CHILDREN_ADD,
        ACTION_CHILDREN_DEL,
        ACTION_CHILDREN_UPD,
        ACTION_DOES_CHILDREN_EXIST,
        NA_ACTION_GET_CHILDREN_LIST,

        //Education related
        ACTION_EDUCATION_ADD,
        ACTION_EDUCATION_DEL,
        ACTION_EDUCATION_UPD,
        ACTION_DOES_EDUCATION_EXIST,
        NA_ACTION_GET_EDUCATION_LIST,

        //Training related
        ACTION_TRAINING_ADD,
        ACTION_TRAINING_DEL,
        ACTION_TRAINING_UPD,
        ACTION_DOES_TRAINING_EXIST,
        NA_ACTION_GET_TRAINING_LIST,

        //Past Employment related
        ACTION_EMPLOYMENT_ADD,
        ACTION_EMPLOYMENT_DEL,
        ACTION_EMPLOYMENT_UPD,
        ACTION_DOES_EMPLOYMENT_EXIST,
        NA_ACTION_GET_EMPLOYMENT_LIST,
        //mislbd.Jarif Update End(04-Mar-2009)

        //mislbd.Jarif Update Start(01-Apr-2009)
        //Employee History related
        NA_ACTION_GET_EMPHIST_LIST_BYID,
        NA_ACTION_GET_EMPHIST_LIST_ALL,
        //mislbd.Jarif Update End(01-Apr-2009)

        //mislbd.Jarif Update Start(16-Apr-2009)
        NA_ACTION_GET_TRANSFER_INFO_BYID,
        NA_ACTION_GET_TRANSFER_INFO_ALL,
        NA_ACTION_GET_PROMOTION_INFO_BYID,
        NA_ACTION_GET_PROMOTION_INFO_ALL,
        NA_ACTION_GET_JOBHISTORY_BYID,
        NA_ACTION_GET_JOBHISTORY_ALL,

        ACTION_CHALANNO_ADD,
        ACTION_CHALANNO_DEL,
        ACTION_CHALANNO_UPD,
        ACTION_DOES_CHALANNO_EXIST,
        NA_ACTION_GET_CHALANNO,

        NA_ACTION_GET_INCOMEYEAR,

        ACTION_TAXCERTIFICATE_ADD,
        ACTION_TAXCERTIFICATE_DEL,
        ACTION_TAXCERTIFICATE_UPD,
        ACTION_DOES_TAXCERTIFICATE_EXIST,
        NA_ACTION_GET_TAXCERTIFICATE_LIST_ALL,
        NA_ACTION_GET_TAXCERTIFICATE_LIST_BYID,

        NA_ACTION_GET_TAXCERTIFICATE_RPT,

        NA_ACTION_GET_CUMULATIVPF_ALL,
        NA_ACTION_GET_CUMULATIVPF_BYID,
        NA_ACTION_GET_CUMULATIVPF_BYDATE,
        //mislbd.Jarif Update End(2-May-2009)

        //mislbd.Jarif Update Start(11-October-2009)
        ACTION_GRATUITYPROVISION_CREATE,
        ACTION_GRATUITYPROVISION_UPD,
        ACTION_GRATUITYPROVISION_DEL,
        NA_ACTION_GET_GRATUITYPROVISION_LIST_LAST_MONTH,
        NA_ACTION_GET_GRATUITYPROVISION_LIST_DATE_RANGE,
        NA_ACTION_GET_PROVISIONEDGRATUITY_ANALYSIS_RPT,
        NA_ACTION_GET_ACCUMULATEDGRATUITY_ANALYSIS_RPT,
        //mislbd.Jarif Update End(11-October-2009)
        //mislbd.Jarif Update Start(07-March-2010)
        NA_ACTION_GET_LEA_STATEMENT,
        //mislbd.Jarif Update End(07-March-2010)

        //mislbd.Jarif Update Start(08-Oct-2010)
        NA_ACTION_GET_TAX_CARD_WITH_CERTIFICATE,
        NA_ACTION_GET_CHALANLIST_BY_FISCALYEAR,

        //mislbd.Jarif Update Start(28-Nov-2010)
        ACTION_KPI_ADD,
        ACTION_KPI_UPD,
        ACTION_KPI_DEL,
        ACTION_DOES_KPI_EXIST,
        ACTION_ITEM_ADD,
        NA_ACTION_GET_KPI,
        NA_ACTION_GET_KPI_LIST,
        NA_ACTION_GET_KPI_ITEM,
        ACTION_KPI_GROUP_ADD,
        ACTION_KPI_GROUP_UPD,
        ACTION_KPI_GROUP_DEL,
        NA_ACTION_GET_KPI_LIST_BY_KPI_GROUP,
        NA_ACTION_GET_KPI_GROUP_BY_DEPT,
        NA_ACTION_GET_KPI_LIST_BY_EMP,
        ACTION_EMP_SCORING_ADD,
        ACTION_EMP_SCORING_DEL,
        NA_ACTION_GET_EMP_SCORING_LIST,
        NA_ACTION_GET_ALL_EMP_SCORING_LIST,
        NA_ACTION_GET_SCORED_PERIOD_LIST,
        NA_ACTION_GET_DETAIL_SCORED_LIST_RPT,
        NA_ACTION_GET_SCORED_LIST_RPT,
        NA_ACTION_GET_SCORED_LIST_CHT_RPT,
        ACTION_TARGET_ADD,
        ACTION_TARGET_DEL,
        NA_ACTION_GET_TARGET,
        NA_ACTION_GET_EMP_WISE_TARGET,
        NA_ACTION_GET_EMP_WISE_TARGET_ADD,
        #region For Start LMS

        ACTION_LMS_ACTIVITIES_UPD,
        NA_ACTION_LMS_GET_ACTIVITY_LIST_ALL,
        ACTION_LMS_LEAVETYPE_CREATE,
        ACTION_LMS_LEAVETYPE_UPD,
        ACTION_LMS_LEAVETYPE_DEL,
        NA_ACTION_LMS_GET_LEAVETYPE_LIST,
        NA_ACTION_LMS_GET_LEAVETYPE_LIST_ALL,
        ACTION_LMS_DOES_LEAVETYPE_EXIST,
        ACTION_LMS_WEEKEND_UPD,
        NA_ACTION_LMS_GET_WEEKEND_LIST_ALL,
        ACTION_LMS_HOLIDAY_ADD,
        ACTION_LMS_HOLIDAY_UPD,
        ACTION_LMS_HOLIDAY_DEL,
        NA_ACTION_LMS_GET_HOLIDAY_LIST,
        NA_ACTION_LMS_GET_HOLIDAY_LIST_ALL,
        ACTION_LMS_LEAVE_CATEGORY_ADD,
        ACTION_LMS_LEAVE_CATEGORY_UPD,
        ACTION_LMS_LEAVE_CATEGORY_DEL,
        NA_ACTION_LMS_GET_LEAVE_CATEGORY_LIST,
        NA_ACTION_LMS_GET_LEAVE_CATEGORY_LIST_ALL,
        ACTION_LMS_DOES_LEAVE_CATEGORY_EXIST,
        ACTION_LMS_LEAVE_CATEGORYCONFIG_ADD,
        ACTION_LMS_LEAVE_CATEGORYCONFIG_DEL,
        NA_ACTION_LMS_GET_LEAVE_CATEGORYCONFIG_LIST,
        ACTION_LMS_LEAVE_CATEGORY_ASSIGN,
        NA_ACTION_LMS_GET_LCGRADE_LIST,
        NA_ACTION_LMS_GET_LCGRADE_EMPLOYEE_TYPES,
        ACTION_LMS_ELQDETAILS_ADD,
        ACTION_LMS_ELQDETAILS_UPD,
        NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST,
        NA_ACTION_LMS_GET_EMPLOYEE_HISTORY,
        ACTION_LMS_ELQ_ADD,
        ACTION_LMS_LEAVE_ADD,
        NA_ACTION_LMS_GET_LEAVE_LIST,
        NA_ACTION_LMS_GET_DAYLENGTH_LIST,
        NA_ACTION_LMS_GET_LEAVE_SUMMERY,
        ACTION_LMS_LEAVE_APPROVAL_ADD,
        NA_ACTION_LMS_GET_USER_BY_EMPCODE,
        NA_ACTION_LMS_GET_USER_FOR_DELEGATE,
        NA_ACTION_LMS_GET_LEAVE_INFO_FOR_REPORT,
        ACTION_LMS_EMAIL_PASSWORD_CHANGE,
        NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST_BY_LOGINID,


        #endregion

        #region For Viewing permission of user interface...








        #endregion

        #region Recruitment...
        //JobSpecification
        ACTION_JOB_SPECIFICATION_CREATE,
        ACTION_JOB_SPECIFICATION_UPD,
        ACTION_JOB_SPECIFICATION_DEL,
        NA_ACTION_JOB_SPECIFICATION_LIST,
        NA_ACTION_JOB_SPECIFICATION_LIST_ALL,
        ACTION_JOB_SPECIFICATION_EXIST,


        //JobTitle
        ACTION_JOB_TITLE_CREATE,
        ACTION_JOB_TITLE_UPD,
        ACTION_JOB_TITLE_DEL,
        NA_ACTION_JOB_TITLE_LIST,
        NA_ACTION_JOB_TITLE_LIST_ALL,
        ACTION_JOB_TITLE_EXIST,


        //JobVacancy
        ACTION_JOB_VACANCY_CREATE,
        ACTION_JOB_VACANCY_UPD,
        ACTION_JOB_VACANCY_DEL,
        NA_ACTION_JOB_VACANCY_LIST,
        NA_ACTION_JOB_VACANCY_LIST_ALL,
        ACTION_JOB_VACANCY_EXIST,


        //JobApplication
        ACTION_JOB_APPLICATION_CREATE,
        NA_ACTION_JOB_APPLICATION_LIST_ALL,
        NA_ACTION_GET_JOB_APP_BY_APPID,


        //City
        NA_ACTION_GET_CITY_LIST,
        ACTION_DOES_CITY_EXIST,

        //JobHistory
        ACTION_JOB_HISTORY_CREATE,
        NA_ACTION_GET_JOB_HISTORY_BY_LASTHISTORY,
        NA_ACTION_GET_JOB_HISTORY_BY_APPID,
        // Update by Asif, 11-mar-12
        ACTION_JOB_HISTORY_UPD,
        // Update end
        #endregion

        #region Requisition...
        //RequisitionType
        ACTION_REQUISITION_TYPE_CREATE,
        ACTION_REQUISITION_TYPE_UPD,
        ACTION_REQUISITION_TYPE_DEL,
        NA_ACTION_GET_REQUISITION_TYPE_LIST,
        NA_ACTION_GET_REQUISITION_TYPE_LIST_ALL,
        ACTION_DOES_REQUISITION_TYPE_EXIST,

        //Requisition Input Field
        ACTION_REQUISITION_INPUT_FIELD_CREATE,
        ACTION_REQUISITION_INPUT_FIELD_UPD,
        NA_ACTION_GET_REQUISITION_INPUT_FIELD_LIST,
        NA_ACTION_GET_REQUISITION_INPUT_FIELD_LIST_ALL,
        NA_ACTION_GET_REQ_INPUTFIELD_LIST_BY_RTYPEID,
        ACTION_DOES_REQUISITION_INPUT_FIELD_EXIST,
        ACTION_REQUISITION_INPUTFIELD_DEL,
        NA_ACTION_GET_REQ_INPUTFIELD_LIST_BY_RTYPEID_AND_CURRENT_USER,

        //RIFConfig
        ACTION_RIFCONFIG_CREATE,
        ACTION_RIFCONFIG_DELETE,

        //Requisition
        ACTION_REQUISITION_CREATE,

        //RequisitionHistory Update by Asif, 17-mar-12
        NA_ACTION_GET_REQUISITION_HISTORY_BY_LASTHISTORY,
        // Update end
        #endregion

        #region LFA
        ACTION_LFA_REQUEST_CREATE,
        NA_ACTION_GET_LEAVE_INFO_BY_EMPLOYEEID,
        NA_ACTION_GET_LFA_SUMMARY,
        #endregion

        ACTION_EMPLOYEE_INCREMENT,
        ACTION_LFA_REQUEST_APPROVAL_CREATE,
        NA_ACTION_Q_TAXHISTORY_BY_SALARYMONTH,
        ACTION_INCOMETAX_UPD,
        NA_ACTION_GET_EMP_JOB_HISTORY_PARKING,
        NA_ACTION_GET_EMPLOYEE_LFA,
        ACTION_EMP_JOB_HISTORY_PARKING_CREATE,
        NA_ACTION_GET_SERIALNO_LIST,
        ACTION_EMP_JOB_HISTORY_PARKING_APPROVE,
        ACTION_EMP_JOB_HISTORY_PARKING_DEL,
        NA_ACTION_GET_SALARY_SALARYDETAIL_LIST,
        NA_ACTION_GET_PAYSHEET_DYNAMIC,
        NA_ACTION_GET_SALARY_BREAK_UP,
        NA_ACTION_GET_EMPLOYEE_ARREAR_LIST,
        NA_ACTION_GET_BASIC_SALARY_ANALYSIS_RPT,

        #region Remember Me
        NA_ACTION_REMEMBERME_ADD,
        NA_ACTION_REMEMBERME_DEL,
        NA_ACTION_GET_USER_BY_IPADDRESS,
        NA_ACTION_GET_USER_BY_PCNAME,
        NA_ACTION_SEC_CONCURRENCE_LOGIN_SET_RM,
        #endregion

        NA_ACTION_GET_EMPLOYEE_LFA_BY_EMPTYPESITE,
        NA_ACTION_GET_USERROLE,
        ACTION_DELEGATE_CREATE,
        NA_ACTION_GET_USER_BY_USERID,
        NA_ACTION_LMS_GET_LEAVE_QUOTA_HISTORY,
        NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST_NAME,
        NA_ACTION_LMS_GET_LEAVE_DATE_LIST,
        NA_ACTION_GET_LFA_REQUEST_LIST,
        NA_ACTION_GET_LFA_REQUEST_LIST_BY_YEAR,
        NA_ACTION_GET_EMPLOYEE_LIST_BY_DEPT,
        ACTION_MAIL_USERS_CREATE,
        NA_ACTION_GET_EMPLOYEE_LIST_FROM_MAILUSER,
        NA_ACTION_GET_RULECONFIGURATION_LIST_ALL,
        NA_ACTION_GET_MAIL_DEPARTMENT_LIST_ALL,
        ACTION_MAIL_RULE_CONFIGURATION_CREATE,
        ACTION_MAIL_RULE_CONFIGURATION_DEL,
        NA_ACTION_GET_MAIL_SEND_DATE_LIST_ALL,
        ACTION_MAIL_SEND_DATE_CREATE,
        ACTION_MAIL_SEND_DATE_DEL,
        NA_ACTION_GET_MAIL_BODY,
        ACTION_MAIL_BODY_CREATE,
        NA_ACTION_GET_MAILUSER_ALL,
        NA_ACTION_GET_EMPLOYEE_ADVANCE_LIST_ALL,
        NA_ACTION_GET_EMPLOYEE_ADVANCE_LIST,
        NA_ACTION_GET_PAYSLIP_WithPF,
        NA_ACTION_GET_SUPERVISOR_LIST,

        #region Shakir 29/08/2012.............................

        //Requisition        

        NA_ACTION_GET_REQUISITION_SUMMERY,
        NA_ACTION_REQ_GET_EMPLOYEE_HISTORY,
        NA_ACTION_REQ_GET_ACTIVITY_LIST_ALL,


        //Requisition Category
        ACTION_REQUISITION_CATEGORY_CREATE,
        ACTION_REQUISITION_CATEGORY_UPD,
        NA_ACTION_GET_CATEGORY_LIST,
        ACTION_CATEGORY_DELETE,
        NA_ACTION_GET_REQUISITION_CATEGORY_LIST_ALL,
        NA_ACTION_GET_REQUISITION_TYPES_LIST_ALL,
        NA_ACTION_GET_REQ_TYPE_LIST_BY_CATEGORYEID,
        NA_ACTION_GET_REQ_FIELD_LIST_BY_TYPEID,


        //Requisition Category Config
        ACTION_REQ_CATEGORY_CONFIG_CREATE,
        ACTION_REQ_CATEGORY_CONFIG_DELETE,
        NA_ActionGetFieldBy_TypeCodeCategoryID,
        NA_ACTION_REQUISITION_GET_LCGRADE_LIST,
        ACTION_REQUISITION_CATEGORY_ASSIGN,
        ACTION_FIELD_LIMIT_CONFIG,
        NA_ACTION_GET_REQ_FIELD_VALUE,
        NA_ACTION_GET_REQ_TYPE_LIST_BY_LOGINID,

        //Requisition Approve
        NA_ACTION_REQ_GET_REQUISITION_SUMMERY,
        NA_ACTION_REQ_GETTING_EMPLOYEE_HISTORY,
        NA_ACTION_GET_REQUISITION_ACTIVITY_LIST_ALL,
        NA_ACTION_REQUISITION_LIST,
        NA_ACTION_GET_REQ_ACTIVITY_LIST_ALL,
        NA_ACTION_GET_REQUISITION_LIST_BY_ACTIVITYID,
        NA_ACTION_GET_REQ_LIST_BY_ACTIVITYID_CODE,
        NA_ACTION_GET_EMP_CODE_BY_LOGINID,
        ACTION_REQUISITION_APPROVATION,
        NA_ACTION_GET_REQ_FIELD_LIST_BY_REQUESTID,
        NA_ACTION_GET_FIELD_LIST_BY_RTYPEID_LOGINID,
        NA_ACTION_GET_REQ_LIMIT_FIELD_LIST_BY_TYPEID,
        #endregion

        ACTION_SUPERVISOR_UPD,
        ACTION_SHIFT_EMPLOYEE_UPD,
        ACTION_SITE_CCMAIL_USER_UPD,

        NA_ACTION_GET_REQ_LIMIT_FIELD_LIST_BY_TYPEID2,
        NA_ACTION_GET_REQ_FIELD_LIST_LIMITMARKABLE,
        NA_ACTION_GET_REQUISITION_HISTORY,

        NA_ACTION_LMS_GET_LEAVETYPE_LIST_BY_LOGONID,
        NA_ACTION_LMS_GET_LEAVETYPE_LIST_BY_EMPCODE,
        NA_ACTION_GET_EMPLOYEE_LIST_BY_DESIGNATION,

        //Reference Releted
        ACTION_REFERENCE_ADD,
        ACTION_DOES_REFERENCE_EXIST,
        NA_ACTION_GET_REFERENCE_LIST,
        ACTION_REFERENCE_UPD,
        ACTION_REFERENCE_DEL,
        //ACTION_CHILDREN_DEL
        //Define Shifting
        ACTION_SHIFTING_ADD,
        ACTION_SHIFTING_UPD,
        ACTION_SHIFTING_DEL,

        NA_ACTION_GET_SHIFTING_LIST,
        NA_ACTION_GET_SHIFTING_LIST_ALL,
        NA_ACTION_GET_SHIFTING_LIST_ALL_DROPDOWN,
        NA_ACTION_GET_SHIFTING_LIST_ALL_GRID,
        NA_ACTION_GET_SHIFTING_LIST_GRID,
        //Shifting Plan
        ACTION_SHIFTINPLAN_ADD,
        ACTION_SHIFTINGPLAN_UPD,
        ACTION_SHIFTINGPLAN_DEL,

        NA_ACTION_GET_SHIFTINGPLAN_LIST,
        NA_ACTION_GET_SHIFTINGPLAN_LIST_ALL,
        NA_ACTION_GET_EMPLOYEELISTFOR_SHIFTINGPLAN,

        //EMPLOYEE ATT
        ACTION_EMPLOYEE_ATTENDANCE_ADD,
        ACTION_EMPLOYEE_ATTENDANCE_UPD,
        NA_ACTION_GET_HOLIDAY_SPECIFIC_EMPLOYEE,
        NA_ACTION_GET_ANNUAL_HOLIDAY,
        NA_ACTION_GET_PERSONAL_LEAVE,
        NA_ACTION_GET_SPECIFIC_EMPLOYEE,
        NA_ACTION_GET_EMPLIST_SPECIFIC_DATE,

        ACTION_EXCEPTIONAL_LOGIN_UPD,
        ACTION_EXCEPTIONAL_LOGIN_ADD,
        NA_ACTION_GET_SHIFTING_PLAN,
        NA_ACTION_GET_SPECIFIC_EMPLOYEE_FOR_FIRST_DAY,
        NA_ACTION_GET_EFFECTIVE_DATE,
        NA_ACTION_GET_EMPLOYEELIST_FOR_FILTER_SHIFTINGPLAN,
        NA_ACTION_GET_EMPLOYEELIST_FOR_FILTER_SHIFTINGPLAN_ONLY_SHIFT,
        NA_ACTION_GET_EMPLOYEELIST_FOR_DAILY_ATTENDANCE,
        NA_ACTION_GET_DAILY_ATTENDANCE_REPORT,
        NA_ACTION_GET_EMPLOYEELIST_FOR_DAILY_ATTENDANCE_ATTREGISTER,
        NA_ACTION_GET_EMPLOYEELIST_FOR_DAILY_ATTENDANCE_ATTREGISTER_GRID,
        NA_ACTION_GET_EMPLOYEE_LIST_EXCEPTIONAL_LOGIN,
        NA_ACTION_GET_SHIFTING_PLAN_CODE,
        NA_ACTION_GET_SHIFTING_PLAN_SPECIFIC_EMPLYEE,
        NA_ACTION_GET_SPECIFIC_EMPLOYEE_ALL,




        NA_ACTION_GET_EMPHEADINGLIST_LIST,
        NA_ACTION_GET_EMPLOYEE_DATA,
        NA_ACTION_GET_DAILY_ATTENDANCE_REPORT_ADMIN,
        NA_ACTION_GET_LEAVE,
        ACTION_DEALLOCATE,
        ACTION_DEALLOCATE_EMPLOYEE,
        NA_ACTION_GET_MAXELQID,
        NA_ACTION_GET_LEAVEHISTORY,
        NA_ACTION_GET_EMPLOYEE_LIST_BY_DESIGNATION2,
        NA_ACTION_GET_PAYSLIP_WithPF_for_Mail,
        NA_ACTION_GET_PAYSLIP_for_mail,
        ACTION_PAYSLIP_MAIL_NOTIFICATION_UPD,
        NA_ACTION_GET_EMPLOYEE_PAY_SLIP_NOTIFY,
        NA_ACTION_GET_LFA_SUMMARY_REPORT,
        ACTION_DISCIPLINARYTYPE_ADD,
        ACTION_DISCIPLINARYTYPE_UPD,

        ACTION_DISCIPLINARY_ADD,
        ACTION_DISCIPLINARY_UPD,

        NA_ACTION_GET_LOAN_INFO_FOR_FINAL_SETTLEMENT,
        NA_ACTION_GET_COMPETENCY_LIST,
        NA_ACTION_GET_EMPLOYEE_LIST_BY_COMPANY_CODE,
        NA_ACTION_GET_PAYSLIP_LUMPL,
        NA_ACTION_USER_LIST_BY_COMPANY_CODE,

        ACTION_COMPETENCY_ADD,
        ACTION_ATTACHMENT_ADD,
        NA_ACTION_GET_ATTACHMENT_LIST,
        NA_ACTION_GET_DISCIPLINARY_RECORDS,
        NA_ACTION_GET_REFERENCE_RECORDS,
        NA_ACTION_GET_COMPETENCY_RECORDS,
        NA_ACTION_GET_INCREMENT_INFO_BYID,
        NA_ACTION_GET_INCREMENT_INFO_ALL,
        NA_ACTION_GET_REQUISITION_HEALTH_SAFTY,
        NA_ACTION_GET_PAYSLIP_LUMPL_WithPF,
        ACTION_EMP_SALARY_PARKING_CREATE,
        NA_ACTION_GET_SALARY_PARKING_DATA,
        NA_ACTION_GET_SALARY_SERIALNO_LIST,
        ACTION_SALARY_PARKING_APPROVE,
        ACTION_SALARY_PARKING_DEL,
        NA_ACTION_GET_LFA_SPECIFIC_USER,

        NA_ACTION_GET_DIVISION_LIST,
        NA_ACTION_GET_DISTRICT_LIST,
        NA_ACTION_GET_THANA_LIST,

        NA_ACTION_GET_DEGREE_LIST,
        ACTION_GUARANTOR_ADD,
        ACTION_GUARANTOR_UPD,
        NA_ACTION_GET_GUARANTOR_LIST,
        ACTION_GUARANTOR_DEL,
        ACTION_DISCIPLINARY_DEL,
        NA_ACTION_GET_EDUCATION_LIST_TO_UPDATE,
        NA_ACTION_GET_ACTIVE_EMPLOYEE_LIST,
        NA_ACTION_GET_EMP_JOB_HISTORY_INCREMENT,
        NA_ACTION_GET_EMP_LIST,

        ACTION_ASSET_TYPE_CREATE,
        ACTION_ASSET_TYPE_UPD,
        ACTION_ASSET_TYPE_DEL,
        NA_ACTION_GET_ASSET_TYPE_LIST,
        NA_ACTION_GET_ASSET_TYPE_LIST_ALL,
        ACTION_DOES_ASSET_TYPE_EXIST,

        ACTION_ASSET_INPUT_FIELD_CREATE,
        ACTION_ASSET_INPUT_FIELD_UPD,
        ACTION_DOES_ASSET_INPUT_FIELD_EXIST,
        NA_ACTION_GET_ASSET_INPUT_FIELD_LIST,
        NA_ACTION_GET_ASSET_INPUT_FIELD_LIST_ALL,
        NA_ACTION_GET_ASS_INPUTFIELD_LIST_BY_ATYPEID,
        ACTION_ASSET_INPUTFIELD_DEL,

        ACTION_ASSET_CATEGORY_CREATE,
        ACTION_ASSET_CATEGORY_UPD,
        NA_ACTION_GET_ASSET_CATEGORY_LIST,
        ACTION_ASSET_CATEGORY_DELETE,
        NA_ACTION_GET_ASSET_CATEGORY_LIST_ALL,
        NA_ACTION_GET_ASSET_TYPES_LIST_ALL,

        ACTION_AIFCONFIG_CREATE,
        ACTION_AIFCONFIG_DELETE,
        ACTION_ASS_CATEGORY_CONFIG_CREATE,
        NA_ACTION_GET_ASS_TYPE_LIST_BY_CATEGORYEID,
        ACTION_ASS_CATEGORY_CONFIG_DELETE,
        NA_ACTION_GET_ASS_FIELD_LIST_BY_TYPEID,
        NA_ACTION_GET_ASS_FIELD_VALUE,
        ACTION_ASSET_FIELD_LIMIT_CONFIG,
        NA_ACTION_ASSET_GET_LCGRADE_LIST,
        ACTION_ASSET_CATEGORY_ASSIGN,
        NA_ACTION_GET_ASS_FIELD_LIST_BY_AEQUESTID,
        NA_ACTION_GET_ASSET_TYPE_USING_LOGINID,
        ACTION_ASSET_APPLY,
        NA_ACTION_GET_FIELD_LIST_BY_ATYPEID_LOGINID,
        NA_ACTION_ASSET_APPLIED_LIST,
        NA_ACTION_GET_ASSET_ACTIVITY_LIST_ALL,
        ACTION_ASSET_APPLY_APPROVATION,
        //End Asset Related Action

        NA_ACTION_GET_ACTIVE_EMP_LIST_DEPDID,
        NA_ACTION_GET_LEAVE_SUMMERY_BY_EMPID,
        NA_ACTION_GET_PEN_LEAVE_SUMMERY_BY_EMPID,
        NA_ACTION_GET_PEN_LEAVE_SUMMERY_BY_EMPCODE,
        NA_ACTION_GET_LEAVE_SUMMERY_BY_LRID,
        NA_ACTION_GET_LEAVE_SUMMERY_BY_EMPCODE,
        NA_ACTION_LMS_GET_EMPLOYEE_HIST_BY_EMPID,
        NA_ACTION_LMS_GET_EMPLOYEE_HIST_BY_EMPCODE,
        NA_ACTION_GET_LEAVE_HISTORY,
        NA_ACTION_GET_EMPLOYEE_DETAILS_FOR_LFA,
        ACTION_LFA_ASSIGN_CREATE,
        NA_ACTION_GET_CONTINUE_EMPLOYEE_LIST,
        NA_ACTION_GET_LEAVE_TENURE_HISTORY,
        NA_ACTION_GET_LEAVE_REQUEST_SELF,
        NA_ACTION_GET_LEAVE_REQUEST_SELF_EMPCODE,
        NA_ACTION_GET_LEAVE_REQUEST_SELF_SUBORDINATE,
        NA_ACTION_GET_LR_SELF_SUBORDINATE_EMPCODE,
        NA_ACTION_LMS_GET_LEAVE_LIST_BY_EMPID_CFYEAR,
        NA_ACTION_LMS_GET_LEAVE_QUOTA_BY_EMPID_CFYEAR,
        NA_ACTION_GET_LEAVE_LIST_BY_LRID,
        ACTION_NEWEMPLOYEE_INFO_ADD,

        NA_ACTION_GET_ACTIVE_USER_LIST,

        // Attendance from Seperate SQL Database :: WALI :: 28-Jan-2014
        ACTION_SQL_DB_CONNECTION_GET,
        NA_ACTION_GET_DAILY_ATTENDANCE_SQL_RPT,
        NA_ACTION_GET_ATTENDANCE_REGISTRY_SQL_RPT,
        // Attendance END :: WALI 

        // Action for Attendance Weekends
        NA_ACTION_GET_ATT_WEEKEND_LIST_ALL,
        ACTION_ATT_WEEKEND_UPD,
        NA_ACTION_LMS_GET_EMPLOYEE_FOR_LPS,

        NA_ACTION_GET_UNIVERSITY_LIST,
        NA_ACTION_LMS_GET_LPR_LIST,

        ACTION_LEAVE_PLAN_RULE_ADD,
        ACTION_LEAVE_PLAN_RULE_UPD,
        ACTION_LEAVE_PLAN_RULE_DEL,

        ACTION_CREATE_LEAVE_PLAN_PARKING,
        NA_ACTION_LMS_GET_EMP_FOR_LPS_INDIVIDUAL,
        NA_ACTION_PLPH_USER_GET,
        NA_ACTION_PLV_USER_GET,
        NA_ACTION_LMS_GET_EMP_FOR_LPS_APPROVAL,

        // Action for LIABILITY
        ACTION_LIABILITY_ADD,
        NA_ACTION_GET_LIABILITY_LIST,
        ACTION_LIABILITY_UPD,
        ACTION_LIABILITY_DEL,
        // LIABILITY Ends

        NA_ACTION_GET_DUE_INCREMENT_EMPLOYEE_LIST,   // RAFAT :: 19-Feb-14

        // Rafat :: Scale of Pay
        ACTION_SCALE_OF_PAY_ADD,
        NA_ACTION_Q_GRADE_SLAB_ALL,
        NA_ACTION_GET_GRADEWISE_SLAB_LIST,
        ACTION_GRADE_WISE_SLAB_DEL,
        ACTION_SCALE_OF_PAY_UPD,
        // End :: Scale of Pay

        NA_ACTION_GET_VERIFIER_LIST,
        NA_ACTION_GET_PAY_FIXATION_INFO,
        ACTION_APPROVE_PAY_FIXATION,
        NA_ACTION_GET_PAY_FIXATION,
        ACTION_CREATE_PAY_FIXATION_PARKING,
        ACTION_REJECT_PAY_FIXATION,

        NA_ACTION_GET_LOCALPOSTING_DATA,
        ACTION_CREATE_LOCALPOSTING,

        // Profit Posting
        ACTION_CREATE_PROFIT_POSTING_PARKING,
        ACTION_APPROVE_PROFIT_POSTING,
        NA_ACTION_GET_ALLOWDEDUCT_LIST_ADPARAM,
        NA_ACTION_GET_EMP_ALLOWANCE_BY_PARAM,
        NA_ACTION_GET_PROFIT,
        NA_ACTION_GET_PENDING_PROFIT_POSTING,

        NA_ACTION_GET_HEADWISE_BALANCE_STATUS,
        NA_ACTION_GET_HEADWISE_BALANCE_WITH_SEARCH_STATUS,

        // Competency Profile :: WALI       21-Apr-2014
        ACTION_COMPETENCY_TYPE_ADD,
        ACTION_COMPETENCY_TYPE_DELETE,
        ACTION_COMPETENCY_TYPE_UPDATE,
        NA_ACTION_GET_COMPETENCY_TYPE,
        // Action to add Competency has already been ADDED.
        ACTION_COMPETENCY_PROFILE_DEL,
        NA_ACTION_GET_COMPETENCY_DETAILS,
        ACTION_COMPETENCY_PROFILE_UPD,

        // Employee History Related Actions :: WALI :: 27-Apr-2014
        ACTION_EDUCATIONBOARD_CREATE,
        ACTION_EDUCATIONBOARD_UPDATE,
        ACTION_GROUP_CREATE,
        ACTION_GROUP_UPDATE,
        ACTION_MAJOR_SUBJECT_CREATE,
        NA_ACTION_GET_MAJOR_SUBJECT_LIST,
        ACTION_MAJOR_SUBJECT_UPDATE,

        ACTION_LMS_ELQDETAILS_UPD_DAYS,
        NA_ACTION_GET_EMPLOYEE_FOR_lEAVE_SANCTION_ADVICE,

        NA_ACTION_GET_APPROVED_PAY_FIXATION_INFO,
        NA_ACTION_GET_ALLOWANCE_DATA_FOR_PAY_FIXATION,

        NA_ACTION_GET_SALARY_CERTIFICATE,
        NA_ACTION_GET_LOANISSUE,
        NA_ACTION_GET_PF_STATEMENT_RPT,
        //Shakir Hossain :: 22.05.2014
        NA_ACTION_GET_SOCIAL_SECUTITY_BENEVOLENT_FUND,

        // Shakir Hossain :: 28.05.2014
        ACTION_ASINFO_ADD,
        ACTION_ASINFO_UPDATE,
        ACTION_ASINFO_DELETE,
        NA_ACTION_ASINFO_GET,

        NA_ACTION_LMS_GET_LEAVE_QUOTA_TO_MODIFY,    // WALI :: 03-Jun-2014
        NA_ACTION_GET_LMS_FISCAL_YEAR,    // WALI :: 03-Jun-2014


        #region Training Related Action [10-Jun-2014]

        ACTION_DOES_CODE_PREFIX_EXIST,
        ACTION_CODE_PREFIX_CREATE,
        NA_ACTION_GET_CODE_PREFIX_LIST,
        ACTION_CODE_PREFIX_DELETE,
        ACTION_CODE_PREFIX_UPDATE,

        ACTION_DOES_EXIST_TRAINING,
        ACTION_TRAINING_CREATE,
        NA_ACTION_GET_TRAINING_LIST_ALL,
        ACTION_TRAINING_DELETE,
        ACTION_TRAINING_UPDATE,

        ACTION_TRAINING_CATEGORY_CREATE,
        ACTION_TRAINING_CATEGORY_UPDATE,
        ACTION_DOES_TRAINING_CATEGORY_EXIST,
        NA_ACTION_GET_TRAINING_CATEGORY_LIST,
        ACTION_TRAINING_CATEGORY_DELETE,

        ACTION_TRAINING_TYPE_CREATE,
        NA_ACTION_GET_TRAINING_TYPE_LIST,
        ACTION_TRAINING_TYPE_DELETE,

        NA_ACTION_GET_TRAINING_FOR_NEED_ASSESSMENT,
        NA_ACTION_GET_EMPLOYMENT_LIST_BY_ID,
        ACTION_EXTERNAL_TRAINER_CREATE,
        ACTION_DOES_EXTERNAL_TRAINER_EXIST,
        ACTION_EXTERNAL_TRAINER_UPDATE,
        NA_ACTION_GET_EXTERNAL_TRAINER_LIST,
        ACTION_EXTERNAL_TRAINER_DELETE,
        NA_ACTION_GET_TRAINING_CATEGORY_LIST_BY_All_TYPEID,
        ACTION_DEPARTMENTAL_MATRIX_CREATE,
        NA_ACTION_GET_TRAINING_MATRIX_LIST,
        NA_ACTION_GET_TRAINING_MATRIX_DETAILS,
        ACTION_DEPARTMENTAL_MATRIX_DELETE,
        ACTION_MISCELLANEOUS_DATA_CREATE,
        NA_ACTION_GET_MISCELLANEOUS_DATA,
        ACTION_TRAINING_ACTIVITIES_UPD,
        NA_ACTION_TR_GET_ACTIVITY_LIST_ALL,
        ACTION_WEIGHTAGE_SETTINGS_FOR_MATRIX,
        NA_ACTION_GET_APPROVALPATH_LIST_BYID,
        ACTION_APPROVALPATH_ADD,
        ACTION_APPROVALPATH_DEL,
        NA_ACTION_GET_TR_CATEGORY_LIST__OF_MATRIX,
        ACTION_MATRIX_MAPPING_WITH_DEPARTMENT,
        NA_ACTION_GET_TRAINING_LIST_BY_ANY_ID,
        ACTION_VERSION_GENERATE,
        NA_ACTION_GET_TRAINING_VERSION_LIST,
        NA_ACTION_BYPASS_DEPARTMENT_GET,
        NA_ACTION_GET_DEPARTMENTLIST_BY_MODULEID,
        ACTION_BYPASS_CONFIGURATION_ADD,
        NA_ACTION_GET_TR_MATRIX_DETAILS_BY_DEPARTMENTID,
        ACTION_TTRAINING_NEED_ASSESSMENT_CREATE,
        NA_ACTION_GET_TR_ASSESSMENT_DETAILS_BY_DEPTID,
        NA_ACTION_GET_CALENDER_LIST_OF_TRAINING,
        ACTION_TTRAINING_NEED_ASSESSMENT_APPROVAL,
        NA_ACTION_GET_TRAINEE_LIST_TO_MAKE_CALENDAR,
        NA_ACTION_GET_TRAINER_LIST_FROM_EMPLOYEE,
        ACTION_CALENDAR_CREATE,
        NA_ACTION_GET_PENDING_TRAINING_FOR_CALENDAR,
        NA_ACTION_GET_YEAR_MONTH_TR_GROUP_OF_CALENDER,
        ACTION_CALENDAR_UPDATE,
        NA_ACTION_GET_MAIL_USER_LIST,//*************
        NA_ACTION_GET_LOGIN_USER_DATA,//************
        NA_ACTION_GET_TRAINING_LIST_TO_MAKE_SHEDULE,
        ACTION_TRAINING_CALENDAR_SHEDULE_UPD,
        ACTION_TRAINING_CONFIRMATION_UPDATE,
        NA_ACTION_GET_TRAINING_LIST_WITHIN_DAY,
        ACTION_TRAINING_ATTENDANCE_CREATE,
        NA_ACTION_GET_TR_NOTICEBOARD_GRID,
        NA_ACTION_GET_TR_GENERALUSER_ID,
        NA_ACTION_GET_TR_NOTICEBOARD_GRID_FOR_GENERAL_USER,
        NA_ACTION_GET_TR_TRAINING_PARTICIPANTS_GRID,
        ACTION_TRAINING_EVALUATION_CREATE,
        ACTION_TRAINING_ASSESSMENT_APPROVAL,
        NA_ACTION_GET_LAST_VERSION_TRAINING_LIST,

        NA_ACTION_GET_TR_AREA_LIST,
        ACTION_TR_AREA_CREATE,
        ACTION_TR_AREA_UPDATE,
        ACTION_TR_AREA_DELETE,
        ACTION_TR_DEGREE_CREATE,
        NA_ACTION_GET_TR_DEGREE_LIST,
        ACTION_TR_DEGREE_UPDATE,
        ACTION_TR_DEGREE_DELETE,
        ACTION_TR_DISEASE_CREATE,
        NA_ACTION_GET_TR_DISEASE_LIST,
        ACTION_TR_DISEASE_UPDATE,
        ACTION_TR_DISEASE_DELETE,
        //ACTION_MAJOR_SUBJECT_CREATE,
        //NA_ACTION_GET_MAJOR_SUBJECT_LIST,
        //ACTION_MAJOR_SUBJECT_UPDATE,

        //ACTION_GROUP_CREATE,
        //ACTION_GROUP_UPDATE,
        //ACTION_EDUCATIONBOARD_CREATE,
        //ACTION_EDUCATIONBOARD_UPDATE,

        NA_ACTION_GET_TRAINING_EMP_HIST_RPT,
        NA_ACTION_TR_GET_JOB_DESCRIPTION_RPT,
        NA_ACTION_GET_SOP_CATEGORY_LIST,
        NA_ACTION_TR_GET_SOP_TRAINING_NEED_ANALYSIS_RPT,
        ACTION_INSTANT_TRAINING_CALENDAR_CREATE,
        NA_ACTION_GET_INSTANT_TRAINING_LIST_ALL,
        NA_ACTION_GET_INSTANT_TRAINING_HISTORY,
        ACTION_INSTANT_TRAINING_CALENDAR_UPDATE,
        ACTION_INSTANT_TRAINEE_DELETE,
        NA_ACTION_TR_GET_INDIVIDUAL_TRAINING_NEED_ANALYSIS_RPT,
        NA_ACTION_TR_GET_ANNUAL_TRAINING_SUMMARY_RECORD_RPT,
        NA_ACTION_TR_GET_INDIVIDUAL_EMPLOYEE_TRAINING_RECORD_RPT,
        NA_ACTION_GET_ASSESS_TR_HIST_BY_RUSERID,
        NA_ACTION_GET_INSTANT_TRAINING_EMPLOYEE_LIST_RPT,
        NA_ACTION_GET_BYPASS_DEPTLIST_BY_MODULEID,
        //NA_ACTION_GET_ACTIVE_USER_LIST,
        ACTION_EMPLOYEE_ADD_TRAINING,
        ACTION_EMPLOYEE_UPD_TRAINING,


        NA_ACTION_GET_VERTION_LIST_ALL,

        ACTION_TR_UPLOADEDFILE_CRUD,
        NA_ACTION_GET_TR_UPLOADEDFILE_LIST,
        ACTION_TRAINING_ATTENDANCE_UPDATE,


        NA_ACTION_GET_VERSION_LIST_BY_TRAININGID,
        NA_ACTION_TR_GET_SOP_TRAINING_RECORD_RPT,
        NA_ACTION_TR_GET_SOP_TRAINING_REPORT,
        NA_ACTION_GET_TR_TRAINING_PARTICIPANTS_FOR_REPORT,
        NA_ACTION_GET_FILTERED_TRAINING_PARTICIPANTS,

        //Safayat bhai
        NA_ACTION_GET_ALL_YEAR_MONTH_TR_GROUP_OF_CALENDER,
        NA_ACTION_TR_GET_ANNUAL_TRAINING_REPORT,
        NA_ACTION_TR_GET_TRAINING_PARTICIPANTS,
        NA_ACTION_TR_GET_TRAINER_LIST,
        //Shafayat End
        NA_ACTION_TR_GET_SHEDULED_TRAINING_REPORT,
        NA_ACTION_TR_GET_SHEDULED_PARTICIPANTS,

        NA_ACTION_TR_GET_TRAINER_LIST_BY_TRAINING_ID,



        #endregion

        ACTION_LOANGRADE_CONFIG_ADD,    // WALI :: 11-Jun-2014
        NA_ACTION_GET_LOANGRADE_CONFIG,    // WALI :: 11-Jun-2014


        //SHAKIR 11.06.14
        ACTION_CERTIFICATE_ADD,
        ACTION_CERTIFICATE_UPD,
        NA_ACTION_GET_CERTIFICATE_LIST,
        ACTION_CERTIFICATE_DEL,
        NA_ACTION_GET_CERTIFICATE,
        ACTION_DOES_CERTIFICATE_EXIST,
        NA_ACTION_GET_ASSETS_HISTORY,

        ACTION_OTHERS_BENEFIT_ADD,
        ACTION_OTHERS_BENEFIT_UPD,
        ACTION_OTHERS_BENEFIT_DEL,
        NA_ACTION_GET_OTHERS_BENEFIT,
        ACTION_JOB_APPLICATION_SHORTING,
        ACTION_JOB_APPLICATION_SHORTING_AD,

        NA_ACTION_Q_ALL_SIMPARAMETER,
        ACTION_SIMPARAMETER_ADD,
        NA_ACTION_GET_IncreaseSalary_KPI,

        #region Benefit Input Management :: WALI :: 13-Jun-2014
        ACTION_LOAN_BENEFIT_RULE_CREATE,
        NA_ACTION_GET_LOAN_BENEFIT_RULE,
        ACTION_BENEFIT_INPUT_FIELD_CREATE,
        ACTION_BENEFIT_INPUT_FIELD_UPD,
        NA_ACTION_GET_BENEFIT_INPUT_FIELD_LIST_ALL,
        ACTION_BENEFIT_INPUTFIELD_DEL,

        ACTION_BENEFIT_CATEGORY_CREATE,
        ACTION_BENEFIT_CATEGORY_UPD,
        NA_ACTION_GET_BENEFIT_CATEGORY_LIST,
        ACTION_BENEFIT_CATEGORY_DELETE,
        NA_ACTION_GET_BENEFIT_CATEGORY_LIST_ALL,

        ACTION_BIFCONFIG_CREATE,
        ACTION_BIFCONFIG_DELETE,
        NA_ACTION_GET_BENEFIT_INPUTFIELD_LIST_BY_BENEFITID,
        NA_ACTION_GET_BENEFIT_LIST_BY_CATEGORYEID,
        ACTION_BENEFIT_CATEGORY_CONFIG_CREATE,

        NA_ACTION_GET_BENEFIT_FIELD_LIST_BY_BREQUESTID,
        NA_ACTION_GET_BENEFIT_USING_LOGINID,
        NA_ACTION_GET_FIELD_LIST_BY_BENEFITID_LOGINID,

        ACTION_BENEFIT_APPLY,
        NA_ACTION_BENEFIT_APPLIED_LIST,
        NA_ACTION_GET_BENEFIT_ACTIVITY_LIST_ALL,
        ACTION_BENEFIT_APPLY_APPROVATION,
        NA_ACTION_GET_BENEFIT_LIMIT_BY_LOGINID,

        NA_ACTION_GET_LOAN_APPROVED_EMPLOYEE,
        #endregion

        #region HSM :: WALI :: 17-Jun-2014
        ACTION_HSM_TYPE_CREATE,
        NA_ACTION_GET_HSM_TYPE_LIST_ALL,
        ACTION_HSM_INPUT_FIELD_CREATE,
        NA_ACTION_GET_HSM_INPUT_FIELD_LIST,
        ACTION_HSMIFCONFIG_CREATE,
        NA_ACTION_GET_HSM_INPUTFIELD_LIST_BY_HSMTYPEID,
        ACTION_HSM_SUBMIT,
        NA_ACTION_GET_HSM_SUBMISSION_DATA,
        #endregion

        NA_GET_EMPLOYEE_FOR_TRAINING_NEED_ASSESMENT,
        NA_ACTION_GET_EMPLOYEE_FOR_TRAINING_COST,
        ACTION_SAVE_TRAINING_COST,

        NA_ACTION_GET_EMPLOYEE_FOR_TERMINATION_CHECKLIST,
        NA_ACTION_SAVE_EMPLOYMENT_TERMINATION_CHECKLIST,
        NA_ACTION_GET_DEPOSIT_DETAILS,
        NA_ACTION_GET_EMPLOYEE_LIST_BY_ANY_CRITERIA,
        NA_ACTION_GET_OUTSTANDING_LOAN,

        ACTION_UNIVERSITY_CREATE,
        ACTION_UNIVERSITY_UPDATE,
        ACTION_UNIVERSITY_DEL,

        ACTION_LAHC_CREATE,
        ACTION_LAHC_DEL,

        NA_ACTION_GET_EMPLOYEE_TYPE_LIST,
        ACTION_EMPLOYEE_TYPE_CREATE,
        ACTION_EMPLOYEE_TYPE_UPDATE,
        ACTION_EMPLOYEE_TYPE_DEL,

        ACTION_JOB_DETAILS_CREATE,
        ACTION_JOB_DETAILS_UPD,
        NA_ACTION_GET_JOB_DETAILS_LIST,
        NA_ACTION_GET_GRADEWISE_JOB_DETAILS,
        ACTION_SAVE_GRADEWISE_JOB_DETAILS,
        NA_ACTION_GET_COMPETENCY_FOR_JOB_DETAILS,
        ACTION_JOB_DETAILS_DELETE,

        NA_ACTION_LMS_GET_EMPLOYEE_FOR_ENCASHMENT_VERIFICATION,
        ACTION_LMS_ENCASHMENT_VERIFICATION_CREATE,
        NA_ACTION_LMS_GET_PENDING_LEAVE_ENCASHMENT_DATA,
        ACTION_LMS_ENCASHMENT_VERIFICATION_APPROVE,

        ACTION_ZONE_CREATE,
        ACTION_ZONE_UPDATE,
        NA_ACTION_GET_ZONE_LIST,
        ACTION_ZONE_DELETE,

        NA_ACTION_LMS_GET_LAHC_ALL,

        ACTION_OCS_MEMBER_UPDATE,
        NA_ACTION_GET_OCS_EMPLOYEE_LIST,

        NA_ACTION_GET_EMPLOYEELIST_FOR_JOB_CONFIRMATION,

        ACTION_NOMINEE_SAVE,
        NA_ACTION_GET_EMPLOYEE_NOMINEE_LIST,

        NA_ACTION_GET_AUTHORITY_LIST,
        ACTION_AUTHORITY_CREATE,
        ACTION_AUTHORITY_UPDATE,
        ACTION_AUTHORITY_DELETE,

        NA_ACTION_GET_PUNISHMENT_NATURE_LIST,
        ACTION_PUNISHMENT_NATURE_CREATE,
        ACTION_PUNISHMENT_NATURE_UPDATE,
        ACTION_PUNISHMENT_NATURE_DEL,

        NA_ACTION_GET_DISCIPLINARY_TYPE_LIST,
        ACTION_DELETE_DISCIPLINARY_TYPE,

        ACTION_QUALIFICATION_ADD,
        ACTION_QUALIFICATION_DEL,
        ACTION_QUALIFICATION_UPD,
        NA_ACTION_GET_QUALIFICATION_LIST,

        ACTION_SERVICE_AGREEMENT_ADD,
        ACTION_SERVICE_AGREEMENT_DEL,
        ACTION_SERVICE_AGREEMENT_UPD,
        NA_ACTION_GET_SERVICE_AGREEMENT_LIST,

        NA_ACTION_GET_REWARD_TYPE_LIST,
        ACTION_REWARD_TYPE_CREATE,
        ACTION_REWARD_TYPE_UPDATE,
        ACTION_REWARD_TYPE_DEL,
        ACTION_REWARD_RECOGNITION_ADD,
        ACTION_REWARD_RECOGNITION_DEL,
        ACTION_REWARD_RECOGNITION_UPD,
        NA_ACTION_GET_REWARD_RECOGNITION_LIST,

        ACTION_SOCIETY_MEMBERSHIP_ADD,
        NA_ACTION_GET_SOCIETY_MEMBERSHIP_LIST,
        ACTION_SOCIETY_MEMBERSHIP_UPD,
        ACTION_SOCIETY_MEMBERSHIP_DEL,

        ACTION_EXCEPTIONAL_LOGIN_ADD_NO_CALCULATION,

        NA_ACTION_GET_EMPLOYEE_LIST_BY_FILTER,
        NA_ACTION_GET_EMPLOYEE_LIST_BY_FILTER_FOR_REPORT,
        NA_ACTION_GET_EMPLOYEE_LIST_BY_FILTER_WITHOUT_SORTING_FOR_REPORT,

        #region WPPWF Related Action
        ACTION_FUND_HEAD_CREATION,
        NA_GET_FUND_HEAD_LIST,
        ACTION_FUND_HEAD_DELETE,

        ACTION_ORGANIZATION_CREATION,
        ACTION_ORGANIZATION_UPDATE,
        NA_GET_ORGANIZATION_LIST,
        ACTION_ORGANIZATION_DELETE,

        ACTION_SCHEME_CREATE,
        NA_GET_SCHEME_LIST_BY_ORGANIZATIONID,
        ACTION_SCHEME_DELETE,

        ACTION_TERMS_OF_SEPARATION_CREATE,
        NA_GET_TERMS_OF_SEPARATION,
        ACTION_TERMS_OF_SEPARATION_UPDATE,
        ACTION_TERMS_OF_SEPARATION_DELETE,

        NA_GET_ACTIVE_EMPLOYEE_LIST,
        ACTION_CREATE_FUND_ALLOCATION,
        ACTION_UPDATE_FUND_ALLOCATION,
        NA_GET_FISCAL_YEAR_WPPWF,
        NA_GET_FUND_ALLOCATION_INFO,
        ACTION_DELETE_FUND_ALLOCATION,

        NA_GET_INVESTMENT_ABLE_FUND_INFO,
        ACTION_CREATE_INVESTMENT,
        ACTION_UPDATE_INVESTMENT,
        NA_GET_INVESTMENT_LIST,
        NA_GET_INVESTMENT_LIST_BY_INVID,
        ACTION_FREEZE_INVESTMENT,

        ACTION_INVESTMENT_SETTLEMENT_CREATE,
        ACTION_EXTRAORDINARY_INCOME_COST_CREATE,
        ACTION_EXTRAORDINARY_INCOME_COST_UPDATE,
        NA_GET_EXTRAORDINARY_INCOME_COST,
        NA_GET_EXTRAORDINARY_INCOME_COST_BENEFICIARY,
        ACTION_EXTRAORDINARY_INCOME_COST_DELETE,

        ACTION_PROVISION_CREATION,
        NA_GET_INVESTMENT_PROVISION_LIST,
        NA_GET_WP_EMPLOYEE_GL_INFO,
        NA_GET_FUND_HEAD_WISE_BALANCE,

        ACTION_HEAD_WISE_PAYMENT_CREATE,
        NA_GET_WPPWF_BALANCE_EMP_WISE,

        NA_GET_WPPWF_FUND_PAYMENT_BENEFICIARY_LIST,
        NA_GET_ALL_BENEFICIARY_LIST,
        ACTION_EMPLOYEE_SEPERATION_CREATE,
        ACTION_TERMS_OF_SEPARATION_CREATE_WPPWF,
        ACTION_TERMS_OF_SEPARATION_UPDATE_WPPWF,
        ACTION_TERMS_OF_SEPARATION_DELETE_WPPWF,
        NA_GET_TERMS_OF_SEPARATION_WPPWF,

        NA_ACTION_GET_PAYSLIP_WPPWF,

        ACTION_DELETE_LAST_INVESTMENT,
        NA_GET_LAST_FUND_ALLOCATION_DATE_WPPWF,
        NA_GET_WP_PROVISION_BENEFICIARY_LIST,

        NA_GET_PROVISION_CALCULATED_DATA_WPPWF,
        ACTION_SAVE_PROVISION_CALCULATED_DATA_WPPWF,
        NA_GET_PROVISIONED_PROFIT_FOR_EMPLOYEE_WPPWF,
        ACTION_DISTRIBUTE_PROVISIONED_PROFIT_WPPWF,
        NA_GET_PENDING_DISTRIBUTION_WPPWF,

        ACTION_FUND_JOURNAL_ENTRY_WPPWF,
        ACTION_EMPLOYEE_JOURNAL_ENTRY_WPPWF,
        NA_GET_FUND_HEAD_GL_WPPWF,
        NA_GET_TOTAL_PAYABLE_TO_LSC_WPPWF,

        NA_GET_DUE_CALCULATION_WPPWF,
        NA_GET_DISTRIBUTED_PROVISION_DATA_WPPWF,

        NA_GET_WPPWF_EMPLOYEE_LIST_FOR_SEPERATION,
        ACTION_WPPWF_EMPLOYEE_PAYMENT,
        NA_GET_WPPWF_FUNDHEAD_GL_BALANCE,
        NA_GET_WPPWF_FUNDHEAD_GL_RECORDS,
        NA_GET_WPPWF_EMPLOYEE_GL_RECORDS,
        ACTION_WPPWF_MODIFY_FUNDHEAD_GL_RECORDS,
        ACTION_WPPWF_MODIFY_EMPLOYEE_GL_RECORDS,
        #endregion

        #region PF Related Actions
        NA_GET_PF_ABLE_TO_INVEST_AMOUNT,
        ACTION_PF_CREATE_INVESTMENT,
        ACTION_PF_UPDATE_INVESTMENT,
        NA_GET_PF_INVESTMENT_LIST,
        NA_GET_PF_INVESTMENT_DETAILS_BY_INVID,
        ACTION_PF_DELETE_LAST_INVESTMENT,

        NA_GET_PF_PROVISION_BENEFICIARY_LIST,
        ACTION_PF_PROVISION_CREATION,
        NA_GET_PF_INVESTMENT_PROVISION_LIST,

        ACTION_PF_INVESTMENT_SETTLEMENT_CREATE,
        ACTION_PF_FREEZE_INVESTMENT,

        ACTION_PF_TERMS_OF_SEPARATION_SAVE,
        ACTION_PF_TERMS_OF_SEPARATION_DEL,
        NA_GET_PF_TERMS_OF_SEPARATION,

        NA_GET_PF_EMPLOYEE_LIST_FOR_SEPERATION,
        NA_GET_PF_EMPLOYEE_LIST_FOR_PAYMENT,
        NA_GET_PF_PAYMENT_AMOUNT_FOR_SEPERATION,
        ACTION_PF_EMPLOYEE_SEPARATION,
        NA_GET_PF_PAYMENT_AMOUNT_FOR_PAYMENT,
        ACTION_PF_EMPLOYEE_PAYMENT,
        NA_ACTION_GET_PF_STATEMENT,

        NA_GET_PF_FUND_HEAD_LIST,
        NA_GET_PF_FUND_RECIEVE_DETAILS,
        ACTION_PF_FUND_RECIEVE,

        NA_GET_PF_PROVISION_CALCULATED_DATA,
        ACTION_PF_SAVE_PROVISION_CALCULATED_DATA,
        NA_GET_PF_PROVISIONED_PROFIT_FOR_EMPLOYEE,
        ACTION_PF_DISTRIBUTE_PROVISIONED_PROFIT,
        NA_GET_PF_PENDING_DISTRIBUTION,

        ACTION_PF_FUND_JOURNAL_ENTRY,
        ACTION_PF_FUND_JOURNAL_ENTRY_MULTIPLE,
        NA_GET_PF_HEADWISE_AVAILABLE_AMOUNT,
        NA_GET_PF_JOURNAL_EMPLOYEE_BY_PF_MEMBDATE,
        ACTION_PF_EMPLOYEE_JOURNAL_ENTRY,
        ACTION_PF_FUND_JOURNAL_DESCRIPTION_ADD,
        ACTION_PF_FUND_JOURNAL_DESCRIPTION_UPD,
        ACTION_PF_FUND_JOURNAL_DESCRIPTION_DEL,

        NA_GET_PF_RECIEVED_FUND_LIST,
        NA_GET_PF_TOTAL_PAYABLE_TO_LSC,

        NA_GET_PF_INVESTMENT_REGISTER,
        NA_GET_PF_INV_ENCASHMENT_REGISTER,
        NA_GET_PF_DISTRIBUTED_PROVISION_DATA,

        NA_GET_PF_EMPLOYEE_BALANCE,
        ACTION_PF_MODIFY_EMPLOYEE_BALANCE,
        NA_GET_PF_FUND_BALANCE,
        ACTION_PF_MODIFY_FUND_BALANCE,

        NA_GET_PF_BENEFICIARY_LIST_TO_FUND_RECIEVE,
        ACTION_PF_FUND_RECIEVE_EMPLOYEE_WISE,

        NA_GET_PF_BALANCE_SHEET,
        #endregion

        NA_GET_DISTINCT_FUND_YEAR_PF,
        NA_ACTION_GET_CUMULATIVPF_YEAR_WISE,

        #region HRAS Related :: Rony
        #region Service Type::Rony
        NA_ACTION_GET_SERVICE_TYPE_LIST,
        ACTION_SERVICE_TYPE_CREATE,
        ACTION_SERVICE_TYPE_UPDATE,
        ACTION_SERVICE_TYPE_DEL,
        #endregion

        #region CheckList ::Rony
        NA_ACTION_GET_CHECKLIST_LIST,
        ACTION_CHECKLIST_CREATE,
        ACTION_CHECKLIST_UPDATE,
        ACTION_CHECKLIST_DEL,
        #endregion

        #region Task Category ::Rony
        NA_ACTION_GET_TASK_LIST,
        ACTION_TASK_CREATE,
        ACTION_TASK_UPDATE,
        ACTION_TASK_DEL,
        #endregion

        #region Task/Task Type ::Rony
        NA_ACTION_GET_TASK_TYPE_ALL_LIST,
        NA_ACTION_GET_TASK_CHECK_LIST,
        ACTION_TASKTYPE_ADD,
        ACTION_TASKTYPE_UPD,
        ACTION_TASKTYPE_DEL,
        #endregion

        #region Vendor ::Rony
        NA_ACTION_GET_VENDOR_ALL_LIST,
        NA_ACTION_GET_VENDOR_SERVICE_LIST,
        ACTION_VENDOR_ADD,
        ACTION_VENDOR_UPD,
        ACTION_VENDOR_DEL,
        #endregion

        #region Task Allocation ::Rony
        NA_ACTION_GET_TASK_FILTER_ALL,
        NA_ACTION_GET_ALLOCATION_ACTIVITIES,
        ACTION_ALLOCATION_REQUEST_ADD,
        ACTION_ALLOCATION_REQUEST_EXECUTE,

        #endregion

        #region Task Execution ::Rony

        NA_ACTION_GET_ALLOCATION_FOR_EXECUTION,
        NA_ACTION_GET_TASK_LIST_OF_VENDOR,
        NA_ACTION_GET_ALLOCATION_FOR_EXECUTION_ALL_INFO,
        #endregion

        #region Job Status ::Rony
        NA_ACTION_GET_ASSIGNED_JOB_STATUS,
        ACTION_ASSIGNED_JOB_STATUS_UPDATE,
        #endregion

        #endregion
        NA_KILL_INACTIVE_SESSIONS,

        ACTION_SET_GRADE_POSITION,  // Update :: WALI :: 28-Dec-2014
        ACTION_SET_DEGREE_POSITION, // WALI :: 06-Jan-2015

        #region Subject Of Letter Action :: Rony :: 21-JAN-2015
        ACTION_LETTER_SUBJECT_CREATE,
        ACTION_LETTER_SUBJECT_UPDATE,
        ACTION_LETTER_SUBJECT_DEL,
        NA_ACTION_GET_LETTER_SUBJECT_LIST,
        #endregion
        #region Subject Of Template Action :: Rony :: 21-JAN-2015
        //ACTION_VIEW_LETTER_TEMPLATE_UI,   //RONY :: 21-JAN-2015
        //ACTION_View_LETTER_TEMPLATE_LIST_UI,
        ACTION_LETTER_TEMPLATE_CREATE,
        ACTION_LETTER_TEMPLATE_UPDATE,
        ACTION_LETTER_TEMPLATE_DEL,
        NA_ACTION_GET_LETTER_TEMPLATE_LIST,
        NA_ACTION_GET_LETTER_TEMPLATE_DETAILS_LIST,
        #endregion
        NA_GET_SALARY_PARTICULARS_GRADELEVEL_WISE,

        ACTION_COMPANY_DIVISION_ADD,
        NA_ACTION_GET_COMPANY_DIVISION_LIST,
        ACTION_COMPANY_DIVISION_UPD,
        ACTION_COMPANY_DIVISION_DEL,


        //New Shakir 16.02.2015
        ACTION_QUALIFICATION_ENTRY,
        ACTION_QUALIFICATION_UPDATE,
        NA_GET_QUALIFICATION_LIST,
        ACTION_QUALIFICATION_DELETE,

        NA_GET_WPPWF_PF_STATEMENT,

        NA_ACTION_GET_RETIREMENT_TYPE,
        NA_ACTION_GET_ALL_LOAN_BENEFIT_RULE,

        NA_ACTION_GET_PAY_FIXATION_ASSESS_EMPLOYEE,

        // Termination Type :: RONY :: 10 MAR 2015
        ACTION_TERMINATION_TYPE_CREATE,
        ACTION_TERMINATION_TYPE_UPDATE,
        ACTION_TERMINATION_TYPE_DEL,
        NA_ACTION_GET_TERMINATION_TYPE_LIST,

        NA_ACTION_GET_REPORT_LIST_FORSIGNATORY,
        NA_ACTION_GET_EMPLOYEE_LIST_FOR_SIGNATORY,
        ACTION_REPORT_SIGNATORY_CREATE,

        NA_ACTION_GET_EMPLOYEE_LIST_FOR_SUCCESSION,



        //New for Training of AIBL
        ACTION_TRAINING_CALENDAR_SHEDULE_CREATE,
        NA_ACTION_GET_SCHEDULE_BY_CALENDARID,
        NA_ACTION_GET_BUDGET_BY_YEAR,
        ACTION_TRAINING_PROGRAMME_SHEDULE_CREATE,
        NA_ACTION_GET_SCHEDULED_SESSION_BY_SCHEDULEID,

        ACTION_COST_HEAD_CREATE,
        NA_GET_COST_HEAD_LIST,
        ACTION_COST_HEAD_DELETE,
        ACTION_COST_HEAD_UPDATE,
        NA_GET_EMPLIST_FOR_TR_NEED_ASSESSMENT,

        ACTION_TRAINING_COST_AND_PAYMENT_RULE_CREATE,
        NA_GET_TRAINING_COST_AND_PAYMENT_RULES_LIST,
        ACTION_TRAINING_COST_AND_PAYMENT_RULE_UPDATE,
        ACTION_TRAINING_COST_AND_PAYMENT_RULE_DELETE,

        ACTION_TRAINING_COST_SETUP_CREATE_FOR_BUDGET,
        NA_GET_COST_SETUP_INFO_BY_YEAR,

        ACTION_TR_ADVICE_CREATE,
        ACTION_TR_ADVICE_UPDATE,
        ACTION_TR_ADVICE_DELETE,
        NA_GET_ADVICE_LIST,
        //End. New Training for AIBL

        //Transfer start
        ACTION_BULK_TRANSFER_CREATE,
        NA_GET_BULK_TRANSFER_LIST,
        ACTION_BULK_TRANSFER_UPDATE,
        ACTION_BULK_TRANSFER_DELETE,
        ACTION_TRANSFER_APPROVAL_PATH_CONFIG_CREATE,
        NA_GET_TRANSFER_APPROVAL_PATH_CONFIG_INFO,
        NA_GET_TRANSFER_INFORMATION,
        ACTION_BULK_TRANSFER_PROCESS_SUBMIT,
        NA_GET_TRANSFER_RULES_VALIDITY_MESSAGE,
        ACTION_TRANSFER_AUTHORITY_CREATE,
        NA_GET_ACTION_TRANSFER_AUTHORITY_CONFIG_INFO,
        NA_GET_LOGEDIN_USER_TRANSFER_MAINTAINENCE_INFO,
        //Transfer end

        //Transfer Authority Delete :: Rony :: 31-Mar-2016
        ACTION_TRANSFER_AUTHORITY_DELETE,
        //End

        //GL Mapping for Salary Transaction Report:: Rony
        ACTION_GL_MAPPING_FOR_SALARY_TRANSACTION_ADD,
        NA_ACTION_GET_GL_MAPPING_DATA_LIST,
        NA_ACTION_GET_ALLOWDEDUCT_ALLTYPE_LIST,
        ACTION_GL_MAPPING_FOR_SALARY_TRANSACTION_UPDATE,

        ACTION_ASSET_ASSIGN,

        #region Separation Management :: Rony

        ACTION_SEPARATION_TYPE_ADD,
        NA_ACTION_SEPARATION_TYPE_LIST,
        NA_ACTION_SEPARATION_TYPE_ITEMS_LIST,
        ACTION_SEPARATION_TYPE_UPDATE,
        ACTION_SEPARATION_TYPE_DELETE,

        ACTION_SEPARATION_TYPE_MAPPING_WITH_GRADE_ADD,
        NA_ACTION_SEPARATION_TYPE_MAPPING_WITH_GRADE_ITEMS,
        NA_ACTION_SEPARATION_TYPE_SALARY_INFO,

        #endregion

        NA_ACTION_GET_EMPLOYEE_LIST_BY_FILTER_WITH_SUBORDINATE,

        NA_ACTION_GET_LEAVE_HISTORY_IN_DATE_RANGE,

        ACTION_SET_EDU_DEGREE_POSITION,
        ACTION_EDU_DEGREE_CREATE,
        ACTION_EDU_DEGREE_UPDATE,
        ACTION_EDU_DEGREE_DELETE,

        //Shakir start
        NA_GET_FINAL_SETTLEMENT_APPROVAL_AUTHORITY_CONFIG_INFO,
        ACTION_FINAL_SETTLEMENT_APPROVAL_AUTHORITY_CONFIG_CREATE,
        NA_GET_FINAL_SETTLEMENT_PARKING_EMP_LIST,
        NA_GET_FINAL_SETTLEMENT_PARKING_EMP_CALCULATION_DETAILS,
        ACTION_FINAL_SETTLEMENT_PARKING_EMP_LIST_UPDATE,
        ACTION_FINAL_SETTLEMENT_PARKING_EMP_LIST_CREATE,
        //Shakir End
        NA_ACTION_GetAdministrativeRoleAllByLoginID,

        // WALI :: RECRUITMENT 18-Jun-2015
        ACTION_JOB_APPLICANT_CREATE,
        ACTION_JOB_APPLICANT_UPDATE,
        ACTION_JOB_APPLICANT_CREATE_AND_JOB_APPLY,
        ACTION_JOB_APPLICANT_UPDATE_AND_JOB_APPLY,
        ACTION_JOB_APPLY_ONLY,
        NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID,
        NA_ACTION_GET_JOB_APPLICANT_BY_JOBAPPLICATIONID,
        // WALI :: End

        //Rony :: Final Settlement :: 23 Jun 2015
        NA_ACTION_GET_EMPLOYEE_COUNT_FOR_SETTLEMENT,

        //Rony ::Letter
        NA_ACTION_GET_EMPLOYEE_FILTER_LIST_BY_EVENTCODE,
        NA_ACTION_GET_EMPLOYEE_PFSLIP_FOR_LETTER,
        NA_ACTION_GET_EMPLOYEE_INCREMENT_FOR_LETTER,
        NA_ACTION_GET_EMPLOYEE_INFO_FOR_ORGANOGRAM,

        NA_ACTION_GET_SALARY_PROCESS_DATA_BY_EMP,     // WALI :: Get salaryData employee wise.

        NA_ACTION_GET_EXISTING_EMPLOYEE_LIST,

        // WALI :: Report - PF Module :: 12-Aug-2015
        ACTION_VIEW_FundLedger_PF_PUI,
        NA_GET_PF_FUND_LEDGER,

        NA_ACTION_GET_GRATUITYSLIP,

        #region Employee Category :: WALI :: 26-Aug-2015
        NA_ACTION_GET_EMPLOYEE_CATEGORY_LIST,
        ACTION_EMPLOYEE_CATEGORY_CREATE,
        ACTION_EMPLOYEE_CATEGORY_UPDATE,
        ACTION_EMPLOYEE_CATEGORY_DELETE,
        #endregion

        #region Country Division :: WALI :: 27-Aug-2015
        ACTION_COUNTRY_DIVISION_CREATE,
        ACTION_COUNTRY_DIVISION_UPDATE,
        ACTION_COUNTRY_DIVISION_DELETE,
        #endregion

        #region Country District :: WALI :: 31-Aug-2015
        ACTION_COUNTRY_DISTRICT_CREATE,
        ACTION_COUNTRY_DISTRICT_UPDATE,
        ACTION_COUNTRY_DISTRICT_DELETE,
        #endregion

        #region Country Thana :: WALI :: 01-Sep-2015
        ACTION_COUNTRY_THANA_CREATE,
        ACTION_COUNTRY_THANA_UPDATE,
        ACTION_COUNTRY_THANA_DELETE,
        #endregion

        NA_ACTION_GET_EMPLOYEE_LIST_TO_ASSIGN_LEAVE,
        ACTION_LMS_LEAVE_ADD_MULTIPLE_EMPLOYEE,

        ACTION_DOES_OLD_PROCESS_EXIST_BY_EMPLOYEE,  // WALI :: Check if old process (Salary, Hist, Leave, PF, WPPWF) exists [While changing EmployeeCode]
        NA_GET_PF_LEDGER_INFORMATION,

        NA_ACTION_GET_SPECIFIC_EMPLOYEE_ATTENDANCE_NO_CALCULATION,
        ACTION_ADD_ATTENDANCE_NO_CALCULATION,
        NA_ACTION_Q_LOAN_ALL_SETTLED,
        NA_ACTION_GET_ALLOWDEDUCT_CONFIG_INFO,

        NA_ACTION_GET_ALLOWDEDUCT_LIST_BY_PARAM,
        ACTION_SUSPEND_RULE_ADD,
        NA_GET_SUSPENDRULE_GENERAL,
        NA_GET_SUSPENDRULE_ALLOWDEDUCT,

        //Start : Rony : Employeewise PF Parameter
        ACTION_EMPLOYEEWISE_PF_PARAMETER_ADD,
        NA_ACTION_EMPLOYEE_PF_MEMBER_LIST,
        NA_ACTION_EMPLOYEE_PF_PARAMETER,
        ACTION_EMPLOYEE_PFMEMBERSHIP_STATUSWISE_UPD,
        //End


        NA_ACTION_GET_PROGRAM_SCHEDULE_FOR_REPORT,
        NA_ACTION_GET_TR_PROGRAM_COORDINATOR,
        NA_ACTION_GET_TR_TRAINEE_FOR_REPORT,
        NA_ACTION_GET_TR_TRAINEE_ATTENDANCE_FOR_REPORT,
        NA_ACTION_GET_TR_EVALUTION_GRADE,
        NA_ACTION_GET_TR_TRAINEE_EVALUTION_FOR_REPORT,

        ACTION_VIEW_TR_Evaluation_Report_UI,
        ACTION_VIEW_TR_Trainer_Assess_Report_UI,

        NA_ACTION_GET_Data_Existence_ALS,

        ACTION_VIEW_PF_Ledger_PUI,
        NA_ACTION_GET_SuspensionDeduction_BySalaryID,
        NA_ACTION_GET_DS_Suspension_ForRefund,
        NA_GET_EMP_SERVICE_DAY,
        NA_GET_PF_EMPLOYEE_LIST_FOR_GEN_PAYMENT,
        ACTION_PF_PAYMENT_GENERAL,
        NA_GET_PF_PAYMENT_AMOUNT_GENERAL,
        NA_GET_PAYSLIP_ANOMALIES,
        ACTION_SALARY_ANOMALY_UPDATE,
        NA_GET_TARGET_RESOURCE_PLANING_INFO,
        ACTION_TARGET_RESOURCE_PLANING_SAVE,
        NA_GET_PF_SEPARATION_INFO,
        NA_GET_ACTION_LEAVE_GOVERNANCE_INFO,
        ACTION_LMS_LEAVE_GOVERNANCE_CREATE,
        NA_ACTION_GET_EMP_LEAVE_ENCAS_BY_EMPTYPESITE,
        NA_ACTION_LMS_GET_LEAVETYPE_LIST_FOR_CC_MAIL,
        ACTION_LMS_LEAVE_CC_MAIL_USER_CREATE,

        NA_ACTION_GET_EMP_LIST_WITH_EDUCATION_INFO,
        NA_ACTION_GET_JOB_APPLICANT_FOR_ADMITCARD,
        ACTION_APPLICANT_ROLL_SAVE_FOR_ADMITCARD,

        NA_ACTION_GET_EMPLOYEE_LIST_SP,
        NA_ACTION_GET_EMPLOYEE_HIST_SP,
        NA_ACTION_GET_BASIC_SALARY_SP,
        NA_ACTION_Q_TAXHISTORY_BY_SALARYYEAR_SP,

        ACTION_RELATION_SAVE,
        ACTION_RELATION_UPDATE,
        ACTION_RELATION_DELETE,
        NA_ACTION_GET_RELATION_LIST,

        ACTION_ORGANIZATION_TYPE_SAVE,
        ACTION_ORGANIZATION_TYPE_UPDATE,
        ACTION_ORGANIZATION_TYPE_DELETE,
        NA_ACTION_GET_ORGANIZATION_TYPE_LIST,


        //Start Benevolent Fund Actions
        NA_ACTION_GET_BF_DATA,

        ACTION_BF_FUND_TYPE_ADD,
        ACTION_BF_FUND_TYPE_UPP,
        ACTION_BF_FUND_TYPE_DEL,
        NA_ACTION_GET_BF_FUND_TYPE_LIST,
        ACTION_BF_SHARE_RATE_UPP,
        ACTION_BF_MEMBER_UPP,
        //End Benevolent Fund Actions

        NA_ACTION_GET_BENEVOLENT_FUND_INFO_SP,




        #region BF Related Actions
        NA_GET_BF_ABLE_TO_INVEST_AMOUNT,
        ACTION_BF_CREATE_INVESTMENT,
        ACTION_BF_UPDATE_INVESTMENT,
        NA_GET_BF_INVESTMENT_LIST,
        NA_GET_BF_INVESTMENT_DETAILS_BY_INVID,
        ACTION_BF_DELETE_LAST_INVESTMENT,
        NA_GET_BF_PROVISION_BENEFICIARY_LIST,
        ACTION_BF_PROVISION_CREATION,
        NA_GET_BF_INVESTMENT_PROVISION_LIST,
        ACTION_BF_INVESTMENT_SETTLEMENT_CREATE,
        ACTION_BF_FREEZE_INVESTMENT,
        ACTION_BF_TERMS_OF_SEPARATION_SAVE,
        ACTION_BF_TERMS_OF_SEPARATION_DEL,
        NA_GET_BF_TERMS_OF_SEPARATION,
        NA_GET_BF_EMPLOYEE_LIST_FOR_SEPERATION,
        NA_GET_BF_EMPLOYEE_LIST_FOR_PAYMENT,
        NA_GET_BF_PAYMENT_AMOUNT_FOR_SEPERATION,
        ACTION_BF_EMPLOYEE_SEPARATION,
        NA_GET_BF_PAYMENT_AMOUNT_FOR_PAYMENT,
        ACTION_BF_EMPLOYEE_PAYMENT,
        NA_ACTION_GET_BF_STATEMENT,
        NA_GET_BF_FUND_HEAD_LIST,
        NA_GET_BF_FUND_RECIEVE_DETAILS,
        ACTION_BF_FUND_RECIEVE,
        NA_GET_BF_PROVISION_CALCULATED_DATA,
        ACTION_BF_SAVE_PROVISION_CALCULATED_DATA,
        NA_GET_BF_PROVISIONED_PROFIT_FOR_EMPLOYEE,
        ACTION_BF_DISTRIBUTE_PROVISIONED_PROFIT,
        NA_GET_BF_PENDING_DISTRIBUTION,
        ACTION_BF_FUND_JOURNAL_ENTRY,
        NA_GET_BF_HEADWISE_AVAILABLE_AMOUNT,
        NA_GET_BF_JOURNAL_EMPLOYEE_BY_BF_MEMBDATE,
        ACTION_BF_EMPLOYEE_JOURNAL_ENTRY,
        NA_GET_BF_RECIEVED_FUND_LIST,
        NA_GET_BF_TOTAL_PAYABLE_TO_LSC,
        NA_GET_BF_INVESTMENT_REGISTER,
        NA_GET_BF_INV_ENCASHMENT_REGISTER,
        NA_GET_BF_DISTRIBUTED_PROVISION_DATA,
        NA_GET_BF_EMPLOYEE_BALANCE,
        ACTION_BF_MODIFY_EMPLOYEE_BALANCE,
        NA_GET_BF_FUND_BALANCE,
        ACTION_BF_MODIFY_FUND_BALANCE,
        NA_GET_BF_BENEFICIARY_LIST_TO_FUND_RECIEVE,
        ACTION_BF_FUND_RECIEVE_EMPLOYEE_WISE,
        NA_GET_BF_BALANCE_SHEET,
        NA_GET_DISTINCT_FUND_YEAR_BF,
        NA_GET_BF_EMPLOYEE_LIST_FOR_GEN_PAYMENT,
        ACTION_BF_PAYMENT_GENERAL,
        NA_GET_BF_PAYMENT_AMOUNT_GENERAL,
        NA_GET_BF_SEPARATION_INFO,
        NA_GET_BF_STATEMENT,
        NA_GET_BF_LEDGER_INFORMATION,
        #endregion

        NA_ACTION_GET_EMP_FOR_INCREMENT_BANGLA_REPORT,

        NA_ACTION_GET_ITINV_ALLOWANCE_SLAB_BY_YEAR,
        NA_ACTION_GET_ITIRSLAB_SP,

        NA_ACTION_GET_SITE_LIST_BYFILTER,
        NA_GET_WPPWF_BALANCE_SHEET,

        #region Gratuity Related Actions
        //Start Gratuity View UI (GR View)
        ACTION_VIEW_GR_SHARE_RATE_UI,
        ACTION_VIEW_GR_FUND_TYPE_UI,
        ACTION_VIEW_GR_MEMBER_LIST_UI,
        ACTION_VIEW_GR_MEMBER_UI,
        ACTION_View_GR_Distribute_ProvisionedProfit_UI,
        ACTION_View_GR_Employee_JournalEntry_UI,
        ACTION_View_GR_Employee_List_For_Payment_UI,
        ACTION_View_GR_Employee_List_For_Seperation_UI,
        ACTION_View_GR_Employee_Payment_UI,
        ACTION_View_GR_Employee_Seperation_UI,
        ACTION_View_GR_GRFundRecieve_UI,
        ACTION_View_GR_Fund_Recieve_Exceptional_UI,
        ACTION_View_GR_Investment_UI,
        ACTION_View_GR_Investment_List_UI,
        ACTION_View_GR_Investment_Settlement_UI,
        ACTION_View_GR_Investment_Settlement_List_UI,
        ACTION_View_GR_Journal_Entry_UI,
        ACTION_View_GR_Modify_Employee_Balance_UI,
        ACTION_View_GR_Modify_Fund_Balance_UI,
        ACTION_View_GR_Investment_Provision_UI,
        ACTION_View_GR_Terms_Of_Separation_UI,

        ACTION_VIEW_InvEncashmentRegister_GR_PUI,
        ACTION_VIEW_InvestmentRegister_GR_PUI,
        ACTION_VIEW_BalanceSheet_GR_PUI,
        ACTION_VIEW_GR_Ledger_PUI,
        ACTION_VIEW_GR_Separation_Report,
        ACTION_VIEW_GRStatement_PUI,
        //End Gratuity View

        NA_ACTION_GET_GR_DATA,
        ACTION_GR_FUND_TYPE_ADD,
        ACTION_GR_FUND_TYPE_UPP,
        ACTION_GR_FUND_TYPE_DEL,
        NA_ACTION_GET_GR_FUND_TYPE_LIST,
        ACTION_GR_SHARE_RATE_UPP,
        ACTION_GR_MEMBER_UPP,
        NA_GET_GR_ABLE_TO_INVEST_AMOUNT,
        ACTION_GR_CREATE_INVESTMENT,
        ACTION_GR_UPDATE_INVESTMENT,
        NA_GET_GR_INVESTMENT_LIST,
        NA_GET_GR_INVESTMENT_DETAILS_BY_INVID,
        ACTION_GR_DELETE_LAST_INVESTMENT,
        NA_GET_GR_PROVISION_BENEFICIARY_LIST,
        ACTION_GR_PROVISION_CREATION,
        NA_GET_GR_INVESTMENT_PROVISION_LIST,
        ACTION_GR_INVESTMENT_SETTLEMENT_CREATE,
        ACTION_GR_FREEZE_INVESTMENT,
        ACTION_GR_TERMS_OF_SEPARATION_SAVE,
        ACTION_GR_TERMS_OF_SEPARATION_DEL,
        NA_GET_GR_TERMS_OF_SEPARATION,
        NA_GET_GR_EMPLOYEE_LIST_FOR_SEPERATION,
        NA_GET_GR_EMPLOYEE_LIST_FOR_PAYMENT,
        NA_GET_GR_PAYMENT_AMOUNT_FOR_SEPERATION,
        ACTION_GR_EMPLOYEE_SEPARATION,
        NA_GET_GR_PAYMENT_AMOUNT_FOR_PAYMENT,
        ACTION_GR_EMPLOYEE_PAYMENT,
        NA_ACTION_GET_GR_STATEMENT,
        NA_GET_GR_FUND_HEAD_LIST,
        NA_GET_GR_FUND_RECIEVE_DETAILS,
        ACTION_GR_FUND_RECIEVE,
        NA_GET_GR_PROVISION_CALCULATED_DATA,
        ACTION_GR_SAVE_PROVISION_CALCULATED_DATA,
        NA_GET_GR_PROVISIONED_PROFIT_FOR_EMPLOYEE,
        ACTION_GR_DISTRIBUTE_PROVISIONED_PROFIT,
        NA_GET_GR_PENDING_DISTRIBUTION,
        ACTION_GR_FUND_JOURNAL_ENTRY,
        NA_GET_GR_HEADWISE_AVAILABLE_AMOUNT,
        NA_GET_GR_JOURNAL_EMPLOYEE_BY_GR_MEMBDATE,
        ACTION_GR_EMPLOYEE_JOURNAL_ENTRY,
        NA_GET_GR_RECIEVED_FUND_LIST,
        NA_GET_GR_TOTAL_PAYABLE_TO_LSC,
        NA_GET_GR_INVESTMENT_REGISTER,
        NA_GET_GR_INV_ENCASHMENT_REGISTER,
        NA_GET_GR_DISTRIBUTED_PROVISION_DATA,
        NA_GET_GR_EMPLOYEE_BALANCE,
        ACTION_GR_MODIFY_EMPLOYEE_BALANCE,
        NA_GET_GR_FUND_BALANCE,
        ACTION_GR_MODIFY_FUND_BALANCE,
        NA_GET_GR_BENEFICIARY_LIST_TO_FUND_RECIEVE,
        ACTION_GR_FUND_RECIEVE_EMPLOYEE_WISE,
        NA_GET_GR_BALANCE_SHEET,
        NA_GET_DISTINCT_FUND_YEAR_GR,
        NA_GET_GR_EMPLOYEE_LIST_FOR_GEN_PAYMENT,
        ACTION_GR_PAYMENT_GENERAL,
        NA_GET_GR_PAYMENT_AMOUNT_GENERAL,
        NA_GET_GR_SEPARATION_INFO,
        NA_GET_GR_STATEMENT,
        NA_GET_GR_LEDGER_INFORMATION,
        #endregion

        ACTION_VIEW_Attendance_Simple_Login_UI,
        ACTION_ADD_ATTENDANCE_SIMPLE_LOGIN,

        #region Basic Accounting
        ACTION_COA_CHILD_NODE_CREATE_UPDATE,
        ACTION_COA_SELECTED_NODE_DELETE,
        NA_ACTION_GET_BASIC_ACCOUNTING_INFO,

        NA_ACTION_GET_ACC_SUPPLIER_LIST,
        NA_ACTION_GET_ACC_CUSTOMER_LIST,

        ACTION_ACC_SUPPLIER_CREATE,
        ACTION_ACC_SUPPLIER_UPDATE,
        ACTION_ACC_SUPPLIER_DELETE,

        ACTION_ACC_CUSTOMER_CREATE,
        ACTION_ACC_CUSTOMER_UPDATE,
        ACTION_ACC_CUSTOMER_DELETE,

        NA_ACTION_GET_ACC_REFERENCE_LIST,
        ACTION_ACC_REFERENCE_CREATE,
        ACTION_ACC_REFERENCE_UPDATE,
        ACTION_ACC_REFERENCE_DELETE,

        NA_ACTION_GET_ACC_SEGMENT_SETUP,
        NA_ACTION_GET_ACC_CODES,

        NA_ACTION_GET_ACC_REP_SPECIFICATION,
        ACTION_ACC_REP_SPEC_NODE_CREATE,
        ACTION_ACC_REP_SPEC_NODE_UPDATE,
        ACTION_ACC_REP_SPEC_NODE_DELETE,

        NA_ACTION_GET_COA_DETAILS,
        NA_ACTION_GET_COA_PARTIES,
        NA_ACTION_GET_COA_BY_PARTY,
        NA_ACTION_GET_COA_LIST,

        ACTION_ACC_VOUCHER_CREATE,
        ACTION_ACC_VOUCHER_UPDATE,
        NA_ACTION_ACC_GET_GENERAL_VOUCHER_BY_FILTER,
        ACTION_ACC_GENERAL_VOUCHER_APPROVE,
        NA_ACTION_ACC_GET_APPROVER_USERS,
        NA_ACTION_ACC_GET_MEMO_VOUCHERS,
        ACTION_ACC_VOUCHER_DELETE,

        ACTION_ACC_BANK_INFO_CREATE,
        ACTION_ACC_BANK_INFO_UPDATE,
        ACTION_ACC_BANK_INFO_DELETE,
        NA_ACTION_GET_ACC_BANK_INFO,

        ACTION_ACC_APPROVAL_ROUTE_ADD,
        NA_ACTION_GET_ACC_APPROVAL_ROUTE_LIST,

        ACTION_ACC_CODE_CREATE,

        ACTION_ACC_VOUCHER_TEMPLATE_CREATE,
        ACTION_ACC_VOUCHER_TEMPLATE_UPDATE,
        ACTION_ACC_VOUCHER_TEMPLATE_DELETE,
        NA_ACTION_GET_ACC_VOUCHER_TEMPLATES,
        ACTION_ACC_VOUCHER_TEMPLATE_PROCESS,

        NA_ACTION_GET_ACC_MONTH_END_PROCESSES,
        ACTION_ACC_MONTH_END_PROCESS_SAVE,
        ACTION_ACC_YEAR_END_PROCESS_SAVE,
        NA_ACTION_GET_ACC_VOUCHERS_IN_DATE_RANGE,
        NA_ACTION_GET_ACC_LAST_YEAR_END_VOUCHER,

        NA_ACTION_GET_ACC_TRIAL_BALANCE_REPORT,
        NA_ACTION_GET_ACC_BALANCE_SHEET_REPORT,
        #endregion

        NA_ACTION_GET_USERS_BY_FILTERS,
        NA_ACTION_SYSTEMINFO_GET,

        NA_ACTION_GET_EMPLOYEE_FOR_SUCCESSION_REPORT,
        NA_ACTION_GET_EMP_SUMMARY_REPORT_GROUPWISE,

        ACTION_SEC_LOGIN_FLAG_SET,
        ACTION_SEC_USER_LOG_SAVE,
        NA_ACTION_GET_SEC_USER_LOG,
        ACTION_SEC_USER_DISCONNECT_FORCEFULLY,
        NA_ACTION_USER_LOGIN_FLAG_CHECK,
        ACTION_COMMON_ANNOUNCEMENT_SUCC_ADD,

        NA_ACTION_GET_RESIGNATION_REASONS,
        ACTION_RESIGNATION_REASON_ADD,
        ACTION_RESIGNATION_REASON_UPD,
        ACTION_RESIGNATION_REASON_DEL,

        ACTION_DOES_EMPLOYEE_NID_EXIST,

        ACTION_ATTACHMENT_UPDATE,
        ACTION_ATTACHMENT_DELETE,

        NA_GET_SEPARATION_APPROVAL_AUTHORITY,
        ACTION_SEPARATION_APPROVAL_PATH_CONFIG_SAVE,

        NA_GET_SALARY_WITHHOLD_INFO,
        ACTION_SALARY_WITHHOLD_INFO_SAVE,

        NA_ACTION_GET_EMPLOYEE_FOR_ADVANCETAX,
        ACTION_EMPLOYEE_ADVANCE_TAX_UPDATE,
        NA_ACTION_GET_EMP_TOTAL_REPORT_GROUPWISE,

        ACTION_GRADE_TAX_REBATE_UPDATE,

        ACTION_RELATIVE_INFORMATION_SAVE,
        ACTION_RELATIVE_INFORMATION_UPDATE,
        ACTION_RELATIVE_INFORMATION_DELETE,
        NA_ACTION_GET_EMP_RELATIVE_INFO,

        ACTION_OCCUPATION_ADD,
        ACTION_OCCUPATION_DELETE,
        ACTION_OCCUPATION_UPDATE,
        NA_ACTION_GET_OCCUPATION_LIST,

        ACTION_EMPLOYEE_PARKING_SAVE,
        ACTION_EMPLOYEE_PARKING_UPDATE,
        NA_ACTION_GET_EMPLOYEE_FROM_PARKING,

        #region SelfService
        ACTION_SELF_SRV_EMPLOYEE_ADD,
        NA_ACTION_GET_SELF_SRV_EMPLOYEE_LIST,
        ACTION_SELF_SRV_EMPLOYEE_APPROVE,

        ACTION_EMPLOYEE_JOB_HIST_PARKING_SAVE,
        NA_ACTION_GET_EMPLOYEE_JOB_HIST_PARKING,
        ACTION_EMPLOYEE_JOB_HIST_PARKING_APPROVE,
        #endregion

        NA_ACTION_GET_EMPLOYEE_PROFILE_INFO,

        ACTION_MAJOR_TOPICS_ADD,
        ACTION_MAJOR_TOPICS_DELETE,
        ACTION_MAJOR_TOPICS_UPDATE,
        NA_ACTION_GET_MAJOR_TOPICS_LIST,

        ACTION_TRAINING_ORGANIZER_ADD,
        ACTION_TRAINING_ORGANIZER_DELETE,
        ACTION_TRAINING_ORGANIZER_UPDATE,
        NA_ACTION_GET_TRAINING_ORGANIZER_LIST,

        ACTION_REFRENCE_EMPLOYEE_ADD,
        ACTION_REFRENCE_EMPLOYEE_DELETE,
        ACTION_REFRENCE_EMPLOYEE_UPDATE,
        NA_ACTION_GET_REFRENCE_EMPLOYEE_LIST,

        NA_ACTION_GET_PERSONAL_HISTORY_COUNT,
        NA_ACTION_GET_EMP_HISTORY_FOR_REPORT,

        NA_ACTION_GET_COMPANY_ORGANOGRAM,
        ACTION_ORGANOGRAM_NODE_SAVE,
        ACTION_ORGANOGRAM_NODE_DELETE,

        ACTION_EMP_ADDITIONAL_JOBHISTORY_ADD,
        ACTION_BF_GRADEWISE_ADD,
        NA_ACTION_GET_EMPLOYEE_FOR_MULTIPURPOSE,
        ACTION_LMS_BELANCE_UPDATE,

        NA_ACTION_GET_INFO_FOR_SELFSERVICE,

        NA_ACTION_GET_LIABILITY_NATURE,

        NA_ACTION_GET_BRANCH_ALLOWANCE_BY_PARAM,
        ACTION_BRANCH_ALLOWANCE_ADD,

        NA_ACTION_GET_EMPLOYEE_BY_LIABILITY_SCOPE,
        NA_ACTION_GET_GENERAL_INFORMATION,

        NA_ACTION_GET_BF_DATA_FOR_REPORT,
        NA_ACTION_GET_EMPLOYMENT_FUNCTION,
        NA_ACTION_GET_ALL_BRANCH_ALLOWANCE_SP,

        ACTION_EMP_SALARY_ARREAR_ADD,
        NA_ACTION_GET_ALLOWANCES_FOR_SALARY_ARREAR,
        NA_ACTION_GET_LAST_SALARY_PROCESS_DATE,

        ACTION_EMP_SALARY_ARREAR_DEL,
        NA_ACTION_GET_EMPLOYEE_FOR_ARREAR_SALARY,
        NA_ACTION_CHECK_IS_SALARY_REPROCESS_PENDING,
        NA_ACTION_GET_LEAVE_WITHOUT_PAY_SP,
        NA_ACTION_CHECK_IS_SALARY_PROCESS_PENDING,

        ACTION_DISCONTINUE_TYPE_ADD,
        ACTION_DISCONTINUE_TYPE_UPDATE,
        ACTION_DISCONTINUE_TYPE_DELETE,
        NA_ACTION_GET_DISCONTINUE_TYPE_LIST,

        ACTION_TRANSFER_TYPE_ADD,
        ACTION_TRANSFER_TYPE_UPDATE,
        ACTION_TRANSFER_TYPE_DELETE,
        NA_ACTION_GET_TRANSFER_TYPE_LIST,

        NA_ACTION_GET_CBS_UPLOAD_DATA,
        NA_ACTION_GET_CBS_UPLOAD_DATA_NRB,

        ACTION_OCS_SHARE_ADD,
        ACTION_OCS_SHARE_UPD,
        ACTION_OCS_SHARE_DEL,

        ACTION_SEC_USER_UPDATE_ON_LOGON,
        NA_ACTION_GET_LFA_ANALYSIS_RPT,

        NA_ACTION_GET_PAID_IN_CASH,
        NA_GET_BF_EMP_LIST_FOR_FUND_PAYMENT,

        ACTION_BF_FUND_PAYMENT,
        NA_ACTION_GET_EMP_INCREMENT_RPT,

        NA_ACTION_GET_LOAN_DATA_FOR_REPORT,

        NA_ACTION_GET_SALARY_REPORT_FOR_FIXED_HEAD,

        NA_ACTION_GET_EMP_WPPF_CERTIFICATE_RPT,

        ACTION_VIEW_LEAVE_APPROVAL_POLICY_ENTRY_UI,
        ACTION_LMS_LEAVE_APPROVAL_POLICY_SAVE,
        ACTION_LMS_LEAVE_APPROVAL_POLICY_DELETE,
        NA_ACTION_GET_LEAVE_APPROVAL_POLICY,

        NA_ACTION_GET_SALARY_SALARYDETAIL_LIST_SP,
        NA_ACTION_GET_LR_FOR_LEAVESANCTIONADVICE,
        
        NA_GET_SALARY_ACCOUNT_LIST,
        NA_GET_NEW_EMPLOYEE_LIST,

        ACTION_EMPLOYEE_PERSONAL_INFO_SAVE,
        NA_ACTION_GET_EMP_PERSONAL_INFO_TO_APPROVE,

        NA_ACTION_GET_LWP_DEDUCTION_SP,        
        NA_ACTION_EMPLOYEE_ARREAR_INFO_REPORT,
        NA_ACTION_GET_RETIREMENT_SALARY_BENEFIT_SP,
        ACTION_RETIREMENT_BENEFIT_POLICY_ADD,
        NA_ACTION_RETIREMENT_BENEFIT_POLICY,
        ACTION_EMPLOYEE_INFO_UPDATE_APPROVE,
        NA_ACTION_GET_PREV_SALARYDETAIL_SP,
        NA_ACTION_GET_ALLOWDEDUCTPARAM_ALL_SP,
        NA_ACTION_GET_EMPLOYEEALLOWDEDUCT_SP,
        NA_ACTION_GET_EMPLOYEE_SM,
        NA_ACTION_Q_TAXHISTORY_BY_SALARYYEAR_SM,
        NA_ACTION_GET_LWP_DEDUCTION_SM,

        //Quiz System :: Start :: Rony
        ACTION_QUIZQUESTION_ADD,
        ACTION_QUIZQUESTION_DEL,
        ACTION_QUIZQUESTION_UPD,
        NA_ACTION_GET_QUIZQUESTION_LIST,
        NA_ACTION_GET_EXITINTERVIEWINFO,

        ACTION_SEP_LIABILITY_QUESTIONNAIRE_ADD,
        ACTION_SEP_LIABILITY_QUESTIONNAIRE_DEL,
        NA_ACTION_GET_SEP_LIABILITY_QUESTION_LIST,

        NA_ACTION_SEP_LIABILITY_QUESTION_ANSWER,
        NA_ACTION_EXIT_INTERVIEW_ADD,
        //end
        NA_ACTION_SALARY_PROCESS_DATE,
        NA_ACTION_GET_USER_MENU_LIST,
        NA_GET_WITHHOLD_INFO,
        NA_ACTION_GET_EMP_PROMOTION_RPT,
        NA_ACTION_GET_EMP_FIXATION_RPT,

        ACTION_WITHHOLDACTION_ADD,
        ACTION_WITHHOLDACTION_DEL,
        ACTION_WITHHOLDACTION_UPD,
        NA_ACTION_GET_LEAVE_REQUEST_DETAILS,

        ACTION_DELEGATE_UPDATE,
        ACTION_VIEW_WITHHOLD_LIST_UI,
        NA_ACTION_GET_PENDING_APPROVAL_LEAVE,
        NA_ACTION_GET_COM_ORGANOGRAM_FOREXCELREPORT,

        NA_GET_EMPLOYEE_APPROVAL_PATH_CONFIG_INFO,
        ACTION_VIEW_LeaveRequestReport_UI,

        ACTION_VIEW_LeaveApprovalReport_NRB_UI,

        NA_ACTION_LMS_GET_BYPASS_APPROVER,
        ACTION_EMPLOYEE_APPROVAL_PATH_CONFIG,

        NA_ACTION_GET_ORGANOGRAM_MANPOWER_LIST,
        ACTION_ORGANOGRAM_MANPOWER_ADD,

        NA_ACTION_GET_EMPLIST_FORCARLOANINSTALLMENT,
        NA_ACTION_GET_ARREAR_LIST,
        ACTION_ARREAR_LIST_ADD,
        NA_ACTION_GET_INFORMATION_ANY,

        NA_ACTION_GET_EMPLOYEE_LIST_FOR_EMPLOYEE_PAGE,

        NA_ACTION_GET_LWP_ADJUSTMENT_SP,
        ACTION_SALARY_BY_EMPID_DEL,
        NA_ACTION_GET_EMPLOYEE_INFORMATION_SP,
        NA_ACTION_GET_EMPLOYEE_HIST_INFO_SP,

        ACTION_SYSTEM_MODULE_HIBERNATE_ADD,

        NA_ACTION_ADMINISTRATIVEROLE_BYMODULEID,

        ACTION_KPI_RANKING_ADD,
        NA_ACTION_GET_KPI_RANKING,

        ACTION_PA_KPI_MAPPING_DEPTWISE_ADD,
        ACTION_PA_KPI_MAPPING_EMPWISE_ADD,
        NA_ACTION_GET_KPI_MAPPING_INFO,
        NA_ACTION_GET_KPI_TARGET_SETUP,
        ACTION_KPI_GENERAL_TARGET_SAVE,
        ACTION_KPI_GENERAL_TARGET_DEL,

        NA_GET_KPI_APPROVAL_AUTHORITY,
        ACTION_KPI_APPROVAL_AUTHORITY_SAVE,

        NA_ACTION_GET_EMP_KPI_SCORING,
        ACTION_EMP_KPI_SCORING_SAVE,

        NA_ACTION_GET_EMPLOYEE_LIST_FOR_LEAVE_REPORT,
        ACTION_KPI_ITEM_CONFIG_BY_DEPT,

        ACTION_KPI_RECOMMENDATION_ADD,
        ACTION_KPI_RECOMMENDATION_UPD,
        NA_GET_ACTION_KPI_RECOMMENDATION,
        ACTION_KPI_RECOMMENDATION_DEL,

        NA_ACTION_GET_KPI_APPROVAL_INFO,
        ACTION_PA_KPI_APPROVAL_ADD,

        NA_ACTION_GET_KPI_SCORE_RECOMMENDATION,
        ACTION_KPI_SCORE_RECOMMENDATION_SAVE,

        NA_ACTION_GET_MANPOWER_DETAILS,
        NA_GET_ASSESSMENT_PERIOD,
        NA_GET_KPI_SCORING_AUTHORITY,
        ACTION_KPI_SCORIMG_AUTHORITY_SAVE,

        ACTION_KPI_ITEM_CONFIG_BY_EMP,

        NA_ACTION_GET_JOB_APPLICANT_DETAILS,
        NA_ACTION_GET_EMPLOYEE_LIST_FOR_LEAVE_ALLOCATED_WITHOUTPOLICY,
        ACTION_PA_KPI_CHECK,
        ACTION_LEAVE_ALLOCATED_WITHOUTPOLICY,

        ACTION_Site_Category_ADD,
        ACTION_Site_Category_UPDATE,
        ACTION_Site_Category_DELETE,
        NA_ACTION_GET_Site_Category_LIST,
        NA_ACTION_GET_SITE_BY_COMPANY,
        NA_ACTION_GET_ACCOMOTDATION_FILTER_BY_ALL,
        ACTION_ACCOMMODATION_PLANNING_SAVE,

        ACTION_KPI_PROMOTION_POLICY_ADD,
        NA_GET_KPI_PROMOTION_POLICY,
        ACTION_KPI_PROMOTION_POLICY_DELETE,
        NA_ACTION_GET_EMPLOYEE_BULK_DETAILS,
        ACTION_EMPLOYEE_INCREMENT_ADV,
        ACTION_EMPLOYEE_INCREMENT_ADV_UPDATE,

        NA_ACTION_GET_BONUSPOSTING_DATA,
        ACTION_CREATE_BONUSPOSTING,

        ACTION_DIVISION_LOCATION_ADD,
        ACTION_DIVISION_LOCATION_UPDATE,
        ACTION_DIVISION_LOCATION_DELETE,

        NA_ACTION_GET_PF_CURRENT_BALANCE,
        ACTION_BENEFIT_APPROVAL_AUTHOR_SAVE,
        NA_ACTION_GET_BENEFIT_APPROVAL_AUTHOR,
        ACTION_BENEFIT_CATEGORY_CONFIG_DELETE,

        // system action IDs: DB connection related action IDs
        ACTION_DB_CONNECTION_GET_Ora,
        ACTION_DB_CONNECTION_CLOSE_Ora,
        ACTION_DB_TRANSACTION_GET_Ora,
        ACTION_DB_TRANSACTION_COMMIT_Ora,
        ACTION_DB_TRANSACTION_ROLLBACK_Ora,

        ACTION_DB_CONNECTION_GET_PG,
        ACTION_DB_CONNECTION_CLOSE_PG,
        ACTION_DB_TRANSACTION_GET_PG,
        ACTION_DB_TRANSACTION_COMMIT_PG,
        ACTION_DB_TRANSACTION_ROLLBACK_PG,

        ACTION_View_Bonus_Report_UI,
        NA_ACTION_GET_DISBUSMENT_DATE, 
        NA_ACTION_GET_BONUS_DATA,
        NA_ACTION_GET_TRAINING_DATA,

        NA_ACTION_GET_EMPLOYEE_COST_DETAILS,
        NA_ACTION_GET_INFORMATION_ANY_PF,
        NA_ACTION_GET_CBS_Bonus_UPLOAD_DATA,
        NA_ACTION_GET_GRATUITYPROVISION_LIST_SP,

        ACTION_TR_TRAINER_COVERED_TRAINING,

        ACTION_EMPLOYEE_HISTORY_INFO_SEND,
        NA_ACTION_GET_EMPLOYEE_HISTORY_PARKING,

        ACTION_LOANISSUE_ADD_BULK,
        NA_ACTION_GET_EMPLOYEE_LOAN_SP,

        NA_ACTION_GET_PENDING_SEPARATION_REQUESTS,
        NA_ACTION_APPROVE_SEPARATION_REQUEST,
        NA_ACTION_GET_PAID_BONUS_SP,
        NA_ACTION_SEND_TEMP_PASSWORD_BY_EMAIL,

        NA_ACTION_ADD_SEP_LIABILITY_COLLECTION,
        NA_ACTION_SEP_LIABILITY_COLLECTION_REQ_SEND,
        NA_ACTION_EXIT_INTERVIEW_CALL,

        ACTION_UPDATE_BONUSPOSTING,
        ACTION_DELETE_BONUSPOSTING,

        NA_ACTION_GET_LOAN_BENEFIT_DATA,
        ACTION_EMP_INJURY_RECORD_ADD,
        NA_ACTION_GET_HEALTHSAFTY_DATA,
        NA_ACTION_GET_HEALTHSAFTY_VERIFIER,
        ACTION_HEALTHSAFTY_VERIFIER_ADD,
        ACTION_HEALTHSAFTY_APPROVAL,

        NA_ACTION_GET_TRAINING_DATA_FOR_REPORT,
        NA_ACTION_SEPARATION_GET_ACTIVITY_LIST_ALL,
        NA_ACTION_ACKNOWLEDGE_EMPLOYEE,

        NA_ACTION_SEPARATION_APPROVAL_HISTORY,

        NA_ACTION_GET_END_OF_SERVICE_BENEFIT,
        NA_ACTION_GET_FINAL_SETTLEMENT_REPORT,
        ACTION_RENEW_DATE_UPDATE,

        NA_ACTION_GET_JOB_USER_HISTORY_BY_TRACKINGID,
        NA_ACTION_GET_EMP_LIST_FOR_EMAIL_UPDATE,

        ACTION_EMP_RECRUITMENT_HIST_ADD,
        NA_ACTION_GET_GRATUITYPROVISION_BY_CODE,
        NA_ACTION_GET_JOB_APPLICANT_SHORTLISTING,
        NA_ACTION_GET_JOB_APPLICANT_SHORTLISTING_AD,
        ACTION_JOB_POSTING_RECRUITMENT,
        ACTION_DB_TRANSACTION_GET_REC,

        ACTION_JOB_EXAM_ORGANIZER,
        NA_ACTION_JOB_EXAM_ORGANIZER,
        ACTION_JOB_EXAM_ORGANIZER_DELETE,

        NA_ACTION_GET_TAXCERTIFICATE_FORMAT3,

        ACTION_JOB_EXAM_RESULT_UPLOAD,
        NA_ACTION_JOB_EXAM_RESULT_UPLOAD,
        ACTION_MIGRATE_APPLICANT_DATA_FROM_eRECRUITMENT,
        NA_ACTION_GET_TOTAL_APPLICANT_APPLAY_FOR_JOB,
        ACTION_DB_CONNECTION_GET_REC,
        NA_ACTION_GET_INFORMATION_ANY_TMS,
        NA_ACTION_GET_TRAINING_TOPIC_LIST,
        ACTION_TRAINING_TOPIC_CREATE,
        ACTION_TRAINING_TOPIC_DELETE,
        ACTION_TR_TOPIC_COVERED_TRAINING,
        NA_ACTION_GET_INFORMATION_ANY_LMS,
        ACTION_EMPLOYEE_ABSENCE_ADD,
        ACTION_EXAMHALL_INFO_ADD,
        ACTION_JOB_HISTORY_STATUS_UPDATE,
        NA_ACTION_GET_EXAM_INFO_ALL,
        NA_ACTION_GET_EMPLOYEE_LIST_REC,
        NA_ACTION_LMS_GET_ALL_LEAVE_QUOTA_HISTORY,
        NA_ACTION_DOES_ROLLNUMBER_EXIST,
        NA_ACTION_DOES_APPLICANT_DATA_EXIST_IN_EXAM_RESULT,
        NA_ACTION_GET_LEAVE_APPROVAL_REPORT,
        ACTION_CBS_PUSH_DATABASE_NRB,
        NA_ACTION_GET_EMP_ATTENDANCE_FOR_SALARY,
        ACTION_EMP_ATTENDANCE_FOR_SALARY,
        NA_ACTION_GET_ATTENDANCE_SHEET,
        NA_ACTION_GET_EMP_WORKSPACE_INFO,
        ACTION_DB_TRANSACTION_GET_CBS_ORA,
        NA_ACTION_GET_ATTENDANCE_STATUS_SLAB_INFO,
        ACTION_CANCEL_ATTENDANCE_ALLOW,
        ACTION_VIEW_IncrementReport_UI,
        NA_ACTION_GET_GROUPWISE_SALARY,
        NA_ACTION_GET_LIABILITY_ALLOWANCE_BY_PARAM,
        ACTION_BRANCH_LIABILITY_ALLOWANCE_ADD,
        NA_ACTION_GET_ALL_LIABILITY_ALLOWANCE_SP,
        NA_ACTION_GET_ALLOW_ATTENDANCE_SP,
        NA_ACTION_GET_ALLOW_ATTENDANCE_ALL_SP,
        NA_ACTION_GET_KPIRank_LIST,
        NA_ACTION_GET_Assesment_Period,
        NA_ACTION_GET_YEAR_WISE_PERFORMANCE_SCORE,
        NA_ACTION_GET_NONCOUNTABLE_LEAVELIST,
        NA_ACTION_LMS_GET_LEAVE_QUOTA_BY_EMPID_CFYEAR_PREVYEAR,
        NA_ACTION_LMS_GET_LEAVETYPE_LIST_BY_LOGONID_PREVYEAR,
        NA_ACTION_LMS_GET_LEAVE_LIST_BY_EMPID_CFYEAR_PREVYEAR,
        ACTION_ADVANCE_EVENT_ADD,
        ACTION_GRADE_CATEGORY_ADD,
        NA_ACTION_GET_GRADECATEGORY_LIST,
        ACTION_GRADE_CATEGORY_Update,
        ACTION_GRADE_CATEGORY_DELETE,
        ACTION_DOES_EMPLOYEE_BANK_ACCOUNT_EXIST,
        ACTION_SYSTEM_NOTIFICATION_CREATE,
        ACTION_SYSTEM_NOTIFICATION_LOG_CREATE,
        NA_ACTION_GET_SUSPECTED_TAXABLE_INCOME,
        ACTION_SUSPECTED_TAXABLE_INCOME_CREATE,
        ACTION_NEWS_ADD,
        NA_ACTION_NEWS_GET_ALL,
        ACTION_DISCIPLINARY_RULE_ADD,
        ACTION_NEWS_DELETE,

        ACTION_REQUISITION_APPROVAL_PATH_CONFIG,
        NA_GET_REQUISITION_APPROVAL_PATH_CONFIG_INFO,
        NA_ACTION_GET_REQUISITION_ACTIVITY_LIST_BY_LOGINUSER,


        //PastEmpTaxableIncome

        ACTION_PAST_EMP_TAXABLE_INCOME_CREATE,
        //NA_ACTION_GET_TAX_PARAM_ID_BY_FISCAL_YEAR,
        NA_ACTION_GET_ALLOWDEDUCT,
        ACTION_OVERTIME_CATEGORY_UPDATE,
        ACTION_EMPLOYEE_ATTENDANCE_UPLOAD,
        ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE,
        NA_ACTION_GET_ATTENDANCE_UPLOAD_PARKING,
        NA_ACTION_GET_SERIALNO,
        ACTION_EMPLOYEE_ATTENDANCE_PARKING_DELETE,

        NA_ACTION_GET_SHIFTING_GROUP_LIST,
        NA_ACTION_GET_SHIFTING_GROUP,
        ACTION_SHIFTING_GROUP_UPD,        
        ACTION_SHIFTING_GROUP_DEL,
        NA_ACTION_GET_SHIFTING_GROUP_DROPDOWN,
        ACTION_SHIFTING_GROUP_ADD,
        ACTION_DOES_GROUPNAME_EXIST,
        NA_ACTION_GET_MONTHLY_DEPERTMENTWISE_SALARY,

        NA_ACTION_GET_INTERNAL_MEMO,
        NA_ACTION_GET_EARNING_REPORT,
        NA_ACTION_GET_HEADCOUNT_VARIANCE,  //for Headcount Variance Report


        ACTION_HOLYDAY_TYPE_ADD,
        ACTION_HOLYDAY_TYPE_UPDATE,
        ACTION_HOLYDAY_TYPE_DELETE,
        NA_ACTION_HOLYDAY_TYPE_GET,
        NA_ACTION_GET_OVERTIME_CATEGORY,
        NA_ACTION_GET_HOLYDAY_TYPE_LIST,
        ACTION_BRANCH_WISE_HOLIDAY_ADD,
        NA_ACTION_GET_SITELIST_HOLIDAY,

        NA_ACTION_GET_EMPLOYEE_FOR_ASSIGNING_SHIFT,
        NA_ACTION_GET_EMPLOYEE_FOR_CHANGING_SHIFTPLAN,

        ACTION_ATTANDANCE_SUMMARY_ADD,
        NA_ACTION_GET_ATTANDANCE_ACTIVITY,
        NA_ACTION_GET_ATTANDANCE_SUMMARY_BYDATE_RANGE,
        NA_ACTION_GET_ATTANDANCE_SUMMARY_BYMONTH,
        NA_ACTION_GET_ATTANDANCE_STATUS_FOR_EMPLOYEE,
        NA_ACTION_GET_EMPLOYEE_INFO_FOR_ATT,
        NA_ACTION_GET_ATTANDANCE_SUMMARY_VALIDATE,
        NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO,
        ACTION_VIEW_ATTANDANCE_SUMMARY_APPROVE_UI,
        ACTION_ATTANDANCE_SUMMARY_UPDATE,
        ACTION_ATTANDANCE_APPROVAL_PATH_ADD,
        ACTION_ATTANDANCE_APPROVAL_PATH_GETMAX_SALARYDATE,
        


        NA_ACTION_GET_EMPLOYEE_OVERTIME_RECORD,
        NA_ACTION_GET_ATTENDANCE_MONTH,

        NA_ACTION_GET_ATTENDANCE_BYEMPID_SP,
        NA_ACTION_GET_ATTENDANCE_SP,

        NA_ACTION_SALARY_OUTSIDE_NOP_LIST,
        NA_ACTION_SALARY_OUTSIDE_MOP_LIST,
        NA_ACTION_BATCH_Q_ALL_OUTSIDE_SALARY_PROCESS,
        NA_ACTION_GET_OUTSIDE_SALARY_PROCESS_DATA_BY_EMP,
        ACTION_BATCH_CHANGED_OUTSIDE_SP_SAVE,
        ACTION_OUTSIDE_SALARY_BY_EMPID_DEL,

        ACTION_BATCH_SALARY_PROCESS_OUTSIDE,
        NA_ACTION_GET_EMPLOYEE_LIST_SP_OUTSIDE,
        NA_ACTION_GET_ALLOWDEDUCTPARAM_BYAD_SP,
        NA_ACTION_GET_EMPLOYEEALLOWDEDUCT_BYAD_SP,
        ACTION_OUTSIDE_PROCESS_INFO_SAVE,
        NA_ACTION_GET_EMPLOYEE_OUTSIDE_PAID_AMOUNT,
        NA_ACTION_GET_ALLOWDEDUCT_LIST_ALL,
        NA_ACTION_GET_EMPLOYEE_LIST_OUTSIDE_PAYROLL,

        ACTION_SHIFTING_PLAN_UPD,
        NA_ACTION_GET_EMPLOYEE_FOR_ADDITIONAL_SHIFTPLAN,
        ACTION_ADDITIONAL_SHIFTING_PLAN_ADD,
        NA_ACTION_LMS_GET_HOLIDAY_LIST_FOR_SITE,

        NA_ACTION_GET_ATT_SPAM_LIST,
        NA_ACTION_GET_ATT_SPAM_DATA,
        ACTION_SEND_ATT_SPAM_FOR_VERIFICATION,
        NA_ACTION_GET_ATT_SPAM_APPROVAL,
        NA_ACTION_GET_ATT_SPAM_DATA_FOR_MODIFY,
        ACTION_ATT_SPAM_PARKING_APPROVE,
        NA_ACTION_GET_HOIDAYS_BY_DATE,

        NA_ACTION_GET_PROPOSED_TRANSFER_EMPLOYEE_FOR_REPORT,
        NA_ACTION_GET_PROPOSED_TRANSFER_EMPLOYEE_INITIATOR,
        NA_GET_TRANSFER_INFORMATION_FOR_ALL,
        NA_ACTION_LMS_GET_LEAVE_REQ_STS_FOR_EMP_TRANSFER,
        NA_GET_SYSTEMINFO_CONF_FOR_EMP_TRANSFER,

        NA_ACTION_GET_LOGIN_USER,
        ACTION_SEND_ATT_SPAM_FOR_REJECTION,
        NA_ACTION_GET_COSTCENTER_WISE_BONUS,
        ACTION_VIEW_AllowanceDesignation_UI,
        NA_ACTION_GET_DESIGNATION_ALLOWANCE_BY_PARAM,
        ACTION_BRANCH_DESIGNATION_ALLOWANCE_ADD,
        ACTION_VIEW_AllowanceDepartment_UI,
        NA_ACTION_GET_DEPARTMENT_ALLOWANCE_BY_PARAM,
        ACTION_BRANCH_DEPARTMENT_ALLOWANCE_ADD,

        NA_ACTION_GET_SHIFTINGPLAN_EMPLOYEE_REPORT,
        NA_ACTION_GET_DEPARTMENT_LIST_BY_SITEID,

        NA_ACTION_GET_DAILY_ATTENDANCE_RPT,
        NA_GET_EMPLOYEE_APPROVAL_REQUEST_COUNT,  
        
        ACTION_YEAR_END,

        NA_ACTION_GET_DAILY_ATTENDANCE_FOR_EXCEPTION,
        ACTION_EXCEPTIONAL_ATTENDANCE_PARKING_CREATE,
        NA_ACTION_GET_EXCEPTIONAL_ATTENDANCE_PARKING,
        ACTION_EXCEPTIONAL_ATTENDANCE_UPDATE,
        ACTION_EXCEPTION_ATTENDANCE_PARKING_DELETE,
        NA_ACTION_GET_PARKING_SERIALNO,

        NA_ACTION_GET_GRATUITY_PARAM_RULE,
        NA_ACTION_GET_GRATUITYPARAMETER,
        NA_ACTION_GET_ALL_DESIGNATION_ALLOWANCE_SP,
        NA_ACTION_GET_ALL_DEPARTMENT_ALLOWANCE_SP,


        NA_ACTION_GET_PF_ACCOUNT_HEADS,
        ACTION_PF_ACCOUNT_HEAD_CREATION,
        ACTION_PF_ACCOUNT_HEAD_DELETE,
        ACTION_PF_GET_ACCOUT_HEAD_BYID,
        ACTION_PF_GET_DAILY_STATEMENT,
        NA_GET_PF_FUND_HEAD_LIST_FOR_EMP_JOURNAL,
        NA_ACTION_GET_EMPLOYEE_FOR_ABSENT_SUMMERY,
        NA_ACTION_GET_ADDITIONAL_SHIFTINGPLAN,
        NA_ACTION_GET_ADDITIONAL_SHIFT_EXIST,
        ACTION_ADDITIONAL_SHIFTING_PLAN_UPD,
        ACTION_ADDITIONAL_SHIFTING_PLAN_DEL,
        NA_ACTION_GET_ATT_DEVICE_DATA,


        ACTION_ATTENDANCE_FOR_SALARY_UPLOAD_DATA,
        ACTION_ATTENDANCE_FOR_SALARY_UPLOAD_DATA_APPROVE,
        ACTION_ATTENDANCE_FOR_SALARY_UPLOAD_DATA_REJECT,


        NA_ACTION_GET_CBS_BONUS_DATA,
        NA_ACTION_GET_EMPLOYEE_BULK_DETAIL,
        ACTION_EMPLOYEE_DISCONTINUE_BULK,
        NA_ACTION_GET_EMPLOYEE_TO_BULK_LEAVEASSIGN,

        NA_ACTION_GET_BANKBRANCH_BY_BANKCODE,
        NA_ACTION_GET_LOCATION_LIST_BY_COMPANY_CODE,
        NA_ACTION_GET_SITE_LIST_BY_LOCATION_CODE,
        NA_ACTION_GET_COMPANYDIVISION_LIST_BY_SITE_CODE,
        NA_ACTION_GET_DEPARTMENT_LIST_BY_COMPANYDIVISION_ID,
        NA_ACTION_GET_FUNCTION_LIST_BY_DEPARTMENT_CODE,
        NA_ACTION_GET_DISTRICT_BY_COUNTRYID,
        ACTION_SET_ALLOW_DEDUCT_ORDER,
        NA_ACTION_GET_ALLOWDEDUCTLISTWITH_LOAN,
        NA_ACTION_GET_LAST_MONTH_JOB_HISTORY,

    }
}

