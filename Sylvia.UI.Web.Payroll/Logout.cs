using System;
using System.Data;
using Sylvia.Common;
using System.Security.Principal;

/// <summary>
/// Summary description for Logout
/// </summary>
namespace Sylvia.UI.Web.Payroll
{
  public class Logout
  {
    public Logout()
    {
      //
      // TODO: Add constructor logic here
      //
    }
    static public void DoLogout()
    {
      AppLogger.LogInfo("DoLogout");


      if (HttpContext.Current.User.Identity.IsAuthenticated == false)
      {
        return;
      }

      string sloginId = HttpContext.Current.User.Identity.Name;


      ActionDS actionDS = new ActionDS();
      DataStringDS stringDS = new DataStringDS();
      stringDS.DataStrings.AddDataString(sloginId);
      stringDS.AcceptChanges();

      actionDS.Actions.AddAction(ActionID.NA_ACTION_SEC_USER_LOGOUT.ToString());

      DataSet requestDS = new DataSet();

      requestDS.Merge(actionDS);
      requestDS.Merge(stringDS);
      DataSet responseDS = BrokerUI.Request(requestDS);
      ErrorDS errDS = new ErrorDS();
      errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);

      if (errDS.Errors.Count > 0)
      {

      }
      else
      {
        FormsAuthentication.SignOut();
        
        FormsAuthenticationTicket authTicket = null;
        GenericIdentity identity = new GenericIdentity("", "");
      
        // This principal will flow throughout the request.
        GenericPrincipal principal =  new GenericPrincipal(identity, new string[]{});

        // Attach the new principal object to the current HttpContext object
        HttpContext.Current.User = principal;

        LoginUserList.RemoveUser(sloginId.Trim().ToLower());    // Wali :: 24-Aug-2016
      
      }
    }
  
  }
}