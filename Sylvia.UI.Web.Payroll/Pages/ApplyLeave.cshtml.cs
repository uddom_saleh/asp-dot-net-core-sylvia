﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Sylvia.Common;

namespace Sylvia.UI.Web.Payroll.Pages
{
    public class ApplyLeaveModel : PageModel
    {



        public DateTime GetSysDateTimeFromDB()
        {
            try
            {
                DataDateDS sysDateDS = UtilUI.GetSystemDate();

                if (sysDateDS != null && sysDateDS.DataDates.Rows.Count > 0)
                {
                    //ViewState["SysDate"] = sysDateDS.DataDates.Rows[0]["DateValue"]; //Jarif 28 April 12.
                    return Convert.ToDateTime(sysDateDS.DataDates.Rows[0]["DateValue"]);
                }
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
            }
            return DateTime.Today;
        }

        public bool _IsValidDate(string strDate)
        {
            try
            {
                DateTime date = Convert.ToDateTime(strDate);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool _IsValidDecimal(string strNumber)
        {
            try
            {
                decimal num = Convert.ToDecimal(strNumber);
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void GetEmployeeHistory()
        {
            #region Local Variables Declaration...
            lmsHolidayDS.HolidaysDataTable holidayTable = new lmsHolidayDS.HolidaysDataTable();
            DataRow[] weekendRow;
            lmsHolidayDS.WeekendsDataTable weekendTable;
            lmsLeaveQuotaDS.ELQDetailsRow[] foundRows;
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
            int leaveTypeID = 0;
            string sExpr = "";
            bool isCalendarDay = false;
            #endregion

            #region Get Value From ViewState & Combo...
            UserDS.UsersDataTable userInfo = new UserDS.UsersDataTable();
            //if (ViewState["UserTable"] != null) userInfo = (UserDS.UsersDataTable)ViewState["UserTable"];

            //if (ViewState["LeaveQuotaDS"] != null) leaveQuotaDS = (lmsLeaveQuotaDS)ViewState["LeaveQuotaDS"];
            //leaveTypeID = (cboLeaveType.SelectedValue != "") ? Convert.ToInt32(cboLeaveType.SelectedValue) : 0;
            //holidayTable = UtilUI.GetLMSHolidayListAll().Holidays;
            //holidayTable = UtilUI.GetLMSHolidayListForSite(userInfo[0].SiteID).Holidays;
            holidayTable = UtilUI.GetLMSHolidayListForSite(34097).Holidays;
            weekendTable = UtilUI.GetLMSWeekendList().Weekends;
            #endregion

            #region Get DaysParking, DaysRemain, ELQID, LCDetails...
            //txtDaysRemain.Text = "0.00";
            //txtDaysParking.Text = "0.00";
            //hdnELQID.Value = "0";
            //hdnMaxLeaveAvail_At_ATime.Value = "0";
            //hdnMinLeaveAvail_At_A_Time.Value = "0";
            //ViewState["LCDetailsID"] = null;

            if (leaveQuotaDS.ELQDetails.Rows.Count > 0)
            {
                var _hdnELQIDValue = leaveQuotaDS.ELQDetails.Rows[0]["ELQID"].ToString();

                sExpr = " LeaveTypeID = " + leaveTypeID;
                foundRows = (lmsLeaveQuotaDS.ELQDetailsRow[])leaveQuotaDS.ELQDetails.Select(sExpr);
                if (foundRows.Length > 0)
                {
                    //chkApplyLFA.Enabled = foundRows[0].LFA_Applicable;

                    //txtDaysRemain.Text = (foundRows[0].CalculatedDaysAllotted == -1) ? "UL" : foundRows[0].LeaveRemain.ToString();
                    ///hdnDaysRemain.Value = foundRows[0].LeaveRemain.ToString();
                    //if (!foundRows[0].IsLeavePendingNull()) txtDaysParking.Text = foundRows[0].LeavePending.ToString();

                    //if (!foundRows[0].IsLCDetailsIDNull()) ViewState["LCDetailsID"] = foundRows[0].LCDetailsID.ToString();
                    //hdnMaxLeaveAvail_At_ATime.Value = foundRows[0].MaxLeaveAt_ATime.ToString();
                    //hdnMinLeaveAvail_At_A_Time.Value = foundRows[0].MinLeaveAvail_At_A_Time.ToString();
                    //hdnIsCalendarDay.Value = foundRows[0].IsCalendarDay.ToString();
                    isCalendarDay = foundRows[0].IsCalendarDay;
                    //if (!foundRows[0].IsLeaveTakenNull()) hdnLeaveTaken.Value = foundRows[0].LeaveTaken.ToString();

                    //if (!foundRows[0].IsPassportExpireDateNull()) hdnPassportExDate.Value = foundRows[0].PassportExpireDate.ToString("dd-MMM-yyyy");
                }
            }
            #endregion

            #region Get Weekend and Holiday...
            if (!isCalendarDay)
            {
                if (weekendTable == null) weekendTable = new lmsHolidayDS.WeekendsDataTable();

                string strWeekendDay = "";
                sExpr = " DayLengthID = " + Convert.ToInt32(LeaveActivities.Approve);
                //sExpr += " AND SiteID = " + Convert.ToInt32(hdnApplicantSiteID.Value);
                weekendRow = weekendTable.Select(sExpr);

                for (int i = 0; i < weekendRow.Length; i++)
                {
                    strWeekendDay += weekendRow[i]["NameOfDay"] + ",";
                }

                //hdnNameOfDay.Value = strWeekendDay;
                //ViewState["NonShiftWeekendDay"] = strWeekendDay;

                string strHolidays = "";

                foreach (lmsHolidayDS.HolidaysRow row in holidayTable.Rows)
                {
                    strHolidays += row.HoliDate.ToString("M-d-yyyy") + ",";
                }

                //hdnHoliday.Value = strHolidays;
            }
            else
            {
                //hdnNameOfDay.Value = "0";
                //hdnHoliday.Value = "0";
                //ViewState["NonShiftWeekendDay"] = null;
            }

            #endregion

            //if (txtDaysRemain.Text == "0.00")
            //{
            //    lnkSave.Enabled = lnkSaveB.Enabled = false;
            //    lblMessage.Text = "No allocation is available for this leave type";
            //}
            //else
            //{
            //    lnkSave.Enabled = lnkSaveB.Enabled = true;
            //    lblMessage.Text = "";
            //}
        }

        private bool HaveDependencyLeaveBalance(Int32 pLeaveTypeID)
        {
            bool vHaveBalance = false;
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            //if (ViewState["LeaveQuotaDS"] != null) 
            //    leaveQuotaDS = (lmsLeaveQuotaDS)ViewState["LeaveQuotaDS"];
            //else 
            leaveQuotaDS = UtilUI.GetLeaveQuotaHist_CFYear(Convert.ToInt32(1809539));

            foreach (lmsLeaveQuotaDS.ELQDetailsRow row in leaveQuotaDS.ELQDetails.Rows)
            {
                if (row.LeaveTypeID == pLeaveTypeID && row.LeaveRemain > 0) { vHaveBalance = true; break; }

            }

            return vHaveBalance;
        }

        private bool LeaveApplicableOnlyForHoliday(Int32 pLeaveTypeID)
        {
            bool isForHoliday = false;

            lmsLeaveDS leaveDS = new lmsLeaveDS();
            //leaveDS = (lmsLeaveDS)ViewState["leaveDSForAppForHoliday"];

            foreach (lmsLeaveDS.LeaveTypeRow row in leaveDS.LeaveType.Rows)
            {
                if (row.LeaveTypeID == pLeaveTypeID && row.ApplicableForHoliday == true) { isForHoliday = true; break; }
            }

            return isForHoliday;
        }

        private void LoadLeaveTypeCombo()
        {
            //ViewState["LeaveType"] = null;
            lmsLeaveDS leaveDS = UtilUI.GetLMSLeaveTypeListByLoginID("6880");

            if (leaveDS != null)
            {
                //ViewState["LeaveType"] = leaveDS.LeaveType;

                //cboLeaveType.DataTextField = "LeaveTypeName";
                //cboLeaveType.DataValueField = "LeaveTypeID";

                foreach (lmsLeaveDS.LeaveTypeRow row in leaveDS.LeaveType.Rows)
                {
                    if (row.DependOnLeavePlan) row.Delete();
                    else if (!row.IsDependencyLeaveTypeIDNull() && HaveDependencyLeaveBalance(row.DependencyLeaveTypeID)) row.Delete();
                }
                leaveDS.LeaveType.AcceptChanges();

                #region Update :: WALI :: 11-Aug-2015
                lmsLeaveDS.LeaveTypeRow newRow = leaveDS.LeaveType.NewLeaveTypeRow();
                newRow.LeaveTypeName = "Select a leave type";
                newRow.LeaveTypeID = 0;
                newRow.DependOnLeavePlan = false;
                leaveDS.LeaveType.AddLeaveTypeRow(newRow);
                #endregion

                //cboLeaveType.DataSource = leaveDS.LeaveType;
                //cboLeaveType.DataBind();

                //cboLeaveType.SelectedIndex = cboLeaveType.Items.Count - 1;  // WALI :: 11-Aug-2015

                //ViewState["leaveDSForAppForHoliday"] = leaveDS;
            }

            //if (cboLeaveType.Items.Count == 0) lblLeaveTypeError.Text = DictionaryUI.MSG_ITEMS_NOT_FOUND;
            //else lblLeaveTypeError.Text = "";
        }

        private void LoadCertificateCombo()
        {
            CertificateDS certDS = UtilUI.GetCertificateList();
            if (certDS != null)
            {
                CertificateDS.CertificateRow row = certDS.Certificate.NewCertificateRow();
                row.Name = "Select a Reference Type";
                row.CertificateID = 0;
                certDS.Certificate.AddCertificateRow(row);

                //certDS.Certificate.AddCertificateRow(0, null, "Select a Type", false);
                //cboCertificateType.DataTextField = "Name";
                //cboCertificateType.DataValueField = "CertificateID";
                //cboCertificateType.DataSource = certDS.Certificate;
                //cboCertificateType.DataBind();
                //cboCertificateType.SelectedIndex = cboCertificateType.Items.Count - 1;
            }

        }

        private void LoadAdjustmentLeaveCombo()
        {
            try
            {
                //txtDaysRemain_Adj.Text = "0.00";
                //hdnDaysRemain_Adjustment.Value = "0";
                //if (cboLeaveType.Items.Count == 0) return;

                #region Build Table...
                //48236
                Int32 masterLeaveID = Convert.ToInt32(48236);
                lmsLeaveDS.LeaveAdjustmentHeadDataTable LAHT = new lmsLeaveDS.LeaveAdjustmentHeadDataTable();
                lmsLeaveDS.LeaveAdjustmentHeadDataTable new_LAHT = new lmsLeaveDS.LeaveAdjustmentHeadDataTable();
                //if (ViewState["AdjustmentHeadDS"] != null) LAHT = (lmsLeaveDS.LeaveAdjustmentHeadDataTable)ViewState["AdjustmentHeadDS"];

                lmsLeaveDS.LeaveAdjustmentHeadRow[] foundRows = (lmsLeaveDS.LeaveAdjustmentHeadRow[])LAHT.Select("MasterLeaveID = " + masterLeaveID);

                lmsLeaveDS.LeaveAdjustmentHeadRow sRow = new_LAHT.NewLeaveAdjustmentHeadRow();
                sRow.AdjustmentLeaveID = 0;
                sRow.AdjustmentLeaveName = "Select an Adjustment Leave";
                sRow.Max_Adjustment = 0;
                new_LAHT.AddLeaveAdjustmentHeadRow(sRow);

                foreach (lmsLeaveDS.LeaveAdjustmentHeadRow row in foundRows)
                {
                    lmsLeaveDS.LeaveAdjustmentHeadRow nRow = new_LAHT.NewLeaveAdjustmentHeadRow();

                    nRow.AdjustmentLeaveID = row.AdjustmentLeaveID;
                    nRow.AdjustmentLeaveName = row.AdjustmentLeaveName;
                    nRow.Max_Adjustment = row.Max_Adjustment;

                    new_LAHT.AddLeaveAdjustmentHeadRow(nRow);
                }
                new_LAHT.AcceptChanges();

                //ViewState["AdjustmentHead"] = new_LAHT;
                #endregion

                if (new_LAHT != null)
                {
                    //cboAdjustmentLeaveType.DataTextField = "AdjustmentLeaveName";
                    //cboAdjustmentLeaveType.DataValueField = "AdjustmentLeaveID";
                    //cboAdjustmentLeaveType.DataSource = new_LAHT;
                    //cboAdjustmentLeaveType.DataBind();
                }

                //if (new_LAHT.Rows.Count == 0) lblAdjustmentLeaveTypeError.Text = DictionaryUI.MSG_ITEMS_NOT_FOUND;
                //else lblAdjustmentLeaveTypeError.Text = "";
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                //lblMessage.Visible = true;
            }
        }

        public void OnGet()
        {
            //#region For Loading Permission Checking...
            //ActionDS LPC_ActionDS = new ActionDS();
            //LPC_ActionDS.Actions.AddAction(ActionID.ACTION_VIEW_ApplyLeave_UI.ToString());
            //LPC_ActionDS.AcceptChanges();

            //DataSet LPC_RequestDS = new DataSet();
            //LPC_RequestDS.Merge(LPC_ActionDS);
            //LPC_RequestDS.AcceptChanges();

            //BrokerUI.Request(LPC_RequestDS);
            //#endregion

            //hdnServerDateTime.Value = UtilUI.GetSystemDate().DataDates[0].DateValue.ToString("dd-MMM-yyyy");
            string _hdnServerDateTime = UtilUI.GetSystemDate().DataDates[0].DateValue.ToString("dd-MMM-yyyy");

            //txtFromDate.ForeColor = Color.Gray;
            //txtToDate.ForeColor = Color.Gray;

            //txtFromDate.Text = "DD-MMM-YYYY";
            //txtToDate.Text = "DD-MMM-YYYY";

            //txtFromDate.Attributes.Add("onfocus", "javascript: txtBox_onFocus('" + txtFromDate.ClientID + "','DD-MMM-YYYY')");
            //txtToDate.Attributes.Add("onfocus", "javascript: txtBox_onFocus('" + txtToDate.ClientID + "','DD-MMM-YYYY')");

            //txtCountry_More.Visible = lblMoreCountries.Visible = false;

            #region Add Controls Attribute

            //string strControlIDs = "";

            //strControlIDs = "'" + txtFromDate.ClientID + "'";
            //strControlIDs += ",'" + txtToDate.ClientID + "'";
            //strControlIDs += ",'" + txtTotalDays.ClientID + "'";
            //strControlIDs += ",'" + lblEqual2.ClientID + "'";
            //strControlIDs += ",'" + lblDays.ClientID + "'";
            //strControlIDs += ",'DD-MMM-YYYY'";
            //strControlIDs += ",'" + cboFromTimeHH.ClientID + "'";
            //strControlIDs += ",'" + cboFromTimeMM.ClientID + "'";
            //strControlIDs += ",'" + cboToTimeHH.ClientID + "'";
            //strControlIDs += ",'" + cboToTimeMM.ClientID + "'";
            //strControlIDs += ",'" + txtTotalTime.ClientID + "'";
            //strControlIDs += ",'" + hdnTotalDay.ClientID + "'";
            //strControlIDs += ",'" + hdnTotalHours.ClientID + "'";
            ////Update by Asif, 13 Feb 2012
            //strControlIDs += ",'" + hdnHoliday.ClientID + "'";
            //strControlIDs += ",'" + hdnNameOfDay.ClientID + "'";
            ////Update End
            ////Rony :: 02 Dec 2016
            //strControlIDs += ",'" + hdnLeaveTypePolicy.ClientID + "'";
            ////End
            //txtFromDate.Attributes.Add("onblur", "javascript: DateDiff(" + strControlIDs + ")");
            //txtToDate.Attributes.Add("onblur", "javascript: DateDiff(" + strControlIDs + ")");

            //strControlIDs = "'" + cboFromTimeHH.ClientID + "'";
            //strControlIDs += ",'" + cboFromTimeMM.ClientID + "'";
            //strControlIDs += ",'" + cboToTimeHH.ClientID + "'";
            //strControlIDs += ",'" + cboToTimeMM.ClientID + "'";
            //strControlIDs += ",'" + txtTotalTime.ClientID + "'";
            //strControlIDs += ",'" + lblEqual2.ClientID + "'";
            //strControlIDs += ",'" + txtTotalDays.ClientID + "'";
            //strControlIDs += ",'" + lblDays.ClientID + "'";
            //strControlIDs += ",'" + hdnTotalHours.ClientID + "'";
            //strControlIDs += ",'" + hdnTotalDay.ClientID + "'";


            //cboFromTimeHH.Attributes.Add("onblur", "javascript: cboBox_onBlur(" + strControlIDs + ")");
            //cboFromTimeMM.Attributes.Add("onblur", "javascript: cboBox_onBlur(" + strControlIDs + ")");
            //cboToTimeHH.Attributes.Add("onblur", "javascript: cboBox_onBlur(" + strControlIDs + ")");
            //cboToTimeMM.Attributes.Add("onblur", "javascript: cboBox_onBlur(" + strControlIDs + ")");

            #endregion

            #region File Attachment :: WALI :: 19-Feb-2015
            //try
            //{
            //    if (Session["ClientName"].ToString() == "Lafarge")
            //    {
            //        rowAttach.Visible = tblAttachment.Visible = false;
            //    }
            //}
            //catch
            //{
            //}

            //lnkAttach.Text = "+";
            //tblAttachment.Visible = false;
            //ViewState["LMS_FileAttachment"] = null;
            LoadCertificateCombo();

            //EmployeeHistDS atchDS = new EmployeeHistDS();
            //grdItemList_File.DataSource = atchDS.Attachment;
            //grdItemList_File.DataBind();
            #endregion

            try
            {
                #region Get User Data From DB: Jarif, 16-Dec-13...
                //UserDS.UsersDataTable userTable = UtilUI.GetUserByLoginID(this.Context.User.Identity.Name,
                //                                                          Convert.ToInt32(Supervisor_SearchParam.Leave));
                UserDS.UsersDataTable userTable = UtilUI.GetUserByLoginID("6880",
                                                                         Convert.ToInt32(Supervisor_SearchParam.Leave));

                //ViewState["UserTable"] = userTable;
                //hdnEmployeeLoginID.Value = userTable[0].LoginId;
                //hdnEmployeeLoginID.Value = userTable[0].LoginId;

                if (userTable != null && userTable.Rows.Count > 0)
                {
                    if (userTable[0].EmpStatus == Convert.ToInt32(DiscontinueType.Suspend))
                    {
                        EmployeeDS.SuspendRule_GeneralDataTable suspend_G = UtilUI.getSuspendRule_General_All();
                        if (suspend_G[0].LeaveApply_held)
                        {
                            //cboLeaveType.Enabled = false;
                            //lblMessage.Text = "Leave held due to suspended.";
                            //lblMessage.ForeColor = Color.Red;
                            return;
                        }
                    }

                    //hdnEmployeeCode.Value = userTable[0].EmployeeCode;
                    //hdnEmployeeID.Value = userTable[0].EMPLOYEEID.ToString();
                    //hdnUserID.Value = userTable[0].userid.ToString();
                    //hdnDepartmentID.Value = (!userTable[0].IsDepartmentIDNull()) ? userTable[0].DepartmentID.ToString() : "0";
                    //hdnUserEmail.Value = (!userTable[0].IsEMAILNull()) ? userTable[0].EMAIL : "0";
                    //hdnEmailPassword.Value = (!userTable[0].IsEmailPasswordNull()) ? userTable[0].EmailPassword : "";
                    //hdnSupervisorEmpID.Value = (!userTable[0].IsS_DelegatedEmployeeIDNull()) ? userTable[0].S_DelegatedEmployeeID.ToString() : userTable[0].SupervisorID.ToString();
                    //hdnSupervisorEmail.Value = (!userTable[0].IsS_DelegateUserEmailNull()) ? userTable[0].S_DelegateUserEmail : (!userTable[0].IsSUPERVISOREMAILNull()) ? userTable[0].SUPERVISOREMAIL : " ";
                    //hdnEmployeeName.Value = userTable[0].EmployeeName;
                    //hdnApplicantSiteID.Value = (!userTable[0].IsSiteIDNull()) ? userTable[0].SiteID.ToString() : "0";
                    //hdnSupervisorName.Value = (!userTable[0].IsSupervisorNameNull()) ? userTable[0].SupervisorName : userTable[0].EmployeeName;
                    //hdnSupervisorCode.Value = (!userTable[0].IsS_DelegateUserCodeNull()) ? userTable[0].S_DelegateUserCode : userTable[0].SupervisorCode;

                    //hdnSDelegatedUserID.Value = (!userTable[0].IsS_DelegatedUserIDNull()) ? Convert.ToString(userTable[0].S_DelegatedUserID) : "0";
                    //hdnSDelegatedEmpID.Value = (!userTable[0].IsS_DelegatedEmployeeIDNull()) ? Convert.ToString(userTable[0].S_DelegatedEmployeeID) : "0";
                    //hdnSDelegatedUserEmail.Value = (!userTable[0].IsS_DelegateUserEmailNull()) ? userTable[0].S_DelegateUserEmail : " ";
                    //hdnSiteID.Value = (!userTable[0].IsSiteIDNull()) ? Convert.ToString(userTable[0].SiteID) : "0";

                    //hdnS_DelegateEmpCode.Value = (!userTable[0].IsS_DelegateUserCodeNull()) ? userTable[0].S_DelegateUserCode : "0";
                    //hdnS_DelegateEmpName.Value = (!userTable[0].IsS_DelegateUserNameNull()) ? userTable[0].S_DelegateUserName : "0";

                    //Jarif, 30Mar16
                    //hdnCCEmail.Value = (!userTable[0].IsCcEmailAddressNull()) ? userTable[0].CcEmailAddress : " ";
                    //hdnCCMailMode.Value = (!userTable[0].IsCCMailModeNull()) ? Convert.ToString(userTable[0].CCMailMode) : "1";
                    //hdnDayForCCMail.Value = (!userTable[0].IsDayForCCMailNull()) ? Convert.ToString(userTable[0].DayForCCMail) : "0";

                    //chkShiftEmployee.Visible = userTable[0].ShiftEmployee;
                    //lblSupervisorName.Text = "Recipient : " + userTable[0].SupervisorCode + " - " + userTable[0].SupervisorName;
                    //if (!userTable[0].IsS_DelegateUserCodeNull())
                    //{
                    //    string s_Delegate = String.Format(" / {0} - {1} [Delegated]", userTable[0].S_DelegateUserCode, userTable[0].S_DelegateUserName);
                    //    lblSupervisorName.Text += s_Delegate;
                    //}
                }
                #endregion

                //ViewState["NonShiftWeekendDay"] = null;
                //ViewState["TotalWeekend"] = null;
                //ViewState["DeductableLeaveTypeID"] = null;
                #region Validation...
                DateTime currDate = GetSysDateTimeFromDB();
                lmsLeaveQuotaDS leaveQuotaDS = UtilUI.GetLeaveQuotaHist_CFYear(Convert.ToInt32(1809539));
                //ViewState["LeaveQuotaDS"] = leaveQuotaDS;

                if (leaveQuotaDS.ELQDetails.Rows.Count > 0)
                {
                    lmsLeaveQuotaDS.ELQDetailsRow foundRow = (lmsLeaveQuotaDS.ELQDetailsRow)leaveQuotaDS.ELQDetails.Rows[0];

                    if (currDate > foundRow.EffectToMonthYear.AddHours(23))
                    {
                        //lblMessage.Text = "No allocation is available  for this year. you may contact with your administrator.";
                        return;
                    }
                }
                else
                {
                    //lblMessage.Text = "No allocation is available  for this year. you may contact with your administrator.";
                    return;
                }
                #endregion

                //ViewState["leaveTable"] = UtilUI.GetLeaveList_CFYear(Convert.ToInt32(1809539));
                var _lmsLeaveds = UtilUI.GetLeaveList_CFYear(Convert.ToInt32(1809539));

                LoadLeaveTypeCombo();
                GetEmployeeHistory();

                //For Shift Employee...
                var _mHolidayDS = UtilUI.GetLMSWeekendList();
                //fillGrid();
                //End

                lmsLeaveDS LAHDS = new lmsLeaveDS();
                LAHDS = UtilUI.GetLAHC_All();
                //ViewState["AdjustmentHeadDS"] = LAHDS.LeaveAdjustmentHead;
                //ViewState["AdjustmentHead"] = null;
                LoadAdjustmentLeaveCombo();
                LoadCountry();

                LoadUserCombo();
                LoadDepartmrntCombo();

                // delegate flexibility start
                    ///hdnDelegateFlexibility.Value = userTable[0].DelegateFlexibility.ToString();
                    //ViewState["DelegateUserDS"] = null;

                    //string userRoleName = "";
                    //if (Session["UserRoleName"] != null) userRoleName = Convert.ToString(Session["UserRoleName"]);

                    //if (Convert.ToBoolean(hdnDelegateFlexibility.Value) == true || this.Context.User.Identity.Name == "admin" || userRoleName == "admin")
                    //{
                    //    LoadDepartmrntCombo();
                    //    cboDepartment.SelectedValue = (userTable[0].DepartmentCode);
                    //}
                    // delegate flexibility end
                //else
                //{
                
                //    //cboDepartment.SelectedValue = (userTable[0].DepartmentCode);
                //    //cboDepartment.Enabled = false;
                //}
                //LoadDelegateCombo();
                //if (userTable[0].LMS_PathFlexibility) lblNewRecipient.Visible = txtSupervisorEmpCode.Visible = btnSet.Visible = true;
            }
            catch (Exception ex)
            {
                //lblMessage.Text = ex.Message;
                //lblMessage.Visible = true;
            }
        }

        public void LoadUserCombo()
        {
            //lblErrDelegateUser.Text = "";

            #region Local Variable Declaration and get Data...
            UserDS.UsersDataTable user = UtilUI.GetUserByLoginID("6880");
            UserDS userDS = UtilUI.GetUsertListForDelegate("6880", "6880");
            //ViewState["DelegateUserDS"] = userDS;

            string senderEmpName = "", senderEmpCode = "", deptCode = "0", fromEmailAddress = "";
            Int32 DelegatedUserID = 0, CompID = 0, SiteID = 0, DivID = 0;

            if (user.Rows.Count > 0)
            {
                if (!user[0].IsDepartmentCodeNull()) deptCode = user[0].DepartmentCode;
                if (!user[0].IsDelegatedUserIDNull()) DelegatedUserID = user[0].DelegatedUserID;
                if (!user[0].IsEmployeeNameNull()) senderEmpName = user[0].EmployeeName;
                if (!user[0].IsEmployeeCodeNull()) senderEmpCode = user[0].EmployeeCode;
                if (!user[0].IsEMAILNull()) fromEmailAddress = user[0].EMAIL;

                if (!user[0].IsCompanyIDNull()) CompID = user[0].CompanyID;
                if (!user[0].IsSiteIDNull()) SiteID = user[0].SiteID;
                if (!user[0].IsCompanyDivisionIDNull()) DivID = user[0].CompanyDivisionID;

            }

            //Session["SenderEmpName"] = senderEmpName;
            //Session["SenderEmpCode"] = senderEmpCode;
            //Session["FromEmailAddress"] = fromEmailAddress;

            #endregion

            string Expr = "DepartmentCode = '" + deptCode + "' AND CompanyID =" + CompID + " AND SiteID =" + SiteID + " AND CompanyDivisionID=" + DivID;
            Expr += " OR LoginId = 'Select a user'";

            DataRow[] foundRows = userDS.Users.Select(Expr);

            //cboDelegateUser.Items.Clear();
            //cboDelegateUser.DataSource = null;
            //cboDelegateUser.DataBind();

            //cboDelegateUser.DataTextField = "LoginId"; //instead of UserName
            //cboDelegateUser.DataTextField = userDS.Users.EmpCodeWithNameColumn.ColumnName;   // WALI :: 25-Aug-2015
            //cboDelegateUser.DataValueField = "Userid";
            //cboDelegateUser.DataSource = foundRows;
            //cboDelegateUser.DataBind();
            //cboDelegateUser.SelectedIndex = cboDelegateUser.Items.Count - 1;

        }

        public void LoadDelegateCombo()
        {
            //lblErrDelegateUser.Text = "";

            UserDS userDS = UtilUI.GetUsertListForDelegate("6880", "6880");
            //ViewState["DelegateUserDS"] = userDS;

            //cboDelegateUser.Items.Clear();
            //cboDelegateUser.DataSource = null;
            //cboDelegateUser.DataBind();

            //cboDelegateUser.DataTextField = "EmpCodeWithName";
            //cboDelegateUser.DataValueField = "Userid";
            //cboDelegateUser.DataSource = userDS.Users;
            //cboDelegateUser.DataBind();

            //cboDelegateUser.SelectedIndex = cboDelegateUser.Items.Count - 1;

        }

        private void LoadCountry()
        {
            CountryDS countryDS = UtilUI.getCountryList(); // code to populate the grades list box

            if (countryDS != null)
            {
                //countryDS.Countries.AddCountry(null, "Select a Country", null, null, 0);
                //cboNationality.DataTextField = "NAME";
                //cboNationality.DataValueField = "CountryID";
                //cboNationality.DataSource = countryDS.Countries;
                //cboNationality.DataBind();
                //cboNationality.SelectedValue = "16";

                //cboNationality_SelectedIndexChanged(null, null);
            }
        }

        private void LoadDepartmrntCombo()
        {
            //---------------Combo Load for Department--------------------
            //UserDS.UsersDataTable userTable = (UserDS.UsersDataTable)ViewState["UserTable"];
            int _siteId = 34097;
            //DepartmentDS depDS = UtilUI.GetDepartmentListForDelegateUser(userTable[0].SiteID.ToString());
            DepartmentDS depDS = UtilUI.GetDepartmentListForDelegateUser(_siteId.ToString());

            if (depDS != null)
            {
                DepartmentDS.Department dep = depDS.Departments.NewDepartment();
                dep._DepartmentName = "Select a Department";
                dep._DepartmentCode = "0";
                dep.DepartmentID = 0;
                depDS.Departments.AddDepartment(dep);

                //cboDepartment.DataTextField = "DepartmentName";
                //cboDepartment.DataValueField = "DepartmentCode";
                //cboDepartment.DataSource = depDS.Departments;
                //cboDepartment.DataBind();
                ////cboDepartment.SelectedIndex = cboDepartment.Items.Count - 1;
                //cboDepartment.SelectedValue = (userTable[0].DepartmentCode);
            }
            //cboDepartment_SelectedIndexChanged(null, null);

        }
    }
}