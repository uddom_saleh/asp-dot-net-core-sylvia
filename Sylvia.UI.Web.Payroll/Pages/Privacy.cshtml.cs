﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Sylvia.Common;
using Sylvia.DAL.Payroll;


namespace Sylvia.UI.Web.Payroll.Pages
{
    public class PrivacyModel : PageModel
    {
        private readonly ILogger<PrivacyModel> _logger;

        public PrivacyModel(ILogger<PrivacyModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {

            try
            {

                ActionDS actionDS = new ActionDS();
                actionDS.Actions.AddAction(ActionID.NA_ACTION_GET_LOGIN_USER.ToString());
                actionDS.AcceptChanges();

                string sLoginId = "admin";
                DataStringDS stringDS = new DataStringDS();
                stringDS.DataStrings.AddDataString(sLoginId);
                stringDS.AcceptChanges();

                DataSet requestDS = new DataSet();

                requestDS.Merge(actionDS);
                requestDS.Merge(stringDS);


                UserDL userDL = new UserDL();

                DataSet responseDS = userDL.Execute(requestDS);


                //userDL.Execute()

                //UserAuthenticationDS authDS = new UserAuthenticationDS();
                //authDS.UserAuthentications.AddUserAuthentication("admin", "12345");



                //actionDS.Actions.AddAction(ActionID.ACTION_USER_AUTHENTICATE.ToString());

                //requestDS.Merge(authDS);






                //UserDS RememberMeUserDS = new UserDS();
                //UserDS.RememberMeRow newRow = RememberMeUserDS.RememberMe.NewRememberMeRow();


                //newRow.IPAddress = " ";
                //newRow.RemembermeID = 0;
                //newRow.PCName = " ";
                //newRow.UserID = 0;
                //newRow.LoginID = " ";
                //newRow.RMPWD = "12345";
                //newRow.RMSalt = " ";
                //newRow.IsRememberMe = false;
                //newRow.Remarks = " ";

                //RememberMeUserDS.RememberMe.AddRememberMeRow(newRow);
                //RememberMeUserDS.AcceptChanges();

                //requestDS.Merge(RememberMeUserDS);

                //DataBoolDS booleanDS = new DataBoolDS();
                //booleanDS.DataBools.AddDataBool(false);
                //requestDS.Merge(booleanDS);


                //DataSet responseDS = BrokerUI.Request(requestDS);

                ErrorDS errDS = new ErrorDS();
                errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);


                if (errDS.Errors.Count > 0)
                {
                    if (errDS.Errors.FindByErrorCode(ErrorCode.ERR_NO_USER_FOUND.ToString()) != null)
                    {
                        ViewData.Add("Pre", "Invalid User ID. Please try again.");
                    }
                    else if (errDS.Errors.FindByErrorCode(ErrorCode.ERR_INVALIED_PASSWORD.ToString()) != null)
                    {
                        ViewData.Add("Pre", "Invalid Password.  Please try again.");

                    }

                    else if (errDS.Errors.FindByErrorCode(ErrorCode.ERR_LOGIN_MAX_CONCURRENCY_EXCEED.ToString()) != null)
                    {
                        ViewData.Add("Pre", "Maximum login limit exceeded , Please contact administrator.");

                    }
                    else if (errDS.Errors.FindByErrorCode(ErrorCode.ERR_LOGIN_MAX_FAILED_ATTEMPT_EXCEED.ToString()) != null)
                    {
                        ViewData.Add("Pre", "Failed login limit exceeded , Please contact administrator..");

                    }
                    else if (errDS.Errors.FindByErrorCode(ErrorCode.ERR_LOGIN_USER_EXPIRED.ToString()) != null)
                    {
                        ViewData.Add("Pre", "Your Account is Expired, Please contact administrator.");
                    }
                    else if (errDS.Errors.FindByErrorCode(ErrorCode.ERR_LOGIN_BEFORE_EFFECT_DATE.ToString()) != null)
                    {
                        //lblAuthencationFailed.Text = "Your account is not active yet.";
                        //this.lblAuthencationFailed.Visible = true;
                    }
                    else if (errDS.Errors.FindByErrorCode(ErrorCode.ERR_LOCKED_USER.ToString()) != null)
                    {
                        //lblAuthencationFailed.Text = "Your Account is Locked, Please contact administrator.";
                        //this.lblAuthencationFailed.Visible = true;
                    }
                    else if (errDS.Errors.FindByErrorCode(ErrorCode.ERR_DB_OPERATION_FAILED.ToString()) != null)
                    {
                        ViewData.Add("Pre", "There was an error during database operation. Detail error infomation can be found in log file.  You may contact your System Administrator for help.");

                    }
                    else
                    {
                        ViewData.Add("Pre", "There was an error during operation. You may contact your System Administrator for help");

                    }
                }


                UserDS userDS = new UserDS();
                userDS.Merge(responseDS.Tables[userDS.Users.TableName], false, MissingSchemaAction.Error);
                userDS.Merge(responseDS.Tables[userDS.UAARole.TableName], false, MissingSchemaAction.Error); //Jarif, 22.Jun.2015
                userDS.AcceptChanges();
                UserDS.User user = userDS.Users[0];


                //var branches = AutoMapper<Entities.Branch>.MapObject(data);

                ViewData.Add("Pre", user.EmployeeName);


            }
            catch (Exception ex)
            {
                throw;
            }



        }


        public void OnPostGetEmployee()
        {

            ActionDS actionDS = new ActionDS();
            actionDS.Actions.AddAction(ActionID.ACTION_DOES_USER_EXIST.ToString());
            actionDS.AcceptChanges();

            string sLoginId = "admin";
            DataStringDS stringDS = new DataStringDS();
            stringDS.DataStrings.AddDataString(sLoginId);
            stringDS.AcceptChanges();

            DataSet requestDS = new DataSet();

            requestDS.Merge(actionDS);
            requestDS.Merge(stringDS);


            UserDL userDL = new UserDL();

            DataSet responseDS = userDL.Execute(requestDS);

            DataBoolDS boolDS = new DataBoolDS();

            boolDS.Merge(responseDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();


            bool x = boolDS.DataBools[0].BoolValue;

            ViewData.Add("Pre", "DOES USER EXIST: " + x.ToString());


            //EmployeeDS empDS_Filtered = UtilUI.GetEmployeeList_ForEmpPage("0", "", "000",
            //                                                                "0", "0",
            //                                                                "0",
            //                                                                false, "EmployeeCode", "0",
            //                                                                "0", "0", "0");


            //EmployeeDS empDS_Filtered = UtilUI.GetEmployeeList_ForEmpPage("0", "", "0",
            //                                                             "0", "0",
            //                                                             "0",
            //                                                             false, "EmployeeCode", "0",
            //                                                             "0", "0", "0");
            //// now you have all submitted values in local variables

            //int x = empDS_Filtered.Employees.Rows.Count;

            //ViewData.Add("Pre", "number of employee: " + x.ToString());

            //bool bForUpdate = false;

            //string sActionTaken = "";
            //DataSet requestDS = new DataSet();
            //DataSet responseDS = null;

            //BankDS bankDS = new BankDS();
            //BankDS.Bank bank = bankDS.Banks.NewBank();
            //bank.BankCode = "xyz00950";
            //bank.BankName = "xyz bank80";
            //bank.BankDescription = "limitedd";
            //bankDS.Banks.AddBank(bank);
            //bankDS.AcceptChanges();
            //requestDS.Merge(bankDS);
            //if (bForUpdate == false)
            //{
            //    responseDS = UtilUI.SaveData(requestDS, ActionID.ACTION_BANK_ADD.ToString());
            //    sActionTaken = "Create";
            //}
            //else
            //{
            //    responseDS = UtilUI.SaveData(requestDS, ActionID.ACTION_BANK_UPD.ToString());
            //    sActionTaken = "Update";
            //}

            //Common.ErrorDS errDS = new ErrorDS();
            //errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
            //if (errDS.Errors.Count > 0) // some error has occured
            //{
            //    if (errDS.Errors.FindByErrorCode(ErrorCode.ERR_TABLE_HAS_ERROR.ToString()) != null)
            //    {
            //        bankDS.Clear();
            //        bankDS.Merge(responseDS.Tables[bankDS.Banks.TableName], false, MissingSchemaAction.Error);
            //        bankDS.Merge(responseDS.Tables[bankDS.BankErrors.TableName], false, MissingSchemaAction.Error);
            //        if (bankDS.BankErrors.Count > 0)
            //        {
            //        }
            //    }

            //    if (errDS.Errors.FindByErrorCode(ErrorCode.ERR_DB_OPERATION_FAILED.ToString()) != null)
            //    {

            //    }
            //}
            //else
            //{


            //}


        }
    }
}
