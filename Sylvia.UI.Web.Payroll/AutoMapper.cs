﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Sylvia.UI.Web.Payroll
{

    internal class AutoMapper<T> where T : new()
    {
        public static List<T> MapObject(SqlDataReader reader)
        {
            var lstObject = new List<T>();
            var objectType = typeof(T);
            var properties = objectType.GetProperties();

            while (reader.Read())
            {
                var t = new T();
                foreach (var pi in properties)
                {
                    try
                    {
                        if (DBNull.Value.Equals(reader[pi.Name]))
                        {
                            if (pi.PropertyType.Name.Equals("String"))
                            {
                                pi.SetValue(t, string.Empty, null);
                            }
                            else if (pi.PropertyType.Name.Equals("int"))
                            {
                                pi.SetValue(t, 0, null);
                            }
                            else if (pi.PropertyType.Name.Equals("decimal"))
                            {
                                pi.SetValue(t, 0.00, null);
                            }
                            continue;
                        }

                        var value = reader[pi.Name];
                        pi.SetValue(t, value, null);
                    }
                    catch
                    {
                        // ignored
                    }
                }
                lstObject.Add(t);
            }
            return lstObject;
        }
        public static List<T> MapObject(DataTable table)
        {
            var lstObject = new List<T>();
            var objectType = typeof(T);
            var properties = objectType.GetProperties();

            foreach (DataRow row in table.Rows)
            {
                var t = new T();
                foreach (var pi in properties)
                {
                    try
                    {
                        if (DBNull.Value.Equals(row[pi.Name]))
                        {
                            if (pi.PropertyType.Name.Equals("String"))
                            {
                                pi.SetValue(t, string.Empty, null);
                            }
                            else if (pi.PropertyType.Name.Equals("int"))
                            {
                                pi.SetValue(t, 0, null);
                            }
                            else if (pi.PropertyType.Name.Equals("decimal"))
                            {
                                pi.SetValue(t, 0.00, null);
                            }
                            continue;
                        }

                        var value = row[pi.Name];
                        pi.SetValue(t, value, null);
                    }
                    catch
                    {
                        // ignored
                    }
                }
                lstObject.Add(t);
            }
            return lstObject;
        }
       

    }

   
}
