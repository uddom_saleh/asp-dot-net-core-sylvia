﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Sylvia.UI.Web.Payroll
{
    public static class Extensions
    {
        /*Converts DataTable To List*/
        public static List<TSource> ToList<TSource>(this DataTable dataTable) where TSource : new()
        {
            /*check, one column mapped to only one prop*/
            List<MappFromDataTable> mapps = new List<MappFromDataTable>();
            DataTableAttributeHelper helper = new DataTableAttributeHelper();
            PropertyInfo[] props = typeof(TSource).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in props)
            {
                bool ignore = helper.IsFromColumnIgnored(prop);
                if (ignore)
                {
                    continue;
                }
                var mapp = new MappFromDataTable
                {
                    ToProperty = prop.Name,
                    FromColumn = helper.FromColumnName(prop),
                    IgnoreMapp = ignore,
                    PropertyType = helper.PropertyType(prop),
                };
                mapps.Add(mapp);
            }

            var objFieldNames = (from x in mapps select new { ColumnName = x.FromColumn, Type = x.PropertyType }).ToList();
            var dataTblFieldNames = (from DataColumn x in dataTable.Columns select new { ColumnName = x.ColumnName, Type = x.DataType }).ToList();
            var commonFields = objFieldNames.Intersect(dataTblFieldNames).ToList();

            var dataList = new List<TSource>();
            foreach (DataRow dataRow in dataTable.AsEnumerable().ToList())
            {
                var aTSource = new TSource();
                foreach (var aField in commonFields)
                {
                    string propName = mapps.First(x => x.FromColumn.Equals(aField.ColumnName)).ToProperty;
                    PropertyInfo propertyInfos = aTSource.GetType().GetProperty(propName);
                    var columnValue = dataRow[aField.ColumnName];
                    var value = (columnValue == DBNull.Value) ? null : columnValue; //if database field is nullable
                    propertyInfos.SetValue(aTSource, value, null);
                }
                dataList.Add(aTSource);
            }
            return dataList;
        }
    }

    public interface IMappToDataTable
    {
        string FromProperty { get; set; }
        string ToColumn { get; set; }
        bool IgnoreMapp { get; set; }
        Type ColumnType { get; set; }
        int? ColumOrder { get; set; }
        int PropertyPosition { get; set; }
    }

    public class MappToDataTable : IMappToDataTable
    {
        public string FromProperty { get; set; }
        public string ToColumn { get; set; }
        public bool IgnoreMapp { get; set; }
        public Type ColumnType { get; set; }
        public int? ColumOrder { get; set; }
        public int PropertyPosition { get; set; }
    }

    public interface IMappFromDataTable
    {
        string FromColumn { get; set; }
        string ToProperty { get; set; }
        Type PropertyType { get; set; }
        bool IgnoreMapp { get; set; }
    }

    public class MappFromDataTable : IMappFromDataTable
    {
        public string FromColumn { get; set; }
        public string ToProperty { get; set; }
        public Type PropertyType { get; set; }
        public bool IgnoreMapp { get; set; }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class DataTableHelperAttribute : Attribute
    {
        protected DataTableHelperAttribute()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Class)]
    public class ToDataTableAttribute : DataTableHelperAttribute
    {
        public String DataTableName { get; set; }
        public ToDataTableAttribute(string dataTableName) : this()
        {
            DataTableName = dataTableName;
        }

        private ToDataTableAttribute() : base()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ToColumnAttribute : DataTableHelperAttribute
    {
        public int? ColumnOrder { get; set; }
        public bool? IgnoreMapping { get; set; }
        public String ColumnName { get; set; }

        public ToColumnAttribute(int columnOrder) : this(true)
        {
            ColumnOrder = columnOrder;
        }

        public ToColumnAttribute(string columnName) : this(true)
        {
            ColumnName = columnName;
        }

        public ToColumnAttribute(string columnName, int columnOrder) : this(columnName)
        {
            ColumnOrder = columnOrder;
        }

        public ToColumnAttribute(bool shouldMapp) : this()
        {
            IgnoreMapping = !shouldMapp;
        }

        private ToColumnAttribute() : base()
        {
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class FromColumnAttribute : DataTableHelperAttribute
    {
        public bool? IgnoreMapping { get; set; }
        public string ColumnName { get; set; }

        public FromColumnAttribute(string columnName) : this(true)
        {
            ColumnName = columnName;
        }

        public FromColumnAttribute(bool shouldMapp) : this()
        {
            IgnoreMapping = !shouldMapp;
        }

        private FromColumnAttribute() : base()
        {
        }
    }

    public class DataTableAttributeHelper
    {
        internal T First<T>(Type type) where T : DataTableHelperAttribute
        {
            T attribute = (T)type.GetCustomAttributes(typeof(T), false).FirstOrDefault();
            return attribute;
        }

        internal T First<T>(PropertyInfo type) where T : DataTableHelperAttribute
        {
            T attribute = (T)type.GetCustomAttributes(typeof(T), false).FirstOrDefault();
            return attribute;
        }

        internal string ToDataTableName(Type type)
        {
            var attribute = First<ToDataTableAttribute>(type);
            string name = attribute == null
                            ? type.Name
                            : String.IsNullOrEmpty(attribute.DataTableName)
                                ? type.Name
                                : attribute.DataTableName;
            return name;
        }

        internal string ToColumnName(PropertyInfo prop)
        {
            var attribute = First<ToColumnAttribute>(prop);
            string name = attribute == null
                            ? prop.Name
                            : String.IsNullOrEmpty(attribute.ColumnName)
                                ? prop.Name
                                : attribute.ColumnName;
            return name;
        }

        internal Type PropertyType(PropertyInfo prop)
        {
            var type = Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType;
            return type;
        }

        internal int? ToColumnOrder(PropertyInfo prop)
        {
            var attribute = First<ToColumnAttribute>(prop);
            int? order = attribute == null
                            ? null
                            : attribute.ColumnOrder;
            return order;
        }

        internal bool IsToColumnIgnored(PropertyInfo prop)
        {
            var attribute = First<ToColumnAttribute>(prop);
            bool value = attribute == null
                            ? false
                            : attribute.IgnoreMapping == null
                                ? false
                                : (bool)attribute.IgnoreMapping;
            return value;
        }

        internal bool IsFromColumnIgnored(PropertyInfo prop)
        {
            var attribute = First<FromColumnAttribute>(prop);
            bool value = attribute == null
                            ? false
                            : attribute.IgnoreMapping == null
                                ? false
                                : (bool)attribute.IgnoreMapping;
            return value;
        }

        internal string FromColumnName(PropertyInfo prop)
        {
            var attribute = First<FromColumnAttribute>(prop);
            string name = attribute == null
                            ? prop.Name
                            : String.IsNullOrEmpty(attribute.ColumnName)
                                ? prop.Name
                                : attribute.ColumnName;
            return name;
        }
    }
}
