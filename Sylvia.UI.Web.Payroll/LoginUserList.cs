﻿using System.Collections.Generic;

namespace Sylvia.UI.Web.Payroll
{
    public static class LoginUserList
    {
        static List<string> _userList = new List<string>();

        ///<summary>
        /// Record this value in the list [If not already exists]
        ///</summary>
        public static void AddUser(string userLoginID)
        {
            bool doesExist = IsValidUser(userLoginID.Trim().ToLower());
            if (!doesExist) _userList.Add(userLoginID.Trim().ToLower());
        }

        ///<summary>
        /// Remove this value from the list.
        ///</summary>
        public static void RemoveUser(string userLoginID)
        {
            try
            { _userList.Remove(userLoginID.Trim().ToLower()); }
            catch (System.Exception exp)
            { /* if this user is not in the list, en ERROR might occur. 
           * We don't have to worry about it.  */
            }
        }

        ///<summary>
        /// Remove given list of users from existing list.
        ///</summary>
        public static void RemoveUsers(List<string> disconnectedUsers)
        {
            foreach (string user in disconnectedUsers)
            {
                try
                { _userList.Remove(user); }
                catch (System.Exception exp)
                { /* if this user is not in the list, en ERROR might occur. 
                   * We don't have to worry about it.  */ }
            }
        }

        ///<summary>
        /// Check if the value EXISTS in the list.
        ///</summary>
        public static bool IsValidUser(string userLoginID)
        {
            return _userList.Contains(userLoginID.Trim().ToLower());
        }
    }
}