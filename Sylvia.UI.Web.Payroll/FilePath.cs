﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Drawing;
using System.Configuration;
using System.Runtime.InteropServices;
/// <summary>
/// Gets the path where file will be saved or retrieved.  
/// </summary>
/// 

//Created by RONY :: 15 Feb 2017
public static class FilePath
{
    public static string sFilePath = Path.Combine(ConfigurationManager.AppSettings["FilePath"].Trim(), "Sylvia");//Path.Combine(Directory.GetParent(System.Web.HttpContext.Current.Server.MapPath("~")).FullName, "Sylvia");
    public static string recFilePath = Path.Combine(ConfigurationManager.AppSettings["REC_FilePath"].Trim(), "Sylvia");

    public static string EmployeeRecruitmentImage = Path.Combine(recFilePath, "EmployeeRecruitmentImage");
    public static string EmployeeRecruitmentSignature = Path.Combine(recFilePath, "EmployeeRecruitmentSignature");
    public static string LastCertificate = Path.Combine(recFilePath, "LastCertificate");
    public static string NID = Path.Combine(recFilePath, "NID");
    public static string FreedomFighter = Path.Combine(recFilePath, "FreedomFighter");

    public static string EmployeeImage = Path.Combine(sFilePath, "EmployeeImage");
    public static string EmployeeSignature = Path.Combine(sFilePath, "EmployeeSignature");
    public static string SelfServiceImage = Path.Combine(sFilePath, "SelfServiceImage");
    public static string SelfServiceSignature = Path.Combine(sFilePath, "SelfServiceSignature");
    public static string ApplicantImage = Path.Combine(sFilePath, "ApplicantImage");
    public static string UploadedFile = Path.Combine(sFilePath, "UploadedFile");
    public static string ReportDownloadPath = Path.Combine(sFilePath, "Report");
    public static string TempEmployeeImage = Path.Combine(sFilePath, "TempEmployeeImage");
    public static string TempEmployeeSignature = Path.Combine(sFilePath, "TempEmployeeSignature");

    public static byte[] StringToByteArray(string imageString)
    {
        byte[] imageBytes = null;
        try
        {
            //imageBytes = Encoding.ASCII.GetBytes(imageString);
            imageBytes = new byte[imageString.Length * sizeof(char)];
            System.Buffer.BlockCopy(imageString.ToCharArray(), 0, imageBytes, 0, imageBytes.Length);
            //return bytes;
            return imageBytes;
        }
        catch(Exception ex)
        { }
        return imageBytes;
    }
    public static byte[] GetImageAsByteArray(string sFilePath)
    {
        try
        {
            FileStream fs;
            BinaryReader br;
            Byte[] imgbyte;

            if (File.Exists(sFilePath))
            {
                fs = new FileStream(sFilePath, FileMode.Open, FileAccess.Read);
            }
            else
            {
                throw new Exception("Missing " + "Logo File" + "on application folder");
            }

            br = new BinaryReader(fs);
            imgbyte = br.ReadBytes(Convert.ToInt32((fs.Length)));
            return imgbyte;
        }
        catch (Exception ex)
        {
            throw new Exception("Missing " + " File" + "on application folder");
            return null;
        }
    }
    public static string GetImageAsString(Byte[] imgbyte)
    {
        try
        {
            string fileAsString = "";
            string base64String = Convert.ToBase64String(imgbyte, 0, imgbyte.Length);
            fileAsString = "data:image/jpg;base64," + base64String;
            return fileAsString;
        }
        catch (Exception ex)
        {
            throw new Exception("Missing " + " File" + "on application folder");
            return null;
        }
    }
    public static void PermissionAccess(string sFilePath) // To give permission to the File path Folder (If needed)
    {
        bool exists = System.IO.Directory.Exists(sFilePath);
        if (exists)
        {
            DirectoryInfo dInfo = new DirectoryInfo(sFilePath);
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            dInfo.SetAccessControl(dSecurity);
        }
    }

    public static bool ImpersonateUser()
    {
        var domain = ConfigurationSettings.AppSettings["ServerDomainName"];//"mislbd";//System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
        var userName = ConfigurationSettings.AppSettings["UserName"];
        var password = ConfigurationSettings.AppSettings["UserPassword"];

        WindowsIdentity tempWindowsIdentity;
        IntPtr token = IntPtr.Zero;
        IntPtr tokenDuplicate = IntPtr.Zero;

        if (RevertToSelf())
        {
            if (LogonUserA(userName, domain, password, LOGON32_LOGON_INTERACTIVE,
                LOGON32_PROVIDER_DEFAULT, ref token) != 0)
            {
                if (DuplicateToken(token, 2, ref tokenDuplicate) != 0)
                {
                    tempWindowsIdentity = new WindowsIdentity(tokenDuplicate);
                    ImpersonationContext = tempWindowsIdentity.Impersonate();
                    if (ImpersonationContext != null)
                    {
                        CloseHandle(token);
                        CloseHandle(tokenDuplicate);
                        return true;
                    }
                }
            }
        }
        if (token != IntPtr.Zero)
            CloseHandle(token);
        if (tokenDuplicate != IntPtr.Zero)
            CloseHandle(tokenDuplicate);
        return false;
    }

    public static void UndoImpersonateUser()
    {
        ImpersonationContext.Undo();
    }

    #region Impersionation global variables
    public const int LOGON32_LOGON_INTERACTIVE = 2;
    public const int LOGON32_PROVIDER_DEFAULT = 0;
    public static WindowsImpersonationContext ImpersonationContext;

    [DllImport("advapi32.dll")]
    public static extern int LogonUserA(String lpszUserName,
        String lpszDomain,
        String lpszPassword,
        int dwLogonType,
        int dwLogonProvider,
        ref IntPtr phToken);
    [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern int DuplicateToken(IntPtr hToken,
        int impersonationLevel,
        ref IntPtr hNewToken);

    [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    public static extern bool RevertToSelf();

    [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
    public static extern bool CloseHandle(IntPtr handle);

    const int LOGON32_LOGON_NEW_CREDENTIALS = 9;

    [DllImport("advapi32.dll", SetLastError = true)]
    public static extern bool LogonUser(string pszUsername, string pszDomain, string pszPassword,
        int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

    #endregion

    
}