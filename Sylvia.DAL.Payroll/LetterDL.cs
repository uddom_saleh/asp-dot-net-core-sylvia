/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: March 27, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class LetterDL
    {
        public LetterDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_LETTER_SUBJECT_CREATE:
                    return _createSubject(inputDS);
                case ActionID.ACTION_LETTER_SUBJECT_UPDATE:
                    return this._updateSubject(inputDS);
                case ActionID.ACTION_LETTER_SUBJECT_DEL:
                    return this._deleteSubject(inputDS);
                case ActionID.NA_ACTION_GET_LETTER_SUBJECT_LIST:
                    return _getSubjectList(inputDS);

                case ActionID.ACTION_LETTER_TEMPLATE_CREATE:
                    return _createTemplate(inputDS);
                case ActionID.NA_ACTION_GET_LETTER_TEMPLATE_LIST:
                    return _getTemplateList(inputDS);
                case ActionID.ACTION_LETTER_TEMPLATE_DEL:
                    return _deleteTemplate(inputDS);
                case ActionID.ACTION_LETTER_TEMPLATE_UPDATE:
                    return _updateTemplate(inputDS);
                case ActionID.NA_ACTION_GET_LETTER_TEMPLATE_DETAILS_LIST:
                    return _getTemplateDetails(inputDS);
                case ActionID.NA_GET_SALARY_PARTICULARS_GRADELEVEL_WISE:
                    return _SalaryParticularsGradeLevelWise(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_PFSLIP_FOR_LETTER:
                    return _getEmp_PFSlip_For_Letter(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_INCREMENT_FOR_LETTER:
                    return _getEmp_Increment_For_Letter(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_INFO_FOR_ORGANOGRAM:
                    return _getEmp_For_Organogram(inputDS);
                case ActionID.NA_ACTION_GET_JOB_APPLICANT_FOR_ADMITCARD:
                    return _getJobApplicantInfoForAdmitCard(inputDS);
                case ActionID.ACTION_APPLICANT_ROLL_SAVE_FOR_ADMITCARD:
                    return _createJobApplicant_RollForAdmitCard(inputDS);
                    
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }
        private DataSet _createSubject(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            LetterDS subjectDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LETTER_SUBJECT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            subjectDS.Merge(inputDS.Tables[subjectDS.Subject.TableName], false, MissingSchemaAction.Error);
            subjectDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (LetterDS.SubjectRow row in subjectDS.Subject.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_SubjectID", (object)genPK);
                }
                cmd.Parameters.AddWithValue("@p_SubjectCode", row.SubjectCode);
                cmd.Parameters.AddWithValue("@p_SubjectName", row.SubjectName);

                if (!row.IsRemarksNull())
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LETTER_SUBJECT_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());

            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(subjectDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getSubjectList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            LetterDS subjectDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getSubjectList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, subjectDS, subjectDS.Subject.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LETTER_SUBJECT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            subjectDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(subjectDS.Subject);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateSubject(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            LetterDS subjectDS = new LetterDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LETTER_SUBJECT_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            subjectDS.Merge(inputDS.Tables[subjectDS.Subject.TableName], false, MissingSchemaAction.Error);
            subjectDS.AcceptChanges();

            foreach (LetterDS.SubjectRow row in subjectDS.Subject.Rows)
            {
                cmd.Parameters.AddWithValue("p_SubjectCode", row.SubjectCode);
                cmd.Parameters.AddWithValue("p_SubjectName", row.SubjectName);

                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                cmd.Parameters.AddWithValue("p_SubjectID", row.SubjectID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LETTER_SUBJECT_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteSubject(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LETTER_SUBJECT_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_SubjectCode", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LETTER_SUBJECT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;  // return empty ErrorDS
        }

        private DataSet _createTemplate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            LetterDS tempDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LETTER_TEMPLATE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            tempDS.Merge(inputDS.Tables[tempDS.Template.TableName], false, MissingSchemaAction.Error);
            tempDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (LetterDS.TemplateRow row in tempDS.Template.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_TemplateID", (object)genPK);
                }
                cmd.Parameters.AddWithValue("@p_TemplateName", row.TemplateName);
                cmd.Parameters.AddWithValue("@p_SubjectCode", row.SubjectCode);

                if (!row.IsTemplateDetailsNull())
                {
                    cmd.Parameters.AddWithValue("@p_Details", row.TemplateDetails);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Details", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LETTER_TEMPLATE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(tempDS.Template[0].TemplateName);
                else messageDS.ErrorMsg.AddErrorMsgRow(tempDS.Template[0].TemplateName);
                messageDS.AcceptChanges();
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(tempDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getTemplateList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            LetterDS tempDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getTemplateList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["SubjectCode1"].Value = StringDS.DataStrings[0].StringValue;
            cmd.Parameters["SubjectCode2"].Value = StringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, tempDS, tempDS.Template.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LETTER_TEMPLATE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            tempDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(tempDS.Template);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteTemplate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            LetterDS tempDS = new LetterDS();

            tempDS.Merge(inputDS.Tables[tempDS.Template.TableName], false, MissingSchemaAction.Error);
            tempDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LETTER_TEMPLATE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (LetterDS.TemplateRow row in tempDS.Template.Rows)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("p_TemplateID", row.TemplateID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID,
                    ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LETTER_TEMPLATE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.TemplateName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.TemplateName);
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateTemplate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            LetterDS tempDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();

            tempDS.Merge(inputDS.Tables[tempDS.Template.TableName], false, MissingSchemaAction.Error);
            tempDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Task Update
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LETTER_TEMPLATE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }
            foreach (LetterDS.TemplateRow row in tempDS.Template.Rows)
            {

                if (!row.IsTemplateDetailsNull()) cmd.Parameters.AddWithValue("@p_Details", row.TemplateDetails);
                else cmd.Parameters.AddWithValue("@p_Details", DBNull.Value);
                cmd.Parameters.AddWithValue("p_TemplateID", row.TemplateID);
                cmd.Parameters.AddWithValue("p_SubjectCode", row.SubjectCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LETTER_TEMPLATE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(tempDS.Template[0].TemplateName);
                else messageDS.ErrorMsg.AddErrorMsgRow(tempDS.Template[0].TemplateName);
                messageDS.AcceptChanges();
            }
            #endregion
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getTemplateDetails(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            LetterDS tempDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getTemplateDetailsByTemID");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["TemplateID1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["TemplateID2"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, tempDS, tempDS.Template.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LETTER_TEMPLATE_DETAILS_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            tempDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(tempDS.Template);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _SalaryParticularsGradeLevelWise(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            LetterDS letterDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getSalaryParticularsGradeLevelWise");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode3"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode4"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, letterDS, letterDS.SalaryParticulars.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LETTER_TEMPLATE_DETAILS_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            letterDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(letterDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getEmp_PFSlip_For_Letter(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            LetterDS letterDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            EmployeeDS empDS = new EmployeeDS();
            empDS.Merge(inputDS.Tables[empDS.GenerateLetter.TableName], false, MissingSchemaAction.Error);
            empDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (EmployeeDS.GenerateLetterRow row in empDS.GenerateLetter.Rows)
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("getEmpPFSlipForLetter");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeCode"].Value = row.EmployeeCode;
                cmd.Parameters["DateOn"].Value = row.EffectDate;
                cmd.Parameters["EmployeeCode1"].Value = row.EmployeeCode;
                cmd.Parameters["DateOn1"].Value = row.EffectDate;

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, letterDS, letterDS.PFSlipForLetter.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_PFSLIP_FOR_LETTER.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                letterDS.AcceptChanges();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(letterDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getEmp_Increment_For_Letter(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            LetterDS letterDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getEmpIncrementForLetter");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, letterDS, letterDS.EmpIncrementForLetter.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_INCREMENT_FOR_LETTER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            letterDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(letterDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getEmp_For_Organogram(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            LetterDS letterDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            //DataStringDS stringDS = new DataStringDS();
            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getEmpForOrganogram");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, letterDS, letterDS.Organogram.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_INFO_FOR_ORGANOGRAM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            letterDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(letterDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getJobApplicantInfoForAdmitCard(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            LetterDS tempDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetJobApplicantInfoForAdmitCard");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, tempDS, tempDS.JobApplicantInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_FOR_ADMITCARD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            tempDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(tempDS.JobApplicantInfo);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createJobApplicant_RollForAdmitCard(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            LetterDS letterDS = new LetterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            OleDbCommand cmd = new OleDbCommand();

            cmd.CommandText = "PRO_JOB_APPLICANT_ROLL_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            letterDS.Merge(inputDS.Tables[letterDS.JobApplicantInfo.TableName], false, MissingSchemaAction.Error);
            letterDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Update Applicant user roll number
            foreach (LetterDS.JobApplicantInfoRow row in letterDS.JobApplicantInfo.Rows)
            {
                cmd.Parameters.AddWithValue("p_JobApplicationID", row.JobApplicationID);
                cmd.Parameters.AddWithValue("p_ApplicantRoll", row.ApplicantRoll);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_APPLICANT_ROLL_SAVE_FOR_ADMITCARD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(letterDS.JobApplicantInfo[0].JobApplicationID.ToString());
                else messageDS.ErrorMsg.AddErrorMsgRow(letterDS.JobApplicantInfo[0].JobApplicationID.ToString());
                messageDS.AcceptChanges();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(letterDS);
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
    }
}
 