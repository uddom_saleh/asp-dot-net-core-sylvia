using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using System.Configuration;
namespace Sylvia.DAL.Payroll
{
  /// <summary>
  /// Summary description for LoanDL.
  /// </summary>
  public class LoanDL
  {
    public LoanDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }
    
    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {
        case ActionID.ACTION_LOAN_ADD:
          return _createLoan( inputDS );
      case ActionID.NA_ACTION_Q_LOAN_ALL:
          return _getLoanList(inputDS);
        case ActionID.ACTION_LOAN_DEL:
          return this._deleteLoan( inputDS );
      case ActionID.NA_ACTION_LOAN_GET:
          return this._getLoan( inputDS );
        case ActionID.ACTION_LOAN_UPD:
          return this._updateLoan( inputDS );
        case ActionID.ACTION_DOES_LOAN_EXIST:
          return this._doesLoanExist(inputDS);

        case ActionID.ACTION_LOANGRADE_CONFIG_ADD:
          return this._createLoanGradeConfig(inputDS);
        case ActionID.NA_ACTION_GET_LOANGRADE_CONFIG:
          return this._getLoanGradeConfig(inputDS);
        case ActionID.ACTION_LOAN_BENEFIT_RULE_CREATE:
          return this._createLoanBenefitRule(inputDS);

        #region Benefit Input Field
        case ActionID.ACTION_BENEFIT_INPUT_FIELD_CREATE:
          return this._createBenefitInputField(inputDS);
        case ActionID.NA_ACTION_GET_BENEFIT_INPUT_FIELD_LIST_ALL:
          return this._getBenefitInputFieldListAll(inputDS);
        case ActionID.ACTION_BENEFIT_INPUTFIELD_DEL:
          return this._deleteBenefitInputfield(inputDS);
        case ActionID.ACTION_BENEFIT_INPUT_FIELD_UPD:
          return this._updateBenefitInputField(inputDS);

        case ActionID.ACTION_BIFCONFIG_CREATE:
          return this._createBIFConfig(inputDS);
        case ActionID.ACTION_BIFCONFIG_DELETE:
          return this._deleteBIFConfig(inputDS); ;
        case ActionID.NA_ACTION_GET_BENEFIT_INPUTFIELD_LIST_BY_BENEFITID:
            return this._GetBenefitInputListByBenefitID(inputDS);
        case ActionID.NA_ACTION_GET_BENEFIT_FIELD_LIST_BY_BREQUESTID:
            return this._GetBenefitFieldListByBenefitRequestID(inputDS);
        case ActionID.NA_ACTION_GET_BENEFIT_USING_LOGINID:
            return this._GetBenefitUsingLoginID(inputDS);
        case ActionID.NA_ACTION_GET_FIELD_LIST_BY_BENEFITID_LOGINID:
            return this._GetBenefitInputListByBenefitIDLoginID(inputDS);
        case ActionID.ACTION_BENEFIT_APPLY:
            return this._createBenefitApply(inputDS);
        case ActionID.NA_ACTION_BENEFIT_APPLIED_LIST:
            return this._GetBenefitAppliedData(inputDS);
        case ActionID.NA_ACTION_GET_BENEFIT_ACTIVITY_LIST_ALL:
            return this._GetBenefitActivityAll(inputDS);
        case ActionID.ACTION_BENEFIT_APPLY_APPROVATION:
            return this._BenefitApplyApprovation(inputDS);
        case ActionID.NA_ACTION_GET_BENEFIT_LIMIT_BY_LOGINID:
            return this._GetBenefitLimitByLoginID(inputDS);

        case ActionID.NA_ACTION_GET_LOAN_APPROVED_EMPLOYEE:
            return this._GetLoanApprovedEmployee(inputDS);

        case ActionID.NA_ACTION_GET_LOAN_BENEFIT_RULE:
            return this._GetLoanBenefitRule(inputDS);

        #endregion

        case ActionID.NA_ACTION_GET_ALL_LOAN_BENEFIT_RULE:
            return this._GetLoanBenefitRules_ALL(inputDS);
        case ActionID.NA_ACTION_Q_LOAN_ALL_SETTLED:
            return this._getLoanSettledInfo(inputDS);
        case ActionID.ACTION_BENEFIT_APPROVAL_AUTHOR_SAVE:
            return this._createBenefitApprovalAuthority(inputDS);
        case ActionID.NA_ACTION_GET_BENEFIT_APPROVAL_AUTHOR:
            return this._GetBenefitApproval(inputDS);
        case ActionID.NA_ACTION_GET_LOAN_BENEFIT_DATA:
            return this._GetLoanBenefitData(inputDS);
               
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }


    private DataSet _createLoan(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      LoanDS loanDS = new LoanDS();
      DBConnectionDS connDS = new  DBConnectionDS();
      DataStringDS stringDS = new DataStringDS();

      

      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      
      //create command
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateLoan");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      
      loanDS.Merge( inputDS.Tables[ loanDS.Loans.TableName ], false, MissingSchemaAction.Error );
      loanDS.AcceptChanges();
      
      
        long genPK = IDGenerator.GetNextGenericPK();
        if(genPK == -1)
        {
          return UtilDL.GetDBOperationFailed();
        }
         
        cmd.Parameters["Id"].Value = (object) genPK;
        if(loanDS.Loans[0].IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) loanDS.Loans[0].Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;
        }
        if(loanDS.Loans[0].IsDescriptionNull()==false)
        {
          cmd.Parameters["Description"].Value = (object) loanDS.Loans[0].Description ;
        }
        else
        {
          cmd.Parameters["Description"].Value = null;
        }
     
        if(loanDS.Loans[0].IsConfirmationRequiredNull()==false)
        {
          cmd.Parameters["ConfirmationRequired"].Value = (object) loanDS.Loans[0].ConfirmationRequired;
        }
        else
        {
          cmd.Parameters["ConfirmationRequired"].Value = null;
        }

        if(loanDS.Loans[0].IsInterestRateNull()==false)
        {
          cmd.Parameters["InterestRate"].Value = (object) loanDS.Loans[0].InterestRate;
        }
        else
        {
          cmd.Parameters["InterestRate"].Value = null;
        }

        if(loanDS.Loans[0].IsLoanInstallmentNull()==false)
        {
          cmd.Parameters["LoanInstallment"].Value = (object) loanDS.Loans[0].LoanInstallment ;
        }
        else
        {
          cmd.Parameters["LoanInstallment"].Value = null;
        }

        if(loanDS.Loans[0].IsLoanDurationNull()==false)
        {
          cmd.Parameters["LoanDuration"].Value = (object) loanDS.Loans[0].LoanDuration ;
        }
        else
        {
          cmd.Parameters["LoanDuration"].Value = null;
        }
        if(loanDS.Loans[0].IsLoanTypeCodeNull()==false)
        {
          long nId= UtilDL.GetLoanTypeId(connDS,loanDS.Loans[0].LoanTypeCode);
          if(nId==-1)
          {
            return UtilDL.GetDBOperationFailed();
          }
          cmd.Parameters["LoanTypeId"].Value = (object) nId ;
        }
        else
        {
          cmd.Parameters["LoanTypeId"].Value = null;
        }
        cmd.Parameters["IsActive"].Value = (object)loanDS.Loans[0].IsActive;

        if (!loanDS.Loans[0].IsIsSalaryExcludedLoanNull())
            cmd.Parameters["IsSalaryExcludedLoan"].Value = loanDS.Loans[0].IsSalaryExcludedLoan;
        else cmd.Parameters["IsSalaryExcludedLoan"].Value = DBNull.Value;

        cmd.Parameters["IsLimitMarkingApply"].Value = loanDS.Loans[0].IsLimitMarkingApply;
        cmd.Parameters["IsApprovalRequired"].Value = loanDS.Loans[0].IsApprovalRequired;

        if (!loanDS.Loans[0].IsUpdated_ByNull()) cmd.Parameters["UpdatedBy"].Value = loanDS.Loans[0].Updated_By;
        else cmd.Parameters["UpdatedBy"].Value = DBNull.Value;

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_LOAN_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      

      cmd.Dispose(); //create command for deleting grades for this allowance
      cmd =DBCommandProvider.GetDBCommand("DeleteLoanGrades");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      
      cmd.Parameters["Id"].Value= genPK;

      bError = false;
      nRowAffected = -1;
      nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_LOAN_ADD.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }

      cmd.Dispose(); //create command for inserting grades for this allowance
      cmd =DBCommandProvider.GetDBCommand("CreateLoanGrades");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
 
      cmd.Parameters["Id"].Value= genPK;

      //extract grades
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      foreach(DataStringDS.DataString grade in stringDS.DataStrings)
      {
        long nGradeId = UtilDL.GetGradeId(connDS,grade.StringValue);
        if(nGradeId==-1)
        {
          return UtilDL.GetDBOperationFailed();
        }
        cmd.Parameters["GradeId"].Value = (object) nGradeId;
      

        bError = false;
        nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }

      }

      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _deleteLoan(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

    
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteLoan");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      foreach(DataStringDS.DataString sValue in stringDS.DataStrings)
      {
        if (sValue.IsStringValueNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) sValue.StringValue;
        }
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_LOAN_DEL.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }

      return errDS;  // return empty ErrorDS
    }
    private DataSet _getLoanList(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        LoanDS loanDS = new LoanDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanList");
        if (cmd == null) return UtilDL.GetCommandNotFound();

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.Loans.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        if (bError == true)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.NA_ACTION_Q_LOAN_ALL.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();
        return returnDS;
    }
  
    private DataSet _updateLoan(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();
      LoanDS loanDS = new LoanDS();
      DBConnectionDS connDS = new  DBConnectionDS();
      DataStringDS stringDS= new DataStringDS();
      long nId = -1;

      

      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      
      //create command
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateLoan");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      
      loanDS.Merge( inputDS.Tables[ loanDS.Loans.TableName ], false, MissingSchemaAction.Error );
      loanDS.AcceptChanges();

      

      if(loanDS.Loans[0].IsCodeNull()==false)
      {
        cmd.Parameters["Code"].Value = (object) loanDS.Loans[0].Code;
      }
      else
      {
        cmd.Parameters["Code"].Value = null;
      }
      if(loanDS.Loans[0].IsDescriptionNull()==false)
      {
        cmd.Parameters["Description"].Value = (object) loanDS.Loans[0].Description ;
      }
      else
      {
        cmd.Parameters["Description"].Value = null;
      }
     
      if(loanDS.Loans[0].IsConfirmationRequiredNull()==false)
      {
        cmd.Parameters["ConfirmationRequired"].Value = (object) loanDS.Loans[0].ConfirmationRequired;
      }
      else
      {
        cmd.Parameters["ConfirmationRequired"].Value = null;
      }

      if(loanDS.Loans[0].IsInterestRateNull()==false)
      {
        cmd.Parameters["InterestRate"].Value = (object) loanDS.Loans[0].InterestRate;
      }
      else
      {
        cmd.Parameters["InterestRate"].Value = null;
      }

      if(loanDS.Loans[0].IsLoanInstallmentNull()==false)
      {
        cmd.Parameters["LoanInstallment"].Value = (object) loanDS.Loans[0].LoanInstallment ;
      }
      else
      {
        cmd.Parameters["LoanInstallment"].Value = null;
      }

      if(loanDS.Loans[0].IsLoanDurationNull()==false)
      {
        cmd.Parameters["LoanDuration"].Value = (object) loanDS.Loans[0].LoanDuration ;
      }
      else
      {
        cmd.Parameters["LoanDuration"].Value = null;
      }
      if(loanDS.Loans[0].IsLoanTypeCodeNull()==false)
      {
         nId= UtilDL.GetLoanTypeId(connDS,loanDS.Loans[0].LoanTypeCode);
        if(nId==-1)
        {
          return UtilDL.GetDBOperationFailed();
        }
        cmd.Parameters["LoanTypeId"].Value = (object) nId ;
      }
      else
      {
        cmd.Parameters["LoanTypeId"].Value = null;
      }

      cmd.Parameters["IsActive"].Value = (object)loanDS.Loans[0].IsActive;

      if (!loanDS.Loans[0].IsIsSalaryExcludedLoanNull())
          cmd.Parameters["IsSalaryExcludedLoan"].Value = loanDS.Loans[0].IsSalaryExcludedLoan;
      else cmd.Parameters["IsSalaryExcludedLoan"].Value = DBNull.Value;

      cmd.Parameters["IsLimitMarkingApply"].Value = loanDS.Loans[0].IsLimitMarkingApply;
      cmd.Parameters["IsApprovalRequired"].Value = loanDS.Loans[0].IsApprovalRequired;

      if (!loanDS.Loans[0].IsUpdated_ByNull()) cmd.Parameters["UpdatedBy"].Value = loanDS.Loans[0].Updated_By;
      else cmd.Parameters["UpdatedBy"].Value = DBNull.Value;

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_LOAN_ADD.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }
      

      cmd.Dispose(); //create command for deleting grades for this allowance
      cmd =DBCommandProvider.GetDBCommand("DeleteLoanGrades");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

       nId = UtilDL.GetLoanId(connDS, loanDS.Loans[0].Code);
      if(nId==-1)
      {
        return UtilDL.GetDBOperationFailed();
      }
      cmd.Parameters["Id"].Value= nId;

      bError = false;
      nRowAffected = -1;
      nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_LOAN_ADD.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }

      cmd.Dispose(); //create command for inserting grades for this allowance
      cmd =DBCommandProvider.GetDBCommand("CreateLoanGrades");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
 
      cmd.Parameters["Id"].Value= nId;

      //extract grades
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      foreach(DataStringDS.DataString grade in stringDS.DataStrings)
      {
        long nGradeId = UtilDL.GetGradeId(connDS,grade.StringValue);
        if(nGradeId==-1)
        {
          return UtilDL.GetDBOperationFailed();
        }
        cmd.Parameters["GradeId"].Value = (object) nGradeId;
      

        bError = false;
        nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }

      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }

    private DataSet _doesLoanExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesLoanExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
        cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    } 


    private DataSet _getLoan(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      DataStringDS stringDS = new DataStringDS(); 
      DBConnectionDS connDS = new  DBConnectionDS();      
     
      //extract connectionDS
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

   
      //extract Allowance/deduction code
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoan");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue;

      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      //LoanPO loanPO = new LoanPO();
      LoanDS loanDS = new LoanDS();

      bool bError = false;
      int nRowAffected = -1;
      //nRowAffected = ADOController.Instance.Fill(adapter, loanPO, loanPO.Loans.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
      nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.Loans.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_ALLOWDEDUCT_GET.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }
      //loanPO.AcceptChanges();
      loanDS.AcceptChanges();

      //LoanDS loanDS = new LoanDS();
      //if ( loanPO.Loans.Count ==1 )
      if (loanDS.Loans.Count == 1)
      {
          #region Romeved PO section
          //foreach ( LoanPO.Loan poLoan in loanPO.Loans)
        //{
        //  LoanDS.Loan loan = loanDS.Loans.NewLoan();
        //  if(poLoan.IsCodeNull()==false)
        //  {
        //    loan.Code = poLoan.Code;
        //  }
        //  if(poLoan.IsDescriptionNull()==false)
        //  {
        //    loan.Description = poLoan.Description;
        //  }
        //  if(poLoan.IsInterestRateNull()==false)
        //  {
        //    loan.InterestRate = poLoan.InterestRate;
        //  }
        //  if(poLoan.IsLoanInstallmentNull()==false)
        //  {
        //    loan.LoanInstallment = poLoan.LoanInstallment;
        //  }
        //  if(poLoan.IsLoanDurationNull()==false)
        //  {
        //    loan.LoanDuration = poLoan.LoanDuration;
        //  }
        //  if(poLoan.IsConfirmationRequiredNull()==false)
        //  {
        //    loan.ConfirmationRequired = poLoan.ConfirmationRequired;
        //  }
        //  if(poLoan.IsLoanTypeIdNull()==false)
        //  {
        //    string sCode = UtilDL.GetLoanTypeCode(connDS, poLoan.LoanTypeId);
        //    if(sCode==null)
        //    {
        //      return UtilDL.GetDBOperationFailed();
        //    }
        //    loan.LoanTypeCode = sCode;
        //  }

          
        //  loanDS.Loans.AddLoan( loan );
        //  loanDS.AcceptChanges();
          //}
          #endregion

          cmd.Dispose();
        cmd = DBCommandProvider.GetDBCommand("GetLoanGrades");
        if(cmd==null)
        {
          return UtilDL.GetCommandNotFound();
        }

        long nLoanId= UtilDL.GetLoanId(connDS,loanDS.Loans[0].Code);
        if(nLoanId==-1)
        {
          return UtilDL.GetDBOperationFailed();
        }
        cmd.Parameters["Id"].Value= nLoanId;
        adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;
       
        LoanGradePO loanGradePO =  new LoanGradePO();
       
        bError = false;
        nRowAffected = -1;
        nRowAffected = ADOController.Instance.Fill(adapter, 
          loanGradePO, 
          loanGradePO.LoanGrades.TableName, 
          connDS.DBConnections[0].ConnectionID, 
          ref bError);
        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.NA_ACTION_ALLOWDEDUCT_GET.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;

        }
        
        loanGradePO.AcceptChanges();
    
        
        if ( loanGradePO.LoanGrades.Count > 0 )
        {
          
          foreach (LoanGradePO.LoanGrade loanGrade in loanGradePO.LoanGrades  )
          {
            string sGradeCode = UtilDL.GetGradeCode(connDS,loanGrade.GradeId);
            
            if (sGradeCode == null)
            {
              return UtilDL.GetDBOperationFailed();
            }  
            stringDS.DataStrings.AddDataString(sGradeCode);
          }//end of foreach
          stringDS.AcceptChanges();
        }//end of if
          
      }
    
        
      

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( loanDS );
      returnDS.Merge(stringDS);
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
    
      return returnDS;


    }

    #region WALI :: 11-Jun-2014
    private DataSet _createLoanGradeConfig(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        LoanDS loanDS = new LoanDS();
        DataIntegerDS integerDS = new DataIntegerDS();
        MessageDS messageDS = new MessageDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataStringDS stringDS = new DataStringDS();
        bool bError;
        int nRowAffected;

        //extract dbconnection 
        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        loanDS.Merge(inputDS.Tables[loanDS.LoanGradeConfig.TableName], false, MissingSchemaAction.Error);
        loanDS.AcceptChanges();

        integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
        integerDS.AcceptChanges();

        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();
        #region Test

        #region First Delete Old Records....

        OleDbCommand cmd_del = new OleDbCommand();
        cmd_del.CommandText = "PRO_LOANGRADECONFIG_DEL";
        cmd_del.CommandType = CommandType.StoredProcedure;
        if (cmd_del == null) return UtilDL.GetCommandNotFound();

        cmd_del.Parameters.AddWithValue("p_LoanID", integerDS.DataIntegers[0].IntegerValue);

        bError = false;
        nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bError);
        if (bError == true)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.ACTION_LOANGRADE_CONFIG_ADD.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
        }
        if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(stringDS.DataStrings[0].StringValue);
        else messageDS.ErrorMsg.AddErrorMsgRow(stringDS.DataStrings[0].StringValue);

        #endregion

        #region Insert new Records.........
        foreach (LoanDS.LoanGradeConfigRow row in loanDS.LoanGradeConfig.Rows)
        {
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LOANGRADECONFIG_ADD";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_LGConfigID", genPK);
            cmd.Parameters.AddWithValue("p_LoanID", row.LoanID);
            cmd.Parameters.AddWithValue("p_GradeID", UtilDL.GetGradeId(connDS, row.GradeCode));

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LOANGRADE_CONFIG_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
            }
            cmd.Parameters.Clear();
        }
        #endregion

        DataSet returnDS = new DataSet();
        returnDS.Merge(errDS);
        returnDS.Merge(messageDS);
        return returnDS;
        #endregion
    }
    private DataSet _getLoanGradeConfig(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DataIntegerDS integerDS = new DataIntegerDS();
        DBConnectionDS connDS = new DBConnectionDS();

        //extract connectionDS
        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        //extract Allowance/deduction code
        integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
        integerDS.AcceptChanges();

        OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanGradeConfig");
        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }
        cmd.Parameters["LoanID"].Value = integerDS.DataIntegers[0].IntegerValue;

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        LoanDS loanDS = new LoanDS();
        bool bError = false;
        int nRowAffected = -1;

        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.LoanGradeConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        if (bError == true)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.NA_ACTION_GET_LOANGRADE_CONFIG.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;

        }
        loanDS.AcceptChanges();

        // now create the packet
        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();
        return returnDS;
    }

    private DataSet _createLoanBenefitRule(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        LoanDS loanDS = new LoanDS();
        MessageDS messageDS = new MessageDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataStringDS stringDS = new DataStringDS();
        bool bError;
        int nRowAffected;

        //extract dbconnection 
        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        loanDS.Merge(inputDS.Tables[loanDS.LoanBenefitRule.TableName], false, MissingSchemaAction.Error);
        loanDS.AcceptChanges();

        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        #region First Delete Old Records
        OleDbCommand cmdDel = new OleDbCommand();
        if (loanDS.LoanBenefitRule[0].RuleFor == "Benefits")
        {
            cmdDel.CommandText = "PRO_BENEFIT_RULE_DEL";
            cmdDel.Parameters.AddWithValue("p_OthersBenefitID", loanDS.LoanBenefitRule[0].BenefitID);
        }
        else
        {
            cmdDel.CommandText = "PRO_LOAN_RULE_DEL";
            cmdDel.Parameters.AddWithValue("p_LoanID", loanDS.LoanBenefitRule[0].LoanID);
        }

        cmdDel.CommandType = CommandType.StoredProcedure;
        if (cmdDel == null) return UtilDL.GetCommandNotFound();
        bool bErrorDel = false;
        int nRowAffectedDel = -1;
        nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);

        if (bErrorDel == true)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.ACTION_LOAN_BENEFIT_RULE_CREATE.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }
        #endregion

        foreach (LoanDS.LoanBenefitRuleRow row in loanDS.LoanBenefitRule.Rows)
        {
            OleDbCommand cmd = new OleDbCommand();

            if (row.RuleFor == "Benefits") cmd.CommandText = "PRO_BENEFIT_RULE_ADD";
            else cmd.CommandText = "PRO_LOAN_RULE_ADD";
            
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1) return UtilDL.GetDBOperationFailed();

            long userID = UtilDL.GetUserId(connDS, row.VerifierLoginID);

            if (row.RuleFor == "Benefits")
            {
                cmd.Parameters.AddWithValue("p_BenefitRuleID", genPK);
                cmd.Parameters.AddWithValue("p_OthersBenefitID", row.BenefitID);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_LoanRuleID", genPK);
                cmd.Parameters.AddWithValue("p_LoanID", row.LoanID);
            }

            cmd.Parameters.AddWithValue("p_WaitingPeriod", row.WaitingPeriod);
            cmd.Parameters.AddWithValue("p_ApplicableFor", row.ApplicableFor);
            cmd.Parameters.AddWithValue("p_GradeID", UtilDL.GetGradeId(connDS, row.GradeCode));
            cmd.Parameters.AddWithValue("p_VerifierUserID", userID);
            cmd.Parameters.AddWithValue("p_Limit", row.Limit);
            if (!row.IsPastExperience_YearNull()) cmd.Parameters.AddWithValue("p_PastExperience_Year", row.PastExperience_Year);
            else cmd.Parameters.AddWithValue("p_PastExperience_Year", DBNull.Value);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LOAN_BENEFIT_RULE_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
            }
            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(stringDS.DataStrings[0].StringValue);
            else messageDS.ErrorMsg.AddErrorMsgRow(stringDS.DataStrings[0].StringValue);
        }

        DataSet returnDS = new DataSet();
        returnDS.Merge(errDS);
        returnDS.Merge(messageDS);
        return returnDS;
    }
    #endregion
    #region Benefit Input Field :: WALI :: 13-Jun-2014
    private DataSet _createBenefitInputField(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        LoanDS benefitDS = new LoanDS();
        DBConnectionDS connDS = new DBConnectionDS();

        OleDbCommand cmd = new OleDbCommand();
        cmd.CommandText = "PRO_BENEFIT_INPUTFIELD_CREATE";
        cmd.CommandType = CommandType.StoredProcedure;

        if (cmd == null) return UtilDL.GetCommandNotFound();

        benefitDS.Merge(inputDS.Tables[benefitDS.BenefittInputField.TableName], false, MissingSchemaAction.Error);
        benefitDS.AcceptChanges();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        foreach (LoanDS.BenefittInputFieldRow row in benefitDS.BenefittInputField.Rows)
        {
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmd.Parameters.AddWithValue("p_BIFID", genPK);

            if (row.IsFIELDNAMENull() == false)
            {
                cmd.Parameters.AddWithValue("p_FIELDNAME", row.FIELDNAME);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_FIELDNAME", DBNull.Value);
            }

            if (row.IsDATATYPENull() == false)
            {
                cmd.Parameters.AddWithValue("p_DATATYPE", row.DATATYPE);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_DATATYPE", DBNull.Value);
            }

            if (row.IsBIFREMARKSNull() == false)
            {
                cmd.Parameters.AddWithValue("p_BIFREMARKS", row.BIFREMARKS);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_BIFREMARKS", DBNull.Value);
            }

            if (row.IsBIFCODENull() == false)
            {
                cmd.Parameters.AddWithValue("p_BIFCODE", row.BIFCODE);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_BIFCODE", DBNull.Value);
            }

            //if (row.IsCurrentUserIDNull() == false)
            //{
            //    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
            //}
            //else
            //{
            //    cmd.Parameters.AddWithValue("p_CurrentUserId", DBNull.Value);
            //}

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BENEFIT_INPUT_FIELD_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
        }
        errDS.Clear();
        errDS.AcceptChanges();
        return errDS;
    }
    private DataSet _getBenefitInputFieldListAll(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();
        LoanDS benefitDS = new LoanDS();
        DataSet returnDS = new DataSet();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();
        cmd = DBCommandProvider.GetDBCommand("GetBenefitInputFieldListAll");
        if (cmd == null) return UtilDL.GetCommandNotFound();

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.Fill(adapter, benefitDS, benefitDS.BenefittInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        if (bError)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.NA_ACTION_GET_BENEFIT_INPUT_FIELD_LIST_ALL.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }
        benefitDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        returnDS.Merge(benefitDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();
        return returnDS;
    }
    private DataSet _deleteBenefitInputfield(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataStringDS stringDS = new DataStringDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();
        cmd.CommandText = "PRO_BENEFIT_INPUTFIELD_DELETE";
        cmd.CommandType = CommandType.StoredProcedure;
        if (cmd == null) return UtilDL.GetCommandNotFound();

        cmd.Parameters.AddWithValue("p_FIELDNAME", (object)stringDS.DataStrings[0].StringValue);

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.ACTION_BENEFIT_INPUTFIELD_DEL.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }

        return errDS;  // return empty ErrorDS
    }
    private DataSet _updateBenefitInputField(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        LoanDS loanDS = new LoanDS();
        DBConnectionDS connDS = new DBConnectionDS();

        OleDbCommand cmd = new OleDbCommand();
        cmd.CommandText = "PRO_BENEFIT_INPUTFIELD_UPDATE";
        cmd.CommandType = CommandType.StoredProcedure;

        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        loanDS.Merge(inputDS.Tables[loanDS.BenefittInputField.TableName], false, MissingSchemaAction.Error);
        loanDS.AcceptChanges();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        foreach (LoanDS.BenefittInputFieldRow row in loanDS.BenefittInputField.Rows)
        {
            if (row.IsDATATYPENull() == false)
            {
                cmd.Parameters.AddWithValue("p_DATATYPE", row.DATATYPE);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_DATATYPE", DBNull.Value);
            }

            if (row.IsBIFREMARKSNull() == false)
            {
                cmd.Parameters.AddWithValue("p_BIFREMARKS", row.BIFREMARKS);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_BIFREMARKS", DBNull.Value);
            }

            if (row.IsFIELDNAMENull() == false)
            {
                cmd.Parameters.AddWithValue("p_FIELDNAME", row.FIELDNAME);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_FIELDNAME", DBNull.Value);
            }

            if (row.IsBIFCODENull() == false)
            {
                cmd.Parameters.AddWithValue("p_BIFCODE", row.BIFCODE);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_BIFCODE", DBNull.Value);
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BENEFIT_INPUT_FIELD_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
        }
        errDS.Clear();
        errDS.AcceptChanges();
        return errDS;
    }

    private DataSet _createBIFConfig(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        OleDbCommand cmd = new OleDbCommand();

        LoanDS loanDS = new LoanDS();
        loanDS.Merge(inputDS.Tables[loanDS.BIFConfig.TableName], false, MissingSchemaAction.Error);
        loanDS.AcceptChanges();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        bool bError = false;
        int nRowAffected = -1;

        #region Delete BIF Config
        cmd.CommandText = "PRO_BIFCONFIG_DELETE";
        cmd.CommandType = CommandType.StoredProcedure;

        foreach (LoanDS.BIFConfigRow row in loanDS.BIFConfig.Rows)
        {
            cmd.Parameters.AddWithValue("p_OthersBenefitID", row.OthersBenefitID);
            cmd.Parameters.AddWithValue("p_BenefitType", row.BenefitType);
            bError = false; nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BIFCONFIG_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            cmd.Parameters.Clear();
            break;
        }
        cmd.Dispose();
        #endregion

        #region Create BIF Config
        cmd.CommandText = "PRO_BIFCONFIG_CREATE";
        cmd.CommandType = CommandType.StoredProcedure;

        foreach (LoanDS.BIFConfigRow row in loanDS.BIFConfig.Rows)
        {
            // Update by Asif, 15-mar-12
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_OthersBenefitID", row.OthersBenefitID);
            cmd.Parameters.AddWithValue("p_BIFID", row.BIFID);
            cmd.Parameters.AddWithValue("p_ISMANDATORY", row.IsMandatory);
            cmd.Parameters.AddWithValue("p_AIFCONFIGID", genPK);
            cmd.Parameters.AddWithValue("p_LIMITMARKABLE", row.Limit_Markable);
            if(!row.IsBenefitTypeNull()) cmd.Parameters.AddWithValue("p_BenefitType", row.BenefitType);
            else cmd.Parameters.AddWithValue("p_BenefitType", DBNull.Value);

            bError = false; nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BIFCONFIG_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            cmd.Parameters.Clear();
        }
        #endregion

        errDS.Clear();
        errDS.AcceptChanges();
        return errDS;
    }
    private DataSet _deleteBIFConfig(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        //DataIntegerDS integerDS = new DataIntegerDS();
        //integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
        //integerDS.AcceptChanges();

        DataStringDS stringDS = new DataStringDS();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();
        cmd.CommandText = "PRO_BIFCONFIG_DELETE";
        cmd.CommandType = CommandType.StoredProcedure;

        if (cmd == null) return UtilDL.GetCommandNotFound();

        //cmd.Parameters.AddWithValue("p_ASSETTYPEID", (object)integerDS.DataIntegers[0].IntegerValue);
        cmd.Parameters.AddWithValue("p_ASSETTYPEID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
        cmd.Parameters.AddWithValue("p_BenefitType", stringDS.DataStrings[1].StringValue);

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.ACTION_BIFCONFIG_DELETE.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }

        return errDS;  // return empty ErrorDS
    }
    private DataSet _GetBenefitInputListByBenefitID(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();

        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        DataStringDS StringDS = new DataStringDS();
        StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        StringDS.AcceptChanges();

        OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBenefitFieldListByBenefitIDLoginID");
        if (cmd == null) return UtilDL.GetCommandNotFound();

        cmd.Parameters["UserLoginID"].Value = (object)StringDS.DataStrings[1].StringValue;
        cmd.Parameters["BenefitID"].Value = Convert.ToInt32(StringDS.DataStrings[0].StringValue.Trim());
        cmd.Parameters["BenefitType"].Value = StringDS.DataStrings[2].StringValue.Trim();

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        LoanDS loanDS = new LoanDS();
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.BenefittInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        if (bError)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.NA_ACTION_GET_BENEFIT_INPUTFIELD_LIST_BY_BENEFITID.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;

        }
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();

        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();

        return returnDS;
    } 
    private DataSet _GetBenefitFieldListByBenefitRequestID(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        DataStringDS StringDS = new DataStringDS();
        StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        StringDS.AcceptChanges();

        //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAssetFieldListByRequestID");
        OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBenefitFieldListByRequestID");
        cmd.Parameters["BenefitRequestID"].Value = (object)StringDS.DataStrings[0].StringValue;

        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;

        LoanDS loanDS = new LoanDS();
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.BenefittInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();

        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();

        return returnDS;
    }
    private DataSet _GetBenefitUsingLoginID(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        DataStringDS StringDS = new DataStringDS();
        StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        StringDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();

        #region Temporarily Modified

        if (StringDS.DataStrings[0].StringValue == "admin")
        {
            cmd = DBCommandProvider.GetDBCommand("GetBenefitsForAdmin");
        }
        else
        {
            cmd = DBCommandProvider.GetDBCommand("GetBenefitByLoginID");
            cmd.Parameters["LoginID1"].Value = (object)StringDS.DataStrings[0].StringValue;
            cmd.Parameters["LoginID2"].Value = (object)StringDS.DataStrings[0].StringValue;
            cmd.Parameters["LoginID3"].Value = (object)StringDS.DataStrings[0].StringValue;
        }

        #endregion


        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;
        ScaleOfPayDS loanDS = new ScaleOfPayDS();
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.OthersBenefit.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();
        return returnDS;
    }
    private DataSet _GetBenefitInputListByBenefitIDLoginID(DataSet inputDS) 
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        DataStringDS StringDS = new DataStringDS();
        StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        StringDS.AcceptChanges();

        //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAssetFieldListByATypeIDLoginID");
        OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBenefitFieldListByBenefitIDLoginID2");
        cmd.Parameters["UserLoginID"].Value = (object)StringDS.DataStrings[1].StringValue;
        cmd.Parameters["BenefitID"].Value = (object)StringDS.DataStrings[0].StringValue;

        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        LoanDS loanDS = new LoanDS();
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.BenefittInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        if (bError)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //err.ErrorInfo1 = ActionID.NA_ACTION_GET_ASS_INPUTFIELD_LIST_BY_ATYPEID.ToString();
            err.ErrorInfo1 = ActionID.NA_ACTION_GET_FIELD_LIST_BY_BENEFITID_LOGINID.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;

        }
        loanDS.AcceptChanges();

        errDS.Clear();
        errDS.AcceptChanges();
        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();
        return returnDS;
    }


    int p_othersBenefitID = 0;
    int p_ReceivedUserID = 0;
    int p_loanID = 0;
    string p_UserID;
    DateTime BenefitAppDate;

   
    private DataSet _GetBenefitActivityAll(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();

        DataStringDS StringDS = new DataStringDS();
        StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        StringDS.AcceptChanges();

        string activeStatus = StringDS.DataStrings[0].StringValue;
        string LoginID = StringDS.DataStrings[1].StringValue;
        string AssAuthorizationKey = StringDS.DataStrings[2].StringValue;


        if (LoginID != "admin" && AssAuthorizationKey == "False")
        {
            //cmd = DBCommandProvider.GetDBCommand("GetAssetActivitiesForGeneral");
            cmd = DBCommandProvider.GetDBCommand("GetBenefitActivitiesForGeneral");
        }
        else if (LoginID != "admin" && AssAuthorizationKey == "True")
        {
            //cmd = DBCommandProvider.GetDBCommand("GetAssetActForSupperVisor");
            cmd = DBCommandProvider.GetDBCommand("GetBenefitActForSupperVisor");
        }
        else
        {
            //cmd = DBCommandProvider.GetDBCommand("GetAssetActivitiesAll");
            cmd = DBCommandProvider.GetDBCommand("GetBenefitActivitiesAll_New");
        }
        LoginID = "";
        AssAuthorizationKey = "";

        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;

        LoanDS loanDS = new LoanDS();
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.Activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();

        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();

        return returnDS;
    }
    private DataSet _GetBenefitLimitByLoginID(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();

        DataStringDS StringDS = new DataStringDS();
        StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        StringDS.AcceptChanges();


        if (StringDS.DataStrings[0].StringValue == "Benefit") cmd = DBCommandProvider.GetDBCommand("GetBenefitLimitByLoginID");
        else if (StringDS.DataStrings[0].StringValue == "Loan") cmd = DBCommandProvider.GetDBCommand("GetLoanLimitByLoginID");
        cmd.Parameters["LoginID"].Value = (object)StringDS.DataStrings[1].StringValue;
        cmd.Parameters["BenefitID"].Value = Convert.ToInt32(StringDS.DataStrings[2].StringValue);

        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;

        LoanDS loanDS = new LoanDS();
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.LoanBenefitRule.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();

        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();

        return returnDS;
    }

    private DataSet _GetLoanApprovedEmployee(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();

        DataIntegerDS integerDS = new DataIntegerDS();
        integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
        integerDS.AcceptChanges();

        cmd = DBCommandProvider.GetDBCommand("GetLoanApprovedEmployee");
        cmd.Parameters["LoanID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;

        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;

        LoanDS loanDS = new LoanDS();
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.BenefitApply.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();

        return returnDS;
    }

    private DataSet _GetLoanBenefitRule(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();

        DataIntegerDS integerDS = new DataIntegerDS();
        integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
        integerDS.AcceptChanges();
        DataStringDS stringDS = new DataStringDS();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        if (stringDS.DataStrings[0].StringValue == "Loan")
        {
            cmd = DBCommandProvider.GetDBCommand("GetLoanRule");
            cmd.Parameters["LoanID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
        }
        else
        {
            cmd = DBCommandProvider.GetDBCommand("GetBenefitRule");
            cmd.Parameters["BenefitID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
        }

        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;

        LoanDS loanDS = new LoanDS();
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.LoanBenefitRule.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();

        return returnDS;
    }
    #endregion

    private DataSet _GetLoanBenefitRules_ALL(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();

        DataStringDS stringDS = new DataStringDS();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        cmd = DBCommandProvider.GetDBCommand("GetBenefitRule_ALL");
        cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

        if (cmd == null) return UtilDL.GetCommandNotFound();

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;

        LoanDS loanDS = new LoanDS();
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.LoanBenefitRule.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();

        return returnDS;
    }
    private DataSet _getLoanSettledInfo(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        LoanDS loanDS = new LoanDS();
        OleDbCommand cmd = new OleDbCommand();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        DataStringDS stringDS = new DataStringDS();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        bool isSalaryExcluded = Convert.ToBoolean(stringDS.DataStrings[5].StringValue);

        #region Get LoanSettlement Info
        if (!isSalaryExcluded) cmd = DBCommandProvider.GetDBCommand("GetLoanSettlementInfo");
        else cmd = DBCommandProvider.GetDBCommand("GetSalaryExcluded_LoanSettleInfo");

        if (cmd == null) return UtilDL.GetCommandNotFound();

        if (stringDS.DataStrings[0].IsStringValueNull() == false)
        {
            cmd.Parameters["LoanCode"].Value = (object)stringDS.DataStrings[0].StringValue;            
            cmd.Parameters["EmployeeCode"].Value = (object)stringDS.DataStrings[1].StringValue;
            //cmd.Parameters["DueInstallmentDate"].Value = (object)Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.Loans.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        if (bError == true)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.NA_ACTION_Q_LOAN_ALL_SETTLED.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }
        loanDS.AcceptChanges();
        #endregion

        #region Max Salary Date...............................
        DataDateDS DateDS = new DataDateDS();       

        cmd.Dispose();
        cmd = DBCommandProvider.GetDBCommand("GetMaxSalaryDate");
        if (cmd == null) return UtilDL.GetCommandNotFound();


        OleDbDataAdapter adapter1 = new OleDbDataAdapter();
        adapter1.SelectCommand = cmd;

        bError = false;
        nRowAffected = -1;
        nRowAffected = ADOController.Instance.Fill(adapter1, DateDS, DateDS.DataDates.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        if (bError == true)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.NA_ACTION_Q_LOAN_ALL_SETTLED.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }
        DateDS.AcceptChanges();
        #endregion

        #region Loan Overlapping info...............................

        DataStringDS StringDS = new DataStringDS();
        adapter1.Dispose();
        cmd.Dispose();
        cmd = DBCommandProvider.GetDBCommand("GetLoanOverlappingInfo");
        if (cmd == null) return UtilDL.GetCommandNotFound();        
        adapter1.SelectCommand = cmd;

        cmd.Parameters["EmployeeCode"].Value = (object)stringDS.DataStrings[1].StringValue;
        cmd.Parameters["LoanCode"].Value = (object)stringDS.DataStrings[0].StringValue;        
        cmd.Parameters["IssueDate"].Value = (object)Convert.ToDateTime(stringDS.DataStrings[2].StringValue);     
        cmd.Parameters["ScheduleDateFrom"].Value = (object)Convert.ToDateTime(stringDS.DataStrings[3].StringValue);
        cmd.Parameters["ScheduleDateTo"].Value = (object)Convert.ToDateTime(stringDS.DataStrings[4].StringValue);

        bError = false;
        nRowAffected = -1;
        nRowAffected = ADOController.Instance.Fill(adapter1, StringDS, StringDS.DataStrings.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        if (bError == true)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.NA_ACTION_Q_LOAN_ALL_SETTLED.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }
        StringDS.AcceptChanges();
        #endregion

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.Merge(DateDS);
        returnDS.Merge(StringDS);
        returnDS.AcceptChanges();
        return returnDS;
    }

    #region Staff Benefit Updated :: Rony :: 20 Dec 2017
    private DataSet _createBenefitApply(DataSet inputDS)
    {
        int priority = 0;
        ErrorDS errDS = new ErrorDS();
        LoanDS loanDS = new LoanDS();
        DBConnectionDS connDS = new DBConnectionDS();

        OleDbCommand cmd = new OleDbCommand();
        cmd.CommandText = "PRO_BENEFIT_APPLY";
        cmd.CommandType = CommandType.StoredProcedure;

        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }
        OleDbCommand cmd2 = new OleDbCommand();
        cmd2.CommandText = "PRO_BEN_REQUEST_HIST_CREATE";
        cmd2.CommandType = CommandType.StoredProcedure;

        if (cmd2 == null)
        {
            return UtilDL.GetCommandNotFound();
        }
        loanDS.Merge(inputDS.Tables[loanDS.BenefitApply.TableName], false, MissingSchemaAction.Error);
        loanDS.AcceptChanges();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        DataStringDS stringDS = new DataStringDS();

        long genPK = IDGenerator.GetNextGenericPK();
        if (genPK == -1)
        {
            return UtilDL.GetDBOperationFailed();
        }

        long genPK2 = IDGenerator.GetNextGenericPK();
        if (genPK2 == -1)
        {
            return UtilDL.GetDBOperationFailed();
        }

        string employeeName = "", employeeCode = "", activityName = "", benefitName = "",
            appliedFor = "", senderemail = "", receiverEmail = "";
        bool IsMailNotification = false;
        decimal actualAmount = 0, limit = 0;
        foreach (LoanDS.BenefitApplyRow row in loanDS.BenefitApply.Rows)
        {
            cmd.Parameters.AddWithValue("p_BEN_REQUESTID", genPK);

            if (row.IsOthersBenefitIDNull() == false)
            {
                cmd.Parameters.AddWithValue("p_OTHERSBENEFITID", row.OthersBenefitID);
                p_othersBenefitID = row.OthersBenefitID;
            }
            else
            {
                cmd.Parameters.AddWithValue("p_OTHERSBENEFITID", DBNull.Value);
            }

            if (row.IsCurrentUserIDNull() == false)
            {
                cmd.Parameters.AddWithValue("p_USERID", row.CurrentUserID);
                p_UserID = row.CurrentUserID;
            }
            else
            {
                cmd.Parameters.AddWithValue("p_USERID", DBNull.Value);
            }


            if (row.IsORIGINALVALUENull() == false)
            {
                cmd.Parameters.AddWithValue("p_ORIGINALVALUE", row.ORIGINALVALUE);
                try
                {
                    Convert.ToDecimal(row.ORIGINALVALUE);
                }
                catch { }
            }
            else
            {
                cmd.Parameters.AddWithValue("p_ORIGINALVALUE", DBNull.Value);
            }

            if (row.IsCHANGINGVALUENull() == false)
            {
                cmd.Parameters.AddWithValue("p_CHANGEVALUE", row.CHANGINGVALUE);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_CHANGEVALUE", DBNull.Value);
            }

            if (row.IsBIFIDNull() == false)
            {
                cmd.Parameters.AddWithValue("p_BIFID", row.BIFID);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_BIFID", DBNull.Value);
            }

            if (row.IsAppliedDateNull() == false)
            {
                BenefitAppDate = row.AppliedDate;
            }

            if (row.IsLoanIDNull() == false)
            {
                cmd.Parameters.AddWithValue("p_LoanID", row.LoanID);
                p_loanID = row.LoanID;
            }
            else
            {
                cmd.Parameters.AddWithValue("p_LoanID", DBNull.Value);
            }
            if (!row.IsMaximumValueNull()) cmd.Parameters.AddWithValue("p_MaximumValue", row.MaximumValue);
            else cmd.Parameters.AddWithValue("p_MaximumValue", DBNull.Value);
            
            cmd.Parameters.AddWithValue("p_Status", row.Status);
            cmd.Parameters.AddWithValue("p_AppliedFor", row.AppliedFor);

            if (!row.IsReceivedUserIDNull()) p_ReceivedUserID = row.ReceivedUserID;
            if (!row.IsEmployeeNameNull()) employeeName = row.EmployeeName;
            if (!row.IsEmployeeCodeNull()) employeeCode = row.EmployeeCode;
            if (!row.IsActivityFriendlyNameNull()) activityName = row.ActivityFriendlyName;
            if (!row.IsOthersBenefitNameNull()) benefitName = row.OthersBenefitName;
            if (!row.IsAppliedForNull()) appliedFor = row.AppliedFor;
            if (!row.IsPriorityNull()) priority = row.Priority;
            if (!row.IsMailNotificationNull()) IsMailNotification = row.MailNotification;
            if (!row.IsEmailNull()) senderemail = row.Email;
            if (!row.IsReceivedUserEmailNull()) receiverEmail = row.ReceivedUserEmail;

            if (!row.IsActualAmountNull()) actualAmount = row.ActualAmount;
            if (!row.IsLimitNull()) limit = row.Limit;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BENEFIT_APPLY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            stringDS.DataStrings.AddDataString(genPK.ToString());
        }

        cmd2.Parameters.AddWithValue("P_BEN_REQUESTID", genPK);

        if (p_othersBenefitID != 0) cmd2.Parameters.AddWithValue("P_OTHERSBENEFITID", p_othersBenefitID);
        else cmd2.Parameters.AddWithValue("P_OTHERSBENEFITID", DBNull.Value);

        cmd2.Parameters.AddWithValue("P_USERID", p_UserID);
        cmd2.Parameters.AddWithValue("P_ReceivedUserID", p_ReceivedUserID);
        cmd2.Parameters.AddWithValue("P_BEN_HISTORYID", genPK2);
        cmd2.Parameters.AddWithValue("P_ACTIVITIESID", Convert.ToInt32(WorkFlowBasedActivity.PendingApproval));
        cmd2.Parameters.AddWithValue("p_BENEFITAPPLYDATE", BenefitAppDate);

        if (p_loanID != 0) cmd2.Parameters.AddWithValue("P_LOANID", p_loanID);
        else cmd2.Parameters.AddWithValue("P_LOANID", DBNull.Value);
        cmd2.Parameters.AddWithValue("p_Priority", priority);

        if (actualAmount != 0) cmd2.Parameters.AddWithValue("P_ACTUALAMOUNT", actualAmount);
        else cmd2.Parameters.AddWithValue("P_ACTUALAMOUNT", DBNull.Value);

        if (limit != 0) cmd2.Parameters.AddWithValue("P_LIMIT", limit);
        else cmd2.Parameters.AddWithValue("P_LIMIT", DBNull.Value);

        int nRowAffected2 = -1;
        bool bError2 = false;

        nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd2, connDS.DBConnections[0].ConnectionID, ref bError2);
        cmd2.Parameters.Clear();

        if (bError2)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.ACTION_BENEFIT_APPLY.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }

        if (IsMailNotification)
        { 
            #region Get Email Information from UtilDL
            string messageBody = "", URL = "", mailSubject = "";
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion
            #region Email Body Creation...........

            messageBody += "<b>Applicant: </b>[" + employeeCode + "] " + employeeName + "<br>";
            messageBody += "<b>Applied Benefit : </b>" +  benefitName + " [" + appliedFor + "]<br>";
            messageBody += "<b>Applied Status : </b>" + activityName  + "<br>";
            messageBody += "<br><br><br>";
            messageBody += "Please login into HRMS system for details.";
            messageBody += "<br>";

            #endregion
            #region Email Send............
            Mail.Mail mail = new Mail.Mail();
            string returnMessage = "";
            mailSubject = "Application for Staff Benefit [" + benefitName + " - " + appliedFor + "]";
            
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, credentialEmailAddress, receiverEmail, "", mailSubject, messageBody, employeeName);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, senderemail, receiverEmail, "", mailSubject, messageBody, employeeName);
                }

                if (returnMessage != "Email successfully sent.")
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BENEFIT_APPLY_APPROVATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            
            #endregion
        }


        errDS.Clear();
        errDS.AcceptChanges();
        //return errDS;

        // Update by Asif, 14-mar-12
        DataSet returnDS = new DataSet();

        returnDS.Merge(loanDS);
        returnDS.Merge(stringDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();

        return returnDS;
        // Update end
    }
    private DataSet _GetBenefitAppliedData(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();

        DataStringDS stringDS = new DataStringDS();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        //string activeStatus = stringDS.DataStrings[0].StringValue;

        //cmd = DBCommandProvider.GetDBCommand("GetAssetAppliedSummary");
        //cmd = DBCommandProvider.GetDBCommand("GetBenefitAppliedSummary"); // Commented By Rony :: 02 Jan 2018

        #region Updated by Rony :: 02 Jan 2018
        if (stringDS.DataStrings[0].StringValue == "BenefitApproval")
        {
            cmd = DBCommandProvider.GetDBCommand("GetBenefitApprovalInfo");
            if (Convert.ToBoolean(stringDS.DataStrings[10].StringValue))
            {
                cmd.Parameters["EmployeeCode1"].Value =  cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["AppHolderCode1"].Value = cmd.Parameters["AppHolderCode2"].Value = "-1";
            }
            else
            {
                cmd.Parameters["EmployeeCode1"].Value  =  cmd.Parameters["EmployeeCode2"].Value = "-1";
                cmd.Parameters["AppHolderCode1"].Value = cmd.Parameters["AppHolderCode2"].Value = stringDS.DataStrings[1].StringValue;
            }
            
            cmd.Parameters["ApplicantCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["ApplicantCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["ApplicantName"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["SiteID1"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
            cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
            cmd.Parameters["CompanyDivisionID1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            cmd.Parameters["CompanyDivisionID2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            cmd.Parameters["DepartmentID1"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
            cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
            cmd.Parameters["DesignationID1"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);
            cmd.Parameters["DesignationID2"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);
            cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[8].StringValue);
            cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[9].StringValue);
            cmd.Parameters["ActivitiesID1"].Value = stringDS.DataStrings[11].StringValue;
            cmd.Parameters["ActivitiesID2"].Value = stringDS.DataStrings[11].StringValue;
        }
        else if (stringDS.DataStrings[0].StringValue == "BenefitRequest")
        {
            cmd = DBCommandProvider.GetDBCommand("GetLoanRequestDataByEmployeeID");
            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
        }
        #endregion

        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;

        LoanDS loanDS = new LoanDS();

        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.BenefitApply.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();
        return returnDS;
    }

    private DataSet _BenefitApplyApprovation(DataSet inputDS)
    {
        

        //--------------------------------------------------------
        ErrorDS errDS = new ErrorDS();
        MessageDS messageDS = new MessageDS();
        DataSet returnDS = new DataSet();
        DBConnectionDS connDS = new DBConnectionDS();

        //cmd.CommandText = "PRO_REQUISITION_APPROVATION";
        //cmd.CommandType = CommandType.StoredProcedure;

        //if (cmd == null)
        //{
        //    return UtilDL.GetCommandNotFound();
        //}

        LoanDS loanDS = new LoanDS();
        //loanDS.Merge(inputDS.Tables[loanDS.BenefitApplyHistory.TableName], false, MissingSchemaAction.Error);
        loanDS.Merge(inputDS.Tables[loanDS.BenefitApply.TableName], false, MissingSchemaAction.Error);
        loanDS.AcceptChanges();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();
        cmd.CommandText = "PRO_BENEFIT_APPLY_APPROVE";
        cmd.CommandType = CommandType.StoredProcedure;

        foreach (LoanDS.BenefitApplyRow row in loanDS.BenefitApply.Rows)
        {
            //if (row.ActivitiesID == 101 || row.ActivitiesID == 104 || row.ActivitiesID == 105)
            //{
            //    //cmd.CommandText = "PRO_ASSET_APPROVE_FINISHING";
            //    cmd.CommandText = "PRO_BENEFIT_APPROVE_FINISHING";
            //    cmd.CommandType = CommandType.StoredProcedure;
            //}
            //else
            //{
            //    //cmd.CommandText = "PRO_ASSET_APPLY_APPROVE";
            //    cmd.CommandText = "PRO_BENEFIT_APPLY_APPROVE";
            //    cmd.CommandType = CommandType.StoredProcedure;
            //}

            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_BenHistoryID", genPK);

            if (!row.IsBenefitRequestIDNull()) cmd.Parameters.AddWithValue("p_BenRequestID", row.BenefitRequestID);
            else cmd.Parameters.AddWithValue("p_BenRequestID", DBNull.Value);

            if (!row.IsSenderUserIDNull()) cmd.Parameters.AddWithValue("p_SendUserID", row.SenderUserID);
            else cmd.Parameters.AddWithValue("p_SendUserID", DBNull.Value);

            if (!row.IsReceivedUserIDNull()) cmd.Parameters.AddWithValue("p_ReceivedUserID", row.ReceivedUserID);
            else cmd.Parameters.AddWithValue("p_ReceivedUserID", DBNull.Value);

            //if (!row.IsLoginIDNull()) cmd.Parameters.AddWithValue("p_LoginID", row.LoginID);
            //else cmd.Parameters.AddWithValue("p_LoginID", DBNull.Value);

            if (!row.IsBenefitHistoryCommentsNull()) cmd.Parameters.AddWithValue("p_BenefitHistoryComments", row.BenefitHistoryComments);
            else cmd.Parameters.AddWithValue("p_BenefitHistoryComments", DBNull.Value);

            if (!row.IsActivitiesIDNull()) cmd.Parameters.AddWithValue("p_ActivityID", row.ActivitiesID);
            else cmd.Parameters.AddWithValue("p_ActivityID", DBNull.Value);

            if (!row.IsPriorityNull()) cmd.Parameters.AddWithValue("p_Priority", row.Priority);
            else cmd.Parameters.AddWithValue("p_Priority", DBNull.Value);

            if (!row.IsChangedAmountNull()) cmd.Parameters.AddWithValue("p_ChangedAmount", row.ChangedAmount);
            else cmd.Parameters.AddWithValue("p_ChangedAmount", DBNull.Value);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + "[" + row.AppliedFor + "]" + " ==> " + row.AuthorityUserCode + " - " + row.AuthorityUserName);
            else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + "[" + row.AppliedFor + "]" + " ==> " + row.AuthorityUserCode + " - " + row.AuthorityUserName);
            messageDS.AcceptChanges();

            
            cmd.Parameters.Clear();
            if (!bError)
            {
                #region Get Email Information from UtilDL
                string messageBody = "", URL = "", mailSubject = "";
                string host = UtilDL.GetHost();
                Int32 port = UtilDL.GetPort();
                bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
                string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
                string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
                bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
                #endregion
                #region Email Body Creation...........
                //URL = row.Url;
                //URL += "?pVal=StaffBenefit";

                messageBody += "<b>Applicant: </b>[" + row.EmployeeCode + "] " + row.EmployeeName + "<br>";
                messageBody += "<b>Applied Benefit : </b>" + row.OthersBenefitName + " [" + row.AppliedFor + "]<br>";
                messageBody += "<b>Applied Status : </b>" + row.ActivityFriendlyName + "<br>";
                messageBody += "<br><br><br>";
                messageBody += "Please login into HRMS system for details.";
                messageBody += "<br>";
                //messageBody += "<a href='" + URL + "'>" + URL + "</a>";

                #endregion
                #region Email Send............
                Mail.Mail mail = new Mail.Mail();
                string returnMessage = "";
                mailSubject = "Application for Staff Benefit [" + row.OthersBenefitName + " - " + row.AppliedFor + "]";
                if (!row.IsMailNotificationNull())
                {
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, credentialEmailAddress, row.Email, "", mailSubject, messageBody, row.EmployeeName);
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, row.Email, row.ReceivedUserEmail, "", mailSubject, messageBody, row.EmployeeName);
                    }

                    if (returnMessage != "Email successfully sent.")
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BENEFIT_APPLY_APPROVATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion
            }
            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.ACTION_BENEFIT_APPLY_APPROVATION.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}
        }
        errDS.Clear();
        errDS.AcceptChanges();
        returnDS.Merge(errDS);
        returnDS.Merge(messageDS);
        return returnDS;
    }
    private DataSet _createBenefitApprovalAuthority(DataSet inputDS)
    {
        bool bError = false;
        int nRowAffected = -1;
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        LoanDS loanDS = new LoanDS();
        OleDbCommand cmd;

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();
        loanDS.Merge(inputDS.Tables[loanDS.Benefit_ApprovalAuthority.TableName], false, MissingSchemaAction.Error);
        loanDS.AcceptChanges();

        #region Delete Old Data
        cmd = new OleDbCommand();
        cmd.CommandText = "PRO_BENEFIT_APP_PATH_DELETE";
        cmd.CommandType = CommandType.StoredProcedure;

        bError = false;
        nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
        cmd.Dispose();
        if (bError == true)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.ACTION_BENEFIT_APPROVAL_AUTHOR_SAVE.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }
        #endregion

        #region Save new data
        cmd = new OleDbCommand();
        cmd.CommandText = "PRO_BENEFIT_APP_PATH_SAVE";
        cmd.CommandType = CommandType.StoredProcedure;

        OleDbCommand cmd_Activity = new OleDbCommand();
        cmd_Activity.CommandText = "PRO_BENEFIT_APP_PATH_ACTIVITY";
        cmd_Activity.CommandType = CommandType.StoredProcedure;

        foreach (LoanDS.Benefit_ApprovalAuthorityRow row in loanDS.Benefit_ApprovalAuthority.Rows)
        {
            string[] Authority = row.Authority.Split(' ', '@');
            int loginID = 0;
            if (Authority.Length == 1) loginID = 1;
            else loginID = 0;

            long pKey = IDGenerator.GetNextGenericPK();
            if (pKey == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_ConfigurationID", pKey);
            cmd.Parameters.AddWithValue("p_Priority", row.Priority);
            cmd.Parameters.AddWithValue("p_Authority", row.Authority);
            cmd.Parameters.AddWithValue("p_IsLoginUserID", loginID);

            if (!row.IsDepartmentIDNull()) cmd.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
            else cmd.Parameters.AddWithValue("p_DepartmentID", DBNull.Value);
            if (!row.IsLiabilityIDNull()) cmd.Parameters.AddWithValue("p_LiabilityID", row.LiabilityID);
            else cmd.Parameters.AddWithValue("p_LiabilityID", DBNull.Value);

            cmd.Parameters.AddWithValue("p_AuthorityType", row.AuthorityType);
            cmd.Parameters.AddWithValue("p_IsAbleToReview", row.IsAbleToReview);
            cmd.Parameters.AddWithValue("p_IsAbleToIssue", row.IsAbleToIssue); 
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (!bError)
            {
                string[] activiy = row.ActivityName.Split(',', ' ');
                for (int index = 0; index < activiy.Length; index++)
                {
                    cmd_Activity.Parameters.AddWithValue("p_ConfigurationID", pKey);
                    cmd_Activity.Parameters.AddWithValue("p_Activity", activiy[index].ToString().Trim());
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Activity, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Activity.Parameters.Clear();
                }
            }

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BENEFIT_APPROVAL_AUTHOR_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
        }
        #endregion

        errDS.Clear();
        errDS.AcceptChanges();
        return errDS;
    }
    private DataSet _GetBenefitApproval(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();

        DataStringDS stringDS = new DataStringDS();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        if (stringDS.DataStrings[0].StringValue == "ApprovalAuthorityAll") cmd = DBCommandProvider.GetDBCommand("GetBenefitApprovalAuthority");
        if (stringDS.DataStrings[0].StringValue == "ApprovalAuthorityByPriority")
        {
            cmd = DBCommandProvider.GetDBCommand("GetApprovalAuthorityByPriority");
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["ApproverCode"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["Priority1"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
            cmd.Parameters["Priority2"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
        }
        if (cmd == null) return UtilDL.GetCommandNotFound();

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;

        LoanDS loanDS = new LoanDS();
        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.Benefit_ApprovalAuthority.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();

        return returnDS;
    }
    private DataSet _GetLoanBenefitData(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        OleDbCommand cmd = new OleDbCommand();

        DataStringDS stringDS = new DataStringDS();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        if (stringDS.DataStrings[0].StringValue == "LoanRuleWiseLoanByEmployeeCode")
        {
            cmd = DBCommandProvider.GetDBCommand("GetLoanRuleWiseLoanByEmployeeCode");
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode3"].Value = (object)stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode4"].Value = (object)stringDS.DataStrings[1].StringValue;
        }
        else if(stringDS.DataStrings[0].StringValue == "LoanByEmployeeCode")
        {
            cmd = DBCommandProvider.GetDBCommand("GetLoanByEmployeeID");
            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
        }
        else if (stringDS.DataStrings[0].StringValue == "LoanWithoutApproval")
        {
            cmd = DBCommandProvider.GetDBCommand("GetLoanListWithoutApproval");
        }
        else if (stringDS.DataStrings[0].StringValue == "LoanListApproval")
        {
            cmd = DBCommandProvider.GetDBCommand("GetLoanListApproval");
        }
        //GetLoanListWithoutApproval
        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        bool bError = false;
        int nRowAffected = -1;

        LoanDS loanDS = new LoanDS();

        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.Loans.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();


        //kamal
        cmd.Dispose();
        cmd = DBCommandProvider.GetDBCommand("GetLastLoanIssueID");
        adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;
        bError = false;
        nRowAffected = -1;

        nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.LoanIssue.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        loanDS.AcceptChanges();

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();
        returnDS.Merge(loanDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();
        return returnDS;
    }
      //
    #endregion
  }
}
