/* *********************************************************************
 *  Compamy: Milllennium Information Solution Limited		 		           *  
 *  Author: Md. Hasinur Rahman Likhon				 				                   *
 *  Comment Date: March 29, 2006									                     *
 ***********************************************************************/

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for FunctionDL.
    /// </summary>
    public class FunctionDL
    {
        public FunctionDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_FUNCTION_ADD:
                    return _createFunction(inputDS);
                case ActionID.NA_ACTION_GET_FUNCTION_LIST:
                    return _getFunctionList(inputDS);
                case ActionID.ACTION_FUNCTION_DEL:
                    return this._deleteFunction(inputDS);
                case ActionID.ACTION_FUNCTION_UPD:
                    return this._updateFunction(inputDS);
                case ActionID.ACTION_DOES_FUNCTION_EXIST:
                    return this._doesFunctionExist(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }
        private DataSet _createFunction(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            FunctionDS funDS = new FunctionDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateFunction");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            funDS.Merge(inputDS.Tables[funDS.Functions.TableName], false, MissingSchemaAction.Error);
            funDS.AcceptChanges();
            foreach (FunctionDS.Function fun in funDS.Functions)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters["Id"].Value = (object)genPK;

                if (fun.IsFunctionCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)fun.FunctionCode;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }
                if (fun.IsFunctionNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = (object)fun.FunctionName;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }
                if (fun.IsFunctionIsActiveNull() == false)
                {
                    cmd.Parameters["IsActive"].Value = (object)fun.FunctionIsActive;
                }
                else
                {
                    cmd.Parameters["IsActive"].Value = null;
                }

                if (fun.IsDepartmentCodeNull() == false)
                {
                    cmd.Parameters["DepartmentCode"].Value = (object)fun.DepartmentCode;
                }
                else
                {
                    cmd.Parameters["DepartmentCode"].Value = DBNull.Value;
                }

                cmd.Parameters["DefaultSelection"].Value = (object)fun.DefaultSelection;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && fun.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LOCATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getFunctionList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFunctionList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            FunctionPO funPO = new FunctionPO();
            FunctionDS funDS = new FunctionDS();

            bool bError = false;
            int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, funPO, funPO.Functions.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            nRowAffected = ADOController.Instance.Fill(adapter, funDS, funDS.Functions.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_FUNCTION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            //funPO.AcceptChanges();
            funDS.AcceptChanges();

            #region Commented :: WALI :: 29-Jul-2015
            //if (funPO.Functions.Count > 0)
            //{
            //    foreach (FunctionPO.Function poFun in funPO.Functions)
            //    {
            //        FunctionDS.Function fun = funDS.Functions.NewFunction();
            //        fun.FunctionCode = poFun.Functioncode;
            //        fun.FunctionName = poFun.Functionname;
            //        fun.FunctionIsActive = poFun.IsActive;
            //        if (poFun.IsDepartmentcodeNull() == false) fun.DepartmentCode = poFun.Departmentcode;
            //        else fun.DepartmentCode = "0";
            //        funDS.Functions.AddFunction(fun);
            //        funDS.AcceptChanges();
            //    }
            //}
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(funDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    

        }
        private DataSet _deleteFunction(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteFunction");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            int nRowAffectedRec = -1;
            if (connDS.DBConnections.Rows.Count == 2)
            {
                nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
            }
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LOCATION_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _updateFunction(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            FunctionDS funDS = new FunctionDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            funDS.Merge(inputDS.Tables[funDS.Functions.TableName], false, MissingSchemaAction.Error);
            funDS.AcceptChanges();


            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_FUN";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (FunctionDS.Function fun in funDS.Functions)
            {
                if (fun.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_FUNCTION_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion



            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateFunction");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            foreach (FunctionDS.Function fun in funDS.Functions)
            {
                if (fun.IsFunctionCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)fun.FunctionCode;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }
                if (fun.IsFunctionNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = (object)fun.FunctionName;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }
                if (fun.IsFunctionIsActiveNull() == false)
                {
                    cmd.Parameters["IsActive"].Value = (object)fun.FunctionIsActive;
                }
                else
                {
                    cmd.Parameters["IsActive"].Value = null;
                }

                if (fun.IsDepartmentCodeNull() == false)
                {
                    cmd.Parameters["DepartmentCode"].Value = (object)fun.DepartmentCode;
                }
                else
                {
                    cmd.Parameters["DepartmentCode"].Value = DBNull.Value;
                }

                cmd.Parameters["DefaultSelection"].Value = (object)fun.DefaultSelection;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && fun.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LOCATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _doesFunctionExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesFunctionExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

    }
}
