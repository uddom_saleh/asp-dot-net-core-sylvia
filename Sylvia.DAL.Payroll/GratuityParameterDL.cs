/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: April 16, 2006
 * Revised: Khandoker Md jarif
 * Comment Date: October 10, 2009
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class GratuityParameterDL
    {
        public GratuityParameterDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_GRATUITYPARAMETER_ADD:
                    return _createGratuityParameter(inputDS);
                case ActionID.ACTION_GRATUITYPROVISION_ADD:
                    return _saveGratuityProvision(inputDS);

                case ActionID.NA_ACTION_Q_ALL_GRATUITYPARAMETER:
                    return _getGratuityParameterList(inputDS);
                case ActionID.NA_ACTION_GET_GRATUITY_PARAM_RULE:
                    return this._getGratuityParameterRule(inputDS);
                case ActionID.NA_ACTION_GET_GRATUITYPARAMETER:
                    return this._getGratuityParameter(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }
        private DataSet _saveGratuityProvision(DataSet inputDS)
        {
            GratuityProvisionDS paramDS = new GratuityProvisionDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ErrorDS errDS = new ErrorDS();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateGratuityProvision");
            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            paramDS.Merge(inputDS.Tables[paramDS.GratuityProvisions.TableName], false, MissingSchemaAction.Error);
            paramDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (GratuityProvisionDS.GratuityProvision param in paramDS.GratuityProvisions)
            {

                if (param.IsEmployeeCodeNull() == false)
                {
                    cmd.Parameters["EmployeeId"].Value = UtilDL.GetEmployeeId(connDS, param.EmployeeCode);
                }
                else
                {
                    cmd.Parameters["EmployeeId"].Value = null;
                }
                if (param.IsMonthDateNull() == false)
                {
                    cmd.Parameters["MonthDate"].Value = param.MonthDate;
                }
                else
                {
                    cmd.Parameters["MonthDate"].Value = null;
                }
                if (param.IsCostCenterCodeNull() == false)
                {
                    cmd.Parameters["CostCenterId"].Value = UtilDL.GetCostCenterId(connDS, param.CostCenterCode);
                }
                else
                {
                    cmd.Parameters["CostCenterId"].Value = null;
                }
                if (param.IsAmountNull() == false)
                {
                    cmd.Parameters["Amount"].Value = param.Amount;
                }
                else
                {
                    cmd.Parameters["Amount"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GRATUITYPROVISION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();

            return errDS;
        }

        private DataSet _doesPFParameterExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesPFParameterExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (dateDS.DataDates[0].IsDateValueNull() == false)
            {
                cmd.Parameters["EffectDate"].Value = dateDS.DataDates[0].DateValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createGratuityParameter(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            errDS = _deleteParameter(inputDS);

            if (errDS.Errors.Count > 0)
            {
                return UtilDL.GetDBOperationFailed();

            }
            GratuityParameterDS paramDS = new GratuityParameterDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateGratuityParameter");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            paramDS.Merge(inputDS.Tables[paramDS.GratuityParameters.TableName], false, MissingSchemaAction.Error);
            paramDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            long RuleID = 0;

            if (paramDS.GratuityParameters[0].IsRuleIDNull() == false) RuleID = paramDS.GratuityParameters[0].RuleID;
            else RuleID = IDGenerator.GetNextGenericPK();
            if (RuleID == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            foreach (GratuityParameterDS.GratuityParameter param in paramDS.GratuityParameters)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }


                cmd.Parameters["ParameterId"].Value = genPK;

                if (param.IsEntitleServiceYearNull() == false)
                {
                    cmd.Parameters["EntitleServiceYears"].Value = param.EntitleServiceYear;
                }
                else
                {
                    cmd.Parameters["EntitleServiceYears"].Value = DBNull.Value;
                }
                if (param.IsNumberOfBasicNull() == false)
                {
                    cmd.Parameters["NoOfBasic"].Value = param.NumberOfBasic;
                }
                else
                {
                    cmd.Parameters["NoOfBasic"].Value = DBNull.Value;
                }

                //mislbd.Jarif Update Start(10-October-2009)
                if (param.IsPercentOfEntitledNull() == false)
                {
                    cmd.Parameters["PercentOfEntitled"].Value = param.PercentOfEntitled;
                }
                else
                {
                    cmd.Parameters["PercentOfEntitled"].Value = DBNull.Value;
                }
                //mislbd.Jarif Update End(10-October-2009)


                if (param.IsYearOfExperienceNull() == false)
                {
                    cmd.Parameters["YearOfExperience"].Value = param.YearOfExperience;
                }
                else
                {
                    cmd.Parameters["YearOfExperience"].Value = DBNull.Value;
                }

                cmd.Parameters["BasedOn"].Value = param.BasedOn;
                cmd.Parameters["DayConsideration"].Value = param.DayConsideration;
                if (param.IsRuleNameNull() == false)
                {
                    cmd.Parameters["RuleName"].Value = param.RuleName;
                }
                else
                {
                    cmd.Parameters["RuleName"].Value = DBNull.Value;
                }
                cmd.Parameters["RuleID"].Value = RuleID;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GRATUITYPARAMETER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                   // return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(param.RuleName);
                else messageDS.ErrorMsg.AddErrorMsgRow(param.RuleName);
               
            }
            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;

        }

        private ErrorDS _deleteParameter(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            GratuityParameterDS paramDS = new GratuityParameterDS();        

            paramDS.Merge(inputDS.Tables[paramDS.GratuityParameters.TableName], false, MissingSchemaAction.Error);
            paramDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteGratuityParameter");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            if (paramDS.GratuityParameters[0].IsRuleIDNull() == false)
            {
                cmd.Parameters["RuleID"].Value = paramDS.GratuityParameters[0].RuleID;
            }
            else
            {
                cmd.Parameters["RuleID"].Value = DBNull.Value;
            }
           

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getGratuityParameterList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGratuityParameterList_New");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //GratuityPO paramPO = new GratuityPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, paramPO, paramPO.GratuityParameters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_GRATUITYPARAMETER.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //paramPO.AcceptChanges();

            //GratuityParameterDS paramDS = new GratuityParameterDS();

            //if (paramPO.GratuityParameters.Count > 0)
            //{

            //    foreach (GratuityPO.GratuityParameter poParam in paramPO.GratuityParameters)
            //    {
            //        GratuityParameterDS.GratuityParameter param = paramDS.GratuityParameters.NewGratuityParameter();
            //        if (poParam.IsEntitleServiceYearNull() == false)
            //        {
            //            param.EntitleServiceYear = poParam.EntitleServiceYear;
            //        }
            //        if (poParam.IsNumberOfBasicNull() == false)
            //        {
            //            param.NumberOfBasic = poParam.NumberOfBasic;
            //        }
            //        //mislbd.Jarif Update Start(10-October-2009)
            //        if (poParam.IsPercentOfEntitledNull() == false)
            //        {
            //            param.PercentOfEntitled = poParam.PercentOfEntitled;
            //        }
            //        //mislbd.Jarif Update End(10-October-2009)

            //        paramDS.GratuityParameters.AddGratuityParameter(param);
            //        paramDS.AcceptChanges();
            //    }
            //}
            #endregion

            GratuityParameterDS paramDS = new GratuityParameterDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, paramDS, paramDS.GratuityParameters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_GRATUITYPARAMETER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            paramDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(paramDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _getGratuityParameterRule(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGratuityParameterRule");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            GratuityParameterDS paramDS = new GratuityParameterDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, paramDS, paramDS.GratuityParameters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_GRATUITYPARAMETER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            paramDS.AcceptChanges();         

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(paramDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getGratuityParameter(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGratuityParameter");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
         
            cmd.Parameters["RuleID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);           
           

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            GratuityParameterDS paramDS = new GratuityParameterDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, paramDS, paramDS.GratuityParameters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_GRATUITYPARAMETER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            paramDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(paramDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

    }
}
