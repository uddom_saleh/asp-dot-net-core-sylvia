using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for LocationDL.
    /// </summary>
    public class LocationDL
    {
        public LocationDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_LOCATION_ADD:
                    return _createLocation(inputDS);
                case ActionID.NA_ACTION_GET_LOCATION_LIST:
                    return _getLocationList(inputDS);
                case ActionID.ACTION_LOCATION_DEL:
                    return this._deleteLocation(inputDS);
                case ActionID.ACTION_LOCATION_UPD:
                    return this._updateLocation(inputDS);
                case ActionID.ACTION_DOES_LOCATION_EXIST:
                    return this._doesLocationExist(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _createLocation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            LocationDS locDS = new LocationDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateLocation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            locDS.Merge(inputDS.Tables[locDS.Locations.TableName], false, MissingSchemaAction.Error);
            locDS.AcceptChanges();

            foreach (LocationDS.Location loc in locDS.Locations)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["Id"].Value = (object)genPK;
                if (loc.IsCodeNull() == false) cmd.Parameters["Code"].Value = (object)loc.Code;
                else cmd.Parameters["Code"].Value = null;

                if (loc.IsNameNull() == false) cmd.Parameters["Name"].Value = (object)loc.Name;
                else cmd.Parameters["Name"].Value = null;

                if (loc.IsDescriptionNull() == false) cmd.Parameters["Description"].Value = (object)loc.Description;
                else cmd.Parameters["Description"].Value = null;

                if (loc.IsCompanyIDNull() == false) cmd.Parameters["CompanyId"].Value = (object)loc.CompanyID;
                else cmd.Parameters["CompanyId"].Value = null;

                if (loc.IsDivisionIDNull() == false) cmd.Parameters["DivisionId"].Value = (object)loc.DivisionID;
                else cmd.Parameters["DivisionId"].Value = null;

                cmd.Parameters["DefaultSelection"].Value = (object)loc.DefaultSelection;

                if (loc.IsLocationName_NativeLangNull() == false) cmd.Parameters["LocationName_Nativelang"].Value = (object)loc.LocationName_NativeLang;
                else cmd.Parameters["LocationName_Nativelang"].Value = null;

                if (loc.IsDivision_LocationIDNull() == false) cmd.Parameters["Division_LocationID"].Value = (object)loc.Division_LocationID;
                else cmd.Parameters["Division_LocationID"].Value = null;
                

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LOCATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteLocation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteLocation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BONUS_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            return errDS;  // return empty ErrorDS
        }

        private DataSet _getLocationList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLocationList");//By Jarif(28 Sep 11)
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLocationList_New");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(28 Sep 11)...
            #region Old...
            //LocationPO locPO = new LocationPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, locPO, locPO.Locations.TableName,  connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_LOCATION_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //locPO.AcceptChanges();

            //LocationDS locDS = new LocationDS();
            //if (locPO.Locations.Count > 0)
            //{


            //    foreach (LocationPO.Location poLoc in locPO.Locations)
            //    {
            //        LocationDS.Location loc = locDS.Locations.NewLocation();
            //        if (poLoc.IsCodeNull() == false)
            //        {
            //            loc.Code = poLoc.Code;
            //        }
            //        if (poLoc.IsNameNull() == false)
            //        {
            //            loc.Name = poLoc.Name;
            //        }
            //        if (poLoc.IsDescriptionNull() == false)
            //        {
            //            loc.Description = poLoc.Description;
            //        }
            //        locDS.Locations.AddLocation(loc);
            //        locDS.AcceptChanges();
            //    }
            //}
            #endregion

            LocationDS locDS = new LocationDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, locDS, locDS.Locations.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LOCATION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            locDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(locDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _updateLocation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            LocationDS locDS = new LocationDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            locDS.Merge(inputDS.Tables[locDS.Locations.TableName], false, MissingSchemaAction.Error);
            locDS.AcceptChanges();


            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_LOC";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (LocationDS.Location loc in locDS.Locations)
            {
                if (loc.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LOCATION_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateLocation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            foreach (LocationDS.Location loc in locDS.Locations)
            {
                if (loc.IsCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)loc.Code;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }

                if (loc.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = (object)loc.Name;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }

                if (loc.IsDescriptionNull() == false)
                {
                    cmd.Parameters["Description"].Value = (object)loc.Description;
                }

                else
                {
                    cmd.Parameters["Description"].Value = null;
                }

                if (loc.IsCompanyIDNull() == false) cmd.Parameters["CompanyId"].Value = (object)loc.CompanyID;
                else cmd.Parameters["CompanyId"].Value = null;

                if (loc.IsDivisionIDNull() == false) cmd.Parameters["DivisionId"].Value = (object)loc.DivisionID;
                else cmd.Parameters["DivisionId"].Value = null;

                cmd.Parameters["DefaultSelection"].Value = (object)loc.DefaultSelection;

                if (loc.IsLocationName_NativeLangNull() == false) cmd.Parameters["LocationName_Nativelang"].Value = (object)loc.LocationName_NativeLang;
                else cmd.Parameters["LocationName_Nativelang"].Value = null;

                if (loc.IsDivision_LocationIDNull() == false) cmd.Parameters["Division_LocationID"].Value = (object)loc.Division_LocationID;
                else cmd.Parameters["Division_LocationID"].Value = null;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LOCATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _doesLocationExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesLocationExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        
    }
}
