/* *********************************************************************
 *  Compamy: Milllennium Information Solution Limited		 		           *  
 *  Author: Md. Hasinur Rahman Likhon				 				                   *
 *  Comment Date: March 29, 2006									                     *
 ***********************************************************************/

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DesignationDL.
    /// </summary>
    public class DesignationDL
    {
        public DesignationDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }
            switch (actionID)
            {
                case ActionID.ACTION_DESIGNATION_ADD:
                    return _createDesignation(inputDS);
                case ActionID.NA_ACTION_GET_DESIGNATION_LIST:
                    return _getDesignationList(inputDS);
                case ActionID.ACTION_DESIGNATION_DEL:
                    return this._deleteDesignation(inputDS);
                case ActionID.ACTION_DESIGNATION_UPD:
                    return this._updateDesignation(inputDS);
                case ActionID.ACTION_DOES_DESIGNATION_EXIST:
                    return this._doesDesignationExist(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }

        private DataSet _createDesignation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DesignationDS desDS = new DesignationDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateDesignation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            desDS.Merge(inputDS.Tables[desDS.Designations.TableName], false, MissingSchemaAction.Error);
            desDS.AcceptChanges();
            foreach (DesignationDS.Designation des in desDS.Designations)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters["Id"].Value = (object)genPK;


                if (des.IsDesignationCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)des.DesignationCode;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }

                if (des.IsDesignationNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = (object)des.DesignationName;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }

                if (des.IsDesignationName_NativeLangNull() == false)    // Rony :: 09 June 2016 :: For Native Language
                {
                    cmd.Parameters["Name_NativeLang"].Value = (object)des.DesignationName_NativeLang;
                }
                else
                {
                    cmd.Parameters["Name_NativeLang"].Value = null;
                }

                if (des.IsDesignationIsActiveNull() == false)
                {
                    cmd.Parameters["IsActive"].Value = (object)des.DesignationIsActive;
                }
                else
                {
                    cmd.Parameters["IsActive"].Value = null;
                }

                if (!des.IsRelevantGradeCodeNull()) cmd.Parameters["RelevantGradeCode"].Value = (object)des.RelevantGradeCode;
                else cmd.Parameters["RelevantGradeCode"].Value = DBNull.Value;

                if (!des.IsHierarchyDesignationCodeNull()) cmd.Parameters["HierarchyDesignationCode"].Value = (object)des.HierarchyDesignationCode;
                else cmd.Parameters["HierarchyDesignationCode"].Value = DBNull.Value;

                cmd.Parameters["DefaultSelection"].Value = (object)des.DefaultSelection;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && des.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DESIGNATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getDesignationList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDesignationList"); //Jarif(28 Sep 11)
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDesignationList_New");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(28 Sep 11)...
            #region Old...
            //DesignationPO desPO = new DesignationPO();
            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, desPO, desPO.Designations.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_DESIGNATION_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}
            //desPO.AcceptChanges();
            //DesignationDS desDS = new DesignationDS();
            //if (desPO.Designations.Count > 0)
            //{
            //    foreach (DesignationPO.Designation poDes in desPO.Designations)
            //    {
            //        DesignationDS.Designation des = desDS.Designations.NewDesignation();
            //        des.DesignationCode = poDes.Designationcode;
            //        des.DesignationName = poDes.Designationname;
            //        des.DesignationIsActive = poDes.IsActive;
            //        desDS.Designations.AddDesignation(des);
            //        desDS.AcceptChanges();
            //    }
            //}
            #endregion

            DesignationDS desDS = new DesignationDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, desDS, desDS.Designations.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DESIGNATION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            desDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(desDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _deleteDesignation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteDesignation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            int nRowAffectedRec = -1;
            if (connDS.DBConnections.Rows.Count == 2)
            {
                nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
            }
            cmd.Parameters.Clear(); 
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DESIGNATION_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;  // return empty ErrorDS
        }

        private DataSet _updateDesignation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DesignationDS desDS = new DesignationDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            desDS.Merge(inputDS.Tables[desDS.Designations.TableName], false, MissingSchemaAction.Error);
            desDS.AcceptChanges();


            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_DES";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (DesignationDS.Designation des in desDS.Designations)
            {
                if (des.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_DESIGNATION_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion



            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateDesignation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            foreach (DesignationDS.Designation des in desDS.Designations)
            {

                if (des.IsDesignationCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)des.DesignationCode;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }

                if (des.IsDesignationNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = (object)des.DesignationName;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }
                if (des.IsDesignationName_NativeLangNull() == false)    // Rony :: 09 June 2016 :: For Native Language
                {
                    cmd.Parameters["Name_NativeLang"].Value = (object)des.DesignationName_NativeLang;
                }
                else
                {
                    cmd.Parameters["Name_NativeLang"].Value = null;
                }
                if (des.IsDesignationIsActiveNull() == false)
                {
                    cmd.Parameters["IsActive"].Value = (object)des.DesignationIsActive;
                }
                else
                {
                    cmd.Parameters["IsActive"].Value = null;
                }


                if (!des.IsRelevantGradeCodeNull()) cmd.Parameters["RelevantGradeCode"].Value = (object)des.RelevantGradeCode;
                else cmd.Parameters["RelevantGradeCode"].Value = DBNull.Value;

                if (!des.IsHierarchyDesignationCodeNull()) cmd.Parameters["HierarchyDesignationCode"].Value = (object)des.HierarchyDesignationCode;
                else cmd.Parameters["HierarchyDesignationCode"].Value = DBNull.Value;
                
                cmd.Parameters["DefaultSelection"].Value = (object)des.DefaultSelection;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && des.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DESIGNATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _doesDesignationExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesDesignationExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

    }
}
