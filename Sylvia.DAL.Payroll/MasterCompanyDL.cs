using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    public class MasterCompanyDL
    {
        public MasterCompanyDL()
        {
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_MASTERCOMPANY_ADD:
                    return _createMasterCompany(inputDS);
                case ActionID.ACTION_MASTERCOMPANY_DEL:
                    return this._deleteMasterCompany(inputDS);
                case ActionID.ACTION_MASTERCOMPANY_UPD:
                    return this._updateMasterCompany(inputDS);
                case ActionID.NA_ACTION_GET_MASTERCOMPANY_LIST:
                    return _getMasterCompanyList(inputDS);
                case ActionID.ACTION_DOES_MASTERCOMPANY_EXIST:
                    return this._doesMasterCompanyExist(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getMasterCompanyList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();



            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetMasterCompanyList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            MasterCompanyPO comPO = new MasterCompanyPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              comPO, comPO._MasterCompanyPO.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_MASTERCOMPANY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            comPO.AcceptChanges();

            MasterCompanyDS comDS = new MasterCompanyDS();

            if (comPO._MasterCompanyPO.Count > 0)
            {


                foreach (MasterCompanyPO.MasterCompanyPORow poCom in comPO._MasterCompanyPO)
                {
                    MasterCompanyDS.MasterCompanyRow com = comDS.MasterCompany.NewMasterCompanyRow();
                    if (poCom.IsCompanyCodeNull() == false)
                    {
                        com.CompanyCode = poCom.CompanyCode;
                    }
                    if (poCom.IsCompanyNameNull() == false)
                    {
                        com.CompanyName = poCom.CompanyName;
                    }
                    if (poCom.IsAddressNull() == false)
                    {
                        com.Address = poCom.Address;
                    }
                    if (poCom.IsTelephoneNull() == false)
                    {
                        com.Telephone = poCom.Telephone;
                    }
                    if (poCom.IsFaxNull() == false)
                    {
                        com.Fax = poCom.Fax;
                    }
                    if (poCom.IsURLNull() == false)
                    {
                        com.URL = poCom.URL;
                    }
                    if (poCom.IsEmailNull() == false)
                    {
                        com.Email = poCom.Email;
                    }
                                        

                    comDS.MasterCompany.AddMasterCompanyRow(com);
                    comDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(comDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }


        private DataSet _createMasterCompany(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MasterCompanyDS comDS = new MasterCompanyDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateMasterCompany");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            comDS.Merge(inputDS.Tables[comDS.MasterCompany.TableName], false, MissingSchemaAction.Error);
            comDS.AcceptChanges();

            foreach (MasterCompanyDS.MasterCompanyRow com in comDS.MasterCompany)
            {
                //code
                if (com.IsCompanyCodeNull() == false)
                {
                    cmd.Parameters["CompanyCode"].Value = (object)com.CompanyCode;
                }
                else
                {
                    cmd.Parameters["CompanyCode"].Value = null;
                }
                //name
                if (com.IsCompanyNameNull() == false)
                {
                    cmd.Parameters["CompanyName"].Value = (object)com.CompanyName;

                }
                else
                {
                    cmd.Parameters["CompanyName"].Value = null;
                }
                //Address
                if (com.IsAddressNull() == false)
                {
                    cmd.Parameters["Address"].Value = (object)com.Address;
                }
                else
                {
                    cmd.Parameters["Address"].Value = null;
                }
                //Telephone
                if (com.IsTelephoneNull() == false)
                {
                    cmd.Parameters["Telephone"].Value = (object)com.Telephone;
                }
                else
                {
                    cmd.Parameters["Telephone"].Value = null;
                }
                //Fax
                if (com.IsFaxNull() == false)
                {
                    cmd.Parameters["Fax"].Value = (object)com.Fax;
                }
                else
                {
                    cmd.Parameters["Fax"].Value = null;
                }
                //URl
                if (com.IsURLNull() == false)
                {
                    cmd.Parameters["URL"].Value = (object)com.URL;
                }
                else
                {
                    cmd.Parameters["URL"].Value = null;
                }
                //Email
                if (com.IsEmailNull() == false)
                {
                    cmd.Parameters["Email"].Value = (object)com.Email;
                }
                else
                {
                    cmd.Parameters["Email"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MASTERCOMPANY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteMasterCompany(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteMasterCompany");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["CompanyCode"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MASTERCOMPANY_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _updateMasterCompany(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MasterCompanyDS comDS = new MasterCompanyDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateMasterCompany");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            comDS.Merge(inputDS.Tables[comDS.MasterCompany.TableName], false, MissingSchemaAction.Error);
            comDS.AcceptChanges();


            foreach (MasterCompanyDS.MasterCompanyRow com in comDS.MasterCompany)
            {

                //code
                if (com.IsCompanyCodeNull() == false)
                {
                    cmd.Parameters["CompanyCode"].Value = (object)com.CompanyCode;
                }
                else
                {
                    cmd.Parameters["CompanyCode"].Value = null;
                }
                //name
                if (com.IsCompanyNameNull() == false)
                {
                    cmd.Parameters["CompanyName"].Value = (object)com.CompanyName;

                }
                else
                {
                    cmd.Parameters["CompanyName"].Value = null;
                }
                //Address
                if (com.IsAddressNull() == false)
                {
                    cmd.Parameters["Address"].Value = (object)com.Address;
                }
                else
                {
                    cmd.Parameters["Address"].Value = null;
                }
                //Telephone
                if (com.IsTelephoneNull() == false)
                {
                    cmd.Parameters["Telephone"].Value = (object)com.Telephone;
                }
                else
                {
                    cmd.Parameters["Telephone"].Value = null;
                }
                //Fax
                if (com.IsFaxNull() == false)
                {
                    cmd.Parameters["Fax"].Value = (object)com.Fax;
                }
                else
                {
                    cmd.Parameters["Fax"].Value = null;
                }
                //URl
                if (com.IsURLNull() == false)
                {
                    cmd.Parameters["URL"].Value = (object)com.URL;
                }
                else
                {
                    cmd.Parameters["URL"].Value = null;
                }
                //Email
                if (com.IsEmailNull() == false)
                {
                    cmd.Parameters["Email"].Value = (object)com.Email;
                }
                else
                {
                    cmd.Parameters["Email"].Value = null;
                }
                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MASTERCOMPANY_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _doesMasterCompanyExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesMasterCompanyExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["CompanyCode"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_MASTERCOMPANY_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

    }
}
