using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class lmsLeaveCategoryDL
    {
        public lmsLeaveCategoryDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                #region Leave Category
                case ActionID.ACTION_LMS_LEAVE_CATEGORY_ADD:
                    return this._createLeaveCategory(inputDS);
                case ActionID.ACTION_LMS_LEAVE_CATEGORY_DEL:
                    return this._deleteLeaveCategory(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVE_CATEGORY_LIST:
                    return this._getLeaveCategoryList(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVE_CATEGORY_LIST_ALL:
                    return this._getLeaveCategoryListAll(inputDS);
                case ActionID.ACTION_LMS_DOES_LEAVE_CATEGORY_EXIST:
                    return this._doesLeaveCategoryExist(inputDS);
                case ActionID.ACTION_LMS_LEAVE_CATEGORY_UPD:
                    return this._updateLeaveCategory(inputDS);

                #endregion
                #region Leave Category Config
                case ActionID.ACTION_LMS_LEAVE_CATEGORYCONFIG_ADD:
                    return this._createLeaveCategoryConfig(inputDS);
                case ActionID.ACTION_LMS_LEAVE_CATEGORYCONFIG_DEL:
                    return this._deleteLeaveCategoryConfig(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVE_CATEGORYCONFIG_LIST:
                    return this._getLeaveCategoryConfigList(inputDS);
                #endregion
                #region Assign Grade
                case ActionID.ACTION_LMS_LEAVE_CATEGORY_ASSIGN:
                    return this._assignGrade(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LCGRADE_LIST:
                    return this._getLCGradeListAll(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LCGRADE_EMPLOYEE_TYPES:
                    return this._getLCGradeEmployeeTypes(inputDS);
                #endregion
                case ActionID.ACTION_LMS_BELANCE_UPDATE:
                    return this.LeaveBelanceUpdate(inputDS);


                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }

        #region Leave Category
        private DataSet _createLeaveCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVECATEGORY_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();
            leaveCatDS.Merge(inputDS.Tables[leaveCatDS.LeaveCategory.TableName], false, MissingSchemaAction.Error);
            leaveCatDS.Merge(inputDS.Tables[leaveCatDS.LCDetails.TableName], false, MissingSchemaAction.Error);
            leaveCatDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveCategoryDS.LeaveCategoryRow row in leaveCatDS.LeaveCategory.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_Leavecategoryid", genPK);
                cmd.Parameters.AddWithValue("p_LeaveCategoryCode", row.LeaveCategoryCode);
                cmd.Parameters.AddWithValue("p_Leavecategoryname", row.LeaveCategoryName);
                if (row.IsLeaveCategoryRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Leavecategoryremarks", row.LeaveCategoryRemarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Leavecategoryremarks", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("p_Isactive", row.IsActive);

                cmd.Parameters.AddWithValue("p_LeaveCategoryID_Old", row.LeaveCategoryID); //Jarif, 230414

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CATEGORY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Dispose();
                cmd.CommandText = "PRO_LMS_LEAVECATDETAILS_CREATE";
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (lmsLeaveCategoryDS.LCDetailsRow detailsRow in leaveCatDS.LCDetails.Rows)
                {
                    cmd.Parameters.Clear();
                    long genPK1 = IDGenerator.GetNextGenericPK();
                    if (genPK1 == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }

                    cmd.Parameters.AddWithValue("p_Lcdetailsid", genPK1);
                    cmd.Parameters.AddWithValue("p_Leavecategoryid", genPK);
                    cmd.Parameters.AddWithValue("p_Serviceyear", detailsRow.ServiceYear);
                    cmd.Parameters.AddWithValue("p_Encashmentinterval", detailsRow.EncashmentInterval);
                    if (detailsRow.IsLCDetailsRemarksNull() == false)
                    {
                        cmd.Parameters.AddWithValue("p_Lcdetailsremarks", detailsRow.LCDetailsRemarks);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("p_Lcdetailsremarks", DBNull.Value);
                    }
                    cmd.Parameters.AddWithValue("p_ServiceYearFormated", detailsRow.Serviceyear_Formated);

                    cmd.Parameters.AddWithValue("p_LCDetailsID_Old", detailsRow.LCDetailsID);  //Jarif, 230414

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CATEGORY_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteLeaveCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVECATEGORY_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_LeaveCategoryCode", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVETYPE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getLeaveCategoryList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveCategoryList");
            cmd.Parameters["Code"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveCategoryPO leaveCatPO = new lmsLeaveCategoryPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveCatPO, leaveCatPO.LeaveCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_CATEGORY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            cmd.Dispose();
            cmd = DBCommandProvider.GetDBCommand("GetLmsLCDList");
            cmd.Parameters["Code"].Value = (object)StringDS.DataStrings[0].StringValue;

            adapter.Dispose();
            adapter.SelectCommand = cmd;

            nRowAffected = ADOController.Instance.Fill(adapter, leaveCatPO, leaveCatPO.LCDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_CATEGORY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveCatPO.AcceptChanges();

            lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();

            #region Populate Leave Category Table
            if (leaveCatPO.LeaveCategory.Count > 0)
            {
                foreach (lmsLeaveCategoryPO.LeaveCategoryRow poRow in leaveCatPO.LeaveCategory.Rows)
                {
                    lmsLeaveCategoryDS.LeaveCategoryRow row = leaveCatDS.LeaveCategory.NewLeaveCategoryRow();

                    row.LeaveCategoryID = poRow.LeaveCategoryID;
                    row.LeaveCategoryCode = poRow.LeaveCategoryCode;
                    if (poRow.IsLeaveCategoryNameNull() == false)
                    {
                        row.LeaveCategoryName = poRow.LeaveCategoryName;
                    }
                    if (poRow.IsLeaveCategoryRemarksNull() == false)
                    {
                        row.LeaveCategoryRemarks = poRow.LeaveCategoryRemarks;
                    }
                    if (poRow.IsIsActiveNull() == false)
                    {
                        row.IsActive = poRow.IsActive;
                    }

                    leaveCatDS.LeaveCategory.AddLeaveCategoryRow(row);
                    leaveCatDS.AcceptChanges();
                }
            }
            #endregion
            #region Populate LCDetails Table
            if (leaveCatPO.LCDetails.Count > 0)
            {
                foreach (lmsLeaveCategoryPO.LCDetailsRow poRow in leaveCatPO.LCDetails.Rows)
                {
                    lmsLeaveCategoryDS.LCDetailsRow row = leaveCatDS.LCDetails.NewLCDetailsRow();

                    if (poRow.IsLCDetailsIDNull() == false)
                    {
                        row.LCDetailsID = poRow.LCDetailsID;
                    }
                    if (poRow.IsLeaveCategoryIDNull() == false)
                    {
                        row.LeaveCategoryID = poRow.LeaveCategoryID;
                    }
                    if (poRow.IsLeaveCategoryCodeNull() == false)
                    {
                        row.LeaveCategoryCode = poRow.LeaveCategoryCode;
                    }
                    if (poRow.IsLeaveCategoryNameNull() == false)
                    {
                        row.LeaveCategoryName = poRow.LeaveCategoryName;
                    }
                    if (poRow.IsServiceYearNull() == false)
                    {
                        row.ServiceYear = poRow.ServiceYear;
                    }
                    if (poRow.IsEncashmentIntervalNull() == false)
                    {
                        row.EncashmentInterval = poRow.EncashmentInterval;
                    }
                    if (poRow.IsLCDetailsRemarksNull() == false)
                    {
                        row.LCDetailsRemarks = poRow.LCDetailsRemarks;
                    }
                    //Kaysar, 01-Mar-2011
                    if (poRow.IsServiceyear_FormatedNull() == false)
                    {
                        row.Serviceyear_Formated = poRow.Serviceyear_Formated;
                    }
                    //

                    leaveCatDS.LCDetails.AddLCDetailsRow(row);
                    leaveCatDS.AcceptChanges();
                }
            }
            #endregion
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveCatDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveCategoryListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            if (activeStatus.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveCategoryListAll");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsActiveLeaveCategoryList");
                if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
                else cmd.Parameters["IsActive"].Value = (object)false;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveCategoryPO leaveCatPO = new lmsLeaveCategoryPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveCatPO, leaveCatPO.LeaveCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_CATEGORY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            cmd.Dispose();
            adapter.Dispose();

            if (activeStatus.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsLCDListAll");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsActiveLCDList");
                if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
                else cmd.Parameters["IsActive"].Value = (object)false;
            }
            adapter.SelectCommand = cmd;

            nRowAffected = ADOController.Instance.Fill(adapter, leaveCatPO, leaveCatPO.LCDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_CATEGORY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveCatPO.AcceptChanges();

            lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();

            #region Populate Leave Category Table
            if (leaveCatPO.LeaveCategory.Count > 0)
            {
                foreach (lmsLeaveCategoryPO.LeaveCategoryRow poRow in leaveCatPO.LeaveCategory.Rows)
                {
                    lmsLeaveCategoryDS.LeaveCategoryRow row = leaveCatDS.LeaveCategory.NewLeaveCategoryRow();

                    row.LeaveCategoryID = poRow.LeaveCategoryID;
                    row.LeaveCategoryCode = poRow.LeaveCategoryCode;
                    if (poRow.IsLeaveCategoryNameNull() == false)
                    {
                        row.LeaveCategoryName = poRow.LeaveCategoryName;
                    }
                    if (poRow.IsLeaveCategoryRemarksNull() == false)
                    {
                        row.LeaveCategoryRemarks = poRow.LeaveCategoryRemarks;
                    }
                    if (poRow.IsIsActiveNull() == false)
                    {
                        row.IsActive = poRow.IsActive;
                    }

                    leaveCatDS.LeaveCategory.AddLeaveCategoryRow(row);
                    leaveCatDS.AcceptChanges();
                }
            }
            #endregion
            #region Populate LCDetails Table
            if (leaveCatPO.LCDetails.Count > 0)
            {
                foreach (lmsLeaveCategoryPO.LCDetailsRow poRow in leaveCatPO.LCDetails.Rows)
                {
                    lmsLeaveCategoryDS.LCDetailsRow row = leaveCatDS.LCDetails.NewLCDetailsRow();

                    if (poRow.IsLCDetailsIDNull() == false)
                    {
                        row.LCDetailsID = poRow.LCDetailsID;
                    }
                    if (poRow.IsLeaveCategoryIDNull() == false)
                    {
                        row.LeaveCategoryID = poRow.LeaveCategoryID;
                    }
                    if (poRow.IsLeaveCategoryCodeNull() == false)
                    {
                        row.LeaveCategoryCode = poRow.LeaveCategoryCode;
                    }
                    if (poRow.IsLeaveCategoryNameNull() == false)
                    {
                        row.LeaveCategoryName = poRow.LeaveCategoryName;
                    }
                    if (poRow.IsServiceYearNull() == false)
                    {
                        row.ServiceYear = poRow.ServiceYear;
                    }
                    if (poRow.IsEncashmentIntervalNull() == false)
                    {
                        row.EncashmentInterval = poRow.EncashmentInterval;
                    }
                    if (poRow.IsLCDetailsRemarksNull() == false)
                    {
                        row.LCDetailsRemarks = poRow.LCDetailsRemarks;
                    }

                    leaveCatDS.LCDetails.AddLCDetailsRow(row);
                    leaveCatDS.AcceptChanges();
                }
            }
            #endregion
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveCatDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _doesLeaveCategoryExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesLmsLeaveCategoryExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_DOES_LEAVE_CATEGORY_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _updateLeaveCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVECATEGORY_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();
            leaveCatDS.Merge(inputDS.Tables[leaveCatDS.LeaveCategory.TableName], false, MissingSchemaAction.Error);
            leaveCatDS.Merge(inputDS.Tables[leaveCatDS.LCDetails.TableName], false, MissingSchemaAction.Error);
            leaveCatDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region For Update...
            foreach (lmsLeaveCategoryDS.LeaveCategoryRow row in leaveCatDS.LeaveCategory.Rows)
            {
                cmd.Parameters.AddWithValue("p_LeaveCategoryId", row.LeaveCategoryID);
                cmd.Parameters.AddWithValue("p_LeaveCategoryName", row.LeaveCategoryName);
                if (row.IsLeaveCategoryRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LeaveCategoryRemarks", row.LeaveCategoryRemarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LeaveCategoryRemarks", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CATEGORY_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Dispose();
                cmd.CommandText = "PRO_LMS_LEAVECATDETAILS_CREATE";
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (lmsLeaveCategoryDS.LCDetailsRow detailsRow in leaveCatDS.LCDetails.Rows)
                {
                    cmd.Parameters.Clear();
                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }

                    cmd.Parameters.AddWithValue("p_Lcdetailsid", genPK);
                    cmd.Parameters.AddWithValue("p_Leavecategoryid", row.LeaveCategoryID);
                    cmd.Parameters.AddWithValue("p_Serviceyear", detailsRow.ServiceYear);
                    cmd.Parameters.AddWithValue("p_Encashmentinterval", detailsRow.EncashmentInterval);
                    if (detailsRow.IsLCDetailsRemarksNull() == false)
                    {
                        cmd.Parameters.AddWithValue("p_Lcdetailsremarks", detailsRow.LCDetailsRemarks);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("p_Lcdetailsremarks", DBNull.Value);
                    }
                    cmd.Parameters.AddWithValue("p_ServiceYearFormated", detailsRow.Serviceyear_Formated);

                    cmd.Parameters.AddWithValue("p_LCDetailsID_Old", detailsRow.LCDetailsID);  //Jarif, 230414

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CATEGORY_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion

            #region For Deletion...
            cmd.Dispose();
            cmd.CommandText = "PRO_LMS_LEAVECATDETAILS_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (DataIntegerDS.DataInteger DRow in integerDS.DataIntegers.Rows)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("p_LCDetailsID", DRow.IntegerValue);
                bool bError = false;
                ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        #endregion
        #region Leave Category Config
        private DataSet _createLeaveCategoryConfig(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();
            leaveCatDS.Merge(inputDS.Tables[leaveCatDS.LCConfig.TableName], false, MissingSchemaAction.Error);
            leaveCatDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool bError = false;
            int nRowAffected = -1;

            #region Delete Leave Category Config

            cmd.CommandText = "PRO_LMS_LCCONFIG_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (lmsLeaveCategoryDS.LCConfigRow row in leaveCatDS.LCConfig.Rows)
            {
                cmd.Parameters.AddWithValue("p_Lcdetailsid", row.LCDetailsID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CATEGORYCONFIG_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                break;
            }
            cmd.Dispose();
            #endregion

            #region Create Leave Category Config

            cmd.CommandText = "PRO_LMS_LCCONFIG_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (lmsLeaveCategoryDS.LCConfigRow row in leaveCatDS.LCConfig.Rows)
            {
                cmd.Parameters.AddWithValue("p_Lcdetailsid", row.LCDetailsID);
                cmd.Parameters.AddWithValue("p_Leavetypecode", row.LeaveTypeCode);
                if (row.IsEntitledNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Entitled", row.Entitled);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Entitled", DBNull.Value);
                }
                if (row.IsMaxCarryOverNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_MaxCarryOver", row.MaxCarryOver);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_MaxCarryOver", DBNull.Value);
                }
                if (row.IsMaxEncashmentNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_MaxEncashment", row.MaxEncashment);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_MaxEncashment", DBNull.Value);
                }
                if (row.IsMaxCarryOver_ConsecutiveNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_MaxCarryOver_Consecutive", row.MaxCarryOver_Consecutive);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_MaxCarryOver_Consecutive", DBNull.Value);
                }
                if (row.IsMaxEncashment_ConsecutiveNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_MaxEncashment_Consecutive", row.MaxEncashment_Consecutive);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_MaxEncashment_Consecutive", DBNull.Value);
                }


                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CATEGORYCONFIG_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteLeaveCategoryConfig(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LCCONFIG_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_Lcdetailsid", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CATEGORYCONFIG_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getLeaveCategoryConfigList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLmsLCConfigList");
            cmd.Parameters["LcDetailsID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveCategoryPO leaveCatPO = new lmsLeaveCategoryPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveCatPO, leaveCatPO.LCConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_CATEGORYCONFIG_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveCatPO.AcceptChanges();

            lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();

            #region Populate LCDetails Table
            if (leaveCatPO.LCConfig.Count > 0)
            {
                foreach (lmsLeaveCategoryPO.LCConfigRow poRow in leaveCatPO.LCConfig.Rows)
                {
                    lmsLeaveCategoryDS.LCConfigRow row = leaveCatDS.LCConfig.NewLCConfigRow();

                    row.LCDetailsID = poRow.LCDetailsID;
                    row.LeaveTypeID = poRow.LeaveTypeID;
                    if (poRow.IsLeaveTypeCodeNull() == false)
                    {
                        row.LeaveTypeCode = poRow.LeaveTypeCode;
                    }
                    if (poRow.IsLeaveTypeNameNull() == false)
                    {
                        row.LeaveTypeName = poRow.LeaveTypeName;
                    }
                    if (poRow.IsEntitledNull() == false)
                    {
                        row.Entitled = poRow.Entitled;
                    }
                    if (poRow.IsMaxCarryOverNull() == false)
                    {
                        row.MaxCarryOver = poRow.MaxCarryOver;
                    }
                    if (poRow.IsMaxEncashmentNull() == false)
                    {
                        row.MaxEncashment = poRow.MaxEncashment;
                    }
                    if (poRow.IsMaxCarryOver_ConsecutiveNull() == false)
                    {
                        row.MaxCarryOver_Consecutive = poRow.MaxCarryOver_Consecutive;
                    }

                    if (poRow.IsMaxEncashment_ConsecutiveNull() == false)
                    {
                        row.MaxEncashment_Consecutive = poRow.MaxEncashment_Consecutive;
                    }

                    leaveCatDS.LCConfig.AddLCConfigRow(row);
                    leaveCatDS.AcceptChanges();
                }
            }
            #endregion
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveCatDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        #endregion
        #region Leave Category Config
        private DataSet _assignGrade(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();


            OleDbCommand cmdDel = new OleDbCommand();
            cmdDel.CommandText = "PRO_LMS_GRADE_EMPLOYEETYPE_DEL";
            cmdDel.CommandType = CommandType.StoredProcedure;

            lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();
            leaveCatDS.Merge(inputDS.Tables[leaveCatDS.GradeEmployeeTypes.TableName], false, MissingSchemaAction.Error);
            leaveCatDS.AcceptChanges(); ;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            bool bErrorDel = false;
            int nRowAffectedDel = -1;
            nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);


            if (bErrorDel)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CATEGORY_ASSIGN.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            OleDbCommand cmd = new OleDbCommand();
            //cmd.CommandText = "PRO_LMS_GRADE_UPDATE";
            cmd.CommandText = "PRO_LMS_GRADE_EMPTYPE_INSERT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

 

            foreach (lmsLeaveCategoryDS.GradeEmployeeTypesRow row in leaveCatDS.GradeEmployeeTypes.Rows)
            {
                cmd.Parameters.AddWithValue("p_GradeID", row.GradeId);
                if (row.IsLeaveCategoryIdNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LeaveCategoryID", row.LeaveCategoryId);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LeaveCategoryID", DBNull.Value);
                }

                if (row.IsEmployeeTypeIdNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LeaveCategoryID", row.EmployeeTypeId);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EmpTypeId", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CATEGORY_ASSIGN.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }


            OleDbCommand cmdUp = new OleDbCommand();
            cmdUp.CommandText = "PRO_LMS_GR_EMPTYPE_UPFOR_ALL";
            cmdUp.CommandType = CommandType.StoredProcedure;

            bool bErrorUp = false;
            int nRowAffectedUp = -1;
            nRowAffectedUp = ADOController.Instance.ExecuteNonQuery(cmdUp, connDS.DBConnections[0].ConnectionID, ref bErrorUp);


            if (bErrorUp)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CATEGORY_ASSIGN.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getLCGradeListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            if (activeStatus.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsGradeListAll");
            }
            else if (StringDS.DataStrings[0].StringValue.Equals("GradeWiseLeaveBalanceUpdateRole"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetLMSBalanceUpdateRole");
                cmd.Parameters["LeaveTypeID"].Value = (object)Convert.ToInt32(StringDS.DataStrings[1].StringValue);
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsActiveGradeList");
                if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
                else cmd.Parameters["IsActive"].Value = (object)false;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveCatDS, leaveCatDS.Grade.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LCGRADE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveCatDS.AcceptChanges();

            //lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();
            //if (leaveCatPO.Grade.Count > 0)
            //{
            //    foreach (lmsLeaveCategoryPO.GradeRow poRow in leaveCatPO.Grade.Rows)
            //    {
            //        lmsLeaveCategoryDS.GradeRow row = leaveCatDS.Grade.NewGradeRow();

            //        row.GradeID = poRow.GradeID;
            //        row.GradeCode = poRow.GradeCode;
            //        if (poRow.IsGradeNameNull() == false)
            //        {
            //            row.GradeName = poRow.GradeName;
            //        }
            //        if (poRow.IsLeaveCategoryIDNull() == false)
            //        {
            //            row.LeaveCategoryID = poRow.LeaveCategoryID;
            //        }

            //        leaveCatDS.Grade.AddGradeRow(row);
            //        leaveCatDS.AcceptChanges();
            //    }
            //}

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveCatDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLCGradeEmployeeTypes(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetLmsActiveGradeEmployeeTypes");
            if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
            else cmd.Parameters["IsActive"].Value = (object)false;


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveCatDS, leaveCatDS.GradeEmployeeTypes.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LCGRADE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveCatDS.AcceptChanges();

            //lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();
            //if (leaveCatPO.Grade.Count > 0)
            //{
            //    foreach (lmsLeaveCategoryPO.GradeRow poRow in leaveCatPO.Grade.Rows)
            //    {
            //        lmsLeaveCategoryDS.GradeRow row = leaveCatDS.Grade.NewGradeRow();

            //        row.GradeID = poRow.GradeID;
            //        row.GradeCode = poRow.GradeCode;
            //        if (poRow.IsGradeNameNull() == false)
            //        {
            //            row.GradeName = poRow.GradeName;
            //        }
            //        if (poRow.IsLeaveCategoryIDNull() == false)
            //        {
            //            row.LeaveCategoryID = poRow.LeaveCategoryID;
            //        }

            //        leaveCatDS.Grade.AddGradeRow(row);
            //        leaveCatDS.AcceptChanges();
            //    }
            //}

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveCatDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        #endregion

        private DataSet LeaveBelanceUpdate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_BALANCE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();
            leaveCatDS.Merge(inputDS.Tables[leaveCatDS.Grade.TableName], false, MissingSchemaAction.Error);
            leaveCatDS.AcceptChanges(); ;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            foreach (lmsLeaveCategoryDS.GradeRow row in leaveCatDS.Grade.Rows)
            {
                cmd.Parameters.AddWithValue("p_ConcateGradeID", row.ConcateGradeID);
                cmd.Parameters.AddWithValue("p_LeaveTypeID", row.LeaveTypeID);
                cmd.Parameters.AddWithValue("p_UpdateDate", row.UpdateDate);
                cmd.Parameters.AddWithValue("p_ServiceDay", row.ServiceDay);
                cmd.Parameters.AddWithValue("p_LeaveEligibleDay", row.LeaveEligibleDay);
                cmd.Parameters.AddWithValue("p_DepandsOnAttendance", row.DepandsOnAttendance);
                cmd.Parameters.AddWithValue("p_BalanceUpBasedOn", row.BalanceUpBasedOn);
                cmd.Parameters.AddWithValue("p_ServiceMonth", row.ServiceMonth);
                cmd.Parameters.AddWithValue("p_EmployeeType", row.EmployeeType);


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_BELANCE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }


            cmd.Parameters.Clear();
            cmd.Dispose();
            cmd.CommandText = "PRO_LMS_BALANCE_RULE_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (lmsLeaveCategoryDS.GradeRow row in leaveCatDS.Grade.Rows)
            {
                cmd.Parameters.AddWithValue("p_LeaveTypeID", row.LeaveTypeID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_BELANCE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }

            cmd.Parameters.Clear();
            cmd.Dispose();
            cmd.CommandText = "PRO_LMS_BALANCE_RULE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (lmsLeaveCategoryDS.GradeRow row in leaveCatDS.Grade.Rows)
            {
                cmd.Parameters.AddWithValue("p_ConcateGradeID", row.ConcateGradeID);
                cmd.Parameters.AddWithValue("p_LeaveTypeID", row.LeaveTypeID);
                cmd.Parameters.AddWithValue("p_UpdateDate", row.UpdateDate);
                cmd.Parameters.AddWithValue("p_ServiceDay", row.ServiceDay);
                cmd.Parameters.AddWithValue("p_LeaveEligibleDay", row.LeaveEligibleDay);
                cmd.Parameters.AddWithValue("p_DepandsOnAttendance", row.DepandsOnAttendance);
                cmd.Parameters.AddWithValue("p_BalanceUpBasedOn", row.BalanceUpBasedOn);
                cmd.Parameters.AddWithValue("p_ServiceMonth", row.ServiceMonth);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_BELANCE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

    }
}
