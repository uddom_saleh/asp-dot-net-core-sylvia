/* Compamy: Milllennium Information Solution Limited
 * Author: Mahbubur Rahman khan
 * Comment Date: March 29, 2006
 * */




using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for AdParamDL.
    /// </summary>
    public class AdParamDL
    {
        public AdParamDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_ALLOWDEDUCTPARAM_ADD:
                    return _createAdParam(inputDS);
                case ActionID.NA_ACTION_ALLOWDEDUCTPARAM_GET:
                    return _getAdParam(inputDS);
                case ActionID.NA_ACTION_Q_ALLOWDEDUCTPARAM_ALL:
                    return _getAdParamList(inputDS);
                case ActionID.ACTION_ALLOWDEDUCTPARAM_UPD:
                    return this._updateAdParam(inputDS);
                case ActionID.ACTION_ALLOWDEDUCTPARAM_DEL:
                    return this._deleteAdParam(inputDS);
                case ActionID.ACTION_DOES_GRADE_USED_IN_ALLOWDEDUCT:
                    return this._doesGradeUsedInAllowDeduct(inputDS);
                case ActionID.ACTION_DOES_GRADE_EXIST_IN_ALLOWDEDUCT:
                    return this._doesGradeExistInAllowDeduct(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }
        private DataSet _createAdParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AdParamDS adParamDS = new AdParamDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            //extract allowance/deduction
            adParamDS.Merge(inputDS.Tables[adParamDS.AdParams.TableName], false, MissingSchemaAction.Error);
            adParamDS.Merge(inputDS.Tables[adParamDS.Grade.TableName], false, MissingSchemaAction.Error);
            adParamDS.AcceptChanges();
            foreach (AdParamDS.AdParam adParam in adParamDS.AdParams)
            {
                //create command for insert allowance/deduction
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateAdParam");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["Id"].Value = (object)genPK;
                cmd.Parameters["Code"].Value = (object)genPK.ToString();
                long nAdId = UtilDL.GetAllowDeductId(connDS, adParamDS.AdParams[0].AllowDeductCode);
                if (nAdId == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["AdId"].Value = (object)nAdId;
                if (adParam.IsAllowDeductTypeNull() == false)
                {
                    cmd.Parameters["AllowDeductType"].Value = (object)adParam.AllowDeductType;
                }
                else
                {
                    cmd.Parameters["AllowDeductType"].Value = System.DBNull.Value;
                }
                if (adParam.IsForConfirmOnlyNull() == false)
                {
                    cmd.Parameters["ForConfirm"].Value = (object)adParam.ForConfirmOnly;
                }
                else
                {
                    cmd.Parameters["ForConfirm"].Value = System.DBNull.Value;
                }
                if (adParam.IsFlatAmountNull() == false)
                {
                    cmd.Parameters["FlatAmount"].Value = (object)adParam.FlatAmount;
                }
                else
                {
                    cmd.Parameters["FlatAmount"].Value = System.DBNull.Value;
                }
                if (adParam.IsPercBasicNull() == false)
                {
                    cmd.Parameters["PercBasic"].Value = (object)adParam.PercBasic;
                }
                else
                {
                    cmd.Parameters["PercBasic"].Value = System.DBNull.Value;
                }
                if (adParam.IsMinAmountNull() == false)
                {
                    cmd.Parameters["MinAmount"].Value = (object)adParam.MinAmount;
                }
                else
                {
                    cmd.Parameters["MinAmount"].Value = System.DBNull.Value;
                }
                if (adParam.IsMaxAmountNull() == false)
                {
                    cmd.Parameters["MaxAmount"].Value = (object)adParam.MaxAmount;
                }
                else
                {
                    cmd.Parameters["MaxAmount"].Value = System.DBNull.Value;
                }
                if (adParam.IsPeriodicityNull() == false)
                {
                    cmd.Parameters["Periodicity"].Value = (object)adParam.Periodicity;
                }
                else
                {
                    cmd.Parameters["Periodicity"].Value = System.DBNull.Value;
                }
                if (adParam.IsEntitleTypeNull() == false)
                {
                    cmd.Parameters["EntitleType"].Value = (object)adParam.EntitleType;
                }
                else
                {
                    cmd.Parameters["EntitleType"].Value = System.DBNull.Value;
                }
                if (adParam.IsRegularNull() == false)
                {
                    cmd.Parameters["Regular"].Value = (object)adParam.Regular;
                }
                else
                {
                    cmd.Parameters["Regular"].Value = System.DBNull.Value;
                }
                if (adParam.IsAllowDeductTypeNull() == false)
                {
                    cmd.Parameters["CurrentlyActive"].Value = (object)adParam.CurrentlyActive;
                }
                else
                {
                    cmd.Parameters["CurrentlyActive"].Value = System.DBNull.Value;
                }
                //Nahid :: 14-Nov-2019
                cmd.Parameters["BasedOnBasic"].Value = (!adParam.IsBasedOnBasicNull()) ? (object)adParam.BasedOnBasic : System.DBNull.Value;
                cmd.Parameters["GrossHead"].Value = (!adParam.IsGrossHeadNull()) ? (object)adParam.GrossHead : System.DBNull.Value;
                //end
                if (adParam.IsLastActiveDeacteDateNull() == false)
                {
                    cmd.Parameters["LastActiveDeacteDate"].Value = (object)adParam.LastActiveDeacteDate;
                }
                else
                {
                    cmd.Parameters["LastActiveDeacteDate"].Value = System.DBNull.Value;
                }
                
                if (adParam.IsNextActivationDateNull() == false)
                {
                    cmd.Parameters["NextActivationDate"].Value = (object)adParam.NextActivationDate;
                }
                else
                {
                    cmd.Parameters["NextActivationDate"].Value = System.DBNull.Value;
                }

                if (adParam.IsExpire_DateNull() == false)
                {
                    cmd.Parameters["expireDate"].Value = (object)adParam.Expire_Date;
                }
                else
                {
                    cmd.Parameters["expireDate"].Value = System.DBNull.Value;
                }

                if (adParam.IsTaxableNull() == false)
                {
                    cmd.Parameters["Taxable"].Value = (object)adParam.Taxable;
                }
                else
                {
                    cmd.Parameters["Taxable"].Value = System.DBNull.Value;
                }
                if (adParam.IsDependsOnAttendanceNull() == false)
                {
                    cmd.Parameters["DependsOnAttendance"].Value = (object)adParam.DependsOnAttendance;
                }
                else
                {
                    cmd.Parameters["DependsOnAttendance"].Value = System.DBNull.Value;
                }

                if (adParam.IsAppliedStatNull() == false)
                {
                    cmd.Parameters["AppliedStat"].Value = (object)adParam.AppliedStat;
                }
                else
                {
                    cmd.Parameters["AppliedStat"].Value = System.DBNull.Value;
                }
                if (adParam.IsTaxProjectionNull() == false)
                {
                    cmd.Parameters["TaxProjection"].Value = (object)adParam.TaxProjection;
                }
                else
                {
                    cmd.Parameters["TaxProjection"].Value = System.DBNull.Value;
                }
                if (adParam.IsProratedPaymentNull() == false)
                {
                    cmd.Parameters["ProratedPayment"].Value = (object)adParam.ProratedPayment;
                }
                else
                {
                    cmd.Parameters["ProratedPayment"].Value = System.DBNull.Value;
                }



                if (adParam.IsGenderNull() == false) { cmd.Parameters["Gender"].Value = (object)adParam.Gender; }
                else { cmd.Parameters["Gender"].Value = System.DBNull.Value; }

                if (adParam.IsReligionNull() == false) { cmd.Parameters["Religion"].Value = (object)adParam.Religion; }
                else { cmd.Parameters["Religion"].Value = System.DBNull.Value; }

                if (adParam.IsDepartmentIDNull() == false) { cmd.Parameters["DepartmentID"].Value = (object)adParam.DepartmentID; }
                else { cmd.Parameters["DepartmentID"].Value = System.DBNull.Value; }

                if (adParam.IsEligibleServiceYearNull() == false) { cmd.Parameters["EligibleServiceYear"].Value = (object)adParam.EligibleServiceYear; }
                else { cmd.Parameters["EligibleServiceYear"].Value = System.DBNull.Value; }

                if (adParam.IsCreate_UserNull() == false) { cmd.Parameters["Create_User"].Value = (object)adParam.Create_User; }
                else { cmd.Parameters["Create_User"].Value = System.DBNull.Value; }

                if (adParam.IsEmployeeTypeNull() == false) { cmd.Parameters["EmployeeType"].Value = (object)adParam.EmployeeType; }
                else { cmd.Parameters["EmployeeType"].Value = System.DBNull.Value; }

                if (adParam.IsExcept_EmployeeTypeNull() == false) { cmd.Parameters["Except_EmployeeType"].Value = (object)adParam.Except_EmployeeType; }
                else { cmd.Parameters["Except_EmployeeType"].Value = System.DBNull.Value; }

                if (!adParam.IsNotForCarOwnerNull()) { cmd.Parameters["NotForCarOwner"].Value = (object)adParam.NotForCarOwner; }
                else { cmd.Parameters["NotForCarOwner"].Value = System.DBNull.Value; }

                if (!adParam.IsHandsOnPaymentNull()) { cmd.Parameters["HandsOnPayment"].Value = (object)adParam.HandsOnPayment; }
                else { cmd.Parameters["HandsOnPayment"].Value = 0; }

                if (!adParam.IsNotForSimOwnerNull()) { cmd.Parameters["NotForSimOwner"].Value = (object)adParam.NotForSimOwner; }
                else { cmd.Parameters["NotForSimOwner"].Value = System.DBNull.Value; }
                if (!adParam.IsProba_Eligibility_PerNull()) { cmd.Parameters["Proba_Eligibility_Per"].Value = (object)adParam.Proba_Eligibility_Per; }
                else { cmd.Parameters["Proba_Eligibility_Per"].Value = System.DBNull.Value; }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Dispose(); //create command for deleting grades for this allowance
                cmd = DBCommandProvider.GetDBCommand("DeleteAdParamDetails");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["AdParamId"].Value = genPK;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Dispose(); //create command for inserting grades for this allowance
                cmd = DBCommandProvider.GetDBCommand("CreateAdParamDetails");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["AdParamId"].Value = genPK;



                foreach (AdParamDS.GradeRow grade in adParam.GetGradeRows())
                {
                    long nGradeId = UtilDL.GetGradeId(connDS, grade.Code);
                    if (nGradeId == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }
                    cmd.Parameters["GradeId"].Value = (object)nGradeId;


                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getAdParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract connectionDS
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            //extract Allowance/deduction code
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAdParam");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            AdParamPO adParamPO = new AdParamPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              adParamPO, adParamPO.AdParams.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_ALLOWDEDUCT_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            adParamPO.AcceptChanges();

            AdParamDS adParamDS = new AdParamDS();
            if (adParamPO.AdParams.Count == 1)
            {
                AdParamDS.AdParam adParam = adParamDS.AdParams.NewAdParam();
                string sAdCode = UtilDL.GetAllowDeductCode(connDS, adParamPO.AdParams[0].AllowDeductId);
                if (sAdCode == null)
                {
                    UtilDL.GetDBOperationFailed();
                }
                adParam.AllowDeductCode = sAdCode;
                if (adParamPO.AdParams[0].IsCodeNull() == false)
                {
                    adParam.Code = adParamPO.AdParams[0].Code;
                }
                if (adParamPO.AdParams[0].IsAllowDeductTypeNull() == false)
                {
                    adParam.AllowDeductType = adParamPO.AdParams[0].AllowDeductType;
                }
                if (adParamPO.AdParams[0].IsForConfirmOnlyNull() == false)
                {
                    adParam.ForConfirmOnly = adParamPO.AdParams[0].ForConfirmOnly;
                }
                if (adParamPO.AdParams[0].IsFlatAmountNull() == false)
                {
                    adParam.FlatAmount = adParamPO.AdParams[0].FlatAmount;
                }
                if (adParamPO.AdParams[0].IsPercBasicNull() == false)
                {
                    adParam.PercBasic = adParamPO.AdParams[0].PercBasic;
                }
                if (adParamPO.AdParams[0].IsMinAmountNull() == false)
                {
                    adParam.MinAmount = adParamPO.AdParams[0].MinAmount;
                }
                if (adParamPO.AdParams[0].IsMaxAmountNull() == false)
                {
                    adParam.MaxAmount = adParamPO.AdParams[0].MaxAmount;
                }
                if (adParamPO.AdParams[0].IsPeriodicityNull() == false)
                {
                    adParam.Periodicity = adParamPO.AdParams[0].Periodicity;
                }
                if (adParamPO.AdParams[0].IsEntitleTypeNull() == false)
                {
                    adParam.EntitleType = adParamPO.AdParams[0].EntitleType;
                }
                if (adParamPO.AdParams[0].IsCurrentlyActiveNull() == false)
                {
                    adParam.CurrentlyActive = adParamPO.AdParams[0].CurrentlyActive;
                }
                if (adParamPO.AdParams[0].IsLastActiveDeacteDateNull() == false)
                {
                    adParam.LastActiveDeacteDate = adParamPO.AdParams[0].LastActiveDeacteDate;
                }
                
                if (adParamPO.AdParams[0].IsNextActivationDateNull() == false)
                {
                    adParam.NextActivationDate = adParamPO.AdParams[0].NextActivationDate;
                }
                if (adParamPO.AdParams[0].IsExpire_DateNull() == false)
                {
                    adParam.Expire_Date = adParamPO.AdParams[0].Expire_Date;
                }

                if (adParamPO.AdParams[0].IsTaxableNull() == false)
                {
                    adParam.Taxable = adParamPO.AdParams[0].Taxable;
                }
                if (adParamPO.AdParams[0].IsRegularNull() == false)
                {
                    adParam.Regular = adParamPO.AdParams[0].Regular;
                }
                if (adParamPO.AdParams[0].IsIsDependsOnAttendanceNull() == false)
                {
                    adParam.DependsOnAttendance = adParamPO.AdParams[0].IsDependsOnAttendance;
                }

                if (adParamPO.AdParams[0].IsAppliedStatNull() == false)
                {
                    adParam.AppliedStat = adParamPO.AdParams[0].AppliedStat;
                }
                if (adParamPO.AdParams[0].IsTaxProjectionNull() == false)
                {
                    adParam.TaxProjection = adParamPO.AdParams[0].TaxProjection;
                }

                if (adParamPO.AdParams[0].IsIsAlternateNull() == false)
                {
                    adParam.IsAlternate = adParamPO.AdParams[0].IsAlternate;
                }
                if (adParamPO.AdParams[0].IsAlternateAmountNull() == false)
                {
                    adParam.AlternateAmount = adParamPO.AdParams[0].AlternateAmount;
                }
                if (adParamPO.AdParams[0].IsProrataAmountNull() == false)
                {
                    adParam.ProrataAmount = adParamPO.AdParams[0].ProrataAmount;

                }
                if (adParamPO.AdParams[0].IsTaxablePercentageNull() == false)
                {
                    adParam.TaxablePercentage = adParamPO.AdParams[0].TaxablePercentage;
                }

                if (adParamPO.AdParams[0].IsProratedPaymentNull() == false)
                {
                    adParam.ProratedPayment = adParamPO.AdParams[0].ProratedPayment;
                }
                //Nahid :: 14-Nov-2019
                if (adParamPO.AdParams[0].IsBasedOnBasicNull() == false)
                {
                    adParam.BasedOnBasic = adParamPO.AdParams[0].BasedOnBasic;
                }
                if (adParamPO.AdParams[0].IsGrossHeadNull() == false)
                {
                    adParam.GrossHead = adParamPO.AdParams[0].GrossHead;
                }
                //end

                if (adParamPO.AdParams[0].IsGenderNull() == false){ adParam.Gender = adParamPO.AdParams[0].Gender;}
                if (adParamPO.AdParams[0].IsReligionNull() == false) { adParam.Religion = adParamPO.AdParams[0].Religion; }
                if (adParamPO.AdParams[0].IsDepartmentIDNull() == false) { adParam.DepartmentID = adParamPO.AdParams[0].DepartmentID; }
                if (adParamPO.AdParams[0].IsEligibleServiceYearNull() == false) { adParam.EligibleServiceYear = adParamPO.AdParams[0].EligibleServiceYear; }
                if (adParamPO.AdParams[0].IsCreate_UserNull() == false) { adParam.Create_User = adParamPO.AdParams[0].Create_User; }
                if (adParamPO.AdParams[0].IsEmployeeTypeNull() == false) { adParam.EmployeeType = adParamPO.AdParams[0].EmployeeType; }
                if (adParamPO.AdParams[0].IsExcept_EmployeeTypeNull() == false) { adParam.Except_EmployeeType = adParamPO.AdParams[0].Except_EmployeeType; }
                if (!adParamPO.AdParams[0].IsNotForCarOwnerNull()) { adParam.NotForCarOwner = adParamPO.AdParams[0].NotForCarOwner; }
                if (!adParamPO.AdParams[0].IsHandsOnPaymentNull()) { adParam.HandsOnPayment = adParamPO.AdParams[0].HandsOnPayment; }
                if (!adParamPO.AdParams[0].IsNotForSimOwnerNull()) { adParam.NotForSimOwner = adParamPO.AdParams[0].NotForSimOwner; }
                if (!adParamPO.AdParams[0].IsProba_Eligibility_PerNull()) { adParam.Proba_Eligibility_Per = adParamPO.AdParams[0].Proba_Eligibility_Per; }

                adParamDS.AdParams.AddAdParam(adParam);
                adParamDS.AcceptChanges();


                cmd.Dispose();
                cmd = DBCommandProvider.GetDBCommand("GetAdParamDetails");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["AdParamId"].Value = adParamPO.AdParams[0].AdParamId;
                adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                AdParamDetailPO paramPO = new AdParamDetailPO();

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter,
                  paramPO,
                  paramPO.AdParamDetails.TableName,
                  connDS.DBConnections[0].ConnectionID,
                  ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_ALLOWDEDUCT_GET.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }

                paramPO.AcceptChanges();

                stringDS.Clear();
                if (paramPO.AdParamDetails.Count > 0)
                {

                    foreach (AdParamDetailPO.AdParamDetail param in paramPO.AdParamDetails)
                    {
                        string sGradeCode = UtilDL.GetGradeCode(connDS, param.GradeId);

                        if (sGradeCode == null)
                        {
                            return UtilDL.GetDBOperationFailed();
                        }
                        stringDS.DataStrings.AddDataString(sGradeCode);
                    }//end of foreach
                    stringDS.AcceptChanges();
                }//end of if

            }




            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(adParamDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _getAdParamList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            longDS.Merge(inputDS.Tables[longDS.DataLongs.TableName], false, MissingSchemaAction.Error);
            longDS.AcceptChanges();


            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAdParamList"); // Jarif (26 Sep 11)
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAdParamList_New");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (longDS.DataLongs[0].IsLongValueNull() == false)
            {
                cmd.Parameters["Type"].Value = longDS.DataLongs[0].LongValue;
            }
            
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(26 Sep 11)...
            #region Old...
            //AdParamPO adParamPO = new AdParamPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, adParamPO, adParamPO.AdParams.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //adParamPO.AcceptChanges();

            //AdParamDS adParamDS = new AdParamDS();

            //for (int i = 0; i < adParamPO.AdParams.Count; i++)
            //{
            //    AdParamDS.AdParam adParam = adParamDS.AdParams.NewAdParam();
            //    string sAdCode = UtilDL.GetAllowDeductCode(connDS, adParamPO.AdParams[i].AllowDeductId);
            //    if (sAdCode == null)
            //    {
            //        UtilDL.GetDBOperationFailed();
            //    }
            //    adParam.AllowDeductCode = sAdCode;
            //    if (adParamPO.AdParams[i].IsCodeNull() == false)
            //    {
            //        adParam.Code = adParamPO.AdParams[i].Code;
            //    }
            //    if (adParamPO.AdParams[i].IsAllowDeductTypeNull() == false)
            //    {
            //        adParam.AllowDeductType = adParamPO.AdParams[i].AllowDeductType;
            //    }
            //    if (adParamPO.AdParams[i].IsForConfirmOnlyNull() == false)
            //    {
            //        adParam.ForConfirmOnly = adParamPO.AdParams[i].ForConfirmOnly;
            //    }
            //    if (adParamPO.AdParams[i].IsFlatAmountNull() == false)
            //    {
            //        adParam.FlatAmount = adParamPO.AdParams[i].FlatAmount;
            //    }
            //    if (adParamPO.AdParams[i].IsPercBasicNull() == false)
            //    {
            //        adParam.PercBasic = adParamPO.AdParams[i].PercBasic;
            //    }
            //    if (adParamPO.AdParams[i].IsMinAmountNull() == false)
            //    {
            //        adParam.MinAmount = adParamPO.AdParams[i].MinAmount;
            //    }
            //    if (adParamPO.AdParams[i].IsMaxAmountNull() == false)
            //    {
            //        adParam.MaxAmount = adParamPO.AdParams[i].MaxAmount;
            //    }
            //    if (adParamPO.AdParams[i].IsPeriodicityNull() == false)
            //    {
            //        adParam.Periodicity = adParamPO.AdParams[i].Periodicity;
            //    }
            //    if (adParamPO.AdParams[i].IsRegularNull() == false)
            //    {
            //        adParam.Regular = adParamPO.AdParams[i].Regular;
            //    }
            //    if (adParamPO.AdParams[i].IsEntitleTypeNull() == false)
            //    {
            //        adParam.EntitleType = adParamPO.AdParams[i].EntitleType;
            //    }
            //    if (adParamPO.AdParams[i].IsCurrentlyActiveNull() == false)
            //    {
            //        adParam.CurrentlyActive = adParamPO.AdParams[i].CurrentlyActive;
            //    }
            //    if (adParamPO.AdParams[i].IsLastActiveDeacteDateNull() == false)
            //    {
            //        adParam.LastActiveDeacteDate = adParamPO.AdParams[i].LastActiveDeacteDate;
            //    }
            //    if (adParamPO.AdParams[i].IsNextActivationDateNull() == false)
            //    {
            //        adParam.NextActivationDate = adParamPO.AdParams[i].NextActivationDate;
            //    }
            //    if (adParamPO.AdParams[i].IsTaxableNull() == false)
            //    {
            //        adParam.Taxable = adParamPO.AdParams[i].Taxable;
            //    }
            //    if (adParamPO.AdParams[i].IsIsDependsOnAttendanceNull() == false)
            //    {
            //        adParam.DependsOnAttendance = adParamPO.AdParams[i].IsDependsOnAttendance;
            //    }

            //    if (adParamPO.AdParams[i].IsAppliedStatNull() == false)
            //    {
            //        adParam.AppliedStat = adParamPO.AdParams[i].AppliedStat;
            //    }
            //    if (adParamPO.AdParams[i].IsTaxProjectionNull() == false)
            //    {
            //        adParam.TaxProjection = adParamPO.AdParams[i].TaxProjection;
            //    }

            //    if (adParamPO.AdParams[i].IsIsAlternateNull() == false)
            //    {
            //        adParam.IsAlternate = adParamPO.AdParams[i].IsAlternate;
            //    }
            //    if (adParamPO.AdParams[i].IsAlternateAmountNull() == false)
            //    {
            //        adParam.AlternateAmount = adParamPO.AdParams[i].AlternateAmount;
            //    }
            //    if (adParamPO.AdParams[i].IsProrataAmountNull() == false)
            //    {
            //        adParam.ProrataAmount = adParamPO.AdParams[i].ProrataAmount;

            //    }
            //    if (adParamPO.AdParams[i].IsTaxablePercentageNull() == false)
            //    {
            //        adParam.TaxablePercentage = adParamPO.AdParams[i].TaxablePercentage;
            //    }


            //    adParamDS.AdParams.AddAdParam(adParam);
            //    adParamDS.AcceptChanges();
            //    ///////////////
            //    cmd.Dispose();
            //    cmd = DBCommandProvider.GetDBCommand("GetAdParamDetails");
            //    if (cmd == null)
            //    {
            //        return UtilDL.GetCommandNotFound();
            //    }

            //    cmd.Parameters["AdParamId"].Value = adParamPO.AdParams[i].AdParamId;
            //    adapter = new OleDbDataAdapter();
            //    adapter.SelectCommand = cmd;

            //    AdParamDetailPO paramPO = new AdParamDetailPO();

            //    bError = false;
            //    nRowAffected = -1;
            //    nRowAffected = ADOController.Instance.Fill(adapter, paramPO, paramPO.AdParamDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //    if (bError == true)
            //    {
            //        ErrorDS.Error err = errDS.Errors.NewError();
            //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //        err.ErrorInfo1 = ActionID.NA_ACTION_ALLOWDEDUCT_GET.ToString();
            //        errDS.Errors.AddError(err);
            //        errDS.AcceptChanges();
            //        return errDS;

            //    }

            //    paramPO.AcceptChanges();

            //    if (paramPO.AdParamDetails.Count > 0)
            //    {
            //        foreach (AdParamDetailPO.AdParamDetail param in paramPO.AdParamDetails)
            //        {
            //            string sGradeCode = UtilDL.GetGradeCode(connDS, param.GradeId);

            //            if (sGradeCode == null)
            //            {
            //                return UtilDL.GetDBOperationFailed();
            //            }
            //            AdParamDS.GradeRow gradeRow = adParamDS.Grade.NewGradeRow();
            //            gradeRow.Code = sGradeCode;
            //            gradeRow.AdParam = adParam;
            //            adParamDS.Grade.AddGradeRow(gradeRow);
            //        }//end of foreach
            //    }
            //    //////////////
            //}
            #endregion

            #region Load AdParams Data...
            AdParamDS adParamDS = new AdParamDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, adParamDS, adParamDS.AdParams.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            adParamDS.AcceptChanges();
            #endregion
            #region Load AdParams Data...
            cmd.Dispose();
            cmd = DBCommandProvider.GetDBCommand("GetAdParamDetails_New");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["Type"].Value = longDS.DataLongs[0].LongValue;
            adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, adParamDS, adParamDS.Grade.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_ALLOWDEDUCT_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            adParamDS.AcceptChanges();
            #endregion
            
            #endregion
            
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(adParamDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _updateAdParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AdParamDS adParamDS = new AdParamDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //extract allowance/deduction
            adParamDS.Merge(inputDS.Tables[adParamDS.AdParams.TableName], false, MissingSchemaAction.Error);
            adParamDS.Merge(inputDS.Tables[adParamDS.Grade.TableName], false, MissingSchemaAction.Error);
            adParamDS.AcceptChanges();

            foreach (AdParamDS.AdParam adParam in adParamDS.AdParams)
            {
                //create command for insert allowance/deduction
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateAdParam");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
                bool bError = false;
                int nRowAffected = -1;
                long nAdParamId = -1;

                if (adParam.IsCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)adParam.Code;
                }
                long nAdId = UtilDL.GetAllowDeductId(connDS, adParam.AllowDeductCode);
                if (nAdId == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["AdId"].Value = (object)nAdId;
                if (adParam.IsAllowDeductTypeNull() == false)
                {
                    cmd.Parameters["AllowDeductType"].Value = (object)adParam.AllowDeductType;
                }
                if (adParam.IsForConfirmOnlyNull() == false)
                {
                    cmd.Parameters["ForConfirm"].Value = (object)adParam.ForConfirmOnly;
                }
                if (adParam.IsFlatAmountNull() == false)
                {
                    cmd.Parameters["FlatAmount"].Value = (object)adParam.FlatAmount;
                }
                if (adParam.IsPercBasicNull() == false)
                {
                    cmd.Parameters["PercBasic"].Value = (object)adParam.PercBasic;
                }
                if (adParam.IsMinAmountNull() == false)
                {
                    cmd.Parameters["MinAmount"].Value = (object)adParam.MinAmount;
                }
                if (adParam.IsMaxAmountNull() == false)
                {
                    cmd.Parameters["MaxAmount"].Value = (object)adParam.MaxAmount;
                }
                if (adParam.IsPeriodicityNull() == false)
                {
                    cmd.Parameters["Periodicity"].Value = (object)adParam.Periodicity;
                }
                if (adParam.IsEntitleTypeNull() == false)
                {
                    cmd.Parameters["EntitleType"].Value = (object)adParam.EntitleType;
                    #region Old...
                    //if (adParam.EntitleType == false)
                    //{
                    //    nAdParamId = UtilDL.GetAdParamId(connDS, adParam.Code);
                    //    if (nAdParamId == -1)
                    //    {
                    //        return UtilDL.GetDBOperationFailed();
                    //    }

                    //    //OleDbCommand cmdTemp = DBCommandProvider.GetDBCommand("DeleteIndividualAllowance");
                    //    //if (cmdTemp == null)
                    //    //{
                    //    //    return UtilDL.GetCommandNotFound();
                    //    //}

                    //    //cmdTemp.Parameters["AdParamId"].Value = nAdParamId;

                    //    OleDbCommand cmdTemp = new OleDbCommand();
                    //    cmdTemp.CommandText = "PRO_IND_ALLOWANCE_DELETE";
                    //    cmdTemp.CommandType = CommandType.StoredProcedure;
                    //    if (cmdTemp == null) return UtilDL.GetCommandNotFound();

                    //    cmdTemp.Parameters.AddWithValue("p_AdParamID", nAdParamId);
                    //    cmdTemp.Parameters.AddWithValue("p_Delete_User", adParam.Create_User);


                    //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdTemp, connDS.DBConnections[0].ConnectionID, ref bError);

                    //    if (bError == true)
                    //    {
                    //        ErrorDS.Error err = errDS.Errors.NewError();
                    //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    //        err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                    //        errDS.Errors.AddError(err);
                    //        errDS.AcceptChanges();
                    //        return errDS;
                    //    }

                    //}
                    #endregion
                }

                if (adParam.IsAllowDeductTypeNull() == false)
                {
                    cmd.Parameters["CurrentlyActive"].Value = (object)adParam.CurrentlyActive;
                }
                // Nahid :: 14-Nov-2019
                cmd.Parameters["BasedOnBasic"].Value = (!adParam.IsBasedOnBasicNull()) ? (object)adParam.BasedOnBasic : System.DBNull.Value;
                cmd.Parameters["GrossHead"].Value = (!adParam.IsGrossHeadNull()) ? (object)adParam.GrossHead : System.DBNull.Value;
                // end
                if (adParam.IsRegularNull() == false)
                {
                    cmd.Parameters["Regular"].Value = (object)adParam.Regular;
                }
                else
                {
                    cmd.Parameters["Regular"].Value = System.DBNull.Value;
                }
                if (adParam.IsLastActiveDeacteDateNull() == false)
                {
                    cmd.Parameters["LastActiveDeacteDate"].Value = (object)adParam.LastActiveDeacteDate;
                }
                else
                {
                    cmd.Parameters["LastActiveDeacteDate"].Value = System.DBNull.Value;
                }
                
                if (adParam.IsNextActivationDateNull() == false)
                {
                    cmd.Parameters["NextActivationDate"].Value = (object)adParam.NextActivationDate;
                }
                else
                {
                    cmd.Parameters["NextActivationDate"].Value = System.DBNull.Value;
                }

                if (adParam.IsExpire_DateNull() == false)
                {
                    cmd.Parameters["ExpireDate"].Value = (object)adParam.Expire_Date;
                }
                else
                {
                    cmd.Parameters["ExpireDate"].Value = System.DBNull.Value;
                }

                if (adParam.IsTaxableNull() == false)
                {
                    cmd.Parameters["Taxable"].Value = (object)adParam.Taxable;
                }
                if (adParam.IsDependsOnAttendanceNull() == false)
                {
                    cmd.Parameters["DependsOnAttendance"].Value = (object)adParam.DependsOnAttendance;
                }

                if (adParam.IsAppliedStatNull() == false)
                {
                    cmd.Parameters["AppliedStat"].Value = (object)adParam.AppliedStat;
                }
                if (adParam.IsTaxProjectionNull() == false)
                {
                    cmd.Parameters["TaxProjection"].Value = (object)adParam.TaxProjection;
                }
                if (adParam.IsProratedPaymentNull() == false)
                {
                    cmd.Parameters["ProratedPayment"].Value = (object)adParam.ProratedPayment;
                }
                else
                {
                    cmd.Parameters["ProratedPayment"].Value = System.DBNull.Value;
                }


                if (adParam.IsGenderNull() == false) { cmd.Parameters["Gender"].Value = (object)adParam.Gender; }
                else { cmd.Parameters["Gender"].Value = System.DBNull.Value; }

                if (adParam.IsReligionNull() == false) { cmd.Parameters["Religion"].Value = (object)adParam.Religion; }
                else { cmd.Parameters["Religion"].Value = System.DBNull.Value; }

                if (adParam.IsDepartmentIDNull() == false) { cmd.Parameters["DepartmentID"].Value = (object)adParam.DepartmentID; }
                else { cmd.Parameters["DepartmentID"].Value = System.DBNull.Value; }

                if (adParam.IsEligibleServiceYearNull() == false) { cmd.Parameters["EligibleServiceYear"].Value = (object)adParam.EligibleServiceYear; }
                else { cmd.Parameters["EligibleServiceYear"].Value = System.DBNull.Value; }

                if (adParam.IsCreate_UserNull() == false) { cmd.Parameters["Create_User"].Value = (object)adParam.Create_User; }
                else { cmd.Parameters["Create_User"].Value = System.DBNull.Value; }

                if (adParam.IsEmployeeTypeNull() == false) { cmd.Parameters["EmployeeType"].Value = (object)adParam.EmployeeType; }
                else { cmd.Parameters["EmployeeType"].Value = System.DBNull.Value; }

                if (adParam.IsExcept_EmployeeTypeNull() == false) { cmd.Parameters["Except_EmployeeType"].Value = (object)adParam.Except_EmployeeType; }
                else { cmd.Parameters["Except_EmployeeType"].Value = System.DBNull.Value; }

                if (!adParam.IsNotForCarOwnerNull()) { cmd.Parameters["NotForCarOwner"].Value = (object)adParam.NotForCarOwner; }
                else { cmd.Parameters["NotForCarOwner"].Value = System.DBNull.Value; }

                if (!adParam.IsHandsOnPaymentNull()) { cmd.Parameters["HandsOnPayment"].Value = (object)adParam.HandsOnPayment; }
                else { cmd.Parameters["HandsOnPayment"].Value = 0; }

                if (!adParam.IsNotForSimOwnerNull()) { cmd.Parameters["NotForSimOwner"].Value = (object)adParam.NotForSimOwner; }
                else { cmd.Parameters["NotForSimOwner"].Value = System.DBNull.Value; }
                if (!adParam.IsProba_Eligibility_PerNull()) { cmd.Parameters["Proba_Eligibility_Per"].Value = (object)adParam.Proba_Eligibility_Per; }
                else { cmd.Parameters["Proba_Eligibility_Per"].Value = System.DBNull.Value; }

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCTPARAM_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


                nAdParamId = UtilDL.GetAdParamId(connDS, adParam.Code);
                if (nAdParamId == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                #region Updated by Rony :: Insert and delete grades and Employee from AD param Detail and AD Employee
                string sGradeCode = "";
                foreach (AdParamDS.GradeRow grade in adParam.GetGradeRows())
                {
                    sGradeCode += grade.Code + ",";
                }
                sGradeCode += "-1";
                OleDbCommand cmdTemp = new OleDbCommand();
                cmdTemp.CommandText = "PRO_AD_PARAM_EMP_DETAIL_DEL";
                cmdTemp.CommandType = CommandType.StoredProcedure;
                if (cmdTemp == null) return UtilDL.GetCommandNotFound();

                cmdTemp.Parameters.AddWithValue("p_AdParamID", nAdParamId);
                cmdTemp.Parameters.AddWithValue("p_Delete_User", adParam.Create_User);
                cmdTemp.Parameters.AddWithValue("p_GradeCode", sGradeCode);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdTemp, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                #region Old :: Commented by Rony :: 19 Apr 2017
                //cmd.Dispose(); //create command for inserting grades for this allowance
                //cmd = DBCommandProvider.GetDBCommand("CreateAdParamDetails");
                //if (cmd == null)
                //{
                //    return UtilDL.GetCommandNotFound();
                //}

                //cmd.Parameters["AdParamId"].Value = nAdParamId;
                
                //foreach (AdParamDS.GradeRow grade in adParam.GetGradeRows())
                //{
                //    long nGradeId = UtilDL.GetGradeId(connDS, grade.Code);
                //    if (nGradeId == -1)
                //    {
                //        return UtilDL.GetDBOperationFailed();
                //    }                    

                //    cmd.Parameters["GradeId"].Value = (object)nGradeId;
                    
                //    bError = false;
                //    nRowAffected = -1;
                //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                //    if (bError == true)
                //    {
                //        ErrorDS.Error err = errDS.Errors.NewError();
                //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //        err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                //        errDS.Errors.AddError(err);
                //        errDS.AcceptChanges();
                //        return errDS;
                //    }
                //}
                #endregion
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        
        private DataSet _deleteAdParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            bool bError = false;
            int nRowAffected = -1;

            //extract connectionDS
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            //extract Allowance/deduction parameter code
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            

            #region For deleting grades for this allowance...
            long nAdParamId = UtilDL.GetAdParamId(connDS, stringDS.DataStrings[0].StringValue);
            if (nAdParamId == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }
            long nAdId = UtilDL.GetAllowDeductId(connDS, stringDS.DataStrings[1].StringValue);
            if (nAdId == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }
            long nUserID = UtilDL.GetUserId(connDS, stringDS.DataStrings[2].StringValue);
            if (nAdId == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            OleDbCommand cmdTemp = new OleDbCommand();
            cmdTemp.CommandText = "PRO_AllowdeductParamDetail_DEL";
            cmdTemp.CommandType = CommandType.StoredProcedure;
            if (cmdTemp == null) return UtilDL.GetCommandNotFound();

            cmdTemp.Parameters.AddWithValue("p_AdParamID", nAdParamId);
            cmdTemp.Parameters.AddWithValue("p_AdID", nAdId);
            cmdTemp.Parameters.AddWithValue("p_Delete_User", nUserID);


            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdTemp, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion


            //delete Adparameter

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteAdParam");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _doesGradeUsedInAllowDeduct(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesGradeUsedInAllowDeduct");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["AllowDeductCode"].Value = (object)stringDS.DataStrings[0].StringValue;
            }
            if (stringDS.DataStrings[2].IsStringValueNull() == false)
            {
                cmd.Parameters["AdParamCode"].Value = (object)stringDS.DataStrings[2].StringValue;
            }
            if (stringDS.DataStrings[1].IsStringValueNull() == false)
            {
                cmd.Parameters["GradeCode"].Value = (object)stringDS.DataStrings[1].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _doesGradeExistInAllowDeduct(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesGradeExistInAllowDeduct");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["AllowDeductCode"].Value = (object)stringDS.DataStrings[0].StringValue;
            }
            if (stringDS.DataStrings[1].IsStringValueNull() == false)
            {
                cmd.Parameters["GradeCode"].Value = (object)stringDS.DataStrings[1].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

    }
}
