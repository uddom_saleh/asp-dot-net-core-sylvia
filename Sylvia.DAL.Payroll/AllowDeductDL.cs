/* Compamy: Milllennium Information Solution Limited
 * Author: Mahbubur Rahman khan
 * Comment Date: March 28, 2006
 * */


using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for AllowDeductDL.
    /// </summary>
    public class AllowDeductDL
    {
        public AllowDeductDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_ALLOWDEDUCT_ADD:
                    return _createAllowDeduct(inputDS);

                case ActionID.ACTION_ALLOWDEDUCT_DEL:
                    return _deleteAllowDeduct(inputDS);
                case ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST:
                    return _getAllowDeductList(inputDS);
                case ActionID.ACTION_ALLOWDEDUCT_UPD:
                    return this._updateAllowDeduct(inputDS);
                case ActionID.ACTION_DOES_ALLOWDEDUCT_EXIST:
                    return this._doesAllowDeductExist(inputDS);

                case ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST_ADPARAM:
                    return this._getAllowDeductListByADParam(inputDS);

                case ActionID.NA_ACTION_GET_ALLOWDEDUCT_ALLTYPE_LIST:
                    return this._getAllowDeductAllType(inputDS);
                case ActionID.NA_ACTION_GET_ALLOWDEDUCT_CONFIG_INFO:
                    return this.getDeductionConfigInfo(inputDS);
                case ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST_BY_PARAM:
                    return this._getAllowDeductListByParam(inputDS);
                case ActionID.ACTION_SUSPEND_RULE_ADD:
                    return this._SuspendRuleCreate(inputDS);
                case ActionID.NA_ACTION_GET_Data_Existence_ALS:
                    return this._getDataExistence_ALS(inputDS);
                case ActionID.NA_ACTION_GET_ALLOWANCES_FOR_SALARY_ARREAR:
                    return this._getAllowForSalaryArrear(inputDS);
                case ActionID.NA_ACTION_GET_ALLOWDEDUCTPARAM_ALL_SP:
                    return _getAllowDeductParamAll(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEEALLOWDEDUCT_SP:
                    return _getEmployeeAllowDeduct(inputDS);
                case ActionID.ACTION_DISCIPLINARY_RULE_ADD:
                    return this._DisciplinaryActionCreate(inputDS);

                case ActionID.ACTION_PAST_EMP_TAXABLE_INCOME_CREATE:
                    return this._createPastEmploymentTaxableIncome(inputDS);

                //case ActionID.NA_ACTION_GET_TAX_PARAM_ID_BY_FISCAL_YEAR:
                //    return this._getTaxParamIdByFiscalYear(inputDS); 

                case ActionID.NA_ACTION_GET_ALLOWDEDUCT:
                    return this._getAllowDeduct(inputDS);

                case ActionID.NA_ACTION_SALARY_OUTSIDE_NOP_LIST:
                    return this._getSalaryOutside_NOP(inputDS);

                case ActionID.NA_ACTION_SALARY_OUTSIDE_MOP_LIST:
                    return this._getSalaryOutside_MOP(inputDS);

                case ActionID.NA_ACTION_GET_ALLOWDEDUCTPARAM_BYAD_SP:
                    return _getAllowDeductParam_BYAD(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEEALLOWDEDUCT_BYAD_SP:
                    return _getEmployeeAllowDeduct_BYAD(inputDS);

                case ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST_ALL:
                    return _getAllowDeductListAll(inputDS);

                case ActionID.NA_ACTION_GET_ALLOWDEDUCTLISTWITH_LOAN:
                    return _getAllowDeductListWithLoan(inputDS);

                case ActionID.ACTION_SET_ALLOW_DEDUCT_ORDER:
                    return _updateAllowDeductOrder(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    AppLogger.LogError(returnDS.GetXml());
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _createAllowDeduct(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AllowDeductDS allowDS = new AllowDeductDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateAllowDeduct");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            allowDS.Merge(inputDS.Tables[allowDS.AllowDeducts.TableName], false, MissingSchemaAction.Error);
            allowDS.AcceptChanges();

            foreach (AllowDeductDS.AllowDeduct allow in allowDS.AllowDeducts)
            {

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();

                cmd.Parameters["Id"].Value = (object)genPK;
                cmd.Parameters["Code"].Value = (object)allow.Code;
                cmd.Parameters["Name"].Value = (object)allow.Name;
                cmd.Parameters["Type"].Value = (object)allow.Type;
                cmd.Parameters["IsActive"].Value = (object)allow.IsActive;

                if (!allow.IsLeaveEncashmentNull()) cmd.Parameters["LeaveEncashment"].Value = (object)allow.LeaveEncashment;
                else cmd.Parameters["LeaveEncashment"].Value = DBNull.Value;

                if (!allow.IsLeaveWithoutPayNull()) cmd.Parameters["LeaveWithoutPay"].Value = (object)allow.LeaveWithoutPay;
                else cmd.Parameters["LeaveWithoutPay"].Value = DBNull.Value;

                if (!allow.IsOverTimeNull()) cmd.Parameters["OverTime"].Value = (object)allow.OverTime;
                else cmd.Parameters["OverTime"].Value = DBNull.Value;

                if (!allow.IsAttendancePenaltyNull()) cmd.Parameters["AttendancePenalty"].Value = (object)allow.AttendancePenalty;
                else cmd.Parameters["AttendancePenalty"].Value = DBNull.Value;

                cmd.Parameters["RefundableArrear"].Value = (object)allow.RefundableArrear;
                cmd.Parameters["ArrearApplicable"].Value = (object)allow.ArrearApplicable;

                if (!allow.IsDepend_On_KPINull()) cmd.Parameters["Depend_On_KPI"].Value = allow.Depend_On_KPI;
                else cmd.Parameters["Depend_On_KPI"].Value = false;

                cmd.Parameters["PerquisitesHead"].Value = (object)allow.PerquisitesHead;

                if (!allow.IsRemarksNull()) cmd.Parameters["Remarks"].Value = (object)allow.Remarks;
                else cmd.Parameters["Remarks"].Value = DBNull.Value;

                cmd.Parameters["OutsidePayrollItem"].Value = (object)allow.OutsidePayrollItem;
                if (!allow.IsSalary_Outside_NOP_IDNull()) cmd.Parameters["Salary_Outside_NOP_ID"].Value = (object)allow.Salary_Outside_NOP_ID;
                else cmd.Parameters["Salary_Outside_NOP_ID"].Value = DBNull.Value;

                if (!allow.IsSalary_Outside_MOP_IDNull()) cmd.Parameters["Salary_Outside_MOP_ID"].Value = (object)allow.Salary_Outside_MOP_ID;
                else cmd.Parameters["Salary_Outside_MOP_ID"].Value = DBNull.Value;




                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getAllowDeductList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            longDS.Merge(inputDS.Tables[longDS.DataLongs.TableName], false, MissingSchemaAction.Error);
            longDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductList_New");
            cmd.Parameters["Type"].Value = longDS.DataLongs[0].LongValue;



            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //AllowDeductPO allowPO = new AllowDeductPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, allowPO, allowPO.AllowDeducts.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //allowPO.AcceptChanges();

            //AllowDeductDS allowDS = new AllowDeductDS();
            //if (allowPO.AllowDeducts.Count > 0)
            //{


            //    foreach (AllowDeductPO.AllowDeduct poAllow in allowPO.AllowDeducts)
            //    {
            //        AllowDeductDS.AllowDeduct allow = allowDS.AllowDeducts.NewAllowDeduct();
            //        allow.Code = poAllow.Code;
            //        allow.Name = poAllow.Name;
            //        allow.Type = poAllow.Type;
            //        allowDS.AllowDeducts.AddAllowDeduct(allow);
            //        allowDS.AcceptChanges();

            //    }
            //}
            #endregion

            AllowDeductDS allowDS = new AllowDeductDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.AllowDeducts.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            allowDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(allowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _updateAllowDeduct(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AllowDeductDS allowDS = new AllowDeductDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //extract allowance/deduction
            allowDS.Merge(inputDS.Tables[allowDS.AllowDeducts.TableName], false, MissingSchemaAction.Error);
            allowDS.AcceptChanges();

            foreach (AllowDeductDS.AllowDeduct allow in allowDS.AllowDeducts)
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateAllowDeduct");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["Code"].Value = (object)allow.Code;
                cmd.Parameters["Name"].Value = (object)allow.Name;
                cmd.Parameters["Type"].Value = (object)allow.Type;
                cmd.Parameters["IsActive"].Value = (object)allow.IsActive;

                if (!allow.IsLeaveEncashmentNull()) cmd.Parameters["LeaveEncashment"].Value = (object)allow.LeaveEncashment;
                else cmd.Parameters["LeaveEncashment"].Value = DBNull.Value;

                if (!allow.IsLeaveWithoutPayNull()) cmd.Parameters["LeaveWithoutPay"].Value = (object)allow.LeaveWithoutPay;
                else cmd.Parameters["LeaveWithoutPay"].Value = DBNull.Value;

                if (!allow.IsOverTimeNull()) cmd.Parameters["OverTime"].Value = (object)allow.OverTime;
                else cmd.Parameters["OverTime"].Value = DBNull.Value;

                if (!allow.IsAttendancePenaltyNull()) cmd.Parameters["AttendancePenalty"].Value = (object)allow.AttendancePenalty;
                else cmd.Parameters["AttendancePenalty"].Value = DBNull.Value;

                cmd.Parameters["RefundableArrear"].Value = (object)allow.RefundableArrear;
                cmd.Parameters["ArrearApplicable"].Value = (object)allow.ArrearApplicable;

                if (!allow.IsDepend_On_KPINull()) cmd.Parameters["Depend_On_KPI"].Value = allow.Depend_On_KPI;
                else cmd.Parameters["Depend_On_KPI"].Value = false;

                cmd.Parameters["PerquisitesHead"].Value = (object)allow.PerquisitesHead;

                if (!allow.IsRemarksNull()) cmd.Parameters["Remarks"].Value = (object)allow.Remarks;
                else cmd.Parameters["Remarks"].Value = DBNull.Value;
                cmd.Parameters["OutsidePayrollItem"].Value = (object)allow.OutsidePayrollItem;

                if (!allow.IsSalary_Outside_NOP_IDNull()) cmd.Parameters["Salary_Outside_NOP_ID"].Value = (object)allow.Salary_Outside_NOP_ID;
                else cmd.Parameters["Salary_Outside_NOP_ID"].Value = DBNull.Value;

                if (!allow.IsSalary_Outside_MOP_IDNull()) cmd.Parameters["Salary_Outside_MOP_ID"].Value = (object)allow.Salary_Outside_MOP_ID;
                else cmd.Parameters["Salary_Outside_MOP_ID"].Value = DBNull.Value;


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _doesAllowDeductExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesAllowDeductExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _deleteAllowDeduct(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract connectionDS
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            //extract Allowance/deduction parameter code
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            //delete grades 
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteAllowDeduct");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ALLOWDEDUCT_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _getAllowDeductListByADParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            longDS.Merge(inputDS.Tables[longDS.DataLongs.TableName], false, MissingSchemaAction.Error);
            longDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetADListByADParam");
            cmd.Parameters["Type"].Value = longDS.DataLongs[0].LongValue;




            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AllowDeductDS allowDS = new AllowDeductDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.AllowDeducts.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST_ADPARAM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            allowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(allowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _getAllowDeductAllType(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            AllowDeductDS allowDS = new AllowDeductDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductAllTypeList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.AllowDeducts.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GL_MAPPING_DATA_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            allowDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(allowDS.AllowDeducts);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet getDeductionConfigInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[0].StringValue == "DeductionTagedInfoWithGrade")
            {
                cmd = DBCommandProvider.GetDBCommand("GetDeductionConfigInfo");
                cmd.Parameters["LoanCode"].Value = stringDS.DataStrings[1].StringValue;
            }
            else if (stringDS.DataStrings[0].StringValue == "DeductionTagedInfoWithLoan")
            {
                cmd = DBCommandProvider.GetDBCommand("GetDeductionConfigInfo_2");
                cmd.Parameters["LoanCode"].Value = stringDS.DataStrings[1].StringValue;
            }
            else if (stringDS.DataStrings[0].StringValue == "FilteredGradeListByDeductionCode")
            {
                cmd = DBCommandProvider.GetDBCommand("GetFilteredGradeListByDeductionCode");
                cmd.Parameters["LoanCode"].Value = stringDS.DataStrings[1].StringValue;
            }
            else if (stringDS.DataStrings[0].StringValue == "GradeListByLoanCodeToUpdate")
            {
                cmd = DBCommandProvider.GetDBCommand("GetGradeListByLoanCodeToUpdate");
                cmd.Parameters["LoanCode"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["ConcatedEmployee1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["ConcatedEmployee2"].Value = stringDS.DataStrings[2].StringValue;
            }
            else if (stringDS.DataStrings[0].StringValue == "AllowDeductDependencyCheck")
            {
                cmd = DBCommandProvider.GetDBCommand("GetAllowDeductDependencyInfo");
                cmd.Parameters["Code"].Value = stringDS.DataStrings[1].StringValue;
            }


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AllowDeductDS allowDS = new AllowDeductDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.AllowDeducts.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_CONFIG_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            allowDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(allowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _getAllowDeductListByParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            EmployeeDS EmpDS = new EmployeeDS();

            if (stringDS.DataStrings[0].StringValue == "SuspendRule")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductList_SuspendRule");
                //cmd.Parameters["DisciplinaryTypeID"].Value = stringDS.DataStrings[1].StringValue;

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpDS, EmpDS.SuspendRule_AllowDeduct.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST_BY_PARAM.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpDS.AcceptChanges();


                //Get Facilities held
                cmd.Dispose();
                adapter.Dispose();
                cmd = DBCommandProvider.GetDBCommand("GetSuspendFacilitiesHeld");
                adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpDS, EmpDS.SuspendRule_General.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST_BY_PARAM.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpDS.AcceptChanges();

            }

            else if (stringDS.DataStrings[0].StringValue == "DisciplinaryActionRule")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDisAction_SuspendRule");
                cmd.Parameters["DisciplinaryTypeID"].Value = stringDS.DataStrings[1].StringValue;

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpDS, EmpDS.SuspendRule_AllowDeduct.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST_BY_PARAM.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpDS.AcceptChanges();


                //Get Facilities held
                cmd.Dispose();
                adapter.Dispose();
                cmd = DBCommandProvider.GetDBCommand("GetDisActionFacilitiesHeld");
                cmd.Parameters["DisciplinaryTypeID"].Value = stringDS.DataStrings[1].StringValue;
                adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpDS, EmpDS.SuspendRule_General.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST_BY_PARAM.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpDS.AcceptChanges();

            }
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(EmpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _SuspendRuleCreate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeDS EmpDS = new EmployeeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //extract allowance/deduction
            EmpDS.Merge(inputDS.Tables[EmpDS.SuspendRule_AllowDeduct.TableName], false, MissingSchemaAction.Error);
            EmpDS.Merge(inputDS.Tables[EmpDS.SuspendRule_General.TableName], false, MissingSchemaAction.Error);
            EmpDS.AcceptChanges();


            #region Old Rule Delete
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SUSPEND_RULE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            //cmd.Parameters.AddWithValue("p_DisciplinaryTypeID", EmpDS.SuspendRule_AllowDeduct[0].DisciplinaryTypeID);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SUSPEND_RULE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion


            //create command for insert allowance/deduction            



            foreach (EmployeeDS.SuspendRule_AllowDeductRow row in EmpDS.SuspendRule_AllowDeduct)
            {
                cmd.Dispose();
                cmd.CommandText = "PRO_SUSPEND_RULE_CREATE";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                //long genPK = IDGenerator.GetNextGenericPK();
                //if (genPK == -1)
                //{
                //    UtilDL.GetDBOperationFailed();
                //}
                //cmd.Parameters.AddWithValue("Id", genPK);


                cmd.Parameters.AddWithValue("p_AllowDeductCode", row.AllowDeductCode);

                if (!row.IsAllowDeductCodeNull()) cmd.Parameters.AddWithValue("p_AllowDeductName", row.AllowDeductName);
                else cmd.Parameters.AddWithValue("p_AllowDeductName", DBNull.Value);

                if (!row.IsApplicable_PercentNull()) cmd.Parameters.AddWithValue("p_Applicable_Percent", row.Applicable_Percent);
                else cmd.Parameters.AddWithValue("p_Applicable_Percent", DBNull.Value);

                if (!row.IsDisciplinaryTypeIDNull()) cmd.Parameters.AddWithValue("p_DisciplinaryTypeID", row.DisciplinaryTypeID);
                cmd.Parameters.AddWithValue("p_DisciplinaryTypeID", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SUSPEND_RULE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }

            //create command for Following Facilities held            
            cmd.Dispose();
            cmd.CommandText = "PRO_SUSPEND_FACILITIES_HELD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            foreach (EmployeeDS.SuspendRule_GeneralRow row in EmpDS.SuspendRule_General)
            {


                cmd.Parameters.AddWithValue("p_Promotion_Held", row.Promotion_held);
                cmd.Parameters.AddWithValue("p_Increment_held", row.Increment_held);
                cmd.Parameters.AddWithValue("p_PayFixation_held", row.PayFixation_held);
                cmd.Parameters.AddWithValue("p_LoanIssue_held", row.LoanIssue_held);
                cmd.Parameters.AddWithValue("p_Transfer_held", row.Transfer_held);
                cmd.Parameters.AddWithValue("p_LeaveApply_held", row.LeaveApply_held);
                cmd.Parameters.AddWithValue("p_LFA_held", row.LFA_held);
                cmd.Parameters.AddWithValue("p_Requisition_held", row.Requisition_held);
                cmd.Parameters.AddWithValue("p_Gratuity_held", row.Gratuity_held);
                cmd.Parameters.AddWithValue("p_WPPWF_held", row.WPPWF_held);
                cmd.Parameters.AddWithValue("p_Training_held", row.Training_held);


                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SUSPEND_RULE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();

            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getDataExistence_ALS(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDataExistence_ALS");
            cmd.Parameters["SurveyData"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SurveyCode"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SurveyData1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SurveyCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SurveyData2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SurveyCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SurveyData3"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SurveyCode3"].Value = stringDS.DataStrings[1].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AllowDeductDS allowDS = new AllowDeductDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.Data_Existence.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_Data_Existence_ALS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            allowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(allowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _getAllowForSalaryArrear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            AllowDeductDS allowDS = new AllowDeductDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowancesForSalaryArrear");

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.AllowDeducts.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWANCES_FOR_SALARY_ARREAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            allowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(allowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAllowDeductParamAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataBoolDS boolDS = new DataBoolDS();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductParamList_SP");
            cmd.Parameters["OutSidePayrollItem"].Value = (object)boolDS.DataBools[0].BoolValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AdParamDS allowDS = new AdParamDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.AllowDeductParamDetail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCTPARAM_ALL_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            allowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(allowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAllowDeductParam_BYAD(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataBoolDS boolDS = new DataBoolDS();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductParamList_BYAD_SP");
            cmd.Parameters["OutSidePayrollItem"].Value = (object)boolDS.DataBools[0].BoolValue;
            cmd.Parameters["ADID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AdParamDS allowDS = new AdParamDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.AllowDeductParamDetail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCTPARAM_BYAD_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            allowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(allowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getEmployeeAllowDeduct(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataBoolDS boolDS = new DataBoolDS();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeAllowDeduct_SP");
            if (cmd == null) return UtilDL.GetDBOperationFailed();

            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["OutSidePayrollItem"].Value = (object)boolDS.DataBools[0].BoolValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeAllowanceDS empAllowDS = new EmployeeAllowanceDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empAllowDS, empAllowDS.EmployeeAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEEALLOWDEDUCT_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            empAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(empAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getEmployeeAllowDeduct_BYAD(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataBoolDS boolDS = new DataBoolDS();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeAllowDeduct_BYAD_SP");
            if (cmd == null) return UtilDL.GetDBOperationFailed();

            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["ADID"].Value = (object)integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["OutSidePayrollItem"].Value = (object)boolDS.DataBools[0].BoolValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeAllowanceDS empAllowDS = new EmployeeAllowanceDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empAllowDS, empAllowDS.EmployeeAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEEALLOWDEDUCT_BYAD_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            empAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(empAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _DisciplinaryActionCreate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeDS EmpDS = new EmployeeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //extract allowance/deduction
            EmpDS.Merge(inputDS.Tables[EmpDS.SuspendRule_AllowDeduct.TableName], false, MissingSchemaAction.Error);
            EmpDS.Merge(inputDS.Tables[EmpDS.SuspendRule_General.TableName], false, MissingSchemaAction.Error);
            EmpDS.AcceptChanges();

            #region Old Rule Delete
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_DISCIPLINARY_ACTION_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_DisciplinaryTypeID", EmpDS.SuspendRule_AllowDeduct[0].DisciplinaryTypeID);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DISCIPLINARY_RULE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            //create command for insert allowance/deduction    

            foreach (EmployeeDS.SuspendRule_AllowDeductRow row in EmpDS.SuspendRule_AllowDeduct)
            {
                cmd.Dispose();
                cmd.CommandText = "PRO_DISCIPLINARY_ACTION_CREATE";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters.AddWithValue("p_AllowDeductCode", row.AllowDeductCode);

                if (!row.IsAllowDeductCodeNull()) cmd.Parameters.AddWithValue("p_AllowDeductName", row.AllowDeductName);
                else cmd.Parameters.AddWithValue("p_AllowDeductName", DBNull.Value);

                if (!row.IsApplicable_PercentNull()) cmd.Parameters.AddWithValue("p_Applicable_Percent", row.Applicable_Percent);
                else cmd.Parameters.AddWithValue("p_Applicable_Percent", DBNull.Value);

                if (!row.IsDisciplinaryTypeIDNull()) cmd.Parameters.AddWithValue("p_DisciplinaryTypeID", row.DisciplinaryTypeID);
                cmd.Parameters.AddWithValue("p_DisciplinaryTypeID", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DISCIPLINARY_RULE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            //create command for Following Facilities held            
            cmd.Dispose();
            cmd.CommandText = "PRO_DIS_ACTION_FACILITIES_HELD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeDS.SuspendRule_GeneralRow row in EmpDS.SuspendRule_General)
            {
                cmd.Parameters.AddWithValue("p_Promotion_Held", row.Promotion_held);
                cmd.Parameters.AddWithValue("p_Increment_held", row.Increment_held);
                cmd.Parameters.AddWithValue("p_PayFixation_held", row.PayFixation_held);
                cmd.Parameters.AddWithValue("p_LoanIssue_held", row.LoanIssue_held);
                cmd.Parameters.AddWithValue("p_Transfer_held", row.Transfer_held);
                cmd.Parameters.AddWithValue("p_LeaveApply_held", row.LeaveApply_held);
                cmd.Parameters.AddWithValue("p_LFA_held", row.LFA_held);
                cmd.Parameters.AddWithValue("p_Requisition_held", row.Requisition_held);
                cmd.Parameters.AddWithValue("p_Gratuity_held", row.Gratuity_held);
                cmd.Parameters.AddWithValue("p_WPPWF_held", row.WPPWF_held);
                cmd.Parameters.AddWithValue("p_Training_held", row.Training_held);
                cmd.Parameters.AddWithValue("p_DisciplinaryTypeID", row.DisciplinaryTypeID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DISCIPLINARY_RULE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        #region Arnob :: Past Employee Taxable Income :: 01 October, 2019
        private DataSet _createPastEmploymentTaxableIncome(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AllowDeductDS allowDeductDS = new AllowDeductDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_PAST_EMP_TAX";
            cmd.CommandType = CommandType.StoredProcedure;

            allowDeductDS.Merge(inputDS.Tables[allowDeductDS.PastEmploymentTaxableIncome.TableName], false, MissingSchemaAction.Error);
            allowDeductDS.AcceptChanges();

            foreach (AllowDeductDS.PastEmploymentTaxableIncomeRow row in allowDeductDS.PastEmploymentTaxableIncome)
            {
                cmd.Parameters.AddWithValue("p_ADId", row.ADId);
                cmd.Parameters.AddWithValue("p_EmployeeId", row.EmployeeId);

                if (!row.IsTaxParamIdNull())
                    cmd.Parameters.AddWithValue("p_TaxParamId", row.TaxParamId);
                else
                    cmd.Parameters.AddWithValue("p_TaxParamId", DBNull.Value);

                if (!row.IsAmountNull())
                    cmd.Parameters.AddWithValue("p_Amount", row.Amount);
                else
                    cmd.Parameters.AddWithValue("p_Amount", DBNull.Value);

                cmd.Parameters.AddWithValue("p_Create_User", row.Create_User);
                cmd.Parameters.AddWithValue("p_Create_Date", row.Create_Date);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PAST_EMP_TAXABLE_INCOME_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        //private DataSet _getTaxParamIdByFiscalYear(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();

        //    DBConnectionDS connDS = new DBConnectionDS();
        //    DataLongDS longDS = new DataLongDS();

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //    connDS.AcceptChanges();

        //    OleDbCommand cmd = new OleDbCommand();

        //    DataStringDS stringDS = new DataStringDS();
        //    stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        //    stringDS.AcceptChanges();

        //    string fiscalYear = stringDS.DataStrings[0].StringValue;

        //    cmd = DBCommandProvider.GetDBCommand("GetTaxParamIdByFiscalYear");
        //    cmd.Parameters["FISCALYEAR"].Value = fiscalYear.ToString().Trim();

        //    if (cmd == null) return UtilDL.GetCommandNotFound();

        //    OleDbDataAdapter adapter = new OleDbDataAdapter();
        //    adapter.SelectCommand = cmd;

        //    IncomeTaxParamDS itParamDS = new IncomeTaxParamDS();
        //    bool bError = false;
        //    int nRowAffected = -1;
        //    nRowAffected = ADOController.Instance.Fill(adapter, itParamDS, itParamDS.IncomeTaxParameters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        //    if (bError)
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorInfo1 = ActionID.NA_ACTION_GET_TAX_PARAM_ID_BY_FISCAL_YEAR.ToString();
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }
        //    itParamDS.AcceptChanges();

        //    DataSet returnDS = new DataSet();
        //    errDS.Clear();
        //    errDS.AcceptChanges();
        //    returnDS.Merge(itParamDS);
        //    returnDS.Merge(errDS);
        //    returnDS.AcceptChanges();

        //    return returnDS;
        //}
        private DataSet _getAllowDeduct(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            longDS.Merge(inputDS.Tables[longDS.DataLongs.TableName], false, MissingSchemaAction.Error);
            longDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAD");

            cmd.Parameters["EmployeeId"].Value = longDS.DataLongs[0].LongValue;
            cmd.Parameters["TaxparamId"].Value = longDS.DataLongs[1].LongValue;
            cmd.Parameters["Type"].Value = longDS.DataLongs[2].LongValue;




            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AllowDeductDS allowDS = new AllowDeductDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.AllowDeducts.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            allowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(allowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        #endregion
        //GetAllowDeductList_ALL
        private DataSet _getAllowDeductListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            longDS.Merge(inputDS.Tables[longDS.DataLongs.TableName], false, MissingSchemaAction.Error);
            longDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductList_ALL");



            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AllowDeductDS allowDS = new AllowDeductDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.AllowDeducts.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCT_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            allowDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(allowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getAllowDeductListWithLoan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductListWithLoan");

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AllowDeductDS allowDS = new AllowDeductDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, allowDS, allowDS.AllowDeducts.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOWDEDUCTLISTWITH_LOAN.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            allowDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(allowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getSalaryOutside_NOP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();



            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalaryOutsideNatureOfPayment");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AllowDeductDS allowDeductDS = new AllowDeductDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              allowDeductDS, allowDeductDS.SalaryOutsideNatureofPayment.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_SALARY_OUTSIDE_NOP_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            allowDeductDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(allowDeductDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _getSalaryOutside_MOP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();



            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalaryOutsideModeOfPayment");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AllowDeductDS allowDeductDS = new AllowDeductDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              allowDeductDS, allowDeductDS.SalaryOutsideModeofPayment.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_SALARY_OUTSIDE_MOP_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            allowDeductDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(allowDeductDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _updateAllowDeductOrder(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AllowDeductDS allowDeductDS = new AllowDeductDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            DataLongDS longDS = new DataLongDS();

            longDS.Merge(inputDS.Tables[longDS.DataLongs.TableName], false, MissingSchemaAction.Error);
            longDS.AcceptChanges();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //#region Update Default Selection to Zero if New Selection found
            //OleDbCommand cmdAllowDeductOrderNull = new OleDbCommand();
            //cmdAllowDeductOrderNull.CommandText = "PRO_SET_ALLOWDEDUCT_ORDER_NULL";
            //cmdAllowDeductOrderNull.CommandType = CommandType.StoredProcedure;

            //cmdAllowDeductOrderNull.Parameters.AddWithValue("P_ALLOWTYPE", Convert.ToInt32(longDS.DataLongs[0].LongValue));

            //bool aError = false;
            //int aRowAffected = -1;
            //aRowAffected = ADOController.Instance.ExecuteNonQuery(cmdAllowDeductOrderNull, connDS.DBConnections[0].ConnectionID, ref aError);
            //cmdAllowDeductOrderNull.Dispose();
            //if (aError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.ACTION_SET_ALLOW_DEDUCT_ORDER.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}
            //#endregion


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SET_ALLOW_DEDUCT_ORDER";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            allowDeductDS.Merge(inputDS.Tables[allowDeductDS.AllowDeducts.TableName], false, MissingSchemaAction.Error);
            allowDeductDS.AcceptChanges();

            foreach (AllowDeductDS.AllowDeduct allow in allowDeductDS.AllowDeducts)
            {
                cmd.Parameters.AddWithValue("P_ALLOWDEDUCTCODE", allow.Code);


                if (!allow.IsSorting_OrderNull())
                    cmd.Parameters.AddWithValue("P_ORDER", Convert.ToInt32(allow.Sorting_Order));
                else cmd.Parameters.AddWithValue("P_ORDER", DBNull.Value);

                //cmd.Parameters.AddWithValue("P_ALLOWTYPE", longDS.DataLongs[0].LongValue);
                cmd.Parameters.AddWithValue("P_ALLOWTYPE", Convert.ToInt32(allow.Type));

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SET_ALLOW_DEDUCT_ORDER.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(allow.Code + " - " + allow.Name);
                else messageDS.ErrorMsg.AddErrorMsgRow(allow.Code + " - " + allow.Name);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

    }
}
