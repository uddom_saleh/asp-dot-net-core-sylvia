using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
  /// <summary>
  /// Summary description for LoanTypeDL.
  /// </summary>
  public class LoanTypeDL
  {
    public LoanTypeDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }
    
    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {
        case ActionID.ACTION_LOANTYPE_ADD:
          return _createLoanType( inputDS );
      case ActionID.NA_ACTION_Q_LOANTYPE_ALL:
          return _getLoanTypeList(inputDS);
        case ActionID.ACTION_LOANTYPE_DEL:
          return this._deleteLoanType( inputDS );
        case ActionID.ACTION_LOANTYPE_UPD:
          return this._updateLoanType( inputDS );
        case ActionID.ACTION_DOES_LOANTYPE_EXIST:
          return this._doesLoanTypeExist( inputDS );
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
    private DataSet _createLoanType(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      LoanTypeDS loanTypeDS = new LoanTypeDS();
      DBConnectionDS connDS = new  DBConnectionDS();

      

      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      
      //create command
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateLoanType");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      
      loanTypeDS.Merge( inputDS.Tables[ loanTypeDS.LoanTypes.TableName ], false, MissingSchemaAction.Error );
      loanTypeDS.AcceptChanges();
      foreach(LoanTypeDS.LoanType loanType in loanTypeDS.LoanTypes)
      {
      
        long genPK = IDGenerator.GetNextGenericPK();
        if(genPK == -1)
        {
          return UtilDL.GetDBOperationFailed();
        }
         
        cmd.Parameters["Id"].Value = (object) genPK;
        if(loanType.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) loanType.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;
        }
        if(loanType.IsNameNull()==false)
        {
          cmd.Parameters["Name"].Value = (object) loanType.Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value = null;
        }
     

        if(loanType.IsInterestRateNull()==false)
        {
          cmd.Parameters["InterestRate"].Value = (object) loanType.InterestRate;
        }
        else
        {
          cmd.Parameters["InterestRate"].Value = null;
        }

        if(loanType.IsNoOfInstallmentNull()==false)
        {
          cmd.Parameters["NoOfInstallment"].Value = (object) loanType.NoOfInstallment ;
        }
        else
        {
          cmd.Parameters["NoOfInstallment"].Value = null;
        }
       
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_LOANTYPE_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _deleteLoanType(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

    
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteLoanType");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      foreach(DataStringDS.DataString sValue in stringDS.DataStrings)
      {
        if (sValue.IsStringValueNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) sValue.StringValue;
        }
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_LOANTYPE_DEL.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }

      return errDS;  // return empty ErrorDS
    }
    private DataSet _getLoanTypeList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataLongDS longDS = new DataLongDS();
      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

   

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanTypeList");
  
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
    

      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      LoanTypePO loanTypePO = new LoanTypePO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter, 
        loanTypePO, loanTypePO.LoanTypes.TableName, 
        connDS.DBConnections[0].ConnectionID, 
        ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_LOANTYPE_ALL.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }

      loanTypePO.AcceptChanges();

      LoanTypeDS loanTypeDS = new LoanTypeDS();
      if ( loanTypePO.LoanTypes.Count > 0 )
      {
       

        foreach ( LoanTypePO.LoanType poLoanType in loanTypePO.LoanTypes)
        {
          LoanTypeDS.LoanType loanType = loanTypeDS.LoanTypes.NewLoanType();
          if(poLoanType.IsCodeNull()==false)
          {
            loanType.Code = poLoanType.Code;
          }
          if(poLoanType.IsNameNull()==false)
          {
            loanType.Name = poLoanType.Name;
          }
          if(poLoanType.IsInterestRateNull()==false)
          {
            loanType.InterestRate = poLoanType.InterestRate;
          }
          if(poLoanType.IsNoOfInstallmentNull()==false)
          {
            loanType.NoOfInstallment = poLoanType.NoOfInstallment;
          }
          
          loanTypeDS.LoanTypes.AddLoanType( loanType );
          loanTypeDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( loanTypeDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
    
      return returnDS;
    
    
    }
  
    private DataSet _updateLoanType(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();
      LoanTypeDS loanTypeDS = new LoanTypeDS();
      DBConnectionDS connDS = new  DBConnectionDS();

      

      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      
      //create command
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateLoanType");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      
      loanTypeDS.Merge( inputDS.Tables[ loanTypeDS.LoanTypes.TableName ], false, MissingSchemaAction.Error );
      loanTypeDS.AcceptChanges();

      foreach(LoanTypeDS.LoanType loanType in loanTypeDS.LoanTypes)
      {
      
        if(loanType.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) loanType.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;
        }
        if(loanType.IsNameNull()==false)
        {
          cmd.Parameters["Name"].Value = (object) loanType.Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value = null;
        }
     

        if(loanType.IsInterestRateNull()==false)
        {
          cmd.Parameters["InterestRate"].Value = (object) loanType.InterestRate;
        }
        else
        {
          cmd.Parameters["InterestRate"].Value = null;
        }

        if(loanType.IsNoOfInstallmentNull()==false)
        {
          cmd.Parameters["NoOfInstallment"].Value = (object) loanType.NoOfInstallment ;
        }
        else
        {
          cmd.Parameters["NoOfInstallment"].Value = null;
        }
       
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_LOANTYPE_UPD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }

    private DataSet _doesLoanTypeExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesLoanTypeExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
        cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    } 

   
   
  
  
  }
}
