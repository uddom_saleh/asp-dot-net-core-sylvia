/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Developer: Km Jarif
 * Comment Date: April 24, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class BatchSalaryProcessDL
    {
        public BatchSalaryProcessDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_BATCH_CHANGED_SP_SAVE:
                    return _SaveChangedSalaryInformation(inputDS);
                case ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE:
                    return _SaveProcessInformation(inputDS);
                case ActionID.ACTION_DOES_SALARY_PROCESS_MONTH_EXIST:
                    return _doesSalaryProcessMonthExist(inputDS);

                //mislbd.Jarif Update Start(10-Apr-2008)
                case ActionID.NA_ACTION_GET_BASIC_SALARY:
                    return _getBasicSalary(inputDS);
                //mislbd.Jarif Update End(10-Apr-2008)

                case ActionID.ACTION_DOES_SALARY_PROCESS_EXIST:
                    return _doesSalaryProcessExist(inputDS);
                case ActionID.ACTION_DOES_MONTH_END_PROCESS_EXIST:
                    return _doesMonthEndProcessExist(inputDS);
                case ActionID.NA_ACTION_BATCH_Q_ALL_SALARY_PROCESS:
                    return _getSalaryProcesList(inputDS);
                case ActionID.NA_ACTION_Q_ALL_EMPLOYEE_HAVE_LOAN:
                    return this._getEmployeeListHaveLoan(inputDS);
                case ActionID.NA_ACTION_BATCH_Q_ALL_PROCESS_DATA:
                    return _getSalaryProcessData(inputDS);
                case ActionID.NA_ACTION_BATCH_PFPARAMETER_GET:
                    return _getPFParameterForThisMonth(inputDS);
                case ActionID.NA_ACTION_BATCH_Q_ALL_ALLOWDEDUCTGRADES:
                    return _getAllowDeductionGrades(inputDS);
                case ActionID.ACTION_BATCH_MONTH_END_PROCESS:
                    return _SaveMonthEndDate(inputDS);
                case ActionID.ACTION_SALARYPROCESS_DEL:
                    return this._deleteSalaryProcess(inputDS);
                case ActionID.NA_ACTION_Q_ALL_EMPLOYEE_ARREAR_INFO:
                    return this._getEmployeeArrearInfo(inputDS);
                case ActionID.NA_ACTION_Q_ALL_EMPLOYEE_ARREAR_INFO_SUMMARY:
                    return this._getEmployeeArrearInfoSummary(inputDS);

                case ActionID.NA_ACTION_BATCH_PREV_MONTH_PROCESS_INFO_GET:
                    return this._getPrevMonthSalaryProcessInfo(inputDS);

                case ActionID.NA_ACTION_GET_SALARY_SALARYDETAIL_LIST:
                    return this._getSalary_SalaryDetailList(inputDS);
                case ActionID.NA_ACTION_GET_SALARY_SALARYDETAIL_LIST_SP:
                    return this._getSalary_SalaryDetailList_SP(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_ARREAR_LIST:
                    return this._getEmployeeArrearList(inputDS);

                case ActionID.NA_ACTION_GET_SALARY_PROCESS_DATA_BY_EMP:
                    return this._getSalaryProcessData_By_Emp(inputDS);

                case ActionID.NA_ACTION_GET_SuspensionDeduction_BySalaryID:
                    return this._getSuspensionDeduction_BySalaryID(inputDS);
                case ActionID.NA_ACTION_GET_DS_Suspension_ForRefund:
                    return this._getDS_Suspension_ForRefund(inputDS);

                case ActionID.NA_ACTION_GET_BASIC_SALARY_SP:
                    return _getBasicSalary_SP(inputDS);
                case ActionID.NA_ACTION_GET_BENEVOLENT_FUND_INFO_SP:
                    return _getBenevolent_Fund_Info_SP(inputDS);

                case ActionID.NA_GET_SALARY_WITHHOLD_INFO:
                    return this._getSalaryWithhold_Info(inputDS);
                case ActionID.ACTION_SALARY_WITHHOLD_INFO_SAVE:
                    return this._saveSalaryWithhold_Info(inputDS);

                case ActionID.NA_ACTION_GET_LAST_SALARY_PROCESS_DATE:
                    return this._getLastSalaryProcessDate(inputDS);

                case ActionID.ACTION_EMP_SALARY_ARREAR_ADD:
                    return this._createEmployee_Salary_Arrear(inputDS);
                case ActionID.ACTION_EMP_SALARY_ARREAR_DEL:
                    return this._deleteEmp_Salary_Arrear(inputDS);

                case ActionID.NA_ACTION_CHECK_IS_SALARY_REPROCESS_PENDING:
                    return this._IsReprocessPending(inputDS);
                case ActionID.NA_ACTION_CHECK_IS_SALARY_PROCESS_PENDING:
                    return this._IsPayProcessPending(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_WITHOUT_PAY_SP:
                    return this._loadLeaveWithoutPay(inputDS);
                case ActionID.NA_ACTION_EMPLOYEE_ARREAR_INFO_REPORT:
                    return this._getEmployeeArrearInfo_Report(inputDS);
                case ActionID.NA_ACTION_GET_LWP_DEDUCTION_SP:
                    return this._loadPrevLWPSalary(inputDS);
                case ActionID.NA_ACTION_GET_RETIREMENT_SALARY_BENEFIT_SP:
                    return this._loadRetirementSalaryBenefit(inputDS);

                case ActionID.NA_ACTION_GET_PREV_SALARYDETAIL_SP:
                    return this._getPrevSalaryDetail_SP(inputDS);

                case ActionID.NA_ACTION_GET_LWP_DEDUCTION_SM:
                    return this._loadLWPSalary_SM(inputDS);
                case ActionID.NA_ACTION_SALARY_PROCESS_DATE:
                    return this._getSalaryProcessDate(inputDS);

                case ActionID.NA_ACTION_GET_LWP_ADJUSTMENT_SP:
                    return this._loadLWPAdjustment(inputDS);
                case ActionID.ACTION_SALARY_BY_EMPID_DEL:
                    return this._deleteSalaryProcess_ByEmployeeID(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_LOAN_SP:
                    return this._getEmpLoan_SP(inputDS);
                case ActionID.NA_ACTION_GET_PAID_BONUS_SP:
                    return this._get_Paid_Bonus_SP(inputDS);

                case ActionID.NA_ACTION_BATCH_Q_ALL_OUTSIDE_SALARY_PROCESS:
                    return this._getOutsideSalaryProcessList(inputDS);
                case ActionID.NA_ACTION_GET_OUTSIDE_SALARY_PROCESS_DATA_BY_EMP:
                    return this._getOutsideSalaryProcessData_By_Emp(inputDS);

                case ActionID.ACTION_BATCH_CHANGED_OUTSIDE_SP_SAVE:
                    return this._saveOutsideChangedSalary(inputDS);

                case ActionID.ACTION_OUTSIDE_SALARY_BY_EMPID_DEL:
                    return this._deleteOutside_SalaryProcess_ByEmployee(inputDS);

                case ActionID.ACTION_OUTSIDE_PROCESS_INFO_SAVE:
                    return _SaveOutSideProcessInfo(inputDS);


                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }
        private DataSet _getEmployeeArrearInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetArearInfo_New");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["CurrentMonth"].Value = dateDS.DataDates[0].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //EmployeeArrearInfoPO InfoPO = new EmployeeArrearInfoPO();
            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, InfoPO, InfoPO.EmployeeArrearInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_EMPLOYEE_ARREAR_INFO.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //InfoPO.AcceptChanges();

            //EmployeeArrearInfoDS InfoDS = new EmployeeArrearInfoDS();
            //if (InfoPO.EmployeeArrearInfo.Count > 0)
            //{
            //    foreach (EmployeeArrearInfoPO.EmployeeArrearInfoRow poInfo in InfoPO.EmployeeArrearInfo)
            //    {
            //        EmployeeArrearInfoDS.EmployeeArrearInfoRow info = InfoDS.EmployeeArrearInfo.NewEmployeeArrearInfoRow();

            //        if (poInfo.IsEmployeeCodeNull() == false)
            //        {
            //            info.EmployeeCode = poInfo.EmployeeCode;
            //        }

            //        if (poInfo.IsCurrentmonthdateNull() == false)
            //        {
            //            info.CurrentMonth = poInfo.Currentmonthdate;
            //        }
            //        if (poInfo.IsSalaryheaddescriptionNull() == false)
            //        {
            //            info.AllowDeductName = poInfo.Salaryheaddescription;
            //        }
            //        if (poInfo.IsSalaryheadcodeNull() == false)
            //        {
            //            info.AllowDeductCode = poInfo.Salaryheadcode;
            //        }
            //        if (poInfo.IsMonthyeardateNull() == false)
            //        {
            //            info.MonthDate = poInfo.Monthyeardate;
            //        }
            //        if (poInfo.IsAmountNull() == false)
            //        {
            //            info.Amount = poInfo.Amount;
            //        }
            //        if (poInfo.IsHeadTypeNull() == false)
            //        {
            //            info.HeadType = poInfo.HeadType;
            //        }
            //        if (poInfo.IsPrevAmountNull() == false)
            //        {
            //            info.PrevAmount = poInfo.PrevAmount;
            //        }
            //        InfoDS.EmployeeArrearInfo.AddEmployeeArrearInfoRow(info);
            //        InfoDS.AcceptChanges();
            //    }
            //}
            #endregion

            EmployeeArrearInfoDS InfoDS = new EmployeeArrearInfoDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, InfoDS, InfoDS.EmployeeArrearInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_EMPLOYEE_ARREAR_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            InfoDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(InfoDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _doesProcessExistForMonth(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesBankExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAllowDeductionGrades(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductGradesList");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(26 Sep 11)...
            #region Old...
            //BatchAllowDeductGradesPO parameterPO = new BatchAllowDeductGradesPO();
            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, parameterPO, parameterPO.AlDeGrs.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_Q_ALL_ALLOWDEDUCTGRADES.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //parameterPO.AcceptChanges();
            //AllowDeductGradesDS parameterDS = new AllowDeductGradesDS();
            //if (parameterPO.AlDeGrs.Count > 0)
            //{
            //    foreach (BatchAllowDeductGradesPO.AlDeGr poParameter in parameterPO.AlDeGrs)
            //    {
            //        AllowDeductGradesDS.AlDeGr parameter = parameterDS.AlDeGrs.NewAlDeGr();

            //        if (poParameter.IsADCodeNull() == false)
            //        {
            //            parameter.ADPCode = poParameter.ADCode;
            //        }

            //        if (poParameter.IsGradeCodeNull() == false)
            //        {
            //            parameter.GradeCode = poParameter.GradeCode;
            //        }
            //        parameterDS.AlDeGrs.AddAlDeGr(parameter);
            //        parameterDS.AcceptChanges();
            //    }
            //}
            #endregion

            AllowDeductGradesDS parameterDS = new AllowDeductGradesDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, parameterDS, parameterDS.AlDeGrs.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_Q_ALL_ALLOWDEDUCTGRADES.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            parameterDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(parameterDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _getPFParameterForThisMonth(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPFParameterForThisMonth");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["Date"].Value = dateDS.DataDates[0].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //PFParameterPO parameterPO = new PFParameterPO();
            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, parameterPO, parameterPO.PFParameters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_PFPARAMETER_GET.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //parameterPO.AcceptChanges();
            //PFParameterDS parameterDS = new PFParameterDS();
            //if (parameterPO.PFParameters.Count > 0)
            //{
            //    foreach (PFParameterPO.PFParameter poParameter in parameterPO.PFParameters)
            //    {
            //        PFParameterDS.PFParameter parameter = parameterDS.PFParameters.NewPFParameter();

            //        if (poParameter.IsEffectDateNull() == false)
            //        {
            //            parameter.EffectDate = poParameter.EffectDate;
            //        }

            //        if (poParameter.IsSelfContributionNull() == false)
            //        {
            //            parameter.SelfContribution = poParameter.SelfContribution;
            //        }
            //        if (poParameter.IsCompanyContributionNull() == false)
            //        {
            //            parameter.CompanyContribution = poParameter.CompanyContribution;
            //        }
            //        parameterDS.PFParameters.AddPFParameter(parameter);
            //        parameterDS.AcceptChanges();
            //    }
            //}
            #endregion

            PFParameterDS parameterDS = new PFParameterDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, parameterDS, parameterDS.PFParameters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_PFPARAMETER_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            parameterDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(parameterDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _getEmployeeListHaveLoan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeHaveLoan_New");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["Date"].Value = dateDS.DataDates[0].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //BatchLoanProcessPO loanProcessPO = new BatchLoanProcessPO();
            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, loanProcessPO, loanProcessPO.MonthlyLoanInfos.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_EMPLOYEE_HAVE_LOAN.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //loanProcessPO.AcceptChanges();
            //BatchLoanProcessDS loanProcessDS = new BatchLoanProcessDS();
            //if (loanProcessPO.MonthlyLoanInfos.Count > 0)
            //{
            //    foreach (BatchLoanProcessPO.MonthlyLoanInfo poLoanProcess in loanProcessPO.MonthlyLoanInfos)
            //    {
            //        BatchLoanProcessDS.MonthlyLoanInfo loanProcess = loanProcessDS.MonthlyLoanInfos.NewMonthlyLoanInfo();

            //        if (poLoanProcess.IsAmountNull() == false)
            //        {
            //            loanProcess.Amount = poLoanProcess.Amount;
            //        }
            //        if (poLoanProcess.IsInterestNull() == false)
            //        {
            //            loanProcess.Interest = poLoanProcess.Interest;
            //        }

            //        if (poLoanProcess.IsDueInstallmentDateNull() == false)
            //        {
            //            loanProcess.DueInstallmentDate = poLoanProcess.DueInstallmentDate;
            //        }
            //        if (poLoanProcess.IsEmployeeCodeNull() == false)
            //        {
            //            loanProcess.EmployeeCode = poLoanProcess.EmployeeCode;
            //        }
            //        if (poLoanProcess.IsEmployeenameNull() == false)
            //        {
            //            loanProcess.Employeename = poLoanProcess.Employeename;
            //        }
            //        if (poLoanProcess.IsLoanAmountNull() == false)
            //        {
            //            loanProcess.LoanAmount = poLoanProcess.LoanAmount;
            //        }
            //        if (poLoanProcess.IsLoanCodeNull() == false)
            //        {
            //            loanProcess.LoanCode = poLoanProcess.LoanCode;
            //        }
            //        if (poLoanProcess.IsLoanNoNull() == false)
            //        {
            //            loanProcess.LoanNo = poLoanProcess.LoanNo;
            //        }
            //        if (poLoanProcess.IsLoanTypeCodeNull() == false)
            //        {
            //            loanProcess.LoanTypeCode = poLoanProcess.LoanTypeCode;
            //        }
            //        if (poLoanProcess.IsScheduleNoNull() == false)
            //        {
            //            loanProcess.ScheduleNo = poLoanProcess.ScheduleNo;
            //        }
            //        loanProcessDS.MonthlyLoanInfos.AddMonthlyLoanInfo(loanProcess);
            //        loanProcessDS.AcceptChanges();
            //    }
            //}
            #endregion

            BatchLoanProcessDS loanProcessDS = new BatchLoanProcessDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, loanProcessDS, loanProcessDS.MonthlyLoanInfos.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_EMPLOYEE_HAVE_LOAN.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            loanProcessDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(loanProcessDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _getEmpLoan_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();            
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmpLoan_SP");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["Date"].Value = dateDS.DataDates[0].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BatchLoanProcessDS loanProcessDS = new BatchLoanProcessDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, loanProcessDS, loanProcessDS.MonthlyLoanInfos.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LOAN_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            loanProcessDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(loanProcessDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _SaveMonthEndDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            SalaryProcessDS salaryProcessDS = new SalaryProcessDS();
            salaryProcessDS.Merge(inputDS.Tables[salaryProcessDS.SalaryProcesses.TableName], false, MissingSchemaAction.Error);
            salaryProcessDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("SetMonthEndDate");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            foreach (SalaryProcessDS.SalaryProcess detail in salaryProcessDS.SalaryProcesses)
            {
                if (detail.IsMonthEndDateNull() == false)
                {
                    cmd.Parameters["MonthEndDate"].Value = detail.MonthEndDate;
                }
                else
                {
                    cmd.Parameters["MonthEndDate"].Value = null;
                }
                if (detail.IsMonthYearDateNull() == false)
                {
                    cmd.Parameters["MonthYearDate"].Value = detail.MonthYearDate;
                }
                else
                {
                    cmd.Parameters["MonthYearDate"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BATCH_MONTH_END_PROCESS.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;





        }

        private DataSet _SaveChangedSalaryInformation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SalaryProcessDS processDS = new SalaryProcessDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateSalaryDetail");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            processDS.Merge(inputDS.Tables[processDS.Salaries.TableName], false, MissingSchemaAction.Error);
            processDS.Merge(inputDS.Tables[processDS.SalaryDetails.TableName], false, MissingSchemaAction.Error);
            processDS.Merge(inputDS.Tables[processDS.SalaryDeductSuspension.TableName], false, MissingSchemaAction.Error);
            processDS.Merge(inputDS.Tables[processDS.LeaveWithoutPay.TableName], false, MissingSchemaAction.Error);
            processDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            TaxHistoryDS historyDS = new TaxHistoryDS();
            historyDS.Merge(inputDS.Tables[historyDS.TaxHistories.TableName], false, MissingSchemaAction.Error);
            historyDS.AcceptChanges();

            EmployeeArrearInfoDS empArrearDS = new EmployeeArrearInfoDS();
            empArrearDS.Merge(inputDS.Tables[empArrearDS.EmployeeArrearInfo.TableName], false, MissingSchemaAction.Error);
            empArrearDS.AcceptChanges();

            DateTime date = new DateTime(2000, 1, 1);
            string sEmployeeCode = "";

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                sEmployeeCode = stringDS.DataStrings[0].StringValue;
            }
            if (dateDS.DataDates[0].IsDateValueNull() == false)
            {
                date = dateDS.DataDates[0].DateValue;
            }

            #region For Leave Without Pay
            OleDbCommand cmdLWP = new OleDbCommand();
            cmdLWP.CommandText = "PRO_LEAVE_WITHOUT_PAY_UPD";
            cmdLWP.CommandType = CommandType.StoredProcedure;

            foreach (SalaryProcessDS.LeaveWithoutPayRow lwp in processDS.LeaveWithoutPay.Rows)
            {
                cmdLWP.Parameters.AddWithValue("p_ChangedAmount", lwp.ChangedAmount);
                cmdLWP.Parameters.AddWithValue("p_SalaryID", lwp.SalaryID);
                cmdLWP.Parameters.AddWithValue("p_MonthYearDate", lwp.MonthYearDate);
                cmdLWP.Parameters.AddWithValue("p_HeadCode", lwp.HeadCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLWP, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdLWP.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BATCH_CHANGED_SP_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Salary Detail + Excluded Loans
            foreach (SalaryProcessDS.SalaryDetail detail in processDS.SalaryDetails)
            {
                bool bError = false;
                int nRowAffected = -1;

                if (!detail.IsSalaryExcludedLoanNull() && detail.SalaryExcludedLoan)
                {
                    #region Update Salary Excluded Loan
                    OleDbCommand cmdExclLoan = new OleDbCommand();
                    cmdExclLoan.CommandText = "PRO_SALARY_EXCLUDEDLOAN_UPD";
                    cmdExclLoan.CommandType = CommandType.StoredProcedure;

                    if (!detail.IsChangeAmountNull()) cmdExclLoan.Parameters.AddWithValue("p_ChangedAmount", detail.ChangeAmount);
                    else cmdExclLoan.Parameters.AddWithValue("p_ChangedAmount", DBNull.Value);

                    if (!detail.IsLoanIssueIDNull()) cmdExclLoan.Parameters.AddWithValue("p_LoanIssueID", detail.LoanIssueID);
                    else cmdExclLoan.Parameters.AddWithValue("p_LoanIssueID", DBNull.Value);

                    cmdExclLoan.Parameters.AddWithValue("p_EmployeeCode", sEmployeeCode);
                    cmdExclLoan.Parameters.AddWithValue("p_Date", date);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdExclLoan, connDS.DBConnections[0].ConnectionID, ref bError);
                    #endregion
                }
                else
                {
                    #region Update Salary Detail
                    if (!detail.IsChangeAmountNull()) cmd.Parameters["ChangedAmount"].Value = detail.ChangeAmount;
                    else cmd.Parameters["ChangedAmount"].Value = DBNull.Value;

                    if (!detail.IsDescriptionNull()) cmd.Parameters["Description"].Value = detail.Description;
                    else cmd.Parameters["Description"].Value = DBNull.Value;

                    cmd.Parameters["EmployeeCode"].Value = sEmployeeCode;
                    cmd.Parameters["Date"].Value = date;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    #endregion
                }
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BATCH_CHANGED_SP_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region For Suspension...
            cmd.Dispose();
            cmd = DBCommandProvider.GetDBCommand("UpdateSDSuspension");
            foreach (SalaryProcessDS.SalaryDeductSuspensionRow sdsRow in processDS.SalaryDeductSuspension.Rows)
            {
                cmd.Parameters["ChangedAmount"].Value = sdsRow.ChangedAmount;
                cmd.Parameters["SDS_ID"].Value = sdsRow.SDS_ID;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BATCH_CHANGED_SP_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Tax History
            if (historyDS.TaxHistories.Count == 1)
            {
                cmd.Dispose();
                cmd = DBCommandProvider.GetDBCommand("UpdateTaxHistory");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (TaxHistoryDS.TaxHistory history in historyDS.TaxHistories)
                {
                    cmd.Parameters["IdealAmount"].Value = history.IdealAmount;
                    cmd.Parameters["CalculatedRegularTax"].Value = history.CalculatedRegularTax;
                    cmd.Parameters["CalculatedExtraTax"].Value = history.CalculatedExtraTax;//
                    cmd.Parameters["EmployeeCode"].Value = history.EmployeeCode;
                    cmd.Parameters["MonthYear"].Value = history.MonthYear;
                    cmd.Parameters["TaxableIncome"].Value = history.TaxableIncome;
                    cmd.Parameters["RegularTax"].Value = history.RegularTax;
                    cmd.Parameters["ExtraTax"].Value = history.ExtraTax;
                    cmd.Parameters["ExtraTaxableIncome"].Value = history.ExtraTaxbleIncome;
                    cmd.Parameters["BasicSalary"].Value = history.BasicSalary;
                    cmd.Parameters["Medical"].Value = history.Medical;
                    cmd.Parameters["HouseRent"].Value = history.HouseRent;
                    cmd.Parameters["CompanyPf"].Value = history.CompanyPf;
                    if (history.IsConveyanceNull() == false) cmd.Parameters["Conveyance"].Value = history.Conveyance;
                    else cmd.Parameters["Conveyance"].Value = 0;
                    cmd.Parameters["ConveyanceFacility"].Value = history.ConveyanceFacility;
                    if (!history.IsAnnualTaxableIncomeNull()) cmd.Parameters["AnnualTaxableIncome"].Value = history.AnnualTaxableIncome;
                    else cmd.Parameters["AnnualTaxableIncome"].Value = 0;
                    if (!history.IsLastCal_TaxPercentNull()) cmd.Parameters["LastCal_TaxPercent"].Value = history.LastCal_TaxPercent;
                    else cmd.Parameters["LastCal_TaxPercent"].Value = 0;

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BATCH_CHANGED_SP_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion

            #region Update Arrear information
            if (empArrearDS.EmployeeArrearInfo.Count > 0)
            {
                cmd.Dispose();
                cmd = DBCommandProvider.GetDBCommand("UpdateArrear");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
                foreach (EmployeeArrearInfoDS.EmployeeArrearInfoRow arrear in empArrearDS.EmployeeArrearInfo)
                {
                    cmd.Parameters["Amount"].Value = arrear.Amount;
                    cmd.Parameters["EmployeeCode"].Value = arrear.EmployeeCode;
                    cmd.Parameters["Date"].Value = arrear.MonthDate;
                    cmd.Parameters["HeadCode"].Value = arrear.AllowDeductCode;

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BATCH_CHANGED_SP_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion

            #region Salary Update By Last User.
            cmd.Dispose();
            cmd = DBCommandProvider.GetDBCommand("SalaryUpdateByLastUser");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            if (!stringDS.DataStrings[1].IsStringValueNull())
            {
                cmd.Parameters["LoginID"].Value = stringDS.DataStrings[1].StringValue;
            }
            else cmd.Parameters["LoginID"].Value = null;
            if (stringDS.DataStrings.Rows.Count > 2)
            {
                cmd.Parameters["Remarks"].Value = (!stringDS.DataStrings[2].IsStringValueNull()) ? stringDS.DataStrings[2].StringValue : null;
            }
            else cmd.Parameters["Remarks"].Value = null;
            cmd.Parameters["EmployeeCode"].Value = sEmployeeCode;
            cmd.Parameters["Date"].Value = date;

            bool bError_SU = false;
            int nRowAffected_SU = -1;
            nRowAffected_SU = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError_SU);
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _SaveProcessInformation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SalaryProcessDS processDS = new SalaryProcessDS();
            DBConnectionDS connDS = new DBConnectionDS();

            Int32 EmployeeType = 0, PayrollSiteID = 0;
            bool IsYearEnd = false;

            processDS.Merge(inputDS.Tables[processDS.SalaryProcesses.TableName], false, MissingSchemaAction.Error);
            processDS.Merge(inputDS.Tables[processDS.Salaries.TableName], false, MissingSchemaAction.Error);
            processDS.Merge(inputDS.Tables[processDS.SalaryDetails.TableName], false, MissingSchemaAction.Error);
            processDS.Merge(inputDS.Tables[processDS.SalaryDeductSuspension.TableName], false, MissingSchemaAction.Error);
            processDS.Merge(inputDS.Tables[processDS.LeaveWithoutPay.TableName], false, MissingSchemaAction.Error);
            processDS.Merge(inputDS.Tables[processDS.SalaryDetail_Arrear.TableName], false, MissingSchemaAction.Error);
            processDS.AcceptChanges();

            TaxHistoryDS historyDS = new TaxHistoryDS();
            historyDS.Merge(inputDS.Tables[historyDS.TaxHistories.TableName], false, MissingSchemaAction.Error);
            historyDS.AcceptChanges();

            EmployeeArrearInfoDS arrearInfoDS = new EmployeeArrearInfoDS();
            arrearInfoDS.Merge(inputDS.Tables[arrearInfoDS.EmployeeArrearInfo.TableName], false, MissingSchemaAction.Error);
            arrearInfoDS.AcceptChanges();

            GratuityProvisionDS gProvisionDS = new GratuityProvisionDS();
            gProvisionDS.Merge(inputDS.Tables[gProvisionDS.GratuityMonthlyProvision.TableName], false, MissingSchemaAction.Error);
            gProvisionDS.AcceptChanges();

            SalaryProcessDS.SalaryDetail_ArrearRow[] foundSDArrear;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("SaveSalaryProcess");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbCommand cmdUSPT = DBCommandProvider.GetDBCommand("UpdateSalaryProcessType");
            if (cmdUSPT == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbCommand cmdSalary = DBCommandProvider.GetDBCommand("SaveSalary_New");
            if (cmdSalary == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbCommand cmdDetails = DBCommandProvider.GetDBCommand("SaveSalaryDetali");
            if (cmdDetails == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            OleDbCommand cmdArrear = DBCommandProvider.GetDBCommand("UpdateArearStatus_SP");
            if (cmdArrear == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbCommand cmdGratuityProvision = DBCommandProvider.GetDBCommand("CreateMonthlyGratuityProvision");
            if (cmdGratuityProvision == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbCommand cmdSuspension = DBCommandProvider.GetDBCommand("SaveSalarySuspension");
            if (cmdSuspension == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbCommand cmdLeaveEncashment = DBCommandProvider.GetDBCommand("UpdateLeaveEncashment");
            if (cmdLeaveEncashment == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbCommand cmdDetails_AD = DBCommandProvider.GetDBCommand("SaveSalaryDetali_ArrearHead");
            if (cmdDetails_AD == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbCommand cmdEmployeeLastSalaryIdUpdate = DBCommandProvider.GetDBCommand("UpdateEmployeeLastSalaryId");
            if (cmdEmployeeLastSalaryIdUpdate == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            long nSalaryId = 0;
            foreach (SalaryProcessDS.SalaryProcess process in processDS.SalaryProcesses)
            {
                bool bError = false;
                int nRowAffected = -1;

                #region Save salary process related information...
                EmployeeType = (Int32)process.EmployeeType;
                PayrollSiteID = process.PayrollsiteID;

                long nSalaryProcessId = GetSalaryProcessID(connDS, EmployeeType, PayrollSiteID, process.MonthYearDate);
                if (nSalaryProcessId > 0)
                {
                    #region for Process Type Update...

                    if (process.DiscontinuedEmpOnly) cmdUSPT.Parameters["ProcessType"].Value = 1;
                    else cmdUSPT.Parameters["ProcessType"].Value = 0;

                    cmdUSPT.Parameters["SalaryProcessID"].Value = nSalaryProcessId;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUSPT, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion
                }
                else
                {
                    #region For New Process...
                    nSalaryProcessId = IDGenerator.GetNextGenericPK();
                    if (nSalaryProcessId == -1) return UtilDL.GetDBOperationFailed();


                    cmd.Parameters["SalaryProcessId"].Value = nSalaryProcessId;
                    cmd.Parameters["MonthYearDate"].Value = process.MonthYearDate;
                    cmd.Parameters["ProcessDate"].Value = process.ProcessDate;

                    if (process.IsMonthEndDateNull() == false)
                    {
                        cmd.Parameters["MonthEndDate"].Value = process.MonthEndDate;
                    }
                    else
                    {
                        cmd.Parameters["MonthEndDate"].Value = System.DBNull.Value;
                    }
                    if (process.IsWorkDaysNull() == false)
                    {
                        cmd.Parameters["WorksDays"].Value = process.WorkDays;
                    }
                    else
                    {
                        cmd.Parameters["WorksDays"].Value = null;
                    }
                    cmd.Parameters["EmployeeType"].Value = process.EmployeeType;
                    cmd.Parameters["PayrollSiteCode"].Value = process.PayrollSiteCode;

                    if (!process.IsLoginIDNull()) cmd.Parameters["LoginID"].Value = process.LoginID;
                    else cmd.Parameters["LoginID"].Value = null;

                    if (process.DiscontinuedEmpOnly) cmd.Parameters["ProcessType"].Value = 1;
                    else cmd.Parameters["ProcessType"].Value = 0;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion
                }
                #endregion

                #region Save salary related information...
                foreach (SalaryProcessDS.Salary salary in process.GetSalaries())
                {
                    #region Save Salary Information...
                    nSalaryId = IDGenerator.GetNextGenericPK();
                    if (nSalaryId == -1) return UtilDL.GetDBOperationFailed();

                    cmdSalary.Parameters["SalaryId"].Value = nSalaryId;
                    cmdSalary.Parameters["SalaryProcessId"].Value = nSalaryProcessId;
                    cmdSalary.Parameters["MonthYearDate"].Value = salary.MonthYearDate;
                    cmdSalary.Parameters["EmployeeID"].Value = salary.EmployeeID;
                    cmdSalary.Parameters["DepartmentID"].Value = salary.DepartmentID;
                    cmdSalary.Parameters["DesignationID"].Value = salary.DesignationID;
                    cmdSalary.Parameters["LocationID"].Value = salary.LocationID;
                    cmdSalary.Parameters["GradeID"].Value = salary.GradeID;
                    if (!salary.IsPrevMonthBasicNull()) cmdSalary.Parameters["PrevMonthBasic"].Value = salary.PrevMonthBasic;
                    else cmdSalary.Parameters["PrevMonthBasic"].Value = 0;
                    cmdSalary.Parameters["ThisMonthBasic"].Value = salary.ThisMonthBasic;
                    if (!salary.IsIsFinalizedNull()) cmdSalary.Parameters["IsFinalized"].Value = salary.IsFinalized;
                    else cmdSalary.Parameters["IsFinalized"].Value = 0;
                    if (!salary.IsCommentsNull()) cmdSalary.Parameters["Comments"].Value = salary.Comments;
                    else cmdSalary.Parameters["Comments"].Value = "Comments";
                    if (!salary.IsBankAccountNoNull()) cmdSalary.Parameters["BankAccountNo"].Value = salary.BankAccountNo;
                    else cmdSalary.Parameters["BankAccountNo"].Value = System.DBNull.Value;
                    cmdSalary.Parameters["BranchID"].Value = salary.BranchID;
                    if (!salary.IsArrearFromNull()) cmdSalary.Parameters["ArrearFrom"].Value = salary.ArrearFrom;
                    else cmdSalary.Parameters["ArrearFrom"].Value = System.DBNull.Value;
                    if (!salary.IsEmployeeTypeNull()) cmdSalary.Parameters["EmployeeType"].Value = salary.EmployeeType;
                    else cmdSalary.Parameters["EmployeeType"].Value = null;
                    if (!salary.IsShiftCodeNull()) cmdSalary.Parameters["ShiftId"].Value = salary.ShiftCode;
                    else cmdSalary.Parameters["ShiftId"].Value = 01;
                    cmdSalary.Parameters["CostCenterID"].Value = salary.CostcenterID;
                    cmdSalary.Parameters["CompanyID"].Value = salary.CompanyID;
                    cmdSalary.Parameters["IsWithCar"].Value = salary.IsWithCar;
                    if (!salary.IsSiteIDNull()) cmdSalary.Parameters["SiteID"].Value = salary.SiteID;
                    else cmdSalary.Parameters["SiteID"].Value = DBNull.Value;
                    cmdSalary.Parameters["SalaryProcessDate"].Value = salary.SalaryProcessDate;
                    cmdSalary.Parameters["Last_EmployeeJobHistID"].Value = salary.Last_EmployeeJobHistID;
                    cmdSalary.Parameters["SA_PercentOfBasic_Gross"].Value = salary.SA_PercentOfBasic_Gross;

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdSalary, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion

                    #region Save in SalaryDetail for Previous Arrear Head...

                    foundSDArrear = (SalaryProcessDS.SalaryDetail_ArrearRow[])processDS.SalaryDetail_Arrear.Select("Salary_Id = " + salary.Salary_Id);
                    foreach (SalaryProcessDS.SalaryDetail_ArrearRow SD_Arrear in foundSDArrear)
                    {
                        cmdDetails_AD.Parameters["SalaryId"].Value = SD_Arrear.SalaryID;
                        cmdDetails_AD.Parameters["HeadId"].Value = SD_Arrear.HeadID;
                        cmdDetails_AD.Parameters["HeadCode"].Value = SD_Arrear.HeadCode;
                        cmdDetails_AD.Parameters["HeadType"].Value = SD_Arrear.HeadType;
                        cmdDetails_AD.Parameters["Description"].Value = SD_Arrear.Description;
                        cmdDetails_AD.Parameters["Position"].Value = SD_Arrear.Position;
                        cmdDetails_AD.Parameters["CalculatedAmount"].Value = SD_Arrear.Amount;
                        cmdDetails_AD.Parameters["IdealAmount"].Value = SD_Arrear.Amount;
                        cmdDetails_AD.Parameters["ChangedAmount"].Value = SD_Arrear.Amount;
                        cmdDetails_AD.Parameters["Arrear_SalaryID"].Value = nSalaryId;
                        cmdDetails_AD.Parameters["HandsOnPayment"].Value = SD_Arrear.HandsOnPayment;
                        cmdDetails_AD.Parameters["GrossHead"].Value = SD_Arrear.GrossHead;

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDetails_AD, connDS.DBConnections[0].ConnectionID, ref bError);

                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                    #endregion

                    #region update  arrear history...
                    cmdArrear.Parameters["SalaryID"].Value = nSalaryId;
                    cmdArrear.Parameters["EmployeeID"].Value = salary.EmployeeID;
                    cmdArrear.Parameters["MonthYearDate"].Value = salary.MonthYearDate;

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdArrear, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion

                    #region Save salary detail related information...

                    foreach (SalaryProcessDS.SalaryDetail detail in salary.GetSalaryDetails())
                    {
                        if (detail.Processed) continue;

                        if (!detail.IsSalaryExcludedLoanNull() && detail.SalaryExcludedLoan)
                        {
                            #region Save in Salary Excluded Loan
                            OleDbCommand cmdExclLoan = new OleDbCommand();
                            cmdExclLoan.CommandText = "PRO_SALARY_EXCLUDEDLOAN_ADD";
                            cmdExclLoan.CommandType = CommandType.StoredProcedure;

                            cmdExclLoan.Parameters.AddWithValue("p_SalaryID", nSalaryId);
                            cmdExclLoan.Parameters.AddWithValue("p_LoanIssueID", detail.LoanIssueID);
                            cmdExclLoan.Parameters.AddWithValue("p_CalculatedAmount", detail.CalculatedAmount);
                            cmdExclLoan.Parameters.AddWithValue("p_ChangedAmount", detail.ChangeAmount);

                            if (!detail.IsHeadIdNull()) cmdExclLoan.Parameters.AddWithValue("p_HeadID", detail.HeadId);
                            else cmdExclLoan.Parameters.AddWithValue("p_HeadID", DBNull.Value);

                            if (!detail.IsHeadCodeNull()) cmdExclLoan.Parameters.AddWithValue("p_HeadCode", detail.HeadCode);
                            else cmdExclLoan.Parameters.AddWithValue("p_HeadCode", DBNull.Value);

                            if (!detail.IsHeadTypeNull()) cmdExclLoan.Parameters.AddWithValue("p_HeadType", detail.HeadType);
                            else cmdExclLoan.Parameters.AddWithValue("p_HeadType", DBNull.Value);

                            if (!detail.IsDescriptionNull()) cmdExclLoan.Parameters.AddWithValue("p_Description", detail.Description);
                            else cmdExclLoan.Parameters.AddWithValue("p_Description", DBNull.Value);

                            if (!detail.IsPositionNull()) cmdExclLoan.Parameters.AddWithValue("p_Position", detail.Position);
                            else cmdExclLoan.Parameters.AddWithValue("p_Position", DBNull.Value);

                            bError = false;
                            nRowAffected = -1;
                            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdExclLoan, connDS.DBConnections[0].ConnectionID, ref bError);
                            #endregion
                        }
                        else
                        {
                            #region Save in SalaryDetail
                            cmdDetails.Parameters["SalaryId"].Value = nSalaryId;
                            cmdDetails.Parameters["HeadId"].Value = detail.HeadId;
                            cmdDetails.Parameters["HeadCode"].Value = detail.HeadCode;
                            cmdDetails.Parameters["HeadType"].Value = detail.HeadType;
                            cmdDetails.Parameters["Description"].Value = detail.Description;
                            cmdDetails.Parameters["Position"].Value = detail.Position;
                            cmdDetails.Parameters["CalculatedAmount"].Value = detail.CalculatedAmount;
                            cmdDetails.Parameters["IdealAmount"].Value = detail.IdealAmount;
                            cmdDetails.Parameters["ChangedAmount"].Value = detail.ChangeAmount;
                            cmdDetails.Parameters["HandsOnPayment"].Value = detail.HandsOnPayment;
                            cmdDetails.Parameters["GrossHead"].Value = detail.GrossHead;

                            bError = false;
                            nRowAffected = -1;
                            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDetails, connDS.DBConnections[0].ConnectionID, ref bError);
                            #endregion
                        }

                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                    #endregion

                    #region Save Gratuity Provision...

                    // Get Gratuity Provision......... 
                    GratuityProvisionDS provisionFoundDS = new GratuityProvisionDS();
                    string sPExpr = "EmployeeCode = '" + salary.EmployeeCode + "'";
                    DataRow[] foundRowsProvision = gProvisionDS.GratuityMonthlyProvision.Select(sPExpr);

                    if (foundRowsProvision.Length > 0)
                    {
                        provisionFoundDS.GratuityMonthlyProvision.ImportRow(foundRowsProvision[0]);
                        provisionFoundDS.AcceptChanges();

                        // Load Parameters......
                        cmdGratuityProvision.Parameters["SalaryId"].Value = nSalaryId;
                        cmdGratuityProvision.Parameters["AccumulatedCalculationAmount"].Value = provisionFoundDS.GratuityMonthlyProvision[0].AccumulatedCalculationAmount;
                        cmdGratuityProvision.Parameters["AccumulatedChangingAmount"].Value = provisionFoundDS.GratuityMonthlyProvision[0].AccumulatedChangingAmount;
                        cmdGratuityProvision.Parameters["ProvisionedAmount"].Value = provisionFoundDS.GratuityMonthlyProvision[0].ProvisionedAmount;
                        cmdGratuityProvision.Parameters["Remarks"].Value = provisionFoundDS.GratuityMonthlyProvision[0].Remarks;

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdGratuityProvision, connDS.DBConnections[0].ConnectionID, ref bError);

                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                    #endregion

                    #region Save Suspension Deduction...

                    // Get Suspension Deduction......... 
                    SalaryProcessDS.SalaryDeductSuspensionRow[] SuspensionRows;
                    string sSDExpr = "EmployeeID = " + salary.EmployeeID;
                    SuspensionRows = (SalaryProcessDS.SalaryDeductSuspensionRow[])processDS.SalaryDeductSuspension.Select(sSDExpr);

                    foreach (SalaryProcessDS.SalaryDeductSuspensionRow SuspensionRow in SuspensionRows)
                    {
                        cmdSuspension.Parameters["EmployeeID"].Value = SuspensionRow.EmployeeID;
                        cmdSuspension.Parameters["SalaryID"].Value = nSalaryId;
                        cmdSuspension.Parameters["MonthYearDate"].Value = salary.MonthYearDate;
                        cmdSuspension.Parameters["HeadID"].Value = SuspensionRow.HeadID;
                        cmdSuspension.Parameters["HeadType"].Value = SuspensionRow.HeadType;
                        cmdSuspension.Parameters["HeadCode"].Value = SuspensionRow.HeadCode;
                        cmdSuspension.Parameters["Description"].Value = SuspensionRow.Description;
                        cmdSuspension.Parameters["CalculatedAmount"].Value = SuspensionRow.CalculatedAmount;
                        cmdSuspension.Parameters["ChangedAmount"].Value = SuspensionRow.ChangedAmount;

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdSuspension, connDS.DBConnections[0].ConnectionID, ref bError);

                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }

                    #endregion

                    #region Update Leave Encashment...
                    if (salary.GetLeaveEncashment)
                    {
                        cmdLeaveEncashment.Parameters["ActualPayMonthYear"].Value = salary.MonthYearDate;
                        cmdLeaveEncashment.Parameters["SalaryProcessID"].Value = nSalaryProcessId;
                        cmdLeaveEncashment.Parameters["ActivitiesID"].Value = (Int32)LeaveActivities.Approve;
                        cmdLeaveEncashment.Parameters["EmployeeID"].Value = salary.EmployeeID;
                        cmdLeaveEncashment.Parameters["MonthYearDate"].Value = salary.MonthYearDate;

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveEncashment, connDS.DBConnections[0].ConnectionID, ref bError);

                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                    #endregion

                    #region Leave Without Pay...
                    if (!salary.IsLWP_LeaveIDNull() && salary.LWP_LeaveID != "")
                    {
                        #region Add SalaryID in Leave Table
                        OleDbCommand cmdLWP = new OleDbCommand();
                        cmdLWP.CommandText = "PRO_ADD_SALARYID_LWP";
                        cmdLWP.CommandType = CommandType.StoredProcedure;

                        cmdLWP.Parameters.AddWithValue("p_LeaveID", salary.LWP_LeaveID);
                        cmdLWP.Parameters.AddWithValue("p_SalaryID", nSalaryId);

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLWP, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmdLWP.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        #endregion
                    }

                    #region Save Leave Without Pay Details
                    SalaryProcessDS.LeaveWithoutPayRow[] lwpRows =
                        (SalaryProcessDS.LeaveWithoutPayRow[])processDS.LeaveWithoutPay.Select("EmployeeID = " + salary.EmployeeID);

                    OleDbCommand cmdLvDet = new OleDbCommand();
                    cmdLvDet.CommandText = "PRO_LEAVE_WITHOUT_PAY_ADD";
                    cmdLvDet.CommandType = CommandType.StoredProcedure;

                    foreach (var item in lwpRows)
                    {
                        if (!item.IsSalaryIDNull()) continue;

                        cmdLvDet.Parameters.AddWithValue("p_SalaryID", nSalaryId);
                        cmdLvDet.Parameters.AddWithValue("p_MonthYearDate", item.MonthYearDate);

                        cmdLvDet.Parameters.AddWithValue("p_CalculatedAmount", item.CalculatedAmount);
                        cmdLvDet.Parameters.AddWithValue("p_ChangedAmount", item.ChangedAmount);
                        cmdLvDet.Parameters.AddWithValue("p_HeadID", item.HeadID);
                        cmdLvDet.Parameters.AddWithValue("p_HeadCode", item.HeadCode);
                        cmdLvDet.Parameters.AddWithValue("p_HeadType", item.HeadType);
                        cmdLvDet.Parameters.AddWithValue("p_Description", item.Description);
                        cmdLvDet.Parameters.AddWithValue("p_Position", item.Position);
                        cmdLvDet.Parameters.AddWithValue("p_EmployeeID", item.EmployeeID);
                        cmdLvDet.Parameters.AddWithValue("p_SalaryMonth", processDS.SalaryProcesses[0].MonthYearDate);
                        cmdLvDet.Parameters.AddWithValue("p_IdealAmount", item.IdealAmount);
                        cmdLvDet.Parameters.AddWithValue("p_Denominator", item.Denominator);
                        cmdLvDet.Parameters.AddWithValue("p_Applicable_Per", item.Applicable_Per);
                        cmdLvDet.Parameters.AddWithValue("p_LeaveDays", item.LeaveDays);

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLvDet, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmdLvDet.Parameters.Clear();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                    cmdLvDet.Dispose();
                    #endregion
                    #endregion

                    #region Update Benevolent Share Purchasing...
                    if (!salary.IsBF_ShareIDNull() && salary.BF_ShareID != "")
                    {
                        OleDbCommand cmdShare_Update = new OleDbCommand();
                        cmdShare_Update.CommandText = "PRO_OCS_SHARE_UPDATE_BY_SP";
                        cmdShare_Update.CommandType = CommandType.StoredProcedure;

                        cmdShare_Update.Parameters.AddWithValue("p_ShareID", salary.BF_ShareID);
                        cmdShare_Update.Parameters.AddWithValue("p_SalaryID", nSalaryId);

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdShare_Update, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmdShare_Update.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                    #endregion

                    #region update  Last SalaryId in Employee...
                    cmdEmployeeLastSalaryIdUpdate.Parameters["Last_SaID"].Value = nSalaryId;
                    cmdEmployeeLastSalaryIdUpdate.Parameters["EmployeeID"].Value = salary.EmployeeID;

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdEmployeeLastSalaryIdUpdate, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion
                }
                #endregion
            } // End salary process related information Saving

            #region Save Tax History...
            cmd = DBCommandProvider.GetDBCommand("SaveTaxHistory");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            foreach (TaxHistoryDS.TaxHistory history in historyDS.TaxHistories)
            {
                IsYearEnd = history.IsYearEnd;

                cmd.Parameters["IdealAmount"].Value = history.IdealAmount;
                cmd.Parameters["CalculatedRegularTax"].Value = history.CalculatedRegularTax;
                cmd.Parameters["CalculatedExtraTax"].Value = history.CalculatedExtraTax;//
                cmd.Parameters["EmployeeCode"].Value = history.EmployeeCode;
                cmd.Parameters["MonthYear"].Value = history.MonthYear;
                cmd.Parameters["TaxableIncome"].Value = history.TaxableIncome;
                cmd.Parameters["RegularTax"].Value = history.RegularTax;
                cmd.Parameters["ExtraTax"].Value = history.ExtraTax;
                cmd.Parameters["ExtraTaxableIncome"].Value = history.ExtraTaxbleIncome;
                cmd.Parameters["BasicSalary"].Value = history.BasicSalary;
                cmd.Parameters["Medical"].Value = history.Medical;
                cmd.Parameters["HouseRent"].Value = history.HouseRent;
                cmd.Parameters["CompanyPf"].Value = history.CompanyPf;
                if (history.IsConveyanceNull() == false) cmd.Parameters["Conveyance"].Value = history.Conveyance;
                else cmd.Parameters["Conveyance"].Value = 0;
                cmd.Parameters["ConveyanceFacility"].Value = history.ConveyanceFacility;
                if (!history.IsAdvanceTaxNull()) cmd.Parameters["AdvanceTax"].Value = history.AdvanceTax;
                else cmd.Parameters["AdvanceTax"].Value = 0;
                if (!history.IsAnnualTaxableIncomeNull()) cmd.Parameters["AnnualTaxableIncome"].Value = history.AnnualTaxableIncome;
                else cmd.Parameters["AnnualTaxableIncome"].Value = 0;
                if (!history.IsLastCal_TaxPercentNull()) cmd.Parameters["LastCal_TaxPercent"].Value = history.LastCal_TaxPercent;
                else cmd.Parameters["LastCal_TaxPercent"].Value = 0;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            #region Reset Employee Wise Monthly Tax...
            if (IsYearEnd)
            {
                OleDbCommand cmdRT = new OleDbCommand();
                cmdRT.CommandText = "PRO_ResetEmployeeWiseTax";
                cmdRT.CommandType = CommandType.StoredProcedure;

                if (cmdRT == null) return UtilDL.GetCommandNotFound();

                cmdRT.Parameters.AddWithValue("p_EmployeeType", EmployeeType);
                cmdRT.Parameters.AddWithValue("p_PayrollSiteID", PayrollSiteID);

                bool bError_RT = false;
                int nRowAffected_RT = -1;
                nRowAffected_RT = ADOController.Instance.ExecuteNonQuery(cmdRT, connDS.DBConnections[0].ConnectionID, ref bError_RT);

                if (bError_RT)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #endregion

            #region Save Arear Info...
            cmd = DBCommandProvider.GetDBCommand("SaveArearInfo_New");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            foreach (EmployeeArrearInfoDS.EmployeeArrearInfoRow arearInfo in arrearInfoDS.EmployeeArrearInfo)
            {
                cmd.Parameters["EmployeeCode"].Value = arearInfo.EmployeeCode;
                cmd.Parameters["MonthYear"].Value = arearInfo.MonthDate;
                cmd.Parameters["Description"].Value = arearInfo.AllowDeductName;
                cmd.Parameters["CurrentMonth"].Value = arearInfo.CurrentMonth;
                cmd.Parameters["Amount"].Value = arearInfo.Amount;
                cmd.Parameters["HeadCode"].Value = arearInfo.AllowDeductCode;
                cmd.Parameters["HeadType"].Value = arearInfo.HeadType;
                cmd.Parameters["PrevAmount"].Value = arearInfo.PrevAmount;
                if (!arearInfo.IsSDS_IDNull()) cmd.Parameters["SDS_ID"].Value = arearInfo.SDS_ID;
                else cmd.Parameters["SDS_ID"].Value = DBNull.Value;
                if (!arearInfo.IsSBIDNull()) cmd.Parameters["SBID"].Value = arearInfo.SBID;
                else cmd.Parameters["SBID"].Value = DBNull.Value;
                cmd.Parameters["HandsOnPayment"].Value = arearInfo.HandsOnPayment;
                cmd.Parameters["GrossHead"].Value = arearInfo.GrossHead;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region LFA Created By Jarif(10 Oct 11)...
            OleDbCommand cmdLFA = new OleDbCommand();
            cmdLFA.CommandText = "PRO_LFA_REQUEST_UPDATE";
            cmdLFA.CommandType = CommandType.StoredProcedure;

            if (cmdLFA == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            LFARequestDS LFADS = new LFARequestDS();
            LFADS.Merge(inputDS.Tables[LFADS.LFASummary.TableName], false, MissingSchemaAction.Error);
            LFADS.AcceptChanges();

            foreach (LFARequestDS.LFASummaryRow row in LFADS.LFASummary.Rows)
            {
                if (row.IsACTUALPAYMONTHYEARNull()) continue;

                cmdLFA.Parameters.AddWithValue("p_LFARequestID", row.LFAREQUESTID);
                cmdLFA.Parameters.AddWithValue("p_IsGet", true);
                cmdLFA.Parameters.AddWithValue("p_MonthYear", row.ACTUALPAYMONTHYEAR);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLFA, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BATCH_SALARY_PROCESS_INFO_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmdLFA.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getSalaryProcesList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalaryProcessList_New");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //SalaryProcessListPO processP0 = new SalaryProcessListPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, processP0, processP0.Process.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENT.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //processP0.AcceptChanges();

            //SalaryProcessListDS processDS = new SalaryProcessListDS();

            //if (processP0.Process.Count > 0)
            //{
            //    foreach (SalaryProcessListPO.ProcessRow poProcess in processP0.Process)
            //    {
            //        SalaryProcessListDS.ProcessRow process = processDS.Process.NewProcessRow();
            //        if (poProcess.IsMonthYearDateNull() == false)
            //        {
            //            process.MonthYearDate = poProcess.MonthYearDate;
            //        }
            //        if (poProcess.IsEmployeeTypeNull() == false)
            //        {
            //            process.EmployeeType = poProcess.EmployeeType;
            //        }
            //        if (poProcess.IsSiteCodeNull() == false)
            //        {
            //            process.SiteCode = poProcess.SiteCode;
            //        }
            //        if (poProcess.IsSiteNameNull() == false)
            //        {
            //            process.SiteName = poProcess.SiteName;
            //        }
            //        processDS.Process.AddProcessRow(process);
            //    }
            //}
            //processDS.AcceptChanges();
            #endregion

            SalaryProcessListDS processDS = new SalaryProcessListDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, processDS, processDS.Process.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_Q_ALL_SALARY_PROCESS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            processDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(processDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _doesMonthEndProcessExist(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            DataBoolDS boolDS = new DataBoolDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DoesMonthEndDateExist");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                DateTime date = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
                cmd.Parameters["Date"].Value = date;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            DatePO datePO = new DatePO();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              datePO,
              datePO.ProcessDates.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            datePO.AcceptChanges();
            DataDateDS dateDS = new DataDateDS();
            if (datePO.ProcessDates.Count > 0)
            {
                foreach (DatePO.ProcessDate poDate in datePO.ProcessDates)
                {

                    if (poDate.IsMonthEndDateNull() == true)
                    {
                        boolDS.DataBools.AddDataBool(false);
                    }
                    else
                    {
                        boolDS.DataBools.AddDataBool(true);
                    }

                }
            }
            boolDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        //mislbd.Jarif Update Start(10-Apr-2008)//
        private DataSet _getBasicSalary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBasicSalary");
            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //BasicSalaryPO salaryPO = new BasicSalaryPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, salaryPO, salaryPO.BasicSalary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ADD.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}


            //salaryPO.AcceptChanges();

            //BasicSalaryDS salaryDS = new BasicSalaryDS();
            //if (salaryPO.BasicSalary.Count > 0)
            //{
            //    foreach (BasicSalaryPO.BasicSalaryRow poSalary in salaryPO.BasicSalary)
            //    {
            //        BasicSalaryDS.BasicSalaryRow salary = salaryDS.BasicSalary.NewBasicSalaryRow();
            //        if (poSalary.IsMONTHYEARDATENull() == false)
            //        {
            //            salary.MONTHYEARDATE = poSalary.MONTHYEARDATE;
            //        }
            //        if (poSalary.IsEMPLOYEEIDNull() == false)
            //        {
            //            salary.EMPLOYEEID = poSalary.EMPLOYEEID;
            //        }
            //        if (poSalary.IsEMPLOYEECODENull() == false)
            //        {
            //            salary.EMPLOYEECODE = poSalary.EMPLOYEECODE;
            //        }
            //        if (poSalary.IsTHISMONTHBASICNull() == false)
            //        {
            //            salary.THISMONTHBASIC = poSalary.THISMONTHBASIC;
            //        }

            //        if (poSalary.IsGRADEIDNull() == false)
            //        {
            //            salary.GRADEID = poSalary.GRADEID;
            //        }

            //        if (poSalary.IsGRADECODENull() == false)
            //        {
            //            salary.GRADECODE = poSalary.GRADECODE;
            //        }

            //        salaryDS.BasicSalary.AddBasicSalaryRow(salary);
            //        salaryDS.AcceptChanges();
            //    }
            //}
            #endregion

            BasicSalaryDS salaryDS = new BasicSalaryDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, salaryDS, salaryDS.BasicSalary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BASIC_SALARY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            salaryDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(salaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        //mislbd.Jarif Update End(10-Apr-2008)     

        private DataSet _doesSalaryProcessExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            SalaryProcessDS salaryProcessDS = new SalaryProcessDS();
            salaryProcessDS.Merge(inputDS.Tables[salaryProcessDS.SalaryProcesses.TableName], false, MissingSchemaAction.Error);
            salaryProcessDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesSalaryProcessExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (SalaryProcessDS.SalaryProcess salary in salaryProcessDS.SalaryProcesses)
            {
                if (salary.IsMonthYearDateNull() == false)
                {
                    cmd.Parameters["Date"].Value = salary.MonthYearDate;
                }
                else
                {
                    cmd.Parameters["Date"].Value = null;
                }
                if (salary.IsEmployeeTypeNull() == false)
                {
                    cmd.Parameters["EmployeeType"].Value = salary.EmployeeType;
                }
                else
                {
                    cmd.Parameters["DaEmployeeTypete"].Value = null;
                }
                if (salary.IsPayrollSiteCodeNull() == false)
                {
                    cmd.Parameters["PayrollSiteCode"].Value = salary.PayrollSiteCode;
                }
                else
                {
                    cmd.Parameters["PayrollSiteCode"].Value = null;
                }
            }
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _doesSalaryProcessMonthExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DataDateDS dateDS = new DataDateDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            //SalaryProcessDS salaryProcessDS = new SalaryProcessDS();
            //salaryProcessDS.Merge(inputDS.Tables[salaryProcessDS.SalaryProcesses.TableName], false, MissingSchemaAction.Error);
            //salaryProcessDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesSalaryProcessMonthExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (dateDS.DataDates[0].IsDateValueNull() == false)
            {
                cmd.Parameters["Date"].Value = dateDS.DataDates[0].DateValue;
            }
            else
            {
                cmd.Parameters["Date"].Value = null;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getSalaryProcessData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalary"); // Jarif (22 Sep 11)
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalary_New");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DateTime date = new DateTime();
            if (dateDS.DataDates[0].IsDateValueNull() == false)
            {
                date = dateDS.DataDates[0].DateValue;
                cmd.Parameters["Date"].Value = date;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(22 Sep 11)...
            #region Old...
            //SalaryPO salaryPO = new SalaryPO();
            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, salaryPO, salaryPO.Salaries.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_Q_ALL_PROCESS_DATA.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //salaryPO.AcceptChanges();
            //SalaryProcessDS processDS = new SalaryProcessDS();
            //SalaryProcessDS.SalaryProcess process = processDS.SalaryProcesses.NewSalaryProcess();
            //if (salaryPO.Salaries.Count > 0)
            //{
            //    foreach (SalaryPO.Salary poSalary in salaryPO.Salaries)
            //    {
            //        SalaryProcessDS.Salary salary = processDS.Salaries.NewSalary();

            //        //processDS.SalaryDetails.AcceptChanges();
            //        if (poSalary.IsEmployeeCodeNull() == false)
            //        {
            //            salary.EmployeeName = poSalary.EmployeeName;
            //        }
            //        if (poSalary.IsEmployeeCodeNull() == false)
            //        {
            //            salary.EmployeeCode = poSalary.EmployeeCode;
            //        }

            //        if (poSalary.IsThisMonthBasicNull() == false)
            //        {
            //            salary.ThisMonthBasic = poSalary.ThisMonthBasic;
            //        }
            //        processDS.Salaries.AddSalary(salary);

            //        if (poSalary.IsSalaryIdNull() == false)
            //        {
            //            //bank.BankCode = poBank.BankCode;
            //            OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("GetSalaryDetail");
            //            if (cmd == null)
            //            {

            //                Util.LogInfo("Command is null.");
            //                return UtilDL.GetCommandNotFound();
            //            }
            //            cmd1.Parameters["SalaryId"].Value = poSalary.SalaryId;

            //            OleDbDataAdapter adapter1 = new OleDbDataAdapter();
            //            adapter1.SelectCommand = cmd1;

            //            SalaryDetailPO detailPO = new SalaryDetailPO();
            //            bError = false;
            //            nRowAffected = -1;
            //            nRowAffected = ADOController.Instance.Fill(adapter1,
            //              detailPO,
            //              detailPO.SalaryDetails.TableName,
            //              connDS.DBConnections[0].ConnectionID,
            //              ref bError);
            //            if (bError == true)
            //            {
            //                ErrorDS.Error err = errDS.Errors.NewError();
            //                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //                err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_Q_ALL_PROCESS_DATA.ToString();
            //                errDS.Errors.AddError(err);
            //                errDS.AcceptChanges();
            //                return errDS;

            //            }

            //            detailPO.AcceptChanges();
            //            if (detailPO.SalaryDetails.Count > 0)
            //            {
            //                foreach (SalaryDetailPO.SalaryDetail poDetail in detailPO.SalaryDetails)
            //                {

            //                    SalaryProcessDS.SalaryDetail detail = processDS.SalaryDetails.NewSalaryDetail();
            //                    detail.Salary = salary;
            //                    if (poDetail.IsHeadIdNull() == false)
            //                    {
            //                        detail.HeadId = poDetail.HeadId;

            //                    }
            //                    if (poDetail.IsHeadTypeNull() == false)
            //                    {
            //                        detail.HeadType = poDetail.HeadType;

            //                    }
            //                    if (poDetail.IsDescriptionNull() == false)
            //                    {
            //                        detail.Description = poDetail.Description;

            //                    }
            //                    if (poDetail.IsHeadCodeNull() == false)
            //                    {
            //                        detail.HeadCode = poDetail.HeadCode;
            //                    }
            //                    if (poDetail.IsPositionNull() == false)
            //                    {
            //                        detail.Position = poDetail.Position;

            //                    }
            //                    if (poDetail.IsCalculatedAmountNull() == false)
            //                    {
            //                        detail.CalculatedAmount = poDetail.CalculatedAmount;

            //                    }
            //                    if (poDetail.IsChangeAmountNull() == false)
            //                    {
            //                        detail.ChangeAmount = poDetail.ChangeAmount;

            //                    }
            //                    if (poDetail.IsIdealAmountNull() == false)
            //                    {
            //                        detail.IdealAmount = poDetail.IdealAmount;

            //                    }
            //                    //processDS.Salaries.
            //                    processDS.SalaryDetails.AddSalaryDetail(detail);
            //                }

            //            }

            //        }
            //    }
            //    processDS.Salaries.AcceptChanges();

            //}

            //processDS.AcceptChanges();
            #endregion

            #region Data Load For Salary...
            SalaryProcessDS processDS = new SalaryProcessDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, processDS, processDS.Salaries.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_Q_ALL_PROCESS_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            processDS.AcceptChanges();

            #endregion
            #region Data Load For Salary Detail...
            OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("GetSalaryDetail_New");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            cmd1.Parameters["Date"].Value = date;

            OleDbDataAdapter adapter1 = new OleDbDataAdapter();
            adapter1.SelectCommand = cmd1;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter1, processDS, processDS.SalaryDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_Q_ALL_PROCESS_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            processDS.AcceptChanges();

            #endregion
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(processDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _deleteSalaryProcess(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError = false;
            int nRowAffected = -1;

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            SalaryProcessDS processDS = new SalaryProcessDS();
            processDS.Merge(inputDS.Tables[processDS.SalaryProcesses.TableName], false, MissingSchemaAction.Error);
            processDS.AcceptChanges();

            #region Salary Information Checking...
            //N.B: Year == 1 (Ivalid),  Year == 2 (Valid),  Year >2 (Compear) 
            DateTime dMaxEntryDate = GetMaxJobHistEntryDate(connDS);

            if (!processDS.SalaryProcesses[0].IsCancelProcessForcely)
            {
                if ((dMaxEntryDate.Year == 1) || ((dMaxEntryDate.AddDays(-(dMaxEntryDate.Day - 1))) > (processDS.SalaryProcesses[0].MonthYearDate)))
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DATE_OUT_OF_RANGE.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SALARYPROCESS_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Delete Salary Information...
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_DEL_SALARY_INFORMATION";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("p_SalaryProcessID", processDS.SalaryProcesses[0].SalaryProcessID);
            cmd.Parameters.AddWithValue("p_SalaryMonth", processDS.SalaryProcesses[0].MonthYearDate);
            cmd.Parameters.AddWithValue("p_ProcessDate", processDS.SalaryProcesses[0].ProcessDate);
            if (!processDS.SalaryProcesses[0].IsMonthEndDateNull())
                cmd.Parameters.AddWithValue("p_MonthendDate", processDS.SalaryProcesses[0].MonthEndDate);
            else cmd.Parameters.AddWithValue("p_MonthendDate", System.DBNull.Value);
            cmd.Parameters.AddWithValue("p_WorkDays", processDS.SalaryProcesses[0].WorkDays);
            cmd.Parameters.AddWithValue("p_EmployeeType", processDS.SalaryProcesses[0].EmployeeType);
            cmd.Parameters.AddWithValue("p_PayrollSiteID", processDS.SalaryProcesses[0].PayrollsiteID);
            if (!processDS.SalaryProcesses[0].IsCreate_UserNull())
            {
                cmd.Parameters.AddWithValue("p_Create_User", processDS.SalaryProcesses[0].Create_User);
                cmd.Parameters.AddWithValue("p_Create_Date", processDS.SalaryProcesses[0].Create_Date);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_Create_User", null);
                cmd.Parameters.AddWithValue("p_Create_Date", System.DBNull.Value);
            }
            cmd.Parameters.AddWithValue("p_LoginID", processDS.SalaryProcesses[0].LoginID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SALARYPROCESS_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getPrevMonthSalaryProcessInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPrevMonthSalaryProcessInfo");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                DateTime date = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
                cmd.Parameters["MonthYearDate"].Value = date;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //SalaryProcessListPO processP0 = new SalaryProcessListPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, processP0, processP0.Process.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENT.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //processP0.AcceptChanges();

            //SalaryProcessListDS processDS = new SalaryProcessListDS();

            //if (processP0.Process.Count > 0)
            //{
            //    foreach (SalaryProcessListPO.ProcessRow poProcess in processP0.Process)
            //    {
            //        SalaryProcessListDS.ProcessRow process = processDS.Process.NewProcessRow();
            //        if (poProcess.IsMonthYearDateNull() == false)
            //        {
            //            process.MonthYearDate = poProcess.MonthYearDate;
            //        }
            //        if (poProcess.IsEmployeeTypeNull() == false)
            //        {
            //            process.EmployeeType = poProcess.EmployeeType;
            //        }
            //        if (poProcess.IsSiteCodeNull() == false)
            //        {
            //            process.SiteCode = poProcess.SiteCode;
            //        }
            //        if (poProcess.IsSiteNameNull() == false)
            //        {
            //            process.SiteName = poProcess.SiteName;
            //        }
            //        processDS.Process.AddProcessRow(process);
            //    }
            //}
            //processDS.AcceptChanges();
            #endregion

            SalaryProcessListDS processDS = new SalaryProcessListDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, processDS, processDS.Process.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_PREV_MONTH_PROCESS_INFO_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            processDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(processDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getEmployeeArrearInfoSummary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetArearInfoSummary_New");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["CurrentMonth"].Value = dateDS.DataDates[0].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(26 Sep 11)...
            #region Old...
            //EmployeeArrearInfoPO InfoPO = new EmployeeArrearInfoPO();
            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, InfoPO, InfoPO.EmployeeArrearInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_EMPLOYEE_ARREAR_INFO_SUMMARY.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //InfoPO.AcceptChanges();

            //EmployeeArrearInfoDS InfoDS = new EmployeeArrearInfoDS();
            //if (InfoPO.EmployeeArrearInfo.Count > 0)
            //{
            //    foreach (EmployeeArrearInfoPO.EmployeeArrearInfoRow poInfo in InfoPO.EmployeeArrearInfo)
            //    {
            //        EmployeeArrearInfoDS.EmployeeArrearInfoRow info = InfoDS.EmployeeArrearInfo.NewEmployeeArrearInfoRow();

            //        if (poInfo.IsEmployeeCodeNull() == false)
            //        {
            //            info.EmployeeCode = poInfo.EmployeeCode;
            //        }

            //        if (poInfo.IsCurrentmonthdateNull() == false)
            //        {
            //            info.CurrentMonth = poInfo.Currentmonthdate;
            //        }
            //        if (poInfo.IsSalaryheaddescriptionNull() == false)
            //        {
            //            info.AllowDeductName = poInfo.Salaryheaddescription;
            //        }
            //        if (poInfo.IsSalaryheadcodeNull() == false)
            //        {
            //            info.AllowDeductCode = poInfo.Salaryheadcode;
            //        }
            //        if (poInfo.IsMonthyeardateNull() == false)
            //        {
            //            info.MonthDate = poInfo.Monthyeardate;
            //        }
            //        if (poInfo.IsAmountNull() == false)
            //        {
            //            info.Amount = poInfo.Amount;
            //        }
            //        if (poInfo.IsHeadTypeNull() == false)
            //        {
            //            info.HeadType = poInfo.HeadType;
            //        }
            //        if (poInfo.IsPrevAmountNull() == false)
            //        {
            //            info.PrevAmount = poInfo.PrevAmount;
            //        }
            //        InfoDS.EmployeeArrearInfo.AddEmployeeArrearInfoRow(info);
            //        InfoDS.AcceptChanges();
            //    }
            //}
            #endregion

            EmployeeArrearInfoDS InfoDS = new EmployeeArrearInfoDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, InfoDS, InfoDS.EmployeeArrearInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_EMPLOYEE_ARREAR_INFO_SUMMARY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            InfoDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(InfoDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        //mislbd.Jarif Update Start(17-Apr-2008)
        private DateTime GetMaxJobHistEntryDate(DBConnectionDS connDS)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetMaxJobHistEntryDate");

            DateTime dInValidDate = new DateTime(1754, 1, 1);

            if (cmd == null)
            {
                return dInValidDate;
            }

            bool bError = false;
            object obj = ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                return dInValidDate;
            }
            if (obj == DBNull.Value)
            {
                DateTime dDateNow = new DateTime(2, 2, 2);
                return dDateNow;
            }
            DateTime maxDate = Convert.ToDateTime(obj);
            return maxDate;
        }
        //mislbd.Jarif Update End(17-Apr-2008)  

        private LFARequestDS _getEmployeeWhoGetLFA(DBConnectionDS connDS, DateTime salaryMonth)
        {
            ErrorDS errDS = new ErrorDS();
            LFARequestDS requestDS = new LFARequestDS();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeWhoGetLFA");

            if (cmd == null)
            {
                return requestDS;
            }

            cmd.Parameters["SalaryMonth"].Value = salaryMonth;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            ADOController.Instance.Fill(adapter, requestDS, requestDS.LFASummary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requestDS.AcceptChanges();

            return requestDS;
        }

        private LFARequestDS _getEmployeeWhoGetLFA(DBConnectionDS connDS, DateTime salaryMonth, Int32 EmpType, string PayrollSite)
        {
            ErrorDS errDS = new ErrorDS();
            LFARequestDS requestDS = new LFARequestDS();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeWhoGetLFAByEmpTypeSite");

            if (cmd == null)
            {
                return requestDS;
            }

            cmd.Parameters["SalaryMonth"].Value = salaryMonth;
            cmd.Parameters["EmpType"].Value = EmpType;
            cmd.Parameters["PayrollSite"].Value = PayrollSite;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            ADOController.Instance.Fill(adapter, requestDS, requestDS.LFASummary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requestDS.AcceptChanges();

            return requestDS;
        }

        private DataSet _getSalary_SalaryDetailList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalary_SalaryDetail");
            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }
            cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            SalaryProcessDS salaryDS = new SalaryProcessDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, salaryDS, salaryDS.Salary_SalaryDetail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SALARY_SALARYDETAIL_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            salaryDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(salaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getSalary_SalaryDetailList_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalary_SalaryDetail_SP");
            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }
            cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            SalaryProcessDS salaryDS = new SalaryProcessDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, salaryDS, salaryDS.Salary_SalaryDetail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SALARY_SALARYDETAIL_LIST_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            salaryDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(salaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getEmployeeArrearList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeArrear");
            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }
            cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeArrearInfoDS arrearDS = new EmployeeArrearInfoDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, arrearDS, arrearDS.EmployeeArrearInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_ARREAR_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            arrearDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(arrearDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getSalaryProcessData_By_Emp(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DataDateDS dateDS = new DataDateDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            SalaryProcessDS processDS = new SalaryProcessDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Data Load For Salary...
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalary_By_SalaryMonth_Emp");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, processDS, processDS.Salaries.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SALARY_PROCESS_DATA_BY_EMP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            processDS.AcceptChanges();
            #endregion

            #region Data Load For Salary Detail...
            OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("GetSalaryDetail_By_SalaryMonth_Emp");
            if (cmd1 == null) return UtilDL.GetCommandNotFound();

            cmd1.Parameters["SalaryMonth1"].Value = dateDS.DataDates[0].DateValue;
            cmd1.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd1.Parameters["SalaryMonth2"].Value = dateDS.DataDates[0].DateValue;
            cmd1.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter1 = new OleDbDataAdapter();
            adapter1.SelectCommand = cmd1;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter1, processDS, processDS.SalaryDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SALARY_PROCESS_DATA_BY_EMP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            processDS.AcceptChanges();
            #endregion

            #region Data Load For Leave Without Pay
            cmd = DBCommandProvider.GetDBCommand("GetLeaveWithoutPay_By_SalaryMonth_Emp");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

            adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, processDS, processDS.LeaveWithoutPay.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SALARY_PROCESS_DATA_BY_EMP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            processDS.AcceptChanges();
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(processDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getSuspensionDeduction_BySalaryID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            SalaryProcessDS processDS = new SalaryProcessDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            #region Data Load Data...
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSDeduction_BySalaryID");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["SalaryID"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, processDS, processDS.SalaryDeductSuspension.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SuspensionDeduction_BySalaryID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            processDS.AcceptChanges();
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(processDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getDS_Suspension_ForRefund(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            SalaryProcessDS processDS = new SalaryProcessDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Data Load Data...
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDS_Suspension_ForRefund");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, processDS, processDS.SalaryDeductSuspension.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DS_Suspension_ForRefund.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            processDS.AcceptChanges();
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(processDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getBasicSalary_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBasicSalary_SP");
            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["SalaryProcessDate"].Value = (object)dateDS.DataDates[0].DateValue;

            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BasicSalaryDS salaryDS = new BasicSalaryDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, salaryDS, salaryDS.BasicSalary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BASIC_SALARY_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            salaryDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(salaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getBenevolent_Fund_Info_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBenevolent_Fund_Info_SP");
            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["SalaryProcessDate"].Value = (object)dateDS.DataDates[0].DateValue;
            //cmd.Parameters["SalaryProcessDate1"].Value = (object)dateDS.DataDates[0].DateValue;

            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            BenevolentFund bfDS = new BenevolentFund();
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_FundMember.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BENEVOLENT_FUND_INFO_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }

        private DataSet _getSalaryWithhold_Info(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd;
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            long designationID = UtilDL.GetDesignationId(connDS, stringDS.DataStrings[4].StringValue);

            if (stringDS.DataStrings[0].StringValue == "EmployeeCode") cmd = DBCommandProvider.GetDBCommand("GetSalaryWithhold_Info_ByCode");
            else cmd = DBCommandProvider.GetDBCommand("GetSalaryWithhold_Info_ByGradePos");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["PayrollSiteID1"].Value = cmd.Parameters["PayrollSiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
            cmd.Parameters["DepartmentID1"].Value = cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
            cmd.Parameters["DesignationID1"].Value = cmd.Parameters["DesignationID2"].Value = designationID;
            cmd.Parameters["EmpStatus1"].Value = cmd.Parameters["EmpStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            SalaryProcessDS salaryDS = new SalaryProcessDS();
            nRowAffected = ADOController.Instance.Fill(adapter, salaryDS, salaryDS.SalaryWithhold.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_SALARY_WITHHOLD_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            salaryDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(salaryDS.SalaryWithhold);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _saveSalaryWithhold_Info(DataSet inputDS)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            SalaryProcessDS salaryDS = new SalaryProcessDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            salaryDS.Merge(inputDS.Tables[salaryDS.SalaryWithhold.TableName], false, MissingSchemaAction.Error);
            salaryDS.AcceptChanges();

            long designationID = UtilDL.GetDesignationId(connDS, stringDS.DataStrings[4].StringValue);

            #region Delete Old Data
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SALARY_WITHHOLD_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("p_SiteID", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_PayrollSiteID", Convert.ToInt32(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_DepartmentID", Convert.ToInt32(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_DesignationID", designationID);
            cmd.Parameters.AddWithValue("p_JobStatus", Convert.ToInt32(stringDS.DataStrings[5].StringValue));

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SALARY_WITHHOLD_INFO_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Save new data
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SALARY_WITHHOLD_INFO_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (SalaryProcessDS.SalaryWithholdRow row in salaryDS.SalaryWithhold.Rows)
            {
                long pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_SalaryWithhold_ID", pKey);
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);

                if (!row.IsWithhold_UptoNull()) cmd.Parameters.AddWithValue("p_Withhold_Upto", row.Withhold_Upto);
                else cmd.Parameters.AddWithValue("p_Withhold_Upto", DBNull.Value);

                if (!row.IsWithhold_RemarksNull()) cmd.Parameters.AddWithValue("p_Withhold_Remarks", row.Withhold_Remarks);
                else cmd.Parameters.AddWithValue("p_Withhold_Remarks", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SALARY_WITHHOLD_INFO_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getLastSalaryProcessDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLastSalaryProcessDate");
            adapter.SelectCommand = cmd;
            Debug.Assert(cmd != null);

            cmd.Parameters["EmployeeType"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;

            bool bError = false;
            int nRowAffected = -1;

            DataDateDS dateDS = new DataDateDS();

            nRowAffected = ADOController.Instance.Fill(adapter, dateDS, dateDS.DataDates.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LAST_SALARY_PROCESS_DATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            dateDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(dateDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        #region Rony :: 26 Nov 2016 :: Arrear Salary.....
        private DataSet _createEmployee_Salary_Arrear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            EmployeeArrearInfoDS arrearDS = new EmployeeArrearInfoDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EMP_SALARY_ARREAR_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Tax = new OleDbCommand();
            cmd_Tax.CommandText = "PRO_SALARY_OUTSIDE_TAX_ADD";
            cmd_Tax.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();
            arrearDS.Merge(inputDS.Tables[arrearDS.EmployeeArrearInfo.TableName], false, MissingSchemaAction.Error);
            arrearDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Rony :: Create Salary Arrear :: 26 Nov 2016

            DataView dv = new DataView(arrearDS.EmployeeArrearInfo);
            DataTable dtEmployee = dv.ToTable(true, "EmployeeCode", "MonthDate");
            dtEmployee.AcceptChanges();

            decimal tax = 0, lastCal_TaxPercent = 0;

            foreach (DataRow dr in dtEmployee.Rows)
            {
                bool bError = false;
                int nRowAffected = -1;
                bool foundEmployeeArrear = DoesSalaryArrearInfoExist(connDS, dr["EmployeeCode"].ToString(), Convert.ToDateTime(dr["MonthDate"]));
                if (!foundEmployeeArrear)
                {

                    string sExpr = "EmployeeCode = '" + dr["EmployeeCode"].ToString() + "'";
                    EmployeeArrearInfoDS.EmployeeArrearInfoRow[] foundRows = (EmployeeArrearInfoDS.EmployeeArrearInfoRow[])arrearDS.EmployeeArrearInfo.Select(sExpr);
                    if (foundRows.Length > 0)
                    {
                        tax = lastCal_TaxPercent = 0;
                        if (!foundRows[0].IsLastCal_TaxPercentNull()) lastCal_TaxPercent = foundRows[0].LastCal_TaxPercent;

                        foreach (EmployeeArrearInfoDS.EmployeeArrearInfoRow row in foundRows)
                        {
                            cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                            cmd.Parameters.AddWithValue("p_SalaryHeadName", row.AllowDeductName);
                            cmd.Parameters.AddWithValue("p_CurrentMonth", row.CurrentMonth);
                            cmd.Parameters.AddWithValue("p_MonthDate", row.MonthDate);
                            if (!row.IsAmountNull()) cmd.Parameters.AddWithValue("p_Amount", row.Amount);
                            else cmd.Parameters.AddWithValue("p_Amount", DBNull.Value);
                            cmd.Parameters.AddWithValue("p_HeadType", row.HeadType);

                            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                            if (bError)
                            {
                                ErrorDS.Error err = errDS.Errors.NewError();
                                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                                err.ErrorInfo1 = ActionID.ACTION_EMP_SALARY_ARREAR_ADD.ToString();
                                errDS.Errors.AddError(err);
                                errDS.AcceptChanges();
                                return errDS;
                            }
                            cmd.Parameters.Clear();

                            tax += (row.Amount * lastCal_TaxPercent) / 100;
                        }

                        #region Save OutSide Tax...
                        if (tax != 0)
                        {
                            cmd_Tax.Parameters.AddWithValue("p_MonthYearDate", foundRows[0].MonthDate);
                            cmd_Tax.Parameters.AddWithValue("p_EmployeeID", foundRows[0].EmployeeID);
                            cmd_Tax.Parameters.AddWithValue("p_Tax", Math.Round(tax, MidpointRounding.AwayFromZero));
                            cmd_Tax.Parameters.AddWithValue("p_SalaryOutside_Tax", 0);
                            cmd_Tax.Parameters.AddWithValue("p_Arrear_Tax", -1);
                            cmd_Tax.Parameters.AddWithValue("p_CreateDate", foundRows[0].CurrentMonth);

                            bError = false;
                            nRowAffected = -1;
                            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Tax, connDS.DBConnections[0].ConnectionID, ref bError);
                            if (bError)
                            {
                                ErrorDS.Error err = errDS.Errors.NewError();
                                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                                err.ErrorInfo1 = ActionID.ACTION_EMP_SALARY_ARREAR_ADD.ToString();
                                errDS.Errors.AddError(err);
                                errDS.AcceptChanges();
                                return errDS;
                            }
                            cmd_Tax.Parameters.Clear();
                        }
                        #endregion

                        if (foundRows[0].IsArrearComplete)
                        {
                            OleDbCommand cmd_updateHist = new OleDbCommand();
                            cmd_updateHist.CommandText = "PRO_EMPLOYEEJOBHIST_ARREAR_UPD";
                            cmd_updateHist.CommandType = CommandType.StoredProcedure;

                            if (cmd_updateHist == null) return UtilDL.GetCommandNotFound();
                            cmd_updateHist.Parameters.AddWithValue("p_EmployeeJobHistID", foundRows[0].EmployeeJobHistID);
                            cmd_updateHist.Parameters.AddWithValue("p_EventCode", (Int32)EventCode.INCREMENT);
                            cmd_updateHist.Parameters.AddWithValue("p_EmployeeCode", foundRows[0].EmployeeCode);

                            bool bErrorDel = false;
                            int nRowAffectedDel = -1;
                            nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmd_updateHist, connDS.DBConnections[0].ConnectionID, ref bErrorDel);
                            cmd_updateHist.Parameters.Clear();
                            if (bErrorDel)
                            {
                                ErrorDS.Error err = errDS.Errors.NewError();
                                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                                err.ErrorInfo1 = ActionID.ACTION_EMP_SALARY_ARREAR_ADD.ToString();
                                errDS.Errors.AddError(err);
                                errDS.AcceptChanges();
                                return errDS;
                            }
                        }
                    }
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(dr["EmployeeCode"].ToString());
                else messageDS.ErrorMsg.AddErrorMsgRow(dr["EmployeeCode"].ToString());
                messageDS.AcceptChanges();
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private bool DoesSalaryArrearInfoExist(DBConnectionDS connDS, string employeeCode, DateTime monthyearDate)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DoesEmployeeSalaryArrearExist");
            cmd.Parameters["MONTHYEARDATE"].Value = monthyearDate;
            cmd.Parameters["EMPLOYEECODE"].Value = employeeCode;
            if (cmd == null) return false;
            bool bError = false;
            object obj = ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true) return true;
            if (obj == DBNull.Value) return false;
            if (Convert.ToInt32(obj) > 0) return true;
            return false;
        }
        private DataSet _deleteEmp_Salary_Arrear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeArrearInfoDS arrearDS = new EmployeeArrearInfoDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            arrearDS.Merge(inputDS.Tables[arrearDS.EmployeeArrearInfo.TableName], false, MissingSchemaAction.Error);
            arrearDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EMP_SALARY_ARREAR_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeArrearInfoDS.EmployeeArrearInfoRow row in arrearDS.EmployeeArrearInfo.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeJobHistID", row.EmployeeJobHistID);
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                cmd.Parameters.AddWithValue("p_ProcessDate", row.CurrentMonth);
                cmd.Parameters.AddWithValue("p_MonthYearDate", row.MonthDate);
                cmd.Parameters.AddWithValue("p_EventCode", (Int32)EventCode.INCREMENT);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SALARY_ARREAR_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode);
                messageDS.AcceptChanges();
            }
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        #endregion

        private long GetSalaryProcessID(DBConnectionDS connDS, Int32 employeeType, Int32 payrollSiteID, DateTime monthyearDate)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalaryProcessID_SP");
            cmd.Parameters["EmployeeType"].Value = employeeType;
            cmd.Parameters["PayrollSiteID"].Value = payrollSiteID;
            cmd.Parameters["MonthYearDate"].Value = monthyearDate;

            if (cmd == null) throw new Exception("CMD Not Found.");

            bool bError = false;
            object obj = ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError) throw new Exception("DB Operation Failed.");
            if (obj == DBNull.Value || obj == null) return -1;

            return Convert.ToInt64(obj);
        }

        private DataSet _IsReprocessPending(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            DataBoolDS boolDS = new DataBoolDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalaryReprocess_Pending");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_CHECK_IS_SALARY_REPROCESS_PENDING.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _IsPayProcessPending(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            DataBoolDS boolDS = new DataBoolDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalaryProcess_MonthDifference");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["MaxProcessDate1"].Value = cmd.Parameters["MaxProcessDate2"].Value =
                cmd.Parameters["MaxProcessDate3"].Value = dateDS.DataDates[0].DateValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_CHECK_IS_SALARY_PROCESS_PENDING.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            // if Month_Difference is 1, that means one or more PayrollSite has pending pay process for last month.
            // if Month_Difference is 0, that means all PayrollSites has completed last month's pay process.
            // if Month_Difference is more than one, that means one or more PayrollSite had pending process before, so ignore them.

            boolDS.DataBools.AddDataBool(nRowAffected == 1 ? true : false);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _loadLeaveWithoutPay(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLeaveWithoutPay_SP");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["SalaryDate"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["SalaryDate2"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmployeeType"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = integerDS.DataIntegers[1].IntegerValue;            

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeDS empDS = new EmployeeDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_WITHOUT_PAY_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            empDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(empDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getEmployeeArrearInfo_Report(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            OleDbCommand cmd = new OleDbCommand();

            if (stringDS.DataStrings[6].StringValue == "ArrearDetailForReport")
            { cmd = DBCommandProvider.GetDBCommand("GetArearInfo_Report"); }
            else if (stringDS.DataStrings[6].StringValue == "ArrearSummaryForReport")
            { cmd = DBCommandProvider.GetDBCommand("GetArearInfoSummary_Report"); }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            long siteID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[0].StringValue);
            cmd.Parameters["CurrentMonth"].Value = dateDS.DataDates[0].DateValue;
            //Rony :: 16 Mar 2017
            cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = siteID;
            cmd.Parameters["DepartmentCode1"].Value = cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["DesignationCode1"].Value = cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["FunctionCode1"].Value = cmd.Parameters["FunctionCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["LiabilityCode1"].Value = cmd.Parameters["LiabilityCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["CompanyDivisionCode1"].Value = cmd.Parameters["CompanyDivisionCode2"].Value = stringDS.DataStrings[5].StringValue;
            cmd.Parameters["CompanyCode1"].Value = cmd.Parameters["CompanyCode2"].Value = stringDS.DataStrings[7].StringValue;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeArrearInfoDS InfoDS = new EmployeeArrearInfoDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, InfoDS, InfoDS.EmployeeArrearInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_EMPLOYEE_ARREAR_INFO_REPORT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            InfoDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(InfoDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _loadPrevLWPSalary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetPrevLWPSalary_SP");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeType"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = integerDS.DataIntegers[1].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            SalaryProcessDS LWP_Deduction = new SalaryProcessDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, LWP_Deduction, LWP_Deduction.LeaveWithoutPay.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LWP_DEDUCTION_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            LWP_Deduction.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(LWP_Deduction);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _loadRetirementSalaryBenefit(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetRetirementSalaryBenefit_SP");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            SalaryProcessDS RSB_Policy = new SalaryProcessDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, RSB_Policy, RSB_Policy.RetirementSalaryBenefit.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_RETIREMENT_SALARY_BENEFIT_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            RSB_Policy.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(RSB_Policy);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getPrevSalaryDetail_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPrevSalaryDetail_SP");
            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }
            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["SalaryProcessDate"].Value = (object)dateDS.DataDates[0].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            SalaryProcessDS salaryDS = new SalaryProcessDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, salaryDS, salaryDS.PrevSalaryDetail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PREV_SALARYDETAIL_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            salaryDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(salaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _loadLWPSalary_SM(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLWPSalary_SM");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["startDate"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["endDate"].Value = dateDS.DataDates[1].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            SalaryProcessDS LWP_Deduction = new SalaryProcessDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, LWP_Deduction, LWP_Deduction.LeaveWithoutPay.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LWP_DEDUCTION_SM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            LWP_Deduction.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(LWP_Deduction);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getSalaryProcessDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalaryProcessDate");
            if (cmd == null) return UtilDL.GetDBOperationFailed();
            
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            SalaryProcessDS salaryDS = new SalaryProcessDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, salaryDS, salaryDS.Salaries.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_SALARY_PROCESS_DATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            salaryDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(salaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _loadLWPAdjustment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLWPAdjustment_SP");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeType"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = integerDS.DataIntegers[1].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            SalaryProcessDS LWP_Adjustment = new SalaryProcessDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, LWP_Adjustment, LWP_Adjustment.LeaveWithoutPay.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LWP_ADJUSTMENT_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            LWP_Adjustment.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(LWP_Adjustment);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _deleteSalaryProcess_ByEmployeeID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            bool bError = false;
            int nRowAffected = -1;

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            SalaryProcessDS processDS = new SalaryProcessDS();
            processDS.Merge(inputDS.Tables[processDS.SalaryProcesses.TableName], false, MissingSchemaAction.Error);
            processDS.AcceptChanges();

            #region Delete Salary Information...
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_DEL_SALARY_INFO_BY_EMPID";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("p_SalaryID", processDS.SalaryProcesses[0].SalaryID);
            cmd.Parameters.AddWithValue("p_EmployeeID", processDS.SalaryProcesses[0].EmployeeID);
            cmd.Parameters.AddWithValue("p_SalaryMonth", processDS.SalaryProcesses[0].MonthYearDate);
            cmd.Parameters.AddWithValue("p_LoginID", processDS.SalaryProcesses[0].LoginID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SALARY_BY_EMPID_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            return errDS;  // return empty ErrorDS
        }

        private DataSet _get_Paid_Bonus_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPaidBonus_SP");
            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }
            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["SalaryProcessDate"].Value = (object)dateDS.DataDates[0].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            SalaryProcessDS salaryDS = new SalaryProcessDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, salaryDS, salaryDS.Salary_SalaryDetail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PAID_BONUS_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            salaryDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(salaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getOutsideSalaryProcessList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetOutsideSalaryMonth");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...


            SalaryProcessListDS processDS = new SalaryProcessListDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, processDS, processDS.Process.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_BATCH_Q_ALL_SALARY_PROCESS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            processDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(processDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getOutsideSalaryProcessData_By_Emp(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DataDateDS dateDS = new DataDateDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            SalaryProcessDS processDS = new SalaryProcessDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Data Load For Salary...
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetOutsideSalaryByMonth_Emp");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, processDS, processDS.Salaries.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_OUTSIDE_SALARY_PROCESS_DATA_BY_EMP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            processDS.AcceptChanges();
            #endregion

            #region Data Load For Salary Detail...
            OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("GetOutsideSalaryDetailByMonth_Emp");
            if (cmd1 == null) return UtilDL.GetCommandNotFound();

            cmd1.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
            cmd1.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd1.Parameters["SalaryMonth1"].Value = dateDS.DataDates[0].DateValue;
            cmd1.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter1 = new OleDbDataAdapter();
            adapter1.SelectCommand = cmd1;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter1, processDS, processDS.Salary_SalaryDetail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_OUTSIDE_SALARY_PROCESS_DATA_BY_EMP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            processDS.AcceptChanges();
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(processDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _saveOutsideChangedSalary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SalaryProcessDS processDS = new SalaryProcessDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();
            DataStringDS stringDS = new DataStringDS();

            processDS.Merge(inputDS.Tables[processDS.Salary_SalaryDetail.TableName], false, MissingSchemaAction.Error);
            processDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DateTime date = new DateTime(2000, 1, 1);
            string sEmployeeCode = "", sLoginId = "";

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                sEmployeeCode = stringDS.DataStrings[0].StringValue;
            }
            if (dateDS.DataDates[0].IsDateValueNull() == false)
            {
                date = dateDS.DataDates[0].DateValue;
            }
            if (stringDS.DataStrings[1].IsStringValueNull() == false)
            {
                sLoginId = stringDS.DataStrings[1].StringValue;
            }

            #region Outside Salary Update By Last User.

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SALARY_OUTSIDE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Tax = new OleDbCommand();
            cmd_Tax.CommandText = "PRO_SALARY_OUTSIDE_TAX_UPDATE";
            cmd_Tax.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            bool bError = false;
            int nRowAffected = -1;
            foreach (SalaryProcessDS.Salary_SalaryDetailRow sdsRow in processDS.Salary_SalaryDetail.Rows)
            {
                #region Update in Salary OutSide...
                if (sdsRow.HeadCode != "TAX")
                {
                    cmd.Parameters.AddWithValue("p_OutsidePayrollID", sdsRow.SalaryProcessID);
                    cmd.Parameters.AddWithValue("p_MonthYearDate", date);
                    cmd.Parameters.AddWithValue("p_EmployeeCode", sEmployeeCode);
                    cmd.Parameters.AddWithValue("p_ChangedAmount", sdsRow.ChangedAmount);
                    cmd.Parameters.AddWithValue("P_LoginID", sLoginId);
                    if (!sdsRow.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", sdsRow.Remarks);
                    else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);


                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    cmd.Parameters.Clear();
                }
                else
                {
                    #region Update OutSide Tax...
                    cmd_Tax.Parameters.AddWithValue("p_MonthYearDate", date);
                    cmd_Tax.Parameters.AddWithValue("p_EmployeeCode", sEmployeeCode);
                    cmd_Tax.Parameters.AddWithValue("p_Tax", sdsRow.ChangedAmount);
                    cmd_Tax.Parameters.AddWithValue("p_SalaryOutside_Tax", -1);
                    cmd_Tax.Parameters.AddWithValue("p_Arrear_Tax", 0);
                    if (!sdsRow.IsRemarksNull()) cmd_Tax.Parameters.AddWithValue("p_Remarks", sdsRow.Remarks);
                    else cmd_Tax.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Tax, connDS.DBConnections[0].ConnectionID, ref bError);

                    cmd_Tax.Parameters.Clear();
                    #endregion
                }

                #endregion

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BATCH_CHANGED_OUTSIDE_SP_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteOutside_SalaryProcess_ByEmployee(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            bool bError = false;
            int nRowAffected = -1;

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            SalaryProcessDS processDS = new SalaryProcessDS();
            processDS.Merge(inputDS.Tables[processDS.Salaries.TableName], false, MissingSchemaAction.Error);
            processDS.AcceptChanges();

            #region Delete Salary Information...
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_DEL_S_OUTSIDE_BY_EMPID";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("p_SalaryMonth", processDS.Salaries[0].MonthYearDate);
            cmd.Parameters.AddWithValue("p_EmployeeCode", processDS.Salaries[0].EmployeeCode);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_OUTSIDE_SALARY_BY_EMPID_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            return errDS;  // return empty ErrorDS
        }

        private DataSet _SaveOutSideProcessInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            SalaryProcessDS processDS = new SalaryProcessDS();
            processDS.Merge(inputDS.Tables[processDS.Salary_SalaryDetail.TableName], false, MissingSchemaAction.Error);
            processDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SALARY_OUTSIDE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Tax = new OleDbCommand();
            cmd_Tax.CommandText = "PRO_SALARY_OUTSIDE_TAX_ADD";
            cmd_Tax.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            bool bError = false;
            int nRowAffected = -1;
            foreach (SalaryProcessDS.Salary_SalaryDetailRow row in processDS.Salary_SalaryDetail.Rows)
            {
                #region Save in Salary OutSide...
                if (row.HeadCode != "TAX")
                {
                    cmd.Parameters.AddWithValue("p_MonthYearDate", row.MonthYearDate);
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_HeadID", row.HeadID);
                    cmd.Parameters.AddWithValue("p_HeadCode", row.HeadCode);
                    cmd.Parameters.AddWithValue("p_HeadType", row.HeadType);
                    cmd.Parameters.AddWithValue("p_Description", row.Description);
                    cmd.Parameters.AddWithValue("p_Position", row.Position);
                    cmd.Parameters.AddWithValue("p_CalculatedAmount", row.CalculatedAmount);
                    cmd.Parameters.AddWithValue("p_ChangedAmount", row.ChangedAmount);
                    cmd.Parameters.AddWithValue("p_IdealAmount", row.IdealAmount);
                    cmd.Parameters.AddWithValue("P_LoginID", row.LoginID);
                    cmd.Parameters.AddWithValue("p_CreateDate", row.CreateDate);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    cmd.Parameters.Clear();
                }
                else
                {
                    #region Save OutSide Tax...
                    cmd_Tax.Parameters.AddWithValue("p_MonthYearDate", row.MonthYearDate);
                    cmd_Tax.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd_Tax.Parameters.AddWithValue("p_Tax", row.ChangedAmount);
                    cmd_Tax.Parameters.AddWithValue("p_SalaryOutside_Tax", -1);
                    cmd_Tax.Parameters.AddWithValue("p_Arrear_Tax", 0);
                    cmd_Tax.Parameters.AddWithValue("p_CreateDate", row.CreateDate);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Tax, connDS.DBConnections[0].ConnectionID, ref bError);

                    cmd_Tax.Parameters.Clear();
                    #endregion
                }

                #endregion

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OUTSIDE_PROCESS_INFO_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }

            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

    }
}
