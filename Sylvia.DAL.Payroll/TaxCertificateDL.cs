/* Compamy: Milllennium Information Solution Limited
 * Author: Km Jarif
 * Comment Date: April, 23, 2009
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

//
using Sylvia.Common.Reports;
//

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for TaxCertificateDL.
    /// </summary>
    
    class TaxCertificateDL
    {
        public TaxCertificateDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                //For Chalan Number
                case ActionID.ACTION_CHALANNO_ADD:
                    return this._createChalanNo(inputDS);
                case ActionID.NA_ACTION_GET_CHALANNO:
                    return this._getChalanNoList(inputDS);
                case ActionID.ACTION_CHALANNO_DEL:
                    return this._deleteChalanNo(inputDS);
                case ActionID.ACTION_CHALANNO_UPD:
                    return this._updateChalanNo(inputDS);
                case ActionID.ACTION_DOES_CHALANNO_EXIST:
                    return this._doesChalanNoExist(inputDS);

                //For Tax Certificate
                case ActionID.ACTION_TAXCERTIFICATE_ADD:
                    return this._createTaxCertificate(inputDS);
                case ActionID.NA_ACTION_GET_TAXCERTIFICATE_LIST_ALL:
                    return this._getTaxCertificateListAll(inputDS);
                case ActionID.NA_ACTION_GET_TAXCERTIFICATE_LIST_BYID:
                    return this._getTaxCertificateListById(inputDS);

                //For Tax Certificate Report
                case ActionID.NA_ACTION_GET_TAXCERTIFICATE_RPT:
                    return this._getTaxCertificateForReport(inputDS);
                case ActionID.NA_ACTION_GET_CHALANLIST_BY_FISCALYEAR:
                    return this._getChalanListByFiscalYear(inputDS);

                //For Income Year List
                case ActionID.NA_ACTION_GET_INCOMEYEAR:
                    return this._getIncomeYearList(inputDS);
                case ActionID.NA_ACTION_GET_TAXCERTIFICATE_FORMAT3:
                    return this._getTaxCertificateForReport_Format3(inputDS);
                    
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }

        private DataSet _createChalanNo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ChalanNoDS cnDS = new ChalanNoDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateChalanNo");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cnDS.Merge(inputDS.Tables[cnDS.ChalanNo.TableName], false, MissingSchemaAction.Error);
            cnDS.AcceptChanges();

            foreach (ChalanNoDS.ChalanNoRow cn in cnDS.ChalanNo.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["Id"].Value = (object)genPK;
                //Chalan Number
                if (cn.IsChalanNoNull() == false)
                {
                    cmd.Parameters["ChalanNo"].Value = (object)cn.ChalanNo;
                }
                else
                {
                    cmd.Parameters["ChalanNo"].Value = null;
                }
                //Tax Param Id
                if (cn.IsTaxParamIdNull() == false)
                {
                    cmd.Parameters["TaxParamId"].Value = (object)cn.TaxParamId;
                }
                else
                {
                    cmd.Parameters["TaxParamId"].Value = null;
                }
                //Payment Day
                if (cn.IsPaymentDateNull() == false)
                {
                    cmd.Parameters["PaymentDate"].Value = (object)cn.PaymentDate;
                }
                else
                {
                    cmd.Parameters["PaymentDate"].Value = null;
                }
                //IsActive
                if (cn.IsIsActiveNull() == false)
                {
                    cmd.Parameters["IsActive"].Value = (object)cn.IsActive;
                }
                else
                {
                    cmd.Parameters["IsActive"].Value = null;
                }

                //Tax Chalan Amount ::Rony
                if (cn.IsChalanAmountNull() == false)
                {
                    cmd.Parameters["ChalanAmount"].Value = (object)cn.ChalanAmount;
                }
                else
                {
                    cmd.Parameters["ChalanAmount"].Value = null;
                }

                //Remarks
                if (cn.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)cn.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = null;
                }

                if (cn.IsSalaryDateNull() == false) cmd.Parameters["SalaryDate"].Value = (object)cn.SalaryDate;
                else cmd.Parameters["SalaryDate"].Value = null;
                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHALANNO_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteChalanNo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS IntegerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            IntegerDS.Merge(inputDS.Tables[IntegerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            IntegerDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteChalan");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataIntegerDS.DataInteger iValue in IntegerDS.DataIntegers)
            {
                if (iValue.IsIntegerValueNull() == false)
                {
                    cmd.Parameters["Id"].Value = (object)iValue.IntegerValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHALANNO_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getChalanNoList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetChalanList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(22 Sep 11)...
            #region Old...
            //ChalanNoPO cnPO = new ChalanNoPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, cnPO, cnPO.ChalanNo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_CHALANNO.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //cnPO.AcceptChanges();

            //ChalanNoDS cnDS = new ChalanNoDS();

            //if (cnPO.ChalanNo.Rows.Count > 0)
            //{
            //    foreach (ChalanNoPO.ChalanNoRow poCn in cnPO.ChalanNo.Rows)
            //    {
            //        ChalanNoDS.ChalanNoRow cn = cnDS.ChalanNo.NewChalanNoRow();
            //        if (poCn.IsChalanNoIdNull() == false)
            //        {
            //            cn.ChalanNoId = poCn.ChalanNoId;
            //        }
            //        if (poCn.IsChalanNoNull() == false)
            //        {
            //            cn.ChalanNo = poCn.ChalanNo;
            //        }
            //        if (poCn.IsTaxParamIdNull() == false)
            //        {
            //            cn.TaxParamId = poCn.TaxParamId;
            //        }
            //        if (poCn.IsPaymentDateNull() == false)
            //        {
            //            cn.PaymentDate = poCn.PaymentDate;
            //        }
            //        if (poCn.IsIsActiveNull() == false)
            //        {
            //            cn.IsActive = poCn.IsActive;
            //        }
            //        if (poCn.IsRemarksNull() == false)
            //        {
            //            cn.Remarks = poCn.Remarks;
            //        }

            //        cnDS.ChalanNo.AddChalanNoRow(cn);
            //        cnDS.AcceptChanges();
            //    }
            //}
            #endregion

            ChalanNoDS cnDS = new ChalanNoDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, cnDS, cnDS.ChalanNo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_CHALANNO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            cnDS.AcceptChanges();

            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(cnDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _updateChalanNo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ChalanNoDS cnDS = new ChalanNoDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateChalanNo");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cnDS.Merge(inputDS.Tables[cnDS.ChalanNo.TableName], false, MissingSchemaAction.Error);
            cnDS.AcceptChanges();


            foreach (ChalanNoDS.ChalanNoRow cn in cnDS.ChalanNo.Rows)
            {
                //Chalan Number
                if (cn.IsChalanNoNull() == false)
                {
                    cmd.Parameters["ChalanNo"].Value = (object)cn.ChalanNo;
                }
                else
                {
                    cmd.Parameters["ChalanNo"].Value = null;
                }
                //Tax Param Id
                if (cn.IsTaxParamIdNull() == false)
                {
                    cmd.Parameters["TaxParamId"].Value = (object)cn.TaxParamId;
                }
                else
                {
                    cmd.Parameters["TaxParamId"].Value = null;
                }
                //Payment Day
                if (cn.IsPaymentDateNull() == false)
                {
                    cmd.Parameters["PaymentDate"].Value = (object)cn.PaymentDate;
                }
                else
                {
                    cmd.Parameters["PaymentDate"].Value = null;
                }
                //IsActive
                if (cn.IsIsActiveNull() == false)
                {
                    cmd.Parameters["IsActive"].Value = (object)cn.IsActive;
                }
                else
                {
                    cmd.Parameters["IsActive"].Value = null;
                }

                //Tax Chalan Amount :: Rony
                if (cn.IsChalanAmountNull() == false)
                {
                    cmd.Parameters["ChalanAmount"].Value = (object)cn.ChalanAmount;
                }
                else
                {
                    cmd.Parameters["ChalanAmount"].Value = null;
                }

                //Remarks
                if (cn.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)cn.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = null;
                }
                if (cn.IsSalaryDateNull() == false) cmd.Parameters["SalaryDate"].Value = (object)cn.SalaryDate;
                else cmd.Parameters["SalaryDate"].Value = null;
                //Chalan No Id
                if (cn.IsChalanNoIdNull() == false)
                {
                    cmd.Parameters["Id"].Value = (object)cn.ChalanNoId;
                }
                else
                {
                    cmd.Parameters["Id"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHALANNO_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _doesChalanNoExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesChalanExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_CHALANNO_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createTaxCertificate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            TaxCertificateDS taxDS = new TaxCertificateDS();
            DBConnectionDS connDS = new DBConnectionDS();
                        
            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //For Delete Tax Certificate
            try
            {
                _deleteTaxCertificate(inputDS, connDS);
            }
            catch (Exception ex)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_TAXCERTIFICATE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateTaxCertificate");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            taxDS.Merge(inputDS.Tables[taxDS.TaxCertificate.TableName], false, MissingSchemaAction.Error);
            taxDS.AcceptChanges();

            foreach (TaxCertificateDS.TaxCertificateRow tax in taxDS.TaxCertificate.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["Id"].Value = (object)genPK;
                //Chalan No Id
                if (tax.IsChalanNoIdNull() == false)
                {
                    cmd.Parameters["ChalanNoId"].Value = (object)tax.ChalanNoId;
                }
                else
                {
                    cmd.Parameters["ChalanNoId"].Value = null;
                }
                //Employee Code
                if (tax.IsEmployeeCodeNull() == false)
                {
                    cmd.Parameters["EmployeeCode"].Value = (object)tax.EmployeeCode;
                }
                else
                {
                    cmd.Parameters["EmployeeCode"].Value = null;
                }
                //Adjustment
                if (tax.IsAdjustmentNull() == false)
                {
                    cmd.Parameters["Adjustment"].Value = (object)tax.Adjustment;
                }
                else
                {
                    cmd.Parameters["Adjustment"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TAXCERTIFICATE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteTaxCertificate(DataSet inputDS, DBConnectionDS connDS)
        {
            ErrorDS errDS = new ErrorDS();

            DataIntegerDS IntegerDS = new DataIntegerDS();
            IntegerDS.Merge(inputDS.Tables[IntegerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            IntegerDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteTaxCertificate");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataIntegerDS.DataInteger iValue in IntegerDS.DataIntegers)
            {
                if (iValue.IsIntegerValueNull() == false)
                {
                    cmd.Parameters["ChalanNoId"].Value = (object)iValue.IntegerValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TAXCERTIFICATE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getTaxCertificateListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetTaxCertificateListAll");

            cmd.Parameters["ChalanNoId"].Value =Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["MonthYearDate"].Value =Convert.ToDateTime(stringDS.DataStrings[1].StringValue); 
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(22 Sep 11)...
            #region Old...
            //TaxCertificatePO taxPO = new TaxCertificatePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, taxPO, taxPO.TaxCertificateList.TableName,  connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_TAXCERTIFICATE_LIST_ALL.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //taxPO.AcceptChanges();

            //TaxCertificateDS taxDS = new TaxCertificateDS();

            //if (taxPO.TaxCertificateList.Rows.Count > 0)
            //{
            //    foreach (TaxCertificatePO.TaxCertificateListRow poTax in taxPO.TaxCertificateList.Rows)
            //    {
            //        TaxCertificateDS.TaxCertificateListRow tax = taxDS.TaxCertificateList.NewTaxCertificateListRow();
            //        if (poTax.IsEmployeeIdNull() == false)
            //        {
            //            tax.EmployeeId = poTax.EmployeeId;
            //        }
            //        if (poTax.IsEmployeeCodeNull() == false)
            //        {
            //            tax.EmployeeCode = poTax.EmployeeCode;
            //        }
            //        if (poTax.IsEmployeeNameNull() == false)
            //        {
            //            tax.EmployeeName = poTax.EmployeeName;
            //        }
            //        if (poTax.IsChalanNoIdNull() == false)
            //        {
            //            tax.ChalanNoId = poTax.ChalanNoId;
            //        }
            //        if (poTax.IsChalanNoNull() == false)
            //        {
            //            tax.ChalanNo = poTax.ChalanNo;
            //        }
            //        if (poTax.IsPaymentDateNull() == false)
            //        {
            //            tax.PaymentDate = poTax.PaymentDate;
            //        }
            //        if (poTax.IsTaxCertificateIdNull() == false)
            //        {
            //            tax.TaxCertificateId = poTax.TaxCertificateId;
            //        }
            //        if (poTax.IsAdjustmentNull() == false)
            //        {
            //            tax.Adjustment = poTax.Adjustment;
            //        }
            //        if (poTax.IsTaxParamIdNull() == false)
            //        {
            //            tax.TaxParamId = poTax.TaxParamId;
            //        }
            //        if (poTax.IsFiscalYearNull() == false)
            //        {
            //            tax.FiscalYear = poTax.FiscalYear;
            //        }
            //        if (poTax.IsAssessmentYearNull() == false)
            //        {
            //            tax.AssessmentYear = poTax.AssessmentYear;
            //        }

            //        taxDS.TaxCertificateList.AddTaxCertificateListRow(tax);
            //        taxDS.AcceptChanges();
            //    }
            //}
            #endregion

            TaxCertificateDS taxDS = new TaxCertificateDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, taxDS, taxDS.TaxCertificateList.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TAXCERTIFICATE_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            taxDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(taxDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getTaxCertificateListById(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataIntegerDS IntegerDS = new DataIntegerDS();
            Int32 ChalanId = 0;

            IntegerDS.Merge(inputDS.Tables[IntegerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            IntegerDS.AcceptChanges();
            ChalanId = IntegerDS.DataIntegers[0].IntegerValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetTaxCertificateListById");

            cmd.Parameters["Id"].Value = ChalanId;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            TaxCertificatePO taxPO = new TaxCertificatePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              taxPO, taxPO.TaxCertificateList.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TAXCERTIFICATE_LIST_BYID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            taxPO.AcceptChanges();

            TaxCertificateDS taxDS = new TaxCertificateDS();

            if (taxPO.TaxCertificateList.Rows.Count > 0)
            {
                foreach (TaxCertificatePO.TaxCertificateListRow poTax in taxPO.TaxCertificateList.Rows)
                {
                    TaxCertificateDS.TaxCertificateListRow tax = taxDS.TaxCertificateList.NewTaxCertificateListRow();
                    if (poTax.IsEmployeeIdNull() == false)
                    {
                        tax.EmployeeId = poTax.EmployeeId;
                    }
                    if (poTax.IsEmployeeCodeNull() == false)
                    {
                        tax.EmployeeCode = poTax.EmployeeCode;
                    }
                    if (poTax.IsEmployeeNameNull() == false)
                    {
                        tax.EmployeeName = poTax.EmployeeName;
                    }
                    if (poTax.IsChalanNoIdNull() == false)
                    {
                        tax.ChalanNoId = poTax.ChalanNoId;
                    }
                    if (poTax.IsChalanNoNull() == false)
                    {
                        tax.ChalanNo = poTax.ChalanNo;
                    }
                    if (poTax.IsPaymentDateNull() == false)
                    {
                        tax.PaymentDate = poTax.PaymentDate;
                    }
                    if (poTax.IsTaxCertificateIdNull() == false)
                    {
                        tax.TaxCertificateId = poTax.TaxCertificateId;
                    }
                    if (poTax.IsAdjustmentNull() == false)
                    {
                        tax.Adjustment = poTax.Adjustment;
                    }
                    if (poTax.IsTaxParamIdNull() == false)
                    {
                        tax.TaxParamId = poTax.TaxParamId;
                    }
                    if (poTax.IsFiscalYearNull() == false)
                    {
                        tax.FiscalYear = poTax.FiscalYear;
                    }
                    if (poTax.IsAssessmentYearNull() == false)
                    {
                        tax.AssessmentYear = poTax.AssessmentYear;
                    }

                    taxDS.TaxCertificateList.AddTaxCertificateListRow(tax);
                    taxDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(taxDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getTaxCertificateForReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetTaxCertificateForReport");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(22 Sep 11)...
            #region Old...
            //TaxCertificatePO taxPO = new TaxCertificatePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, taxPO, taxPO.CertificateOfTaxRpt.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_TAXCERTIFICATE_RPT.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //taxPO.AcceptChanges();

            //rptTaxCertificateDS taxDS = new rptTaxCertificateDS();

            //if (taxPO.CertificateOfTaxRpt.Rows.Count > 0)
            //{
            //    foreach (TaxCertificatePO.CertificateOfTaxRptRow poTax in taxPO.CertificateOfTaxRpt.Rows)
            //    {
            //        rptTaxCertificateDS.CertificateOfTaxRow tax = taxDS.CertificateOfTax.NewCertificateOfTaxRow();
            //        if (poTax.IsEmployeeCodeNull() == false)
            //        {
            //            tax.EmployeeCode = poTax.EmployeeCode;
            //        }
            //        if (poTax.IsEmployeeNameNull() == false)
            //        {
            //            tax.EmployeeName = poTax.EmployeeName;
            //        }
            //        if (poTax.IsFiscalYearNull() == false)
            //        {
            //            tax.FiscalYear = poTax.FiscalYear;
            //        }
            //        if (poTax.IsAssessmentYearNull() == false)
            //        {
            //            tax.AssessmentYear = poTax.AssessmentYear;
            //        }
            //        if (poTax.IsTaxNull() == false)
            //        {
            //            tax.Tax = poTax.Tax;
            //        }
            //        if (poTax.IsPaymentDateNull() == false)
            //        {
            //            tax.PaymentDate = poTax.PaymentDate;
            //        }
            //        if (poTax.IsChalanNoNull() == false)
            //        {
            //            tax.ChalanNo = poTax.ChalanNo;
            //        }
            //        if (poTax.IsAdjustmentNull() == false)
            //        {
            //            tax.Adjustment = poTax.Adjustment;
            //        }

            //        taxDS.CertificateOfTax.AddCertificateOfTaxRow(tax);
            //        taxDS.AcceptChanges();
            //    }
            //}
            #endregion

            rptTaxCertificateDS taxDS = new rptTaxCertificateDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, taxDS, taxDS.CertificateOfTax.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TAXCERTIFICATE_RPT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            taxDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(taxDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getIncomeYearList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetIncomeYearList");
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetIncomeYearInfo");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(22 Sep 11)...
            #region Old...
            //IncomeYearPO icyPO = new IncomeYearPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, icyPO, icyPO.IncomeYear.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_INCOMEYEAR.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //icyPO.AcceptChanges();

            //IncomeYearDS icyDS = new IncomeYearDS();

            //if (icyPO.IncomeYear.Rows.Count > 0)
            //{
            //    foreach (IncomeYearPO.IncomeYearRow poIcy in icyPO.IncomeYear.Rows)
            //    {
            //        IncomeYearDS.IncomeYearRow icy = icyDS.IncomeYear.NewIncomeYearRow();
            //        if (poIcy.IsTaxParamIdNull() == false)
            //        {
            //            icy.TaxParamId = poIcy.TaxParamId;
            //        }
            //        if (poIcy.IsFiscalYearNull() == false)
            //        {
            //            icy.FiscalYear = poIcy.FiscalYear;
            //        }
            //        if (poIcy.IsAssessmentYearNull() == false)
            //        {
            //            icy.AssessmentYear = poIcy.AssessmentYear;
            //        }

            //        icyDS.IncomeYear.AddIncomeYearRow(icy);
            //        icyDS.AcceptChanges();
            //    }
            //}
            #endregion

            IncomeYearDS icyDS = new IncomeYearDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, icyDS, icyDS.IncomeYear.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_INCOMEYEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            icyDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(icyDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getChalanListByFiscalYear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            Int32 FiscalYearId = 0;
            DateTime PaymentDate = new DateTime();

            DataIntegerDS IntegerDS = new DataIntegerDS();
            IntegerDS.Merge(inputDS.Tables[IntegerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            IntegerDS.AcceptChanges();
            FiscalYearId = IntegerDS.DataIntegers[0].IntegerValue;

            DataDateDS DateDS = new DataDateDS();
            DateDS.Merge(inputDS.Tables[DateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            DateDS.AcceptChanges();
            PaymentDate = DateDS.DataDates[0].DateValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetChalanListByFiscalYear");

            cmd.Parameters["FiscalYearId"].Value = FiscalYearId;
            cmd.Parameters["PaymentDate"].Value = PaymentDate;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            TaxCertificatePO taxPO = new TaxCertificatePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, taxPO, taxPO.TaxCertificateList.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_CHALANLIST_BY_FISCALYEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            taxPO.AcceptChanges();

            TaxCertificateDS taxDS = new TaxCertificateDS();

            if (taxPO.TaxCertificateList.Rows.Count > 0)
            {
                foreach (TaxCertificatePO.TaxCertificateListRow poTax in taxPO.TaxCertificateList.Rows)
                {
                    TaxCertificateDS.TaxCertificateListRow tax = taxDS.TaxCertificateList.NewTaxCertificateListRow();
                    if (poTax.IsEmployeeCodeNull() == false)
                    {
                        tax.EmployeeCode = poTax.EmployeeCode;
                    }
                    if (poTax.IsEmployeeNameNull() == false)
                    {
                        tax.EmployeeName = poTax.EmployeeName;
                    }
                    if (poTax.IsChalanNoNull() == false)
                    {
                        tax.ChalanNo = poTax.ChalanNo;
                    }
                    if (poTax.IsPaymentDateNull() == false)
                    {
                        tax.PaymentDate = poTax.PaymentDate;
                    }

                    taxDS.TaxCertificateList.AddTaxCertificateListRow(tax);
                    taxDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(taxDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getTaxCertificateForReport_Format3(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetTaxCertificateReport_Format3");
            if (cmd == null)  return UtilDL.GetCommandNotFound();

            cmd.Parameters["FiscalYear1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["FiscalYear2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region  Common Data

            rptTaxCertificateDS taxDS = new rptTaxCertificateDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, taxDS, taxDS.CertificateOfTax.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            
            
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TAXCERTIFICATE_FORMAT3.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            taxDS.AcceptChanges();
            #endregion

            #region Allowances Data
            bError = false;
            nRowAffected = -1;
            cmd.Dispose();
            cmd = DBCommandProvider.GetDBCommand("GetTaxCertificateReport_Format3_Allowances");

            cmd.Parameters["FiscalYear1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["FiscalYear2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;

            cmd.Parameters["FiscalYear3"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["FiscalYear4"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode3"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode4"].Value = stringDS.DataStrings[1].StringValue;

            adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            nRowAffected = ADOController.Instance.Fill(adapter, taxDS, taxDS.TaxableAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TAXCERTIFICATE_FORMAT3.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            taxDS.AcceptChanges();
            #endregion

            #region Tax Chalan Data
            bError = false;
            nRowAffected = -1;
            cmd.Dispose();
            cmd = DBCommandProvider.GetDBCommand("GetTaxChalanDetails");

            cmd.Parameters["FiscalYear1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;

            adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            nRowAffected = ADOController.Instance.Fill(adapter, taxDS, taxDS.TaxChalan.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TAXCERTIFICATE_FORMAT3.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            taxDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(taxDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
    }
}
