
/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: May 22, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
  /// <summary>
  /// Summary description for DepartmentDL.
  /// </summary>
  public class SalaryReviewDL
  {
    public SalaryReviewDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }
    
    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {
        case ActionID.ACTION_COMPERBONUS_ADD:
          return _createCompantPerfBonus(inputDS);
        case ActionID.ACTION_PAREMPLOYEE_ADD:
          return _createPAREmployee(inputDS);
        case ActionID.ACTION_PARATING_ADD:
          return _createPARating( inputDS );
        case ActionID.ACTION_PAPARFORANGE_ADD:
          return _createEmployeePARange(inputDS);
        case ActionID.ACTION_COMPERBONUS_UPD:
          return this._updateCompantPerfBonus(inputDS);
        case ActionID.ACTION_PARATING_UPD:
          return this._updatePARating( inputDS );
        case ActionID.ACTION_PAREMPLOYEE_UPD:
          return this._updatePAREmployee(inputDS);
        case ActionID.ACTION_PAPARFORANGE_UPD:
          return this._updateEmployeePARange(inputDS);
        case ActionID.NA_ACTION_Q_ALL_COMPERBONUS:
          return _getCompanyPerfBonusList(inputDS);
      case ActionID.NA_ACTION_Q_ALL_PARATING:
          return _getPARatingList(inputDS);
        case ActionID.ACTION_PARATING_DEL:
          return this._deletePARating( inputDS );
        case ActionID.ACTION_DOES_PARATING_EXIST:
          return this._doesPARatingExist( inputDS );
        case ActionID.ACTION_DOES_COMPERBONUS_EXIST:
          return this._doesCompanyPerfBonusExist(inputDS);
        case ActionID.ACTION_DOES_PAREMPLOYEE_EXIST:
          return this._doesPAREmployeeExist(inputDS);
       case ActionID.ACTION_DOES_PAPARFORANGE_EXIST:
          return this._doesEmployeePARangeExist(inputDS);
      case ActionID.NA_ACTION_Q_ALL_PAPARFORANGE:
          return this._getPAPerfRangeList(inputDS);
      case ActionID.NA_ACTION_Q_ALL_PAREMPLOYEE:
          return this._getPAREmployeeList(inputDS);
       
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
    private DataSet _doesPAREmployeeExist(DataSet inputDS)
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
      stringDS.AcceptChanges();
      DataDateDS dateDS = new DataDateDS();
      dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
      dateDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesPAREmployeeExist");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if (stringDS.DataStrings[0].IsStringValueNull() == false)
      {
        cmd.Parameters["EmployeeId"].Value = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
      }
      if (dateDS.DataDates[0].IsDateValueNull() == false)
      {
        cmd.Parameters["YearDate"].Value = dateDS.DataDates[0].DateValue;
      }
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

      if (bError == true)
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;
      }

      boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge(boolDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();
      return returnDS;
    }
    private DataSet _doesPARatingExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesPARatingExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
        cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    }
    private DataSet _doesEmployeePARangeExist(DataSet inputDS)
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      
      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesPAPerfRangeExist");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

      if (bError == true)
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;
      }

      boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge(boolDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();
      return returnDS;
    }
    private DataSet _doesCompanyPerfBonusExist(DataSet inputDS)
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      DataDateDS dateDS = new DataDateDS();
      dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
      dateDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesCompPerfBonusExist");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      if (dateDS.DataDates[0].IsDateValueNull() == false)
      {
        cmd.Parameters["YearDate"].Value = dateDS.DataDates[0].DateValue;
      }
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

      if (bError == true)
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;
      }

      boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge(boolDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();
      return returnDS;
    }
    private DataSet _updateCompantPerfBonus(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      CompanyPerfBonusDS comBonusDS = new CompanyPerfBonusDS();
      DBConnectionDS connDS = new DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateComPerfBonus");
      if (cmd == null)
        return UtilDL.GetCommandNotFound();

      comBonusDS.Merge(inputDS.Tables[comBonusDS.CompanyPerfBonus.TableName], false, MissingSchemaAction.Error);
      comBonusDS.AcceptChanges();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();
      foreach (CompanyPerfBonusDS.CompanyPerfBonusRow bonus in comBonusDS.CompanyPerfBonus)
      {
        if (bonus.IsEmployeeCategoryNull() == false)
        {
          cmd.Parameters["EmployeeCaregory"].Value = bonus.EmployeeCategory;
        }
        else
        {
          cmd.Parameters["EmployeeCaregory"].Value = null;
        }
        if (bonus.IsYearDateNull() == false)
        {
          cmd.Parameters["YearDate"].Value = bonus.YearDate;
        }
        else
        {
          cmd.Parameters["YearDate"].Value = null;
        }
        if (bonus.IsPercentageNull() == false)
        {
          cmd.Parameters["Percentage"].Value = bonus.Percentage;
        }
        else
        {
          cmd.Parameters["Percentage"].Value = null;
        }
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_COMPERBONUS_UPD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _updateEmployeePARange(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      PAPerfRangeDS rangeDS = new PAPerfRangeDS();
      DBConnectionDS connDS = new DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdatePAPerfRange");
      if (cmd == null)
        return UtilDL.GetCommandNotFound();

      rangeDS.Merge(inputDS.Tables[rangeDS.PAPerfRange.TableName], false, MissingSchemaAction.Error);
      rangeDS.AcceptChanges();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();
      foreach (PAPerfRangeDS.PAPerfRangeRow range in rangeDS.PAPerfRange)
      {
        if (range.IsEmployeeCategoryNull() == false)
        {
          cmd.Parameters["EmployeeCategory"].Value = range.EmployeeCategory;
        }
        else
        {
          cmd.Parameters["EmployeeCategory"].Value = null;
        }
        if (range.IsIndiMinNull() == false)
        {
          cmd.Parameters["IndMin"].Value = range.IndiMin;
        }
        else
        {
          cmd.Parameters["IndMin"].Value = null;
        }
        if (range.IsIndiMaxNull() == false)
        {
          cmd.Parameters["IndMax"].Value = range.IndiMax;
        }
        else
        {
          cmd.Parameters["IndMax"].Value = null;
        }
        if (range.IsCompMinNull() == false)
        {
          cmd.Parameters["ComMin"].Value = range.CompMin;
        }
        else
        {
          cmd.Parameters["ComMin"].Value = null;
        }
        if (range.IsCompMaxNull() == false)
        {
          cmd.Parameters["ComMax"].Value = range.CompMax;
        }
        else
        {
          cmd.Parameters["ComMax"].Value = null;
        }


        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_PAPARFORANGE_ADD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _updatePAREmployee(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      PAREmployeeDS ratingDS = new PAREmployeeDS();
      DBConnectionDS connDS = new DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateEmployeePARating");
      if (cmd == null)
        return UtilDL.GetCommandNotFound();

      ratingDS.Merge(inputDS.Tables[ratingDS.PARatingEmployee.TableName], false, MissingSchemaAction.Error);
      ratingDS.AcceptChanges();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();
      foreach (PAREmployeeDS.PARatingEmployeeRow rating in ratingDS.PARatingEmployee)
      {

        if (rating.IsYearNull() == false)
        {
          cmd.Parameters["YearDate"].Value = rating.Year;
        }
        else
        {
          cmd.Parameters["YearDate"].Value = null;
        }
        if (rating.IsEmployeeCodeNull() == false)
        {
          cmd.Parameters["EmployeeId"].Value = UtilDL.GetEmployeeId(connDS, rating.EmployeeCode);
        }
        else
        {
          cmd.Parameters["EmployeeId"].Value = null;
        }
        if (rating.IsRatingCodeNull() == false)
        {
          cmd.Parameters["PARatingId"].Value = UtilDL.GetPARatingId(connDS, rating.RatingCode);
        }
        else
        {
          cmd.Parameters["PARatingId"].Value = null;
        }

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_PAREMPLOYEE_UPD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _createPAREmployee(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      PAREmployeeDS ratingDS = new PAREmployeeDS();
      DBConnectionDS connDS = new DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateEmployeePARating");
      if (cmd == null)
        return UtilDL.GetCommandNotFound();

      ratingDS.Merge(inputDS.Tables[ratingDS.PARatingEmployee.TableName], false, MissingSchemaAction.Error);
      ratingDS.AcceptChanges();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();
      foreach (PAREmployeeDS.PARatingEmployeeRow rating in ratingDS.PARatingEmployee)
      {
        
        if (rating.IsYearNull() == false)
        {
          cmd.Parameters["YearDate"].Value = rating.Year;
        }
        else
        {
          cmd.Parameters["YearDate"].Value = null;
        }
        if (rating.IsEmployeeCodeNull() == false)
        {
          cmd.Parameters["EmployeeId"].Value = UtilDL.GetEmployeeId(connDS, rating.EmployeeCode);
        }
        else
        {
          cmd.Parameters["EmployeeId"].Value = null;
        }
        if (rating.IsRatingCodeNull() == false)
        {
          cmd.Parameters["PARatingId"].Value =  UtilDL.GetPARatingId(connDS, rating.RatingCode);
        }
        else
        {
          cmd.Parameters["PARatingId"].Value = null;
        }
       
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_PAREMPLOYEE_ADD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _createEmployeePARange(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      
      PAPerfRangeDS rangeDS = new PAPerfRangeDS();
      DBConnectionDS connDS = new DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreatePerfRange");
      if (cmd == null)
        return UtilDL.GetCommandNotFound();

      rangeDS.Merge(inputDS.Tables[rangeDS.PAPerfRange.TableName], false, MissingSchemaAction.Error);
      rangeDS.AcceptChanges();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();
      foreach (PAPerfRangeDS.PAPerfRangeRow range in rangeDS.PAPerfRange)
      {
        if (range.IsEmployeeCategoryNull() == false)
        {
          cmd.Parameters["EmployeeCategory"].Value = range.EmployeeCategory;
        }
        else
        {
          cmd.Parameters["EmployeeCategory"].Value = null;
        }
        if (range.IsIndiMinNull() == false)
        {
          cmd.Parameters["IndMin"].Value = range.IndiMin;
        }
        else
        {
          cmd.Parameters["IndMin"].Value = null;
        }
        if (range.IsIndiMaxNull() == false)
        {
          cmd.Parameters["IndMax"].Value = range.IndiMax;
        }
        else
        {
          cmd.Parameters["IndMax"].Value = null;
        }
        if (range.IsCompMinNull() == false)
        {
          cmd.Parameters["ComMin"].Value = range.CompMin;
        }
        else
        {
          cmd.Parameters["ComMin"].Value = null;
        }
        if (range.IsCompMaxNull() == false)
        {
          cmd.Parameters["ComMax"].Value = range.CompMax;
        }
        else
        {
          cmd.Parameters["ComMax"].Value = null;
        }


        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_PAPARFORANGE_ADD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _createCompantPerfBonus(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      CompanyPerfBonusDS comBonusDS = new CompanyPerfBonusDS();
      DBConnectionDS connDS = new DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateComPerfBonus");
      if (cmd == null)
        return UtilDL.GetCommandNotFound();

      comBonusDS.Merge(inputDS.Tables[comBonusDS.CompanyPerfBonus.TableName], false, MissingSchemaAction.Error);
      comBonusDS.AcceptChanges();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();
      foreach (CompanyPerfBonusDS.CompanyPerfBonusRow bonus in comBonusDS.CompanyPerfBonus)
      {
        if (bonus.IsEmployeeCategoryNull() == false)
        {
          cmd.Parameters["EmployeeCaregory"].Value = bonus.EmployeeCategory;
        }
        else
        {
          cmd.Parameters["EmployeeCaregory"].Value = null;
        }
        if (bonus.IsYearDateNull() == false)
        {
          cmd.Parameters["YearDate"].Value = bonus.YearDate;
        }
        else
        {
          cmd.Parameters["YearDate"].Value = null;
        }
        if (bonus.IsPercentageNull() == false)
        {
          cmd.Parameters["Percentage"].Value = bonus.Percentage;
        }
        else
        {
          cmd.Parameters["Percentage"].Value = null;
        }
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_COMPERBONUS_ADD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _createPARating(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      PARatingDS ratingDS = new PARatingDS();
      DBConnectionDS connDS = new  DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreatePARating");
      if( cmd == null )
         return UtilDL.GetCommandNotFound();
      
      ratingDS.Merge( inputDS.Tables[ ratingDS.PARating.TableName ], false, MissingSchemaAction.Error );
      ratingDS.AcceptChanges();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      foreach(PARatingDS.PARatingRow rating in ratingDS.PARating)
      {
        long genPK = IDGenerator.GetNextGenericPK();
        if( genPK == -1)
          return UtilDL.GetDBOperationFailed();

        cmd.Parameters["Id"].Value = genPK;
    
        if(rating.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = rating.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;
        }
        if(rating.IsNameNull()==false)
       {
        cmd.Parameters["Name"].Value = rating.Name;
        }
        else
        {
          cmd.Parameters["Name"].Value = null;
        }
        if (rating.IsSalaryIncrementNull() == false)
        {
          cmd.Parameters["Salary"].Value = rating.SalaryIncrement;
        }
        else
        {
          cmd.Parameters["Salary"].Value = null;
        }
        if (rating.IsBonusIncrementNull() == false)
        {
          cmd.Parameters["Bonus"].Value = rating.BonusIncrement;
        }
        else
        {
          cmd.Parameters["Bonus"].Value = null;
        }
        

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_PARATING_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _updatePARating(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();
      PARatingDS ratingDS = new PARatingDS();
      DBConnectionDS connDS = new DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdatePARating");
      if (cmd == null)
        return UtilDL.GetCommandNotFound();

      ratingDS.Merge(inputDS.Tables[ratingDS.PARating.TableName], false, MissingSchemaAction.Error);
      ratingDS.AcceptChanges();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();
      foreach (PARatingDS.PARatingRow rating in ratingDS.PARating)
      {
        if (rating.IsCodeNull() == false)
        {
          cmd.Parameters["Code"].Value = rating.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;
        }
        if (rating.IsNameNull() == false)
        {
          cmd.Parameters["Name"].Value = rating.Name;
        }
        else
        {
          cmd.Parameters["Name"].Value = null;
        }
        if (rating.IsSalaryIncrementNull() == false)
        {
          cmd.Parameters["Salary"].Value = rating.SalaryIncrement;
        }
        else
        {
          cmd.Parameters["Salary"].Value = null;
        }
        if (rating.IsBonusIncrementNull() == false)
        {
          cmd.Parameters["Bonus"].Value = rating.BonusIncrement;
        }
        else
        {
          cmd.Parameters["Bonus"].Value = null;
        }


        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_PARATING_ADD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    
    }

    private DataSet _deletePARating(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();


      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeletePARating");
      if( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
      
        cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue;
      }
        bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_PARATING_DEL.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }

      return errDS;  // return empty ErrorDS
    }

    private DataSet _getPAREmployeeList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPAREmployeeList");
      if (cmd == null)
      {

        Util.LogInfo("Command is null.");
        return UtilDL.GetCommandNotFound();
      }
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      DataDateDS dateDS = new DataDateDS();
      dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
      dateDS.AcceptChanges();

      if (dateDS.DataDates[0].IsDateValueNull() == false)
      {

        cmd.Parameters["YearDate"].Value = dateDS.DataDates[0].DateValue;
      }
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;
      PARatingEmployeePO ratingPO = new PARatingEmployeePO();
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter,
        ratingPO,
        ratingPO.PARatingEmployee.TableName,
        connDS.DBConnections[0].ConnectionID,
        ref bError);
      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_PAREMPLOYEE.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;

      }

      ratingPO.AcceptChanges();
      PAREmployeeDS ratingDS = new PAREmployeeDS();
      if (ratingPO.PARatingEmployee.Count > 0)
      {
        foreach (PARatingEmployeePO.PARatingEmployeeRow poRating in ratingPO.PARatingEmployee)
        {
          PAREmployeeDS.PARatingEmployeeRow rating = ratingDS.PARatingEmployee.NewPARatingEmployeeRow();

          if (poRating.IsEmployeeCodeNull() == false)
          {
            rating.EmployeeCode = poRating.EmployeeCode;
          }
          if (poRating.IsEmployeeNameNull() == false)
          {
            rating.EmployeeName = poRating.EmployeeName;
          }
          if (poRating.IsPARatingCodeNull() == false)
          {
            rating.RatingCode = poRating.PARatingCode;
          }
          if (poRating.IsPARatingNameNull() == false)
          {
            rating.RatingName = poRating.PARatingName;
          }

          ratingDS.PARatingEmployee.AddPARatingEmployeeRow(rating);
          ratingDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      DataSet returnDS = new DataSet();
      returnDS.Merge(ratingDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();
      return returnDS;

    }
    private DataSet _getPARatingList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      DBConnectionDS connDS = new  DBConnectionDS();      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPARatingList");
      if (cmd == null)
      {

        Util.LogInfo("Command is null.");
        return UtilDL.GetCommandNotFound();
      }
      if( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;
      PARatingPO ratingPO = new PARatingPO();
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter, 
        ratingPO, 
        ratingPO.PARating.TableName, 
        connDS.DBConnections[0].ConnectionID, 
        ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_PARATING.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }

      ratingPO.AcceptChanges();
      PARatingDS ratingDS = new PARatingDS();
      if ( ratingPO.PARating.Count > 0 )
      {
        foreach ( PARatingPO.PARatingRow poRating in ratingPO.PARating)
        {
          PARatingDS.PARatingRow rating = ratingDS.PARating.NewPARatingRow();
         
          if (poRating.IsCodeNull() == false )
          {
            rating.Code = poRating.Code;
          }
          if (poRating.IsNameNull() == false)
          {
            rating.Name = poRating.Name;
          }
          if (poRating.IsSalaryIncrementNull() == false)
          {
            rating.SalaryIncrement= poRating.SalaryIncrement;
          }
          if (poRating.IsBonusIncrementNull() == false)
          {
            rating.BonusIncrement = poRating.BonusIncrement;
          }

          ratingDS.PARating.AddPARatingRow( rating );
          ratingDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      DataSet returnDS = new DataSet();
      returnDS.Merge( ratingDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
   
    }
    private DataSet _getPAPerfRangeList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPerfRangeList");
      if (cmd == null)
      {

        Util.LogInfo("Command is null.");
        return UtilDL.GetCommandNotFound();
      }
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;
      PAPerfRangePO rangePO = new PAPerfRangePO();
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter,
        rangePO,
        rangePO.PAPerfRange.TableName,
        connDS.DBConnections[0].ConnectionID,
        ref bError);
      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_PAPARFORANGE.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;

      }

      rangePO.AcceptChanges();
      PAPerfRangeDS rangeDS = new PAPerfRangeDS();
      if (rangePO.PAPerfRange.Count > 0)
      {
        foreach (PAPerfRangePO.PAPerfRangeRow poRange in rangePO.PAPerfRange)
        {
          PAPerfRangeDS.PAPerfRangeRow range = rangeDS.PAPerfRange.NewPAPerfRangeRow();

          if (poRange.IsEmployeeCategoryNull() == false)
          {
            range.EmployeeCategory = poRange.EmployeeCategory;
          }
          if (poRange.IsIndiMinNull() == false)
          {
            range.IndiMin = poRange.IndiMin;
          }
          if (poRange.IsIndiMaxNull() == false)
          {
            range.IndiMax = poRange.IndiMax;
          }
          if (poRange.IsCompMinNull() == false)
          {
            range.CompMin = poRange.CompMin;
          }
          if (poRange.IsCompMaxNull() == false)
          {
            range.CompMax = poRange.CompMax;
          }

          rangeDS.PAPerfRange.AddPAPerfRangeRow(range);
          rangeDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      DataSet returnDS = new DataSet();
      returnDS.Merge(rangeDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();
      return returnDS;

    }
    private DataSet _getCompanyPerfBonusList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetComPerfBonusList");
      if (cmd == null)
      {

        Util.LogInfo("Command is null.");
        return UtilDL.GetCommandNotFound();
      }
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      DataDateDS dateDS = new DataDateDS();
      dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
      dateDS.AcceptChanges();

      if (dateDS.DataDates[0].IsDateValueNull() == false)
      {

        cmd.Parameters["YearDate"].Value = dateDS.DataDates[0].DateValue;
      }
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;
      CompanyPerfBonusPO comBonusPO = new CompanyPerfBonusPO();
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter,
        comBonusPO,
        comBonusPO.CompanyPerfBonus.TableName,
        connDS.DBConnections[0].ConnectionID,
        ref bError);
      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_COMPERBONUS.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;

      }

      comBonusPO.AcceptChanges();
      CompanyPerfBonusDS comBonusDS = new CompanyPerfBonusDS();
      if (comBonusPO.CompanyPerfBonus.Count > 0)
      {
        foreach (CompanyPerfBonusPO.CompanyPerfBonusRow poComBonus in comBonusPO.CompanyPerfBonus)
        {
          CompanyPerfBonusDS.CompanyPerfBonusRow bonus = comBonusDS.CompanyPerfBonus.NewCompanyPerfBonusRow();

          if (poComBonus.IsYearDateNull() == false)
          {
            bonus.YearDate = poComBonus.YearDate;
          }
          if (poComBonus.IsEmployeeCategoryNull() == false)
          {
            bonus.EmployeeCategory = poComBonus.EmployeeCategory;
          }
          if (poComBonus.IsPercentageNull() == false)
          {
            bonus.Percentage = poComBonus.Percentage;
          }

          comBonusDS.CompanyPerfBonus.AddCompanyPerfBonusRow(bonus);
          comBonusDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      DataSet returnDS = new DataSet();
      returnDS.Merge(comBonusDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();
      return returnDS;

    }

  }
}
