/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: April 25, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class SystemDL
    {
        public SystemDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_ACTION_SYSTEM_DATETIME_GET:
                    return _getOracleDateTime(inputDS);
                case ActionID.ACTION_SYSTEMINFO_ADD:
                    return _createSystemInfo(inputDS);
                case ActionID.ACTION_SYSTEMINFO_GET:
                    return _getSystemInfo(inputDS);

                case ActionID.NA_ACTION_SYSTEMINFO_GET:
                    return this._getSystemInfo(inputDS);


                case ActionID.ACTION_NEWS_ADD:
                    return this._saveNewsPost(inputDS);
                case ActionID.NA_ACTION_NEWS_GET_ALL:
                    return this._getAllNewsPosts(inputDS);

                case ActionID.ACTION_NEWS_DELETE:
                    return this._DeleteNewsPost(inputDS);
                case ActionID.NA_GET_SYSTEMINFO_CONF_FOR_EMP_TRANSFER:
                    return this._getSystemInfoForEmployeeTransfer(inputDS);

                

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getAllNewsPosts(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllNews");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["p_IsActive"].Value =Convert.ToBoolean(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["p_Date"].Value =Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["p_Date2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            NewsDS newsDS = new NewsDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, newsDS, newsDS.NEWS.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SYSTEMINFO_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(newsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _saveNewsPost(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            NewsDS newsDS = new NewsDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region OLD
            //if (_deleteSystemInfo(connDS) == true)
            //{
            //    OleDbCommand cmd = DBCommandProvider.GetDBCommand("SaveNewsPost");
            //    if (cmd == null)
            //    {
            //        return UtilDL.GetCommandNotFound();
            //    }
            //    newsDS.Merge(inputDS.Tables[newsDS.NEWS.TableName], false, MissingSchemaAction.Error);
            //    newsDS.AcceptChanges();

            //    //foreach (NewsDS.NEWSRow news in newsDS.NEWS)
            //    for (int i = 0; i < newsDS.NEWS.Count; i++)
            //    {
            //        cmd.Parameters["ID"].Value = newsDS.NEWS[i].ID;
            //        cmd.Parameters["HEADER"].Value = newsDS.NEWS[i].HEADER;

            //        cmd.Parameters["DETAILS"].Value = newsDS.NEWS[i].IsDETAILSNull() == false
            //           ? (object)newsDS.NEWS[i].DETAILS
            //           : DBNull.Value;

            //        cmd.Parameters["ATTACHMENT"].Value = newsDS.NEWS[i].IsATTACHMENTNull() == false
            //            ? (object)newsDS.NEWS[i].ATTACHMENT
            //            : DBNull.Value;

            //        cmd.Parameters["VIEWSTARTDATE"].Value = newsDS.NEWS[i].IsVIEWSTARTDATENull() == false
            //            ? (object)newsDS.NEWS[i].VIEWSTARTDATE
            //            : DBNull.Value;

            //        cmd.Parameters["VIEWENDDATE"].Value = newsDS.NEWS[i].IsVIEWENDDATENull() == false
            //            ? (object)newsDS.NEWS[i].VIEWENDDATE
            //            : DBNull.Value;

            //        cmd.Parameters["POSTINGUSER"].Value = newsDS.NEWS[i].IsPOSTINGUSERNull() == false
            //            ? (object)newsDS.NEWS[i].POSTINGUSER
            //            : DBNull.Value;


            //        bool bError = false;
            //        int nRowAffected = -1;
            //        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            //        if (bError == true)
            //        {
            //            ErrorDS.Error err = errDS.Errors.NewError();
            //            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //            err.ErrorInfo1 = ActionID.ACTION_SYSTEMINFO_ADD.ToString();
            //            errDS.Errors.AddError(err);
            //            errDS.AcceptChanges();
            //            return errDS;
            //        }
            //    }
            //}
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_NEWS_ACTION_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            newsDS.Merge(inputDS.Tables[newsDS.NEWS.TableName], false, MissingSchemaAction.Error);
            newsDS.AcceptChanges();
            foreach (NewsDS.NEWSRow row in newsDS.NEWS)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("ID", genPK);
                cmd.Parameters.AddWithValue("HEADER", row.HEADER);

                if (!row.IsDETAILSNull()) cmd.Parameters.AddWithValue("DETAILS", row.DETAILS);
                else cmd.Parameters.AddWithValue("DETAILS", DBNull.Value);

                if (!row.IsATTACHMENTNull()) cmd.Parameters.AddWithValue("ATTACHMENT", row.ATTACHMENT);
                else cmd.Parameters.AddWithValue("ATTACHMENT", DBNull.Value);

                if (!row.IsVIEWSTARTDATENull()) cmd.Parameters.AddWithValue("VIEWSTARTDATE", row.VIEWSTARTDATE);
                else cmd.Parameters.AddWithValue("VIEWSTARTDATE", DBNull.Value);

                if (!row.IsVIEWENDDATENull()) cmd.Parameters.AddWithValue("VIEWENDDATE", row.VIEWENDDATE);
                else cmd.Parameters.AddWithValue("VIEWENDDATE", DBNull.Value);

                if (!row.IsPOSTINGUSERNull()) cmd.Parameters.AddWithValue("POSTINGUSER", row.POSTINGUSER);
                else cmd.Parameters.AddWithValue("POSTINGUSER", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_NEWS_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getSystemInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSystemInfo");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            SystemInfoDS infoDS = new SystemInfoDS();
            SystemInfoPO infoPO = new SystemInfoPO();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.SystemInformation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SYSTEMINFO_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #region Rony:: OLD ::Updated:: 05 Jun 2016
            //nRowAffected = ADOController.Instance.Fill(adapter, infoPO, infoPO.SystemInformation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.ACTION_SYSTEMINFO_GET.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}

            //infoPO.AcceptChanges();
            //SystemInfoDS infoDS = new SystemInfoDS();
            //if (infoPO.SystemInformation.Rows.Count > 0)
            //{
            //    foreach (SystemInfoPO.SystemInformationRow poInfo in infoPO.SystemInformation)
            //    {
            //        SystemInfoDS.SystemInformationRow info = infoDS.SystemInformation.NewSystemInformationRow();

            //        if (poInfo.IsCompanyCodeNull() == false)
            //        {
            //            info.CompanyCode = poInfo.CompanyCode;
            //        }
            //        if (poInfo.IsCompanyNameNull() == false)
            //        {
            //            info.CompanyName = poInfo.CompanyName;
            //        }
            //        if (poInfo.IsAddressNull() == false)
            //        {
            //            info.Address = poInfo.Address;
            //        }
            //        if (poInfo.IsTelephoneNull() == false)
            //        {
            //            info.Telephone = poInfo.Telephone;
            //        }
            //        if (poInfo.IsFaxNull() == false)
            //        {
            //            info.Fax = poInfo.Fax;
            //        }
            //        if (poInfo.IsUrlNull() == false)
            //        {
            //            info.Url = poInfo.Url;
            //        }
            //        if (poInfo.IsEmailNull() == false)
            //        {
            //            info.Email = poInfo.Email;
            //        }
            //        if (poInfo.IsHoursPerMonthNull() == false)
            //        {
            //            info.HoursPerMonth = poInfo.HoursPerMonth;
            //        }
            //        if (poInfo.IsOverTimeRatioNull() == false)
            //        {
            //            info.OverTimeRatio = poInfo.OverTimeRatio;
            //        }
            //        if (poInfo.IsOCSRateNull() == false)
            //        {
            //            info.OCSRate = poInfo.OCSRate;
            //        }
            //        if (poInfo.IsDayOfYearNull() == false)
            //        {
            //            info.DayOfYear = poInfo.DayOfYear;
            //        }
            //        infoDS.SystemInformation.AddSystemInformationRow(info);
            //        infoDS.AcceptChanges();
            //    }
            //}
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(infoDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getSystemInfoForEmployeeTransfer(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSystemInfoDataForTransfer");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            SystemInfoDS infoDS = new SystemInfoDS();
            SystemInfoPO infoPO = new SystemInfoPO();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.SystemInformation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_SYSTEMINFO_CONF_FOR_EMP_TRANSFER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(infoDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private bool _deleteSystemInfo(DBConnectionDS connDS)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteSystemInfo");
            if (cmd == null)
            {
                UtilDL.GetCommandNotFound();
                return false;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                return false;
            }

            return true;  // return empty ErrorDS
        }
        private DataSet _getOracleDateTime(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSystemDateTimeForOracle");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            SystemDatePO datePO = new SystemDatePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              datePO,
              datePO.SystemDates.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_SYSTEM_DATETIME_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            datePO.AcceptChanges();
            DataDateDS dateDS = new DataDateDS();
            if (datePO.SystemDates.Count > 0)
            {
                DateTime dt = datePO.SystemDates[0].SysDate;
                dateDS.DataDates.AddDataDate(datePO.SystemDates[0].SysDate);

            }
            dateDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(dateDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _createSystemInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            SystemInfoDS systemInfoDS = new SystemInfoDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            if (_deleteSystemInfo(connDS) == true)
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateSystemInfo");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
                systemInfoDS.Merge(inputDS.Tables[systemInfoDS.SystemInformation.TableName], false, MissingSchemaAction.Error);
                systemInfoDS.AcceptChanges();

                foreach (SystemInfoDS.SystemInformationRow info in systemInfoDS.SystemInformation)
                {

                    if (info.IsCompanyCodeNull() == false)
                    {
                        cmd.Parameters["Code"].Value = info.CompanyCode;
                    }
                    else
                    {
                        cmd.Parameters["Code"].Value = DBNull.Value;
                    }
                    if (info.IsCompanyNameNull() == false)
                    {
                        cmd.Parameters["Name"].Value = info.CompanyName;
                    }
                    else
                    {
                        cmd.Parameters["Name"].Value = DBNull.Value;
                    }
                    if (info.IsAddressNull() == false)
                    {
                        cmd.Parameters["Address"].Value = info.Address;
                    }
                    else
                    {
                        cmd.Parameters["Address"].Value = DBNull.Value;
                    }
                    if (info.IsTelephoneNull() == false)
                    {
                        cmd.Parameters["Telephone"].Value = info.Telephone;
                    }
                    else
                    {
                        cmd.Parameters["Telephone"].Value = DBNull.Value;
                    }
                    if (info.IsFaxNull() == false)
                    {
                        cmd.Parameters["Fax"].Value = info.Fax;
                    }
                    else
                    {
                        cmd.Parameters["Fax"].Value = DBNull.Value;
                    }
                    if (info.IsUrlNull() == false)
                    {
                        cmd.Parameters["Url"].Value = info.Url;
                    }
                    else
                    {
                        cmd.Parameters["Url"].Value = DBNull.Value;
                    }
                    if (info.IsEmailNull() == false)
                    {
                        cmd.Parameters["Email"].Value = info.Email;
                    }
                    else
                    {
                        cmd.Parameters["Email"].Value = DBNull.Value;
                    }
                    if (info.IsHoursPerMonthNull() == false)
                    {
                        cmd.Parameters["HourPerMonth"].Value = info.HoursPerMonth;
                    }
                    else
                    {
                        cmd.Parameters["HourPerMonth"].Value = DBNull.Value;
                    }
                    if (info.IsOverTimeRatioNull() == false)
                    {
                        cmd.Parameters["OverTimeRatio"].Value = info.OverTimeRatio;
                    }
                    else
                    {
                        cmd.Parameters["OverTimeRatio"].Value = DBNull.Value;
                    }

                    if (!info.IsOCSRateNull()) cmd.Parameters["OCSRate"].Value = info.OCSRate;
                    else cmd.Parameters["OCSRate"].Value = DBNull.Value;

                    //Rony :: 05 June 2016
                    if (!info.IsCountryIDNull()) cmd.Parameters["p_CountryID"].Value = info.CountryID;
                    else cmd.Parameters["p_OrganizationTypeID"].Value = System.DBNull.Value;

                    if (!info.IsStatuary_ComplianceNull()) cmd.Parameters["p_Statuary_Compliance"].Value = info.Statuary_Compliance;
                    else cmd.Parameters["p_Statuary_Compliance"].Value = System.DBNull.Value;

                    if (!info.IsOrganizationTypeIDNull()) cmd.Parameters["p_OrganizationTypeID"].Value = info.OrganizationTypeID;
                    else cmd.Parameters["p_OrganizationTypeID"].Value = System.DBNull.Value;

                    if (!info.IsPinCodeNull()) cmd.Parameters["p_PinCode"].Value = info.PinCode;
                    else cmd.Parameters["p_PinCode"].Value = System.DBNull.Value;

                    if (!info.IsBin_NoNull()) cmd.Parameters["p_Bin_No"].Value = info.Bin_No;
                    else cmd.Parameters["p_Bin_No"].Value = System.DBNull.Value;

                    if (!info.IsTin_NoNull()) cmd.Parameters["p_Tin_No"].Value = info.Tin_No;
                    else cmd.Parameters["p_Tin_No"].Value = System.DBNull.Value;

                    if (!info.IsVat_NoNull()) cmd.Parameters["p_Vat_No"].Value = info.Vat_No;
                    else cmd.Parameters["p_Vat_No"].Value = System.DBNull.Value;

                    if (!info.IsIdentification_NoNull()) cmd.Parameters["p_Identification_No"].Value = info.Identification_No;
                    else cmd.Parameters["p_Identification_No"].Value = System.DBNull.Value;

                    if (!info.IsCurrencyIDNull()) cmd.Parameters["p_CurrencyID"].Value = info.CurrencyID;
                    else cmd.Parameters["p_CurrencyID"].Value = System.DBNull.Value;

                    if (!info.IsMaintain_AccountNull()) cmd.Parameters["p_Maintain_Account"].Value = info.Maintain_Account;
                    else cmd.Parameters["p_Maintain_Account"].Value = System.DBNull.Value;

                    if (!info.IsFinancial_Year_FromNull()) cmd.Parameters["p_Financial_Year_From"].Value = info.Financial_Year_From;
                    else cmd.Parameters["p_Financial_Year_From"].Value = System.DBNull.Value;
                    //End

                    if (!info.IsKPI_MappedByNull()) cmd.Parameters["p_KPI_MappedBy"].Value = info.KPI_MappedBy;
                    else cmd.Parameters["p_KPI_MappedBy"].Value = System.DBNull.Value;
                    if (!info.IsSelfTNA_ApplicableNull()) cmd.Parameters["p_SelfTNA"].Value = info.SelfTNA_Applicable;
                    else cmd.Parameters["p_SelfTNA"].Value = System.DBNull.Value;

                    if (!info.IsLeaveApprovalNull()) cmd.Parameters["p_LeaveApproval"].Value = info.LeaveApproval;
                    else cmd.Parameters["p_LeaveApproval"].Value = System.DBNull.Value;

                    cmd.Parameters["ShowFundReceiveDetails"].Value = info.ShowFundReceiveDetails;

                    if (!info.IsPercentOfBasic_GrossNull()) cmd.Parameters["p_PercentOfBasic_Gross"].Value = info.PercentOfBasic_Gross;
                    else cmd.Parameters["p_PercentOfBasic_Gross"].Value = System.DBNull.Value;
                  
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_SYSTEMINFO_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }


        private DataSet _DeleteNewsPost(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            NewsDS newsDS = new NewsDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            //extract dbconnection
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_NEWS_DEACTIVE";
            cmd.CommandType = CommandType.StoredProcedure;

            newsDS.Merge(inputDS.Tables[newsDS.NEWS.TableName], false, MissingSchemaAction.Error);
            newsDS.AcceptChanges();

            foreach (NewsDS.NEWSRow row in newsDS.NEWS.Rows)
            {
                cmd.Parameters.AddWithValue("p_ID", row.ID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_NEWS_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.HEADER);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.HEADER);
            }
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
    }
}
