﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using System.Globalization;


namespace Sylvia.DAL.Payroll
{
    class FinalSettlementDL
    {

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_ACTION_GET_TERMINATION_TYPE_LIST:
                    return this._getTerminationType(inputDS);
                case ActionID.ACTION_TERMINATION_TYPE_CREATE:
                    return this._createTerminationType(inputDS);
                case ActionID.ACTION_TERMINATION_TYPE_UPDATE:
                    return this._updateTerminationType(inputDS);
                case ActionID.ACTION_TERMINATION_TYPE_DEL:
                    return this._deleteTerminationType(inputDS);
                case ActionID.NA_GET_FINAL_SETTLEMENT_APPROVAL_AUTHORITY_CONFIG_INFO:
                    return this._GetFinalSettlementApprovalAuthority(inputDS);
                case ActionID.ACTION_FINAL_SETTLEMENT_APPROVAL_AUTHORITY_CONFIG_CREATE:
                    return this._FinalSettlementApprovalAuthority_Create(inputDS);
                case ActionID.NA_GET_FINAL_SETTLEMENT_PARKING_EMP_LIST:
                    return this._GetFinalSettlementPerkingEmpList(inputDS);
                case ActionID.NA_GET_FINAL_SETTLEMENT_PARKING_EMP_CALCULATION_DETAILS:
                    return this._GetFinalSettlementPerkingEmpCalculationDetails(inputDS);
                case ActionID.ACTION_FINAL_SETTLEMENT_PARKING_EMP_LIST_UPDATE:
                    return this._FinalSettlementParkingEmpUpdate(inputDS);
                case ActionID.ACTION_FINAL_SETTLEMENT_PARKING_EMP_LIST_CREATE:
                    return this._FinalSettlementParkingEmpCreate(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_COUNT_FOR_SETTLEMENT:
                    return this._getEmployeeCountForSettlement(inputDS);

                case ActionID.NA_GET_SEPARATION_APPROVAL_AUTHORITY:
                    return this._getSeparationApprovalAuthority(inputDS);
                case ActionID.ACTION_SEPARATION_APPROVAL_PATH_CONFIG_SAVE:
                    return this._SeparationApprovalAuthority_Save(inputDS);
                case ActionID.NA_ACTION_SAVE_EMPLOYMENT_TERMINATION_CHECKLIST:
                    return this._saveTerminationChecklist(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_FOR_TERMINATION_CHECKLIST:
                    return this._getEmployeeForTerminationChecklist(inputDS);
                case ActionID.NA_ACTION_GET_PENDING_SEPARATION_REQUESTS:
                    return this._getPendingSeparationRequests(inputDS);
                case ActionID.NA_ACTION_APPROVE_SEPARATION_REQUEST:
                    return this._approveSeparationRequests(inputDS);
                case ActionID.NA_ACTION_SEPARATION_GET_ACTIVITY_LIST_ALL:
                    return this._getActivityListAll(inputDS);
                case ActionID.NA_ACTION_SEPARATION_APPROVAL_HISTORY:
                    return this._getSeparationApprovalHistory(inputDS);
                case ActionID.NA_ACTION_GET_FINAL_SETTLEMENT_REPORT:
                    return this._getFinalSettlementInforForReport(inputDS);
                    
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }


        private DataSet _getTerminationType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            FinalSettlementDS finalSettDS = new FinalSettlementDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetTerminationTypeList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, finalSettDS, finalSettDS.TerminationType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TERMINATION_TYPE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            finalSettDS.AcceptChanges();

            returnDS.Merge(finalSettDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createTerminationType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            FinalSettlementDS finalSettDS = new FinalSettlementDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TERMINATION_TYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            finalSettDS.Merge(inputDS.Tables[finalSettDS.TerminationType.TableName], false, MissingSchemaAction.Error);
            finalSettDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (FinalSettlementDS.TerminationTypeRow row in finalSettDS.TerminationType.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_TerminationTypeID", (object)genPK);
                }
                cmd.Parameters.AddWithValue("@p_TerminationTypeCode", row.TerminationTypeCode);
                cmd.Parameters.AddWithValue("@p_TerminationTypeName", row.TerminationTypeName);

                if (!row.IsRemarksNull())
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TERMINATION_TYPE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(finalSettDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateTerminationType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            FinalSettlementDS finalSettDS = new FinalSettlementDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TERMINATION_TYPE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            finalSettDS.Merge(inputDS.Tables[finalSettDS.TerminationType.TableName], false, MissingSchemaAction.Error);
            finalSettDS.AcceptChanges();

            foreach (FinalSettlementDS.TerminationTypeRow row in finalSettDS.TerminationType.Rows)
            {
                cmd.Parameters.AddWithValue("p_TerminationTypeCode", row.TerminationTypeCode);
                cmd.Parameters.AddWithValue("p_TerminationTypeName", row.TerminationTypeName);

                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                cmd.Parameters.AddWithValue("p_TerminationTypeID", row.TerminationTypeID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TERMINATION_TYPE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteTerminationType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TERMINATION_TYPE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_TerminationTypeCode", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_TERMINATION_TYPE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;  // return empty ErrorDS
        }

        private DataSet _GetFinalSettlementApprovalAuthority(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            FinalSettlementDS fsDS = new FinalSettlementDS();

            if (StringDS.DataStrings[0].StringValue == "FinalSettlementApprovalAuthorityAll")
            {
                #region Approval Path Config Info
                cmd = DBCommandProvider.GetDBCommand("GetFinalSettAuthorityActivityInfo");

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, fsDS, fsDS.FinalSettApprovalAuthorityWiseActivity.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_FINAL_SETTLEMENT_APPROVAL_AUTHORITY_CONFIG_INFO.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                fsDS.AcceptChanges();
                #endregion

                #region Get PriorityWise Activity
                //cmd.Dispose();
                //cmd.Parameters.Clear();

                //cmd = DBCommandProvider.GetDBCommand("GetTransferPriorityWiseActivityList");
                //if (cmd == null)
                //{
                //    return UtilDL.GetCommandNotFound();
                //}

                //adapter.SelectCommand = cmd;

                //bError = false;
                //nRowAffected = -1;
                //nRowAffected = ADOController.Instance.Fill(adapter, EmpHistDS, EmpHistDS.TransferPriotityWiseActivity.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                //if (bError)
                //{
                //    ErrorDS.Error err = errDS.Errors.NewError();
                //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //    err.ErrorInfo1 = ActionID.NA_GET_TRANSFER_APPROVAL_PATH_CONFIG_INFO.ToString();
                //    errDS.Errors.AddError(err);
                //    errDS.AcceptChanges();
                //    return errDS;

                //}
                //EmpHistDS.AcceptChanges();
                #endregion
            }


            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(fsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _FinalSettlementApprovalAuthority_Create(DataSet inputDS)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            FinalSettlementDS fsDS = new FinalSettlementDS();
            fsDS.Merge(inputDS.Tables[fsDS.FinalSettApprovalAuthorityWiseActivity.TableName], false, MissingSchemaAction.Error);
            fsDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_FINAL_SETT_APPROV_AUTH_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            cmd.Parameters.Clear();



            foreach (FinalSettlementDS.FinalSettApprovalAuthorityWiseActivityRow row in fsDS.FinalSettApprovalAuthorityWiseActivity.Rows)
            {

                cmd.CommandText = "PRO_FINAL_SET_APPRO_AUTH_CONF";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }


                string[] DevidedActivity;

                long pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_ConfigurationID", pKey);
                cmd.Parameters.AddWithValue("p_Authority", row.EmployeeCode);


                //#region Get LoginID
                //string[] Authority = row.Authority.Split(' ', '@');
                //if (Authority.Length == 1)
                //{
                //    cmd.Parameters.AddWithValue("p_IsLoginUserID", 1);
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("p_IsLoginUserID", 0);
                //}
                //#endregion






                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Dispose();
                cmd.Parameters.Clear();

                DevidedActivity = row.Activity.Split(' ', ',');
                AuthorityWiseActivityListCreate(pKey, DevidedActivity, connDS.DBConnections[0].ConnectionID);

            }

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FINAL_SETTLEMENT_APPROVAL_AUTHORITY_CONFIG_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet AuthorityWiseActivityListCreate(long ConfigurationID, string[] TransferActivityList, int ConnectionID)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();



            for (int i = 0; i < TransferActivityList.Length; i++)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_FINAL_SETT_AUTHO_WISE_ACT";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters.AddWithValue("p_Activity", TransferActivityList[i]);
                cmd.Parameters.AddWithValue("p_ConfigurationID", ConfigurationID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, ConnectionID, ref bError);
                cmd.Dispose();
                cmd.Parameters.Clear();
            }

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FINAL_SETTLEMENT_APPROVAL_AUTHORITY_CONFIG_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;

        }
        private DataSet _GetFinalSettlementPerkingEmpList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            FinalSettlementDS fsDS = new FinalSettlementDS();

            if (StringDS.DataStrings[0].StringValue == "FinalSettlementParkingEmpList")
            {
                if (StringDS.DataStrings[1].StringValue == "admin")
                {
                    cmd = DBCommandProvider.GetDBCommand("GetFinalSettEmplistParkingByAdmin");

                    cmd.Parameters["DepartmentCode1"].Value = (object)StringDS.DataStrings[2].StringValue;
                    cmd.Parameters["DepartmentCode2"].Value = (object)StringDS.DataStrings[2].StringValue;
                    cmd.Parameters["DesignationCode1"].Value = (object)StringDS.DataStrings[3].StringValue;
                    cmd.Parameters["DesignationCode2"].Value = (object)StringDS.DataStrings[3].StringValue;
                    cmd.Parameters["SiteCode1"].Value = (object)StringDS.DataStrings[4].StringValue;
                    cmd.Parameters["SiteCode2"].Value = (object)StringDS.DataStrings[4].StringValue;
                    cmd.Parameters["EmployeeCode1"].Value = (object)StringDS.DataStrings[5].StringValue;
                    cmd.Parameters["EmployeeCode2"].Value = (object)StringDS.DataStrings[5].StringValue;
                    cmd.Parameters["EntryDateFrom"].Value = DateTime.ParseExact(StringDS.DataStrings[7].StringValue, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                    cmd.Parameters["EntryDateTo"].Value = DateTime.ParseExact(StringDS.DataStrings[8].StringValue, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    cmd = DBCommandProvider.GetDBCommand("GetFinalSettEmplistParkingByLoginID");

                    cmd.Parameters["DepartmentCode1"].Value = (object)StringDS.DataStrings[2].StringValue;
                    cmd.Parameters["DepartmentCode2"].Value = (object)StringDS.DataStrings[2].StringValue;
                    cmd.Parameters["DesignationCode1"].Value = (object)StringDS.DataStrings[3].StringValue;
                    cmd.Parameters["DesignationCode2"].Value = (object)StringDS.DataStrings[3].StringValue;
                    cmd.Parameters["SiteCode1"].Value = (object)StringDS.DataStrings[4].StringValue;
                    cmd.Parameters["SiteCode2"].Value = (object)StringDS.DataStrings[4].StringValue;
                    cmd.Parameters["EmployeeCode1"].Value = (object)StringDS.DataStrings[5].StringValue;
                    cmd.Parameters["EmployeeCode2"].Value = (object)StringDS.DataStrings[5].StringValue;
                    cmd.Parameters["LoginID1"].Value = (object)StringDS.DataStrings[6].StringValue;
                    cmd.Parameters["LoginID2"].Value = (object)StringDS.DataStrings[6].StringValue;
                    cmd.Parameters["EntryDateFrom"].Value = DateTime.ParseExact(StringDS.DataStrings[7].StringValue, "dd-MMM-yyyy", CultureInfo.InvariantCulture);
                    cmd.Parameters["EntryDateTo"].Value = DateTime.ParseExact(StringDS.DataStrings[8].StringValue, "dd-MMM-yyyy", CultureInfo.InvariantCulture);

                }

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, fsDS, fsDS.FinalSettEmpParkingList.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_FINAL_SETTLEMENT_PARKING_EMP_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                fsDS.AcceptChanges();
                cmd.Parameters.Clear();
                cmd.Dispose();

            }

            if (StringDS.DataStrings[0].StringValue == "FinalSettlementParkingEmpList")
            {
                if (StringDS.DataStrings[1].StringValue == "admin")
                {
                    cmd = DBCommandProvider.GetDBCommand("GetFinalSettActivityAdmin");

                }
                else
                {
                    cmd = DBCommandProvider.GetDBCommand("GetFinalSettActivityLoginID");
                    cmd.Parameters["LoginID"].Value = (object)StringDS.DataStrings[6].StringValue;
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, fsDS, fsDS.FinalSettApprovalAuthorityWiseActivity.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_FINAL_SETTLEMENT_PARKING_EMP_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                fsDS.AcceptChanges();

            }


            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(fsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _GetFinalSettlementPerkingEmpCalculationDetails(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            FinalSettlementDS fsDS = new FinalSettlementDS();

            if (StringDS.DataStrings[0].StringValue == "finalSettlementCalculationDetails")
            {
                cmd = DBCommandProvider.GetDBCommand("GetFinalSettEmpParkingByEmpCode");
                cmd.Parameters["EmployeeCode"].Value = (object)StringDS.DataStrings[1].StringValue;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, fsDS, fsDS.FinalSettEmpParkingList.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_FINAL_SETTLEMENT_PARKING_EMP_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                fsDS.AcceptChanges();
                cmd.Parameters.Clear();
                cmd.Dispose();


                cmd = DBCommandProvider.GetDBCommand("GetFinalSettEmpOthersBenefitByEmpCode");
                cmd.Parameters["EmployeeCode"].Value = (object)StringDS.DataStrings[1].StringValue;


                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, fsDS, fsDS.FinalSettOthersPayableParking.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_FINAL_SETTLEMENT_PARKING_EMP_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                fsDS.AcceptChanges();

            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(fsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _FinalSettlementParkingEmpUpdate(DataSet inputDS)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            FinalSettlementDS fsDS = new FinalSettlementDS();
            fsDS.Merge(inputDS.Tables[fsDS.FinalSettEmpParkingList.TableName], false, MissingSchemaAction.Error);
            fsDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_FINAL_SETT_PARKING_EMP_UP";


            foreach (FinalSettlementDS.FinalSettEmpParkingListRow row in fsDS.FinalSettEmpParkingList.Rows)
            {
                long pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_HistoryID", pKey);
                cmd.Parameters.AddWithValue("p_FinalSettParkingID", row.FinalSettParkingID);
                cmd.Parameters.AddWithValue("p_LoginID", row.LoginID);
                cmd.Parameters.AddWithValue("p_ActivityID", row.ActivityID);
                cmd.Parameters.AddWithValue("p_ActivityFriendlyName", row.ActivityFriendlyName);
                cmd.Parameters.AddWithValue("p_ReceiverUserID", row.ReceiverUserID);

                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }


                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);


                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FINAL_SETTLEMENT_APPROVAL_AUTHORITY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _FinalSettlementParkingEmpCreate(DataSet inputDS)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            FinalSettlementDS fsDS = new FinalSettlementDS();
            fsDS.Merge(inputDS.Tables[fsDS.FinalSettEmpParkingList.TableName], false, MissingSchemaAction.Error);
            fsDS.Merge(inputDS.Tables[fsDS.FinalSettOthersPayableParking.TableName], false, MissingSchemaAction.Error);
            fsDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_FINAL_SETT_PARK_EMP_CREATE";

            long pKey = 0;

            foreach (FinalSettlementDS.FinalSettEmpParkingListRow row in fsDS.FinalSettEmpParkingList.Rows)
            {
                pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_FinalSettParkingID", pKey);
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                if (row.IsServiceLengthNull() == false) cmd.Parameters.AddWithValue("p_ServiceLength", row.ServiceLength);
                else cmd.Parameters.AddWithValue("p_ServiceLength", row.ServiceLength);
                cmd.Parameters.AddWithValue("p_LastBasic", row.LastBasic);
                cmd.Parameters.AddWithValue("p_Pay_Salary", row.Pay_Salary);
                cmd.Parameters.AddWithValue("p_Pay_LFA", row.Pay_LFA);
                cmd.Parameters.AddWithValue("p_Pay_Leave_Encashment", row.Pay_Leave_Encashment);
                cmd.Parameters.AddWithValue("p_Pay_NoticePay", row.Pay_NoticePay);
                cmd.Parameters.AddWithValue("p_Pay_Ex_Gratia", row.Pay_Ex_Gratia);
                cmd.Parameters.AddWithValue("p_Pay_TaxreFund", row.Pay_TaxRefund);
                cmd.Parameters.AddWithValue("p_Pay_Other_Benefit", row.Pay_Other_Benefit);
                cmd.Parameters.AddWithValue("p_Recover_OutstandingLoan", row.Recover_OutstandingLoan);
                cmd.Parameters.AddWithValue("p_Rec_LFA", row.Rec_LFA);
                cmd.Parameters.AddWithValue("p_Rec_NoticePay", row.Rec_NoticePay);
                cmd.Parameters.AddWithValue("p_Rec_CompanyAssets", row.Rec_CompanyAssets);
                cmd.Parameters.AddWithValue("p_Rec_TaxDeduction", row.Rec_TaxDeduction);
                cmd.Parameters.AddWithValue("p_Rec_TrainingBonddue", row.Rec_TrainingBondDue);
                cmd.Parameters.AddWithValue("p_Rec_Others", row.Rec_Others);
                cmd.Parameters.AddWithValue("p_Gross_Payable", row.Gross_Payable);
                cmd.Parameters.AddWithValue("p_Gross_Deduction", row.Gross_Deduction);
                cmd.Parameters.AddWithValue("p_Net_Amount", row.Net_Amount);
                cmd.Parameters.AddWithValue("p_PaymentStatus", row.PaymentStatus);
                cmd.Parameters.AddWithValue("p_WithDiscontinue", row.WithdisContinue);                
                cmd.Parameters.AddWithValue("p_ReceiverUserID", row.ReceiverUserID);
                if(row.IsEventCodeNull() == false) cmd.Parameters.AddWithValue("p_EventCode", row.EventCode);
                else cmd.Parameters.AddWithValue("p_EventCode", System.DBNull.Value);
                if (row.IsSettlementDateNull() == false) cmd.Parameters.AddWithValue("p_SettlementDate", row.SettlementDate);
                else cmd.Parameters.AddWithValue("p_SettlementDate", System.DBNull.Value);
                cmd.Parameters.AddWithValue("p_LoginID", row.LoginID);


                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }


                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                cmd.Parameters.Clear();
                

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FINAL_SETTLEMENT_PARKING_EMP_LIST_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }



            cmd.Parameters.Clear();
            cmd.Dispose();
            cmd.CommandText = "PRO_FINAL_SET_PARK_HEAD_CREATE";           

            foreach (FinalSettlementDS.FinalSettOthersPayableParkingRow row in fsDS.FinalSettOthersPayableParking.Rows)
            {

                cmd.Parameters.AddWithValue("p_FinalSettParkingID", pKey);
               if(row.IsBenefitCodeNull() == false) cmd.Parameters.AddWithValue("p_BenefitCode", row.BenefitCode);
               else cmd.Parameters.AddWithValue("p_BenefitCode", System.DBNull.Value); 
               if (row.IsBenefitNameNull() == false) cmd.Parameters.AddWithValue("p_BenefitName", row.BenefitName);
               else cmd.Parameters.AddWithValue("p_BenefitName", System.DBNull.Value);
               if (row.IsChangedAmountNull() == false) cmd.Parameters.AddWithValue("p_ChangedAmount", row.ChangedAmount);           

                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }


                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FINAL_SETTLEMENT_PARKING_EMP_LIST_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getEmployeeCountForSettlement(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            FinalSettlementDS finalSettDS = new FinalSettlementDS();
            DataSet returnDS = new DataSet();


            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            string empCode = stringDS.DataStrings[0].StringValue;
            cmd = DBCommandProvider.GetDBCommand("GetEmployeeCountForSettlement");
            //cmd.Parameters.AddWithValue("EmployeeCode", stringDS.DataStrings[0].StringValue);
            cmd.Parameters["EmployeeCode"].Value = empCode;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, finalSettDS, finalSettDS.EmployeeCount.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_COUNT_FOR_SETTLEMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            finalSettDS.AcceptChanges();

            returnDS.Merge(finalSettDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getSeparationApprovalAuthority(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS StringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            FinalSettlementDS settlDS = new FinalSettlementDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            if (StringDS.DataStrings[0].StringValue == "Separation_ApprovalAuthorityAll")
            {
                #region Approval Path Config Info
                cmd = DBCommandProvider.GetDBCommand("GetSeparation_ApprovalAuthorityAll");
                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, settlDS, settlDS.SeparationApprovalPath.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_SEPARATION_APPROVAL_AUTHORITY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                settlDS.AcceptChanges();
                #endregion

                #region Get PriorityWise Activity
                cmd = DBCommandProvider.GetDBCommand("GetSeparation_PriorityWiseActivityList");
                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, settlDS, settlDS.SeparationApprovalActivity.TableName,
                                                           connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_SEPARATION_APPROVAL_AUTHORITY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                settlDS.AcceptChanges();
                #endregion
            }
            else if (StringDS.DataStrings[0].StringValue == "NextSeparationApprover")
            {
                #region Next Approver
                cmd = DBCommandProvider.GetDBCommand("GetNextSeparationApprover");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["Applicant"].Value = StringDS.DataStrings[1].StringValue;
                cmd.Parameters["Sender"].Value = StringDS.DataStrings[2].StringValue;
                cmd.Parameters["Priority"].Value = Convert.ToInt32(StringDS.DataStrings[3].StringValue);
                cmd.Parameters["Interval"].Value = 1;  // Search one step forward

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, settlDS, settlDS.SeparationApprovalPath.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_SEPARATION_APPROVAL_AUTHORITY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                settlDS.AcceptChanges();
                #endregion
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(settlDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet SeparationActivityListCreate(long ConfigurationID, string[] activityList, int ConnectionID)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SEPARATION_APP_ACT_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 0; i < activityList.Length; i++)
            {
                long pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_App_ActivityID", pKey);
                cmd.Parameters.AddWithValue("p_Activity", activityList[i]);
                cmd.Parameters.AddWithValue("p_ConfigurationID", ConfigurationID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEPARATION_APPROVAL_PATH_CONFIG_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _SeparationApprovalAuthority_Save(DataSet inputDS)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            FinalSettlementDS sepDS = new FinalSettlementDS();
            OleDbCommand cmd;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            sepDS.Merge(inputDS.Tables[sepDS.SeparationApprovalPath.TableName], false, MissingSchemaAction.Error);
            sepDS.AcceptChanges();

            #region Delete Old Data
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SEPARATION_APP_PATH_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEPARATION_APPROVAL_PATH_CONFIG_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Save new data
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SEPARATION_APP_PATH_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (FinalSettlementDS.SeparationApprovalPathRow row in sepDS.SeparationApprovalPath.Rows)
            {
                // Get LoginID
                string[] Authority = row.Authority.Split(' ', '@');
                int loginID = 0;
                if (Authority.Length == 1) loginID = 1;
                else loginID = 0;

                string[] DevidedActivity = row.Activity.Split(' ', ',');

                long pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_ConfigurationID", pKey);
                cmd.Parameters.AddWithValue("p_Priority", row.Priority);
                cmd.Parameters.AddWithValue("p_Authority", row.Authority);
                cmd.Parameters.AddWithValue("p_IsLoginUserID", loginID);

                if (!row.IsDepartmentCodeNull()) cmd.Parameters.AddWithValue("p_DepartmentCode", row.DepartmentCode);
                else cmd.Parameters.AddWithValue("p_DepartmentCode", DBNull.Value);

                //if (!row.IsLiabilityIDNull()) cmd.Parameters.AddWithValue("p_LiabilityID", row.LiabilityID);
                //else cmd.Parameters.AddWithValue("p_LiabilityID", DBNull.Value);

                if (!row.IsLiability_Nature_IDNull()) cmd.Parameters.AddWithValue("p_Liability_Nature_ID", row.Liability_Nature_ID);
                else cmd.Parameters.AddWithValue("p_Liability_Nature_ID", DBNull.Value);

                cmd.Parameters.AddWithValue("p_IsInitiator", row.Is_Initiator);
                cmd.Parameters.AddWithValue("p_IsLiabililtyCollector", row.Is_LiabililtyCollector);
                cmd.Parameters.AddWithValue("p_IsInterviewCaller", row.Is_InterviewCaller);
                cmd.Parameters.AddWithValue("p_Change_EffectDate", row.Change_EffectDate);
                cmd.Parameters.AddWithValue("p_Add_Recommendation", row.Add_Recommendation);
                cmd.Parameters.AddWithValue("p_Acknowledge", row.Acknowledge);
                cmd.Parameters.AddWithValue("p_Required", row.Is_Required);
                cmd.Parameters.AddWithValue("p_Can_Discontinue", row.Can_Discontinue);
                cmd.Parameters.AddWithValue("p_Can_WithholdSalary", row.Can_WithholdSalary);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEPARATION_APPROVAL_PATH_CONFIG_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                // Now Save this user's activities
                DataSet act_Response = SeparationActivityListCreate(pKey, DevidedActivity, connDS.DBConnections[0].ConnectionID);

                ErrorDS errDS_Act = new ErrorDS();
                errDS_Act.Merge(act_Response.Tables[errDS_Act.Errors.TableName], false, MissingSchemaAction.Error);
                errDS_Act.AcceptChanges();
                if (errDS_Act.Errors.Count > 0) return errDS_Act;
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getEmployeeForTerminationChecklist(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            DataDateDS dateDS = new DataDateDS();
            DataLongDS longDS = new DataLongDS();
            DateTime EffectDate_PS = new DateTime(1754, 1, 1);
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            decimal serviceLength = Convert.ToDecimal(stringDS.DataStrings[3].StringValue);
            decimal age = Convert.ToDecimal(stringDS.DataStrings[4].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetEmployeeForTerminationChecklist");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            #region Set Params
            cmd.Parameters["Age01"].Value = age;
            cmd.Parameters["ServiceLength01"].Value = serviceLength;
            cmd.Parameters["ServiceLength02"].Value = serviceLength;
            cmd.Parameters["Age02"].Value = age;
            cmd.Parameters["Age03"].Value = age;
            cmd.Parameters["ServiceLength03"].Value = serviceLength;

            cmd.Parameters["Age04"].Value = age;
            cmd.Parameters["ServiceLength04"].Value = serviceLength;
            cmd.Parameters["Age05"].Value = age;
            cmd.Parameters["ServiceLength05"].Value = serviceLength;
            cmd.Parameters["ServiceLength06"].Value = serviceLength;
            cmd.Parameters["Age06"].Value = age;

            cmd.Parameters["Age07"].Value = age;
            cmd.Parameters["ServiceLength07"].Value = serviceLength;
            cmd.Parameters["Age08"].Value = age;
            cmd.Parameters["ServiceLength08"].Value = serviceLength;
            cmd.Parameters["Age09"].Value = age;
            cmd.Parameters["ServiceLength09"].Value = serviceLength;

            cmd.Parameters["ToDateAge2"].Value = dateDS.DataDates[3].DateValue;
            cmd.Parameters["ToDateAge3"].Value = dateDS.DataDates[3].DateValue;

            cmd.Parameters["DisciplinaryTypeID"].Value = integerDS.DataIntegers[3].IntegerValue;
            cmd.Parameters["InitiatorEmpCode"].Value = stringDS.DataStrings[7].StringValue;

            cmd.Parameters["EmployeeName"].Value = stringDS.DataStrings[6].StringValue;
            cmd.Parameters["Ter_ActivityIDs"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["EmployeeCode"].Value = cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[5].StringValue;
            cmd.Parameters["GradeCode"].Value = cmd.Parameters["GradeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["DesignationCode"].Value = cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SiteID"].Value = cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["DepartmentID"].Value = cmd.Parameters["DepartmentID2"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["EmployeeType"].Value = cmd.Parameters["EmployeeType2"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["DisciplinaryTypeID2"].Value = cmd.Parameters["DisciplinaryTypeID3"].Value = integerDS.DataIntegers[3].IntegerValue;

            cmd.Parameters["ServiceLength"].Value = serviceLength;
            cmd.Parameters["FromDateJoin"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["ToDateJoin"].Value = dateDS.DataDates[1].DateValue;

            cmd.Parameters["Age"].Value = age;
            cmd.Parameters["FromDateAge"].Value = dateDS.DataDates[2].DateValue;
            cmd.Parameters["ToDateAge"].Value = dateDS.DataDates[3].DateValue;
            #endregion
            adapter.SelectCommand = cmd;

            FinalSettlementDS settleDS = new FinalSettlementDS();
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, settleDS, settleDS.Separation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_FOR_TERMINATION_CHECKLIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            settleDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(settleDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _saveTerminationChecklist(DataSet inputDS)
        {
            FinalSettlementDS separationDS = new FinalSettlementDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            Int32 checklistID = 0;

            separationDS.Merge(inputDS.Tables[separationDS.Separation.TableName], false, MissingSchemaAction.Error);
            separationDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            string messageBody = "", toAddress = "";
            string returnMessage = "Email successfully sent.";
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());

            bool isNotifyByMail = false;
            string mailSubject = "", senderEmail = "";

            #region Set Message
            messageBody = String.Format("<b>Dispatcher :</b> {0} - {1}, {2}, {3}<br><br>", separationDS.Separation[0].EmployeeCode, separationDS.Separation[0].EmployeeName, separationDS.Separation[0].DesignationName, separationDS.Separation[0].DepartmentName);
            messageBody = String.Format("<b>Employee for Separation :</b> {0} - {1}, {2}, {3}<br><br>", separationDS.Separation[0].EmployeeCode, separationDS.Separation[0].EmployeeName, separationDS.Separation[0].DesignationName, separationDS.Separation[0].DepartmentName);
            messageBody += separationDS.Separation[0].EmailBody;
            //messageBody += "You have pending requests for employee separation.<br>";
            //messageBody += "Please check the HR Management system for details.<br>";
            messageBody += "<br><br><br>";
            #endregion
            #endregion

            //OleDbCommand cmdDel = new OleDbCommand();
            //cmdDel.CommandText = "PRO_CHECKLIST_DELETE";
            //cmdDel.CommandType = CommandType.StoredProcedure;
            //if (cmdDel == null) return UtilDL.GetCommandNotFound();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TERMINATION_CHECKLIST_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmdHist = new OleDbCommand();
            cmdHist.CommandText = "PRO_CHECKLIST_HISTORY_ADD";
            cmdHist.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmdHist == null) return UtilDL.GetCommandNotFound();

            #region Send Notification Mails
            foreach (FinalSettlementDS.SeparationRow row in separationDS.Separation.Rows)
            {
                if (row.IsNotifyByMail)
                {
                    isNotifyByMail = true;
                    senderEmail = row.Email;
                    mailSubject = row.SubjectForEmail;
                    //toAddress += row.Next_AppEmail + ",";
                    toAddress = row.Next_AppEmail;
                }
            }

            if (isNotifyByMail)
            {
                senderEmail = credentialEmailAddress;
                returnMessage = "";
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, senderEmail, toAddress, "",
                                                   mailSubject, messageBody, "Separation Request");
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, senderEmail, toAddress, "", mailSubject, messageBody, "Separation Request");
                }
            }
            #endregion

            if (returnMessage == "Email successfully sent.")
            {
                foreach (FinalSettlementDS.SeparationRow row in separationDS.Separation.Rows)
                {
                    #region Delete Old Record
                    //cmdDel.Parameters.AddWithValue("p_EmployeeID", row.Employeeid);
                    //bool bErrorDel = false;
                    //int nRowAffectedDel = -1;
                    //nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);
                    //cmdDel.Parameters.Clear();
                    //if (bErrorDel)
                    //{
                    //    ErrorDS.Error err = errDS.Errors.NewError();
                    //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    //    err.ErrorInfo1 = ActionID.NA_ACTION_SAVE_EMPLOYMENT_TERMINATION_CHECKLIST.ToString();
                    //    errDS.Errors.AddError(err);
                    //    errDS.AcceptChanges();
                    //    return errDS;
                    //}
                    #endregion

                    #region Save into CheckList
                    cmd.Parameters.Add("p_CheckListID", OleDbType.Numeric, 22).Direction = ParameterDirection.Output;
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_DiscontinueTypeID", row.DiscontinueTypeID);
                    cmd.Parameters.AddWithValue("p_TerminationReasonID", row.TerminationReasonID);
                    cmd.Parameters.AddWithValue("p_Reason", row.TerminationReason);
                    cmd.Parameters.AddWithValue("p_EffectDate", row.EffectDate);
                    cmd.Parameters.AddWithValue("p_Maker", row.LoginID);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    checklistID = Convert.ToInt32(cmd.Parameters["p_CheckListID"].Value.ToString());
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.NA_ACTION_SAVE_EMPLOYMENT_TERMINATION_CHECKLIST.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();

                        messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} - {1} -> Request SAVE ERROR", row.EmployeeCode, row.EmployeeName));
                        continue;
                    }
                    #endregion

                    #region Save History
                    cmdHist.Parameters.AddWithValue("p_CheckListID", checklistID);
                    cmdHist.Parameters.AddWithValue("p_SenderUserId", row.SenderUserId);
                    cmdHist.Parameters.AddWithValue("p_ReceiverUserId", row.ReceiverUserId);
                    cmdHist.Parameters.AddWithValue("p_ActivityId", row.ActivityID);
                    cmdHist.Parameters.AddWithValue("p_Changed_EffectDate", row.Changed_EffectDate);
                    cmdHist.Parameters.AddWithValue("p_Priority", row.Priority);
                    cmdHist.Parameters.AddWithValue("p_Initiated", row.Initiated);
                    cmdHist.Parameters.AddWithValue("p_Liability_Collected", row.Liability_Collected);
                    cmdHist.Parameters.AddWithValue("p_Interviewed", row.Interviewed);
                    cmdHist.Parameters.AddWithValue("p_Process_Completed", row.Process_Completed);

                    if (!row.IsDiscontinueTypeIDNull()) cmdHist.Parameters.AddWithValue("p_DiscontinueTypeID", row.DiscontinueTypeID);
                    else cmdHist.Parameters.AddWithValue("p_DiscontinueTypeID", DBNull.Value);

                    if (!row.IsIs_RefundableNull()) cmdHist.Parameters.AddWithValue("p_IsRefundable", row.Is_Refundable);
                    else cmdHist.Parameters.AddWithValue("p_IsRefundable", DBNull.Value);

                    if (!row.IsHistory_CommentNull()) cmdHist.Parameters.AddWithValue("p_Comment", row.History_Comment);
                    else cmdHist.Parameters.AddWithValue("p_Comment", DBNull.Value);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdHist, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmdHist.Parameters.Clear();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.NA_ACTION_SAVE_EMPLOYMENT_TERMINATION_CHECKLIST.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();

                        messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} - {1} -> History SAVE ERROR", row.EmployeeCode, row.EmployeeName));
                        continue;
                    }
                    #endregion

                    messageDS.SuccessMsg.AddSuccessMsgRow(String.Format("{0} - {1} -> {2} - {3}",
                                                           row.EmployeeCode, row.EmployeeName,
                                                           row.Next_AppEmployeeCode, row.Next_AppEmployeeName));
                    stringDS.DataStrings.AddDataString(checklistID.ToString());
                }
            }
            else
            {
                messageDS.ErrorMsg.AddErrorMsgRow("Email sending failed.");
                messageDS.AcceptChanges();
            }
            errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.Merge(stringDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getPendingSeparationRequests(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS StringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            FinalSettlementDS settleDS = new FinalSettlementDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetPendingSeparationRequests");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["EmployeeName"].Value = StringDS.DataStrings[0].StringValue;
            cmd.Parameters["AppHolder1"].Value = cmd.Parameters["AppHolder2"].Value = StringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = StringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = Convert.ToInt16(StringDS.DataStrings[3].StringValue);
            cmd.Parameters["DivisionID1"].Value = cmd.Parameters["DivisionID2"].Value = Convert.ToInt16(StringDS.DataStrings[4].StringValue);
            cmd.Parameters["DepartmentID1"].Value = cmd.Parameters["DepartmentID2"].Value = Convert.ToInt16(StringDS.DataStrings[5].StringValue);
            cmd.Parameters["DesignationID1"].Value = cmd.Parameters["DesignationID2"].Value = Convert.ToInt16(StringDS.DataStrings[6].StringValue);
            cmd.Parameters["ActivityID1"].Value = cmd.Parameters["ActivityID2"].Value = StringDS.DataStrings[7].StringValue;
            cmd.Parameters["ApplicantID1"].Value = cmd.Parameters["ApplicantID2"].Value = Convert.ToInt32(StringDS.DataStrings[8].StringValue);

            adapter.SelectCommand = cmd;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, settleDS, settleDS.Separation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PENDING_SEPARATION_REQUESTS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            settleDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(settleDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _approveSeparationRequests(DataSet inputDS)
        {
            FinalSettlementDS separationDS = new FinalSettlementDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmdHist = new OleDbCommand();
            OleDbCommand cmdDEL = new OleDbCommand();
            Int32 checklistID = 0;
            bool bError = false;
            int nRowAffected = -1;                       

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            separationDS.Merge(inputDS.Tables[separationDS.Separation.TableName], false, MissingSchemaAction.Error);
            separationDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            //string messageBody = "", toAddress = "";
            string returnMessage = "Email successfully sent.";
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());

            bool isNotifyByMail = false;
            string mailSubject = "", senderEmail = "";

            #region Set Message
            //messageBody = String.Format("<b>Dispatcher :</b> {0} - {1} ({2})<br><br>", separationDS.Separation[0].EmployeeCode, separationDS.Separation[0].EmployeeName, separationDS.Separation[0].DesignationName);
            //messageBody += "Exit Interview is submitted by " + String.Format("{0} - {1}<br><br>", separationDS.Separation[0].EmployeeCode, separationDS.Separation[0].EmployeeName) + ".<br>";
            //messageBody += "Please check the HR Management system for details.<br>";
            //messageBody += "<br><br><br>";
            #endregion
            #endregion

            cmdHist.CommandText = "PRO_CHECKLIST_HISTORY_ADD";
            cmdDEL.CommandText = "PRO_SEP_LIA_COLLECTION_DELETE";
            cmdHist.CommandType = cmdDEL.CommandType = CommandType.StoredProcedure;

            foreach (FinalSettlementDS.SeparationRow row in separationDS.Separation.Rows)
            {
                #region DELETE Liability Collection [before inserting NEW history]
                if (row.Delete_LiabilityCollection)
                {
                    cmdDEL.Parameters.AddWithValue("p_CheckListID", row.ChecklistId);
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDEL, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmdDEL.Parameters.Clear();
                    if (bError)
                    {
                        //ErrorDS.Error err = errDS.Errors.NewError();
                        //err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        //err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        //err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        //err.ErrorInfo1 = ActionID.NA_ACTION_APPROVE_SEPARATION_REQUEST.ToString();
                        //errDS.Errors.AddError(err);
                        //errDS.AcceptChanges();

                        messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} => Previous liability collection delete ERROR", row.EmployeeName));
                        continue;
                    }
                }
                #endregion

                cmdHist.Parameters.AddWithValue("p_CheckListID", row.ChecklistId);
                cmdHist.Parameters.AddWithValue("p_SenderUserId", row.SenderUserId);
                cmdHist.Parameters.AddWithValue("p_ReceiverUserId", row.ReceiverUserId);
                cmdHist.Parameters.AddWithValue("p_ActivityId", row.ActivityID);
                cmdHist.Parameters.AddWithValue("p_Changed_EffectDate", row.Changed_EffectDate);
                cmdHist.Parameters.AddWithValue("p_Priority", row.Priority);
                cmdHist.Parameters.AddWithValue("p_Initiated", row.Initiated);
                cmdHist.Parameters.AddWithValue("p_Liability_Collected", row.Liability_Collected);
                cmdHist.Parameters.AddWithValue("p_Interviewed", row.Interviewed);
                cmdHist.Parameters.AddWithValue("p_Process_Completed", row.Process_Completed);

                if (!row.IsDiscontinueTypeIDNull()) cmdHist.Parameters.AddWithValue("p_DiscontinueTypeID", row.DiscontinueTypeID);
                else cmdHist.Parameters.AddWithValue("p_DiscontinueTypeID", DBNull.Value);

                if (!row.IsIs_RefundableNull()) cmdHist.Parameters.AddWithValue("p_IsRefundable", row.Is_Refundable);
                else cmdHist.Parameters.AddWithValue("p_IsRefundable", DBNull.Value);
                
                if (!row.IsHistory_CommentNull()) cmdHist.Parameters.AddWithValue("p_Comment", row.History_Comment);
                else cmdHist.Parameters.AddWithValue("p_Comment", DBNull.Value);

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdHist, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdHist.Parameters.Clear();
                if (bError)
                {
                    //ErrorDS.Error err = errDS.Errors.NewError();
                    //err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    //err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    //err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    //err.ErrorInfo1 = ActionID.NA_ACTION_APPROVE_SEPARATION_REQUEST.ToString();
                    //errDS.Errors.AddError(err);
                    //errDS.AcceptChanges();

                    messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} => History SAVE ERROR", row.EmployeeName));
                    continue;
                }                
                messageDS.SuccessMsg.AddSuccessMsgRow(String.Format("{0} -> {1}", row.EmployeeName, row.Next_AppEmployeeName));
                
                #region Send Notification Mails

                if (!bError)
                {
                    string messageBody = "", toAddress = "", ccMailAddress = "";
                    messageBody = String.Format("<b>Dispatcher :</b> {0}<br><br>", row.DispatcherEmployee);
                    messageBody += String.Format("<b>Employee for Separation :</b> {0}<br><br>", row.SeparateEmployee);
                    messageBody += row.EmailBody;
                    //messageBody += row.EmailBody + ".<br>";
                    //messageBody += "Please check the HR Management system for details.<br>";
                    messageBody += "<br><br><br>";

                    if (row.IsNotifyByMail)
                    {
                        isNotifyByMail = true;
                        senderEmail = row.ApproverEmail;
                        mailSubject = row.SubjectForEmail;
                        //toAddress += row.Next_AppEmail + ",";
                        toAddress = row.RecevierEmail + ",";
                        if (!row.IsCcMailAddressNull()) ccMailAddress = row.CcMailAddress;
                    }

                    if (isNotifyByMail)
                    {
                        senderEmail = credentialEmailAddress;
                        returnMessage = "";
                        if (IsEMailSendWithCredential)
                        {
                            //returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, senderEmail, toAddress, "",
                            //                               mailSubject, messageBody, "Exit Interview Submission");
                            returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, senderEmail, toAddress, ccMailAddress, mailSubject, messageBody, "Separation Request");
                        }
                        else
                        {
                            //returnMessage = mail.SendEmail(host, port, senderEmail, toAddress, "", mailSubject, messageBody, "Exit Interview Submission");
                            returnMessage = mail.SendEmail(host, port, senderEmail, toAddress, ccMailAddress, mailSubject, messageBody, "Separation Request");
                        }
                    }
                    if (returnMessage != "Email successfully sent.")
                    {
                        messageDS.ErrorMsg.AddErrorMsgRow("Email sending failed.");
                        messageDS.AcceptChanges();
                    }
                }
                #endregion
            }

            errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getActivityListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetSeparationActivityListAll");

            //string activeStatus = StringDS.DataStrings[0].StringValue;
            //if (activeStatus.Equals("All"))
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetSeparationActivityListAll");
            //}
            //else
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetLmsActiveActivityList");
            //    if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
            //    else cmd.Parameters["IsActive"].Value = (object)false;
            //}

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            FinalSettlementDS setDS = new FinalSettlementDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, setDS, setDS.Activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_SEPARATION_GET_ACTIVITY_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            setDS.AcceptChanges();
            
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(setDS.Activities);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getSeparationApprovalHistory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS StringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            FinalSettlementDS settleDS = new FinalSettlementDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetSeparationApprovalHistory");
            if (cmd == null) return UtilDL.GetCommandNotFound();


            cmd.Parameters["CheckListID"].Value = Convert.ToInt32(StringDS.DataStrings[0].StringValue);
            
            adapter.SelectCommand = cmd;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, settleDS, settleDS.Separation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_SEPARATION_APPROVAL_HISTORY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            settleDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(settleDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getFinalSettlementInforForReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS StringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            FinalSettlementDS settleDS = new FinalSettlementDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetFinalSettlementInformation _For_Report");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["EmployeeCode"].Value = StringDS.DataStrings[0].StringValue;

            adapter.SelectCommand = cmd;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, settleDS, settleDS.Settlement.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_FINAL_SETTLEMENT_REPORT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            settleDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(settleDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}
