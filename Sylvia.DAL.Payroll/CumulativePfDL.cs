/* Compamy: Milllennium Information Solution Limited
 * Author: Km Jarif
 * Comment Date: May, 27, 2009
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for CumulativePfDL.
    /// </summary>
    
    class CumulativePfDL
    {
        public CumulativePfDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_ACTION_GET_CUMULATIVPF_ALL:
                    return this._getCumulativePfAll(inputDS);
                case ActionID.NA_ACTION_GET_CUMULATIVPF_BYID:
                    return this._getCumulativePfById(inputDS);
                case ActionID.NA_ACTION_GET_CUMULATIVPF_BYDATE:
                    return this._getCumulativePfByDate(inputDS);
                case ActionID.NA_ACTION_GET_CUMULATIVPF_YEAR_WISE:
                    return this._getCumulativePfYearWise(inputDS);

                case ActionID.NA_ACTION_GET_PF_CURRENT_BALANCE:
                    return this._getPFCurrentBalanceByEmpID(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }

        private DataSet _getCumulativePfAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCumulativePFAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            CumulativePfPO cpfPO = new CumulativePfPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              cpfPO, cpfPO.CumulativePf.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_CUMULATIVPF_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            cpfPO.AcceptChanges();

            CumulativePfDS cpfDS = new CumulativePfDS();

            if (cpfPO.CumulativePf.Rows.Count > 0)
            {
                foreach (CumulativePfPO.CumulativePfRow poCpf in cpfPO.CumulativePf.Rows)
                {
                    CumulativePfDS.CumulativePfRow cpf = cpfDS.CumulativePf.NewCumulativePfRow();
                    if (poCpf.IsEmployeeIdNull() == false)
                    {
                        cpf.EmployeeId = poCpf.EmployeeId;
                    }
                    if (poCpf.IsEmployeeCodeNull() == false)
                    {
                        cpf.EmployeeCode = poCpf.EmployeeCode;
                    }
                    if (poCpf.IsEmployeeNameNull() == false)
                    {
                        cpf.EmployeeName = poCpf.EmployeeName;
                    }
                    if (poCpf.IsCompanyPFNull() == false)
                    {
                        cpf.CompanyPF = poCpf.CompanyPF;
                    }
                    if (poCpf.IsSelfPFNull() == false)
                    {
                        cpf.SelfPF = poCpf.SelfPF;
                    }
                    if (poCpf.IsTotalAmountNull() == false)
                    {
                        cpf.TotalAmount = poCpf.TotalAmount;
                    }

                    cpfDS.CumulativePf.AddCumulativePfRow(cpf);
                    cpfDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(cpfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getCumulativePfById(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS StringDS = new DataStringDS();
            string empCode = "";
            long empId = 0;

            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();
            empCode = StringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            empId = UtilDL.GetEmployeeId(connDS, empCode);

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCumulativePFById");
            cmd.Parameters["Id"].Value = empId;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            CumulativePfPO cpfPO = new CumulativePfPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              cpfPO, cpfPO.CumulativePf.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_CUMULATIVPF_BYID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            cpfPO.AcceptChanges();

            CumulativePfDS cpfDS = new CumulativePfDS();

            if (cpfPO.CumulativePf.Rows.Count > 0)
            {
                foreach (CumulativePfPO.CumulativePfRow poCpf in cpfPO.CumulativePf.Rows)
                {
                    CumulativePfDS.CumulativePfRow cpf = cpfDS.CumulativePf.NewCumulativePfRow();
                    if (poCpf.IsEmployeeIdNull() == false)
                    {
                        cpf.EmployeeId = poCpf.EmployeeId;
                    }
                    if (poCpf.IsEmployeeCodeNull() == false)
                    {
                        cpf.EmployeeCode = poCpf.EmployeeCode;
                    }
                    if (poCpf.IsEmployeeNameNull() == false)
                    {
                        cpf.EmployeeName = poCpf.EmployeeName;
                    }
                    if (poCpf.IsCompanyPFNull() == false)
                    {
                        cpf.CompanyPF = poCpf.CompanyPF;
                    }
                    if (poCpf.IsSelfPFNull() == false)
                    {
                        cpf.SelfPF = poCpf.SelfPF;
                    }
                    if (poCpf.IsTotalAmountNull() == false)
                    {
                        cpf.TotalAmount = poCpf.TotalAmount;
                    }

                    cpfDS.CumulativePf.AddCumulativePfRow(cpf);
                    cpfDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(cpfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        #region OLD....
        //private DataSet _getCumulativePfByDate(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();

        //    DBConnectionDS connDS = new DBConnectionDS();
        //    DataDateDS DateDS = new DataDateDS();

        //    DateDS.Merge(inputDS.Tables[DateDS.DataDates.TableName], false, MissingSchemaAction.Error);
        //    DateDS.AcceptChanges();

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //    connDS.AcceptChanges();

        //    OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCumulativePFByDate");
        //    cmd.Parameters["FromDate"].Value = DateDS.DataDates[0].DateValue;
        //    cmd.Parameters["ToDate"].Value = DateDS.DataDates[1].DateValue;

        //    if (cmd == null)
        //    {
        //        return UtilDL.GetCommandNotFound();
        //    }

        //    OleDbDataAdapter adapter = new OleDbDataAdapter();
        //    adapter.SelectCommand = cmd;

        //    CumulativePfPO cpfPO = new CumulativePfPO();

        //    bool bError = false;
        //    int nRowAffected = -1;
        //    nRowAffected = ADOController.Instance.Fill(adapter,
        //      cpfPO, cpfPO.CumulativePf.TableName,
        //      connDS.DBConnections[0].ConnectionID,
        //      ref bError);
        //    if (bError == true)
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorInfo1 = ActionID.NA_ACTION_GET_CUMULATIVPF_BYDATE.ToString();
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;

        //    }

        //    cpfPO.AcceptChanges();

        //    CumulativePfDS cpfDS = new CumulativePfDS();

        //    if (cpfPO.CumulativePf.Rows.Count > 0)
        //    {
        //        foreach (CumulativePfPO.CumulativePfRow poCpf in cpfPO.CumulativePf.Rows)
        //        {
        //            CumulativePfDS.CumulativePfRow cpf = cpfDS.CumulativePf.NewCumulativePfRow();
        //            if (poCpf.IsEmployeeIdNull() == false)
        //            {
        //                cpf.EmployeeId = poCpf.EmployeeId;
        //            }
        //            if (poCpf.IsEmployeeCodeNull() == false)
        //            {
        //                cpf.EmployeeCode = poCpf.EmployeeCode;
        //            }
        //            if (poCpf.IsEmployeeNameNull() == false)
        //            {
        //                cpf.EmployeeName = poCpf.EmployeeName;
        //            }
        //            if (poCpf.IsCompanyPFNull() == false)
        //            {
        //                cpf.CompanyPF = poCpf.CompanyPF;
        //            }
        //            if (poCpf.IsSelfPFNull() == false)
        //            {
        //                cpf.SelfPF = poCpf.SelfPF;
        //            }
        //            if (poCpf.IsTotalAmountNull() == false)
        //            {
        //                cpf.TotalAmount = poCpf.TotalAmount;
        //            }

        //            cpfDS.CumulativePf.AddCumulativePfRow(cpf);
        //            cpfDS.AcceptChanges();
        //        }
        //    }

        //    // now create the packet
        //    errDS.Clear();
        //    errDS.AcceptChanges();

        //    DataSet returnDS = new DataSet();

        //    returnDS.Merge(cpfDS);
        //    returnDS.Merge(errDS);
        //    returnDS.AcceptChanges();

        //    return returnDS;

        //}
        #endregion

        private DataSet _getCumulativePfByDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS DateDS = new DataDateDS();

            DateDS.Merge(inputDS.Tables[DateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            DateDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCumulativePFByDate");
            cmd.Parameters["FromDate"].Value = DateDS.DataDates[0].DateValue;
            cmd.Parameters["ToDate"].Value = DateDS.DataDates[1].DateValue;
            cmd.Parameters["FromDate1"].Value = DateDS.DataDates[0].DateValue;
            cmd.Parameters["ToDate1"].Value = DateDS.DataDates[1].DateValue;
            cmd.Parameters["FromDate2"].Value = DateDS.DataDates[0].DateValue;
            cmd.Parameters["ToDate2"].Value = DateDS.DataDates[1].DateValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            CumulativePfDS cpfDS = new CumulativePfDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, cpfDS, cpfDS.CumulativePf.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_CUMULATIVPF_BYDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            cpfDS.AcceptChanges();
                       

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(cpfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getCumulativePfYearWise(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCumulativePFYearWise");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            CumulativePfDS cpfDS = new CumulativePfDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, cpfDS, cpfDS.CumulativePFYearWise.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_CUMULATIVPF_YEAR_WISE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            cpfDS.AcceptChanges();

            
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(cpfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getPFCurrentBalanceByEmpID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPFCurrentBalanceByEmpID");
            cmd.Parameters["FromDate"].Value = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["ToDate"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["FromDate1"].Value = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["ToDate1"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["FromDate2"].Value = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["ToDate2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            CumulativePfDS cpfDS = new CumulativePfDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, cpfDS, cpfDS.CumulativePf.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PF_CURRENT_BALANCE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            cpfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(cpfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
    }
}
