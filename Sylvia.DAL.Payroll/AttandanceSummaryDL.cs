﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Sylvia.Common;
using System.Data.OleDb;
using Sylvia.DAL.Database;

namespace Sylvia.DAL.Payroll
{
    public class AttandanceSummaryDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_ACTION_GET_ATTANDANCE_SUMMARY_BYDATE_RANGE:
                    return _getAttandanceSummaryByDateRange(inputDS);
                case ActionID.ACTION_ATTANDANCE_SUMMARY_ADD:
                    return _saveAtt_Summary(inputDS);
                case ActionID.NA_ACTION_GET_ATTANDANCE_ACTIVITY:
                    return this._LoadAttandanceActivityCombo(inputDS);
                case ActionID.NA_ACTION_GET_ATTANDANCE_SUMMARY_BYMONTH:
                    return this._getAttandanceSummaryByMonth(inputDS);
                case ActionID.ACTION_ATTANDANCE_SUMMARY_UPDATE:
                    return this._updateAtt_Summary(inputDS);
                case ActionID.ACTION_ATTANDANCE_APPROVAL_PATH_ADD:
                    return this._Att_Approval_Path_Create(inputDS);
                case ActionID.NA_ACTION_GET_ATTANDANCE_STATUS_FOR_EMPLOYEE:
                    return _getAttandanceSummaryByEmployeeCodeNDateRange(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_INFO_FOR_ATT:
                    return _getEmployeeInfoForAttandanceSummary(inputDS);
                case ActionID.NA_ACTION_GET_ATTANDANCE_SUMMARY_VALIDATE:
                    return _getAttandanceSummaryValidateData(inputDS);
                case ActionID.NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO:
                    return _GetAttApprovalPathConfigInfo(inputDS);
                case ActionID.ACTION_ATTANDANCE_APPROVAL_PATH_GETMAX_SALARYDATE:
                    return _getMaxSalaryProcessDateBySummaryByMonth(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getEmployeeInfoForAttandanceSummary(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            AttandanceSummaryDS _attandanceSummaryDS = new AttandanceSummaryDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetEmployeeInfoForAttendance");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, _attandanceSummaryDS, _attandanceSummaryDS.Att_Summary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            _attandanceSummaryDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(_attandanceSummaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _GetAttApprovalPathConfigInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            EmployeeHistoryDS EmpHistDS = new EmployeeHistoryDS();



            if (StringDS.DataStrings[0].StringValue == "AttAppPathConfInfoAll")
            {
                #region Approval Path Config Info
                cmd = DBCommandProvider.GetDBCommand("GetAttApprovalPathConfigurationInfo");

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpHistDS, EmpHistDS.TransferApprovalPathConfigBulk.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpHistDS.AcceptChanges();
                #endregion

                #region Get PriorityWise Activity
                cmd.Dispose();
                cmd.Parameters.Clear();

                cmd = DBCommandProvider.GetDBCommand("GetAttPriorityWiseActivityList");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpHistDS, EmpHistDS.TransferPriotityWiseActivity.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpHistDS.AcceptChanges();
                #endregion
            }
            else if (StringDS.DataStrings[0].StringValue == "PriorityWiseAllActivities")
            {
                #region Get Priority Wise Activity
                cmd.Dispose();
                cmd.Parameters.Clear();

                cmd = DBCommandProvider.GetDBCommand("GetPriorityWiseAllActivities");

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpHistDS, EmpHistDS.TransferPriotityWiseActivity.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpHistDS.AcceptChanges();
                #endregion
            }
            else if (StringDS.DataStrings[0].StringValue == "GetNextReceipentForAttSummaryApprove_SL")
            {
                #region Get Priority Wise Activity
                cmd.Dispose();
                cmd.Parameters.Clear();

                cmd = DBCommandProvider.GetDBCommand("GetNextReceiverForSL_AttAPPROVER");

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                cmd.Parameters["p_ApplicantCode"].Value = cmd.Parameters["p_SenderEmpCode"].Value = Convert.ToString(StringDS.DataStrings[1].StringValue);
                cmd.Parameters["p_CurrentPriority"].Value = Convert.ToInt32(StringDS.DataStrings[2].StringValue);
                cmd.Parameters["p_Interval"].Value = Convert.ToInt32(StringDS.DataStrings[3].StringValue);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpHistDS, EmpHistDS.TransferApprovalPathConfigBulk.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpHistDS.AcceptChanges();
                #endregion
            }
            else if (StringDS.DataStrings[0].StringValue == "MinimumPriority")
            {

                cmd.Dispose();
                cmd.Parameters.Clear();

                cmd = DBCommandProvider.GetDBCommand("GetMinimumPriority");

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpHistDS, EmpHistDS.TransferPriotityWiseActivity.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpHistDS.AcceptChanges();

            }
            else if (StringDS.DataStrings[0].StringValue == "GetNextReceipentForAttSummaryApprove")
            {

                cmd.Dispose();
                cmd.Parameters.Clear();

                cmd = DBCommandProvider.GetDBCommand("GetAttApprovalPathDataForApprove");
                cmd.Parameters["priorityid"].Value = Convert.ToString(StringDS.DataStrings[1].StringValue);

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpHistDS, EmpHistDS.TransferApprovalPathConfigBulk.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpHistDS.AcceptChanges();

            }
            else if (StringDS.DataStrings[0].StringValue == "MaximumPriority")
            {

                cmd.Dispose();
                cmd.Parameters.Clear();

                cmd = DBCommandProvider.GetDBCommand("GetMaximumPriority");

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpHistDS, EmpHistDS.TransferPriotityWiseActivity.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpHistDS.AcceptChanges();

            }
            else if (StringDS.DataStrings[0].StringValue == "adminWiseAllActivities")
            {
                #region Get admin Wise Activity
                cmd.Dispose();
                cmd.Parameters.Clear();

                cmd = DBCommandProvider.GetDBCommand("GetAdminWiseAllActivities");

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpHistDS, EmpHistDS.TransferPriotityWiseActivity.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpHistDS.AcceptChanges();
                #endregion
            }
            else if (StringDS.DataStrings[0].StringValue == "GetNextReceipentForAttSummary")
            {
                #region Get admin Wise Activity
                cmd.Dispose();
                cmd.Parameters.Clear();

                cmd = DBCommandProvider.GetDBCommand("GetAttApprovalPathForAttSummary");

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, EmpHistDS, EmpHistDS.TransferApprovalPathConfigBulk.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_ATT_APPROVAL_PATH_CONFIG_INFO.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                EmpHistDS.AcceptChanges();
                #endregion
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(EmpHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAttandanceSummaryByDateRange(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            AttandanceSummaryDS _attandanceSummaryDS = new AttandanceSummaryDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataDateDS stringDS = new DataDateDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataDates.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataStringDS _DataStringDS = new DataStringDS();
            _DataStringDS.Merge(inputDS.Tables[_DataStringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            _DataStringDS.AcceptChanges();
                
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            
            cmd = DBCommandProvider.GetDBCommand("GetAttandanceSummaryByDateRange");
            cmd.Parameters["fromDate"].Value = Convert.ToDateTime(stringDS.DataDates[0].DateValue);
            cmd.Parameters["toDate"].Value = Convert.ToDateTime(stringDS.DataDates[1].DateValue);
            cmd.Parameters["employeetypeid"].Value = cmd.Parameters["employeetypeid1"].Value  = Convert.ToInt32(_DataStringDS.DataStrings[0].StringValue);
            cmd.Parameters["DepartmentCode"].Value = cmd.Parameters["DepartmentCode1"].Value = Convert.ToString(_DataStringDS.DataStrings[1].StringValue);
            cmd.Parameters["SiteCode"].Value = cmd.Parameters["SiteCode1"].Value = Convert.ToString(_DataStringDS.DataStrings[2].StringValue);
            cmd.Parameters["CompanyCode"].Value = cmd.Parameters["CompanyCode1"].Value = Convert.ToString(_DataStringDS.DataStrings[3].StringValue);


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, _attandanceSummaryDS, _attandanceSummaryDS.Att_Summary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            _attandanceSummaryDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(_attandanceSummaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getAttandanceSummaryByEmployeeCodeNDateRange(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            AttandanceSummaryDS _attandanceSummaryDS = new AttandanceSummaryDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS _dataDateDs = new DataDateDS();
            _dataDateDs.Merge(inputDS.Tables[_dataDateDs.DataDates.TableName], false, MissingSchemaAction.Error);
            _dataDateDs.AcceptChanges();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAttandanceStatusByEmployee");
            cmd.Parameters["employeeid"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["loginstatus"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["frmDate"].Value = Convert.ToDateTime(_dataDateDs.DataDates[0].DateValue);
            cmd.Parameters["toDate"].Value = Convert.ToDateTime(_dataDateDs.DataDates[1].DateValue);

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, _attandanceSummaryDS, _attandanceSummaryDS.Att_Summary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            _attandanceSummaryDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(_attandanceSummaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getAttandanceSummaryByMonth(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            AttandanceSummaryDS _attandanceSummaryDS = new AttandanceSummaryDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataDateDS stringDS = new DataDateDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataDates.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataStringDS _DataStringDS = new DataStringDS();
            _DataStringDS.Merge(inputDS.Tables[_DataStringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            _DataStringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAttandanceSummaryByMonth");
            cmd.Parameters["summmarymonth"].Value = Convert.ToDateTime(stringDS.DataDates[0].DateValue);
            cmd.Parameters["receiveduserid"].Value = Convert.ToString(_DataStringDS.DataStrings[0].StringValue);

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, _attandanceSummaryDS, _attandanceSummaryDS.Att_Summary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            _attandanceSummaryDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(_attandanceSummaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getMaxSalaryProcessDateBySummaryByMonth(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            AttandanceSummaryDS _attandanceSummaryDS = new AttandanceSummaryDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataDateDS _dateDS = new DataDateDS();
            _dateDS.Merge(inputDS.Tables[_dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            _dateDS.AcceptChanges();

    
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetMaxSalaryProcessDateByDate");
            cmd.Parameters["summarydate"].Value = Convert.ToDateTime(_dateDS.DataDates[0].DateValue);

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, _attandanceSummaryDS, _attandanceSummaryDS.Att_Summary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ATTANDANCE_APPROVAL_PATH_GETMAX_SALARYDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            _attandanceSummaryDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(_attandanceSummaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _LoadAttandanceActivityCombo(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            AttandanceSummaryDS _attandanceSummaryDS = new AttandanceSummaryDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
                       

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAttandanceActivities");
            
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, _attandanceSummaryDS, _attandanceSummaryDS.att_activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            _attandanceSummaryDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(_attandanceSummaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _saveAtt_Summary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError;
            int nRowAffected;
            long _attgenSummaryID = 0, _attsumhistID = 0;

            DBConnectionDS connDS = new DBConnectionDS();
            AttandanceSummaryDS _attandanceSummaryDS = new AttandanceSummaryDS();
           

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            _attandanceSummaryDS.Merge(inputDS.Tables[_attandanceSummaryDS.Att_Summary.TableName], false, MissingSchemaAction.Error);
            _attandanceSummaryDS.AcceptChanges();

            #region To Create Att Summary/Hist...
            OleDbCommand cmdAttSum = new OleDbCommand();
            cmdAttSum.CommandText = "Pro_att_summary_create";
            cmdAttSum.CommandType = CommandType.StoredProcedure;

            if (cmdAttSum == null) return UtilDL.GetCommandNotFound();

            foreach (AttandanceSummaryDS.Att_SummaryRow row in _attandanceSummaryDS.Att_Summary.Rows)
            {
                // If score was previously posted, then SKIP this step.
                //if (!row.IsScoreIDNull() && row.ScoreID > 0)
                //{
                //    genScorePK = row.ScoreID;
                //    isPreviouslyScored = true;
                //    break;
                //}

                _attgenSummaryID = IDGenerator.GetNextGenericPK();
                _attsumhistID = IDGenerator.GetNextGenericPK();
                if (_attgenSummaryID == -1) return UtilDL.GetDBOperationFailed();

                cmdAttSum.Parameters.AddWithValue("p_attsummaryid", _attgenSummaryID);
                cmdAttSum.Parameters.AddWithValue("p_summary_histid", _attsumhistID);


                cmdAttSum.Parameters.AddWithValue("p_employeeid", row.employeeid);
                cmdAttSum.Parameters.AddWithValue("p_summary_month", row.summary_month);
                cmdAttSum.Parameters.AddWithValue("p_att_from_date", row.att_from_date);
                cmdAttSum.Parameters.AddWithValue("p_att_to_date", row.att_to_date);
                cmdAttSum.Parameters.AddWithValue("p_present_day", row.presentctn);
                cmdAttSum.Parameters.AddWithValue("p_changed_present_day", row.new_prectn);
                cmdAttSum.Parameters.AddWithValue("p_absent_day", row.absentctn);
                cmdAttSum.Parameters.AddWithValue("p_changed_absent_day", row.new_absentctn);
                cmdAttSum.Parameters.AddWithValue("p_late_day", row.latectn);
                cmdAttSum.Parameters.AddWithValue("p_changed_late_day", row.new_latectn);
                cmdAttSum.Parameters.AddWithValue("p_weekend_day", row.weekendctn);
                cmdAttSum.Parameters.AddWithValue("p_changed_weekend_day", row.new_weekendctn);
                cmdAttSum.Parameters.AddWithValue("p_holiday", row.holidayctn);
                cmdAttSum.Parameters.AddWithValue("p_changed_holiday", row.new_holidayctn);
                cmdAttSum.Parameters.AddWithValue("p_overtime", row.ot_m);
                cmdAttSum.Parameters.AddWithValue("p_changed_overtime", row.new_ot);
                cmdAttSum.Parameters.AddWithValue("p_fest_overtime", row.festot);
                cmdAttSum.Parameters.AddWithValue("p_changed_fest_overtime", row.new_festot);
                cmdAttSum.Parameters.AddWithValue("p_night_shift", row.ngctn);
                cmdAttSum.Parameters.AddWithValue("p_leavehistorycomments", row.leavehistorycomments);
                cmdAttSum.Parameters.AddWithValue("p_activitiesid", row.activitiesid);
                cmdAttSum.Parameters.AddWithValue("p_senderuserid", row.senderuserid);
                cmdAttSum.Parameters.AddWithValue("p_receiveduserid", row.receiveduserid);
                cmdAttSum.Parameters.AddWithValue("p_receiveddate_time", row.receiveddate_time);
                cmdAttSum.Parameters.AddWithValue("p_priority", row.PRIORITY);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdAttSum, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdAttSum.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getAttandanceSummaryValidateData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError;
            int nRowAffected;
         

            DBConnectionDS connDS = new DBConnectionDS();
            AttandanceSummaryDS _attandanceSummaryDS = new AttandanceSummaryDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            _attandanceSummaryDS.Merge(inputDS.Tables[_attandanceSummaryDS.Att_Summary.TableName], false, MissingSchemaAction.Error);
            _attandanceSummaryDS.AcceptChanges();

            #region To Create Att Summary/Hist...
            OleDbCommand cmdAttSum;
            OleDbDataAdapter adapter;

   

            foreach (AttandanceSummaryDS.Att_SummaryRow row in _attandanceSummaryDS.Att_Summary.Rows)
            {
                cmdAttSum = new OleDbCommand();
                cmdAttSum = DBCommandProvider.GetDBCommand("ValidateEmployeeAttendenceSUmmaryByEmpId");

                if (cmdAttSum == null) return UtilDL.GetCommandNotFound();

                cmdAttSum.Parameters["searchdate"].Value = Convert.ToDateTime(row.att_from_date);
                cmdAttSum.Parameters["employeeid"].Value = Convert.ToInt32(row.employeeid);

                adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmdAttSum;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, _attandanceSummaryDS, _attandanceSummaryDS.att_activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


                _attandanceSummaryDS.AcceptChanges();

            


                //cmdAttSum.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(_attandanceSummaryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _updateAtt_Summary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError;
            int nRowAffected;
            long _attsumhistID = 0;

            DBConnectionDS connDS = new DBConnectionDS();
            AttandanceSummaryDS _attandanceSummaryDS = new AttandanceSummaryDS();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            _attandanceSummaryDS.Merge(inputDS.Tables[_attandanceSummaryDS.Att_Summary.TableName], false, MissingSchemaAction.Error);
            _attandanceSummaryDS.AcceptChanges();

            #region To Create Att Summary/Hist...
            OleDbCommand cmdAttSum = new OleDbCommand();
            cmdAttSum.CommandText = "Pro_att_summary_update";
            cmdAttSum.CommandType = CommandType.StoredProcedure;

            if (cmdAttSum == null) return UtilDL.GetCommandNotFound();

            foreach (AttandanceSummaryDS.Att_SummaryRow row in _attandanceSummaryDS.Att_Summary.Rows)
            {
                // If score was previously posted, then SKIP this step.
                //if (!row.IsScoreIDNull() && row.ScoreID > 0)
                //{
                //    genScorePK = row.ScoreID;
                //    isPreviouslyScored = true;
                //    break;
                //}

                //_attgenSummaryID = IDGenerator.GetNextGenericPK();
                _attsumhistID = IDGenerator.GetNextGenericPK();
                //if (_attgenSummaryID == -1) return UtilDL.GetDBOperationFailed();

                cmdAttSum.Parameters.AddWithValue("p_attsummaryid", row.attsummaryid);
                cmdAttSum.Parameters.AddWithValue("p_summary_histid", _attsumhistID);


                cmdAttSum.Parameters.AddWithValue("p_employeeid", row.employeeid);
                cmdAttSum.Parameters.AddWithValue("p_summary_month", row.summary_month);
                cmdAttSum.Parameters.AddWithValue("p_att_from_date", row.att_from_date);
                cmdAttSum.Parameters.AddWithValue("p_att_to_date", row.att_to_date);
                cmdAttSum.Parameters.AddWithValue("p_present_day", row.presentctn);
                cmdAttSum.Parameters.AddWithValue("p_changed_present_day", row.new_prectn);
                cmdAttSum.Parameters.AddWithValue("p_absent_day", row.absentctn);
                cmdAttSum.Parameters.AddWithValue("p_changed_absent_day", row.new_absentctn);
                cmdAttSum.Parameters.AddWithValue("p_late_day", row.latectn);
                cmdAttSum.Parameters.AddWithValue("p_changed_late_day", row.new_latectn);
                cmdAttSum.Parameters.AddWithValue("p_weekend_day", row.weekendctn);
                cmdAttSum.Parameters.AddWithValue("p_changed_weekend_day", row.new_weekendctn);
                cmdAttSum.Parameters.AddWithValue("p_holiday", row.holidayctn);
                cmdAttSum.Parameters.AddWithValue("p_changed_holiday", row.new_holidayctn);
                cmdAttSum.Parameters.AddWithValue("p_overtime", row.ot_m);
                cmdAttSum.Parameters.AddWithValue("p_changed_overtime", row.new_ot);
                cmdAttSum.Parameters.AddWithValue("p_fest_overtime", row.festot);
                cmdAttSum.Parameters.AddWithValue("p_changed_fest_overtime", row.new_festot);
                cmdAttSum.Parameters.AddWithValue("p_night_shift", row.ngctn);
                cmdAttSum.Parameters.AddWithValue("p_leavehistorycomments", row.leavehistorycomments);
                cmdAttSum.Parameters.AddWithValue("p_activitiesid", row.activitiesid);
                cmdAttSum.Parameters.AddWithValue("p_senderuserid", row.senderuserid);
                cmdAttSum.Parameters.AddWithValue("p_receiveduserid", row.receiveduserid);
                cmdAttSum.Parameters.AddWithValue("p_receiveddate_time", row.receiveddate_time);
                cmdAttSum.Parameters.AddWithValue("p_PRIORITY", row.PRIORITY);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdAttSum, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdAttSum.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _Att_Approval_Path_Create(DataSet inputDS)
        {
            bool bError = false;
            //int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            EmployeeHistoryDS empDS = new EmployeeHistoryDS();
            empDS.Merge(inputDS.Tables[empDS.TransferApprovalPathConfigBulk.TableName], false, MissingSchemaAction.Error);
            empDS.AcceptChanges();
            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ATT_APPROVAL_PATH_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;



            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            cmd.Parameters.Clear();


            OleDbCommand cmd1 = new OleDbCommand();

            foreach (EmployeeHistoryDS.TransferApprovalPathConfigBulkRow row in empDS.TransferApprovalPathConfigBulk.Rows)
            {

                cmd1.CommandText = "PRO_ATT_APPROVAL_PATH_CREATE";
                cmd1.CommandType = CommandType.StoredProcedure;
                // = DBCommandProvider.GetDBCommand("InsertAttApprovePath");

                if (cmd1 == null)
                {
                    return UtilDL.GetCommandNotFound();
                }


                string[] DevidedActivity;

                long pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                Int32 _id = Convert.ToInt32(pKey);

                cmd1.Parameters.AddWithValue("p_ConfigurationID", _id);
                cmd1.Parameters.AddWithValue("p_Priority", row.Priority);
                cmd1.Parameters.AddWithValue("p_Authority", row.Authority);
                
                #region Get LoginID
                string[] Authority = row.Authority.Split(' ', '@');
                if (Authority.Length == 1)
                {
                    cmd1.Parameters.AddWithValue("p_IsLoginUserID", 1);
                }
                else
                {
                    cmd1.Parameters.AddWithValue("p_IsLoginUserID", 0);
                }
                #endregion

                cmd1.Parameters.AddWithValue("p_AuthorityType", row.AuthorityType);

                bError = false;
                int nRowAffectedd = -1;
                nRowAffectedd = ADOController.Instance.ExecuteNonQuery(cmd1, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd1.Dispose();
                cmd1.Parameters.Clear();

                DevidedActivity = row.Activity.Split(' ', ',');
                Int32 _ilin = 0;
                if (row.IsLAIBILITY_NATURE_IDNull() == true)
                {
                    _ilin = 0;
                }
                else 
                {
                    _ilin = row.LAIBILITY_NATURE_ID;
                }
                
                PriorityWiseActivityListCreate(_id, DevidedActivity, connDS.DBConnections[0].ConnectionID, _ilin);

            }

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ATTANDANCE_APPROVAL_PATH_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet PriorityWiseActivityListCreate(long ConfigurationID, string[] TransferActivityList, int ConnectionID, Int32 _li)
        {
            bool bError = false;
            //int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();



            for (int i = 0; i < TransferActivityList.Length; i++)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ATT_PRIORITY_WISE_ACT";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                long pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_PriotityWiseActivityID", pKey);
                cmd.Parameters.AddWithValue("p_Activity", TransferActivityList[i]);
                cmd.Parameters.AddWithValue("p_ConfigurationID", ConfigurationID);
                cmd.Parameters.AddWithValue("p_laubility_nature_id", _li);

                bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, ConnectionID, ref bError);
                cmd.Dispose();
                cmd.Parameters.Clear();
            }

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ATTANDANCE_APPROVAL_PATH_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;

        }
    }
}
