/* *********************************************************************
 *  Compamy: Milllennium Information Solution Limited		 		           *  
 *  Author: Md. Hasinur Rahman Likhon				 				                   *
 *  Comment Date: May 07, 2006									                     *
 ***********************************************************************/

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{

    public class AttendanceMonthDL
    {
        public AttendanceMonthDL()
        {

        }
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {

                case ActionID.NA_ACTION_GET_ATTENDANCEMONTH_LIST:
                    return _getAttendanceMonthList(inputDS);
                case ActionID.NA_ACTION_Q_ALL_ATTENDANCE_DAY:
                    return _getDailyAttendanceMonthlyList(inputDS);
                case ActionID.ACTION_DOES_ATTENDANCEMONTH_EXIST:
                    return _doesAttendentMonthExist(inputDS);
                case ActionID.ACTION_DOES_ATTENDANCE_DAY_EXIST:
                    return _doesDailyAttendentExist(inputDS);
                case ActionID.ACTION_ATTENDANCEMONTH_ADD:
                    return _createAttendanceMonth(inputDS);
                case ActionID.ACTION_ATTENDANCE_DAY_ADD:
                    return _createDailyAttendance(inputDS);
                case ActionID.ACTION_ATTENDANCEMONTH_DEL:
                    return _deleteAttendanceMonth(inputDS);
                case ActionID.ACTION_ATTENDANCEMONTH_UPD:
                    return _updateAttendanceMonth(inputDS);
                case ActionID.ACTION_EMP_ATTENDANCE_FOR_SALARY:
                    return _createAttendanceForSalary(inputDS);
                case ActionID.NA_ACTION_GET_EMP_ATTENDANCE_FOR_SALARY:
                    return _getAttendanceForSalary(inputDS);
                case ActionID.ACTION_CANCEL_ATTENDANCE_ALLOW:
                    return _cancelAttendanceAllow(inputDS);
                case ActionID.NA_ACTION_GET_ALLOW_ATTENDANCE_SP:
                    return _getAllowAttendance(inputDS);
                case ActionID.NA_ACTION_GET_ALLOW_ATTENDANCE_ALL_SP:
                    return _getAllowAttendanceAll(inputDS);
                case ActionID.NA_ACTION_GET_OVERTIME_CATEGORY:
                    return _getOverTimeCategory(inputDS);
                case ActionID.NA_ACTION_GET_ATTENDANCE_BYEMPID_SP:
                    return _getAttendanceByEmpID_SP(inputDS);
                case ActionID.NA_ACTION_GET_ATTENDANCE_SP:
                    return _getAttendance_SP(inputDS);
                case ActionID.NA_ACTION_GET_ATT_SPAM_LIST:
                    return _getAttSpamList(inputDS);
                case ActionID.NA_ACTION_GET_ATT_SPAM_DATA:
                    return _getAtt_SpamData(inputDS);
                case ActionID.ACTION_SEND_ATT_SPAM_FOR_VERIFICATION:
                    return _sendAttSpamForVerification(inputDS);
                case ActionID.NA_ACTION_GET_ATT_SPAM_APPROVAL:
                    return _getAtt_Spam_Approval(inputDS);
                case ActionID.NA_ACTION_GET_ATT_SPAM_DATA_FOR_MODIFY:
                    return _getAttSpam_DataForModify(inputDS);
                case ActionID.ACTION_ATT_SPAM_PARKING_APPROVE:
                    return _AttSpam_ParkingApprove(inputDS);
                case ActionID.ACTION_SEND_ATT_SPAM_FOR_REJECTION:
                    return _sendAttSpamForRejection(inputDS);
                case ActionID.ACTION_ATTENDANCE_FOR_SALARY_UPLOAD_DATA:
                    return _createAttendanceForSalaryUploadData(inputDS);
                case ActionID.ACTION_ATTENDANCE_FOR_SALARY_UPLOAD_DATA_APPROVE:
                    return _approveAttendanceForSalaryUploadData(inputDS);

                case ActionID.ACTION_ATTENDANCE_FOR_SALARY_UPLOAD_DATA_REJECT:
                    return _rejectAttendanceForSalaryUploadData(inputDS); 

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }
        private DataSet _getDailyAttendanceMonthlyList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDailyAttendanceList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            cmd.Parameters["Datetime"].Value = (object)dateDS.DataDates[0].DateValue;

            DailyAttendancePO attPO = new DailyAttendancePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
                            attPO, attPO.Attendance.TableName,
                            connDS.DBConnections[0].ConnectionID,
                            ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_ATTENDANCE_DAY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attPO.AcceptChanges();
            DailyAttendanceDS attDS = new DailyAttendanceDS();
            if (attPO.Attendance.Count > 0)
            {
                foreach (DailyAttendancePO.AttendanceRow poAtt in attPO.Attendance)
                {
                    DailyAttendanceDS.AttendanceRow att = attDS.Attendance.NewAttendanceRow();
                    if (poAtt.IsEmployeeCodeNull() == false)
                    {
                        att.EmployeeCode = poAtt.EmployeeCode;
                    }
                    if (poAtt.IsRecorderNoNull() == false)
                    {
                        att.TimeRecorderNo = poAtt.RecorderNo;
                    }
                    if (poAtt.IsDateTimeNull() == false)
                    {
                        att.DateTime = poAtt.DateTime;
                    }
                    attDS.Attendance.AddAttendanceRow(att);
                    attDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        private DataSet _getAttendanceMonthList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAttendanceMonthList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            cmd.Parameters["MonthYear"].Value = (object)dateDS.DataDates[0].DateValue;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //AttendanceMonthPO attPO = new AttendanceMonthPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, attPO, attPO.AttendanceMonths.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATTENDANCEMONTH_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}
            //attPO.AcceptChanges();
            //AttendanceMonthDS attDS = new AttendanceMonthDS();
            //if (attPO.AttendanceMonths.Count > 0)
            //{
            //    foreach (AttendanceMonthPO.AttendanceMonth poAtt in attPO.AttendanceMonths)
            //    {
            //        AttendanceMonthDS.AttendanceMonth att = attDS.AttendanceMonths.NewAttendanceMonth();
            //        if (poAtt.IsEmployeeCodeNull() == false)
            //        {
            //            att.EmployeeCode = poAtt.EmployeeCode;
            //        }
            //        if (poAtt.IsMonthYearNull() == false)
            //        {
            //            att.MonthYear = poAtt.MonthYear;
            //        }
            //        if (poAtt.IsOrdinaryHoursNull() == false)
            //        {
            //            att.OrdinaryHours = poAtt.OrdinaryHours;
            //        }
            //        if (poAtt.IsOrdinaryAmountNull() == false)
            //        {
            //            att.OrdinaryAmount = poAtt.OrdinaryAmount;
            //        }
            //        if (poAtt.IsAdditionalHoursNull() == false)
            //        {
            //            att.AdditionalHours = poAtt.AdditionalHours;
            //        }
            //        if (poAtt.IsAdditionalAmountNull() == false)
            //        {
            //            att.AdditionalAmount = poAtt.AdditionalAmount;
            //        }
            //        if (poAtt.IsOTHoursNull() == false)
            //        {
            //            att.OTHours = poAtt.OTHours;
            //        }
            //        if (poAtt.IsOTAmountNull() == false)
            //        {
            //            att.OTAmount = poAtt.OTAmount;
            //        }
            //        if (poAtt.IsAttendanceDayNull() == false)
            //        {
            //            att.AttendanceDay = poAtt.AttendanceDay;
            //        }
            //        if (poAtt.IsRemarksNull() == false)
            //        {
            //            att.Remarks = poAtt.Remarks;
            //        }

            //        attDS.AttendanceMonths.AddAttendanceMonth(att);
            //        attDS.AcceptChanges();
            //    }
            //}
            #endregion

            AttendanceMonthDS attDS = new AttendanceMonthDS();
            
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attDS, attDS.AttendanceMonths.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATTENDANCEMONTH_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        private DataSet _doesAttendentMonthExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesAttendanceMonthExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["EmployeeId"].Value = (object)UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
            }
            if (dateDS.DataDates[0].IsDateValueNull() == false)
            {
                cmd.Parameters["MonthYear"].Value = (object)dateDS.DataDates[0].DateValue;
            }
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _doesDailyAttendentExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesDailyAttendanceExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["EmployeeId"].Value = (object)UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
            }
            if (dateDS.DataDates[0].IsDateValueNull() == false)
            {
                cmd.Parameters["DateTime"].Value = (object)dateDS.DataDates[0].DateValue;
            }
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createAttendanceMonth(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AttendanceMonthDS attDS = new AttendanceMonthDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateAttendanceMonth");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            attDS.Merge(inputDS.Tables[attDS.AttendanceMonths.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();
            foreach (AttendanceMonthDS.AttendanceMonth att in attDS.AttendanceMonths)
            {
                //Employee Id
                if (att.IsEmployeeCodeNull() == false)
                {
                    long nId = UtilDL.GetEmployeeId(connDS, att.EmployeeCode);
                    if (nId == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }
                    cmd.Parameters["EmployeeId"].Value = (object)nId;
                }
                else
                {
                    cmd.Parameters["EmployeeId"].Value = System.DBNull.Value;
                }


                //Month Year
                if (att.IsMonthYearNull() == false)
                {
                    cmd.Parameters["MonthYear"].Value = (object)att.MonthYear;
                }
                else
                {
                    cmd.Parameters["MonthYear"].Value = null;
                }
                //Ordinary Hours
                if (att.IsOrdinaryHoursNull() == false)
                {
                    cmd.Parameters["OrdinaryHours"].Value = (object)att.OrdinaryHours;
                }
                else
                {
                    cmd.Parameters["OrdinaryHours"].Value = null;
                }
                //Additional Hours
                if (att.IsAdditionalHoursNull() == false)
                {
                    cmd.Parameters["AdditionalHours"].Value = (object)att.AdditionalHours;
                }
                else
                {
                    cmd.Parameters["AdditionalHours"].Value = null;
                }

                //OT Hours
                if (att.IsOTHoursNull() == false)
                {
                    cmd.Parameters["OTHours"].Value = (object)att.OTHours;
                }
                else
                {
                    cmd.Parameters["OTHours"].Value = null;
                }
                //Ordinary Amount
                if (att.IsOrdinaryAmountNull() == false)
                {
                    cmd.Parameters["OrdinaryAmount"].Value = (object)att.OrdinaryAmount;
                }
                else
                {
                    cmd.Parameters["OrdinaryAmount"].Value = null;
                }
                //Additional Amount
                if (att.IsAdditionalAmountNull() == false)
                {
                    cmd.Parameters["AdditionalAmount"].Value = (object)att.AdditionalAmount;
                }
                else
                {
                    cmd.Parameters["AdditionalAmount"].Value = null;
                }
                //OT Amount
                if (att.IsOTAmountNull() == false)
                {
                    cmd.Parameters["OTAmount"].Value = (object)att.OTAmount;
                }
                else
                {
                    cmd.Parameters["OTAmount"].Value = null;
                }

                //Attendance Day
                if (att.IsAttendanceDayNull() == false)
                {
                    cmd.Parameters["AttendanceDay"].Value = (object)att.AttendanceDay;
                }
                else
                {
                    cmd.Parameters["AttendanceDay"].Value = null;
                }
                //Remarks
                if (att.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)att.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ATTENDANCEMONTH_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _createDailyAttendance(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DailyAttendanceDS attDS = new DailyAttendanceDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateDailyAttendance");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            attDS.Merge(inputDS.Tables[attDS.Attendance.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();
            foreach (DailyAttendanceDS.AttendanceRow att in attDS.Attendance)
            {
                //Employee Id
                if (att.IsEmployeeCodeNull() == false)
                {
                    long nId = UtilDL.GetEmployeeId(connDS, att.EmployeeCode);
                    if (nId == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }
                    cmd.Parameters["EmployeeId"].Value = nId;
                    cmd.Parameters["EmployeeCode"].Value = att.EmployeeCode;

                }
                else
                {
                    cmd.Parameters["EmployeeCode"].Value = null;

                    cmd.Parameters["EmployeeId"].Value = System.DBNull.Value;
                }


                if (att.IsTimeRecorderNoNull() == false)
                {
                    cmd.Parameters["RecorderNo"].Value = att.TimeRecorderNo;
                }
                else
                {
                    cmd.Parameters["RecorderNo"].Value = null;
                }

                if (att.IsDateTimeNull() == false)
                {
                    cmd.Parameters["DateTime"].Value = att.DateTime;
                }
                else
                {
                    cmd.Parameters["DateTime"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ATTENDANCE_DAY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteAttendanceMonth(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteAttendanceMonth");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["EmployeeId"].Value = (object)UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
            cmd.Parameters["MonthYear"].Value = (object)dateDS.DataDates[0].DateValue;


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ATTENDANCEMONTH_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;  // return empty ErrorDS
        }
        private DataSet _updateAttendanceMonth(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AttendanceMonthDS attDS = new AttendanceMonthDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateAttendanceMonth");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            attDS.Merge(inputDS.Tables[attDS.AttendanceMonths.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();
            foreach (AttendanceMonthDS.AttendanceMonth att in attDS.AttendanceMonths)
            {

                //Employee Id
                if (att.IsEmployeeCodeNull() == false)
                {
                    long nId = UtilDL.GetEmployeeId(connDS, att.EmployeeCode);
                    if (nId == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }
                    cmd.Parameters["EmployeeId"].Value = (object)nId;
                }
                else
                {
                    cmd.Parameters["EmployeeId"].Value = System.DBNull.Value;
                }
                //Month Year
                if (att.IsMonthYearNull() == false)
                {
                    cmd.Parameters["MonthYear"].Value = (object)att.MonthYear;
                }
                else
                {
                    cmd.Parameters["MonthYear"].Value = null;
                }
                //Ordinary Hours
                if (att.IsOrdinaryHoursNull() == false)
                {
                    cmd.Parameters["OrdinaryHours"].Value = (object)att.OrdinaryHours;
                }
                else
                {
                    cmd.Parameters["OrdinaryHours"].Value = null;
                }
                //Additional Hours
                if (att.IsAdditionalHoursNull() == false)
                {
                    cmd.Parameters["AdditionalHours"].Value = (object)att.AdditionalHours;
                }
                else
                {
                    cmd.Parameters["AdditionalHours"].Value = null;
                }

                //OT Hours
                if (att.IsOTHoursNull() == false)
                {
                    cmd.Parameters["OTHours"].Value = (object)att.OTHours;
                }
                else
                {
                    cmd.Parameters["OTHours"].Value = null;
                }
                //Ordinary Amount
                if (att.IsOrdinaryAmountNull() == false)
                {
                    cmd.Parameters["OrdinaryAmount"].Value = (object)att.OrdinaryAmount;
                }
                else
                {
                    cmd.Parameters["OrdinaryAmount"].Value = null;
                }
                //Additional Amount
                if (att.IsAdditionalAmountNull() == false)
                {
                    cmd.Parameters["AdditionalAmount"].Value = (object)att.AdditionalAmount;
                }
                else
                {
                    cmd.Parameters["AdditionalAmount"].Value = null;
                }
                //OT Amount
                if (att.IsOTAmountNull() == false)
                {
                    cmd.Parameters["OTAmount"].Value = (object)att.OTAmount;
                }
                else
                {
                    cmd.Parameters["OTAmount"].Value = null;
                }

                //Attendance Day
                if (att.IsAttendanceDayNull() == false)
                {
                    cmd.Parameters["AttendanceDay"].Value = (object)att.AttendanceDay;
                }
                else
                {
                    cmd.Parameters["AttendanceDay"].Value = null;
                }
                //Remarks
                if (att.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)att.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ATTENDANCEMONTH_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createAttendanceForSalaryUploadData(DataSet inputDS)
        {
            
            string messageBody = "", returnMessage = "";
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            EmployeeHistoryDS empHistoryDS = new EmployeeHistoryDS();
            AttendanceMonthDS attDS = new AttendanceMonthDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();

            attDS.Merge(inputDS.Tables[attDS.AttendanceMonths.TableName], false, MissingSchemaAction.Error);
            attDS.Merge(inputDS.Tables[attDS.EmailPacket.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

         
            #region Send Email
            AttendanceMonthDS.EmailPacketRow emailRow = (AttendanceMonthDS.EmailPacketRow)attDS.EmailPacket.Rows[0];

            if (emailRow.IsNotificationByMail)
            {
                string FromEmail = credentialEmailAddress;
                returnMessage = "";

                messageBody = "<b>Dispatcher: </b>" + emailRow.MakerName + " [" + emailRow.MakerCode + "]" +

"<br><br>";
               
                messageBody += "<br><br><br>";
                messageBody += "Click the following link: ";
                messageBody += "<br>";
                messageBody += "<a href='" + emailRow.URL + "'>" + emailRow.URL + "</a>";

                Mail.Mail mail = new Mail.Mail();
                emailRow.CcEmailAddress = FromEmail;

                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, FromEmail, emailRow.ToEmailAddress, emailRow.CcEmailAddress, emailRow.MailingSubject, messageBody, emailRow.MakerName);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, FromEmail, emailRow.ToEmailAddress, emailRow.CcEmailAddress, emailRow.MailingSubject, messageBody, emailRow.MakerName);
                }

                if (returnMessage != "Email successfully sent.")
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ATTENDANCE_FOR_SALARY_UPLOAD_DATA.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion


            #region Previous Data Deletion...
            foreach (AttendanceMonthDS.AttendanceMonth row in attDS.AttendanceMonths.Rows)
            {
                if (row.Att_AllowanceId > 0) 
                {
                    cmd.CommandText = "PRO_ATT_ALLOW_MASTER_DEL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("p_AllowanceID", row.Att_AllowanceId);

                    bool _bError = false;
                    int _nRowAffected = -1;
                    _nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref _bError);

                    if (_bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ATTENDANCE_FOR_SALARY_UPLOAD_DATA.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();
                }
                
            }
            #endregion

            #region Create

            foreach (AttendanceMonthDS.AttendanceMonth row in attDS.AttendanceMonths.Rows)
            {
                cmd.CommandText = "PRO_ATT_SALARY_UPLOAD_DATA";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_Att_AllowanceID", genPK);
                cmd.Parameters.AddWithValue("p_ADParamID", row.ADParamID);
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                cmd.Parameters.AddWithValue("p_AllowanceID", row.AllowanceID);

                cmd.Parameters.AddWithValue("p_MonthYear", row.MonthYear);
                cmd.Parameters.AddWithValue("p_Attendance_Days", row.AttendanceDay);
                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                cmd.Parameters.AddWithValue("p_MakerId", row.MakerId);
                cmd.Parameters.AddWithValue("p_CheckerId", row.CheckerId);
                cmd.Parameters.AddWithValue("p_CDays", row.CDays);
                cmd.Parameters.AddWithValue("p_CMonth", row.CMonth);
                cmd.Parameters.AddWithValue("p_BasicOrGrossMonth", row.BasicOrGrossMonth);
                if (!row.IsUptoDateNull()) cmd.Parameters.AddWithValue("p_UptoDate", row.UptoDate);
                else cmd.Parameters.AddWithValue("p_UptoDate", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();



                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode);
                messageDS.AcceptChanges();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _approveAttendanceForSalaryUploadData(DataSet inputDS)
        {
          
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            AttendanceMonthDS attDS = new AttendanceMonthDS();
            OleDbCommand cmd = new OleDbCommand();

            attDS.Merge(inputDS.Tables[attDS.AttendanceMonths.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Approve

            foreach (AttendanceMonthDS.AttendanceMonth row in attDS.AttendanceMonths.Rows)
            {
                cmd.CommandText = "PRO_ATT_SALARY_DATA_APPROVE";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_Att_AllowanceID", row.Att_AllowanceId);
                cmd.Parameters.AddWithValue("p_remarks", row.Remarks);
                

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode);
                messageDS.AcceptChanges();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _rejectAttendanceForSalaryUploadData(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            AttendanceMonthDS attDS = new AttendanceMonthDS();
            OleDbCommand cmd = new OleDbCommand();

            attDS.Merge(inputDS.Tables[attDS.AttendanceMonths.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Approve

            foreach (AttendanceMonthDS.AttendanceMonth row in attDS.AttendanceMonths.Rows)
            {
                cmd.CommandText = "PRO_ATT_SALARY_DATA_REJECT";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_Att_AllowanceID", row.Att_AllowanceId);
                cmd.Parameters.AddWithValue("p_remarks", row.Remarks);


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode);
                messageDS.AcceptChanges();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createAttendanceForSalary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            

            AttendanceMonthDS attDS = new AttendanceMonthDS();

            attDS.Merge(inputDS.Tables[attDS.AttendanceMonths.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create 

            foreach (AttendanceMonthDS.AttendanceMonth row in attDS.AttendanceMonths.Rows)
            {
                cmd.CommandText = "PRO_ATTENDANCE_SALARY_SAVE";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_Att_AllowanceID", genPK);
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                cmd.Parameters.AddWithValue("p_AllowanceID", row.AllowanceID);

                cmd.Parameters.AddWithValue("p_MonthYear", row.MonthYear);
                cmd.Parameters.AddWithValue("p_Attendance_Days", row.AttendanceDay);
                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (!bError)
                {
                    cmd.Dispose();
                    string[] strAttendanceDates = new string[] { };
                    strAttendanceDates = row.AttendanceDates_String.Split(',', ' ');
                    foreach (var strDate in strAttendanceDates)
                    {
                        cmd.CommandText = "PRO_ATTENDANCE_SALARY_DETAILS";
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (cmd == null) return UtilDL.GetCommandNotFound();

                        cmd.Parameters.AddWithValue("p_Att_AllowanceID", genPK);
                        cmd.Parameters.AddWithValue("p_Attendance_Date", Convert.ToDateTime(strDate));

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Parameters.Clear();
                        cmd.Dispose();
                    }
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeName);
                messageDS.AcceptChanges();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _getAttendanceForSalary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            AttendanceMonthDS attDS = new AttendanceMonthDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            if (stringDS.DataStrings[0].StringValue == "GetEmpAttendanceForSalary")
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmpAttendanceForSalary");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                adapter.SelectCommand = cmd;

                cmd.Parameters["DateStrings1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DateStrings2"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["SiteCode1"].Value = cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["DesignationCode1"].Value = cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["CompanyDivisionID1"].Value = cmd.Parameters["CompanyDivisionID2"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["AllowanceID"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);               
            }
            else if (stringDS.DataStrings[0].StringValue == "GetWeekendDateByDateRange")
            {
                cmd = DBCommandProvider.GetDBCommand("GetWeekendDateByDateRange");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                adapter.SelectCommand = cmd;

                cmd.Parameters["FromDate"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["ToDate"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["WeekendDay"].Value = stringDS.DataStrings[3].StringValue;
            }
            else if (stringDS.DataStrings[0].StringValue == "SalaryForAttendance_DoesExist")
            {
                cmd = DBCommandProvider.GetDBCommand("SalaryForAttendance_DoesExist");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                adapter.SelectCommand = cmd;

                cmd.Parameters["MonthYear"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["AllowanceID"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
            }


            else if (stringDS.DataStrings[0].StringValue == "SalaryForAttendance_UploadData_ForApprove")
            {
                cmd = DBCommandProvider.GetDBCommand("SalaryForAttendance_UploadData_ForApprove");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                adapter.SelectCommand = cmd;

                cmd.Parameters["employeeId"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            }

            else if (stringDS.DataStrings[0].StringValue == "SalaryForAttendance_UploadData")
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmpAttendanceForSalaryUploadData");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                adapter.SelectCommand = cmd;

                cmd.Parameters["ADID"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["MonthYear"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["ADID1"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["ADID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["ComID"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["ComID1"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["SiteID"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["SiteID1"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);

                cmd.Parameters["DepartmentID"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
                cmd.Parameters["DepartmentID1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
                cmd.Parameters["EmployeeType"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                cmd.Parameters["EmployeeType1"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                
            }

            #region Data Fill
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attDS, attDS.AttendanceMonths.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMP_ATTENDANCE_FOR_SALARY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _cancelAttendanceAllow(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            //EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            //employeeHistDS.Merge(inputDS.Tables[employeeHistDS.MajorTopic.TableName], false, MissingSchemaAction.Error);
            //employeeHistDS.AcceptChanges();
            //DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_CANCEL_ATTENDANCE_ALLOW";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            //foreach (EmployeeHistDS.MajorTopicRow row in employeeHistDS.MajorTopic.Rows)
            //{
            cmd.Parameters.AddWithValue("p_ALLOWANCEID", stringDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_MONTHYEAR", stringDS.DataStrings[1].StringValue);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAJOR_TOPICS_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow("Successfully Canceled");
                else messageDS.ErrorMsg.AddErrorMsgRow("Cancel Failed");
            //}

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _getAllowAttendance(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowAttendance_SP");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            cmd.Parameters["EmployeeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["MonthYear"].Value = (object)dateDS.DataDates[0].DateValue;

            AttendanceMonthDS attDS = new AttendanceMonthDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attDS, attDS.AttendanceMonths.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOW_ATTENDANCE_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getAllowAttendanceAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowAttendanceAll_SP");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["CompanyID"].Value = (object)integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["MonthYear"].Value = (object)dateDS.DataDates[0].DateValue;

            AttendanceMonthDS attDS = new AttendanceMonthDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attDS, attDS.AttendanceMonths.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOW_ATTENDANCE_ALL_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getOverTimeCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            AttendanceMonthDS attendanceDS = new AttendanceMonthDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();          
           
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            cmd = DBCommandProvider.GetDBCommand("Get_OverTimeCatetory");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;


            #region Data Fill
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attendanceDS, attendanceDS.OverTimeCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMP_ATTENDANCE_FOR_SALARY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attendanceDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attendanceDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAttendanceByEmpID_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAttendanceByEmpID_SP");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["MonthYear"].Value = (object)dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmployeeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;;

            AttendanceMonthDS attDS = new AttendanceMonthDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attDS, attDS.AttendanceMonths.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATTENDANCE_BYEMPID_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        private DataSet _getAttendance_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS(); 

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAttendance_SP");
            
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["MonthYear"].Value = (object)dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["CompanyID"].Value = (object)integerDS.DataIntegers[2].IntegerValue;

            AttendanceMonthDS attDS = new AttendanceMonthDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attDS, attDS.AttendanceMonths.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATTENDANCE_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getAttSpamList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();            
            AttendanceMonthDS attendanceDS = new AttendanceMonthDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            cmd = DBCommandProvider.GetDBCommand("Get_AttSpamLog");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;


            #region Data Fill
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attendanceDS, attendanceDS.Att_Spam.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATT_SPAM_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attendanceDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attendanceDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getAtt_SpamData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            AttendanceMonthDS attendanceDS = new AttendanceMonthDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataBoolDS boolDS = new DataBoolDS();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            
            long EmployeeID = -1;

            if (stringDS.DataStrings[3].StringValue == "user")
            {
                EmployeeID = UtilDL.GetEmployeeIdbyUserID(connDS, stringDS.DataStrings[2].StringValue);
            }

            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            if (!boolDS.DataBools[0].BoolValue)
            {
                cmd = DBCommandProvider.GetDBCommand("Get_AttSpamData");
                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;
                cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = Convert.ToInt32((object)stringDS.DataStrings[0].StringValue);
                cmd.Parameters["ErrorType1"].Value = cmd.Parameters["ErrorType2"].Value = (object)stringDS.DataStrings[1].StringValue;
                cmd.Parameters["SupervisorID1"].Value = cmd.Parameters["SupervisorID2"].Value = (object)EmployeeID;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("Get_AttSpamRejectedData");
                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;
                cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = Convert.ToInt32((object)stringDS.DataStrings[0].StringValue);
                cmd.Parameters["ErrorType1"].Value = cmd.Parameters["ErrorType2"].Value = (object)stringDS.DataStrings[1].StringValue;
                cmd.Parameters["SupervisorID1"].Value = cmd.Parameters["SupervisorID2"].Value = (object)EmployeeID;
            }
            
            #region Data Fill
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attendanceDS, attendanceDS.Att_Spam.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATT_SPAM_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attendanceDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attendanceDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _sendAttSpamForVerification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();            
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();


            AttendanceMonthDS attDS = new AttendanceMonthDS();
            attDS.Merge(inputDS.Tables[attDS.Att_Spam_Approval_Parking.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create

            foreach (AttendanceMonthDS.Att_Spam_Approval_ParkingRow row in attDS.Att_Spam_Approval_Parking.Rows)
            {
                cmd.CommandText = "PRO_ATT_SPAM_APPROV_PARK_SAVE";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_Att_SpamID", row.Att_SpamID);
                cmd.Parameters.AddWithValue("p_Actual_LogInDateTime", row.Changed_LogInDateTime);
                cmd.Parameters.AddWithValue("p_Actual_LogOutDateTime", row.Changed_LogOutDateTime);        
                cmd.Parameters.AddWithValue("p_GroupID", row.GroupID);
                cmd.Parameters.AddWithValue("p_ShiftingID", row.ShiftingID);              
                cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                cmd.Parameters.AddWithValue("p_Maker", row.Maker);
                cmd.Parameters.AddWithValue("p_RecipientEmployeeCode", row.RecipientEmployeeCode);
                cmd.Parameters.AddWithValue("p_Att_Type", row.Att_Type);
                cmd.Parameters.AddWithValue("p_Holidays_TypeID", row.Holidays_TypeID);
               
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEND_ATT_SPAM_FOR_VERIFICATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }               
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);            
            return returnDS;
        }
        private DataSet _getAtt_Spam_Approval(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            AttendanceMonthDS attendanceDS = new AttendanceMonthDS();


            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            cmd = DBCommandProvider.GetDBCommand("Get_AttSpamForApproval");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;
            cmd.Parameters["ErrorType1"].Value = cmd.Parameters["ErrorType2"].Value = (object)stringDS.DataStrings[0].StringValue;            

            #region Data Fill
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attendanceDS, attendanceDS.Att_Spam_Approval_Parking.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATT_SPAM_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attendanceDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attendanceDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getAttSpam_DataForModify(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            AttendanceMonthDS attendanceDS = new AttendanceMonthDS();


            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            cmd = DBCommandProvider.GetDBCommand("Get_AttSpamDataForModify");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;
            cmd.Parameters["empCode"].Value =  (object)stringDS.DataStrings[0].StringValue;
            cmd.Parameters["LoginDateTime"].Value = Convert.ToDateTime((object)stringDS.DataStrings[1].StringValue);

            #region Data Fill
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, attendanceDS, attendanceDS.Att_Spam.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATT_SPAM_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attendanceDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(attendanceDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _AttSpam_ParkingApprove(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();

            empAttendanceDS EmpAttDS = new empAttendanceDS();
            EmpAttDS.Merge(inputDS.Tables[EmpAttDS.AttendanceMaster.TableName], false, MissingSchemaAction.Error);
            EmpAttDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create

            foreach (empAttendanceDS.AttendanceMasterRow row in EmpAttDS.AttendanceMaster.Rows)
            {
                if (row.Att_Type == "Master")
                {
                    cmd.CommandText = "PRO_ATT_SPAM_APPROVE_MASTER";
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (cmd == null) return UtilDL.GetCommandNotFound();

                    if (!row.IsEmployeeIDNull()) cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    else cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);

                    if (!row.IsActual_LoginDatetimeNull()) cmd.Parameters.AddWithValue("p_Att_Date", row.Actual_LoginDatetime.Date);
                    else cmd.Parameters.AddWithValue("p_Att_Date", DBNull.Value);

                    if (!row.IsActual_LoginDatetimeNull()) cmd.Parameters.AddWithValue("p_Actual_LoginDateTime", row.Actual_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Actual_LoginDateTime", DBNull.Value);

                    if (!row.IsActual_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Actual_LogoutDateTime", row.Actual_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Actual_LogoutDateTime", DBNull.Value);

                    if (!row.IsCal_LoginDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LoginDateTime", row.Cal_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LoginDateTime", DBNull.Value);

                    if (!row.IsCal_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LogoutDateTime", row.Cal_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LogoutDateTime", DBNull.Value);

                    if (!row.IsLoginStatusNull()) cmd.Parameters.AddWithValue("p_LoginStatus", row.LoginStatus);
                    else cmd.Parameters.AddWithValue("p_LoginStatus", DBNull.Value);

                    if (!row.IsStd_Working_MinNull()) cmd.Parameters.AddWithValue("p_Std_Working_Min", row.Std_Working_Min);
                    else cmd.Parameters.AddWithValue("p_Std_Working_Min", DBNull.Value);

                    if (!row.IsWorked_MinNull()) cmd.Parameters.AddWithValue("p_Worked_Min", row.Worked_Min);
                    else cmd.Parameters.AddWithValue("p_Worked_Min", DBNull.Value);

                    if (!row.IsOvertime_MinNull()) cmd.Parameters.AddWithValue("p_OverTime_Min", row.Overtime_Min);
                    else cmd.Parameters.AddWithValue("p_OverTime_Min", DBNull.Value);

                    if (!row.IsEarlyDep_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", row.EarlyDep_Min);
                    else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                    if (!row.IsChanged_ShiftingIDNull()) cmd.Parameters.AddWithValue("p_Changed_ShiftingID", row.Changed_ShiftingID);
                    else cmd.Parameters.AddWithValue("p_Changed_ShiftingID", DBNull.Value);

                    if (!row.IsIsNight_ShiftNull()) cmd.Parameters.AddWithValue("p_IsNight_Shift", row.IsNight_Shift);
                    else cmd.Parameters.AddWithValue("p_IsNight_Shift", DBNull.Value);

                    if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                    else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                    if (!row.IsUpdate_UserNull()) cmd.Parameters.AddWithValue("p_Update_User", row.Update_User);
                    else cmd.Parameters.AddWithValue("p_Update_User", DBNull.Value);

                    if (!row.IsAtt_SpamIDNull()) cmd.Parameters.AddWithValue("p_Att_SpamID", row.Att_SpamID);
                    else cmd.Parameters.AddWithValue("p_Att_SpamID", DBNull.Value);

                    if (!row.IsAtt_Spam_ParkingIDNull()) cmd.Parameters.AddWithValue("p_Att_Spam_ParkingID", row.Att_Spam_ParkingID);
                    else cmd.Parameters.AddWithValue("p_Att_Spam_ParkingID", DBNull.Value);
                }
                else 
                {
                    cmd.CommandText = "PRO_ATT_SPAM_APPROVE_ADDI";                   
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (!row.IsEmployeeIDNull()) cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    else cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);

                    if (!row.IsActual_LoginDatetimeNull()) cmd.Parameters.AddWithValue("p_Att_Date", row.Actual_LoginDatetime.Date);
                    else cmd.Parameters.AddWithValue("p_Att_Date", DBNull.Value);

                    if (!row.IsActual_LoginDatetimeNull()) cmd.Parameters.AddWithValue("p_Actual_LoginDateTime", row.Actual_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Actual_LoginDateTime", DBNull.Value);

                    if (!row.IsActual_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Actual_LogoutDateTime", row.Actual_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Actual_LogoutDateTime", DBNull.Value);

                    if (!row.IsCal_LoginDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LoginDateTime", row.Cal_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LoginDateTime", DBNull.Value);

                    if (!row.IsCal_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LogoutDateTime", row.Cal_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LogoutDateTime", DBNull.Value);

                    if (!row.IsLoginStatusNull()) cmd.Parameters.AddWithValue("p_LoginStatus", row.LoginStatus);
                    else cmd.Parameters.AddWithValue("p_LoginStatus", DBNull.Value);

                    if (!row.IsStd_Working_MinNull()) cmd.Parameters.AddWithValue("p_Std_Working_Min", row.Std_Working_Min);
                    else cmd.Parameters.AddWithValue("p_Std_Working_Min", DBNull.Value);

                    if (!row.IsWorked_MinNull()) cmd.Parameters.AddWithValue("p_Worked_Min", row.Worked_Min);
                    else cmd.Parameters.AddWithValue("p_Worked_Min", DBNull.Value);

                    if (!row.IsOvertime_MinNull()) cmd.Parameters.AddWithValue("p_OverTime_Min", row.Overtime_Min);
                    else cmd.Parameters.AddWithValue("p_OverTime_Min", DBNull.Value);

                    if (!row.IsEarlyDep_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", row.EarlyDep_Min);
                    else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                    if (!row.IsChanged_ShiftingIDNull()) cmd.Parameters.AddWithValue("p_Changed_ShiftingID", row.Changed_ShiftingID);
                    else cmd.Parameters.AddWithValue("p_Changed_ShiftingID", DBNull.Value);

                    if (!row.IsHolidays_TypeIDNull()) cmd.Parameters.AddWithValue("p_Holidays_TypeID", row.Holidays_TypeID);
                    else cmd.Parameters.AddWithValue("p_Holidays_TypeID", DBNull.Value);

                    if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                    else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                    if (!row.IsUpdate_UserNull()) cmd.Parameters.AddWithValue("p_Update_User", row.Update_User);
                    else cmd.Parameters.AddWithValue("p_Update_User", DBNull.Value);

                    if (!row.IsAtt_SpamIDNull()) cmd.Parameters.AddWithValue("p_Att_SpamID", row.Att_SpamID);
                    else cmd.Parameters.AddWithValue("p_Att_SpamID", DBNull.Value);

                    if (!row.IsAtt_Spam_ParkingIDNull()) cmd.Parameters.AddWithValue("p_Att_Spam_ParkingID", row.Att_Spam_ParkingID);
                    else cmd.Parameters.AddWithValue("p_Att_Spam_ParkingID", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEND_ATT_SPAM_FOR_VERIFICATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.Att_Date.Date.ToString());
                else messageDS.ErrorMsg.AddErrorMsgRow(row.Att_Date.Date.ToString());
                

            }
            #endregion
            messageDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _sendAttSpamForRejection(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();


            AttendanceMonthDS attDS = new AttendanceMonthDS();
            attDS.Merge(inputDS.Tables[attDS.Att_Spam_Approval_Parking.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create

            foreach (AttendanceMonthDS.Att_Spam_Approval_ParkingRow row in attDS.Att_Spam_Approval_Parking.Rows)
            {
                cmd.CommandText = "PRO_ATT_SPAM_REJECT";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_Att_SpamID", row.Att_SpamID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEND_ATT_SPAM_FOR_REJECTION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            return returnDS;
        }
    }
}
