/* Compamy: Milllennium Information Solution Limited
 * Author: Km Jarif
 * Comment Date: Feb 26, 2009
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for ConfigurationDL.
    /// </summary>
    class ConfigurationDL
    {
        public ConfigurationDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }

    public DataSet Execute(DataSet inputDS)
    {
        DataSet returnDS = new DataSet();
        ErrorDS errDS = new ErrorDS();

        ActionDS actionDS = new ActionDS();
        actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
        actionDS.AcceptChanges();

        ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
        try
        {
            actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
        }
        catch
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
            err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        switch (actionID)
        {
            case ActionID.ACTION_CONFIGURATIONPROMOTION_ADD:
                return _createConfigPromotion(inputDS);
            case ActionID.NA_ACTION_GET_CONFIGURATIONPROMOTION_LIST:
                return _getConfigPromotionList(inputDS);
            case ActionID.ACTION_CONFIGURATIONPROMOTION_DEL:
                return this._deleteConfigPromotion(inputDS);
            case ActionID.ACTION_CONFIGURATIONPROMOTION_UPD:
                return this._updateConfigPromotion(inputDS);
            case ActionID.ACTION_DOES_CONFIGURATIONPROMOTION_EXIST:
                return this._doesConfigPromotionExist(inputDS);

            default:
                Util.LogInfo("Action ID is not supported in this class.");
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                errDS.Errors.AddError(err);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
        }  // end of switch statement

    }

    private DataSet _createConfigPromotion(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        ConfigurationDS comDS = new ConfigurationDS();
        DBConnectionDS connDS = new DBConnectionDS();
        
        //extract dbconnection 
        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        //create command
        OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateConfigPromotion");
        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        comDS.Merge(inputDS.Tables[comDS.ConfigurationPromotion.TableName], false, MissingSchemaAction.Error);
        comDS.AcceptChanges();

        foreach (ConfigurationDS.ConfigurationPromotionRow com in comDS.ConfigurationPromotion.Rows)
        {
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                UtilDL.GetDBOperationFailed();
            }

            cmd.Parameters["Id"].Value = (object)genPK;
            //code
            if (com.IsCodeNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)com.Code;
            }
            else
            {
                cmd.Parameters["Code"].Value = null;
            }
            //Header Row No
            if (com.IsHeaderRowNoNull() == false)
            {
                cmd.Parameters["HeaderRowNo"].Value = (object)com.HeaderRowNo;

            }
            else
            {
                cmd.Parameters["HeaderRowNo"].Value = null;
            }
            //Starting Row No
            if (com.IsStartingRowNoNull() == false)
            {
                cmd.Parameters["StartingRowNo"].Value = (object)com.StartingRowNo;
            }
            else
            {
                cmd.Parameters["StartingRowNo"].Value = null;
            }
            //Starting Row No
            if (com.IsEndingRowNoNull() == false)
            {
                cmd.Parameters["EndingRowNo"].Value = (object)com.EndingRowNo;
            }
            else
            {
                cmd.Parameters["EndingRowNo"].Value = null;
            }
            //Employee Code
            if (com.IsEmpCodeCNNull() == false)
            {
                cmd.Parameters["EmpCode"].Value = (object)com.EmpCodeCN;
            }
            else
            {
                cmd.Parameters["EmpCode"].Value = null;
            }
            //Effected Date
            if (com.IsEffectDateCNNull() == false)
            {
                cmd.Parameters["EffectDate"].Value = (object)com.EffectDateCN;
            }
            else
            {
                cmd.Parameters["EffectDate"].Value = null;
            }
            //Designation Code
            if (com.IsDesignationCodeCNNull() == false)
            {
                cmd.Parameters["DesignationCode"].Value = (object)com.DesignationCodeCN;
            }
            else
            {
                cmd.Parameters["DesignationCode"].Value = null;
            }
            //Grade Code
            if (com.IsGradeCodeCNNull() == false)
            {
                cmd.Parameters["GradeCode"].Value = (object)com.GradeCodeCN;
            }
            else
            {
                cmd.Parameters["GradeCode"].Value = null;
            }
            //Basic Salary
            if (com.IsBasicSalaryCNNull() == false)
            {
                cmd.Parameters["BasicSalary"].Value = (object)com.BasicSalaryCN;
            }
            else
            {
                cmd.Parameters["BasicSalary"].Value = null;
            }
            //Arrear Status
            if (com.IsArrearStatusCNNull() == false)
            {
                cmd.Parameters["ArrearStatus"].Value = (object)com.ArrearStatusCN;
            }
            else
            {
                cmd.Parameters["ArrearStatus"].Value = null;
            }
            //Remarks
            if (com.IsRemarksNull() == false)
            {
                cmd.Parameters["Remarks"].Value = (object)com.Remarks;
            }
            else
            {
                cmd.Parameters["Remarks"].Value = null;
            }
            //isactive
            if (com.IsIsActiveNull() == false)
            {
                cmd.Parameters["Active"].Value = (object)com.IsActive;
            }
            else
            {
                cmd.Parameters["Active"].Value = null;
            }


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_CONFIGURATIONPROMOTION_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
        }
        errDS.Clear();
        errDS.AcceptChanges();
        return errDS;
    }

    private DataSet _deleteConfigPromotion(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();

        DBConnectionDS connDS = new DBConnectionDS();
        DataStringDS stringDS = new DataStringDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();


        OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteConfigPromotion");
        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
        {
            if (sValue.IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)sValue.StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_CONFIGURATIONPROMOTION_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
        }

        return errDS;  // return empty ErrorDS
    }

    private DataSet _getConfigPromotionList(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();

        DBConnectionDS connDS = new DBConnectionDS();
        DataLongDS longDS = new DataLongDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();
        
        OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetConfigPromotionList");

        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        ConfigurationPO comPO = new ConfigurationPO();

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.Fill(adapter,
          comPO, comPO.CONFIGURATION_PROMOTION.TableName,
          connDS.DBConnections[0].ConnectionID,
          ref bError);
        if (bError == true)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.NA_ACTION_GET_CONFIGURATIONPROMOTION_LIST.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;

        }

        comPO.AcceptChanges();

        ConfigurationDS comDS = new ConfigurationDS();

        if (comPO.CONFIGURATION_PROMOTION.Rows.Count > 0)
        {
            foreach (ConfigurationPO.CONFIGURATION_PROMOTIONRow poCom in comPO.CONFIGURATION_PROMOTION.Rows)
            {
                ConfigurationDS.ConfigurationPromotionRow com = comDS.ConfigurationPromotion.NewConfigurationPromotionRow();
                if (poCom.IsCONFIGURATIONIDNull() == false)
                {
                    com.ConfigurationId = poCom.CONFIGURATIONID;
                }
                if (poCom.IsCONFIGURATIONCODENull() == false)
                {
                    com.Code = poCom.CONFIGURATIONCODE;
                }
                if (poCom.IsHEADERROWNONull() == false)
                {
                    com.HeaderRowNo = poCom.HEADERROWNO;
                }
                if (poCom.IsSTARTINGROWNONull() == false)
                {
                    com.StartingRowNo = poCom.STARTINGROWNO;
                }
                if (poCom.IsENDINGROWNONull() == false)
                {
                    com.EndingRowNo = poCom.ENDINGROWNO;
                }
                if (poCom.IsEMPCODECNNull() == false)
                {
                    com.EmpCodeCN = poCom.EMPCODECN;
                }
                if (poCom.IsEFFECTDATECNNull() == false)
                {
                    com.EffectDateCN = poCom.EFFECTDATECN;
                }
                if (poCom.IsDESIGNATIONCODECNNull() == false)
                {
                    com.DesignationCodeCN = poCom.DESIGNATIONCODECN;
                }
                if (poCom.IsGRADECODECNNull() == false)
                {
                    com.GradeCodeCN = poCom.GRADECODECN;
                }
                if (poCom.IsBASICSALARYCNNull() == false)
                {
                    com.BasicSalaryCN = poCom.BASICSALARYCN;
                }
                if (poCom.IsARREARSTATUSCNNull() == false)
                {
                    com.ArrearStatusCN = poCom.ARREARSTATUSCN;
                }
                if (poCom.IsREMARKSNull() == false)
                {
                    com.Remarks = poCom.REMARKS;
                }
                if (poCom.IsISACTIVENull() == false)
                {
                    com.IsActive = poCom.ISACTIVE;
                }
                
                comDS.ConfigurationPromotion.AddConfigurationPromotionRow(com);
                comDS.AcceptChanges();
            }
        }

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();

        returnDS.Merge(comDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();

        return returnDS;

    }

    private DataSet _updateConfigPromotion(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        ConfigurationDS comDS = new ConfigurationDS();
        DBConnectionDS connDS = new DBConnectionDS();



        //extract dbconnection 
        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        //create command
        OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateConfigPromotion");
        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }
        comDS.Merge(inputDS.Tables[comDS.ConfigurationPromotion.TableName], false, MissingSchemaAction.Error);
        comDS.AcceptChanges();


        foreach (ConfigurationDS.ConfigurationPromotionRow com in comDS.ConfigurationPromotion.Rows)
        {
            //code
            if (com.IsCodeNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)com.Code;
            }
            else
            {
                cmd.Parameters["Code"].Value = null;
            }
            //Header Row No
            if (com.IsHeaderRowNoNull() == false)
            {
                cmd.Parameters["HeaderRowNo"].Value = (object)com.HeaderRowNo;

            }
            else
            {
                cmd.Parameters["HeaderRowNo"].Value = null;
            }
            //Starting Row No
            if (com.IsStartingRowNoNull() == false)
            {
                cmd.Parameters["StartingRowNo"].Value = (object)com.StartingRowNo;
            }
            else
            {
                cmd.Parameters["StartingRowNo"].Value = null;
            }
            //Starting Row No
            if (com.IsEndingRowNoNull() == false)
            {
                cmd.Parameters["EndingRowNo"].Value = (object)com.EndingRowNo;
            }
            else
            {
                cmd.Parameters["EndingRowNo"].Value = null;
            }
            //Employee Code
            if (com.IsEmpCodeCNNull() == false)
            {
                cmd.Parameters["EmpCode"].Value = (object)com.EmpCodeCN;
            }
            else
            {
                cmd.Parameters["EmpCode"].Value = null;
            }
            //Effected Date
            if (com.IsEffectDateCNNull() == false)
            {
                cmd.Parameters["EffectDate"].Value = (object)com.EffectDateCN;
            }
            else
            {
                cmd.Parameters["EffectDate"].Value = null;
            }
            //Designation Code
            if (com.IsDesignationCodeCNNull() == false)
            {
                cmd.Parameters["DesignationCode"].Value = (object)com.DesignationCodeCN;
            }
            else
            {
                cmd.Parameters["DesignationCode"].Value = null;
            }
            //Grade Code
            if (com.IsGradeCodeCNNull() == false)
            {
                cmd.Parameters["GradeCode"].Value = (object)com.GradeCodeCN;
            }
            else
            {
                cmd.Parameters["GradeCode"].Value = null;
            }
            //Basic Salary
            if (com.IsBasicSalaryCNNull() == false)
            {
                cmd.Parameters["BasicSalary"].Value = (object)com.BasicSalaryCN;
            }
            else
            {
                cmd.Parameters["BasicSalary"].Value = null;
            }
            //Arrear Status
            if (com.IsArrearStatusCNNull() == false)
            {
                cmd.Parameters["ArrearStatus"].Value = (object)com.ArrearStatusCN;
            }
            else
            {
                cmd.Parameters["ArrearStatus"].Value = null;
            }
            //Remarks
            if (com.IsRemarksNull() == false)
            {
                cmd.Parameters["Remarks"].Value = (object)com.Remarks;
            }
            else
            {
                cmd.Parameters["Remarks"].Value = null;
            }
            //isactive
            if (com.IsIsActiveNull() == false)
            {
                cmd.Parameters["Active"].Value = (object)com.IsActive;
            }
            else
            {
                cmd.Parameters["Active"].Value = null;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_CONFIGURATIONPROMOTION_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
        }


        errDS.Clear();
        errDS.AcceptChanges();
        return errDS;
    }

    private DataSet _doesConfigPromotionExist(DataSet inputDS)
    {
        DataBoolDS boolDS = new DataBoolDS();
        ErrorDS errDS = new ErrorDS();
        DataSet returnDS = new DataSet();

        DBConnectionDS connDS = new DBConnectionDS();
        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        DataStringDS stringDS = new DataStringDS();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        OleDbCommand cmd = null;
        cmd = DBCommandProvider.GetDBCommand("DoesConfigPromotionExist");
        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        if (stringDS.DataStrings[0].IsStringValueNull() == false)
        {
            cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
        }

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

        if (bError == true)
        {
            errDS.Clear();
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.ACTION_DOES_CONFIGURATIONPROMOTION_EXIST.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }

        boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
        boolDS.AcceptChanges();
        errDS.Clear();
        errDS.AcceptChanges();

        returnDS.Merge(boolDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();
        return returnDS;
    } 
    }
}
