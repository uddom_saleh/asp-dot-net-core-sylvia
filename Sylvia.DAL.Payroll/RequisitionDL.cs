using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    class RequisitionDL
    {
        int p_Req_TypeID;
        string p_UserID;
        DateTime RequisitionDate;

        public RequisitionDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                #region Requisition Type
                case ActionID.ACTION_REQUISITION_TYPE_CREATE:
                    return this._createRequisitionType(inputDS);
                case ActionID.ACTION_REQUISITION_TYPE_UPD:
                    return this._updateRequisitionType(inputDS);
                case ActionID.ACTION_REQUISITION_TYPE_DEL:
                    return this._deleteRequisitionType(inputDS);
                case ActionID.NA_ACTION_GET_REQUISITION_TYPE_LIST:
                    return this._getRequisitionTypeList(inputDS);
                case ActionID.NA_ACTION_GET_REQUISITION_TYPE_LIST_ALL:
                    return this._getRequisitionTypeListAll(inputDS);
                case ActionID.ACTION_DOES_REQUISITION_TYPE_EXIST:
                    return this._doesRequisitionTypeExist(inputDS);
                #endregion


                #region Requisition Category
                case ActionID.ACTION_REQUISITION_CATEGORY_CREATE:

                #endregion

                #region Requisition Input Field
                case ActionID.ACTION_REQUISITION_INPUT_FIELD_CREATE:
                    return this._createRequisitionInputField(inputDS);
                case ActionID.ACTION_REQUISITION_INPUT_FIELD_UPD:
                    return this._updateRequisitionInputField(inputDS);
                case ActionID.NA_ACTION_GET_REQUISITION_INPUT_FIELD_LIST:
                    return this._getRequisitionInputFieldList(inputDS);
                case ActionID.NA_ACTION_GET_REQUISITION_INPUT_FIELD_LIST_ALL:
                    return this._getRequisitionInputFieldListAll(inputDS);
                case ActionID.NA_ACTION_GET_REQ_INPUTFIELD_LIST_BY_RTYPEID:
                    return this._getRequisitionInputFieldListByReqTypeID(inputDS);
                case ActionID.ACTION_DOES_REQUISITION_INPUT_FIELD_EXIST:
                    return this._doesRequisitionInputFieldExist(inputDS);
                // Update by Asif, 14-mar-12
                case ActionID.ACTION_REQUISITION_INPUTFIELD_DEL:
                    return this._deleteRequisitionInputfield(inputDS);
                case ActionID.NA_ACTION_GET_REQ_INPUTFIELD_LIST_BY_RTYPEID_AND_CURRENT_USER:
                    return this._getRequisitionInputFieldListByReqTypeIDAndCurrentUser(inputDS);
                case ActionID.NA_ACTION_GET_REQ_TYPE_LIST_BY_LOGINID:
                    return this._GetRequisitionTypeByLoginID(inputDS);

                    

                // Update end
                #endregion

                #region RIF Config
                case ActionID.ACTION_RIFCONFIG_CREATE:
                    return this._createRIFConfig(inputDS);
                case ActionID.ACTION_RIFCONFIG_DELETE:
                    return this._deleteRIFConfig(inputDS);
                #endregion

                #region Requisition...
                case ActionID.ACTION_REQUISITION_CREATE:
                    return this._createRequisition(inputDS);
                case ActionID.NA_ACTION_REQ_GET_REQUISITION_SUMMERY:
                    return this._GetRequisitionsSummary(inputDS);
                case ActionID.NA_ACTION_REQ_GETTING_EMPLOYEE_HISTORY:
                    return this._GetRequisitionHistData(inputDS);                    
                    
                #endregion
                case ActionID.NA_ACTION_GET_REQUISITION_ACTIVITY_LIST_ALL:
                    return this._GetReqActivityList(inputDS);

                case ActionID.NA_ACTION_GET_REQUISITION_ACTIVITY_LIST_BY_LOGINUSER:
                    return this._GetRequisitionDataByLoginUser(inputDS);

                case ActionID.NA_ACTION_REQUISITION_LIST:
                    return this._GetRequisitionData(inputDS);
                case ActionID.NA_ACTION_GET_REQ_ACTIVITY_LIST_ALL:
                    return this._GetRequisitionActivitiyAll(inputDS);
                case ActionID.NA_ACTION_GET_REQUISITION_LIST_BY_ACTIVITYID:
                    return this._GetRequisitionByActivityID(inputDS);
                case ActionID.NA_ACTION_GET_REQ_LIST_BY_ACTIVITYID_CODE:
                    return this._GetReqByActivityIDCode(inputDS);
                case ActionID.NA_ACTION_GET_EMP_CODE_BY_LOGINID:
                    return this._GetEmpCodeByLoginID(inputDS);
                case ActionID.ACTION_REQUISITION_APPROVATION:
                    return this._RequisitionApprovation(inputDS);
                case ActionID.NA_ACTION_GET_REQ_FIELD_LIST_BY_REQUESTID:
                    return this._GetRequisitionFieldListByReqRequestID(inputDS);
                case ActionID.NA_ACTION_GET_FIELD_LIST_BY_RTYPEID_LOGINID:
                    return this._GetRequisitionInputListByReqTypeIDLoginID(inputDS);
                case ActionID.NA_ACTION_GET_REQ_FIELD_LIST_LIMITMARKABLE:
                    return this._GetReqFieldListLimitMarkable(inputDS);                  
                    
                                      
                    
                    

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        #region Requisition Type
        private DataSet _createRequisitionType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            RequisitionDS requisitionDS = new RequisitionDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REQUISITION_TYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            requisitionDS.Merge(inputDS.Tables[requisitionDS.RequisitionType.TableName], false, MissingSchemaAction.Error);
            requisitionDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (RequisitionDS.RequisitionTypeRow row in requisitionDS.RequisitionType.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_REQUISITIONTYPEID", genPK);

                if (row.IsREQUISITIONTYPECODENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_REQUISITIONTYPECODE", row.REQUISITIONTYPECODE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_REQUISITIONTYPECODE", DBNull.Value);
                }

                if (row.IsREQUISITIONTYPENAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_REQUISITIONTYPENAME", row.REQUISITIONTYPENAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_REQUISITIONTYPENAME", DBNull.Value);
                }

                if (row.IsREQUISITIONTYPEREMARKSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_REQUISITIONTYPEREMARKS", row.REQUISITIONTYPEREMARKS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_REQUISITIONTYPEREMARKS", DBNull.Value);
                }

                if (row.IsISACTIVENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ISACTIVE", row.ISACTIVE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ISACTIVE", DBNull.Value);
                }

                //if (row.IsCurrentUserIDNull() == false)
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", DBNull.Value);
                //}

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQUISITION_TYPE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateRequisitionType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            RequisitionDS requisitionDS = new RequisitionDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REQUISITION_TYPE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            requisitionDS.Merge(inputDS.Tables[requisitionDS.RequisitionType.TableName], false, MissingSchemaAction.Error);
            requisitionDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (RequisitionDS.RequisitionTypeRow row in requisitionDS.RequisitionType.Rows)
            {
                if (row.IsREQUISITIONTYPENAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_RequisitionTypeName", row.REQUISITIONTYPENAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_RequisitionTypeName", DBNull.Value);
                }

                if (row.IsREQUISITIONTYPEREMARKSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_RequisitionTypeRemarks", row.REQUISITIONTYPEREMARKS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_RequisitionTypeRemarks", DBNull.Value);
                }

                if (row.IsISACTIVENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_IsActive", row.ISACTIVE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_IsActive", DBNull.Value);
                }

                if (row.IsREQUISITIONTYPECODENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_RequisitionTypeCode", row.REQUISITIONTYPECODE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_RequisitionTypeCode", DBNull.Value);
                }

                //if (row.IsCurrentUserIDNull() == false)
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", DBNull.Value);
                //}

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQUISITION_TYPE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteRequisitionType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REQUISITION_TYPE__DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_REQUISITIONTYPECODE", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_REQUISITION_TYPE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        # region Created by Asif, 14-mar-12
        private DataSet _deleteRequisitionInputfield(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REQ_INPUTFIELD_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_FIELDNAME", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_REQUISITION_INPUTFIELD_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        # endregion

        private DataSet _getRequisitionTypeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRequisitionTypeList");
            cmd.Parameters["Code"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            RequisitionPO requisitionPO = new RequisitionPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionPO, requisitionPO.RequisitionType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REQUISITION_TYPE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            requisitionPO.AcceptChanges();

            RequisitionDS requisitionDS = new RequisitionDS();
            if (requisitionPO.RequisitionType.Count > 0)
            {
                foreach (RequisitionPO.RequisitionTypeRow poRow in requisitionPO.RequisitionType.Rows)
                {
                    RequisitionDS.RequisitionTypeRow row = requisitionDS.RequisitionType.NewRequisitionTypeRow();
                    if (poRow.IsREQUISITIONTYPEIDNull() == false)
                    {
                        row.REQUISITIONTYPEID = poRow.REQUISITIONTYPEID;
                    }
                    if (poRow.IsREQUISITIONTYPECODENull() == false)
                    {
                        row.REQUISITIONTYPECODE = poRow.REQUISITIONTYPECODE;
                    }
                    if (poRow.IsREQUISITIONTYPENAMENull() == false)
                    {
                        row.REQUISITIONTYPENAME = poRow.REQUISITIONTYPENAME;
                    }
                    if (poRow.IsREQUISITIONTYPEREMARKSNull() == false)
                    {
                        row.REQUISITIONTYPEREMARKS = poRow.REQUISITIONTYPEREMARKS;
                    }
                    if (poRow.IsISACTIVENull() == false)
                    {
                        row.ISACTIVE = poRow.ISACTIVE;
                    }

                    requisitionDS.RequisitionType.AddRequisitionTypeRow(row);
                    requisitionDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getRequisitionTypeListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;
            if (activeStatus.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetRequisitionTypeListAll");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetActiveRequisitionTypeList");
                if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
                else cmd.Parameters["IsActive"].Value = (object)false;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            RequisitionPO requisitionPO = new RequisitionPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionPO, requisitionPO.RequisitionType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REQUISITION_TYPE_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            requisitionPO.AcceptChanges();

            RequisitionDS requisitionDS = new RequisitionDS();
            if (requisitionPO.RequisitionType.Count > 0)
            {
                foreach (RequisitionPO.RequisitionTypeRow poRow in requisitionPO.RequisitionType.Rows)
                {
                    RequisitionDS.RequisitionTypeRow row = requisitionDS.RequisitionType.NewRequisitionTypeRow();
                    if (poRow.IsREQUISITIONTYPEIDNull() == false)
                    {
                        row.REQUISITIONTYPEID = poRow.REQUISITIONTYPEID;
                    }
                    if (poRow.IsREQUISITIONTYPECODENull() == false)
                    {
                        row.REQUISITIONTYPECODE = poRow.REQUISITIONTYPECODE;
                    }
                    if (poRow.IsREQUISITIONTYPENAMENull() == false)
                    {
                        row.REQUISITIONTYPENAME = poRow.REQUISITIONTYPENAME;
                    }
                    if (poRow.IsREQUISITIONTYPEREMARKSNull() == false)
                    {
                        row.REQUISITIONTYPEREMARKS = poRow.REQUISITIONTYPEREMARKS;
                    }
                    if (poRow.IsISACTIVENull() == false)
                    {
                        row.ISACTIVE = poRow.ISACTIVE;
                    }

                    requisitionDS.RequisitionType.AddRequisitionTypeRow(row);
                    requisitionDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _doesRequisitionTypeExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesRequisitionTypeExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_REQUISITION_TYPE_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Requisition Input Field
        private DataSet _createRequisitionInputField(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            RequisitionDS requisitionDS = new RequisitionDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REQ_INPUTFIELD_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            requisitionDS.Merge(inputDS.Tables[requisitionDS.RequisitionInputField.TableName], false, MissingSchemaAction.Error);
            requisitionDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (RequisitionDS.RequisitionInputFieldRow row in requisitionDS.RequisitionInputField.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_RIFID", genPK);

                if (row.IsFIELDNAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_FIELDNAME", row.FIELDNAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_FIELDNAME", DBNull.Value);
                }

                if (row.IsDATATYPENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DATATYPE", row.DATATYPE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DATATYPE", DBNull.Value);
                }

                if (row.IsDATATYPENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_RIFREMARKS", row.RIFREMARKS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_RIFREMARKS", DBNull.Value);
                }

                if (row.IsRIFCODENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_RIFCODE", row.RIFCODE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_RIFCODE", DBNull.Value);
                }

                //if (row.IsCurrentUserIDNull() == false)
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", DBNull.Value);
                //}

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQUISITION_INPUT_FIELD_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateRequisitionInputField(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            RequisitionDS requisitionDS = new RequisitionDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REQ_INPUTFIELD_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            requisitionDS.Merge(inputDS.Tables[requisitionDS.RequisitionInputField.TableName], false, MissingSchemaAction.Error);
            requisitionDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (RequisitionDS.RequisitionInputFieldRow row in requisitionDS.RequisitionInputField.Rows)
            {
                if (row.IsDATATYPENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DATATYPE", row.DATATYPE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DATATYPE", DBNull.Value);
                }

                if (row.IsRIFREMARKSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_RIFREMARKS", row.RIFREMARKS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_RIFREMARKS", DBNull.Value);
                }

                if (row.IsFIELDNAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_FIELDNAME", row.FIELDNAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_FIELDNAME", DBNull.Value);
                }

                if (row.IsRIFCODENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_RIFCODE", row.RIFCODE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_RIFCODE", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQUISITION_INPUT_FIELD_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getRequisitionInputFieldList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRequisitionInputFieldList");
            cmd.Parameters["REQUISITIONINPUTFIELDID"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            RequisitionPO requisitionPO = new RequisitionPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionPO, requisitionPO.RequisitionInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REQUISITION_INPUT_FIELD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            requisitionPO.AcceptChanges();

            RequisitionDS requisitionDS = new RequisitionDS();
            if (requisitionPO.RequisitionInputField.Count > 0)
            {
                foreach (RequisitionPO.RequisitionInputFieldRow poRow in requisitionPO.RequisitionInputField.Rows)
                {
                    RequisitionDS.RequisitionInputFieldRow row = requisitionDS.RequisitionInputField.NewRequisitionInputFieldRow();

                    if (poRow.IsFIELDNAMENull() == false)
                    {
                        row.FIELDNAME = poRow.FIELDNAME;
                    }
                    if (poRow.IsDATATYPENull() == false)
                    {
                        row.DATATYPE = poRow.DATATYPE;
                    }
                    if (poRow.IsRIFREMARKSNull() == false)
                    {
                        row.RIFREMARKS = poRow.RIFREMARKS;
                    }



                    requisitionDS.RequisitionInputField.AddRequisitionInputFieldRow(row);
                    requisitionDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getRequisitionInputFieldListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetRequisitionInputFieldListAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            RequisitionPO requisitionPO = new RequisitionPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionPO, requisitionPO.RequisitionInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REQUISITION_INPUT_FIELD_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            requisitionPO.AcceptChanges();

            RequisitionDS requisitionDS = new RequisitionDS();
            if (requisitionPO.RequisitionInputField.Count > 0)
            {
                foreach (RequisitionPO.RequisitionInputFieldRow poRow in requisitionPO.RequisitionInputField.Rows)
                {
                    RequisitionDS.RequisitionInputFieldRow row = requisitionDS.RequisitionInputField.NewRequisitionInputFieldRow();

                    row.RIFID = poRow.RIFID;

                    if (poRow.IsFIELDNAMENull() == false)
                    {
                        row.FIELDNAME = poRow.FIELDNAME;
                    }
                    if (poRow.IsDATATYPENull() == false)
                    {
                        row.DATATYPE = poRow.DATATYPE;
                    }
                    if (poRow.IsRIFREMARKSNull() == false)
                    {
                        row.RIFREMARKS = poRow.RIFREMARKS;
                    }
                    if (poRow.IsRIFCODENull() == false)
                    {
                        row.RIFCODE = poRow.RIFCODE;
                    }

                    requisitionDS.RequisitionInputField.AddRequisitionInputFieldRow(row);
                    requisitionDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getRequisitionInputFieldListByReqTypeID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRequisitionInputFieldListByRTypeID");
            cmd.Parameters["REQUISITIONTYPEID"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            RequisitionPO requisitionPO = new RequisitionPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionPO, requisitionPO.RequisitionInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REQ_INPUTFIELD_LIST_BY_RTYPEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            requisitionPO.AcceptChanges();

            RequisitionDS requisitionDS = new RequisitionDS();
            if (requisitionPO.RequisitionInputField.Count > 0)
            {
                foreach (RequisitionPO.RequisitionInputFieldRow poRow in requisitionPO.RequisitionInputField.Rows)
                {
                    RequisitionDS.RequisitionInputFieldRow row = requisitionDS.RequisitionInputField.NewRequisitionInputFieldRow();

                    if (poRow.IsRIFIDNull() == false)
                    {
                        row.RIFID = poRow.RIFID;
                    }
                    if (poRow.IsFIELDNAMENull() == false)
                    {
                        row.FIELDNAME = poRow.FIELDNAME;
                    }
                    if (poRow.IsDATATYPENull() == false)
                    {
                        row.DATATYPE = poRow.DATATYPE;
                    }
                    if (poRow.IsISMANDATORYNull() == false)
                    {
                        row.ISMANDATORY = poRow.ISMANDATORY;
                    }                
                    if (poRow.IsRIFCONFIGIDNull() == false)
                    {
                        row.RIFCONFIGID = poRow.RIFCONFIGID;
                    }
                    if (poRow.IsLIMIT_MARKABLENull() == false)
                    {
                        row.LIMIT_MARKABLE = poRow.LIMIT_MARKABLE;
                    }  
                   

                    requisitionDS.RequisitionInputField.AddRequisitionInputFieldRow(row);
                    requisitionDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        # region Created by Asif, 14-mar-12
        private DataSet _getRequisitionInputFieldListByReqTypeIDAndCurrentUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRequisitionInputFieldListByRTypeIDAndCurrentUser");
            cmd.Parameters["REQUISITIONTYPEID"].Value = (object)StringDS.DataStrings[1].StringValue;
            cmd.Parameters["CURRENTUSER"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            RequisitionPO requisitionPO = new RequisitionPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionPO, requisitionPO.RequisitionInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REQ_INPUTFIELD_LIST_BY_RTYPEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            requisitionPO.AcceptChanges();

            RequisitionDS requisitionDS = new RequisitionDS();
            if (requisitionPO.RequisitionInputField.Count > 0)
            {
                foreach (RequisitionPO.RequisitionInputFieldRow poRow in requisitionPO.RequisitionInputField.Rows)
                {
                    RequisitionDS.RequisitionInputFieldRow row = requisitionDS.RequisitionInputField.NewRequisitionInputFieldRow();

                    if (poRow.IsRIFIDNull() == false)
                    {
                        row.RIFID = poRow.RIFID;
                    }
                    if (poRow.IsFIELDNAMENull() == false)
                    {
                        row.FIELDNAME = poRow.FIELDNAME;
                    }
                    if (poRow.IsDATATYPENull() == false)
                    {
                        row.DATATYPE = poRow.DATATYPE;
                    }
                    if (poRow.IsISMANDATORYNull() == false)
                    {
                        row.ISMANDATORY = poRow.ISMANDATORY;
                    }
                    if (poRow.IsORIGINALVALUENull() == false)
                    {
                        row.ORIGINALVALUE = poRow.ORIGINALVALUE;
                    }
                    if (poRow.IsRIFCONFIGIDNull() == false)
                    {
                        row.RIFCONFIGID = poRow.RIFCONFIGID;
                    }

                    requisitionDS.RequisitionInputField.AddRequisitionInputFieldRow(row);
                    requisitionDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        # endregion

        private DataSet _doesRequisitionInputFieldExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesRequisitionInputFieldExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["FieldName"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_REQUISITION_INPUT_FIELD_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region RIF Config
        private DataSet _createRIFConfig(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            RequisitionDS requisitionDS = new RequisitionDS();
            requisitionDS.Merge(inputDS.Tables[requisitionDS.RIFConfig.TableName], false, MissingSchemaAction.Error);
            requisitionDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool bError = false;
            int nRowAffected = -1;

            #region Delete RIF Config

            cmd.CommandText = "PRO_RIFCONFIG_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (RequisitionDS.RIFConfigRow row in requisitionDS.RIFConfig.Rows)
            {
                cmd.Parameters.AddWithValue("p_REQUISITIONTYPEID", row.REQUISITIONTYPEID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RIFCONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                break;
            }
            cmd.Dispose();
            #endregion

            #region Create RIF Config

            cmd.CommandText = "PRO_RIFCONFIG_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (RequisitionDS.RIFConfigRow row in requisitionDS.RIFConfig.Rows)
            {
                // Update by Asif, 15-mar-12
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }


                //cmd.Parameters.AddWithValue("p_RIFCONFIGID", genPK);
                // Update end
                cmd.Parameters.AddWithValue("p_REQUISITIONTYPEID", row.REQUISITIONTYPEID);
                cmd.Parameters.AddWithValue("p_RIFID", row.RIFID);
                cmd.Parameters.AddWithValue("p_ISMANDATORY", row.ISMANDATORY);
                cmd.Parameters.AddWithValue("p_RIFCONFIGID",genPK);
                cmd.Parameters.AddWithValue("p_LIMITMARKABLE", row.LIMIT_MARKABLE);
                
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RIFCONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteRIFConfig(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_RIFCONFIG_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_REQUISITIONTYPEID", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_RIFCONFIG_DELETE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        #endregion

        #region Requisition
        private DataSet _createRequisition(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            RequisitionDS requisitionDS = new RequisitionDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REQUISITION_CREATE_FINAL";
            cmd.CommandType = CommandType.StoredProcedure;
            
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            //-================
            OleDbCommand cmd2 = new OleDbCommand();
            cmd2.CommandText = "PRO_REQ_REQUEST_HIST_CREATE";
            cmd2.CommandType = CommandType.StoredProcedure;

            if (cmd2 == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            //================
            requisitionDS.Merge(inputDS.Tables[requisitionDS.Requisition.TableName], false, MissingSchemaAction.Error);
            requisitionDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            // Update by Asif, 14-mar12
            DataStringDS stringDS = new DataStringDS();
            // Update end

            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            long genPK2 = IDGenerator.GetNextGenericPK();
            if (genPK2 == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }
            Int32 receiverUserId = 0;
            string receiverEmail = "";
            foreach (RequisitionDS.RequisitionRow row in requisitionDS.Requisition.Rows)
            {              
                cmd.Parameters.AddWithValue("P_REQ_REQUIESTID", genPK);

                if (row.IsREQUISITIONTYPEIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("P_REQ_TYPEID", row.REQUISITIONTYPEID);
                    p_Req_TypeID = row.REQUISITIONTYPEID;
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_REQ_TYPEID", DBNull.Value);
                }

                if (row.IsCurrentUserIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("P_USERID", row.CurrentUserID);
                    p_UserID = row.CurrentUserID; 
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_USERID", DBNull.Value);
                }


                if (row.IsORIGINALVALUENull() == false)
                {
                    cmd.Parameters.AddWithValue("P_ORIGINALVALUE", row.ORIGINALVALUE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_ORIGINALVALUE", DBNull.Value);
                }

                if (row.IsCHANGINGVALUENull() == false)
                {
                    cmd.Parameters.AddWithValue("P_CHANGEVALUE", row.CHANGINGVALUE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_CHANGEVALUE", DBNull.Value);
                }


                if (row.IsRIFIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("P_RIFID", row.RIFID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_RIFID", DBNull.Value);
                }  

                if(row.IsAppliedDateNull() == false)
                {
                    RequisitionDate = row.AppliedDate;
                }
                if (!row.IsReceiverUserIdNull()) receiverUserId = row.ReceiverUserId;
                if (!row.IsReceiverEmailNull()) receiverEmail = row.ReceiverEmail;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQUISITION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }


            cmd2.Parameters.AddWithValue("P_REQ_REQUIESTID", genPK);
            cmd2.Parameters.AddWithValue("P_REQ_TYPEID", p_Req_TypeID);
            cmd2.Parameters.AddWithValue("P_USERID", p_UserID);
            cmd2.Parameters.AddWithValue("P_REQ_HISTORYID", genPK2);
            cmd2.Parameters.AddWithValue("p_ACTIVITIESID", Convert.ToInt32(RequisitionActivities.PendingApproval));
            cmd2.Parameters.AddWithValue("p_RequisitionDate", RequisitionDate);
            cmd2.Parameters.AddWithValue("p_RequisitionDate", receiverUserId);
            int nRowAffected2 = -1;
            bool bError2 = false;

            nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd2, connDS.DBConnections[0].ConnectionID, ref bError2);
            cmd2.Parameters.Clear();

            errDS.Clear();
            errDS.AcceptChanges();
            //return errDS;

            // Update by Asif, 14-mar-12
            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
            // Update end
        }
        #endregion

        #region Requisition Updated by Shakir 30/08/2012 
        private DataSet _GetRequisitionTypeByLoginID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (StringDS.DataStrings[0].StringValue != "admin")
            {
                cmd = DBCommandProvider.GetDBCommand("GetRequTypesByLoginID");
                cmd.Parameters["LOGINID"].Value = (object)StringDS.DataStrings[0].StringValue;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetRequTypesByAdmin");  
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }            

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RequisitionType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        #endregion

        private DataSet _GetRequisitionsSummary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetRequisitionSummary");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

                     

            //lmsLeaveDS leaveDS = new lmsLeaveDS();
            RequisitionDS RequisitionDS = new RequisitionDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, RequisitionDS, RequisitionDS.Requisition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_SUMMERY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            RequisitionDS.AcceptChanges();

     

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(RequisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetRequisitionHistData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetReqEmployeeHistory");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
           
            //lmsEmployeeHistoryDS lmsEmpHistDS = new lmsEmployeeHistoryDS();
            RequisitionDS RequisitionDS = new RequisitionDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, RequisitionDS, RequisitionDS.Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HISTORY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            //lmsEmpHistPO.AcceptChanges();
            RequisitionDS.AcceptChanges();  
                   

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(RequisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetReqActivityList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetActivityListAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetRequisitionData(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetRequisitionSummary");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            //cmd.Parameters["RECEIVEDUSERID1"].Value = (object)StringDS.DataStrings[9].StringValue;
            //cmd.Parameters["RECEIVEDUSERID2"].Value = (object)StringDS.DataStrings[9].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Requisition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;




        }

        private DataSet _GetRequisitionActivitiyAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;
            string LoginID = StringDS.DataStrings[1].StringValue;
            string ReqAuthorizationKey = StringDS.DataStrings[2].StringValue;


            if (LoginID != "admin" && ReqAuthorizationKey =="False")
            {
                cmd = DBCommandProvider.GetDBCommand("GetActivitiesOnlyForGeneral");
            }
            else if (LoginID != "admin" && ReqAuthorizationKey == "True")
            {
                cmd = DBCommandProvider.GetDBCommand("GetActivitiesForSupperVisor");
            }
            else
            {
               cmd = DBCommandProvider.GetDBCommand("GetActivitiesListListAll");

            }
            LoginID = "";
            ReqAuthorizationKey = "";

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetRequisitionByActivityID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRequisitionByActivityIDDate");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["REQACTIVITYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            cmd.Parameters["DateFrom"].Value =(object)StringDS.DataStrings[1].StringValue;
            cmd.Parameters["DateTo"].Value =(object)StringDS.DataStrings[2].StringValue;


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Requisition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _GetReqByActivityIDCode(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetReqByActivityIDDateCode");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["REQACTIVITYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            cmd.Parameters["DateFrom"].Value = (object)StringDS.DataStrings[1].StringValue;
            cmd.Parameters["DateTo"].Value = (object)StringDS.DataStrings[2].StringValue;
            cmd.Parameters["Code"].Value = (object)StringDS.DataStrings[3].StringValue;


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Requisition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _GetEmpCodeByLoginID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmpCodebyLoginID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["LoginID"].Value = (object)StringDS.DataStrings[0].StringValue;


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Requisition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _RequisitionApprovation(DataSet inputDS)
        {

            string messageBody = "", URL = "";

            #region Get Email Information from UtilDL
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            //string credentialEmailAddress = "shakir@mislbd.com";
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            //string credentialEmailPwd = "*****";
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion


            //--------------------------------------------------------
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            //cmd.CommandText = "PRO_REQUISITION_APPROVATION";
            //cmd.CommandType = CommandType.StoredProcedure;

            //if (cmd == null)
            //{
            //    return UtilDL.GetCommandNotFound();
            //}

            RequisitionDS RequisitionDS = new RequisitionDS();
            RequisitionDS.Merge(inputDS.Tables[RequisitionDS.ReqHistory.TableName], false, MissingSchemaAction.Error);
            RequisitionDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();



            foreach (RequisitionDS.ReqHistoryRow row in RequisitionDS.ReqHistory.Rows)
            {
                cmd.CommandText = "PRO_REQUISITION_APPROVATION";
                cmd.CommandType = CommandType.StoredProcedure;

                //if (row.ActivitiesID == 101 || row.ActivitiesID == 104 || row.ActivitiesID == 105)
                //{
                //    cmd.CommandText = "PRO_REQ_APPROVATION_FINISHING";
                //    cmd.CommandType = CommandType.StoredProcedure;
                //}
                //else
                //{
                //    cmd.CommandText = "PRO_REQUISITION_APPROVATION";
                //    cmd.CommandType = CommandType.StoredProcedure;
                //}


                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_ReqHistoryID", genPK);

                if (row.IsREQUISITIONREQUESTIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ReqRequestID", row.REQUISITIONREQUESTID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ReqRequestID", DBNull.Value);
                }


                if (row.IsSenderUserIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SendUserID", row.SenderUserID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SendUserID", DBNull.Value);
                }

                if (row.IsLoginIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LoginID", row.LoginID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LoginID", DBNull.Value);
                }

                if (row.IsActivitiesIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ActivityID", row.ActivitiesID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ActivityID", DBNull.Value);
                }
                if (row.IsReceivedUserIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ReceivedUserID", row.ReceivedUserID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ReceivedUserID", DBNull.Value);
                }
                if (row.IsApproval_PriorityNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Approval_Priority", row.Approval_Priority);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Approval_Priority", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_RECEIVEDDATETIME", DateTime.Now.Date);

                //

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQUISITION_CATEGORY_ASSIGN.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();

                #region Email Body Creation...........
                URL = row.Url;
                URL += "?pVal=Requisition";
                messageBody += "<b>Applicant: </b>" + row.APPHOLDER + " [" + row.EmployeeCode + "]<br>";
                messageBody += "<b>Requisition Name : </b>" + row.RequisitionTypeName + "<br>";
                messageBody += "<br><br><br>";
                messageBody += "Click the following link for details: ";
                messageBody += "<br>";
                messageBody += "<a href='" + URL + "'>" + URL + "</a>";

                #endregion

                #region Email Send............
                Mail.Mail mail = new Mail.Mail();
                if (row.IsMailNotificationNull() == false)
                {
                    if (IsEMailSendWithCredential)
                    {
                        //mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, emailRow.FromEmailAddress, emailRow.EmailAddress, emailRow.MailingSubject, messageBody);
                        mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, credentialEmailAddress, row.Email, "", "Requisition " + row.ACTIVITYFRIENDLYNAME, messageBody, row.APPHOLDER);

                    }
                }
                #endregion
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetRequisitionFieldListByReqRequestID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRequisitionFieldListByRequestID");
            cmd.Parameters["REQUISITIONREQUESTID"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RequisitionInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;  
        }

        private DataSet _GetRequisitionInputListByReqTypeIDLoginID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRequisitionFieldListByRTypeIDLoginID");
            cmd.Parameters["REQUISITIONTYPEID"].Value = (object)StringDS.DataStrings[0].StringValue;
            cmd.Parameters["UserLoginID"].Value = (object)StringDS.DataStrings[1].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            RequisitionPO requisitionPO = new RequisitionPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionPO, requisitionPO.RequisitionInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REQ_INPUTFIELD_LIST_BY_RTYPEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            requisitionPO.AcceptChanges();

            RequisitionDS requisitionDS = new RequisitionDS();
            if (requisitionPO.RequisitionInputField.Count > 0)
            {
                foreach (RequisitionPO.RequisitionInputFieldRow poRow in requisitionPO.RequisitionInputField.Rows)
                {
                    RequisitionDS.RequisitionInputFieldRow row = requisitionDS.RequisitionInputField.NewRequisitionInputFieldRow();

                    if (poRow.IsRIFIDNull() == false)
                    {
                        row.RIFID = poRow.RIFID;
                    }
                    if (poRow.IsFIELDNAMENull() == false)
                    {
                        row.FIELDNAME = poRow.FIELDNAME;
                    }
                    if (poRow.IsDATATYPENull() == false)
                    {
                        row.DATATYPE = poRow.DATATYPE;
                    }
                    if (poRow.IsISMANDATORYNull() == false)
                    {
                        row.ISMANDATORY = poRow.ISMANDATORY;
                    }
                    
                    if (poRow.IsRIFCONFIGIDNull() == false)
                    {
                        row.RIFCONFIGID = poRow.RIFCONFIGID;
                    }

                    if (poRow.IsMAXVALUE1Null() == false)
                    {
                        row.MAXVALUE1 = poRow.MAXVALUE1;
                    }

                    requisitionDS.RequisitionInputField.AddRequisitionInputFieldRow(row);
                    requisitionDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetReqFieldListLimitMarkable(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFieldListLimtMarkable");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RequisitionInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetRequisitionDataByLoginUser(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetRequisitionSummaryByLoginUser");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["EmployeeCode1"].Value = (object)StringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = (object)StringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode3"].Value = (object)StringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode4"].Value = (object)StringDS.DataStrings[1].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Requisition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS.Requisition);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;            
        }
    }
}