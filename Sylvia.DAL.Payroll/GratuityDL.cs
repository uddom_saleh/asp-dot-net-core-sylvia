using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using System.Globalization;

namespace Sylvia.DAL.Payroll
{

    public class GratuityDL
    {
        public GratuityDL()
        {
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_ACTION_GET_GR_DATA:
                    return this._getGR_Data(inputDS);

                case ActionID.ACTION_GR_FUND_TYPE_ADD:
                    return _createGR_Fund_Type(inputDS);
                case ActionID.ACTION_GR_FUND_TYPE_UPP:
                    return _updateGR_Fund_Type(inputDS);
                case ActionID.ACTION_GR_FUND_TYPE_DEL:
                    return _deleteGR_Fund_Type(inputDS);
                case ActionID.ACTION_GR_SHARE_RATE_UPP:
                    return _updateGR_Share_Rate(inputDS);
                case ActionID.ACTION_GR_MEMBER_UPP:
                    return _updateGR_Member(inputDS);
                case ActionID.NA_GET_GR_ABLE_TO_INVEST_AMOUNT:
                    return this._GetAbleToInvestAmount(inputDS);
                case ActionID.ACTION_GR_CREATE_INVESTMENT:
                    return this._CreateInvestment_GR(inputDS);
                case ActionID.ACTION_GR_UPDATE_INVESTMENT:
                    return this._UpdateInvestment_GR(inputDS);
                case ActionID.NA_GET_GR_INVESTMENT_LIST:
                    return this._GetInvestmentList_GR(inputDS);
                case ActionID.NA_GET_GR_INVESTMENT_DETAILS_BY_INVID:
                    return this._GetInvestmentDetailsByInvID_GR(inputDS);
                case ActionID.ACTION_GR_DELETE_LAST_INVESTMENT:
                    return this._CancelLastInvestment_GR(inputDS);
                case ActionID.ACTION_GR_INVESTMENT_SETTLEMENT_CREATE:
                    return this._InvestmentSettlementCreate_GR(inputDS);

                case ActionID.NA_GET_GR_PROVISION_BENEFICIARY_LIST:
                    return this._GetProvisionBeneficiaryList_GR(inputDS);
                case ActionID.NA_GET_GR_INVESTMENT_PROVISION_LIST:
                    return this._GetInvestmentProvisionList_GR(inputDS);
                case ActionID.ACTION_GR_PROVISION_CREATION:
                    return this._ProvisionCreation_GR(inputDS);

                case ActionID.ACTION_GR_TERMS_OF_SEPARATION_SAVE:
                    return this._SaveTermsOfSeperation_GR(inputDS);
                case ActionID.NA_GET_GR_TERMS_OF_SEPARATION:
                    return this._GetTermsOfSeperation_GR(inputDS);

                case ActionID.NA_GET_GR_EMPLOYEE_LIST_FOR_SEPERATION:
                    return this._GetEmployeeListForSeperation_GR(inputDS);
                case ActionID.NA_GET_GR_EMPLOYEE_LIST_FOR_PAYMENT:
                    return this._GetEmployeeListForPayment_GR(inputDS);
                case ActionID.NA_GET_GR_PAYMENT_AMOUNT_FOR_SEPERATION:
                    return this._GetPaymentAmountsForSeperation_GR(inputDS);
                case ActionID.ACTION_GR_EMPLOYEE_SEPARATION:
                    return this._EmployeeSeperation_GR(inputDS);
                case ActionID.NA_GET_GR_PAYMENT_AMOUNT_FOR_PAYMENT:
                    return this._GetPaymentAmountsForPayment_GR(inputDS);
                case ActionID.ACTION_GR_EMPLOYEE_PAYMENT:
                    return this._EmployeePayment_GR(inputDS);

                case ActionID.NA_GET_GR_FUND_HEAD_LIST:
                    return this._GetFundHeadList_GR(inputDS);
                case ActionID.NA_GET_GR_FUND_RECIEVE_DETAILS:
                    return this._GetFundRecieveDetails_GR(inputDS);
                case ActionID.ACTION_GR_FUND_RECIEVE:
                    return this._FundRecieve_GR(inputDS);

                case ActionID.NA_GET_GR_PROVISION_CALCULATED_DATA:
                    return this._GetProvisionCalculatedData_GR(inputDS);
                case ActionID.ACTION_GR_SAVE_PROVISION_CALCULATED_DATA:
                    return this._SaveProvisionCalculatedData_GR(inputDS);
                case ActionID.NA_GET_GR_PROVISIONED_PROFIT_FOR_EMPLOYEE:
                    return this._GetProvisionedProfit_ForEmployee_GR(inputDS);
                case ActionID.ACTION_GR_DISTRIBUTE_PROVISIONED_PROFIT:
                    return this._DistributeProvisionedProfit_GR(inputDS);
                case ActionID.NA_GET_GR_DISTRIBUTED_PROVISION_DATA:
                    return this.GetDistributedProvisionData_GR(inputDS);

                case ActionID.NA_GET_GR_PENDING_DISTRIBUTION:
                    return this._GetPendingDistributions_GR(inputDS);

                case ActionID.ACTION_GR_FUND_JOURNAL_ENTRY:
                    return this._FundJournalEntry_GR(inputDS);
                case ActionID.NA_GET_GR_HEADWISE_AVAILABLE_AMOUNT:
                    return this._GetHeadWiseAvailableAmount_GR(inputDS);
                case ActionID.NA_GET_GR_JOURNAL_EMPLOYEE_BY_GR_MEMBDATE:
                    return this._GetEmployeeList_By_GRMembDate(inputDS);
                case ActionID.ACTION_GR_EMPLOYEE_JOURNAL_ENTRY:
                    return this._EmployeeJournalEntry_GR(inputDS);

                case ActionID.NA_GET_DISTINCT_FUND_YEAR_GR:
                    return this._GetDistinctFundYear_GR(inputDS);
                case ActionID.NA_GET_GR_RECIEVED_FUND_LIST:
                    return this._GetRecievedFundList_GR(inputDS);
                case ActionID.NA_GET_GR_TOTAL_PAYABLE_TO_LSC:
                    return this.GetTotalPayableToLSC_GR(inputDS);

                case ActionID.NA_GET_GR_EMPLOYEE_BALANCE:
                    return this._GetEmployeeBalanceRecord_GR(inputDS);
                case ActionID.ACTION_GR_MODIFY_EMPLOYEE_BALANCE:
                    return this._ModifyEmployeeBalanceRecord_GR(inputDS);
                case ActionID.NA_GET_GR_FUND_BALANCE:
                    return this._GetFundHeadBalanceRecord_GR(inputDS);
                case ActionID.ACTION_GR_MODIFY_FUND_BALANCE:
                    return this._ModifyFundHeadBalanceRecord_GR(inputDS);
                case ActionID.NA_GET_GR_BENEFICIARY_LIST_TO_FUND_RECIEVE:
                    return this._GetBeneficiaryList_To_FundRecieve_GR(inputDS);
                case ActionID.ACTION_GR_FUND_RECIEVE_EMPLOYEE_WISE:
                    return this._Exceptional_FundRecieve_GR(inputDS);
                case ActionID.NA_GET_GR_EMPLOYEE_LIST_FOR_GEN_PAYMENT:
                    return this._GetGR_EmpListForGenaralPayment(inputDS);
                case ActionID.ACTION_GR_PAYMENT_GENERAL:
                    return this._GR_Payment_General(inputDS);
                case ActionID.NA_GET_GR_PAYMENT_AMOUNT_GENERAL:
                    return this._GetGR_PaymentAmountsGeneral(inputDS);
                case ActionID.NA_GET_GR_SEPARATION_INFO:
                    return this._GetGR_SeparationInfo(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    AppLogger.LogError(returnDS.GetXml());
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getGR_Data(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError = false;
            int nRowAffected = -1;

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            GratuityDS grDS = new GratuityDS();



            if (stringDS.DataStrings[0].StringValue == "FundTypeExistencyCheck")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFundTypeExistency_GR");
                cmd.Parameters["Code"].Value = stringDS.DataStrings[1].StringValue;

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.Data_Existence.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            else if (stringDS.DataStrings[0].StringValue == "GR_Fund_Type_All")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGR_Fund_Type_All");

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_FundType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            else if (stringDS.DataStrings[0].StringValue == "GR_Share_Rate_by_Fund_Type_ID")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGR_ShareRateByTypeID");
                cmd.Parameters["Fund_Type_ID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_ShareRate.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            else if (stringDS.DataStrings[0].StringValue == "GR_Memeber_List")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGR_Memeber_List");
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;

                cmd.Parameters["BranchID"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["BranchID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);

                cmd.Parameters["DepartmentCode"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;

                cmd.Parameters["DesignationCode"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;

                cmd.Parameters["Fund_Type_ID"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
                cmd.Parameters["Fund_Type_ID2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);

                cmd.Parameters["EmployeeStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                cmd.Parameters["EmployeeStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_FundMember.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GR_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            grDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }
        private DataSet _createGR_Fund_Type(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            GratuityDS grDS = new GratuityDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_GR_FUND_TYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            grDS.Merge(inputDS.Tables[grDS.GR_FundType.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            foreach (GratuityDS.GR_FundTypeRow row in grDS.GR_FundType)
            {

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();


                cmd.Parameters.AddWithValue("p_FT_Id", (object)genPK);
                cmd.Parameters.AddWithValue("p_FT_Code", (object)row.FT_Code);

                if (!row.IsFT_NameNull()) cmd.Parameters.AddWithValue("p_FT_Name", row.FT_Name);
                else cmd.Parameters.AddWithValue("p_FT_Name", DBNull.Value);

                cmd.Parameters.AddWithValue("p_FT_Taxable", (object)row.FT_Taxable);
                cmd.Parameters.AddWithValue("p_FT_IsActive", (object)row.FT_IsActive);
                cmd.Parameters.AddWithValue("p_LoginID", (object)row.LoginID);
                cmd.Parameters.AddWithValue("p_FT_PercentOfBasic", (object)row.FT_PercentOfBasic);
                cmd.Parameters.AddWithValue("p_FT_FlatAmount", (object)row.FT_FlatAmount);



                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_FUND_TYPE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateGR_Fund_Type(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            GratuityDS grDS = new GratuityDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_GR_FUND_TYPE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            grDS.Merge(inputDS.Tables[grDS.GR_FundType.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            foreach (GratuityDS.GR_FundTypeRow row in grDS.GR_FundType)
            {
                cmd.Parameters.AddWithValue("p_FT_Code", (object)row.FT_Code);

                if (!row.IsFT_NameNull()) cmd.Parameters.AddWithValue("p_FT_Name", row.FT_Name);
                else cmd.Parameters.AddWithValue("p_FT_Name", DBNull.Value);

                cmd.Parameters.AddWithValue("p_FT_Taxable", (object)row.FT_Taxable);
                cmd.Parameters.AddWithValue("p_FT_IsActive", (object)row.FT_IsActive);
                cmd.Parameters.AddWithValue("p_LoginID", (object)row.LoginID);
                cmd.Parameters.AddWithValue("p_FT_PercentOfBasic", (object)row.FT_PercentOfBasic);
                cmd.Parameters.AddWithValue("p_FT_FlatAmount", (object)row.FT_FlatAmount);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_FUND_TYPE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteGR_Fund_Type(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_GR_FUND_TYPE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters.AddWithValue("p_FT_Code", (object)stringDS.DataStrings[0].StringValue);


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_FUND_TYPE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }



            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateGR_Share_Rate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            GratuityDS grDS = new GratuityDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_GR_SHARE_RATE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            grDS.Merge(inputDS.Tables[grDS.GR_ShareRate.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            foreach (GratuityDS.GR_ShareRateRow row in grDS.GR_ShareRate)
            {
                cmd.Parameters.AddWithValue("p_FT_ID", (object)row.FT_ID);

                if (!row.IsFT_PercentOfBasicNull()) cmd.Parameters.AddWithValue("p_FT_PercentOfBasic", row.FT_PercentOfBasic);
                else cmd.Parameters.AddWithValue("p_FT_PercentOfBasic", DBNull.Value);

                if (!row.IsFT_FlatAmountNull()) cmd.Parameters.AddWithValue("p_FT_FlatAmount", row.FT_FlatAmount);
                else cmd.Parameters.AddWithValue("p_FT_FlatAmount", DBNull.Value);

                if (!row.IsFT_EffectDateNull()) cmd.Parameters.AddWithValue("p_FT_EffectDate", row.FT_EffectDate);
                else cmd.Parameters.AddWithValue("p_FT_EffectDate", DBNull.Value);

                cmd.Parameters.AddWithValue("p_FT_IsActive", row.FT_IsActive);
                cmd.Parameters.AddWithValue("p_LoginID", row.LoginID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_SHARE_RATE_UPP.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateGR_Member(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            GratuityDS grDS = new GratuityDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_GR_MEMBER_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            grDS.Merge(inputDS.Tables[grDS.GR_FundMember.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            foreach (GratuityDS.GR_FundMemberRow row in grDS.GR_FundMember)
            {
                cmd.Parameters.AddWithValue("p_FT_ID", (object)row.FT_ID);

                if (!row.IsEmployeeCodeNull()) cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                else cmd.Parameters.AddWithValue("p_EmployeeCode", DBNull.Value);

                if (!row.IsFT_MembershipDateNull()) cmd.Parameters.AddWithValue("p_FT_MembershipDate", row.FT_MembershipDate);
                else cmd.Parameters.AddWithValue("p_FT_MembershipDate", DBNull.Value);

                if (!row.IsFT_NoOfShareNull()) cmd.Parameters.AddWithValue("p_FT_NoOfShare", row.FT_NoOfShare);
                else cmd.Parameters.AddWithValue("p_FT_NoOfShare", DBNull.Value);

                if (!row.IsFT_RemarksNull()) cmd.Parameters.AddWithValue("p_FT_Remarks", row.FT_Remarks);
                else cmd.Parameters.AddWithValue("p_FT_Remarks", DBNull.Value);

                cmd.Parameters.AddWithValue("p_FT_IsActive", row.FT_IsActive);
                cmd.Parameters.AddWithValue("p_LoginID", row.LoginID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_MEMBER_UPP.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetAbleToInvestAmount(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAbleToInvestAmount_GR");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_ABLE_TO_INVEST_AMOUNT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _CreateInvestment_GR(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            GratuityDS grDS = new GratuityDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            grDS.Merge(inputDS.Tables[grDS.Investment.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            #region Create New Investment
            cmd.CommandText = "PRO_GR_INVESTMENT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            long InvID = IDGenerator.GetNextGenericPK();
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", InvID);
            cmd.Parameters.AddWithValue("p_InvestmentDate", DateTime.ParseExact(stringDS.DataStrings[0].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_MaturityDate", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_ProductID", Convert.ToInt32(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_PrincipalAmount", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_Profit", Convert.ToDecimal(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_Duration", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[6].StringValue);
            cmd.Parameters.AddWithValue("p_Tax", Convert.ToDecimal(stringDS.DataStrings[7].StringValue));
            cmd.Parameters.AddWithValue("p_ExciseDuty", Convert.ToDecimal(stringDS.DataStrings[8].StringValue));
            cmd.Parameters.AddWithValue("p_OthersCost", Convert.ToDecimal(stringDS.DataStrings[9].StringValue));
            cmd.Parameters.AddWithValue("p_Buyer", stringDS.DataStrings[10].StringValue);
            cmd.Parameters.AddWithValue("p_Total_Instrument", stringDS.DataStrings[12].StringValue);

            cmd.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[13].StringValue);       // WALI :: 13-Jan-2015

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_CREATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Add Product_Ins_Reference
            cmd.Dispose();
            cmd.CommandText = "PRO_GR_INV_REFERENCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (GratuityDS.InvestmentRow row in grDS.Investment.Rows)
            {
                cmd.Parameters.AddWithValue("p_INVID", InvID);
                cmd.Parameters.AddWithValue("p_Product_Ins_Reference", row.Product_Ins_Reference);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_CREATE_INVESTMENT.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetInvestmentList_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "NotSettled") cmd = DBCommandProvider.GetDBCommand("GetInvestmentListNotSettled_GR");
            else if (stringDS.DataStrings[0].StringValue == "All") cmd = DBCommandProvider.GetDBCommand("GetInvestmentListAll_GR");

            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_INVESTMENT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetInvestmentDetailsByInvID_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetInvestmentDetailsByInvID_GR");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["p_INVID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_INVESTMENT_DETAILS_BY_INVID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _UpdateInvestment_GR(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            GratuityDS grDS = new GratuityDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            grDS.Merge(inputDS.Tables[grDS.Investment.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            #region Update Investment
            cmd.CommandText = "PRO_GR_INVESTMENT_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            Int32 InvID = -1;
            InvID = Convert.ToInt32(stringDS.DataStrings[11].StringValue);
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[11].StringValue));
            cmd.Parameters.AddWithValue("p_InvestmentDate", DateTime.ParseExact(stringDS.DataStrings[0].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_MaturityDate", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_ProductID", Convert.ToInt32(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_PrincipalAmount", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_Profit", Convert.ToDecimal(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_Duration", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[6].StringValue);
            cmd.Parameters.AddWithValue("p_Tax", Convert.ToDecimal(stringDS.DataStrings[7].StringValue));
            cmd.Parameters.AddWithValue("p_ExciseDuty", Convert.ToDecimal(stringDS.DataStrings[8].StringValue));
            cmd.Parameters.AddWithValue("p_OthersCost", Convert.ToDecimal(stringDS.DataStrings[9].StringValue));
            cmd.Parameters.AddWithValue("p_Buyer", stringDS.DataStrings[10].StringValue);
            cmd.Parameters.AddWithValue("p_Total_Instrument", stringDS.DataStrings[12].StringValue);

            cmd.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[13].StringValue);       // WALI :: 13-Jan-2015

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_UPDATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Delete OLD Product_Ins_Reference
            cmd.Dispose();
            cmd.CommandText = "PRO_GR_INV_REFERENCE_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_INVID", InvID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_UPDATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Add New Product_Ins_Reference
            cmd.Dispose();
            cmd.CommandText = "PRO_GR_INV_REFERENCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (GratuityDS.InvestmentRow row in grDS.Investment.Rows)
            {
                cmd.Parameters.AddWithValue("p_INVID", InvID);
                cmd.Parameters.AddWithValue("p_Product_Ins_Reference", row.Product_Ins_Reference);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_CREATE_INVESTMENT.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _CancelLastInvestment_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_GR_DEL_LAST_INVESTMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_InvID", integerDS.DataIntegers[0].IntegerValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_DELETE_LAST_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _FreezeInvestment_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "PRO_GR_FREEZE_INVESTMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //err.ErrorInfo1 = ActionID.ACTION_GR_FREEZE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _InvestmentSettlementCreate_GR(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DateTime settlementDate = DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null);
            DateTime provisionDate = new DateTime(settlementDate.Year, settlementDate.Month, 2);  // Second day of 'Settlement Month'

            #region Investment Settlement
            cmd.CommandText = "PRO_GR_INV_SETTLEMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            Int32 InvID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_Settlement_Status", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_SettlementDate", settlementDate);  // WALI :: 06-May-2015

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_INVESTMENT_SETTLEMENT_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Provision For Mismatched Profit
            if (Convert.ToDecimal(stringDS.DataStrings[3].StringValue) != 0)
            {
                cmd.Dispose();
                cmd.CommandText = "PRO_GR_PROVISION_CREATE_ST";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
                cmd.Parameters.AddWithValue("p_ProvisionDate", provisionDate);
                cmd.Parameters.AddWithValue("p_Day_P", 0);
                cmd.Parameters.AddWithValue("p_Profit_P", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
                cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[4].StringValue);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_INVESTMENT_SETTLEMENT_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetProvisionBeneficiaryList_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetProvisionBeneficiaryList_GR");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_PROVISION_BENEFICIARY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetInvestmentProvisionList_GR(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();


            cmd = DBCommandProvider.GetDBCommand("GetInvestmentProvisionList_GR");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_INVESTMENT_PROVISION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ProvisionCreation_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "PRO_GR_PROVISION_PROCESS";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            string ProvisionDate = stringDS.DataStrings[0].StringValue + "/01/" + stringDS.DataStrings[1].StringValue + " 12:00:01 AM";

            cmd.Parameters.AddWithValue("p_ProvisionDate", (object)Convert.ToDateTime(ProvisionDate));
            cmd.Parameters.AddWithValue("p_LoginID", (object)stringDS.DataStrings[2].StringValue);
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_PROVISION_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }

        private DataSet _SaveTermsOfSeperation_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            grDS.Merge(inputDS.Tables[grDS.TermsOfSeparation_GR.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            cmd.CommandText = "PRO_GR_TERM_OF_SEPARATE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd_Del.CommandText = "PRO_GR_TERM_OF_SEPARATE_DEL";
            cmd_Del.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmd_Del == null) return UtilDL.GetCommandNotFound();

            #region Delete Old Terms
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_TERMS_OF_SEPARATION_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create New Terms
            if (grDS.TermsOfSeparation_GR.Rows.Count > 0)
            {
                foreach (GratuityDS.TermsOfSeparation_GRRow row in grDS.TermsOfSeparation_GR.Rows)
                {
                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) return UtilDL.GetDBOperationFailed();

                    cmd.Parameters.AddWithValue("p_TermsOfSeparationID", genPK);
                    cmd.Parameters.AddWithValue("p_ServiceYear", row.ServiceYear);
                    cmd.Parameters.AddWithValue("p_PaymentPercent_Principal", row.PaymentPercent_Principal);
                    cmd.Parameters.AddWithValue("p_PaymentPercent_Profit", row.PaymentPercent_Profit);
                    cmd.Parameters.AddWithValue("p_DependsOn", grDS.TermsOfSeparation_GR[0].DependsOn);
                    bError = false;
                    nRowAffected = -1;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_GR_TERMS_OF_SEPARATION_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion
            return errDS;
        }
        private DataSet _GetTermsOfSeperation_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetTermsOfSeparation_GR");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.TermsOfSeparation_GR.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_TERMS_OF_SEPARATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetEmployeeListForSeperation_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_ForSeperation_GR");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_EMPLOYEE_LIST_FOR_SEPERATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetEmployeeListForPayment_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_ForPayment_GR");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["PaymentStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);   // Added ::WALI :: 19-Apr-2015
            cmd.Parameters["PaymentStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);   // Added ::WALI :: 19-Apr-2015

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_EMPLOYEE_LIST_FOR_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetPaymentAmountsForSeperation_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPaymentAmountsForSeperation_GR");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_PAYMENT_AMOUNT_FOR_SEPERATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _EmployeeSeperation_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            OleDbCommand cmd_Sep = new OleDbCommand();
            OleDbCommand cmd_Head = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            grDS.Merge(inputDS.Tables[grDS.GR_Employee_Payment.TableName], false, MissingSchemaAction.Error);
            grDS.Merge(inputDS.Tables[grDS.GR_Head_GL.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            cmd_Sep.CommandText = "PRO_GR_EMPLOYEE_SEPERATION";
            cmd_Sep.CommandType = CommandType.StoredProcedure;

            cmd_Head.CommandText = "PRO_GR_HEADGL_ADD_FOR_LF";
            cmd_Head.CommandType = CommandType.StoredProcedure;

            if (cmd_Sep == null || cmd_Head == null) return UtilDL.GetCommandNotFound();

            #region Employee Seperation
            cmd_Sep.Parameters.AddWithValue("p_EmployeeID", grDS.GR_Employee_Payment[0].EmployeeID);
            cmd_Sep.Parameters.AddWithValue("p_Principal", grDS.GR_Employee_Payment[0].Principal);
            cmd_Sep.Parameters.AddWithValue("p_Profit", grDS.GR_Employee_Payment[0].Profit);
            cmd_Sep.Parameters.AddWithValue("p_Payment_Date", grDS.GR_Employee_Payment[0].SeperationDate);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Sep, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd_Sep.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_EMPLOYEE_SEPARATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region For Differences [Forfeirute]
            if (grDS.GR_Head_GL.Rows.Count > 0)
            {
                foreach (GratuityDS.GR_Head_GLRow row in grDS.GR_Head_GL.Rows)
                {
                    cmd_Head.Parameters.AddWithValue("p_RefHeadCode", row.RefHeadCode);
                    cmd_Head.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                    cmd_Head.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                    cmd_Head.Parameters.AddWithValue("p_Ref_EmployeeID", row.Ref_EmployeeID);
                    cmd_Head.Parameters.AddWithValue("p_Calculated_GL_Amount", row.GL_Amount);      // WALI :: 09-Mar-2015

                    bError = false;
                    nRowAffected = -1;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Head, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Head.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_GR_EMPLOYEE_SEPARATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion

            #region Fund Journal Add for 'PAYABLE TO LSC'
            if (grDS.GR_Employee_Payment[0].Payment_Source == "Donor")
            {
                OleDbCommand cmd_Fund = new OleDbCommand();
                cmd_Fund.CommandText = "PRO_GR_FUND_RECIEVE";
                cmd_Fund.CommandType = CommandType.StoredProcedure;
                if (cmd_Fund == null) return UtilDL.GetCommandNotFound();

                decimal gl_Amount = grDS.GR_Employee_Payment[0].Principal + grDS.GR_Employee_Payment[0].Profit;

                cmd_Fund.Parameters.AddWithValue("p_HeadID", 108);          // HeadID for 'Payable to LSC'
                cmd_Fund.Parameters.AddWithValue("p_GL_Amount", gl_Amount);
                cmd_Fund.Parameters.AddWithValue("p_Event_Date", grDS.GR_Employee_Payment[0].SeperationDate);
                cmd_Fund.Parameters.AddWithValue("p_Ref_EmployeeID", grDS.GR_Employee_Payment[0].EmployeeID);

                if (!grDS.GR_Employee_Payment[0].IsDonor_ReferenceNull()) cmd_Fund.Parameters.AddWithValue("p_Ref_Other", grDS.GR_Employee_Payment[0].Donor_Reference);
                else cmd_Fund.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);

                cmd_Fund.Parameters.AddWithValue("p_SalaryDate", DBNull.Value);
                cmd_Fund.Parameters.AddWithValue("p_Calculated_GL_Amount", gl_Amount);

                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Fund, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Fund.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_EMPLOYEE_SEPARATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            return errDS;
        }
        private DataSet _GetPaymentAmountsForPayment_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPaymentAmountsForPayment_GR");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_PAYMENT_AMOUNT_FOR_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _EmployeePayment_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            grDS.Merge(inputDS.Tables[grDS.GR_Employee_Payment.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            cmd.CommandText = "PRO_GR_EMPLOYEE_PAYMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_EmployeeID", grDS.GR_Employee_Payment[0].EmployeeID);
            cmd.Parameters.AddWithValue("p_Payment_Status", true);
            cmd.Parameters.AddWithValue("p_Actual_Payment_Date", grDS.GR_Employee_Payment[0].Actual_Payment_Date);

            if (!grDS.GR_Employee_Payment[0].IsPayment_ReferenceNull()) cmd.Parameters.AddWithValue("p_Payment_Reference", grDS.GR_Employee_Payment[0].Payment_Reference);
            else cmd.Parameters.AddWithValue("p_Payment_Reference", DBNull.Value);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_EMPLOYEE_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }

        private DataSet _GetFundHeadList_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetFundHeadlist_GR");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Fund_Head.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetFundRecieveDetails_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            DataDateDS dateDS = new DataDateDS();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetFundRecieveDetails_GR");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["SalaryDate1"].Value = Convert.ToDateTime(dateDS.DataDates[0].DateValue);
            cmd.Parameters["SalaryDate2"].Value = Convert.ToDateTime(dateDS.DataDates[0].DateValue);
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_FUND_RECIEVE_DETAILS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _FundRecieve_GR(DataSet inputDS)
        {


            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "Pro_GR_FUND_RECEIVE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_SalaryDate", Convert.ToDateTime(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_Event_Date", Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_Ref_Other", stringDS.DataStrings[2].StringValue);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_FUND_RECIEVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;

        }

        private DataSet _GetProvisionCalculatedData_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetProvisionCalculatedData_GR");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["ProvisionDate"].Value = stringDS.DataStrings[0].StringValue;
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_PROVISION_CALCULATED_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _SaveProvisionCalculatedData_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            GratuityDS grDS = new GratuityDS();

            grDS.Merge(inputDS.Tables[grDS.InvestmentProvision.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "PRO_GR_PROVISION_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            string ProvisionDate = stringDS.DataStrings[0].StringValue + "/01/" + stringDS.DataStrings[1].StringValue + " 12:00:01 AM";

            foreach (GratuityDS.InvestmentProvisionRow row in grDS.InvestmentProvision.Rows)
            {
                cmd.Parameters.AddWithValue("p_INVID", row.InvID);
                cmd.Parameters.AddWithValue("p_ProvisionDate", Convert.ToDateTime(ProvisionDate));
                cmd.Parameters.AddWithValue("p_Day_P", row.Day_P);
                cmd.Parameters.AddWithValue("p_Profit_P", row.Profit_P);
                cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[2].StringValue);
                bool bError = false;
                int nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_SAVE_PROVISION_CALCULATED_DATA.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;
        }
        private DataSet _GetProvisionedProfit_ForEmployee_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetProvisionedProfit_ForEmployee_GR");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_PROVISIONED_PROFIT_FOR_EMPLOYEE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _DistributeProvisionedProfit_GR(DataSet inputDS)
        {


            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Upd = new OleDbCommand();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;


            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "Pro_GR_INV_PROFIT_DIST";
            cmd.CommandType = CommandType.StoredProcedure;


            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_Event_Date", Convert.ToDateTime(stringDS.DataStrings[0].StringValue));


            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_DISTRIBUTE_PROVISIONED_PROFIT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;

        }
        private DataSet GetDistributedProvisionData_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetDistributedProvisionData_GR");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_DISTRIBUTED_PROVISION_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetPendingDistributions_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetPendingDistributions_GR");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_PENDING_DISTRIBUTION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _FundJournalEntry_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_GR_FUND_RECIEVE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_HeadID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_GL_Amount", Convert.ToDecimal(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_Event_Date", Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture));
            cmd.Parameters.AddWithValue("p_Ref_EmployeeID", DBNull.Value);

            if (stringDS.DataStrings[3].StringValue.Trim() == "") cmd.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);
            else cmd.Parameters.AddWithValue("p_Ref_Other", stringDS.DataStrings[3].StringValue);

            cmd.Parameters.AddWithValue("p_SalaryDate", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Calculated_GL_Amount", Convert.ToDecimal(stringDS.DataStrings[1].StringValue));     // WALI :: 09-Mar-2015

            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_FUND_JOURNAL_ENTRY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }
        private DataSet _GetHeadWiseAvailableAmount_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetHeadWiseAvailableAmount_ByFundYear_GR");
            adapter.SelectCommand = cmd;

            cmd.Parameters["HeadID1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["HeadID2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["FundYear"].Value = integerDS.DataIntegers[1].IntegerValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Fund_Head.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_HEADWISE_AVAILABLE_AMOUNT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetEmployeeList_By_GRMembDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            DataDateDS dateDS = new DataDateDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_By_GRMembDate");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["GRMembDate1"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmpStatus1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmpStatus2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["GRMembDate2"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmpStatus3"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmpStatus4"].Value = integerDS.DataIntegers[0].IntegerValue;

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_JOURNAL_EMPLOYEE_BY_GR_MEMBDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _EmployeeJournalEntry_GR(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;


            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "Pro_GR_EMP_JOURNAL_ENTRY";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();


            cmd.Parameters.AddWithValue("p_SalaryYearMonthDate", Convert.ToDateTime(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_GR_membershipDate", Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_TransactionDate", Convert.ToDateTime(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_FundHeadID", Convert.ToInt32(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_WithSeperated", Convert.ToBoolean(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_TransactionAmount", Convert.ToDecimal(stringDS.DataStrings[5].StringValue));

            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_EMPLOYEE_JOURNAL_ENTRY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            return errDS;

        }

        private DataSet _GetDistinctFundYear_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetDistinctFundYear_GR");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, integerDS, integerDS.DataIntegers.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_DISTINCT_FUND_YEAR_GR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            integerDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(integerDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetRecievedFundList_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetRecievedFundList_GR");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, dateDS, dateDS.DataDates.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_RECIEVED_FUND_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            dateDS.AcceptChanges();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(dateDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet GetTotalPayableToLSC_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetTotalPayableToLSC_GR");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_TOTAL_PAYABLE_TO_LSC.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetEmployeeBalanceRecord_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "Principal") cmd = DBCommandProvider.GetDBCommand("GetEmployeePrincipalAmounts_GR");
            else if (stringDS.DataStrings[0].StringValue == "Profit") cmd = DBCommandProvider.GetDBCommand("GetEmployeeProvisionDistributions_GR");
            else if (stringDS.DataStrings[0].StringValue == "Others") cmd = DBCommandProvider.GetDBCommand("GetEmployeeOtherAmounts_GR");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[6].StringValue, CultureInfo.InvariantCulture);
            cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[7].StringValue, CultureInfo.InvariantCulture);
            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[2].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[5].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[5].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_EMPLOYEE_BALANCE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ModifyEmployeeBalanceRecord_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            GratuityDS grDS = new GratuityDS();
            MessageDS messageDS = new MessageDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            grDS.Merge(inputDS.Tables[grDS.GR_Employee.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "Principal")
            {
                #region Modify Principal Amount(s)
                cmd.CommandText = "PRO_GR_EDIT_EMP_PRINCIPAL";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (GratuityDS.GR_EmployeeRow row in grDS.GR_Employee.Rows)
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_SalaryDate", row.Event_Date);
                    cmd.Parameters.AddWithValue("p_Amount", row.Amount);
                    cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_GR_MODIFY_EMPLOYEE_BALANCE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                }
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Profit")
            {
                #region Modify Profit Amount(s)
                cmd.CommandText = "PRO_GR_EDIT_EMP_PROFIT";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (GratuityDS.GR_EmployeeRow row in grDS.GR_Employee.Rows)
                {
                    cmd.Parameters.AddWithValue("p_ProfitGLID", row.Row_ID);
                    cmd.Parameters.AddWithValue("p_Amount", row.Amount);
                    cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_GR_MODIFY_EMPLOYEE_BALANCE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                }
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Others")
            {
                #region Modify Other Amount(s)
                cmd.CommandText = "PRO_GR_EDIT_EMP_OTHER_AMNT";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (GratuityDS.GR_EmployeeRow row in grDS.GR_Employee.Rows)
                {
                    cmd.Parameters.AddWithValue("p_ProfitGLID", row.Row_ID);
                    cmd.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                    cmd.Parameters.AddWithValue("p_Amount", row.Amount);
                    cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_GR_MODIFY_EMPLOYEE_BALANCE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                }
                #endregion
            }

            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetFundHeadBalanceRecord_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetFundHeadBalanceRecord_GR");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["HeadID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue, CultureInfo.InvariantCulture);
            cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_FUND_BALANCE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ModifyFundHeadBalanceRecord_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            GratuityDS grDS = new GratuityDS();
            MessageDS messageDS = new MessageDS();
            bool bError = false;
            int nRowAffected = -1;

            grDS.Merge(inputDS.Tables[grDS.GR_Head_GL.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_GR_EDIT_FUND_BALANCE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (GratuityDS.GR_Head_GLRow row in grDS.GR_Head_GL.Rows)
            {
                cmd.Parameters.AddWithValue("p_HeadGLID", row.Row_ID);
                cmd.Parameters.AddWithValue("p_Changed_GL_Amount", row.GL_Amount);
                cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_MODIFY_EMPLOYEE_BALANCE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.Event_Date.ToString("dd-MMM-yyyy") + "  [" + row.Description + "]");
                else messageDS.ErrorMsg.AddErrorMsgRow(row.Event_Date.ToString("dd-MMM-yyyy") + "  [" + row.Description + "]");
            }

            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetBeneficiaryList_To_FundRecieve_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetBeneficiaryList_To_FundRecieve_GR");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            long siteID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[2].StringValue);

            cmd.Parameters["MonthYearDate"].Value = stringDS.DataStrings[6].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue.ToUpper() + "%";
            cmd.Parameters["SiteID1"].Value = siteID;
            cmd.Parameters["SiteID2"].Value = siteID;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["EmployeeStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            cmd.Parameters["EmployeeStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_BENEFICIARY_LIST_TO_FUND_RECIEVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _Exceptional_FundRecieve_GR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            grDS.Merge(inputDS.Tables[grDS.GR_Head_GL.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            cmd_Del.CommandText = "PRO_GR_FUND_RECIEVE_DEL_EMP";
            cmd_Del.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRO_GR_FUND_RECIEVE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd_Del == null || cmd == null) return UtilDL.GetCommandNotFound();

            foreach (GratuityDS.GR_Head_GLRow row in grDS.GR_Head_GL.Rows)
            {
                #region First Delete prior FundRecieve of this (SalaryMonth+Employee)
                cmd_Del.Parameters.AddWithValue("p_SalaryDate", row.SalaryDate);
                cmd_Del.Parameters.AddWithValue("p_EmployeeID", row.Ref_EmployeeID);
                cmd_Del.Parameters.AddWithValue("p_HeadID", row.HeadID);
                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Del.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_FUND_RECIEVE_EMPLOYEE_WISE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                #region Then, Create new Fund Recieve
                cmd.Parameters.AddWithValue("p_HeadID", row.HeadID);
                cmd.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                cmd.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                cmd.Parameters.AddWithValue("p_Ref_EmployeeID", row.Ref_EmployeeID);

                if (!row.IsRef_OtherNull()) cmd.Parameters.AddWithValue("p_Ref_Other", row.Ref_Other);
                else cmd.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);

                cmd.Parameters.AddWithValue("p_SalaryDate", row.SalaryDate);
                cmd.Parameters.AddWithValue("p_Calculated_GL_Amount", row.GL_Amount);

                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GR_FUND_RECIEVE_EMPLOYEE_WISE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion
            }
            return errDS;
        }
        private DataSet _GetGR_EmpListForGenaralPayment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGR_EmpListForGenaralPayment");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_EMPLOYEE_LIST_FOR_SEPERATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GR_Payment_General(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            OleDbCommand cmd_Sep = new OleDbCommand();
            OleDbCommand cmd_Head = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            grDS.Merge(inputDS.Tables[grDS.GR_Employee_Payment.TableName], false, MissingSchemaAction.Error);
            grDS.Merge(inputDS.Tables[grDS.GR_Head_GL.TableName], false, MissingSchemaAction.Error);
            grDS.AcceptChanges();

            cmd_Sep.CommandText = "PRO_GR_PAYMENT_GENERAL";
            cmd_Sep.CommandType = CommandType.StoredProcedure;

            cmd_Head.CommandText = "PRO_GR_HEADGL_ADD_FOR_LF";
            cmd_Head.CommandType = CommandType.StoredProcedure;

            if (cmd_Sep == null || cmd_Head == null) return UtilDL.GetCommandNotFound();

            #region Employee Seperation
            cmd_Sep.Parameters.AddWithValue("p_EmployeeID", grDS.GR_Employee_Payment[0].EmployeeID);
            cmd_Sep.Parameters.AddWithValue("p_GR_Comp", grDS.GR_Employee_Payment[0].Principal);
            cmd_Sep.Parameters.AddWithValue("p_GR_Comp_Profit", grDS.GR_Employee_Payment[0].Profit);
            cmd_Sep.Parameters.AddWithValue("p_Payment_Date", grDS.GR_Employee_Payment[0].SeperationDate);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Sep, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd_Sep.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GR_EMPLOYEE_SEPARATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region For Differences [Forfeirute]
            if (grDS.GR_Head_GL.Rows.Count > 0)
            {
                foreach (GratuityDS.GR_Head_GLRow row in grDS.GR_Head_GL.Rows)
                {
                    cmd_Head.Parameters.AddWithValue("p_RefHeadCode", row.RefHeadCode);
                    cmd_Head.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                    cmd_Head.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                    cmd_Head.Parameters.AddWithValue("p_Ref_EmployeeID", row.Ref_EmployeeID);
                    cmd_Head.Parameters.AddWithValue("p_Calculated_GL_Amount", row.GL_Amount);      // WALI :: 09-Mar-2015

                    bError = false;
                    nRowAffected = -1;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Head, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Head.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_GR_EMPLOYEE_SEPARATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion


            return errDS;
        }
        private DataSet _GetGR_PaymentAmountsGeneral(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            GratuityDS grDS = new GratuityDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGR_PaymentAmountsGeneral");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_PAYMENT_AMOUNT_GENERAL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetGR_SeparationInfo(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            GratuityDS grDS = new GratuityDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "GR_SeparationInfo")
            {
                cmd = DBCommandProvider.GetDBCommand("GetGR_SeparationInformtion");
                adapter.SelectCommand = cmd;

                cmd.Parameters["Date_From"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
            }



            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, grDS, grDS.GR_SeparationInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_GR_SEPARATION_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            grDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(grDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}
