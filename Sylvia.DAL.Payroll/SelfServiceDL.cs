using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using System.Globalization;
using System.Configuration;
using System.IO;

namespace Sylvia.DAL.Payroll
{

    public class SelfServiceDL
    {
        public SelfServiceDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_SELF_SRV_EMPLOYEE_ADD:
                    return _createEmployee(inputDS);
                case ActionID.NA_ACTION_GET_SELF_SRV_EMPLOYEE_LIST:
                    return _getEmployeeList(inputDS);
                case ActionID.ACTION_EMPLOYEE_DEL:
                    return this._deleteEmployee(inputDS);
                case ActionID.ACTION_SELF_SRV_EMPLOYEE_APPROVE:
                    return this._approveEmployee(inputDS);

                case ActionID.NA_ACTION_GET_PERSONAL_HISTORY_COUNT:
                    return this._getPersonalHistoryCount(inputDS);
                case ActionID.NA_ACTION_GET_INFO_FOR_SELFSERVICE:
                    return this._getInformation_For_SelfService(inputDS);
                    
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }

        }

        private DataSet _createEmployee(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SelfServiceDS selfServiceDS = new SelfServiceDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            selfServiceDS.Merge(inputDS.Tables[selfServiceDS.Employees.TableName], false, MissingSchemaAction.Error);
            selfServiceDS.AcceptChanges();

            selfServiceDS.Merge(inputDS.Tables[selfServiceDS.LeaveTypeForSelfService.TableName], false, MissingSchemaAction.Error);
            selfServiceDS.AcceptChanges();

            foreach (SelfServiceDS.Employee emp in selfServiceDS.Employees)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_SELF_SRV_EMPLOYEE_ADD";
                cmd.CommandType = CommandType.StoredProcedure;

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                #region Add Parameters
                cmd.Parameters.AddWithValue("p_Employee_ParkingID", genPK);
                cmd.Parameters.AddWithValue("p_EmployeeCode", emp.EmployeeCode);

                if (!emp.IsFirstNameNull()) cmd.Parameters.AddWithValue("p_FirstName", emp.FirstName);
                else cmd.Parameters.AddWithValue("p_FirstName", System.DBNull.Value);

                if (!emp.IsMidleNameNull()) cmd.Parameters.AddWithValue("p_MidleName", emp.MidleName);
                else cmd.Parameters.AddWithValue("p_MidleName", System.DBNull.Value);

                if (!emp.IsLastNameNull()) cmd.Parameters.AddWithValue("p_LastName", emp.LastName);
                else cmd.Parameters.AddWithValue("p_LastName", System.DBNull.Value);

                if (!emp.IsEnglishReadingNull()) cmd.Parameters.AddWithValue("p_EnglishReading", emp.EnglishReading);
                else cmd.Parameters.AddWithValue("p_EnglishReading", System.DBNull.Value);

                if (!emp.IsEnglishWritingNull()) cmd.Parameters.AddWithValue("p_EnglishWriting", emp.EnglishWriting);
                else cmd.Parameters.AddWithValue("p_EnglishWriting", System.DBNull.Value);

                if (!emp.IsEnglishSpokenNull()) cmd.Parameters.AddWithValue("p_EnglishSpoken", emp.EnglishSpoken);
                else cmd.Parameters.AddWithValue("p_EnglishSpoken", System.DBNull.Value);

                if (!emp.IsEmployeeNameNull()) cmd.Parameters.AddWithValue("p_EmployeeName", emp.EmployeeName);
                else cmd.Parameters.AddWithValue("p_EmployeeName", System.DBNull.Value);

                if (!emp.IsGenderNull()) cmd.Parameters.AddWithValue("p_Gender", emp.Gender);
                else cmd.Parameters.AddWithValue("p_Gender", System.DBNull.Value);

                if (!emp.IsReligionNull()) cmd.Parameters.AddWithValue("p_Religion", emp.Religion);
                else cmd.Parameters.AddWithValue("p_Religion", System.DBNull.Value);

                if (!emp.IsBloodGroupNull()) cmd.Parameters.AddWithValue("p_BloodGroup", emp.BloodGroup);
                else cmd.Parameters.AddWithValue("p_BloodGroup", System.DBNull.Value);

                if (!emp.IsMaritalStatusNull()) cmd.Parameters.AddWithValue("p_MaritalStatus", emp.MaritalStatus);
                else cmd.Parameters.AddWithValue("p_MaritalStatus", System.DBNull.Value);

                if (!emp.IsPassportNumberNull()) cmd.Parameters.AddWithValue("p_PassportNumber", emp.PassportNumber);
                else cmd.Parameters.AddWithValue("p_PassportNumber", System.DBNull.Value);

                if (!emp.IsTinNumberNull()) cmd.Parameters.AddWithValue("p_TinNumber", emp.TinNumber);
                else cmd.Parameters.AddWithValue("p_TinNumber", System.DBNull.Value);

                if (!emp.IsPlaceOfBirthNull()) cmd.Parameters.AddWithValue("p_PlaceOfBirth", emp.PlaceOfBirth);
                else cmd.Parameters.AddWithValue("p_PlaceOfBirth", System.DBNull.Value);

                if (!emp.IsFatherNameNull()) cmd.Parameters.AddWithValue("p_FatherName", emp.FatherName);
                else cmd.Parameters.AddWithValue("p_FatherName", System.DBNull.Value);

                if (!emp.IsFatherOccupationNull()) cmd.Parameters.AddWithValue("p_FatherOccupation", emp.FatherOccupation);
                else cmd.Parameters.AddWithValue("p_FatherOccupation", System.DBNull.Value);

                if (!emp.IsMotherNameNull()) cmd.Parameters.AddWithValue("p_MotherName", emp.MotherName);
                else cmd.Parameters.AddWithValue("p_MotherName", System.DBNull.Value);

                if (!emp.IsMotherOccupationNull()) cmd.Parameters.AddWithValue("p_MotherOccupation", emp.MotherOccupation);
                else cmd.Parameters.AddWithValue("p_MotherOccupation", System.DBNull.Value);

                if (!emp.IsSpouseNameNull()) cmd.Parameters.AddWithValue("p_SpouseName", emp.SpouseName);
                else cmd.Parameters.AddWithValue("p_SpouseName", System.DBNull.Value);

                if (!emp.IsSpouseOccupationNull()) cmd.Parameters.AddWithValue("p_SpouseOccupation", emp.SpouseOccupation);
                else cmd.Parameters.AddWithValue("p_SpouseOccupation", System.DBNull.Value);


                if (!emp.IsPresentAddressNull()) cmd.Parameters.AddWithValue("p_PresentAddress", emp.PresentAddress);
                else cmd.Parameters.AddWithValue("p_PresentAddress", System.DBNull.Value);

                if (!emp.IsPermanentAddressNull()) cmd.Parameters.AddWithValue("p_PermanentAddress", emp.PermanentAddress);
                else cmd.Parameters.AddWithValue("p_PermanentAddress", System.DBNull.Value);

                if (!emp.IsMobileNull()) cmd.Parameters.AddWithValue("p_Mobile", emp.Mobile);
                else cmd.Parameters.AddWithValue("p_Mobile", System.DBNull.Value);

                if (!emp.IsTelephoneNull()) cmd.Parameters.AddWithValue("p_Telephone", emp.Telephone);
                else cmd.Parameters.AddWithValue("p_Telephone", System.DBNull.Value);

                if (!emp.IsEmailNull()) cmd.Parameters.AddWithValue("p_Email", emp.Email);
                else cmd.Parameters.AddWithValue("p_Email", System.DBNull.Value);

                if (!emp.IsFaxNull()) cmd.Parameters.AddWithValue("p_Fax", emp.Fax);
                else cmd.Parameters.AddWithValue("p_Fax", System.DBNull.Value);

                if (!emp.IsNationalityNull()) cmd.Parameters.AddWithValue("p_Nationality", emp.Nationality);
                else cmd.Parameters.AddWithValue("p_Nationality", System.DBNull.Value);

                if (!emp.IsLNNameNull()) cmd.Parameters.AddWithValue("p_LNName", emp.LNName);
                else cmd.Parameters.AddWithValue("p_LNName", System.DBNull.Value);

                if (!emp.IsLNAddressNull()) cmd.Parameters.AddWithValue("p_LNAddress", emp.LNAddress);
                else cmd.Parameters.AddWithValue("p_LNAddress", System.DBNull.Value);

                if (!emp.IsNationalCodeNull()) cmd.Parameters.AddWithValue("p_NationalCode", emp.NationalCode);
                else cmd.Parameters.AddWithValue("p_NationalCode", System.DBNull.Value);

                if (!emp.IsThanaIDNull()) cmd.Parameters.AddWithValue("p_ThanaID", emp.ThanaID);
                else cmd.Parameters.AddWithValue("p_ThanaID", System.DBNull.Value);

                if (!emp.IsHouseLocationNull()) cmd.Parameters.AddWithValue("p_HouseLocation", emp.HouseLocation);
                else cmd.Parameters.AddWithValue("p_HouseLocation", System.DBNull.Value);

                if (!emp.IsExperienceTypeNull()) cmd.Parameters.AddWithValue("p_ExperienceType", emp.ExperienceType);
                else cmd.Parameters.AddWithValue("p_ExperienceType", System.DBNull.Value);

                if (!emp.IsExperienceYearNull()) cmd.Parameters.AddWithValue("p_ExperienceYear", emp.ExperienceYear);
                else cmd.Parameters.AddWithValue("p_ExperienceYear", System.DBNull.Value);

                if (!emp.IsExperienceDetailsNull()) cmd.Parameters.AddWithValue("p_ExperienceDetails", emp.ExperienceDetails);
                else cmd.Parameters.AddWithValue("p_ExperienceDetails", System.DBNull.Value);

                if (!emp.IsFamilyHomeContactNull()) cmd.Parameters.AddWithValue("p_FamilyHomeContact", emp.FamilyHomeContact);
                else cmd.Parameters.AddWithValue("p_FamilyHomeContact", System.DBNull.Value);

                if (!emp.IsAlternativeMobileNoNull()) cmd.Parameters.AddWithValue("p_AlternativeMobileNo", emp.AlternativeMobileNo);
                else cmd.Parameters.AddWithValue("p_AlternativeMobileNo", System.DBNull.Value);


                if (!emp.IsEmergencyContactMobileNull()) cmd.Parameters.AddWithValue("p_EmergencyContactMobile", emp.EmergencyContactMobile);
                else cmd.Parameters.AddWithValue("p_EmergencyContactMobile", System.DBNull.Value);

                if (!emp.IsEmergencyTelephoneNull()) cmd.Parameters.AddWithValue("p_EmergencyTelephone", emp.EmergencyTelephone);
                else cmd.Parameters.AddWithValue("p_EmergencyTelephone", System.DBNull.Value);

                if (!emp.IsEmergencyEmailNull()) cmd.Parameters.AddWithValue("p_EmergencyEmail", emp.EmergencyEmail);
                else cmd.Parameters.AddWithValue("p_EmergencyEmail", System.DBNull.Value);

                if (!emp.IsParentAddressNull()) cmd.Parameters.AddWithValue("p_ParentAddress", emp.ParentAddress);
                else cmd.Parameters.AddWithValue("p_ParentAddress", System.DBNull.Value);

                if (!emp.IsEmergencyContactRelationNull()) cmd.Parameters.AddWithValue("p_EmergencyContactRelation", emp.EmergencyContactRelation);
                else cmd.Parameters.AddWithValue("p_EmergencyContactRelation", System.DBNull.Value);

                if (!emp.IsEmployeeName_NativeLangNull()) cmd.Parameters.AddWithValue("p_EmployeeName_NativeLang", emp.EmployeeName_NativeLang);
                else cmd.Parameters.AddWithValue("p_EmployeeName_NativeLang", System.DBNull.Value);


                if (!emp.IsThanaID_PermanentNull()) cmd.Parameters.AddWithValue("p_ThanaID_Permanent", emp.ThanaID_Permanent);
                else cmd.Parameters.AddWithValue("p_ThanaID_Permanent", System.DBNull.Value);

                if (!emp.IsFather_NationalIDNull()) cmd.Parameters.AddWithValue("p_Father_NationalID", emp.Father_NationalID);
                else cmd.Parameters.AddWithValue("p_Father_NationalID", System.DBNull.Value);

                if (!emp.IsMother_NationalIDNull()) cmd.Parameters.AddWithValue("p_Mother_NationalID", emp.Mother_NationalID);
                else cmd.Parameters.AddWithValue("p_Mother_NationalID", System.DBNull.Value);

                if (!emp.IsSpouse_NationalIDNull()) cmd.Parameters.AddWithValue("p_Spouse_NationalID", emp.Spouse_NationalID);
                else cmd.Parameters.AddWithValue("p_Spouse_NationalID", System.DBNull.Value);

                if (!emp.IsCreatedByNull()) cmd.Parameters.AddWithValue("p_CreatedBy", emp.CreatedBy);
                else cmd.Parameters.AddWithValue("p_CreatedBy", System.DBNull.Value);

                if (!emp.IsLast_Update_UserIDNull()) cmd.Parameters.AddWithValue("p_Last_Update_UserID", emp.Last_Update_UserID);
                else cmd.Parameters.AddWithValue("p_Last_Update_UserID", System.DBNull.Value);

                if (!emp.IsNickNameNull()) cmd.Parameters.AddWithValue("p_NickName", emp.NickName);
                else cmd.Parameters.AddWithValue("p_NickName", System.DBNull.Value);

                // WALI
                if (!emp.IsLeavePendingNull()) cmd.Parameters.AddWithValue("p_LeavePending", emp.LeavePending);
                else cmd.Parameters.AddWithValue("p_LeavePending", System.DBNull.Value);

                if (!emp.IsBasicSalaryNull()) cmd.Parameters.AddWithValue("p_BasicSalary", emp.BasicSalary);
                else cmd.Parameters.AddWithValue("p_BasicSalary", System.DBNull.Value);

                if (!emp.IsSenderUserIDNull()) cmd.Parameters.AddWithValue("p_SenderUserID", emp.SenderUserID);
                else cmd.Parameters.AddWithValue("p_SenderUserID", System.DBNull.Value);

                if (!emp.IsReceiverUserIDNull()) cmd.Parameters.AddWithValue("p_ReceiverUserID", emp.ReceiverUserID);
                else cmd.Parameters.AddWithValue("p_ReceiverUserID", System.DBNull.Value);

                try
                { if (!emp.IsReceiverUserIDNull()) cmd.Parameters.AddWithValue("p_DateOfBirth", emp.DateOfBirth); }
                catch
                { cmd.Parameters.AddWithValue("p_DateOfBirth", System.DBNull.Value); }

                cmd.Parameters.AddWithValue("p_IsCarAllocated", emp.IsCarAllocated);

                //if (!emp.IsCar_ForWhich_TermNull()) cmd.Parameters.AddWithValue("p_Car_ForWhich_Term", emp.Car_ForWhich_Term);
                //else cmd.Parameters.AddWithValue("p_Car_ForWhich_Term", System.DBNull.Value);

                if (!emp.IsCar_RegistrationDate_1stNull()) cmd.Parameters.AddWithValue("p_Car_RegistrationDate_1st", emp.Car_RegistrationDate_1st);
                else cmd.Parameters.AddWithValue("p_Car_RegistrationDate_1st", System.DBNull.Value);

                if (!emp.IsCar_RegistrationDate_2ndNull()) cmd.Parameters.AddWithValue("p_Car_RegistrationDate_2nd", emp.Car_RegistrationDate_2nd);
                else cmd.Parameters.AddWithValue("p_Car_RegistrationDate_2nd", System.DBNull.Value);

                if (!emp.IsCar_RegistrationDate_3rdNull()) cmd.Parameters.AddWithValue("p_Car_RegistrationDate_3rd", emp.Car_RegistrationDate_3rd);
                else cmd.Parameters.AddWithValue("p_Car_RegistrationDate_3rd", System.DBNull.Value);

                if (!emp.IsBankAccountNoNull()) cmd.Parameters.AddWithValue("p_BankAccountNo", emp.BankAccountNo);
                else cmd.Parameters.AddWithValue("p_BankAccountNo", System.DBNull.Value);

                if (!emp.IsTaxZoneNull()) cmd.Parameters.AddWithValue("p_TaxZone", emp.TaxZone);
                else cmd.Parameters.AddWithValue("p_TaxZone", System.DBNull.Value);

                if (!emp.IsTaxCircleNull()) cmd.Parameters.AddWithValue("p_TaxCircle", emp.TaxCircle);
                else cmd.Parameters.AddWithValue("p_TaxCircle", System.DBNull.Value);

                if (!emp.IsTaxLocationNull()) cmd.Parameters.AddWithValue("p_TaxLocation", emp.TaxLocation);
                else cmd.Parameters.AddWithValue("p_TaxLocation", System.DBNull.Value);

                //if (!emp.IsPhotofileNameNull()) cmd.Parameters.AddWithValue("p_PhotofileName", emp.PhotofileName);
                //else cmd.Parameters.AddWithValue("p_PhotofileName", System.DBNull.Value);

                //if (!emp.IsImgPhotoNull()) cmd.Parameters.AddWithValue("p_ImgPhoto", emp.ImgPhoto);
                //else cmd.Parameters.AddWithValue("p_ImgPhoto", System.DBNull.Value);

                //if (!emp.IsSignfilenameNull()) cmd.Parameters.AddWithValue("p_Signfilename", emp.Signfilename);
                //else cmd.Parameters.AddWithValue("p_Signfilename", System.DBNull.Value);

                //if (!emp.IsImgSignNull()) cmd.Parameters.AddWithValue("p_ImgSign", emp.ImgSign);
                //else cmd.Parameters.AddWithValue("p_ImgSign", System.DBNull.Value);

                #endregion

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            #region Delete Leave info :: Rony
            OleDbCommand cmd_Leave_Del = new OleDbCommand();
            cmd_Leave_Del.CommandText = "PRO_SELF_LEAVE_INFO_DELETE";
            cmd_Leave_Del.CommandType = CommandType.StoredProcedure;

            cmd_Leave_Del.Parameters.AddWithValue("p_EmployeeCode", selfServiceDS.LeaveTypeForSelfService[0].EmployeeCode);

            bool bError_Leave_Del = false;
            int nRowAffected_Leave_Del = -1;
            nRowAffected_Leave_Del = ADOController.Instance.ExecuteNonQuery(cmd_Leave_Del, connDS.DBConnections[0].ConnectionID, ref bError_Leave_Del);
            cmd_Leave_Del.Parameters.Clear();
            if (bError_Leave_Del)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create Leave Information :: Rony
            foreach (SelfServiceDS.LeaveTypeForSelfServiceRow row in selfServiceDS.LeaveTypeForSelfService)
            {
                OleDbCommand cmd_Leave = new OleDbCommand();
                cmd_Leave.CommandText = "PRO_SELF_LEAVE_INFO_CREATE";
                cmd_Leave.CommandType = CommandType.StoredProcedure;

                cmd_Leave.Parameters.AddWithValue("p_LeaveTypeId", row.LeaveTypeID);
                cmd_Leave.Parameters.AddWithValue("p_Entitled", row.Entitled);
                cmd_Leave.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);

                bool bError_Leave = false;
                int nRowAffected_Leave = -1;
                nRowAffected_Leave = ADOController.Instance.ExecuteNonQuery(cmd_Leave, connDS.DBConnections[0].ConnectionID, ref bError_Leave);
                cmd_Leave.Parameters.Clear();
                if (bError_Leave)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            return returnDS;
        }

        private DataSet _getEmployeeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            SelfServiceDS selfServiceDS = new SelfServiceDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[0].StringValue == "SelfServiceUser")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServiceEmpListByCode");
                Debug.Assert(cmd != null);

                cmd.Parameters["EmployeeCode"].Value = (object)stringDS.DataStrings[1].StringValue;
            }
            else if (stringDS.DataStrings[0].StringValue == "ApprovalUser")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServiceEmpListAll");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue.ToUpper();
                cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[3].StringValue.ToUpper() + "%";
                cmd.Parameters["SiteCode1"].Value = cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["DesignationCode1"].Value = cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["ReceiverEmpCode"].Value = cmd.Parameters["SenderEmpCode"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["ApproveStatus1"].Value = cmd.Parameters["ApproveStatus2"].Value = stringDS.DataStrings[8].StringValue;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, selfServiceDS, selfServiceDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SELF_SRV_EMPLOYEE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            selfServiceDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(selfServiceDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _deleteEmployee(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            //-----------------------------------------
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteEmployeeHistory");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["EmployeeId"].Value = (object)UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            //-----------------------------------------

            cmd = DBCommandProvider.GetDBCommand("DeleteEmployee");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["employeecode"].Value = (object)stringDS.DataStrings[0].StringValue;
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;
        }

        private DataSet _approveEmployee(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SelfServiceDS empDS = new SelfServiceDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;

            empDS.Merge(inputDS.Tables[empDS.Employees.TableName], false, MissingSchemaAction.Error);
            empDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            string messageBody = "", toAddress = "";
            string returnMessage = "Email successfully sent.";
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());

            bool isNotifyByMail = false;
            string mailSubject = "", senderEmail = "";

            #region Set Message
            messageBody = String.Format("<b>Dispatcher :</b> {0}<br><br>", empDS.Employees[0].SenderName);
            messageBody += "You have pending requests for personal data verification.<br>";
            messageBody += "Please check the HR Management system for details.<br>";
            messageBody += "<br><br><br>";
            //messageBody += "For details, click the following link: ";
            //messageBody += "<br>";
            //messageBody += row.URL;
            //messageBody += "<br>";
            #endregion
            #endregion

            cmd.CommandText = "PRO_SELF_SRV_EMPLOYEE_APPROVE";
            cmd.CommandType = CommandType.StoredProcedure;

            #region Send Notification Mails
            foreach (SelfServiceDS.Employee row in empDS.Employees)
            {
                if (row.NotificationByMail)
                {
                    isNotifyByMail = true;
                    senderEmail = row.SenderMail;
                    mailSubject = row.SubjectForEmail;
                    toAddress += row.ReceiverMail + ",";
                }
            }

            if (isNotifyByMail)
            {
                senderEmail = credentialEmailAddress;
                returnMessage = "";
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, senderEmail, toAddress, "",
                                                   mailSubject, messageBody, "Self Service - Employee Information");
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, senderEmail, toAddress, "", mailSubject, messageBody, "Self Service - Employee Profile");
                }
            }
            #endregion

            if (returnMessage == "Email successfully sent.")
            {
                foreach (SelfServiceDS.Employee row in empDS.Employees)
                {
                    #region Approve / Forward
                    cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                    cmd.Parameters.AddWithValue("p_IsApproved", row.IsApproved);
                    cmd.Parameters.AddWithValue("p_SenderUserID", row.SenderUserID);
                    cmd.Parameters.AddWithValue("p_ReceiverUserID", row.ReceiverUserID);

                    if (!row.IsApprovedByNull()) cmd.Parameters.AddWithValue("p_ApprovedBy", row.ApprovedBy);
                    else cmd.Parameters.AddWithValue("p_ApprovedBy", DBNull.Value);

                    if (!row.IsApproverCommentNull()) cmd.Parameters.AddWithValue("p_ApproverComment", row.ApproverComment);
                    else cmd.Parameters.AddWithValue("p_ApproverComment", DBNull.Value);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_SELF_SRV_EMPLOYEE_APPROVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                    if (bError)
                    { messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} - {1} [{2}]", row.EmployeeCode, row.EmployeeName, row.EventName)); }
                    else
                    { messageDS.SuccessMsg.AddSuccessMsgRow(String.Format("{0} - {1} [{2}]", row.EmployeeCode, row.EmployeeName, row.EventName)); }
                    messageDS.AcceptChanges();
                    #endregion
                }
            }
            else
            {
                messageDS.ErrorMsg.AddErrorMsgRow("Email sending failed.");
                messageDS.AcceptChanges();
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getPersonalHistoryCount(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            SelfServiceDS selfServiceDS = new SelfServiceDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPersonalHistoryCount");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = cmd.Parameters["EmployeeCode3"].Value =
                cmd.Parameters["EmployeeCode4"].Value = cmd.Parameters["EmployeeCode5"].Value = cmd.Parameters["EmployeeCode6"].Value =
                cmd.Parameters["EmployeeCode7"].Value = cmd.Parameters["EmployeeCode8"].Value = cmd.Parameters["EmployeeCode9"].Value =
                cmd.Parameters["EmployeeCode10"].Value = cmd.Parameters["EmployeeCode11"].Value = cmd.Parameters["EmployeeCode12"].Value =
                cmd.Parameters["EmployeeCode13"].Value = cmd.Parameters["EmployeeCode14"].Value = cmd.Parameters["EmployeeCode15"].Value =
                cmd.Parameters["EmployeeCode16"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, selfServiceDS, selfServiceDS.PersonalHistoryCount.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PERSONAL_HISTORY_COUNT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            selfServiceDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(selfServiceDS.PersonalHistoryCount);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getInformation_For_SelfService(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            SelfServiceDS selfServiceDS = new SelfServiceDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "LeaveTypeForSelfService")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLeaveTypeForSelfService");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["ApplyFor"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["Religion"].Value = stringDS.DataStrings[2].StringValue;

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, selfServiceDS, selfServiceDS.LeaveTypeForSelfService.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_INFO_FOR_SELFSERVICE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                selfServiceDS.AcceptChanges();
            }
            else if (stringDS.DataStrings[0].StringValue == "LeaveTypeForSelfServiceByEmpCode")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLeaveTypeForSelfServiceByEmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[1].StringValue;

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, selfServiceDS, selfServiceDS.LeaveTypeForSelfService.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_INFO_FOR_SELFSERVICE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                selfServiceDS.AcceptChanges();
            }
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(selfServiceDS.LeaveTypeForSelfService);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
    }
}
