using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
  /// <summary>
  /// Summary description for CurrencyDL.
  /// </summary>
  public class CurrencyDL
  {
    public CurrencyDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }
    
    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {
        case ActionID.ACTION_CURRENCY_ADD:
          return _createCurrency( inputDS );
      case ActionID.NA_ACTION_GET_CURRENCY_LIST:
          return _getCurrencyList(inputDS);
        case ActionID.ACTION_CURRENCY_DEL:
          return this._deleteCurrency( inputDS );
        case ActionID.ACTION_CURRENCY_UPD:
          return this._updateCurrency( inputDS );
        case ActionID.ACTION_DOES_CURRENCY_EXIST:
          return this._doesCurrencyExist( inputDS );
        case ActionID.ACTION_CURRENCY_CONV_ADD:
          return _createCurrencyConv(inputDS);
        case ActionID.ACTION_CURRENCY_CONV_DEL:
          return _deleteCurrencyConv(inputDS);
        case ActionID.ACTION_CURRENCY_CONV_UPD:
          return _updateCurrencyConv(inputDS);
      case ActionID.NA_ACTION_Q_ALL_CURRENCY_CONV:
          return _getCurrencyConvList(inputDS);
        case ActionID.ACTION_DOES_CURRENCY_CONV_EXIST:
          return this._doesCurrencyConvExist(inputDS);
       
        case ActionID.ACTION_EMPLOYEE_CURRENCY_ADD:
          return _createEmployeeCurrency(inputDS);
        case ActionID.ACTION_EMPLOYEE_CURRENCY_UPD:
          return _updateEmployeeCurrency(inputDS);
        case ActionID.ACTION_EMPLOYEE_CURRENCY_DEL:
          return _deleteEmployeeCurrency(inputDS);
      case ActionID.NA_ACTION_Q_ALL_EMPLOYEE_CURRENCY:
          return _getEmployeeCurrencyList(inputDS);
        case ActionID.ACTION_DOES_EMPLOYEE_CURRENCY_EXIST:
          return _doesEmployeeCurrencyExist(inputDS);

        case ActionID.ACTION_EMPLOYEE_FC_SALARY_ADD:
          return _createEmployeeFCSalary(inputDS);
        case ActionID.ACTION_EMPLOYEE_FC_SALARY_UPD:
          return _updateEmployeeFCSalary(inputDS);
        case ActionID.ACTION_EMPLOYEE_FC_SALARY_DEL:
          return _deleteEmployeeFCSalary(inputDS);
      case ActionID.NA_ACTION_Q_ALL_EMPLOYEE_FC_SALARY:
          return _getEmployeeFCSalaryList(inputDS);
        case ActionID.ACTION_DOES_EMPLOYEE_FC_SALARY_EXIST:
          return _doesEmployeeFCSalaryExist(inputDS);    
                 
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
    private DataSet _createCurrency(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      CurrencyDS currDS = new CurrencyDS();
      DBConnectionDS connDS = new  DBConnectionDS();

      

      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      
      //create currmand
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateCurrency");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      currDS.Merge( inputDS.Tables[ currDS.Currencies.TableName ], false, MissingSchemaAction.Error );
      currDS.AcceptChanges();
      
      foreach(CurrencyDS.Currency curr in currDS.Currencies)
      {
        long genPK = IDGenerator.GetNextGenericPK();
        if( genPK == -1)
        {
          UtilDL.GetDBOperationFailed();
        }
        cmd.Parameters["Id"].Value = (object) genPK;
        //code
        if(curr.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) curr.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value =null;

        }
        //name
        if(curr.IsNameNull()==false)
        {
          cmd.Parameters["Name"].Value = (object) currDS.Currencies[0].Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value =null;

        }
        //      country 
        if(curr.IsCountryNull()==false)
        {
          cmd.Parameters["Country"].Value = (object) currDS.Currencies[0].Country;
        }
        else
        {
          cmd.Parameters["Country"].Value =null;

        }
        //  cmd.Parameters["Symbol"].Value = (object)currDS.Currencies[0].Symbol;
      

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_CURRENCY_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _createCurrencyConv(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      CurrencyConvDS currDS = new CurrencyConvDS();
      DBConnectionDS connDS = new DBConnectionDS();



      //extract dbconnection 
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      //create currmand
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateCurrencyConv");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      currDS.Merge(inputDS.Tables[currDS.CurrencyConversion.TableName], false, MissingSchemaAction.Error);
      currDS.AcceptChanges();

      foreach (CurrencyConvDS.CurrencyConversionRow curr in currDS.CurrencyConversion)
      {
        //code
        if (curr.IsCodeNull() == false)
        {
          cmd.Parameters["Code"].Value = curr.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;

        }
        //currencyid
        if (curr.IsCurrencyCodeNull() == false)
        {
          cmd.Parameters["CurrencyCode"].Value = curr.CurrencyCode;
        }
        else
        {
          cmd.Parameters["CurrencyCode"].Value = null;

        }
        //      country 
        if (curr.IsMonthYearDateNull() == false)
        {
          cmd.Parameters["EffectDate"].Value = curr.MonthYearDate;
        }
        else
        {
          cmd.Parameters["EffectDate"].Value = null;

        }

        if (curr.IsRateNull() == false)
        {
          cmd.Parameters["RateBDT"].Value = curr.Rate;
        }
        else
        {
          cmd.Parameters["RateBDT"].Value = null;

        }

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_CURRENCY_CONV_ADD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _deleteCurrency(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

    
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteCurrency");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      foreach(DataStringDS.DataString sValue in stringDS.DataStrings)
      {
        if (sValue.IsStringValueNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) sValue.StringValue;
        }
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_BONUS_DEL.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }


      return errDS;  // return empty ErrorDS
    }
    private DataSet _deleteCurrencyConv(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new DBConnectionDS();
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
      stringDS.AcceptChanges();


      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteCurrencyConv");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
      {
        if (sValue.IsStringValueNull() == false)
        {
          cmd.Parameters["Code"].Value = (object)sValue.StringValue;
        }

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_CURRENCY_CONV_DEL.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }


      return errDS;  // return empty ErrorDS
    }
    private DataSet _getCurrencyList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataLongDS longDS = new DataLongDS();
      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

   

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCurrencyList");
  
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
    

      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      CurrencyPO currPO = new CurrencyPO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter, 
        currPO, currPO.Currencies.TableName, 
        connDS.DBConnections[0].ConnectionID, 
        ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_GET_CURRENCY_LIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }

      currPO.AcceptChanges();

      CurrencyDS currDS = new CurrencyDS();
      if ( currPO.Currencies.Count > 0 )
      {
       

        foreach ( CurrencyPO.Currency poCom in currPO.Currencies)
        {
          CurrencyDS.Currency curr = currDS.Currencies.NewCurrency();
          if(poCom.IsCodeNull()==false)
          {
            curr.Code = poCom.Code;
          }
          if(poCom.IsNameNull()==false)
          {
            curr.Name = poCom.Name;
          }
          if(poCom.IsCountryNull()==false)
          {
            curr.Country = poCom.Country;
          }
          if (poCom.IsCurrencyIDNull() == false)//RONY::05 JUNE 2016
          {
              curr.CurrencyID = poCom.CurrencyID;
          }
//          
//          curr.Symbol = poCom.Symbol;
          currDS.Currencies.AddCurrency( curr );
          currDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( currDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
    
      return returnDS;
    
    
    }
    private DataSet _getCurrencyConvList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new DBConnectionDS();
     
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();



      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCurrencyConvList");

      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }


      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;
    
      CurrencyConvPO currPO = new CurrencyConvPO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter,
        currPO, currPO.CurrencyConv.TableName,
        connDS.DBConnections[0].ConnectionID,
        ref bError);
      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_CURRENCY_CONV.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;

      }

      currPO.AcceptChanges();

      CurrencyConvDS currDS = new CurrencyConvDS();
      if (currPO.CurrencyConv.Count > 0)
      {


        foreach (CurrencyConvPO.CurrencyConvRow poConv in currPO.CurrencyConv)
        {
          CurrencyConvDS.CurrencyConversionRow conv = currDS.CurrencyConversion.NewCurrencyConversionRow();
          if (poConv.IsCurrencyConvCodeNull() == false)
          {
            conv.Code = poConv.CurrencyConvCode;
          }
          if (poConv.IsCurrencyCodeNull() == false)
          {
            conv.CurrencyCode = poConv.CurrencyCode;
          }
          if (poConv.IsEffectDateNull() == false)
          {
            conv.MonthYearDate = poConv.EffectDate;
          }
          if (poConv.IsRateBdtNull() == false)
          {
            conv.Rate = poConv.RateBdt;
          }
          //          
          //          conv.Symbol = poConv.Symbol;
          currDS.CurrencyConversion.AddCurrencyConversionRow(conv);
          currDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();

      DataSet returnDS = new DataSet();

      returnDS.Merge(currDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();

      return returnDS;


    }
    private DataSet _updateCurrency(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();
      CurrencyDS currDS = new CurrencyDS();
      DBConnectionDS connDS = new  DBConnectionDS();

      

      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      
      //create currmand
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateCurrency");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      
      currDS.Merge( inputDS.Tables[ currDS.Currencies.TableName ], false, MissingSchemaAction.Error );
      currDS.AcceptChanges();
      
      foreach(CurrencyDS.Currency curr in currDS.Currencies)
      {
        //code
        if(curr.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) curr.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value =null;

        }
        //name
        if(curr.IsNameNull()==false)
        {
          cmd.Parameters["Name"].Value = (object) currDS.Currencies[0].Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value =null;

        }
  //      country 
        if(curr.IsCountryNull()==false)
        {
          cmd.Parameters["Country"].Value = (object) currDS.Currencies[0].Country;
        }
        else
        {
          cmd.Parameters["Country"].Value =null;

        }
        //  cmd.Parameters["Symbol"].Value = (object)currDS.Currencies[0].Symbol;
      


        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_CURRENCY_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _updateCurrencyConv(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      CurrencyConvDS currDS = new CurrencyConvDS();
      DBConnectionDS connDS = new DBConnectionDS();



      //extract dbconnection 
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      //create currmand
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateCurrencyConv");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }

      currDS.Merge(inputDS.Tables[currDS.CurrencyConversion.TableName], false, MissingSchemaAction.Error);
      currDS.AcceptChanges();

      foreach (CurrencyConvDS.CurrencyConversionRow curr in currDS.CurrencyConversion)
      {
        //code
        if (curr.IsCodeNull() == false)
        {
          cmd.Parameters["Code"].Value = curr.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;

        }
        //name
        if (curr.IsRateNull() == false)
        {
          cmd.Parameters["RateBDT"].Value = curr.Rate;
        }
        else
        {
          cmd.Parameters["RateBDT"].Value = null;

        }
        


        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_CURRENCY_CONV_UPD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _doesCurrencyExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesCurrencyExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
        cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    }
    private DataSet _doesCurrencyConvExist(DataSet inputDS)
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesCurrencyConvExist");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if (stringDS.DataStrings[0].IsStringValueNull() == false)
      {
        cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

      if (bError == true)
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;
      }

      boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge(boolDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();
      return returnDS;
    }



    private DataSet _createEmployeeCurrency(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      EmployeeCurrencyDS empCurrencyDS = new EmployeeCurrencyDS();
      DBConnectionDS connDS = new DBConnectionDS();



      //extract dbconnection 
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      //create currmand
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateEmployeeCurrency");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      empCurrencyDS.Merge(inputDS.Tables[empCurrencyDS.EmployeeCurrency.TableName], false, MissingSchemaAction.Error);
      empCurrencyDS.AcceptChanges();

      foreach (EmployeeCurrencyDS.EmployeeCurrencyRow empCurrency in empCurrencyDS.EmployeeCurrency)
      {
        //code
        if (empCurrency.IsEmployeeCodeNull() == false)
        {
          cmd.Parameters["EmployeeCode"].Value = empCurrency.EmployeeCode;
        }
        else
        {
          cmd.Parameters["EmployeeCode"].Value = null;

        }
        //currencyid
        if (empCurrency.IsCurrencyCodeNull() == false)
        {
          cmd.Parameters["CurrencyCode"].Value = empCurrency.CurrencyCode;
        }
        else
        {
          cmd.Parameters["CurrencyCode"].Value = null;

        }
        //      country 
       
        if (empCurrency.IsRateBDTNull() == false)
        {
          cmd.Parameters["RateBDT"].Value = empCurrency.RateBDT;
        }
        else
        {
          cmd.Parameters["RateBDT"].Value = null;

        }

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_CURRENCY_ADD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _updateEmployeeCurrency(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      EmployeeCurrencyDS empCurrencyDS = new EmployeeCurrencyDS();
      DBConnectionDS connDS = new DBConnectionDS();



      //extract dbconnection 
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      //create currmand
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateEmployeeCurrency");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      empCurrencyDS.Merge(inputDS.Tables[empCurrencyDS.EmployeeCurrency.TableName], false, MissingSchemaAction.Error);
      empCurrencyDS.AcceptChanges();

      foreach (EmployeeCurrencyDS.EmployeeCurrencyRow empCurrency in empCurrencyDS.EmployeeCurrency)
      {
        //code
        if (empCurrency.IsEmployeeCodeNull() == false)
        {
          cmd.Parameters["EmployeeCode"].Value = empCurrency.EmployeeCode;
        }
        else
        {
          cmd.Parameters["EmployeeCode"].Value = null;

        }
        //currencyid
        if (empCurrency.IsCurrencyCodeNull() == false)
        {
          cmd.Parameters["CurrencyCode"].Value = empCurrency.CurrencyCode;
        }
        else
        {
          cmd.Parameters["CurrencyCode"].Value = null;

        }
        //      country 

        if (empCurrency.IsRateBDTNull() == false)
        {
          cmd.Parameters["RateBDT"].Value = empCurrency.RateBDT;
        }
        else
        {
          cmd.Parameters["RateBDT"].Value = null;

        }

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_CURRENCY_UPD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _deleteEmployeeCurrency(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new DBConnectionDS();
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
      stringDS.AcceptChanges();


      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteEmployeeCurrency");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
      {
        if (sValue.IsStringValueNull() == false)
        {
          cmd.Parameters["EmployeeCode"].Value = sValue.StringValue;
        }

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_CURRENCY_DEL.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }


      return errDS;  // return empty ErrorDS
    }
    private DataSet _doesEmployeeCurrencyExist(DataSet inputDS)
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesEmployeeCurrencyExist");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if (stringDS.DataStrings[0].IsStringValueNull() == false)
      {
        cmd.Parameters["EmployeeCode"].Value = (object)stringDS.DataStrings[0].StringValue;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

      if (bError == true)
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;
      }

      boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge(boolDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();
      return returnDS;
    }
    private DataSet _getEmployeeCurrencyList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new DBConnectionDS();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();



      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeCurrencyList");

      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }


      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

       
      EmployeeCurrencyPO currPO = new EmployeeCurrencyPO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter,
        currPO, currPO.EmployeeCurrency.TableName,
        connDS.DBConnections[0].ConnectionID,
        ref bError);
      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_EMPLOYEE_CURRENCY.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;

      }

      currPO.AcceptChanges();

      EmployeeCurrencyDS currDS = new EmployeeCurrencyDS();
      if (currPO.EmployeeCurrency.Count > 0)
      {


        foreach (EmployeeCurrencyPO.EmployeeCurrencyRow poEmpCur in currPO.EmployeeCurrency)
        {
          EmployeeCurrencyDS.EmployeeCurrencyRow empCur = currDS.EmployeeCurrency.NewEmployeeCurrencyRow();
          if (poEmpCur.IsEmployeeCodeNull() == false)
          {
            empCur.EmployeeCode = poEmpCur.EmployeeCode;
          }
          if (poEmpCur.IsCurrencyCodeNull() == false)
          {
            empCur.CurrencyCode = poEmpCur.CurrencyCode;
          }
          if (poEmpCur.IsRateBDTNull() == false)
          {
            empCur.RateBDT = poEmpCur.RateBDT;
          }
          //          
          //          empCur.Symbol = poEmpCur.Symbol;
          currDS.EmployeeCurrency.AddEmployeeCurrencyRow(empCur);
          currDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();

      DataSet returnDS = new DataSet();

      returnDS.Merge(currDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();

      return returnDS;


    }


    private DataSet _createEmployeeFCSalary(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      EmployeeFCSalaryHistDS empFCSalaryDS = new EmployeeFCSalaryHistDS();
      DBConnectionDS connDS = new DBConnectionDS();



      //extract dbconnection 
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      //create currmand
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateEmployeeFCSalary");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      empFCSalaryDS.Merge(inputDS.Tables[empFCSalaryDS.EmployeeFCSalary.TableName], false, MissingSchemaAction.Error);
      empFCSalaryDS.AcceptChanges();

      foreach (EmployeeFCSalaryHistDS.EmployeeFCSalaryRow empFCSalary in empFCSalaryDS.EmployeeFCSalary)
      {
        //code

        if (empFCSalary.IsCodeNull() == false)
        {
          cmd.Parameters["Code"].Value = empFCSalary.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;

        }
       
        if (empFCSalary.IsEmployeeCodeNull() == false)
        {
          cmd.Parameters["EmployeeCode"].Value = empFCSalary.EmployeeCode;
        }
        else
        {
          cmd.Parameters["EmployeeCode"].Value = null;

        }
        //currencyid
        if (empFCSalary.IsEffectDateNull() == false)
        {
          cmd.Parameters["EffectDate"].Value = empFCSalary.EffectDate;
        }
        else
        {
          cmd.Parameters["EffectDate"].Value = null;

        }
        
        if (empFCSalary.IsSalaryBDTNull() == false)
        {
          cmd.Parameters["SalaryBDT"].Value = empFCSalary.SalaryBDT;
        }
        else
        {
          cmd.Parameters["SalaryBDT"].Value = null;

        }
        if (empFCSalary.IsSalaryFCNull() == false)
        {
          cmd.Parameters["SalaryFC"].Value = empFCSalary.SalaryFC;
        }
        else
        {
          cmd.Parameters["SalaryFC"].Value = null;

        }

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_FC_SALARY_ADD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _updateEmployeeFCSalary(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      EmployeeFCSalaryHistDS empFCSalaryDS = new EmployeeFCSalaryHistDS();
      DBConnectionDS connDS = new DBConnectionDS();



      //extract dbconnection 
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      //create currmand
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateEmployeeFCSalary");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      empFCSalaryDS.Merge(inputDS.Tables[empFCSalaryDS.EmployeeFCSalary.TableName], false, MissingSchemaAction.Error);
      empFCSalaryDS.AcceptChanges();

      foreach (EmployeeFCSalaryHistDS.EmployeeFCSalaryRow empFCSalary in empFCSalaryDS.EmployeeFCSalary)
      {
        //code

        if (empFCSalary.IsCodeNull() == false)
        {
          cmd.Parameters["Code"].Value = empFCSalary.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;

        }

        if (empFCSalary.IsSalaryBDTNull() == false)
        {
          cmd.Parameters["SalaryBDT"].Value = empFCSalary.SalaryBDT;
        }
        else
        {
          cmd.Parameters["SalaryBDT"].Value = null;

        }
        if (empFCSalary.IsSalaryFCNull() == false)
        {
          cmd.Parameters["SalaryFC"].Value = empFCSalary.SalaryFC;
        }
        else
        {
          cmd.Parameters["SalaryFC"].Value = null;

        }

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_FC_SALARY_ADD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _deleteEmployeeFCSalary(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new DBConnectionDS();
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
      stringDS.AcceptChanges();


      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteEmployeeFCSalary");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
      {
        if (sValue.IsStringValueNull() == false)
        {
          cmd.Parameters["Code"].Value = (object)sValue.StringValue;
        }

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_FC_SALARY_DEL.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }


      return errDS;  // return empty ErrorDS
    }
    private DataSet _doesEmployeeFCSalaryExist(DataSet inputDS)
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesEmployeeFCSalaryExist");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if (stringDS.DataStrings[0].IsStringValueNull() == false)
      {
        cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

      if (bError == true)
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;
      }

      boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge(boolDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();
      return returnDS;
    }
    private DataSet _getEmployeeFCSalaryList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new DBConnectionDS();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();



      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeFCSalaryList");

      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }


      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      
      EmployeeFCSalaryPO empFCSalaryPO = new EmployeeFCSalaryPO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter,
        empFCSalaryPO, empFCSalaryPO.EmployeeFCSalary.TableName,
        connDS.DBConnections[0].ConnectionID,
        ref bError);
      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_EMPLOYEE_FC_SALARY.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;

      }

      empFCSalaryPO.AcceptChanges();

      EmployeeFCSalaryHistDS empFCSalaryDS = new EmployeeFCSalaryHistDS();
      if (empFCSalaryPO.EmployeeFCSalary.Count > 0)
      {


        foreach (EmployeeFCSalaryPO.EmployeeFCSalaryRow poEmpFCSalary in empFCSalaryPO.EmployeeFCSalary)
        {
          EmployeeFCSalaryHistDS.EmployeeFCSalaryRow empFCSalary = empFCSalaryDS.EmployeeFCSalary.NewEmployeeFCSalaryRow();
          if (poEmpFCSalary.IsCodeNull() == false)
          {
            empFCSalary.Code = poEmpFCSalary.Code;
          }
          if (poEmpFCSalary.IsEmployeeCodeNull() == false)
          {
            empFCSalary.EmployeeCode = poEmpFCSalary.EmployeeCode;
          }
          if (poEmpFCSalary.IsEffectDateNull() == false)
          {
            empFCSalary.EffectDate = poEmpFCSalary.EffectDate;
          }
          if (poEmpFCSalary.IsSalaryBDTNull() == false)
          {
            empFCSalary.SalaryBDT = poEmpFCSalary.SalaryBDT;
          }
          if (poEmpFCSalary.IsSalaryFCNull() == false)
          {
            empFCSalary.SalaryFC = poEmpFCSalary.SalaryFC;
          }

          //          
          //          empFCSalary.Symbol = poEmpFCSalary.Symbol;
          empFCSalaryDS.EmployeeFCSalary.AddEmployeeFCSalaryRow(empFCSalary);
          empFCSalaryDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();

      DataSet returnDS = new DataSet();

      returnDS.Merge(empFCSalaryDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();

      return returnDS;


    }

  }
}
