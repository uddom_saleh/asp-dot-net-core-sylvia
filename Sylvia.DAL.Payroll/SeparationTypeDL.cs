/* *********************************************************************
 *  Compamy: Milllennium Information Solution Limited		 		           *  
 *  Author: Md. Hasinur Rahman Likhon				 				                   *
 *  Comment Date: April 13, 2006									                     *
 ***********************************************************************/

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for ActivityDL.
    /// </summary>
    public class SeparationTypeDL
    {
        public SeparationTypeDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_SEPARATION_TYPE_ADD:
                    return _createSeparationType(inputDS);
                case ActionID.ACTION_SEPARATION_TYPE_UPDATE:
                    return _updateSeparationType(inputDS);
                case ActionID.NA_ACTION_SEPARATION_TYPE_LIST:
                    return _getAllSeparationTypes(inputDS);
                case ActionID.NA_ACTION_SEPARATION_TYPE_ITEMS_LIST:
                    return _getAllSeparationTypeItems(inputDS);
                case ActionID.ACTION_SEPARATION_TYPE_DELETE:
                    return _deleteSeparationType(inputDS);
                case ActionID.ACTION_SEPARATION_TYPE_MAPPING_WITH_GRADE_ADD:
                    return _createSeparationTypeMapping(inputDS);
                case ActionID.NA_ACTION_SEPARATION_TYPE_MAPPING_WITH_GRADE_ITEMS:
                    return _getSeparationTypeMappingWithGradeItems(inputDS);
                case ActionID.NA_ACTION_SEPARATION_TYPE_SALARY_INFO:
                    return _getSalaryInfoForSeparation(inputDS);

                case ActionID.NA_ACTION_GET_RESIGNATION_REASONS:
                    return this._getResignationReasonList(inputDS);
                case ActionID.ACTION_RESIGNATION_REASON_ADD:
                    return this._createResignationReason(inputDS);
                case ActionID.ACTION_RESIGNATION_REASON_UPD:
                    return this._updateResignationReason(inputDS);
                case ActionID.ACTION_RESIGNATION_REASON_DEL:
                    return this._deleteResignationReason(inputDS);
                case ActionID.ACTION_RETIREMENT_BENEFIT_POLICY_ADD:
                    return this._createRetirementBenefitPolicy(inputDS);
                case ActionID.NA_ACTION_RETIREMENT_BENEFIT_POLICY:
                    return this._getRetirementBenefitPolicy(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _createSeparationType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SEPARATION_TYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmdItem = new OleDbCommand();
            cmdItem.CommandText = "PRO_SEPARATIONTYPE_ITEM_CREATE";
            cmdItem.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmdItem == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            SeparationTypeDS sepTypeDS = new SeparationTypeDS();
            sepTypeDS.Merge(inputDS.Tables[sepTypeDS.SeparationType.TableName], false, MissingSchemaAction.Error);
            sepTypeDS.AcceptChanges();

            sepTypeDS.Merge(inputDS.Tables[sepTypeDS.SeparationTypeItem.TableName], false, MissingSchemaAction.Error);
            sepTypeDS.AcceptChanges();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Cteate Separation Type..
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmd.Parameters.AddWithValue("p_SeparationTypeID", genPK);
            cmd.Parameters.AddWithValue("p_SeparatiopnTypeName", sepTypeDS.SeparationType[0].SeparationTypeName);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID,
                ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEPARATION_TYPE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(sepTypeDS.SeparationType[0].SeparationTypeName);
            else messageDS.ErrorMsg.AddErrorMsgRow(sepTypeDS.SeparationType[0].SeparationTypeName);
            messageDS.AcceptChanges();
            #endregion

            #region Create Separation type Items...
            long genItemPK;
            foreach (SeparationTypeDS.SeparationTypeItemRow row in sepTypeDS.SeparationTypeItem.Rows)
            {
                genItemPK = IDGenerator.GetNextGenericPK();

                cmdItem.Parameters.AddWithValue("p_SeparationTypeItemID", genItemPK);
                cmdItem.Parameters.AddWithValue("p_SeparationTypeID", genPK);
                cmdItem.Parameters.AddWithValue("p_BenefitName", row.BenefitName);
                cmdItem.Parameters.AddWithValue("p_BenefitCode", row.Code);
                cmdItem.Parameters.AddWithValue("p_FlatAmount", row.FlatAmount);
                cmdItem.Parameters.AddWithValue("p_PercentOfContribution", row.PercentOfContribution);
                cmdItem.Parameters.AddWithValue("p_PercentOfBasic", row.PercentOFBasic);
                cmdItem.Parameters.AddWithValue("p_NoOfTime", row.NoOfTimes);
                cmdItem.Parameters.AddWithValue("p_IsLastMonthSalary", row.IsLastMonthSalary);
                cmdItem.Parameters.AddWithValue("p_IsDependOnRule", row.IsDependOnRule);
                cmdItem.Parameters.AddWithValue("p_BenefitType", row.BenefitType);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdItem, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdItem.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEPARATION_TYPE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateSeparationType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();

            sepTypeDS.Merge(inputDS.Tables[sepTypeDS.SeparationType.TableName], false, MissingSchemaAction.Error);
            sepTypeDS.AcceptChanges();

            sepTypeDS.Merge(inputDS.Tables[sepTypeDS.SeparationTypeItem.TableName], false, MissingSchemaAction.Error);
            sepTypeDS.AcceptChanges();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region First Delete Old items Records
            OleDbCommand cmdDel = new OleDbCommand();
            cmdDel.CommandText = "PRO_SEPARATIONTYPE_ITEM_DELETE";
            cmdDel.CommandType = CommandType.StoredProcedure;

            //cmdDel.Parameters.AddWithValue("p_SeparationTypeID", sepTypeDS.SeparationType[0].SeparationTypeID);
            cmdDel.Parameters.AddWithValue("p_DiscontinueTypeID", sepTypeDS.SeparationTypeItem[0].DiscontinueTypeID);

            if (cmdDel == null) return UtilDL.GetCommandNotFound();

            bool bErrorDel = false;
            int nRowAffectedDel = -1;
            nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);
            cmdDel.Parameters.Clear();
            if (bErrorDel)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEPARATION_TYPE_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            if (nRowAffectedDel > 0) messageDS.SuccessMsg.AddSuccessMsgRow(sepTypeDS.SeparationTypeItem[0].DiscontinueType);
            else messageDS.ErrorMsg.AddErrorMsgRow(sepTypeDS.SeparationTypeItem[0].DiscontinueType);
            messageDS.AcceptChanges();

            #endregion

            #region Then Create New items
            OleDbCommand cmdItem = new OleDbCommand();
            cmdItem.CommandText = "PRO_SEPARATIONTYPE_ITEM_CREATE";
            cmdItem.CommandType = CommandType.StoredProcedure;
            if (cmdItem == null) return UtilDL.GetCommandNotFound();
            
            long genItemPK;
            foreach (SeparationTypeDS.SeparationTypeItemRow row in sepTypeDS.SeparationTypeItem.Rows)
            {
                genItemPK = IDGenerator.GetNextGenericPK();

                if (!row.IsSeparationTypeItemIDNull()) cmdItem.Parameters.AddWithValue("p_SeparationTypeItemID", row.SeparationTypeItemID);
                else cmdItem.Parameters.AddWithValue("p_SeparationTypeItemID", genItemPK);
                //cmdItem.Parameters.AddWithValue("p_SeparationTypeID", row.SeparationTypeID);
                cmdItem.Parameters.AddWithValue("p_DiscontinueTypeID", row.DiscontinueTypeID);
                cmdItem.Parameters.AddWithValue("p_BenefitName", row.BenefitName);
                cmdItem.Parameters.AddWithValue("p_BenefitCode", row.Code);
                cmdItem.Parameters.AddWithValue("p_FlatAmount", row.FlatAmount);
                cmdItem.Parameters.AddWithValue("p_PercentOfContribution", row.PercentOfContribution);
                cmdItem.Parameters.AddWithValue("p_PercentOfBasic", row.PercentOFBasic);
                cmdItem.Parameters.AddWithValue("p_NoOfTime", row.NoOfTimes);
                cmdItem.Parameters.AddWithValue("p_IsLastMonthSalary", row.IsLastMonthSalary);
                cmdItem.Parameters.AddWithValue("p_IsDependOnRule", row.IsDependOnRule);
                cmdItem.Parameters.AddWithValue("p_BenefitType", row.BenefitType);

                bool bError2 = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdItem, connDS.DBConnections[0].ConnectionID, ref bError2);
                cmdItem.Parameters.Clear();
                if (bError2)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEPARATION_TYPE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getAllSeparationTypes(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSeparationTypes");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sepTypeDS, sepTypeDS.SeparationType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TASK_TYPE_ALL_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            sepTypeDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(sepTypeDS.SeparationType);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAllSeparationTypeItems(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSeparationTypeItems");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sepTypeDS, sepTypeDS.SeparationTypeItem.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_SEPARATION_TYPE_ITEMS_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            sepTypeDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(sepTypeDS.SeparationTypeItem);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteSeparationType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SEPARATIONTYPE_ITEM_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_DiscontinueTypeID", stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_TASKTYPE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createSeparationTypeMapping(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();

            sepTypeDS.Merge(inputDS.Tables[sepTypeDS.SeparationTypeMapping.TableName], false, MissingSchemaAction.Error);
            sepTypeDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region First Delete Old items Records
            OleDbCommand cmdDel = new OleDbCommand();
            cmdDel.CommandText = "PRO_SEPARATIONTYPE_MAPPING_DEL";
            cmdDel.CommandType = CommandType.StoredProcedure;

            cmdDel.Parameters.AddWithValue("p_GradeCode", stringDS.DataStrings[0].StringValue);

            if (cmdDel == null) return UtilDL.GetCommandNotFound();

            bool bErrorDel = false;
            int nRowAffectedDel = -1;
            nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);
            cmdDel.Parameters.Clear();
            if (bErrorDel)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEPARATION_TYPE_MAPPING_WITH_GRADE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #endregion

            #region Then Create New items

            foreach (SeparationTypeDS.SeparationTypeMappingRow row in sepTypeDS.SeparationTypeMapping.Rows)
            {
                OleDbCommand cmdItem = new OleDbCommand();
                cmdItem.CommandText = "PRO_SEPARATIONTYPE_MAPPING_ADD";
                cmdItem.CommandType = CommandType.StoredProcedure;

                if (cmdItem == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
                long genItemPK;
                genItemPK = IDGenerator.GetNextGenericPK();

                if (!row.IsSeparationTypeMappingIDNull()) cmdItem.Parameters.AddWithValue("p_SeparationTypeMappingID", row.SeparationTypeMappingID);
                else cmdItem.Parameters.AddWithValue("p_SeparationTypeMappingID", genItemPK);

                cmdItem.Parameters.AddWithValue("p_GradeCode", row.GradeCode);
                cmdItem.Parameters.AddWithValue("p_DiscontinueTypeID", row.DiscontinueTypeID);

                bool bError2 = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdItem, connDS.DBConnections[0].ConnectionID, ref bError2);
                cmdItem.Parameters.Clear();
                if (bError2)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEPARATION_TYPE_MAPPING_WITH_GRADE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(stringDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getSeparationTypeMappingWithGradeItems(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSeparationTypeMappingItems");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sepTypeDS, sepTypeDS.SeparationTypeMapping.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_SEPARATION_TYPE_MAPPING_WITH_GRADE_ITEMS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            sepTypeDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(sepTypeDS.SeparationTypeMapping);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getSalaryInfoForSeparation(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSeparationTypeInfo_All");

            OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("GetSeparationTypeSalaryInfo");

            if (cmd == null || cmd1 == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd1.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd1.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd1.Parameters["EmployeeCode3"].Value = stringDS.DataStrings[0].StringValue;
            cmd1.Parameters["EmployeeCode4"].Value = stringDS.DataStrings[0].StringValue;
            //cmd1.Parameters["SeparationDate"].Value = dateDS.DataDates[0].DateValue;
            //cmd1.Parameters["EmployeeCode5"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sepTypeDS, sepTypeDS.SeparationTypeMapping.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            OleDbDataAdapter adapter1 = new OleDbDataAdapter();
            adapter1.SelectCommand = cmd1;

            int nRowAffected1 = -1;
            nRowAffected1 = ADOController.Instance.Fill(adapter1, sepTypeDS, sepTypeDS.SeparationTypeSalaryInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_SEPARATION_TYPE_SALARY_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            sepTypeDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(sepTypeDS.SeparationTypeMapping);
            returnDS.Merge(sepTypeDS.SeparationTypeSalaryInfo);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getResignationReasonList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetResignationReasonList");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sepTypeDS, sepTypeDS.ResignationReason.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_RESIGNATION_REASONS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            sepTypeDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(sepTypeDS.ResignationReason);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createResignationReason(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();
            bool bError = false;
            int nRowAffected = -1;

            OleDbCommand cmd = new OleDbCommand();

            sepTypeDS.Merge(inputDS.Tables[sepTypeDS.ResignationReason.TableName], false, MissingSchemaAction.Error);
            sepTypeDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (SeparationTypeDS.ResignationReasonRow row in sepTypeDS.ResignationReason.Rows)
            {
                cmd.CommandText = "PRO_RESIGNATION_REASON_ADD";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_ResignationReasonID", genPK);

                if (!row.IsReasonTitleNull()) cmd.Parameters.AddWithValue("p_ReasonTitle", row.ReasonTitle);
                else cmd.Parameters.AddWithValue("p_ReasonTitle", DBNull.Value);

                if (!row.IsReasonDescriptionNull()) cmd.Parameters.AddWithValue("p_ReasonDescription", row.ReasonDescription);
                else cmd.Parameters.AddWithValue("p_ReasonDescription", DBNull.Value);

                if (!row.IsReasonIsActiveNull()) cmd.Parameters.AddWithValue("p_ReasonIsActive", row.ReasonIsActive);
                else cmd.Parameters.AddWithValue("p_ReasonIsActive", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RESIGNATION_REASON_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _updateResignationReason(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();
            OleDbCommand cmd = new OleDbCommand();

            sepTypeDS.Merge(inputDS.Tables[sepTypeDS.ResignationReason.TableName], false, MissingSchemaAction.Error);
            sepTypeDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_RESIGNATION_REASON_UPD";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            SeparationTypeDS.ResignationReasonRow row = sepTypeDS.ResignationReason[0];

            if (!row.IsResignationReasonIDNull()) cmd.Parameters.AddWithValue("p_ResignationReasonID", row.ResignationReasonID);
            else cmd.Parameters.AddWithValue("p_ResignationReasonID", DBNull.Value);

            if (!row.IsReasonTitleNull()) cmd.Parameters.AddWithValue("p_ReasonTitle", row.ReasonTitle);
            else cmd.Parameters.AddWithValue("p_ReasonTitle", DBNull.Value);

            if (!row.IsReasonDescriptionNull()) cmd.Parameters.AddWithValue("p_ReasonDescription", row.ReasonDescription);
            else cmd.Parameters.AddWithValue("p_ReasonDescription", DBNull.Value);

            if (!row.IsReasonIsActiveNull()) cmd.Parameters.AddWithValue("p_ReasonIsActive", row.ReasonIsActive);
            else cmd.Parameters.AddWithValue("p_ReasonIsActive", DBNull.Value);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_RESIGNATION_REASON_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _deleteResignationReason(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SeparationTypeDS sepDS = new SeparationTypeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_RESIGNATION_REASON_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            sepDS.Merge(inputDS.Tables[sepDS.ResignationReason.TableName], false, MissingSchemaAction.Error);
            sepDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (SeparationTypeDS.ResignationReasonRow row in sepDS.ResignationReason.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (!row.IsResignationReasonIDNull()) cmd.Parameters.AddWithValue("p_ResignationReasonID", row.ResignationReasonID);
                else cmd.Parameters.AddWithValue("p_ResignationReasonID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RESIGNATION_REASON_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.ReasonTitle);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.ReasonTitle);
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createRetirementBenefitPolicy(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd_Del = new OleDbCommand();
            cmd_Del.CommandText = "PRO_RETIREMENT_BEN_POLICY_DEL";
            cmd_Del.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_RETIREMENT_BENEFIT_POLICY";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd_Del == null || cmd == null) return UtilDL.GetCommandNotFound();

            SeparationTypeDS sepTypeDS = new SeparationTypeDS();
            sepTypeDS.Merge(inputDS.Tables[sepTypeDS.RetirementBenefitPolicy.TableName], false, MissingSchemaAction.Error);
            sepTypeDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Delete First

            cmd_Del.Parameters.AddWithValue("p_RSB_DiscontinueTypeID", sepTypeDS.RetirementBenefitPolicy[0].RSB_DiscontinueTypeID);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_RETIREMENT_BENEFIT_POLICY_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(sepTypeDS.RetirementBenefitPolicy[0].RSB_DiscontinueType);
            else messageDS.ErrorMsg.AddErrorMsgRow(sepTypeDS.RetirementBenefitPolicy[0].RSB_DiscontinueType);
            messageDS.AcceptChanges();
            #endregion

            #region Create Retirement benefit Policy...

            foreach (SeparationTypeDS.RetirementBenefitPolicyRow row in sepTypeDS.RetirementBenefitPolicy.Rows)
            {
                cmd.Parameters.AddWithValue("p_RSB_DiscontinueTypeID", row.RSB_DiscontinueTypeID);
                cmd.Parameters.AddWithValue("p_RSB_ADCode", row.RSB_ADCode);
                cmd.Parameters.AddWithValue("p_RSB_Payment_Per", row.RSB_Payment_Per);
                cmd.Parameters.AddWithValue("p_RSB_MinimumWorkingDays", row.RSB_MinimumWorkingDays);
                
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RETIREMENT_BENEFIT_POLICY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _getRetirementBenefitPolicy(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            SeparationTypeDS sepTypeDS = new SeparationTypeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();//

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRetirementBenefitPolicy");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["RSB_DiscontinueTypeID1"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["RSB_DiscontinueTypeID2"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sepTypeDS, sepTypeDS.RetirementBenefitPolicy.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_RETIREMENT_BENEFIT_POLICY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            sepTypeDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(sepTypeDS.RetirementBenefitPolicy);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}
