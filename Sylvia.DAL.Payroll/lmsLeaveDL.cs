using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using Sylvia.Common.Reports;

namespace Sylvia.DAL.Payroll
{
    class lmsLeaveDL
    {
        public lmsLeaveDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                #region Leave Type
                case ActionID.ACTION_LMS_LEAVETYPE_CREATE:
                    return _createLeaveType(inputDS);
                case ActionID.ACTION_LMS_LEAVETYPE_UPD:
                    return this._updateLeaveType(inputDS);
                case ActionID.ACTION_LMS_LEAVETYPE_DEL:
                    return this._deleteLeaveType(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST:
                    return _getLeaveTypeList(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_ALL:
                    return _getLeaveTypeListAll(inputDS);
                case ActionID.ACTION_LMS_DOES_LEAVETYPE_EXIST:
                    return this._doesLeaveTypeExist(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_BY_LOGONID:
                    return this._getLeaveTypeListByLoginID(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_BY_LOGONID_PREVYEAR:
                    return this._getLeaveTypeListByLoginID_PrevYear(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_BY_EMPCODE:
                    return this._getLeaveTypeListByEmpCode(inputDS);
                case ActionID.NA_ACTION_GET_NONCOUNTABLE_LEAVELIST:
                    return this._getNonCountableLeaveList(inputDS);
                #endregion

                #region Leave Activity
                case ActionID.NA_ACTION_LMS_GET_ACTIVITY_LIST_ALL:
                    return this._getActivityListAll(inputDS);
                case ActionID.ACTION_LMS_ACTIVITIES_UPD:
                    return this._updateActivity(inputDS);
                #endregion

                #region Leave Request & Leave
                case ActionID.ACTION_LMS_LEAVE_ADD:
                    return _createLeave(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVE_LIST:
                    return _getLeaveList(inputDS);
                case ActionID.NA_ACTION_LMS_GET_USER_BY_EMPCODE:
                    return _getUserByEmpCode(inputDS);
                #endregion

                #region Leave Approval
                case ActionID.NA_ACTION_LMS_GET_LEAVE_SUMMERY:
                    return _getLeaveSummary(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_SUMMERY_BY_EMPID:
                    return _getLeaveSummaryByEmpID(inputDS);
                case ActionID.NA_ACTION_GET_PEN_LEAVE_SUMMERY_BY_EMPID:
                    return _getPendingLeaveSummaryByEmpID(inputDS);
                case ActionID.NA_ACTION_GET_PEN_LEAVE_SUMMERY_BY_EMPCODE:
                    return _getPendingLeaveSummaryByEmpCode(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_SUMMERY_BY_LRID:
                    return _getLeaveSummaryByLeaveRequestID(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_SUMMERY_BY_EMPCODE:
                    return _getLeaveSummaryByEmpCode(inputDS);
                case ActionID.ACTION_LMS_LEAVE_APPROVAL_ADD:
                    return _createLeaveApproval(inputDS);
                #endregion

                #region Others
                case ActionID.NA_ACTION_LMS_GET_USER_FOR_DELEGATE:
                    return _getUserListForDelegate(inputDS);
                #endregion
                case ActionID.NA_ACTION_GET_LEAVE_HISTORY:
                    return _getLeaveHistory(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_TENURE_HISTORY:
                    return _getLeaveTenureHistory(inputDS);

                case ActionID.NA_ACTION_GET_LEAVE_REQUEST_SELF:
                    return _getLeaveRequest_Self(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_REQUEST_SELF_EMPCODE:
                    return _getLeaveRequest_Self_EmpCode(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_REQUEST_SELF_SUBORDINATE:
                    return _getLeaveRequest_Self_Subordinate(inputDS);
                case ActionID.NA_ACTION_GET_LR_SELF_SUBORDINATE_EMPCODE:
                    return _getLR_Self_Subordinate_EmpCode(inputDS);

                case ActionID.NA_ACTION_LMS_GET_LEAVE_LIST_BY_EMPID_CFYEAR:
                    return _getLeaveListByEmpID_CFYear(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_LIST_BY_LRID:
                    return _getLeaveListByRequestID(inputDS);

                case ActionID.NA_ACTION_LMS_GET_EMPLOYEE_FOR_LPS:
                    return _getEmployeeForLPS(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LPR_LIST:
                    return this._getLeavePlanRuleAll(inputDS);


                case ActionID.ACTION_LEAVE_PLAN_RULE_ADD:
                    return this._createLeavePlanRule(inputDS);
                case ActionID.ACTION_LEAVE_PLAN_RULE_UPD:
                    return this._updateLeavePlanRule(inputDS);
                case ActionID.ACTION_LEAVE_PLAN_RULE_DEL:
                    return this._deleteLeavePlanRule(inputDS);
                case ActionID.ACTION_CREATE_LEAVE_PLAN_PARKING:
                    return this._createLeavePlanParking(inputDS);

                case ActionID.NA_ACTION_LMS_GET_EMP_FOR_LPS_INDIVIDUAL:
                    return _getEmpForLPS_Self_Subordinate(inputDS);
                case ActionID.NA_ACTION_LMS_GET_EMP_FOR_LPS_APPROVAL:
                    return _getEmpForLPS_Approval(inputDS);

                case ActionID.ACTION_LAHC_CREATE:
                    return _createLAHC(inputDS);
                case ActionID.ACTION_LAHC_DEL:
                    return _deletLAHC(inputDS);

                case ActionID.NA_ACTION_LMS_GET_EMPLOYEE_FOR_ENCASHMENT_VERIFICATION:
                    return _getEmployeeForEncashmentVerification(inputDS);
                case ActionID.ACTION_LMS_ENCASHMENT_VERIFICATION_CREATE:
                    return _createEncashmentVerification(inputDS);
                case ActionID.NA_ACTION_LMS_GET_PENDING_LEAVE_ENCASHMENT_DATA:
                    return _getPendingLeaveEncashmentData(inputDS);
                case ActionID.ACTION_LMS_ENCASHMENT_VERIFICATION_APPROVE:
                    return _approveEncashmentVerification(inputDS);

                case ActionID.NA_ACTION_LMS_GET_LAHC_ALL:
                    return _getLAHC_All(inputDS);

                case ActionID.NA_ACTION_GET_LEAVE_HISTORY_IN_DATE_RANGE:
                    return _getLeaveHistory_InDateRange(inputDS);

                case ActionID.ACTION_LMS_LEAVE_ADD_MULTIPLE_EMPLOYEE:
                    return _createLeave_MultipleEmployee(inputDS);
                case ActionID.NA_GET_ACTION_LEAVE_GOVERNANCE_INFO:
                    return _getLeaveGovernanceInfo(inputDS);
                case ActionID.ACTION_LMS_LEAVE_GOVERNANCE_CREATE:
                    return _createleaveGovernance(inputDS);
                case ActionID.NA_ACTION_GET_EMP_LEAVE_ENCAS_BY_EMPTYPESITE:
                    return _loadEmpLeaveEncash_By_EmpTypeSite(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_FOR_CC_MAIL:
                    return _getLeaveTypeListForCCMail(inputDS);
                case ActionID.ACTION_LMS_LEAVE_CC_MAIL_USER_CREATE:
                    return _LMS_CC_MailUserCreate(inputDS);

                case ActionID.ACTION_LMS_LEAVE_APPROVAL_POLICY_SAVE:
                    return _saveLMS_LeaveApprovalPolicy(inputDS);
                case ActionID.ACTION_LMS_LEAVE_APPROVAL_POLICY_DELETE:
                    return _deleteLMS_LeaveApprovalPolicy(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_APPROVAL_POLICY:
                    return _getLMS_LeaveApprovalPolicy(inputDS);

                case ActionID.NA_ACTION_GET_PENDING_APPROVAL_LEAVE:
                    return _getPendingApprovalLeaveList(inputDS);

                case ActionID.NA_ACTION_LMS_GET_BYPASS_APPROVER:
                    return _getLMS_BypassApprover(inputDS);
                case ActionID.NA_ACTION_GET_INFORMATION_ANY_LMS:
                    return _getInformationAny_LMS(inputDS);

                case ActionID.ACTION_EMPLOYEE_ABSENCE_ADD:
                    return _saveAbsentEmployees(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_APPROVAL_REPORT:
                    return _getLeaveApprovalReport(inputDS);
                case ActionID.NA_ACTION_GET_EMP_WORKSPACE_INFO:
                    return _getEmployeeWorkSpaceInfo(inputDS);

                case ActionID.NA_ACTION_LMS_GET_LEAVE_LIST_BY_EMPID_CFYEAR_PREVYEAR:
                    return _getLeaveListByEmpID_CFYear_PrevYear(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        #region Leave Type
        private DataSet _createLeaveType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVETYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            leaveDS.Merge(inputDS.Tables[leaveDS.LeaveType.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveDS.LeaveTypeRow row in leaveDS.LeaveType.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_LeaveTypeId", genPK);

                if (row.IsLeaveTypeCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeCode", row.LeaveTypeCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeCode", DBNull.Value);
                }

                if (row.IsLeaveTypeNameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeName", row.LeaveTypeName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeName", DBNull.Value);
                }

                if (row.IsLeaveTypeRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeRemarks", row.LeaveTypeRemarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeRemarks", DBNull.Value);
                }

                if (row.IsIsActiveNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_IsActive", DBNull.Value);
                }

                if (row.IsIsWithPayNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_IsWithPay", row.IsWithPay);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_IsWithPay", DBNull.Value);
                }

                //Kaysar, 19-Jan-2011
                if (row.IsPaymode_PercentNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_PaymodePercent", row.Paymode_Percent);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_PaymodePercent", DBNull.Value);
                }
                //

                //if (row.IsCurrentUserIDNull() == false)
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", DBNull.Value);
                //}
                #region raqib
                //raqib 03 nov
                if (row.IsApplyForNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ApplyFor", row.ApplyFor);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ApplyFor", DBNull.Value);
                }
                if (row.IsISMAILNOTIFICATIONSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_isMailaNotifications", row.ISMAILNOTIFICATIONS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_isMailaNotifications", DBNull.Value);

                }

                if (row.IsISSTATICNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_IsStatic", row.ISSTATIC);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_IsStatic", DBNull.Value);
                }
                if (row.IsMAXLEAVEAT_ATIMENull() == false)
                {
                    cmd.Parameters.AddWithValue("P_MaxLeaveAtaTime", row.MAXLEAVEAT_ATIME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_MaxLeaveAtaTime", DBNull.Value);
                }
                #endregion
                /////////////

                cmd.Parameters.AddWithValue("P_MinLeaveAvail_At_A_Time", row.MinLeaveAvail_At_A_Time);
                cmd.Parameters.AddWithValue("P_IsForNextYear", row.IsForNextYear);
                cmd.Parameters.AddWithValue("P_IsCalendarDay", row.IsCalendarDay);
                cmd.Parameters.AddWithValue("P_ApplicableReligion", row.ApplicableReligion);
                cmd.Parameters.AddWithValue("P_ApplicableTimes", row.ApplicableTimes);
                cmd.Parameters.AddWithValue("P_DependOnLeavePlan", row.DependOnLeavePlan);
                cmd.Parameters.AddWithValue("P_LFA_Applicable", row.LFA_Applicable);
                cmd.Parameters.AddWithValue("P_ISserviceYearExcluded", row.ISserviceYearExcluded);
                cmd.Parameters.AddWithValue("p_DestinationRequiredDays", row.DestinationRequiredDays);
                cmd.Parameters.AddWithValue("p_DelegateRequiredDays", row.DelegateRequiredDays);

                if (row.IsDependencyLeaveTypeIDNull() == false) cmd.Parameters.AddWithValue("p_DependencyLeaveTypeID", row.DependencyLeaveTypeID);
                else cmd.Parameters.AddWithValue("p_DependencyLeaveTypeID", DBNull.Value);
                cmd.Parameters.AddWithValue("p_ConsecutiveApplicable", row.ConsecutiveApplicable);
                cmd.Parameters.AddWithValue("p_ApplicableForHoliday", row.ApplicableForHoliday);

                if (row.IsLeave_Suffix_PrefixNull() == false) cmd.Parameters.AddWithValue("p_Leave_Suffix_Prefix", row.Leave_Suffix_Prefix);
                else cmd.Parameters.AddWithValue("p_Leave_Suffix_Prefix", DBNull.Value);

                cmd.Parameters.AddWithValue("p_OutOfCountryAllowed", row.OutOfCountryAllowed);
                cmd.Parameters.AddWithValue("p_ConsecutiveApply_Different", row.ConsecutiveApply_Different);
                cmd.Parameters.AddWithValue("p_IsRevocableByApplicant", row.IsRevocableByApplicant);

                if (row.IsDeductableLeaveTypeIDNull() == false) cmd.Parameters.AddWithValue("p_DeductableLeaveTypeID", row.DeductableLeaveTypeID);
                else cmd.Parameters.AddWithValue("p_DeductableLeaveTypeID", DBNull.Value);
                cmd.Parameters.AddWithValue("p_MinDeductableBalance", row.MinDeductableBalance);

                cmd.Parameters.AddWithValue("p_IsForwardLeaveApplicable", row.IsForwardLeaveApplicable);
                cmd.Parameters.AddWithValue("p_NonCountableLeave", row.NonCountableLeave);
                cmd.Parameters.AddWithValue("p_PartiallyRejectByApprover", row.PartiallyRejectByApprover);
                cmd.Parameters.AddWithValue("p_AttachmentRequiredDays", row.AttachmentRequiredDays);
                cmd.Parameters.AddWithValue("p_IsAbsenceNotAllowed", row.IsAbsenceNotAllowed);
                cmd.Parameters.AddWithValue("p_IsLeavePathFlexibility", row.IsLeavePathFlexibility);
                cmd.Parameters.AddWithValue("p_ChildAllow", row.ChildAllow);
                cmd.Parameters.AddWithValue("p_ApplyValidation", row.ApplyValidation);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVETYPE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateLeaveType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVETYPE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            leaveDS.Merge(inputDS.Tables[leaveDS.LeaveType.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveDS.LeaveTypeRow row in leaveDS.LeaveType.Rows)
            {
                if (row.IsLeaveTypeNameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeName", row.LeaveTypeName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeName", DBNull.Value);
                }

                if (row.IsLeaveTypeRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeRemarks", row.LeaveTypeRemarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeRemarks", DBNull.Value);
                }

                if (row.IsIsActiveNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_IsActive", DBNull.Value);
                }

                if (row.IsIsWithPayNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_IsWithPay", row.IsWithPay);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_IsWithPay", DBNull.Value);
                }

                if (row.IsLeaveTypeCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeCode", row.LeaveTypeCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeCode", DBNull.Value);
                }

                //Kaysar, 19-Jan-2011
                if (row.IsPaymode_PercentNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_PaymodePercent", row.Paymode_Percent);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_PaymodePercent", DBNull.Value);
                }
                //

                //if (row.IsCurrentUserIDNull() == false)
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", DBNull.Value);
                //}
                #region raqib update
                //raqib 03 nov
                if (row.IsApplyForNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ApplyFor", row.ApplyFor);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ApplyFor", DBNull.Value);
                }
                if (row.IsISMAILNOTIFICATIONSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_isMailaNotifications", row.ISMAILNOTIFICATIONS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_isMailaNotifications", DBNull.Value);

                }

                if (row.IsISSTATICNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_IsStatic", row.ISSTATIC);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_IsStatic", DBNull.Value);
                }
                if (row.IsMAXLEAVEAT_ATIMENull() == false)
                {
                    cmd.Parameters.AddWithValue("P_MaxLeaveAtaTime", row.MAXLEAVEAT_ATIME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_MaxLeaveAtaTime", DBNull.Value);
                }
                #endregion
                //raqib

                cmd.Parameters.AddWithValue("P_MinLeaveAvail_At_A_Time", row.MinLeaveAvail_At_A_Time);
                cmd.Parameters.AddWithValue("P_IsForNextYear", row.IsForNextYear);
                cmd.Parameters.AddWithValue("P_IsCalendarDay", row.IsCalendarDay);
                cmd.Parameters.AddWithValue("P_ApplicableReligion", row.ApplicableReligion);
                cmd.Parameters.AddWithValue("P_ApplicableTimes", row.ApplicableTimes);
                cmd.Parameters.AddWithValue("P_DependOnLeavePlan", row.DependOnLeavePlan);
                cmd.Parameters.AddWithValue("P_LFA_Applicable", row.LFA_Applicable);
                cmd.Parameters.AddWithValue("P_ISserviceYearExcluded", row.ISserviceYearExcluded);
                cmd.Parameters.AddWithValue("p_DestinationRequiredDays", row.DestinationRequiredDays);
                cmd.Parameters.AddWithValue("p_DelegateRequiredDays", row.DelegateRequiredDays);

                if (row.IsDependencyLeaveTypeIDNull() == false) cmd.Parameters.AddWithValue("p_DependencyLeaveTypeID", row.DependencyLeaveTypeID);
                else cmd.Parameters.AddWithValue("p_DependencyLeaveTypeID", DBNull.Value);
                cmd.Parameters.AddWithValue("p_ConsecutiveApplicable", row.ConsecutiveApplicable);
                cmd.Parameters.AddWithValue("p_ApplicableForHoliday", row.ApplicableForHoliday);

                if (row.IsLeave_Suffix_PrefixNull() == false) cmd.Parameters.AddWithValue("p_Leave_Suffix_Prefix", row.Leave_Suffix_Prefix);
                else cmd.Parameters.AddWithValue("p_Leave_Suffix_Prefix", DBNull.Value);

                cmd.Parameters.AddWithValue("p_OutOfCountryAllowed", row.OutOfCountryAllowed);
                cmd.Parameters.AddWithValue("p_ConsecutiveApply_Different", row.ConsecutiveApply_Different);
                cmd.Parameters.AddWithValue("p_IsRevocableByApplicant", row.IsRevocableByApplicant);

                if (row.IsDeductableLeaveTypeIDNull() == false) cmd.Parameters.AddWithValue("p_DeductableLeaveTypeID", row.DeductableLeaveTypeID);
                else cmd.Parameters.AddWithValue("p_DeductableLeaveTypeID", DBNull.Value);
                cmd.Parameters.AddWithValue("p_MinDeductableBalance", row.MinDeductableBalance);

                cmd.Parameters.AddWithValue("p_IsForwardLeaveApplicable", row.IsForwardLeaveApplicable);
                cmd.Parameters.AddWithValue("p_NonCountableLeave", row.NonCountableLeave);
                cmd.Parameters.AddWithValue("p_PartiallyRejectByApprover", row.PartiallyRejectByApprover);
                cmd.Parameters.AddWithValue("p_AttachmentRequiredDays", row.AttachmentRequiredDays);
                cmd.Parameters.AddWithValue("p_IsAbsenceNotAllowed", row.IsAbsenceNotAllowed);
                cmd.Parameters.AddWithValue("p_IsLeavePathFlexibility", row.IsLeavePathFlexibility);
                cmd.Parameters.AddWithValue("p_ChildAllow", row.ChildAllow);
                cmd.Parameters.AddWithValue("p_ApplyValidation", row.ApplyValidation);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVETYPE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteLeaveType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVETYPE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_LeaveTypeCode", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVETYPE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getLeaveTypeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveTypeList");
            cmd.Parameters["Code"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update, Jarif 26 Dec 11...
            #region Old...
            //lmsLeavePO leavePO = new lmsLeavePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, leavePO, leavePO.LeaveType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}
            //leavePO.AcceptChanges();

            //lmsLeaveDS leaveDS = new lmsLeaveDS();
            //if (leavePO.LeaveType.Count > 0)
            //{
            //    foreach (lmsLeavePO.LeaveTypeRow poRow in leavePO.LeaveType.Rows)
            //    {
            //        lmsLeaveDS.LeaveTypeRow row = leaveDS.LeaveType.NewLeaveTypeRow();
            //        if (poRow.IsLeaveTypeIDNull() == false)
            //        {
            //            row.LeaveTypeID = poRow.LeaveTypeID;
            //        }
            //        if (poRow.IsLeaveTypeCodeNull() == false)
            //        {
            //            row.LeaveTypeCode = poRow.LeaveTypeCode;
            //        }
            //        if (poRow.IsLeaveTypeNameNull() == false)
            //        {
            //            row.LeaveTypeName = poRow.LeaveTypeName;
            //        }
            //        if (poRow.IsLeaveTypeRemarksNull() == false)
            //        {
            //            row.LeaveTypeRemarks = poRow.LeaveTypeRemarks;
            //        }
            //        if (poRow.IsIsActiveNull() == false)
            //        {
            //            row.IsActive = poRow.IsActive;
            //        }
            //        if (poRow.IsIsWithPayNull() == false)
            //        {
            //            row.IsWithPay = poRow.IsWithPay;
            //        }
            //        //kaysar, 19-Jan-2011
            //        if (poRow.IsPaymode_PercentNull() == false)
            //        {
            //            row.Paymode_Percent = poRow.Paymode_Percent;
            //        }
            //        //
            //        leaveDS.LeaveType.AddLeaveTypeRow(row);
            //        leaveDS.AcceptChanges();
            //    }
            //}
            #endregion

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveTypeListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;
            if (activeStatus.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveTypeListAll");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsActiveLeaveTypeList");
                if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
                else cmd.Parameters["IsActive"].Value = (object)false;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update, Jarif 26 Dec 11...
            #region Old...
            //lmsLeavePO leavePO = new lmsLeavePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, leavePO, leavePO.LeaveType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_ALL.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}
            //leavePO.AcceptChanges();

            //lmsLeaveDS leaveDS = new lmsLeaveDS();
            //if (leavePO.LeaveType.Count > 0)
            //{
            //    foreach (lmsLeavePO.LeaveTypeRow poRow in leavePO.LeaveType.Rows)
            //    {
            //        lmsLeaveDS.LeaveTypeRow row = leaveDS.LeaveType.NewLeaveTypeRow();
            //        if (poRow.IsLeaveTypeIDNull() == false)
            //        {
            //            row.LeaveTypeID = poRow.LeaveTypeID;
            //        }
            //        if (poRow.IsLeaveTypeCodeNull() == false)
            //        {
            //            row.LeaveTypeCode = poRow.LeaveTypeCode;
            //        }
            //        if (poRow.IsLeaveTypeNameNull() == false)
            //        {
            //            row.LeaveTypeName = poRow.LeaveTypeName;
            //        }
            //        if (poRow.IsLeaveTypeRemarksNull() == false)
            //        {
            //            row.LeaveTypeRemarks = poRow.LeaveTypeRemarks;
            //        }
            //        if (poRow.IsIsActiveNull() == false)
            //        {
            //            row.IsActive = poRow.IsActive;
            //        }
            //        if (poRow.IsIsWithPayNull() == false)
            //        {
            //            row.IsWithPay = poRow.IsWithPay;
            //        }
            //        //kaysar, 19-Jan-2011
            //        if (poRow.IsPaymode_PercentNull() == false)
            //        {
            //            row.Paymode_Percent = poRow.Paymode_Percent;
            //        }
            //        //
            //        leaveDS.LeaveType.AddLeaveTypeRow(row);
            //        leaveDS.AcceptChanges();
            //    }
            //}
            #endregion

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _doesLeaveTypeExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesLmsLeaveTypeExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_DOES_LEAVETYPE_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getLeaveTypeListByLoginID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveTypeListByLoginID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["LoginID"].Value = cmd.Parameters["LoginID2"].Value = (object)StringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_BY_LOGONID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveTypeListByLoginID_PrevYear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveTypeListByLoginID_PrevYear");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["LoginID"].Value = cmd.Parameters["LoginID2"].Value = cmd.Parameters["LoginID3"].Value = (object)StringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_BY_LOGONID_PREVYEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getLeaveTypeListByEmpCode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveTypeListByEmpCode");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["EmpCode"].Value = (object)StringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_BY_LOGONID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getNonCountableLeaveList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetNonCountableLeaveList");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_NONCOUNTABLE_LEAVELIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        #endregion

        #region Leave Activity
        private DataSet _getActivityListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;
            if (activeStatus.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsActivityListAll");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsActiveActivityList");
                if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
                else cmd.Parameters["IsActive"].Value = (object)false;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeavePO leavePO = new lmsLeavePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leavePO, leavePO.Activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_ACTIVITY_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leavePO.AcceptChanges();

            lmsLeaveDS leaveDS = new lmsLeaveDS();
            if (leavePO.Activities.Count > 0)
            {
                foreach (lmsLeavePO.ActivitiesRow poRow in leavePO.Activities.Rows)
                {
                    lmsLeaveDS.ActivitiesRow row = leaveDS.Activities.NewActivitiesRow();
                    if (poRow.IsLeaveActivityidNull() == false)
                    {
                        row.LeaveActivityid = poRow.LeaveActivityid;
                    }
                    if (poRow.IsActivityNameNull() == false)
                    {
                        row.ActivityName = poRow.ActivityName;
                    }
                    if (poRow.IsActivityFriendlyNameNull() == false)
                    {
                        row.ActivityFriendlyName = poRow.ActivityFriendlyName;
                    }
                    if (poRow.IsSubjectForEmailNull() == false)
                    {
                        row.SubjectForEmail = poRow.SubjectForEmail;
                    }
                    if (poRow.IsIsActiveNull() == false)
                    {
                        row.IsActive = poRow.IsActive;
                    }

                    leaveDS.Activities.AddActivitiesRow(row);
                    leaveDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _updateActivity(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_ACTIVITIES_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            leaveDS.Merge(inputDS.Tables[leaveDS.Activities.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveDS.ActivitiesRow row in leaveDS.Activities.Rows)
            {
                cmd.Parameters.AddWithValue("p_ACTIVITIESID", row.LeaveActivityid);

                if (row.ActivityFriendlyName != "")
                {
                    cmd.Parameters.AddWithValue("p_ACTIVITIESFRIENDLYNAME", row.ActivityFriendlyName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ACTIVITIESFRIENDLYNAME", DBNull.Value);
                }
                if (row.SubjectForEmail != "")
                {
                    cmd.Parameters.AddWithValue("p_SUBJECTFOREMAIL", row.SubjectForEmail);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SUBJECTFOREMAIL", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_ACTIVITIES_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        #endregion

        #region Leave Request & Leave
        private DataSet _createLeave(DataSet inputDS)
        {
            string messageBody = "", returnMessage = "";
            int employeeID = -1;
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DataBoolDS boolDS = new DataBoolDS();
            DBConnectionDS connDS = new DBConnectionDS();

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            string subjectForLFA = UtilDL.GetMailingSubjectForLFA();
            #endregion

            OleDbCommand cmdLeaveWithLFARequest = new OleDbCommand();
            cmdLeaveWithLFARequest.CommandText = "PRO_LMS_LEAVE_LFA_REQU_CREATE";
            cmdLeaveWithLFARequest.CommandType = CommandType.StoredProcedure;
            if (cmdLeaveWithLFARequest == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbCommand cmdLeaveRequest = new OleDbCommand();
            cmdLeaveRequest.CommandText = "PRO_LMS_LEAVE_REQUEST_CREATE";
            cmdLeaveRequest.CommandType = CommandType.StoredProcedure;
            if (cmdLeaveRequest == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            //Jarif, 04.Aug.2014
            OleDbCommand cmdAdjustmentLeaveRequest = new OleDbCommand();
            cmdAdjustmentLeaveRequest.CommandText = "PRO_LMS_LEAVE_REQUEST_CREATE";
            cmdAdjustmentLeaveRequest.CommandType = CommandType.StoredProcedure;

            //OleDbCommand cmdDelegate = new OleDbCommand();
            //cmdDelegate.CommandText = "PRO_DELEGATE_CREATE";
            //cmdDelegate.CommandType = CommandType.StoredProcedure;

            leaveDS.Merge(inputDS.Tables[leaveDS.LeaveRequests.TableName], false, MissingSchemaAction.Error);
            leaveDS.Merge(inputDS.Tables[leaveDS.Leave.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            foreach (lmsLeaveDS.LeaveRequestsRow row in leaveDS.LeaveRequests.Rows)
            {
                employeeID = row.EmployeeID;
                #region Save Request BY Jarif, 29 April 12...

                decimal regularDays = row.NoOfDays;
                long lfaRequestID = 0, adj_LeaveRequestID = 0;
                long leaveRequestID = IDGenerator.GetNextGenericPK();

                if (row.NoOf_Adjustment > 0)
                {
                    regularDays -= row.NoOf_Adjustment;
                    adj_LeaveRequestID = IDGenerator.GetNextGenericPK();
                }

                if (leaveRequestID == -1 || adj_LeaveRequestID == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                row.LeaveRequestID = (Int32)leaveRequestID;     // WALI :: 23-Feb-2015 :: For file upload.

                #region For Leave/ LFA Request...
                #region For Adjustment Leave Request...
                if (row.NoOf_Adjustment > 0)
                {
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_ELQID", row.ELQID);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTID", adj_LeaveRequestID);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_LCDETAILSID", row.LCDetailsID);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_LEAVETYPEID", row.Adjustment_LeaveID);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                    if (!row.IsLeaveRequestCommentsNull())
                    {
                        cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", row.LeaveRequestComments);
                    }
                    else
                    {
                        cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", " ");
                    }
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_BeginDate", row.LeaveFrom);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_EndDate", row.LeaveTo);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_NoOfDays", row.NoOfDays);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_NoOf_Adjustment", row.NoOf_Adjustment);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_IsChild", true);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_Last_ActivitiesID", row.Last_ActivitiesID);
                    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_Adjustment_LR_ID", leaveRequestID);

                    #region Shakir, Destination and Delegate Add
                    if (!row.IsCountryIDNull()) { cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_CountryID", row.CountryID); }
                    else cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_CountryID", DBNull.Value);

                    if (!row.IsDivisionIDNull()) { cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_DivisionID", row.DivisionID); }
                    else cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_DivisionID", DBNull.Value);

                    if (!row.IsDestinationNull()) { cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_Destination", row.Destination); }
                    else cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_Destination", DBNull.Value);

                    if (!row.IsDelegatedUserIDNull()) { cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_DelegatedUserID", row.DelegatedUserID); }
                    else cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_DelegatedUserID", DBNull.Value);

                    if (!row.IsDelegateNull()) { cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_Delegate", row.Delegate); }
                    else cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_Delegate", DBNull.Value);

                    if (!row.IsDelegateEmailIDNull()) { cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_DelegateEmailID", row.DelegateEmailID); }
                    else cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_DelegateEmailID", DBNull.Value);

                    #endregion

                    if (!row.IsDistrictIDNull()) { cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_DistrictID", row.DistrictID); }
                    else cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_DistrictID", DBNull.Value);

                    if (!row.IsMailing_AddressNull()) { cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_Mailing_Address", row.Mailing_Address); }
                    else cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_Mailing_Address", DBNull.Value);

                    if (!row.IsIsDisciplinaryActionNull()) { cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_IsDisciplinaryAction", row.IsDisciplinaryAction); }
                    else cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_IsDisciplinaryAction", 0);
                }
                #endregion
                if (row.ApplyWithLFA)
                {
                    row.MailingSubject = subjectForLFA;

                    #region For Leave Request...
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_ELQID", row.ELQID);//Jarif, 09.Dec.13
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LEAVEREQUESTID", leaveRequestID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LCDETAILSID", row.LCDetailsID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LEAVETYPEID", row.LeaveTypeID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                    if (row.IsLeaveRequestCommentsNull() == false)
                    {
                        cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", row.LeaveRequestComments);
                    }
                    else
                    {
                        cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", " ");
                    }
                    //Jarif, 04.Aug.2014
                    cmdLeaveRequest.Parameters.AddWithValue("p_BeginDate", row.LeaveFrom);
                    cmdLeaveRequest.Parameters.AddWithValue("p_EndDate", row.LeaveTo);
                    cmdLeaveRequest.Parameters.AddWithValue("p_NoOfDays", row.NoOfDays);
                    cmdLeaveRequest.Parameters.AddWithValue("p_NoOf_Adjustment", row.NoOf_Adjustment);
                    cmdLeaveRequest.Parameters.AddWithValue("p_IsChild", false);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Last_ActivitiesID", row.Last_ActivitiesID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Adjustment_LR_ID", DBNull.Value);
                    #endregion
                    #region LFA...
                    lfaRequestID = IDGenerator.GetNextGenericPK();
                    if (lfaRequestID == -1) return UtilDL.GetDBOperationFailed();

                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAREQUESTID", lfaRequestID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAAMOUNT", row.LFAAmount);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAYEAR", row.AppliedDate.Year);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_PAYMONTH", row.AppliedDate.Month);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAREQUESTREMARKS", " ");

                    long lfaHistoryID = IDGenerator.GetNextGenericPK();
                    if (lfaHistoryID == -1) return UtilDL.GetDBOperationFailed();

                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAHISTORYID", lfaHistoryID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_RECEIVEDEMPID", row.SupervisorID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_ACTIVITIESID", Convert.ToInt32(LFARequestActivities.PendingApproval));
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAHISTORYREMARKS", " ");
                    //Jarif, 04.Aug.2014
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_BeginDate", row.LeaveFrom);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_EndDate", row.LeaveTo);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_NoOfDays", row.NoOfDays);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_NoOf_Adjustment", row.NoOf_Adjustment);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_IsChild", false);
                    //cmdLeaveWithLFARequest.Parameters.AddWithValue("p_RequestStatus", row.RequestStatus);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Last_ActivitiesID", row.Last_ActivitiesID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Adjustment_LR_ID", DBNull.Value);


                    // Shakir, Destination and Delegate Add
                    if (!row.IsCountryIDNull()) { cmdLeaveWithLFARequest.Parameters.AddWithValue("p_CountryID", row.CountryID); }
                    else cmdLeaveWithLFARequest.Parameters.AddWithValue("p_CountryID", DBNull.Value);

                    if (!row.IsDivisionIDNull()) { cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DivisionID", row.DivisionID); }
                    else cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DivisionID", DBNull.Value);

                    if (!row.IsDestinationNull()) { cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Destination", row.Destination); }
                    else cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Destination", DBNull.Value);

                    if (!row.IsDelegatedUserIDNull()) { cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DelegatedUserID", row.DelegatedUserID); }
                    else cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DelegatedUserID", DBNull.Value);

                    if (!row.IsDelegateNull()) { cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Delegate", row.Delegate); }
                    else cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Delegate", DBNull.Value);

                    if (!row.IsDelegateEmailIDNull()) { cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DelegateEmailID", row.DelegateEmailID); }
                    else cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DelegateEmailID", DBNull.Value);

                    if (!row.IsDistrictIDNull()) { cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DistrictID", row.DistrictID); }
                    else cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DistrictID", DBNull.Value);

                    if (!row.IsMailing_AddressNull()) { cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Mailing_Address", row.Mailing_Address); }
                    else cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Mailing_Address", DBNull.Value);

                    if (!row.IsIsDisciplinaryActionNull()) { cmdLeaveWithLFARequest.Parameters.AddWithValue("p_IsDisciplinaryAction", row.IsDisciplinaryAction); }
                    else cmdLeaveWithLFARequest.Parameters.AddWithValue("p_IsDisciplinaryAction", 0);
                    //
                    #endregion
                }
                else
                {
                    #region For Leave Request...
                    cmdLeaveRequest.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_ELQID", row.ELQID);//Jarif, 09.Dec.13
                    cmdLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTID", leaveRequestID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_LCDETAILSID", row.LCDetailsID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_LEAVETYPEID", row.LeaveTypeID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                    if (!row.IsLeaveRequestCommentsNull())
                    {
                        cmdLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", row.LeaveRequestComments);
                    }
                    else
                    {
                        cmdLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", " ");
                    }
                    //Jarif, 04.Aug.2014
                    cmdLeaveRequest.Parameters.AddWithValue("p_BeginDate", row.LeaveFrom);
                    cmdLeaveRequest.Parameters.AddWithValue("p_EndDate", row.LeaveTo);
                    cmdLeaveRequest.Parameters.AddWithValue("p_NoOfDays", row.NoOfDays);
                    cmdLeaveRequest.Parameters.AddWithValue("p_NoOf_Adjustment", row.NoOf_Adjustment);
                    cmdLeaveRequest.Parameters.AddWithValue("p_IsChild", false);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Last_ActivitiesID", row.Last_ActivitiesID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Adjustment_LR_ID", DBNull.Value);
                    #endregion

                    #region Shakir, Destination and Delegate Add
                    if (!row.IsCountryIDNull()) { cmdLeaveRequest.Parameters.AddWithValue("p_CountryID", row.CountryID); }
                    else cmdLeaveRequest.Parameters.AddWithValue("p_CountryID", DBNull.Value);

                    if (!row.IsDivisionIDNull()) { cmdLeaveRequest.Parameters.AddWithValue("p_DivisionID", row.DivisionID); }
                    else cmdLeaveRequest.Parameters.AddWithValue("p_DivisionID", DBNull.Value);

                    if (!row.IsDestinationNull()) { cmdLeaveRequest.Parameters.AddWithValue("p_Destination", row.Destination); }
                    else cmdLeaveRequest.Parameters.AddWithValue("p_Destination", DBNull.Value);

                    if (!row.IsDelegatedUserIDNull()) { cmdLeaveRequest.Parameters.AddWithValue("p_DelegatedUserID", row.DelegatedUserID); }
                    else cmdLeaveRequest.Parameters.AddWithValue("p_DelegatedUserID", DBNull.Value);

                    if (!row.IsDelegateNull()) { cmdLeaveRequest.Parameters.AddWithValue("p_Delegate", row.Delegate); }
                    else cmdLeaveRequest.Parameters.AddWithValue("p_Delegate", DBNull.Value);

                    if (!row.IsDelegateEmailIDNull()) { cmdLeaveRequest.Parameters.AddWithValue("p_DelegateEmailID", row.DelegateEmailID); }
                    else cmdLeaveRequest.Parameters.AddWithValue("p_DelegateEmailID", DBNull.Value);

                    if (!row.IsDistrictIDNull()) { cmdLeaveRequest.Parameters.AddWithValue("p_DistrictID", row.DistrictID); }
                    else cmdLeaveRequest.Parameters.AddWithValue("p_DistrictID", DBNull.Value);

                    if (!row.IsMailing_AddressNull()) { cmdLeaveRequest.Parameters.AddWithValue("p_Mailing_Address", row.Mailing_Address); }
                    else cmdLeaveRequest.Parameters.AddWithValue("p_Mailing_Address", DBNull.Value);

                    if (!row.IsIsDisciplinaryActionNull()) { cmdLeaveRequest.Parameters.AddWithValue("p_IsDisciplinaryAction", row.IsDisciplinaryAction); }
                    else cmdLeaveRequest.Parameters.AddWithValue("p_IsDisciplinaryAction", 0);

                    #endregion
                }
                #endregion

                #region for Transaction...
                #region CompanyName
                string companyName = System.Configuration.ConfigurationManager.AppSettings["CompanyName"].Trim();
                OleDbCommand cmdCom = null;
                cmdCom = DBCommandProvider.GetDBCommand("GetCompanyNameBYEmployeeID");
                if (cmdCom == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
                cmdCom.Parameters["EmployeeID"].Value = employeeID;

                OleDbDataAdapter adapterCom = new OleDbDataAdapter();
                adapterCom.SelectCommand = cmdCom;

                CompanyDS companyDS = new CompanyDS();
                bool bErrorCom = false;
                int nRowAffectedCom = -1;

                nRowAffectedCom = ADOController.Instance.Fill(adapterCom, companyDS, companyDS.Companies.TableName, connDS.DBConnections[0].ConnectionID, ref bErrorCom);
                if (bErrorCom == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                companyDS.AcceptChanges();
                companyName = companyDS.Companies[0].Name;
                #endregion
                bool bError = false;
                int nRowAffected = -1;

                Mail.Mail mail = new Mail.Mail();
                if (row.IsNotificationByMail)
                {
                    returnMessage = "";

                    //Kaysar, 01-Nov-2011
                    row.Url += "?pVal=lms";

                    messageBody = "<br><p style=text-align:center;><b><i>" + companyName + "</i></b></p><br>";
                    messageBody += "<br>";
                    messageBody += "<b>Dispatcher: </b>" + row.SenderEmpName + " [" + row.SenderEmpCode + "]" + "<br><br>";                    
                    messageBody += "<b>Applicant: </b>" + row.EmployeeName + " [" + row.EmployeeCode + "]<br>";
                    messageBody += "<b>Leave Type: </b>" + row.LeaveTypeName + "<br>";
                    messageBody += "<b>Leave Date: </b>" + row.LeaveRange;
                    messageBody += "<br><br><br>";
                    messageBody += "Click the following link: ";
                    messageBody += "<br>";
                    messageBody += "<a href='" + row.Url + "'>" + row.Url + "</a>";

                    if (row.IsDelegatedUserIDNull() || row.DelegatedUserID == 0)
                    {
                        if (IsEMailSendWithCredential)
                        {
                            //returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, row.FromEmailAddress, row.EmailAddress, row.MailingSubject, messageBody);
                            returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, row.FromEmailAddress, row.EmailAddress, row.CcEmailAddress, row.MailingSubject, messageBody, row.SenderEmpName);
                        }
                        else
                        {
                            returnMessage = mail.SendEmail(host, port, row.FromEmailAddress, row.EmailAddress, row.CcEmailAddress, row.MailingSubject, messageBody, row.SenderEmpName);
                        }

                        if (returnMessage == "Email successfully sent.")
                        {
                            ADOController.Instance.WriteEmailLog(row.MailingSubject, row.FromEmailAddress, row.EmailAddress, row.CcEmailAddress);

                            if (row.ApplyWithLFA) nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveWithLFARequest, connDS.DBConnections[0].ConnectionID, ref bError);
                            else nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveRequest, connDS.DBConnections[0].ConnectionID, ref bError);
                        }
                        else
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                    else
                    {
                        messageBody = "<br><p style=text-align:center;><b><i>" + companyName + "</i></b></p><br>";
                        messageBody += "<br>";
                        messageBody += "<b>Attention Mr./ Mrs. : </b>" + row.Delegate + "<br><br>";                      
                        messageBody += "<b>Mr./ Mrs. : </b>" + row.EmployeeName + " has delegated his job assignments to you. For more details, you are requested to contact him/her." + " <br>";
                        messageBody += "<br><br><br>";

                        if (IsEMailSendWithCredential)
                        {
                            returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, row.FromEmailAddress, row.DelegateEmailID, row.CcEmailAddress, "Job Delegated to You", messageBody, row.SenderEmpName);
                        }
                        else
                        {
                            returnMessage = mail.SendEmail(host, port, row.FromEmailAddress, row.DelegateEmailID, row.CcEmailAddress, "Job Delegated to You", messageBody, row.SenderEmpName);
                        }

                        if (returnMessage == "Email successfully sent.")
                        {
                            ADOController.Instance.WriteEmailLog(row.MailingSubject, row.FromEmailAddress, row.EmailAddress, row.CcEmailAddress);

                            if (row.ApplyWithLFA) nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveWithLFARequest, connDS.DBConnections[0].ConnectionID, ref bError);
                            else nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveRequest, connDS.DBConnections[0].ConnectionID, ref bError);
                        }
                        else
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                }
                else
                {
                    if (row.ApplyWithLFA) nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveWithLFARequest, connDS.DBConnections[0].ConnectionID, ref bError);
                    else nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveRequest, connDS.DBConnections[0].ConnectionID, ref bError);
                }

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                //Jarif, 04.Aug.2014
                if (row.NoOf_Adjustment > 0)
                {
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdAdjustmentLeaveRequest, connDS.DBConnections[0].ConnectionID, ref bError);
                }
                #endregion

                #region Rony :: 09 May 2017 :: Update ELQDetails 
                if (!row.IsIsDeductedFromAdjLeaveNull() && row.IsDeductedFromAdjLeave)
                {
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.CommandText = "PRO_LMS_LR_DEDUCT_ADJLEAVE";
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (cmd == null) return UtilDL.GetCommandNotFound();

                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_LeaveTypeID", row.LeaveTypeID);
                    cmd.Parameters.AddWithValue("p_DeductedAdjLeaveID", row.Deducted_AdjLeaveID);
                    cmd.Parameters.AddWithValue("p_NoOfDays", row.NoOfDays);
                    cmd.Parameters.AddWithValue("p_LeaveRequestID", leaveRequestID);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();
                }
                #endregion

                #endregion

                #region Save Leave & Leave History...

                OleDbCommand cmdLeave = new OleDbCommand();
                cmdLeave.CommandType = CommandType.StoredProcedure;

                foreach (lmsLeaveDS.LeaveRow leaveRow in leaveDS.Leave.Rows)
                {
                    long leaveID = IDGenerator.GetNextGenericPK();
                    if (leaveID == -1) return UtilDL.GetDBOperationFailed();
                    
                    cmdLeave.Parameters.AddWithValue("p_LEAVEID", leaveID);

                    if (leaveRow.IsLeaveDateNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_LEAVEDATE", leaveRow.LeaveDate);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_LEAVEDATE", DBNull.Value);
                    }

                    if (leaveRow.IsLeaveLengthHoursNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_LEAVELENGTHHOURS", leaveRow.LeaveLengthHours);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_LEAVELENGTHHOURS", DBNull.Value);
                    }

                    if (leaveRow.IsLeaveLengthDaysNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_LEAVELENGTHDAYS", leaveRow.LeaveLengthDays);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_LEAVELENGTHDAYS", DBNull.Value);
                    }

                    if (leaveRow.IsLeaveCommentsNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_LEAVECOMMENTS", leaveRow.LeaveComments);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_LEAVECOMMENTS", DBNull.Value);
                    }

                    if (regularDays > 0) cmdLeave.Parameters.AddWithValue("p_LEAVEREQUESTID", leaveRequestID);
                    else cmdLeave.Parameters.AddWithValue("p_LEAVEREQUESTID", adj_LeaveRequestID > 0 ? adj_LeaveRequestID : leaveRequestID);

                    if (leaveRow.IsStartTimeNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_STARTTIME", leaveRow.StartTime);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_STARTTIME", DBNull.Value);
                    }

                    if (leaveRow.IsEndTimeNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_ENDTIME", leaveRow.EndTime);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_ENDTIME", DBNull.Value);
                    }

                    cmdLeave.Parameters.AddWithValue("p_LEAVEHISTORYID", 0);

                    if (leaveRow.IsEmployeeIDNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_EMPLOYEEID", leaveRow.EmployeeID);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_EMPLOYEEID", DBNull.Value);
                    }

                    if (leaveRow.IsActivitiesIDNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_ACTIVITIESID", leaveRow.ActivitiesID);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_ACTIVITIESID", DBNull.Value);
                    }

                    if (leaveRow.IsDayLengthNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_DayLength", leaveRow.DayLength);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_DayLength", DBNull.Value);
                    }

                    if (leaveRow.IsDayLengthNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_DayLengthID", leaveRow.DayLengthID);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_DayLengthID", DBNull.Value);
                    }

                    //Check LeaveApply/LeaveAssign..
                    if (boolDS.DataBools[0].BoolValue) //LeaveApply...
                    {
                        cmdLeave.CommandText = "PRO_LMS_LEAVE_CREATE";

                        //Kaysar, 07-Feb-2011
                        //if (leaveRow.IsSupervisorIDNull() == false)
                        //{ cmdLeave.Parameters.AddWithValue("p_receivedempid", leaveRow.SupervisorID); }
                        //else
                        //{ cmdLeave.Parameters.AddWithValue("p_receivedempid", DBNull.Value); }

                        //if (row.IsDelegatedUserIDNull() || row.DelegatedUserID == 0)
                        //    cmdLeave.Parameters.AddWithValue("p_DelegatorEmpID", DBNull.Value);
                        //else cmdLeave.Parameters.AddWithValue("p_DelegatorEmpID", row.EmployeeID);

                        if (!leaveRow.IsReceiverEmployeeIDNull()) cmdLeave.Parameters.AddWithValue("p_receivedempid", leaveRow.ReceiverEmployeeID);
                        else cmdLeave.Parameters.AddWithValue("p_receivedempid", DBNull.Value);
                        
                        //if (!leaveRow.IsSupervisorIDNull()) cmdLeave.Parameters.AddWithValue("p_SupervisorID", leaveRow.SupervisorID);
                        if (!leaveRow.IsSupervisorIDNull())
                        {
                            if (!leaveRow.IsActivitiesIDNull() && !row.ApplicableForHoliday &&(Convert.ToInt32(leaveRow.ActivitiesID) == 108 || Convert.ToInt32(leaveRow.ActivitiesID) == 109))
                            { cmdLeave.Parameters.AddWithValue("p_SupervisorID", DBNull.Value); }
                            else cmdLeave.Parameters.AddWithValue("p_SupervisorID", leaveRow.SupervisorID); 
                        }
                        else cmdLeave.Parameters.AddWithValue("p_SupervisorID", DBNull.Value);
                    }
                    else // LeaveAssign...
                    {
                        cmdLeave.CommandText = "PRO_LMS_LEAVE_ASSIGN_CREATE";

                        if (leaveRow.IsLogedEmpIDNull() == false)
                        {
                            cmdLeave.Parameters.AddWithValue("p_LOGEDEMPID", leaveRow.LogedEmpID);
                        }
                        else
                        {
                            cmdLeave.Parameters.AddWithValue("p_LOGEDEMPID", DBNull.Value);
                        }

                        if (regularDays > 0) cmdLeave.Parameters.AddWithValue("p_LEAVETYPEID", row.LeaveTypeID);
                        else cmdLeave.Parameters.AddWithValue("p_LEAVETYPEID", row.Adjustment_LeaveID > 0 ? row.Adjustment_LeaveID : row.LeaveTypeID);
                    }

                    if (row.IsLeaveRequestCommentsNull() == false)
                    {
                        cmdLeave.Parameters.AddWithValue("p_leavereqcomments", row.LeaveRequestComments);
                    }
                    else
                    {
                        cmdLeave.Parameters.AddWithValue("p_leavereqcomments", DBNull.Value);
                    }


                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeave, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmdLeave.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    regularDays -= leaveRow.LeaveLengthDays;
                }

                #endregion

                #region Assign Delegate...
                //if (!row.IsDelegatedUserIDNull() && row.DelegatedUserID > 0)
                //{
                //    cmdDelegate.Parameters.AddWithValue("p_LoginID", row.LoginID);
                //    cmdDelegate.Parameters.AddWithValue("p_dUserID", row.DelegatedUserID);
                //    cmdDelegate.Parameters.AddWithValue("p_dEmpID", row.DelegatedEmployeeID);

                //    bError = false;
                //    nRowAffected = -1;
                //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDelegate, connDS.DBConnections[0].ConnectionID, ref bError);

                //    if (bError)
                //    {
                //        ErrorDS.Error err = errDS.Errors.NewError();
                //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //        err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD.ToString();
                //        errDS.Errors.AddError(err);
                //        errDS.AcceptChanges();
                //        return errDS;
                //    }

                //}
                #endregion
            }
            errDS.Clear();
            errDS.AcceptChanges();


            #region DELEGATE Operation :: WALI :: 04-May-2017
            OleDbCommand cmdDelegate = new OleDbCommand();
            cmdDelegate.CommandType = CommandType.StoredProcedure;
            cmdDelegate.CommandText = "PRO_DELEGATE_BY_LEAVE";

            foreach (lmsLeaveDS.LeaveRequestsRow reqRow in leaveDS.LeaveRequests.Rows)
            {
                cmdDelegate.Parameters.AddWithValue("p_LeaveRequestID", reqRow.LeaveRequestID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDelegate, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_APPROVAL_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdDelegate.Parameters.Clear();
            }
            #endregion


            DataSet returnDS = new DataSet();
            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            return returnDS;
            //
        }

        private DataSet _getLeaveList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string Status = StringDS.DataStrings[0].StringValue;

            if (Status.Equals("All")) cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveListAll");
            else cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveListCurrFiscalYear");  //for current fiscal year's data   

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 22-Dec-11)...
            #region Old...
            //lmsLeavePO leavePO = new lmsLeavePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, leavePO, leavePO.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}
            //leavePO.AcceptChanges();

            //lmsLeaveDS leaveDS = new lmsLeaveDS();
            //if (leavePO.Leave.Count > 0)
            //{
            //    foreach (lmsLeavePO.LeaveRow poRow in leavePO.Leave.Rows)
            //    {
            //        lmsLeaveDS.LeaveRow row = leaveDS.Leave.NewLeaveRow();

            //        row.LeaveID = poRow.LeaveID;

            //        if (poRow.IsLeaveDateNull() == false)
            //        {
            //            row.LeaveDate = poRow.LeaveDate;
            //        }
            //        if (poRow.IsLeaveLengthHoursNull() == false)
            //        {
            //            row.LeaveLengthHours = poRow.LeaveLengthHours;
            //        }
            //        if (poRow.IsLeaveLengthDaysNull() == false)
            //        {
            //            row.LeaveLengthDays = poRow.LeaveLengthDays;
            //        }
            //        if (poRow.IsLeaveCommentsNull() == false)
            //        {
            //            row.LeaveComments = poRow.LeaveComments;
            //        }
            //        if (poRow.IsLeaveRequestIDNull() == false)
            //        {
            //            row.LeaveRequestID = poRow.LeaveRequestID;
            //        }
            //        if (poRow.IsLastLeaveHistoryIDNull() == false)
            //        {
            //            row.LastLeaveHistoryID = poRow.LastLeaveHistoryID;
            //        }
            //        if (poRow.IsStartTimeNull() == false)
            //        {
            //            row.StartTime = poRow.StartTime;
            //        }
            //        if (poRow.IsEndTimeNull() == false)
            //        {
            //            row.EndTime = poRow.EndTime;
            //        }
            //        if (poRow.IsDayLengthNull() == false)
            //        {
            //            row.DayLength = poRow.DayLength;
            //        }
            //        if (poRow.IsDayLengthIDNull() == false)
            //        {
            //            row.DayLengthID = poRow.DayLengthID;
            //        }
            //        if (poRow.IsLeaveTypeIDNull() == false)
            //        {
            //            row.LeaveTypeID = poRow.LeaveTypeID;
            //        }
            //        if (poRow.IsEmployeeIDNull() == false)
            //        {
            //            row.EmployeeID = poRow.EmployeeID;
            //        }
            //        if (poRow.IsActivitiesIDNull() == false)
            //        {
            //            row.ActivitiesID = poRow.ActivitiesID;
            //        }

            //        leaveDS.Leave.AddLeaveRow(row);
            //        leaveDS.AcceptChanges();
            //    }
            //}
            #endregion

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getUserByEmpCode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            string scode = stringDS.DataStrings[0].StringValue;

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUserByEmpCode");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["EmpCode1"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmpCode2"].Value = stringDS.DataStrings[0].StringValue;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            UserDS userDS = new UserDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_USER_BY_EMPCODE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            stringDS.Clear();
            stringDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(stringDS);
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        #endregion

        #region Leave Approval
        private DataSet _getLeaveSummary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            /////////////////////raqib 08 nov
            //cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveSummary");
            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveSummary_New");
            ////////////////////////
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 22-Dec-11)...
            #region Old...
            //lmsLeavePO leavePO = new lmsLeavePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, leavePO, leavePO.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_SUMMERY.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}
            //leavePO.AcceptChanges();

            //lmsLeaveDS leaveDS = new lmsLeaveDS();
            //if (leavePO.Leave.Count > 0)
            //{
            //    foreach (lmsLeavePO.LeaveRow poRow in leavePO.Leave.Rows)
            //    {
            //        lmsLeaveDS.LeaveRow row = leaveDS.Leave.NewLeaveRow();

            //        row.LeaveID = poRow.LeaveID;

            //        if (poRow.IsLeaveDateNull() == false)
            //        {
            //            row.LeaveDate = poRow.LeaveDate;
            //        }
            //        if (poRow.IsLeaveLengthHoursNull() == false)
            //        {
            //            row.LeaveLengthHours = poRow.LeaveLengthHours;
            //        }
            //        if (poRow.IsLeaveLengthDaysNull() == false)
            //        {
            //            row.LeaveLengthDays = poRow.LeaveLengthDays;
            //        }
            //        if (poRow.IsLeaveCommentsNull() == false)
            //        {
            //            row.LeaveComments = poRow.LeaveComments;
            //        }
            //        if (poRow.IsLeaveRequestIDNull() == false)
            //        {
            //            row.LeaveRequestID = poRow.LeaveRequestID;
            //        }
            //        if (poRow.IsLeaveTypeIDNull() == false)
            //        {
            //            row.LeaveTypeID = poRow.LeaveTypeID;
            //        }
            //        if (poRow.IsEmployeeIDNull() == false)
            //        {
            //            row.EmployeeID = poRow.EmployeeID;
            //        }
            //        if (poRow.IsActivitiesIDNull() == false)
            //        {
            //            row.ActivitiesID = poRow.ActivitiesID;
            //        }
            //        if (poRow.IsEmployeeNameNull() == false)
            //        {
            //            row.EmployeeName = poRow.EmployeeName;
            //        } 
            //        if (poRow.IsLeaveTypeNameNull() == false)
            //        {
            //            row.LeaveTypeName = poRow.LeaveTypeName;
            //        } 
            //        if (poRow.IsDateRangeNull() == false)
            //        {
            //            row.DateRange = poRow.DateRange;
            //        }
            //        if (poRow.IsNoOfDaysNull() == false)
            //        {
            //            row.NoOfDays = poRow.NoOfDays;
            //        }
            //        if (poRow.IsSenderUserIDNull() == false)
            //        {
            //            row.SenderUserID = poRow.SenderUserID;
            //        }
            //        if (poRow.IsReceivedUserIDNull() == false)
            //        {
            //            row.ReceivedUserID = poRow.ReceivedUserID;
            //        }
            //        if (poRow.IsEmailNull() == false)
            //        {
            //            row.Email = poRow.Email;
            //        }
            //        if (poRow.IsTotalOnDutyEmployeeNull() == false)
            //        {
            //            row.TotalOnDutyEmployee = poRow.TotalOnDutyEmployee;
            //        }
            //        if (poRow.IsTotalDeptEmployeeNull() == false)
            //        {
            //            row.TotalDeptEmployee = poRow.TotalDeptEmployee;
            //        }
            //        if (poRow.IsDepartmentIDNull() == false)
            //        {
            //            row.DepartmentID = poRow.DepartmentID;
            //        }
            //        if (poRow.IsBeginDateNull() == false)
            //        {
            //            row.BeginDate = poRow.BeginDate;
            //        }
            //        if (poRow.IsEndDateNull() == false)
            //        {
            //            row.EndDate = poRow.EndDate;
            //        }
            //        if (poRow.IsEmployeeCodeNull() == false)
            //        {
            //            row.EmployeeCode = poRow.EmployeeCode;
            //        }
            //        if (poRow.IsleavehistorycommentsNull() == false)
            //        {
            //            row.leavehistorycomments = poRow.leavehistorycomments;
            //        }
            //        else row.leavehistorycomments = "";

            //        if (poRow.IsleaverequestcommentsNull() == false)
            //        {
            //            row.leaverequestcomments = poRow.leaverequestcomments;
            //        }
            //        else row.leaverequestcomments = "";

            //        if (poRow.IsAPPLIEDDATENull() == false)
            //        {
            //            row.APPLIEDDATE = poRow.APPLIEDDATE;
            //        }

            //        leaveDS.Leave.AddLeaveRow(row);
            //        leaveDS.AcceptChanges();
            //    }
            //}
            #endregion

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_SUMMERY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveSummaryByEmpID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLeaveSummaryByEmpIDWithSubC");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["SupervisorID1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["SupervisorID2"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["ToDate"].Value = dateDS.DataDates[1].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 11-Sep-13)...

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_SUMMERY_BY_EMPID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getPendingLeaveSummaryByEmpID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetPendingLeaveSummaryByEmpIDWithSubC");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["ActivitiesID_R"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
            cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["SupervisorID1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["SupervisorID2"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 11-Sep-13)...

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PEN_LEAVE_SUMMERY_BY_EMPID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getPendingLeaveSummaryByEmpCode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetPendingLeaveSummaryByEmpCode");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["ActivitiesID_R"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 11-Sep-13)...

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PEN_LEAVE_SUMMERY_BY_EMPCODE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveSummaryByLeaveRequestID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLeaveSummaryByLRID");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["LeaveRequestID"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 11-Sep-13)...

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_SUMMERY_BY_LRID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveSummaryByEmpCode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLeaveSummaryByEmpCode");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["ToDate"].Value = dateDS.DataDates[1].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 11-Sep-13)...

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_SUMMERY_BY_EMPCODE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createLeaveApproval(DataSet inputDS)
        {
            #region Local Variable Declaration
            int employeeID = -1;
            string applicantEmail = "", applicantName = "", oldDelegateEmailID = "", newDelegateEmailID = "";
            bool delegateMailRequired = false;

            long leaveHistoryID = 0, lfaHistoryID = 0;
            ErrorDS errDS = new ErrorDS();
            DataBoolDS boolDS = new DataBoolDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            string messageBody = "", returnMessage = "", URL = "";
            bool bError = false;
            int nRowAffected = -1, vLeaveRequestID = 0;
            #endregion

            #region Get Email Information from UtilDL
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            #region For LFA By Jarif, 02 May 12...
            OleDbCommand cmdLFARequestHistory = new OleDbCommand();
            cmdLFARequestHistory.CommandType = CommandType.StoredProcedure;

            cmdLFARequestHistory.CommandText = "PRO_LFA_REQ_APPROVAL_CREATE";
            if (cmdLFARequestHistory == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            #endregion

            OleDbCommand cmdLeaveHistory = new OleDbCommand();
            cmdLeaveHistory.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmdLeaveRequest = new OleDbCommand();
            cmdLeaveRequest.CommandType = CommandType.StoredProcedure;
            cmdLeaveRequest.CommandText = "PRO_LMS_REQUEST_UPDATE";

            if (cmdLeaveHistory == null || cmdLeaveRequest == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            leaveDS.Merge(inputDS.Tables[leaveDS.LeaveRequests.TableName], false, MissingSchemaAction.Error);
            leaveDS.Merge(inputDS.Tables[leaveDS.LeaveHistory.TableName], false, MissingSchemaAction.Error);
            leaveDS.Merge(inputDS.Tables[leaveDS.EmailPacket.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Request Table Saving...
            foreach (lmsLeaveDS.LeaveRequestsRow reqRow in leaveDS.LeaveRequests.Rows)
            {
                vLeaveRequestID = reqRow.LeaveRequestID;
                cmdLeaveRequest.Parameters.AddWithValue("p_LeaveRequestID", reqRow.LeaveRequestID);
                cmdLeaveRequest.Parameters.AddWithValue("p_ActivitiesID", reqRow.ActivitiesID);
                cmdLeaveRequest.Parameters.AddWithValue("p_TotalActivities", reqRow.TotalActivities);


                if (!reqRow.IsDelegateNull())
                {                    
                    cmdLeaveRequest.Parameters.AddWithValue("p_DelegateCode", reqRow.Delegate);

                    #region Delegate Email Address Collect to mail
                    if (reqRow.ActivitiesID == 101)
                    {
                        try
                        {
                            delegateMailRequired = true;
                            if (!reqRow.IsEmployeeCodeNull() && !reqRow.IsEmployeeNameNull()) applicantName = reqRow.EmployeeCode + " - " + reqRow.EmployeeName;
                            if (!reqRow.IsEmailAddressNull()) applicantEmail = reqRow.EmailAddress;
                            if (!reqRow.IsDelegateEmailIDNull()) oldDelegateEmailID = reqRow.DelegateEmailID;
                            newDelegateEmailID = getDelegateEmailAddress(inputDS, reqRow.Delegate);
                        }
                        catch { }
                    }
                    #endregion

                }
                else cmdLeaveRequest.Parameters.AddWithValue("p_DelegateCode", DBNull.Value);


                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveRequest, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_APPROVAL_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdLeaveRequest.Parameters.Clear();
            }
            #endregion

            foreach (lmsLeaveDS.LeaveHistoryRow row in leaveDS.LeaveHistory.Rows)
            {
                employeeID = row.EmployeeID;
                #region Save Leave Approval...
                cmdLeaveHistory.Parameters.AddWithValue("p_LEAVEHISTORYID", 0);
                cmdLeaveHistory.Parameters.AddWithValue("p_LEAVEID", row.LeaveID);
                cmdLeaveHistory.Parameters.AddWithValue("p_SENDERUSERID", row.SenderUserID);
                cmdLeaveHistory.Parameters.AddWithValue("p_RECEIVEDEMPID", row.ReceivedEmpID);
                cmdLeaveHistory.Parameters.AddWithValue("p_ACTIVITIESID", row.ActivitiesID);
                cmdLeaveHistory.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);
                cmdLeaveHistory.Parameters.AddWithValue("p_LEAVETYPEID", row.LeaveTypeID);

                if (row.IsLeaveApprovedInDaysNull() == false)
                {
                    cmdLeaveHistory.Parameters.AddWithValue("p_LEAVEAPPROVEDINDAYS", row.LeaveApprovedInDays);
                }
                else
                {
                    cmdLeaveHistory.Parameters.AddWithValue("p_LEAVEAPPROVEDINDAYS", DBNull.Value);
                }

                //Check LeaveApproval/ApprovalDetails..
                if (boolDS.DataBools[1].BoolValue) //LeaveApproval...
                {
                    cmdLeaveHistory.Parameters.AddWithValue("p_LeaveRequestID", row.LeaveRequestID);

                    if (!row.IsLeaveHistoryCommentsNull()) cmdLeaveHistory.Parameters.AddWithValue("p_LEAVEHISTORYCOMMENTS", row.LeaveHistoryComments);                    
                    else cmdLeaveHistory.Parameters.AddWithValue("p_LEAVEHISTORYCOMMENTS", " ");                    

                    cmdLeaveHistory.CommandText = "PRO_LMS_BULK_LEAVE_APPROVAL"; //"PRO_LMS_LEAVE_APPROVAL_CREATE";
                }
                else // ApprovalDetails...
                {
                    cmdLeaveHistory.CommandText = "PRO_LMS_APPROVAL_DETAIL_CREATE";

                    if (row.IsLeaveLengthInDaysNull() == false)
                    {
                        cmdLeaveHistory.Parameters.AddWithValue("p_LEAVELENGTHDAYS", row.LeaveLengthInDays);
                    }
                    else
                    {
                        cmdLeaveHistory.Parameters.AddWithValue("p_LEAVELENGTHDAYS", DBNull.Value);
                    }

                    if (row.IsLeaveLengthInHoursNull() == false)
                    {
                        cmdLeaveHistory.Parameters.AddWithValue("p_LEAVELENGTHHOURS", row.LeaveLengthInHours);
                    }
                    else
                    {
                        cmdLeaveHistory.Parameters.AddWithValue("p_LEAVELENGTHHOURS", DBNull.Value);
                    }

                    if (row.IsLeaveHistoryCommentsNull() == false)
                    {
                        cmdLeaveHistory.Parameters.AddWithValue("p_LEAVEHISTORYCOMMENTS", row.LeaveHistoryComments);
                    }
                    else
                    {
                        cmdLeaveHistory.Parameters.AddWithValue("p_LEAVEHISTORYCOMMENTS", " ");
                    }
                }

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveHistory, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_APPROVAL_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdLeaveHistory.Parameters.Clear();
                #endregion

                #region Save LFA Approval BY Jarif, 02 May 12...
                if (row.LFARequestID > 0)
                {
                    lfaHistoryID = IDGenerator.GetNextGenericPK();
                    if (lfaHistoryID == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }

                    cmdLFARequestHistory.Parameters.AddWithValue("p_LFAHISTORYID", lfaHistoryID);
                    cmdLFARequestHistory.Parameters.AddWithValue("p_LFAREQUESTID", row.LFARequestID);
                    cmdLFARequestHistory.Parameters.AddWithValue("p_SENDERUSERID", row.SenderUserID);
                    cmdLFARequestHistory.Parameters.AddWithValue("p_RECEIVEDEMPID", row.ReceivedEmpID);

                    if (row.ActivitiesID == Convert.ToInt32(LeaveActivities.Approve))
                    {
                        cmdLFARequestHistory.Parameters.AddWithValue("p_ACTIVITIESID", Convert.ToInt32(LFARequestActivities.Approve));
                    }
                    else if (row.ActivitiesID == Convert.ToInt32(LeaveActivities.Reject))
                    {
                        cmdLFARequestHistory.Parameters.AddWithValue("p_ACTIVITIESID", Convert.ToInt32(LFARequestActivities.Reject));
                    }
                    else
                    {
                        cmdLFARequestHistory.Parameters.AddWithValue("p_ACTIVITIESID", Convert.ToInt32(LFARequestActivities.Cancel));
                    }

                    cmdLFARequestHistory.Parameters.AddWithValue("p_LFAHISTORYREMARKS", " ");

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLFARequestHistory, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LFA_REQUEST_APPROVAL_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                    cmdLFARequestHistory.Parameters.Clear();
                }
                #endregion
            }


            #region DELEGATE Operation :: WALI :: 04-May-2017

            OleDbCommand cmdDelegate = new OleDbCommand();
            cmdDelegate.CommandType = CommandType.StoredProcedure;
            cmdDelegate.CommandText = "PRO_DELEGATE_BY_LEAVE";

            foreach (lmsLeaveDS.LeaveRequestsRow reqRow in leaveDS.LeaveRequests.Rows)
            {
                cmdDelegate.Parameters.AddWithValue("p_LeaveRequestID", reqRow.LeaveRequestID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDelegate, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_APPROVAL_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdDelegate.Parameters.Clear();
            }
            #endregion

            #region Rony :: 14 May 2017 ::Rollback Deductable Leave Days when leave is rejected or canceled
            foreach (lmsLeaveDS.LeaveRequestsRow rRow in leaveDS.LeaveRequests.Rows)
            {
                if (!rRow.IsDeductableLeaveTypeIDNull() && !rRow.IsRollbackDeductableLeaveDaysNull() 
                    && rRow.RollbackDeductableLeaveDays > 0)
                {
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.CommandText = "PRO_LMS_DEDUCT_LEAVE_ROLLBACK";
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (cmd == null) return UtilDL.GetCommandNotFound();

                    cmd.Parameters.AddWithValue("p_LeaveRequestID", rRow.LeaveRequestID);
                    cmd.Parameters.AddWithValue("p_NoOfRollbackDays", rRow.RollbackDeductableLeaveDays);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_APPROVAL_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();
                }

                #region Rony :: 09 May 2017 :: Update ELQDetails
                if (!rRow.IsIsDeductedFromAdjLeaveNull() && rRow.IsDeductedFromAdjLeave)
                {
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.CommandText = "PRO_LMS_LR_DEDUCT_ADJLEAVE";
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (cmd == null) return UtilDL.GetCommandNotFound();

                    cmd.Parameters.AddWithValue("p_EmployeeID", rRow.EmployeeID);
                    cmd.Parameters.AddWithValue("p_LeaveTypeID", rRow.LeaveTypeID);
                    cmd.Parameters.AddWithValue("p_DeductedAdjLeaveID", rRow.Deducted_AdjLeaveID);
                    cmd.Parameters.AddWithValue("p_NoOfDays", rRow.NoOfDays);
                    cmd.Parameters.AddWithValue("p_LeaveRequestID", rRow.LeaveRequestID);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();
                }
                #endregion
            }
            #endregion

            

            #region Send Email...
            #region CompanyName
            string companyName = System.Configuration.ConfigurationManager.AppSettings["CompanyName"].Trim();
            OleDbCommand cmdCom = null;
            cmdCom = DBCommandProvider.GetDBCommand("GetCompanyNameBYEmployeeID");
            if (cmdCom == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmdCom.Parameters["EmployeeID"].Value = employeeID;

            OleDbDataAdapter adapterCom = new OleDbDataAdapter();
            adapterCom.SelectCommand = cmdCom;

            CompanyDS companyDS = new CompanyDS();
            bool bErrorCom = false;
            int nRowAffectedCom = -1;

            nRowAffectedCom = ADOController.Instance.Fill(adapterCom, companyDS, companyDS.Companies.TableName, connDS.DBConnections[0].ConnectionID, ref bErrorCom);
            if (bErrorCom == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            companyDS.AcceptChanges();
            companyName = companyDS.Companies[0].Name;
            #endregion
            if (boolDS.DataBools[0].BoolValue) //If NotifyByMail is Checked
            {
                URL = stringDS.DataStrings[0].StringValue;
                foreach (lmsLeaveDS.EmailPacketRow emailRow in leaveDS.EmailPacket.Rows)
                {
                    returnMessage = "";
                    URL += "?pVal=lms";

                    #region Mail Body
                    if (emailRow.IsActionTakenNull() || emailRow.ActionTaken.Trim() == "")
                    {
                        // OLD mail
                        messageBody = "<br><p style=text-align:center;><b><i>" + companyName + "</i></b></p><br>";
                        messageBody += "<br>";
                        messageBody += "<b>Dispatcher: </b>" + emailRow.SenderEmpName + " [" + emailRow.SenderEmpCode + "]" + "<br><br>";
                        messageBody += "<b>Applicant: </b>" + emailRow.ApplicantEmpName + " [" + emailRow.ApplicantEmpCode + "]<br>";
                        messageBody += "<b>Leave Type: </b>" + emailRow.LeaveTypeName + "<br>";
                        messageBody += "<b>Leave Date: </b>" + emailRow.LeaveRange;
                        messageBody += "<br><br><br>";
                    }
                    else
                    {
                        // NEW mail
                        messageBody = "<br><p style=text-align:center;><b><i>" + companyName + "</i></b></p><br>";
                        messageBody += "<br>";
                        messageBody += String.Format("<b>Dispatcher: </b>{0}.<br><br>", emailRow.DispatcherEmployee);
                        messageBody += String.Format("<b>Applicant: </b>{0}.<br>", emailRow.ApplicantEmployee);

                        messageBody += "<b>Leave Type: </b>" + emailRow.LeaveTypeName + "<br>";
                        messageBody += "<b>Leave Duration: </b>" + emailRow.LeaveDuration.ToString("0.##") + " day(s)<br>";
                        messageBody += "<b>Leave Date: </b>" + emailRow.LeaveRange + "<br>";
                        if (!emailRow.IsLeaveDestinationNull()) messageBody += "<b>Destination: </b>" + emailRow.LeaveDestination + ".<br>";
                        if (!emailRow.IsLeaveReasonsNull()) { messageBody += "<br><b>Leave Reason(s): </b>" + emailRow.LeaveReasons.Trim('.') + ".<br>"; }
                        if (!emailRow.IsDelegateEmployeeNull()) 
                            messageBody += String.Format("<br><b>Delegate User during Leave Period: </b>{0}.<br>", emailRow.DelegateEmployee);
                        if (!emailRow.IsApproverEmployeeNull())
                            messageBody += String.Format("<br><b>{0} by: </b>{1}.<br>", emailRow.ActionTaken, emailRow.ApproverEmployee);

                        if (!emailRow.IsRemarksNull()) { messageBody += "<br><b>Note: </b>" + emailRow.Remarks + "."; }
                        //LeaveReasons
                        messageBody += "<br><br><br><u><b>This is a system generated notification. Signature is not required.</b></u><br>";
                        messageBody += "<br><br>";
                    }

                    if (delegateMailRequired)
                    {
                        if (emailRow.CcEmailAddress != "")
                        {
                            if (newDelegateEmailID != "") emailRow.CcEmailAddress += "," + newDelegateEmailID;
                            if (oldDelegateEmailID != "") emailRow.CcEmailAddress += "," + oldDelegateEmailID;
                        }
                        else
                        {
                            if (newDelegateEmailID != "") emailRow.CcEmailAddress += newDelegateEmailID;

                            if (emailRow.CcEmailAddress != "" && oldDelegateEmailID != "") emailRow.CcEmailAddress += "," + oldDelegateEmailID;
                            else if (emailRow.CcEmailAddress == "" && oldDelegateEmailID != "") emailRow.CcEmailAddress += newDelegateEmailID;
                        }

                        messageBody += "Delegate changed<br><br>";
                    }


                    messageBody += "Click the following link: ";
                    messageBody += "<br>";
                    messageBody += "<a href='" + URL + "'>" + URL + "</a>";
                    #endregion

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        //returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, emailRow.FromEmailAddress, emailRow.EmailAddress, emailRow.MailingSubject, messageBody);
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, emailRow.FromEmailAddress, emailRow.EmailAddress, emailRow.CcEmailAddress, emailRow.MailingSubject, messageBody, emailRow.SenderEmpName);
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, emailRow.FromEmailAddress, emailRow.EmailAddress, emailRow.CcEmailAddress, emailRow.MailingSubject, messageBody, emailRow.SenderEmpName);
                    }

                    ADOController.Instance.WriteEmailLog(emailRow.MailingSubject, emailRow.FromEmailAddress, emailRow.EmailAddress, emailRow.CcEmailAddress);
                }
            }
            #endregion


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        #endregion

        #region Others
        private DataSet _getUserListForDelegate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            string scode = stringDS.DataStrings[0].StringValue;

            if (stringDS.DataStrings[0].StringValue == "Get_UserForDepartment")
            {
                cmd = DBCommandProvider.GetDBCommand("GetDepartmentUserListForDelegate");
                if (cmd == null)
                {
                    Util.LogInfo("Command is null.");
                    return UtilDL.GetCommandNotFound();
                }
                if (stringDS.DataStrings[0].IsStringValueNull() == false)
                {
                    cmd.Parameters["LoginId"].Value = stringDS.DataStrings[1].StringValue;
                    cmd.Parameters["SiteID"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                    cmd.Parameters["DepartmentCode"].Value = stringDS.DataStrings[3].StringValue;
                }
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetUserListForDelegate");

                if (cmd == null)
                {
                    Util.LogInfo("Command is null.");
                    return UtilDL.GetCommandNotFound();
                }

                if (stringDS.DataStrings[0].IsStringValueNull() == false)
                {
                    cmd.Parameters["LoginId"].Value = stringDS.DataStrings[0].StringValue;
                    cmd.Parameters["LoginId1"].Value = stringDS.DataStrings[1].StringValue;
                    cmd.Parameters["LoginId2"].Value = stringDS.DataStrings[1].StringValue;
                    cmd.Parameters["LoginId3"].Value = stringDS.DataStrings[1].StringValue;
                    cmd.Parameters["LoginId4"].Value = stringDS.DataStrings[1].StringValue;
                }
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            #region Update By Jarif, 05 Mar 2012...
            #region Old...
            //UserPO userPO = new UserPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, userPO, userPO.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_USER.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}

            //userPO.AcceptChanges();
            //UserDS userDS = new UserDS();
            //if (userPO.Users.Count > 0)
            //{
            //    foreach (UserPO.User poUser in userPO.Users)
            //    {
            //        UserDS.User user = userDS.Users.NewUser();

            //        user.User_Id = poUser.USERID;

            //        if (poUser.IsLoginIdNull() == false)
            //        {
            //            user.LoginId = poUser.LoginId;
            //        }
            //        if (poUser.IsEMPLOYEEIDNull() == false)
            //        {
            //            user.EMPLOYEEID = poUser.EMPLOYEEID;
            //        }
            //        if (poUser.IsEmployeeCodeNull() == false)
            //        {
            //            user.EmployeeCode = poUser.EmployeeCode;
            //        }                    
            //        if (poUser.IsDEPARTMENTCODENull() == false)
            //        {
            //            user.DepartmentCode = poUser.DEPARTMENTCODE;
            //        }                    
            //        if (poUser.IsDELEGATEDUSERIDNull() == false)
            //        {
            //            user.DelegatedUserID = poUser.DELEGATEDUSERID;
            //        }

            //        userDS.Users.AddUser(user);
            //        userDS.AcceptChanges();
            //    }
            //}
            #endregion

            UserDS userDS = new UserDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_USER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            userDS.AcceptChanges();
            #endregion


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        private DataSet _getLeaveHistory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            // only for summary
            if (integerDS.DataIntegers[0].IntegerValue != 0 && integerDS.DataIntegers[1].IntegerValue != 0)
            {
                cmd = DBCommandProvider.GetDBCommand("GetLeaveSummaryByApprID_PendID");
                cmd.Parameters["pActIDapproved"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["pElqID1"].Value = integerDS.DataIntegers[2].IntegerValue;      // WALI :: 26-Jul-2015
                cmd.Parameters["pActIDpending"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
                cmd.Parameters["pActIDRecommend"].Value = Convert.ToInt32(LeaveActivities.Recommend);
                cmd.Parameters["pActIDNotRecommend"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
                cmd.Parameters["pElqID2"].Value = integerDS.DataIntegers[2].IntegerValue;      // WALI :: 26-Jul-2015
                cmd.Parameters["pCoutryID"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["pElqID3"].Value = integerDS.DataIntegers[2].IntegerValue;      // WALI :: 26-Jul-2015
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetLeaveSummaryByApprID_OR_PendID");
                cmd.Parameters["pActIDapproved"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["pActIDpending"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
                cmd.Parameters["pActIDRecommend"].Value = Convert.ToInt32(LeaveActivities.Recommend);
                cmd.Parameters["pActIDNotRecommend"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);

                if (integerDS.DataIntegers[0].IntegerValue != 0)
                {
                    cmd.Parameters["pActivityID"].Value = integerDS.DataIntegers[0].IntegerValue;
                    cmd.Parameters["pActIDRecommend2"].Value = integerDS.DataIntegers[0].IntegerValue;
                    cmd.Parameters["pActIDNotRecommend2"].Value = integerDS.DataIntegers[0].IntegerValue;
                }
                else
                {
                    cmd.Parameters["pActivityID"].Value = integerDS.DataIntegers[1].IntegerValue;
                    cmd.Parameters["pActIDRecommend2"].Value = Convert.ToInt32(LeaveActivities.Recommend);
                    cmd.Parameters["pActIDNotRecommend2"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
                }
                cmd.Parameters["pCoutryID"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["pElqID"].Value = integerDS.DataIntegers[2].IntegerValue;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveHist.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_SUMMERY_BY_EMPID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();



            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getLeaveTenureHistory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();



            if (integerDS.DataIntegers[1].IntegerValue != 0)
            {
                cmd = DBCommandProvider.GetDBCommand("GetLeaveTenureHistory");
                cmd.Parameters["pActIDapproved"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["pElqID"].Value = integerDS.DataIntegers[0].IntegerValue;
                if (integerDS.DataIntegers[1].IntegerValue != 0) cmd.Parameters["pLeaveTypeID"].Value = integerDS.DataIntegers[1].IntegerValue;
                //else cmd.Parameters["pLeaveTypeID"].Value = DBNull.Value;
                if (dateDS.DataDates[0].DateValue != null) cmd.Parameters["pLeaveFromDate"].Value = dateDS.DataDates[0].DateValue;
                else cmd.Parameters["pLeaveFromDate"].Value = DBNull.Value;
                if (dateDS.DataDates[1].DateValue != null) cmd.Parameters["pLeaveToDate"].Value = dateDS.DataDates[1].DateValue;
                else cmd.Parameters["pLeaveToDate"].Value = DBNull.Value;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetLeaveTenureHistory2");
                cmd.Parameters["pActIDapproved"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["pElqID"].Value = integerDS.DataIntegers[0].IntegerValue;
                if (dateDS.DataDates[0].DateValue != null) cmd.Parameters["pLeaveFromDate"].Value = dateDS.DataDates[0].DateValue;
                else cmd.Parameters["pLeaveFromDate"].Value = DBNull.Value;
                if (dateDS.DataDates[1].DateValue != null) cmd.Parameters["pLeaveToDate"].Value = dateDS.DataDates[1].DateValue;
                else cmd.Parameters["pLeaveToDate"].Value = DBNull.Value;
            }


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveHist.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_SUMMERY_BY_EMPID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();



            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveRequest_Self(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            // Get List of employees who have delegated this Login User
            int loginEmpID = integerDS.DataIntegers[0].IntegerValue;
            string empWithDelegate = _getEmpCodeWithDelegates(loginEmpID, connDS);

            if (dateDS.DataDates.Rows.Count > 0)
            {
                #region Select DB Command for GetLR_Self...
                cmd = DBCommandProvider.GetDBCommand("GetLR_Self");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeWithDelegate"].Value = empWithDelegate;   // WALI :: 17-May-2017
                cmd.Parameters["EmployeeID"].Value = loginEmpID;
                cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["ToDate"].Value = dateDS.DataDates[1].DateValue;

                cmd.Parameters["ActivitiesID_A"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["ActivitiesID_R"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ActivitiesID_NR"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ActivitiesID_RJ"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["ActivitiesID_C"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["ActivitiesID_PA"].Value = integerDS.DataIntegers[6].IntegerValue;
                cmd.Parameters["ActivitiesID_W"].Value = integerDS.DataIntegers[7].IntegerValue;
                cmd.Parameters["ActivitiesID_H"].Value = integerDS.DataIntegers[8].IntegerValue;
                cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["LoginEmployeeID"].Value = loginEmpID;  // WALI :: 17-May-2017
                cmd.Parameters["NonCountable"].Value = integerDS.DataIntegers[9].IntegerValue;
                cmd.Parameters["ExCountry"].Value = integerDS.DataIntegers[10].IntegerValue;

                //cmd.Parameters["ActivitiesID_T"].Value = Convert.ToInt32(LeaveActivities.Taken);
                //cmd.Parameters["ActivitiesID_W2"].Value = Convert.ToInt32(LeaveActivities.Weekend);
                //cmd.Parameters["ActivitiesID_H2"].Value = Convert.ToInt32(LeaveActivities.Holiday);
                #endregion
            }
            else
            {
                #region Select DB Command for GetLR_Self_withoutDate...
                cmd = DBCommandProvider.GetDBCommand("GetLR_Self_withoutDate");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeWithDelegate"].Value = empWithDelegate;   // WALI :: 17-May-2017
                cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["ActivitiesID_A"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["ActivitiesID_R"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ActivitiesID_NR"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ActivitiesID_RJ"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["ActivitiesID_C"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["ActivitiesID_PA"].Value = integerDS.DataIntegers[6].IntegerValue;
                cmd.Parameters["ActivitiesID_W"].Value = integerDS.DataIntegers[7].IntegerValue;
                cmd.Parameters["ActivitiesID_H"].Value = integerDS.DataIntegers[8].IntegerValue;
                cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["LoginEmployeeID"].Value = loginEmpID;  // WALI :: 17-May-2017
                cmd.Parameters["NonCountable"].Value = integerDS.DataIntegers[9].IntegerValue;
                cmd.Parameters["ExCountry"].Value = integerDS.DataIntegers[10].IntegerValue;

                //cmd.Parameters["ActivitiesID_T"].Value = Convert.ToInt32(LeaveActivities.Taken);
                //cmd.Parameters["ActivitiesID_W2"].Value = Convert.ToInt32(LeaveActivities.Weekend);
                //cmd.Parameters["ActivitiesID_H2"].Value = Convert.ToInt32(LeaveActivities.Holiday);
                #endregion
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveRequests.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_REQUEST_SELF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getLeaveRequest_Self_EmpCode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            // [WALI :: 17-May-2017]
            // Note : This scenario occurs when 'admin' user or 'admin' role user searches for leave reqs of an employee.
                   // Then if Admin Recommends these request, it need to be forwarded to respective request's next step.
                   // For this reason, need to get Next_Supervisor for each request.
                   // As Get_Supervisor function is RECURSIVE, this query might be slower.
                   // In query, No check for approval authority in POLICY. For admin this is always TRUE.

            if (dateDS.DataDates.Rows.Count > 0)
            {
                #region Select DB Command for GetLR_Self_EmpCode...
                cmd = DBCommandProvider.GetDBCommand("GetLR_Self_EmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["ToDate"].Value = dateDS.DataDates[1].DateValue;

                cmd.Parameters["ActivitiesID_A"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["ActivitiesID_R"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["ActivitiesID_NR"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ActivitiesID_RJ"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ActivitiesID_C"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["ActivitiesID_PA"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["ActivitiesID_W"].Value = integerDS.DataIntegers[6].IntegerValue;
                cmd.Parameters["ActivitiesID_H"].Value = integerDS.DataIntegers[7].IntegerValue;
                cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["NonCountable"].Value = integerDS.DataIntegers[8].IntegerValue;
                cmd.Parameters["ExCountry"].Value = integerDS.DataIntegers[9].IntegerValue;

                //cmd.Parameters["ActivitiesID_T"].Value = Convert.ToInt32(LeaveActivities.Taken);
                //cmd.Parameters["ActivitiesID_W2"].Value = Convert.ToInt32(LeaveActivities.Weekend);
                //cmd.Parameters["ActivitiesID_H2"].Value = Convert.ToInt32(LeaveActivities.Holiday);
                #endregion
            }
            else
            {
                #region Select DB Command for GetLR_Self_EmpCode_withoutDate...
                cmd = DBCommandProvider.GetDBCommand("GetLR_Self_EmpCode_withoutDate");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["ActivitiesID_A"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["ActivitiesID_R"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["ActivitiesID_NR"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ActivitiesID_RJ"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ActivitiesID_C"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["ActivitiesID_PA"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["ActivitiesID_W"].Value = integerDS.DataIntegers[6].IntegerValue;
                cmd.Parameters["ActivitiesID_H"].Value = integerDS.DataIntegers[7].IntegerValue;
                cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["NonCountable"].Value = integerDS.DataIntegers[8].IntegerValue;
                cmd.Parameters["ExCountry"].Value = integerDS.DataIntegers[9].IntegerValue;

                //cmd.Parameters["ActivitiesID_T"].Value = Convert.ToInt32(LeaveActivities.Taken);
                //cmd.Parameters["ActivitiesID_W2"].Value = Convert.ToInt32(LeaveActivities.Weekend);
                //cmd.Parameters["ActivitiesID_H2"].Value = Convert.ToInt32(LeaveActivities.Holiday);
                #endregion
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveRequests.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_REQUEST_SELF_EMPCODE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getLeaveRequest_Self_Subordinate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            // Get List of employees who have delegated this Login User
            int loginEmpID = integerDS.DataIntegers[0].IntegerValue;
            string empWithDelegate = _getEmpCodeWithDelegates(loginEmpID, connDS);

            OleDbCommand cmd = new OleDbCommand();
            if (dateDS.DataDates.Rows.Count > 0)
            {
                #region Select DB Command for GetLR_Self_Subordinate...
                cmd = DBCommandProvider.GetDBCommand("GetLR_Self_Subordinate");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeWithDelegate"].Value = empWithDelegate;   // WALI :: 17-May-2017
                cmd.Parameters["EmployeeID"].Value = loginEmpID;
                cmd.Parameters["UserID"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["ToDate"].Value = dateDS.DataDates[1].DateValue;

                cmd.Parameters["ActivitiesID_A"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ActivitiesID_R"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ActivitiesID_NR"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["ActivitiesID_RJ"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["ActivitiesID_C"].Value = integerDS.DataIntegers[6].IntegerValue;
                cmd.Parameters["ActivitiesID_PA"].Value = integerDS.DataIntegers[7].IntegerValue;
                cmd.Parameters["ActivitiesID_W"].Value = integerDS.DataIntegers[8].IntegerValue;
                cmd.Parameters["ActivitiesID_H"].Value = integerDS.DataIntegers[9].IntegerValue;
                cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["LoginEmployeeID"].Value = loginEmpID;  // WALI :: 17-May-2017
                cmd.Parameters["NonCountable"].Value = integerDS.DataIntegers[10].IntegerValue;
                cmd.Parameters["ExCountry"].Value = integerDS.DataIntegers[11].IntegerValue;

                //cmd.Parameters["ActivitiesID_T"].Value = Convert.ToInt32(LeaveActivities.Taken);
                //cmd.Parameters["ActivitiesID_W2"].Value = Convert.ToInt32(LeaveActivities.Weekend);
                //cmd.Parameters["ActivitiesID_H2"].Value = Convert.ToInt32(LeaveActivities.Holiday);
                #endregion
            }
            else
            {
                #region Select DB Command for GetLR_Self_Subordinate...
                cmd = DBCommandProvider.GetDBCommand("GetLR_Self_Subordinate_withoutDate");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeWithDelegate"].Value = empWithDelegate;   // WALI :: 17-May-2017
                cmd.Parameters["EmployeeID"].Value = loginEmpID;
                cmd.Parameters["UserID"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["ActivitiesID_A"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ActivitiesID_R"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ActivitiesID_NR"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["ActivitiesID_RJ"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["ActivitiesID_C"].Value = integerDS.DataIntegers[6].IntegerValue;
                cmd.Parameters["ActivitiesID_PA"].Value = integerDS.DataIntegers[7].IntegerValue;
                cmd.Parameters["ActivitiesID_W"].Value = integerDS.DataIntegers[8].IntegerValue;
                cmd.Parameters["ActivitiesID_H"].Value = integerDS.DataIntegers[9].IntegerValue;
                cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["LoginEmployeeID"].Value = loginEmpID;  // WALI :: 17-May-2017
                cmd.Parameters["NonCountable"].Value = integerDS.DataIntegers[10].IntegerValue;
                cmd.Parameters["ExCountry"].Value = integerDS.DataIntegers[11].IntegerValue;

                //cmd.Parameters["ActivitiesID_T"].Value = Convert.ToInt32(LeaveActivities.Taken);
                //cmd.Parameters["ActivitiesID_W2"].Value = Convert.ToInt32(LeaveActivities.Weekend);
                //cmd.Parameters["ActivitiesID_H2"].Value = Convert.ToInt32(LeaveActivities.Holiday);
                #endregion
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveRequests.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_REQUEST_SELF_SUBORDINATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getLR_Self_Subordinate_EmpCode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            // Get List of employees who have delegated this Login User
            int loginEmpID = integerDS.DataIntegers[0].IntegerValue;
            string empWithDelegate = _getEmpCodeWithDelegates(loginEmpID, connDS);

            OleDbCommand cmd = new OleDbCommand();
            if (dateDS.DataDates.Rows.Count > 0)
            {
                #region Select DB Command for GetLR_Self_Subordinate_EmpCode...
                cmd = DBCommandProvider.GetDBCommand("GetLR_Self_Subordinate_EmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeWithDelegate"].Value = empWithDelegate;   // WALI :: 17-May-2017
                cmd.Parameters["EmployeeCode"].Value = //cmd.Parameters["EmployeeCode2"].Value = 
                    stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeID"].Value = loginEmpID;
                cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["ToDate"].Value = dateDS.DataDates[1].DateValue;

                cmd.Parameters["ActivitiesID_A"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ActivitiesID_R"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ActivitiesID_NR"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["ActivitiesID_RJ"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["ActivitiesID_C"].Value = integerDS.DataIntegers[6].IntegerValue;
                cmd.Parameters["ActivitiesID_PA"].Value = integerDS.DataIntegers[7].IntegerValue;
                cmd.Parameters["ActivitiesID_W"].Value = integerDS.DataIntegers[8].IntegerValue;
                cmd.Parameters["ActivitiesID_H"].Value = integerDS.DataIntegers[9].IntegerValue;
                cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["LoginEmployeeID"].Value = loginEmpID;  // WALI :: 17-May-2017
                cmd.Parameters["NonCountable"].Value = integerDS.DataIntegers[10].IntegerValue;
                cmd.Parameters["ExCountry"].Value = integerDS.DataIntegers[11].IntegerValue;

                //cmd.Parameters["ActivitiesID_T"].Value = Convert.ToInt32(LeaveActivities.Taken);
                //cmd.Parameters["ActivitiesID_W2"].Value = Convert.ToInt32(LeaveActivities.Weekend);
                //cmd.Parameters["ActivitiesID_H2"].Value = Convert.ToInt32(LeaveActivities.Holiday);
                #endregion
            }
            else
            {
                #region Select DB Command for GetLR_Self_Subordinate_EmpCode_withoutDate...
                cmd = DBCommandProvider.GetDBCommand("GetLR_Self_Subordinate_EmpCode_withoutDate");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeWithDelegate"].Value = empWithDelegate;   // WALI :: 17-May-2017
                cmd.Parameters["EmployeeCode"].Value = //cmd.Parameters["EmployeeCode2"].Value = 
                    stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeID"].Value = loginEmpID;
                cmd.Parameters["ActivitiesID_A"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ActivitiesID_R"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ActivitiesID_NR"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["ActivitiesID_RJ"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["ActivitiesID_C"].Value = integerDS.DataIntegers[6].IntegerValue;
                cmd.Parameters["ActivitiesID_PA"].Value = integerDS.DataIntegers[7].IntegerValue;
                cmd.Parameters["ActivitiesID_W"].Value = integerDS.DataIntegers[8].IntegerValue;
                cmd.Parameters["ActivitiesID_H"].Value = integerDS.DataIntegers[9].IntegerValue;
                cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["LoginEmployeeID"].Value = loginEmpID;  // WALI :: 17-May-2017
                cmd.Parameters["NonCountable"].Value = integerDS.DataIntegers[10].IntegerValue;
                cmd.Parameters["ExCountry"].Value = integerDS.DataIntegers[11].IntegerValue;

                //cmd.Parameters["ActivitiesID_T"].Value = Convert.ToInt32(LeaveActivities.Taken);
                //cmd.Parameters["ActivitiesID_W2"].Value = Convert.ToInt32(LeaveActivities.Weekend);
                //cmd.Parameters["ActivitiesID_H2"].Value = Convert.ToInt32(LeaveActivities.Holiday);
                #endregion
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveRequests.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LR_SELF_SUBORDINATE_EMPCODE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveListByEmpID_CFYear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataIntegerDS IntegerDS = new DataIntegerDS();
            IntegerDS.Merge(inputDS.Tables[IntegerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            IntegerDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveListByEmpID_CFYear");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["EmployeeID"].Value = IntegerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_LIST_BY_EMPID_CFYEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getLeaveListByEmpID_CFYear_PrevYear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataIntegerDS IntegerDS = new DataIntegerDS();
            IntegerDS.Merge(inputDS.Tables[IntegerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            IntegerDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveListByEmpID_CFYear_PrevYear");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["EmployeeID"].Value = IntegerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_LIST_BY_EMPID_CFYEAR_PREVYEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getLeaveListByRequestID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLeaveListByRequestID");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["ActivitiesID_A"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["LeaveRequestID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["LeaveRequestID1"].Value = integerDS.DataIntegers[0].IntegerValue; //Jarif, 21.Jul.2014

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_LIST_BY_LRID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getEmployeeForLPS(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetEmployeeForLPS");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["LeaveTypeID2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["SiteID_1"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["DepartmentID_1"].Value = integerDS.DataIntegers[2].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveRequests.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_FOR_LPS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeavePlanRuleAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLeavePlanRuleList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeavePlanRule.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LPR_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        #region WALI :: Leave Plan Rule
        private DataSet _createLeavePlanRule(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVE_PLAN_RULE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            leaveDS.Merge(inputDS.Tables[leaveDS.LeavePlanRule.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveDS.LeavePlanRuleRow row in leaveDS.LeavePlanRule.Rows)
            {
                // LeavePlanRuleId is inserted by trigger.
                cmd.Parameters.AddWithValue("p_LeaveTenureFrom", row.LeaveTenureFrom);
                cmd.Parameters.AddWithValue("p_LeaveTenureTo", row.LeaveTenureTo);
                cmd.Parameters.AddWithValue("p_EffectedDate", row.EffectedDate);
                cmd.Parameters.AddWithValue("p_LastSubmissionDate", row.LastSubmissionDate);
                cmd.Parameters.AddWithValue("p_DependOn", row.DependOn);
                cmd.Parameters.AddWithValue("P_LeaveTypeID", row.LeaveTypeID);           // Update :: WALI :: 03-Jun-2014
                cmd.Parameters.AddWithValue("P_MarginDate", row.MarginDate);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LEAVE_PLAN_RULE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.LeaveTenureFrom.ToString());
                else messageDS.ErrorMsg.AddErrorMsgRow(row.LeaveTenureFrom.ToString());
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(leaveDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateLeavePlanRule(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVE_PLAN_RULE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            leaveDS.Merge(inputDS.Tables[leaveDS.LeavePlanRule.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveDS.LeavePlanRuleRow row in leaveDS.LeavePlanRule.Rows)
            {
                cmd.Parameters.AddWithValue("p_LeaveTenureTo", row.LeaveTenureTo);
                cmd.Parameters.AddWithValue("p_EffectedDate", row.EffectedDate);
                cmd.Parameters.AddWithValue("p_LastSubmissionDate", row.LastSubmissionDate);
                cmd.Parameters.AddWithValue("p_DependOn", row.DependOn);
                cmd.Parameters.AddWithValue("p_LeaveTenureFrom", row.LeaveTenureFrom);
                cmd.Parameters.AddWithValue("p_LeaveTypeID", row.LeaveTypeID);           // Update :: WALI :: 03-Jun-2014
                cmd.Parameters.AddWithValue("P_MarginDate", row.MarginDate);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LEAVE_PLAN_RULE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.LeaveTenureFrom.ToString());
                else messageDS.ErrorMsg.AddErrorMsgRow(row.LeaveTenureFrom.ToString());
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(leaveDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteLeavePlanRule(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVE_PLAN_RULE_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            leaveDS.Merge(inputDS.Tables[leaveDS.LeavePlanRule.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveDS.LeavePlanRuleRow row in leaveDS.LeavePlanRule.Rows)
            {
                cmd.Parameters.AddWithValue("p_LeaveTenureFrom", row.LeaveTenureFrom);
                cmd.Parameters.AddWithValue("p_LeaveTypeID", row.LeaveTypeID);              // Update :: WALI :: 03-Jun-2014

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LEAVE_PLAN_RULE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.LeaveTenureFrom.ToString());
                else messageDS.ErrorMsg.AddErrorMsgRow(row.LeaveTenureFrom.ToString());
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(leaveDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createLeavePlanParking(DataSet inputDS)
        {
            #region Local variable declaraation.........
            int activityID = 0;
            string messageBody = "", returnMessage = "";
            int senderUserID = 0, recipientUserID = 0;
            string senderEmpName = "", senderEmpCode = "";
            string fromEmailAddress = "", fromEmailPassword = "", emailAddress = "";
            string mailingSubject = "", ccEmailAddress = "", leaveType = "";
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            bool sendMail = false;
            bool bError = false;
            int nRowAffected = -1;
            #endregion

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            string subjectForLFA = UtilDL.GetMailingSubjectForLFA();
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVEPLANPARKING_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            leaveDS.Merge(inputDS.Tables[leaveDS.LeaveRequests.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Update :: WALI :: Mail to Dept. Head :: 17-Feb-2014
            DataView viewDistinctMail = new DataView(leaveDS.LeaveRequests);
            DataTable tblDistinctMail = new DataTable();
            #endregion

            foreach (lmsLeaveDS.LeaveRequestsRow rowMail in leaveDS.LeaveRequests.Rows)
            {
                if (rowMail.IsNotificationByMail == true)
                {
                    activityID = rowMail.ActivitiesID;  // Get activityID to check whether Leave is APPROVED or RECOMENDED.

                    #region Common Mail Informations ....
                    sendMail = true;
                    senderUserID = rowMail.SenderUserID;
                    senderEmpName = rowMail.SenderEmpName;
                    senderEmpCode = rowMail.SenderEmpCode;
                    fromEmailAddress = rowMail.FromEmailAddress;
                    fromEmailPassword = rowMail.EmailPassword;
                    recipientUserID = rowMail.ReceivedUserID;
                    emailAddress = rowMail.EmailAddress;
                    leaveType = rowMail.MailingSubject;
                    mailingSubject = "Notification to check " + rowMail.MailingSubject + " plan";
                    #endregion

                    tblDistinctMail = viewDistinctMail.ToTable(true, "SenderEmailAddress");     // Gets distinct mail address from LeaveRequest table
                    break;
                }
                else break;
            }

            if (sendMail == true)
            {
                #region Save data after sending MAIL

                Mail.Mail mail = new Mail.Mail();

                #region Send Email to Primary LeavePlanHolder

                if (activityID == Convert.ToInt32(LeaveActivities.Recommend) || activityID == Convert.ToInt32(LeaveActivities.PendingApproval))
                {
                    messageBody = "<b>You are requested to process " + leaveType + " plan." + "<br>";
                    messageBody += "Please check the system for details." + "<br>";
                }
                else if (activityID == Convert.ToInt32(LeaveActivities.Approve))
                {
                    messageBody = "<b>You have a notification for APPROVED LEAVE [" + leaveType + "]." + "<br>";
                    messageBody += "Please check the system for details." + "<br>";
                }
                messageBody += "<b>Notified by : </b>" + senderEmpName + " [" + senderEmpCode + "]" + "<br><br>";
                messageBody += "<br><br><br>";
                returnMessage = "";

                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, senderEmpName);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, senderEmpName);
                }
                #endregion

                #region Send Email to Dept.Head

                if (returnMessage == "Email successfully sent." && activityID == Convert.ToInt32(LeaveActivities.Approve))
                {
                    foreach (DataRow rowMail in tblDistinctMail.Rows)
                    {
                        emailAddress = rowMail["SenderEmailAddress"].ToString().Trim();

                        if (emailAddress != "")
                        {
                            messageBody = "<b>You have a notification for APPROVED LEAVE [" + leaveType + "]." + "<br>";
                            messageBody += "Please check the system for details." + "<br>";
                            messageBody += "<b>Notified by : </b>" + senderEmpName + " [" + senderEmpCode + "]" + "<br><br>";
                            messageBody += "<br><br><br>";
                            returnMessage = "";

                            if (IsEMailSendWithCredential)
                            {
                                returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, senderEmpName);
                            }
                            else
                            {
                                returnMessage = mail.SendEmail(host, port, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, senderEmpName);
                            }
                        }
                    }
                }
                #endregion

                if (returnMessage == "Email successfully sent.")
                {
                    #region Save Data to DB
                    foreach (lmsLeaveDS.LeaveRequestsRow row in leaveDS.LeaveRequests.Rows)
                    {
                        // LEAVEPLANPARKINGID is inserted by trigger.
                        cmd.Parameters.AddWithValue("P_leaveplanruleid", row.LeavePlanRuleID);
                        cmd.Parameters.AddWithValue("P_employeeid", UtilDL.GetEmployeeId(connDS, row.EmployeeCode));
                        cmd.Parameters.AddWithValue("P_leavefrom", row.LeaveTenureFrom);
                        cmd.Parameters.AddWithValue("P_leaveto", row.LeaveTenureTo);
                        cmd.Parameters.AddWithValue("P_senderuserid", row.SenderUserID);
                        cmd.Parameters.AddWithValue("P_receiveduserid", row.ReceivedUserID);
                        cmd.Parameters.AddWithValue("P_activitiesid", row.ActivitiesID);
                        if (row.IsLeaveRequestCommentsNull() == false) cmd.Parameters.AddWithValue("P_lpcomments", row.LeaveRequestComments);
                        else cmd.Parameters.AddWithValue("P_lpcomments", " ");
                        cmd.Parameters.AddWithValue("P_leavetypeid", row.LeaveTypeID);
                        cmd.Parameters.AddWithValue("P_senderemailaddress", row.SenderEmailAddress);  // WALI :: Update :: 16-Feb-2014

                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Parameters.Clear();

                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_CREATE_LEAVE_PLAN_PARKING.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            //return errDS;
                        }
                        if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode.ToString());
                        else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode.ToString());
                    }
                    #endregion
                }

                else    // Email sending FAILED !
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CREATE_LEAVE_PLAN_PARKING.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }
                #endregion
            }
            else
            {
                #region Save data without sending MAIL
                foreach (lmsLeaveDS.LeaveRequestsRow row in leaveDS.LeaveRequests.Rows)
                {
                    // LEAVEPLANPARKINGID is inserted by trigger.
                    cmd.Parameters.AddWithValue("P_leaveplanruleid", row.LeavePlanRuleID);
                    cmd.Parameters.AddWithValue("P_employeeid", UtilDL.GetEmployeeId(connDS, row.EmployeeCode));
                    cmd.Parameters.AddWithValue("P_leavefrom", row.LeaveTenureFrom);
                    cmd.Parameters.AddWithValue("P_leaveto", row.LeaveTenureTo);
                    cmd.Parameters.AddWithValue("P_senderuserid", row.SenderUserID);
                    cmd.Parameters.AddWithValue("P_receiveduserid", row.ReceivedUserID);
                    cmd.Parameters.AddWithValue("P_activitiesid", row.ActivitiesID);
                    if (row.IsLeaveRequestCommentsNull() == false) cmd.Parameters.AddWithValue("P_lpcomments", row.LeaveRequestComments);
                    else cmd.Parameters.AddWithValue("P_lpcomments", " ");
                    cmd.Parameters.AddWithValue("P_leavetypeid", row.LeaveTypeID);
                    cmd.Parameters.AddWithValue("P_senderemailaddress", row.SenderEmailAddress);  // WALI :: Update :: 16-Feb-2014

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_CREATE_LEAVE_PLAN_PARKING.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode.ToString());
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode.ToString());
                }
                #endregion
            }

            //errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(leaveDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        private DataSet _getEmpForLPS_Self_Subordinate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetEmpForLPS_Self_Subordinate");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["LeaveTypeID2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["EmployeeID_1"].Value = integerDS.DataIntegers[1].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveRequests.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMP_FOR_LPS_INDIVIDUAL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getEmpForLPS_Approval(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            DateTime leaveDate = new DateTime(1754, 1, 1);

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName],false,MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Select DB Command...
            OleDbCommand cmd = new OleDbCommand();
            if (dateDS.DataDates[0].DateValue != leaveDate)
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeForLPS_Approval_WD");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["LeaveTypeID_1"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["SiteID_1"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["ActivitiesID"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ActivitiesID_1"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ReceivedUserID"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ReceivedUserID_1"].Value = integerDS.DataIntegers[3].IntegerValue;

                cmd.Parameters["LeaveFromDate"].Value = dateDS.DataDates[0].DateValue;

                if (dateDS.DataDates[1].DateValue == leaveDate) cmd.Parameters["LeaveToDate"].Value = dateDS.DataDates[0].DateValue;
                else cmd.Parameters["LeaveToDate"].Value = dateDS.DataDates[1].DateValue;
                cmd.Parameters["LiabilityID"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["LiabilityID_1"].Value = integerDS.DataIntegers[4].IntegerValue;

                cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["DepartmentID_1"].Value = integerDS.DataIntegers[5].IntegerValue;

                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeForLPS_Approval");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["LeaveTypeID_1"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["SiteID_1"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["ActivitiesID"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ActivitiesID_1"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["ReceivedUserID"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ReceivedUserID_1"].Value = integerDS.DataIntegers[3].IntegerValue;

                cmd.Parameters["LiabilityID"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["LiabilityID_1"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["DepartmentID_1"].Value = integerDS.DataIntegers[5].IntegerValue;

                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            }
            #endregion

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveRequests.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMP_FOR_LPS_APPROVAL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createLAHC(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError = false;
            int nRowAffected = -1;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LAHC_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Del = new OleDbCommand();
            cmd_Del.CommandText = "PRO_LAHC_DELETE";
            cmd_Del.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmd_Del == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            lmsLeaveDS leaveDS = new lmsLeaveDS();
            leaveDS.Merge(inputDS.Tables[leaveDS.LeaveAdjustmentHead.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region For Deletion...
            foreach (lmsLeaveDS.LeaveAdjustmentHeadRow row in leaveDS.LeaveAdjustmentHead.Rows)
            {
                cmd_Del.Parameters.AddWithValue("p_MasterLeaveID", row.MasterLeaveID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LAHC_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                break;
            }
            #endregion

            #region For Creation...

            foreach (lmsLeaveDS.LeaveAdjustmentHeadRow row in leaveDS.LeaveAdjustmentHead.Rows)
            {
                cmd.Parameters.AddWithValue("p_MasterLeaveID", row.MasterLeaveID);
                cmd.Parameters.AddWithValue("p_AdjustmentLeaveCode", row.AdjustmentLeaveCode);
                cmd.Parameters.AddWithValue("p_Max_Adjustment", row.Max_Adjustment);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LAHC_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deletLAHC(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LAHC_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_MasterLeaveID", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LAHC_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getEmployeeForEncashmentVerification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DataSet returnDS = new DataSet();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DataBoolDS boolDS = new DataBoolDS();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            long designationID = UtilDL.GetDesignationId(connDS, stringDS.DataStrings[3].StringValue);

            if (!boolDS.DataBools[1].BoolValue)      // LeaveType doesn't depend on LeavePlan
            {
                #region Unique Selection yr false
                if (stringDS.DataStrings[4].StringValue == "month")
                {
                    if (stringDS.DataStrings[5].StringValue == "EC")
                    {
                        // ok
                        if(stringDS.DataStrings[9].StringValue == "1") cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerification_EC");
                        else cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerification_EC_OrderByID");
                        if (cmd == null)
                        {
                            return UtilDL.GetCommandNotFound();
                        }

                        cmd.Parameters["EncashmentFor"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_1"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["BasicPercent"].Value = integerDS.DataIntegers[3].IntegerValue;
                        cmd.Parameters["FlatAmount"].Value = integerDS.DataIntegers[4].IntegerValue;

                        cmd.Parameters["HouseRent"].Value = Convert.ToDecimal(stringDS.DataStrings[6].StringValue);
                        cmd.Parameters["Medical"].Value = Convert.ToDecimal(stringDS.DataStrings[7].StringValue);
                        cmd.Parameters["Conveyance"].Value = Convert.ToDecimal(stringDS.DataStrings[8].StringValue);

                        cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["SalaryMonth_1"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                        cmd.Parameters["EncashmentFor_2"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["Balance"].Value = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                        cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DepartmentID2"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DesignationID"].Value = designationID;
                        cmd.Parameters["DesignationID2"].Value = designationID;
                        cmd.Parameters["EncashmentFor_4"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                    }
                    else
                    {
                        // ok
                        if (stringDS.DataStrings[9].StringValue == "1") cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerification_C");
                        else cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerification_C_ByID");
                        if (cmd == null)
                        {
                            return UtilDL.GetCommandNotFound();
                        }

                        cmd.Parameters["EncashmentFor"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_1"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["BasicPercent"].Value = integerDS.DataIntegers[3].IntegerValue;
                        cmd.Parameters["FlatAmount"].Value = integerDS.DataIntegers[4].IntegerValue;

                        cmd.Parameters["HouseRent"].Value = Convert.ToDecimal(stringDS.DataStrings[6].StringValue);
                        cmd.Parameters["Medical"].Value = Convert.ToDecimal(stringDS.DataStrings[7].StringValue);
                        cmd.Parameters["Conveyance"].Value = Convert.ToDecimal(stringDS.DataStrings[8].StringValue);

                        cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["SalaryMonth_1"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                        cmd.Parameters["EncashmentFor_2"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["Balance"].Value = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                        cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DepartmentID2"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DesignationID"].Value = designationID;
                        cmd.Parameters["DesignationID2"].Value = designationID;
                        cmd.Parameters["EncashmentFor_4"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                    }

                }
                else if (stringDS.DataStrings[4].StringValue == "day")
                {
                    if (stringDS.DataStrings[5].StringValue == "EC")
                    {
                        // ok
                        if (stringDS.DataStrings[9].StringValue == "1") cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationDayWise_EC");
                        else cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationDayWise_EC_ById");
                        if (cmd == null)
                        {
                            return UtilDL.GetCommandNotFound();
                        }

                        cmd.Parameters["EncashmentFor"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_1"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_2"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["FlatAmount"].Value = integerDS.DataIntegers[4].IntegerValue;
                        cmd.Parameters["BasicPercent"].Value = integerDS.DataIntegers[3].IntegerValue;


                        cmd.Parameters["HouseRent"].Value = Convert.ToDecimal(stringDS.DataStrings[6].StringValue);
                        cmd.Parameters["Medical"].Value = Convert.ToDecimal(stringDS.DataStrings[7].StringValue);
                        cmd.Parameters["Conveyance"].Value = Convert.ToDecimal(stringDS.DataStrings[8].StringValue);

                        cmd.Parameters["SalaryYear"].Value = integerDS.DataIntegers[5].IntegerValue;
                        cmd.Parameters["dayOfMonth"].Value = integerDS.DataIntegers[6].IntegerValue;
                        cmd.Parameters["MonthNo"].Value = integerDS.DataIntegers[7].IntegerValue;


                        cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["SalaryMonth_1"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                        cmd.Parameters["EncashmentFor_3"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["Balance"].Value = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                        cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DepartmentID2"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DesignationID"].Value = designationID;
                        cmd.Parameters["DesignationID2"].Value = designationID;
                        cmd.Parameters["EncashmentFor_4"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);


                    }
                    else
                    {
                        if (stringDS.DataStrings[9].StringValue == "1") cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationDayWise_C");
                        else cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationDayWise_C_ById");
                        if (cmd == null)
                        {
                            return UtilDL.GetCommandNotFound();
                        }

                        cmd.Parameters["EncashmentFor"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_1"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_2"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["FlatAmount"].Value = integerDS.DataIntegers[4].IntegerValue;
                        cmd.Parameters["BasicPercent"].Value = integerDS.DataIntegers[3].IntegerValue;

                        cmd.Parameters["HouseRent"].Value = Convert.ToDecimal(stringDS.DataStrings[6].StringValue);
                        cmd.Parameters["Medical"].Value = Convert.ToDecimal(stringDS.DataStrings[7].StringValue);
                        cmd.Parameters["Conveyance"].Value = Convert.ToDecimal(stringDS.DataStrings[8].StringValue);

                        cmd.Parameters["SalaryYear"].Value = integerDS.DataIntegers[5].IntegerValue;
                        cmd.Parameters["dayOfMonth"].Value = integerDS.DataIntegers[6].IntegerValue;
                        cmd.Parameters["MonthNo"].Value = integerDS.DataIntegers[7].IntegerValue;

                        cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["SalaryMonth_1"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                        cmd.Parameters["EncashmentFor_3"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["Balance"].Value = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                        cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DepartmentID2"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DesignationID"].Value = designationID;
                        cmd.Parameters["DesignationID2"].Value = designationID;
                        cmd.Parameters["EncashmentFor_4"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                    }
                }



                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.EncashmentVerification.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_FOR_ENCASHMENT_VERIFICATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                #endregion
            }
            else
            {
                #region Unique Selection yr True
                if (stringDS.DataStrings[4].StringValue == "month")
                {
                    if (stringDS.DataStrings[5].StringValue == "EC")
                    {
                        if (stringDS.DataStrings[9].StringValue == "1") cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationForUnique_EC");
                        else cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationForUnique_EC_ById");
                        if (cmd == null)
                        {
                            return UtilDL.GetCommandNotFound();
                        }

                        cmd.Parameters["EncashmentFor"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_1"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["BasicPercent"].Value = integerDS.DataIntegers[3].IntegerValue;
                        cmd.Parameters["FlatAmount"].Value = integerDS.DataIntegers[4].IntegerValue;

                        cmd.Parameters["HouseRent"].Value = Convert.ToDecimal(stringDS.DataStrings[6].StringValue);
                        cmd.Parameters["Medical"].Value = Convert.ToDecimal(stringDS.DataStrings[7].StringValue);
                        cmd.Parameters["Conveyance"].Value = Convert.ToDecimal(stringDS.DataStrings[8].StringValue);

                        cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["SalaryMonth_1"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                        cmd.Parameters["EncashmentFor_2"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["Balance"].Value = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                        cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DepartmentID2"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DesignationID"].Value = designationID;
                        cmd.Parameters["DesignationID2"].Value = designationID;
                        cmd.Parameters["SalaryYear"].Value = dateDS.DataDates[0].DateValue.Year;
                        cmd.Parameters["EncashmentFor_4"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);

                        OleDbDataAdapter adapter = new OleDbDataAdapter();
                        adapter.SelectCommand = cmd;

                        bool bError = false;
                        int nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.EncashmentVerification.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_FOR_ENCASHMENT_VERIFICATION.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                    else
                    {
                        if (stringDS.DataStrings[9].StringValue == "1") cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationForUnique_C");
                        else cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationForUnique_C_ById");
                        if (cmd == null)
                        {
                            return UtilDL.GetCommandNotFound();
                        }

                        cmd.Parameters["EncashmentFor"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_1"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["BasicPercent"].Value = integerDS.DataIntegers[3].IntegerValue;
                        cmd.Parameters["FlatAmount"].Value = integerDS.DataIntegers[4].IntegerValue;

                        cmd.Parameters["HouseRent"].Value = Convert.ToDecimal(stringDS.DataStrings[6].StringValue);
                        cmd.Parameters["Medical"].Value = Convert.ToDecimal(stringDS.DataStrings[7].StringValue);
                        cmd.Parameters["Conveyance"].Value = Convert.ToDecimal(stringDS.DataStrings[8].StringValue);

                        cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["SalaryMonth_1"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                        cmd.Parameters["EncashmentFor_2"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["Balance"].Value = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                        cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DepartmentID2"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DesignationID"].Value = designationID;
                        cmd.Parameters["DesignationID2"].Value = designationID;
                        cmd.Parameters["SalaryYear"].Value = dateDS.DataDates[0].DateValue.Year;
                        cmd.Parameters["EncashmentFor_4"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);

                        OleDbDataAdapter adapter = new OleDbDataAdapter();
                        adapter.SelectCommand = cmd;

                        bool bError = false;
                        int nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.EncashmentVerification.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_FOR_ENCASHMENT_VERIFICATION.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                }
                else if (stringDS.DataStrings[4].StringValue == "day")
                {
                    if (stringDS.DataStrings[5].StringValue == "EC")
                    {
                        if (stringDS.DataStrings[9].StringValue == "1") cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationForUniqueDayWise_EC");
                        else cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationForUniqueDayWise_EC_ById");
                        if (cmd == null)
                        {
                            return UtilDL.GetCommandNotFound();
                        }

                        cmd.Parameters["EncashmentFor"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_1"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_2"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["FlatAmount"].Value = integerDS.DataIntegers[4].IntegerValue;
                        cmd.Parameters["BasicPercent"].Value = integerDS.DataIntegers[3].IntegerValue;

                        cmd.Parameters["HouseRent"].Value = Convert.ToDecimal(stringDS.DataStrings[6].StringValue);
                        cmd.Parameters["Medical"].Value = Convert.ToDecimal(stringDS.DataStrings[7].StringValue);
                        cmd.Parameters["Conveyance"].Value = Convert.ToDecimal(stringDS.DataStrings[8].StringValue);

                        cmd.Parameters["SalaryYear"].Value = integerDS.DataIntegers[5].IntegerValue;
                        cmd.Parameters["dayOfMonth"].Value = integerDS.DataIntegers[6].IntegerValue;
                        cmd.Parameters["MonthNo"].Value = integerDS.DataIntegers[7].IntegerValue;

                        cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["SalaryMonth_1"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                        cmd.Parameters["EncashmentFor_3"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["Balance"].Value = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                        cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DepartmentID2"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DesignationID"].Value = designationID;
                        cmd.Parameters["DesignationID2"].Value = designationID;
                        cmd.Parameters["SalaryYear2"].Value = dateDS.DataDates[0].DateValue.Year;
                        cmd.Parameters["EncashmentFor_4"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);

                        OleDbDataAdapter adapter = new OleDbDataAdapter();
                        adapter.SelectCommand = cmd;

                        bool bError = false;
                        int nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.EncashmentVerification.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_FOR_ENCASHMENT_VERIFICATION.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                    else
                    {
                        if (stringDS.DataStrings[9].StringValue == "1")  cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationForUniqueDayWise_C");
                        else cmd = DBCommandProvider.GetDBCommand("GetEmpForEncashVerificationForUniqueDayWise_C_ById");
                        if (cmd == null)
                        {
                            return UtilDL.GetCommandNotFound();
                        }

                        cmd.Parameters["EncashmentFor"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_1"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["EncashmentFor_2"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["FlatAmount"].Value = integerDS.DataIntegers[4].IntegerValue;
                        cmd.Parameters["BasicPercent"].Value = integerDS.DataIntegers[3].IntegerValue;

                        cmd.Parameters["HouseRent"].Value = Convert.ToDecimal(stringDS.DataStrings[6].StringValue);
                        cmd.Parameters["Medical"].Value = Convert.ToDecimal(stringDS.DataStrings[7].StringValue);
                        cmd.Parameters["Conveyance"].Value = Convert.ToDecimal(stringDS.DataStrings[8].StringValue);

                        cmd.Parameters["SalaryYear"].Value = integerDS.DataIntegers[5].IntegerValue;
                        cmd.Parameters["dayOfMonth"].Value = integerDS.DataIntegers[6].IntegerValue;
                        cmd.Parameters["MonthNo"].Value = integerDS.DataIntegers[7].IntegerValue;

                        cmd.Parameters["SalaryMonth"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["SalaryMonth_1"].Value = dateDS.DataDates[0].DateValue;
                        cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
                        cmd.Parameters["EncashmentFor_3"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);
                        cmd.Parameters["Balance"].Value = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                        cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;
                        cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[1].IntegerValue;
                        cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DepartmentID2"].Value = integerDS.DataIntegers[2].IntegerValue;
                        cmd.Parameters["DesignationID"].Value = designationID;
                        cmd.Parameters["DesignationID2"].Value = designationID;
                        cmd.Parameters["SalaryYear2"].Value = dateDS.DataDates[0].DateValue.Year;
                        cmd.Parameters["EncashmentFor_4"].Value = Convert.ToDecimal(stringDS.DataStrings[0].StringValue);

                        OleDbDataAdapter adapter = new OleDbDataAdapter();
                        adapter.SelectCommand = cmd;

                        bool bError = false;
                        int nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.EncashmentVerification.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_FOR_ENCASHMENT_VERIFICATION.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                }
                #endregion
            }


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _createEncashmentVerification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_ENCASH_VERIFY_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            leaveDS.Merge(inputDS.Tables[leaveDS.EncashmentVerification.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveDS.EncashmentVerificationRow row in leaveDS.EncashmentVerification.Rows)
            {
                // SerialNo is inserted by TRIGGER

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("P_EncashVerificationID", genPK);
                cmd.Parameters.AddWithValue("P_EmployeeID", row.EmployeeID);
                cmd.Parameters.AddWithValue("P_LeaveTypeID", row.LeaveTypeID);
                cmd.Parameters.AddWithValue("P_EncashDays", row.EncashDays);
                cmd.Parameters.AddWithValue("P_ELQID", row.ELQID);
                cmd.Parameters.AddWithValue("P_BasicPercent", row.BasicPercent);
                cmd.Parameters.AddWithValue("P_FlatAmount", row.FlatAmount);
                cmd.Parameters.AddWithValue("P_EncashmentYear", row.EncashmentYear);
                cmd.Parameters.AddWithValue("P_PayableAmount", row.PayableAmount);
                cmd.Parameters.AddWithValue("P_PaymentMode", row.PaymentMode);
                cmd.Parameters.AddWithValue("P_Taxable", row.Taxable);
                cmd.Parameters.AddWithValue("P_PayableBasic", row.PayableBasic);
                cmd.Parameters.AddWithValue("P_Balance", row.Balance);
                cmd.Parameters.AddWithValue("P_Status", row.Status);
                cmd.Parameters.AddWithValue("P_SenderUser", row.SenderUser);
                cmd.Parameters.AddWithValue("P_VerifierUserID", row.VerifierUserID);
                cmd.Parameters.AddWithValue("P_LeaveActivityID", row.LeaveActivityID);
                cmd.Parameters.AddWithValue("P_LeaveSource", row.LeaveSource);
                if (!row.IsActualPayMonthYearNull()) cmd.Parameters.AddWithValue("P_ActualPayMonthYear", row.ActualPayMonthYear);
                else cmd.Parameters.AddWithValue("P_ActualPayMonthYear", DBNull.Value);

                cmd.Parameters.AddWithValue("P_HouseRent_P", row.HouseRent_P);
                cmd.Parameters.AddWithValue("P_HouseRent_F", row.HouseRent_F);
                cmd.Parameters.AddWithValue("P_Medical_P", row.Medical_P);
                cmd.Parameters.AddWithValue("P_Medical_F", row.Medical_F);
                cmd.Parameters.AddWithValue("P_Conveyance_P", row.Conveyance_P);
                cmd.Parameters.AddWithValue("P_Conveyance_F", row.Conveyance_F);


                bool bError2 = false;
                int nRowAffected2 = -1;
                nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError2);
                cmd.Parameters.Clear();

                if (bError2)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_ENCASHMENT_VERIFICATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected2 > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode);
            }

            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(leaveDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getPendingLeaveEncashmentData(DataSet inputDS)
        {
            // Update :: WALI :: 03-Aug-2014 :: All records for Latest LeaveQuota are retrieved [Not only PENDING]

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataIntegerDS integerDS = new DataIntegerDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetPendingLeaveEncashmentData");
            //cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;     // Update :: WALI :: 03-Aug-2014
            cmd.Parameters["VerifierUserID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["VerifierUserID2"].Value = integerDS.DataIntegers[0].IntegerValue;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.EncashmentVerification.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_PENDING_LEAVE_ENCASHMENT_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            leaveDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _approveEncashmentVerification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_ENCASH_VERIFY_APPROVE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            leaveDS.Merge(inputDS.Tables[leaveDS.EncashmentVerification.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveDS.EncashmentVerificationRow row in leaveDS.EncashmentVerification.Rows)
            {
                cmd.Parameters.AddWithValue("p_Status", row.Status);
                cmd.Parameters.AddWithValue("P_EncashVerificationID", row.EncashVerificationID);
                if (!row.IsLeaveActivityIDNull()) cmd.Parameters.AddWithValue("P_LeaveActivityID", row.LeaveActivityID);
                else cmd.Parameters.AddWithValue("P_LeaveActivityID", DBNull.Value);

                bool bError2 = false;
                int nRowAffected2 = -1;
                nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError2);
                cmd.Parameters.Clear();

                if (bError2)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_ENCASHMENT_VERIFICATION_APPROVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected2 > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode);
            }

            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(leaveDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getLAHC_All(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            DateTime leaveDate = new DateTime(1754, 1, 1);

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Select DB Command...
            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLeaveAdjustmentHead_All");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            #endregion

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveAdjustmentHead.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LAHC_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveHistory_InDateRange(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            if (integerDS.DataIntegers[0].IntegerValue != 0 && integerDS.DataIntegers[1].IntegerValue != 0)
            {
                cmd = DBCommandProvider.GetDBCommand("GetLeaveSummaryByApprID_PendID_InDateRange");
                cmd.Parameters["pActIDapproved"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["pElqID1"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["FromDate1"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["ToDate1"].Value = dateDS.DataDates[1].DateValue;
                cmd.Parameters["pActIDpending"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
                cmd.Parameters["pActIDRecommend"].Value = Convert.ToInt32(LeaveActivities.Recommend);
                cmd.Parameters["pActIDNotRecommend"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
                cmd.Parameters["pElqID2"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["FromDate2"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["ToDate2"].Value = dateDS.DataDates[1].DateValue;
                cmd.Parameters["pCoutryID"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["pElqID3"].Value = integerDS.DataIntegers[2].IntegerValue;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetLeaveSummaryByApprID_OR_PendID_InDateRange");
                cmd.Parameters["pActIDapproved"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["pActIDpending"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
                cmd.Parameters["pActIDRecommend"].Value = Convert.ToInt32(LeaveActivities.Recommend);
                cmd.Parameters["pActIDNotRecommend"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);

                if (integerDS.DataIntegers[0].IntegerValue != 0)
                {
                    cmd.Parameters["pActivityID"].Value = integerDS.DataIntegers[0].IntegerValue;
                    cmd.Parameters["pActIDRecommend2"].Value = integerDS.DataIntegers[0].IntegerValue;
                    cmd.Parameters["pActIDNotRecommend2"].Value = integerDS.DataIntegers[0].IntegerValue;
                }
                else
                {
                    cmd.Parameters["pActivityID"].Value = integerDS.DataIntegers[1].IntegerValue;
                    cmd.Parameters["pActIDRecommend2"].Value = Convert.ToInt32(LeaveActivities.Recommend);
                    cmd.Parameters["pActIDNotRecommend2"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
                }
                cmd.Parameters["FromDate1"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["ToDate1"].Value = dateDS.DataDates[1].DateValue;
                cmd.Parameters["pCoutryID"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["pElqID"].Value = integerDS.DataIntegers[2].IntegerValue;
            }

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveHist.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_HISTORY_IN_DATE_RANGE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            leaveDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createLeave_MultipleEmployee(DataSet inputDS)
        {
            string messageBody = "", returnMessage = "";
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DataBoolDS boolDS = new DataBoolDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            string subjectForLFA = UtilDL.GetMailingSubjectForLFA();
            #endregion

            OleDbCommand cmdLeaveWithLFARequest = new OleDbCommand();
            cmdLeaveWithLFARequest.CommandText = "PRO_LMS_LEAVE_LFA_REQU_CREATE";
            cmdLeaveWithLFARequest.CommandType = CommandType.StoredProcedure;
            if (cmdLeaveWithLFARequest == null) return UtilDL.GetCommandNotFound();

            OleDbCommand cmdLeaveRequest = new OleDbCommand();
            cmdLeaveRequest.CommandText = "PRO_LMS_LEAVE_REQUEST_CREATE";
            cmdLeaveRequest.CommandType = CommandType.StoredProcedure;
            if (cmdLeaveRequest == null) return UtilDL.GetCommandNotFound();

            OleDbCommand cmdModifyLeave = new OleDbCommand();
            cmdModifyLeave.CommandText = "PRO_LMS_LEAVE_MODIFY";
            cmdModifyLeave.CommandType = CommandType.StoredProcedure;
            if (cmdModifyLeave == null) return UtilDL.GetCommandNotFound();

            //OleDbCommand cmdAdjustmentLeaveRequest = new OleDbCommand();
            //cmdAdjustmentLeaveRequest.CommandText = "PRO_LMS_LEAVE_REQUEST_CREATE";
            //cmdAdjustmentLeaveRequest.CommandType = CommandType.StoredProcedure;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            leaveDS.Merge(inputDS.Tables[leaveDS.LeaveRequests.TableName], false, MissingSchemaAction.Error);
            leaveDS.Merge(inputDS.Tables[leaveDS.Leave.TableName], false, MissingSchemaAction.Error);
            leaveDS.Merge(inputDS.Tables[leaveDS.ModifyLeave.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            #region Assign New Leave
            foreach (lmsLeaveDS.LeaveRequestsRow row in leaveDS.LeaveRequests.Rows)
            {
                #region Save Leave Request
                decimal regularDays = row.NoOfDays;
                long lfaRequestID = 0, adj_LeaveRequestID = 0;
                long leaveRequestID = IDGenerator.GetNextGenericPK();

                if (row.NoOf_Adjustment > 0)
                {
                    regularDays -= row.NoOf_Adjustment;
                    adj_LeaveRequestID = IDGenerator.GetNextGenericPK();
                }

                if (leaveRequestID == -1 || adj_LeaveRequestID == -1) return UtilDL.GetDBOperationFailed();

                #region Bind data to Procedure Parameters [For Leave / LFA Request]

                #region For Adjustment Leave Request... [Commented]
                //if (row.NoOf_Adjustment > 0)
                //{
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_ELQID", row.ELQID);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTID", adj_LeaveRequestID);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_LCDETAILSID", row.LCDetailsID);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_LEAVETYPEID", row.Adjustment_LeaveID);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                //    if (!row.IsLeaveRequestCommentsNull())
                //    {
                //        cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", row.LeaveRequestComments);
                //    }
                //    else
                //    {
                //        cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", " ");
                //    }
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_BeginDate", row.LeaveFrom);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_EndDate", row.LeaveTo);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_NoOfDays", row.NoOfDays);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_NoOf_Adjustment", row.NoOf_Adjustment);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_IsChild", true);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_Last_ActivitiesID", row.Last_ActivitiesID);
                //    cmdAdjustmentLeaveRequest.Parameters.AddWithValue("p_Adjustment_LR_ID", leaveRequestID);
                //}
                #endregion

                if (row.ApplyWithLFA)
                {
                    row.MailingSubject = subjectForLFA;

                    #region For Leave Request...
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_ELQID", row.ELQID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LEAVEREQUESTID", leaveRequestID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LCDETAILSID", row.LCDetailsID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LEAVETYPEID", row.LeaveTypeID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                    if (row.IsLeaveRequestCommentsNull() == false)
                    {
                        cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", row.LeaveRequestComments);
                    }
                    else cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", " ");

                    cmdLeaveRequest.Parameters.AddWithValue("p_BeginDate", row.LeaveFrom);
                    cmdLeaveRequest.Parameters.AddWithValue("p_EndDate", row.LeaveTo);
                    cmdLeaveRequest.Parameters.AddWithValue("p_NoOfDays", row.NoOfDays);
                    cmdLeaveRequest.Parameters.AddWithValue("p_NoOf_Adjustment", row.NoOf_Adjustment);
                    cmdLeaveRequest.Parameters.AddWithValue("p_IsChild", false);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Last_ActivitiesID", row.Last_ActivitiesID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Adjustment_LR_ID", DBNull.Value);

                    

                    #endregion


                    #region LFA...
                    lfaRequestID = IDGenerator.GetNextGenericPK();
                    if (lfaRequestID == -1) return UtilDL.GetDBOperationFailed();

                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAREQUESTID", lfaRequestID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAAMOUNT", row.LFAAmount);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAYEAR", row.AppliedDate.Year);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_PAYMONTH", row.AppliedDate.Month);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAREQUESTREMARKS", " ");

                    long lfaHistoryID = IDGenerator.GetNextGenericPK();
                    if (lfaHistoryID == -1) return UtilDL.GetDBOperationFailed();

                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAHISTORYID", lfaHistoryID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_RECEIVEDEMPID", row.SupervisorID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_ACTIVITIESID", Convert.ToInt32(LFARequestActivities.PendingApproval));
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_LFAHISTORYREMARKS", " ");
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_BeginDate", row.LeaveFrom);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_EndDate", row.LeaveTo);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_NoOfDays", row.NoOfDays);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_NoOf_Adjustment", row.NoOf_Adjustment);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_IsChild", false);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Last_ActivitiesID", row.Last_ActivitiesID);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Adjustment_LR_ID", DBNull.Value);

                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_CountryID", DBNull.Value);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DivisionID", DBNull.Value);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Destination", DBNull.Value);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DelegatedUserID", DBNull.Value);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_Delegate", DBNull.Value);
                    cmdLeaveWithLFARequest.Parameters.AddWithValue("p_DelegateEmailID", DBNull.Value);

                    #endregion
                }
                else  // Without LFA
                {
                    #region For Leave Request...
                    cmdLeaveRequest.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_ELQID", row.ELQID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTID", leaveRequestID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_LCDETAILSID", row.LCDetailsID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_LEAVETYPEID", row.LeaveTypeID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                    if (!row.IsLeaveRequestCommentsNull())
                    {
                        cmdLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", row.LeaveRequestComments);
                    }
                    else cmdLeaveRequest.Parameters.AddWithValue("p_LEAVEREQUESTCOMMENTS", " ");

                    cmdLeaveRequest.Parameters.AddWithValue("p_BeginDate", row.LeaveFrom);
                    cmdLeaveRequest.Parameters.AddWithValue("p_EndDate", row.LeaveTo);
                    cmdLeaveRequest.Parameters.AddWithValue("p_NoOfDays", row.NoOfDays);
                    cmdLeaveRequest.Parameters.AddWithValue("p_NoOf_Adjustment", row.NoOf_Adjustment);
                    cmdLeaveRequest.Parameters.AddWithValue("p_IsChild", false);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Last_ActivitiesID", row.Last_ActivitiesID);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Adjustment_LR_ID", DBNull.Value);

                    cmdLeaveRequest.Parameters.AddWithValue("p_CountryID", DBNull.Value);
                    cmdLeaveRequest.Parameters.AddWithValue("p_DivisionID", DBNull.Value);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Destination", DBNull.Value);
                    cmdLeaveRequest.Parameters.AddWithValue("p_DelegatedUserID", DBNull.Value);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Delegate", DBNull.Value);
                    cmdLeaveRequest.Parameters.AddWithValue("p_DelegateEmailID", DBNull.Value);
                    cmdLeaveRequest.Parameters.AddWithValue("p_DistrictID", DBNull.Value);
                    cmdLeaveRequest.Parameters.AddWithValue("p_Mailing_Address", DBNull.Value);
                    cmdLeaveRequest.Parameters.AddWithValue("p_IsDisciplinaryAction", "0");
                    #endregion
                }
                #endregion

                #region Save Transaction ... [Leave / LFA Request]
                bool bError = false;
                int nRowAffected = -1;

                if (row.IsNotificationByMail)
                {
                    #region Transaction after Mail
                    returnMessage = "";
                    row.Url += "?pVal=lms";

                    messageBody = "<b>Dispatcher: </b>" + row.SenderEmpName + " [" + row.SenderEmpCode + "]" + "<br><br>";
                    messageBody += "<b>Applicant: </b>" + row.EmployeeName + " [" + row.EmployeeCode + "]<br>";
                    messageBody += "<b>Leave Type: </b>" + row.LeaveTypeName + "<br>";
                    messageBody += "<b>Leave Date: </b>" + row.LeaveRange;
                    messageBody += "<br><br><br>";
                    messageBody += "Click the following link: ";
                    messageBody += "<br>";
                    messageBody += "<a href='" + row.Url + "'>" + row.Url + "</a>";

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, row.FromEmailAddress, row.EmailAddress, row.CcEmailAddress, row.MailingSubject, messageBody, row.SenderEmpName);
                    }
                    else returnMessage = mail.SendEmail(host, port, row.FromEmailAddress, row.EmailAddress, row.CcEmailAddress, row.MailingSubject, messageBody, row.SenderEmpName);

                    if (returnMessage == "Email successfully sent.")
                    {
                        if (row.ApplyWithLFA) nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveWithLFARequest, connDS.DBConnections[0].ConnectionID, ref bError);
                        else nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveRequest, connDS.DBConnections[0].ConnectionID, ref bError);

                        cmdLeaveRequest.Parameters.Clear();
                        cmdLeaveWithLFARequest.Parameters.Clear();
                    }
                    else
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD_MULTIPLE_EMPLOYEE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;

                        messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [Mail sending failed]");
                        continue;
                    }
                    #endregion
                }
                else   // Without Mail Notification
                {
                    if (row.ApplyWithLFA) nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveWithLFARequest, connDS.DBConnections[0].ConnectionID, ref bError);
                    else nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeaveRequest, connDS.DBConnections[0].ConnectionID, ref bError);

                    cmdLeaveRequest.Parameters.Clear();
                    cmdLeaveWithLFARequest.Parameters.Clear();
                }

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD_MULTIPLE_EMPLOYEE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;

                    if (row.IsNotificationByMail && returnMessage == "Email successfully sent.") messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [Mail sent, DB Operation Err]");
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                    continue;
                }

                //if (row.NoOf_Adjustment > 0)
                //{
                //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdAdjustmentLeaveRequest, connDS.DBConnections[0].ConnectionID, ref bError);
                //}
                #endregion
                #endregion

                if (nRowAffected > 0)  // When Leave Request is successful
                {
                    #region Save Leave & Leave History...
                    lmsLeaveDS leaveFiltered = new lmsLeaveDS();
                    DataRow[] foundRows = leaveDS.Leave.Select("LeaveRequestID=" + row.LeaveRequestID);
                    foreach (DataRow leaveRow in foundRows)
                    { leaveFiltered.Leave.ImportRow(leaveRow); }
                    leaveFiltered.AcceptChanges();

                    OleDbCommand cmdLeave = new OleDbCommand();
                    cmdLeave.CommandType = CommandType.StoredProcedure;

                    OleDbCommand cmdLvHisUpdate = new OleDbCommand();
                    cmdLvHisUpdate.CommandText = "PRO_LMS_LEAVE_HIS_UPDATE";
                    cmdLvHisUpdate.CommandType = CommandType.StoredProcedure;

                    foreach (lmsLeaveDS.LeaveRow leaveRow in leaveFiltered.Leave.Rows)
                    {
                        long leaveID = IDGenerator.GetNextGenericPK();

                        if (leaveID == -1) return UtilDL.GetDBOperationFailed();

                        cmdLeave.Parameters.AddWithValue("p_LEAVEID", leaveID);

                        if (!leaveRow.IsLeaveDateNull()) cmdLeave.Parameters.AddWithValue("p_LEAVEDATE", leaveRow.LeaveDate);
                        else cmdLeave.Parameters.AddWithValue("p_LEAVEDATE", DBNull.Value);

                        if (!leaveRow.IsLeaveLengthHoursNull()) cmdLeave.Parameters.AddWithValue("p_LEAVELENGTHHOURS", leaveRow.LeaveLengthHours);
                        else cmdLeave.Parameters.AddWithValue("p_LEAVELENGTHHOURS", DBNull.Value);

                        if (!leaveRow.IsLeaveLengthDaysNull()) cmdLeave.Parameters.AddWithValue("p_LEAVELENGTHDAYS", leaveRow.LeaveLengthDays);
                        else cmdLeave.Parameters.AddWithValue("p_LEAVELENGTHDAYS", DBNull.Value);

                        if (!leaveRow.IsLeaveCommentsNull()) cmdLeave.Parameters.AddWithValue("p_LEAVECOMMENTS", leaveRow.LeaveComments);
                        else cmdLeave.Parameters.AddWithValue("p_LEAVECOMMENTS", DBNull.Value);

                        if (regularDays > 0) cmdLeave.Parameters.AddWithValue("p_LEAVEREQUESTID", leaveRequestID);
                        else cmdLeave.Parameters.AddWithValue("p_LEAVEREQUESTID", adj_LeaveRequestID > 0 ? adj_LeaveRequestID : leaveRequestID);

                        if (!leaveRow.IsStartTimeNull()) cmdLeave.Parameters.AddWithValue("p_STARTTIME", leaveRow.StartTime);
                        else cmdLeave.Parameters.AddWithValue("p_STARTTIME", DBNull.Value);

                        if (!leaveRow.IsEndTimeNull()) cmdLeave.Parameters.AddWithValue("p_ENDTIME", leaveRow.EndTime);
                        else cmdLeave.Parameters.AddWithValue("p_ENDTIME", DBNull.Value);

                        cmdLeave.Parameters.AddWithValue("p_LEAVEHISTORYID", 0);

                        if (!leaveRow.IsEmployeeIDNull()) cmdLeave.Parameters.AddWithValue("p_EMPLOYEEID", leaveRow.EmployeeID);
                        else cmdLeave.Parameters.AddWithValue("p_EMPLOYEEID", DBNull.Value);

                        if (!leaveRow.IsActivitiesIDNull()) cmdLeave.Parameters.AddWithValue("p_ACTIVITIESID", leaveRow.ActivitiesID);
                        else cmdLeave.Parameters.AddWithValue("p_ACTIVITIESID", DBNull.Value);

                        if (!leaveRow.IsDayLengthNull()) cmdLeave.Parameters.AddWithValue("p_DayLength", leaveRow.DayLength);
                        else cmdLeave.Parameters.AddWithValue("p_DayLength", DBNull.Value);

                        if (!leaveRow.IsDayLengthNull()) cmdLeave.Parameters.AddWithValue("p_DayLengthID", leaveRow.DayLengthID);
                        else cmdLeave.Parameters.AddWithValue("p_DayLengthID", DBNull.Value);

                        //Check LeaveApply/LeaveAssign..
                        if (boolDS.DataBools[0].BoolValue) //LeaveApply...
                        {
                            cmdLeave.CommandText = "PRO_LMS_LEAVE_CREATE";

                            //if (!leaveRow.IsSupervisorIDNull()) cmdLeave.Parameters.AddWithValue("p_receivedempid", leaveRow.SupervisorID);
                            //else cmdLeave.Parameters.AddWithValue("p_receivedempid", DBNull.Value);

                            if (!leaveRow.IsReceiverEmployeeIDNull()) cmdLeave.Parameters.AddWithValue("p_receivedempid", leaveRow.ReceiverEmployeeID);
                            else cmdLeave.Parameters.AddWithValue("p_receivedempid", DBNull.Value);

                            if (!leaveRow.IsSupervisorIDNull()) cmdLeave.Parameters.AddWithValue("p_SupervisorID", leaveRow.SupervisorID);
                            else cmdLeave.Parameters.AddWithValue("p_SupervisorID", DBNull.Value);
                        }
                        else // LeaveAssign...
                        {
                            cmdLeave.CommandText = "PRO_LMS_LEAVE_ASSIGN_CREATE";

                            if (!leaveRow.IsLogedEmpIDNull()) cmdLeave.Parameters.AddWithValue("p_LOGEDEMPID", leaveRow.LogedEmpID);
                            else cmdLeave.Parameters.AddWithValue("p_LOGEDEMPID", DBNull.Value);

                            if (regularDays > 0) cmdLeave.Parameters.AddWithValue("p_LEAVETYPEID", row.LeaveTypeID);
                            else cmdLeave.Parameters.AddWithValue("p_LEAVETYPEID", row.Adjustment_LeaveID > 0 ? row.Adjustment_LeaveID : row.LeaveTypeID);
                        }

                        //if (row.IsDelegatedUserIDNull() || row.DelegatedUserID == 0)
                        //{ cmdLeave.Parameters.AddWithValue("p_DelegatorEmpID", DBNull.Value); }
                        //else
                        //{ cmdLeave.Parameters.AddWithValue("p_DelegatorEmpID", row.EmployeeID); }

                        if (!row.IsLeaveRequestCommentsNull()) cmdLeave.Parameters.AddWithValue("p_leavereqcomments", row.LeaveRequestComments);
                        else cmdLeave.Parameters.AddWithValue("p_leavereqcomments", DBNull.Value);

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLeave, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmdLeave.Parameters.Clear();

                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD_MULTIPLE_EMPLOYEE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            //return errDS;
                            continue;
                        }
                        #region Hasinul
                        else
                        {
                            if (leaveRow.IsHolidayDeclared)
                            {
                                cmdLvHisUpdate.Parameters.AddWithValue("p_LEAVEID", leaveID);

                                if (regularDays > 0) cmdLvHisUpdate.Parameters.AddWithValue("p_LEAVEREQUESTID", leaveRequestID);
                                else cmdLvHisUpdate.Parameters.AddWithValue("p_LEAVEREQUESTID", adj_LeaveRequestID > 0 ? adj_LeaveRequestID : leaveRequestID);

                                if (!leaveRow.IsEmployeeIDNull()) cmdLvHisUpdate.Parameters.AddWithValue("p_EMPLOYEEID", leaveRow.EmployeeID);
                                else cmdLvHisUpdate.Parameters.AddWithValue("p_EMPLOYEEID", DBNull.Value);

                                cmdLvHisUpdate.Parameters.AddWithValue("p_ACTIVITIESID", 101);

                                if (!leaveRow.IsReceiverEmployeeIDNull()) cmdLvHisUpdate.Parameters.AddWithValue("p_RECEIVEDEMPID", leaveRow.ReceiverEmployeeID);
                                else cmdLvHisUpdate.Parameters.AddWithValue("p_RECEIVEDEMPID", DBNull.Value);

                                if (!leaveRow.IsSupervisorIDNull()) cmdLvHisUpdate.Parameters.AddWithValue("p_SupervisorID", leaveRow.SupervisorID);
                                else cmdLvHisUpdate.Parameters.AddWithValue("p_SupervisorID", DBNull.Value);

                                if (!row.IsLeaveRequestCommentsNull()) cmdLvHisUpdate.Parameters.AddWithValue("p_LEAVEREQCOMMENTS", row.LeaveRequestComments);
                                else cmdLvHisUpdate.Parameters.AddWithValue("p_LEAVEREQCOMMENTS", DBNull.Value);

                                bError = false;
                                nRowAffected = -1;
                                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLvHisUpdate, connDS.DBConnections[0].ConnectionID, ref bError);
                                if (bError)
                                {
                                    ErrorDS.Error err = errDS.Errors.NewError();
                                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD_MULTIPLE_EMPLOYEE.ToString();
                                    errDS.Errors.AddError(err);
                                    errDS.AcceptChanges();
                                    //return errDS;
                                    continue;
                                }
                            }
                        }
                        #endregion
                        regularDays -= leaveRow.LeaveLengthDays;
                    }
                    #endregion

                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                }
            }
            #endregion

            #region Modify Old Leave
            string emailSentTo = "", toEmailAddress = "";
            foreach (lmsLeaveDS.ModifyLeaveRow rowModify in leaveDS.ModifyLeave.Rows)
            {
                string successMessage = "";
                bool bErrorModify = false;
                int nRowAffectedModify = -1;

                #region Bind parameter value
                
                cmdModifyLeave.Parameters.AddWithValue("p_LeaveRequestID", rowModify.LeaveRequestID);
                cmdModifyLeave.Parameters.AddWithValue("p_EmployeeID", rowModify.EmployeeID);
                cmdModifyLeave.Parameters.AddWithValue("p_ELQID", rowModify.ELQID);
                cmdModifyLeave.Parameters.AddWithValue("p_LeaveDate", rowModify.LeaveDate);
                cmdModifyLeave.Parameters.AddWithValue("p_ActivitiesID", rowModify.ActivitiesID);
                cmdModifyLeave.Parameters.AddWithValue("p_DayLength", rowModify.DayLength);
                cmdModifyLeave.Parameters.AddWithValue("p_LeaveLengthDays", rowModify.LeaveLengthDays);
                cmdModifyLeave.Parameters.AddWithValue("p_LeaveLengthHours", rowModify.LeaveLengthHours);
                cmdModifyLeave.Parameters.AddWithValue("p_DayLengthID", rowModify.DayLengthID);
                cmdModifyLeave.Parameters.AddWithValue("p_LeaveHistoryID", 0);
                cmdModifyLeave.Parameters.AddWithValue("p_SenderUserID", rowModify.SenderUserID);
                cmdModifyLeave.Parameters.AddWithValue("p_ReceivedUserID", rowModify.ReceivedUserID);

                if (!rowModify.IsLeaveCommentsNull()) cmdModifyLeave.Parameters.AddWithValue("p_LeaveComments", rowModify.LeaveComments);
                else cmdModifyLeave.Parameters.AddWithValue("p_LeaveComments", DBNull.Value);
                #endregion

                if (rowModify.IsNotificationByMail)
                {
                    #region Transaction after Mail
                    returnMessage = "";
                    toEmailAddress = rowModify.EmailAddress;
                    rowModify.Url += "?pVal=lms";

                    messageBody = "<b>Dispatcher: </b>" + rowModify.SenderUserName + "<br><br>";
                    messageBody += "<b>Applicant: </b>" + rowModify.EmployeeName + " [" + rowModify.EmployeeCode + "]<br>";
                    messageBody += "<b>Leave Type: </b>" + rowModify.LeaveTypeName + "<br>";
                    messageBody += "<b>Leave Date: </b>" + rowModify.LeaveRange;
                    messageBody += "<br><br><br>";
                    messageBody += "Click the following link: ";
                    messageBody += "<br>";
                    messageBody += "<a href='" + rowModify.Url + "'>" + rowModify.Url + "</a>";

                    Mail.Mail mail = new Mail.Mail();
                    if (emailSentTo != toEmailAddress)  // Don't send Email to a User twice.
                    {
                        if (IsEMailSendWithCredential)
                        {
                            returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, rowModify.FromEmailAddress, rowModify.EmailAddress, rowModify.CcEmailAddress, rowModify.MailingSubject, messageBody, rowModify.MailingSubject);
                            emailSentTo = toEmailAddress;
                        }
                        else
                        {
                            returnMessage = mail.SendEmail(host, port, rowModify.FromEmailAddress, rowModify.EmailAddress, rowModify.CcEmailAddress, rowModify.MailingSubject, messageBody, rowModify.MailingSubject);
                            emailSentTo = toEmailAddress;
                        }
                    }
                    else returnMessage = "Email successfully sent.";

                    if (returnMessage == "Email successfully sent.")
                    {
                        nRowAffectedModify = ADOController.Instance.ExecuteNonQuery(cmdModifyLeave, connDS.DBConnections[0].ConnectionID, ref bErrorModify);
                        cmdModifyLeave.Parameters.Clear();
                    }
                    else
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD_MULTIPLE_EMPLOYEE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;

                        messageDS.ErrorMsg.AddErrorMsgRow(rowModify.EmployeeCode + " - " + rowModify.EmployeeName + " [Mail sending failed]");
                        continue;
                    }
                    #endregion
                }
                else  // Mail Not Required
                {
                    nRowAffectedModify = ADOController.Instance.ExecuteNonQuery(cmdModifyLeave, connDS.DBConnections[0].ConnectionID, ref bErrorModify);
                    cmdModifyLeave.Parameters.Clear();
                }
                if (bErrorModify)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_ADD_MULTIPLE_EMPLOYEE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                    continue;
                }

                if (nRowAffectedModify > 0)
                {
                    successMessage = rowModify.EmployeeCode + " - " + rowModify.EmployeeName + " [Modified]";
                    DataRow[] foundRows = messageDS.SuccessMsg.Select("StringValue = '" + successMessage + "'");
                    if (foundRows.Length == 0) messageDS.SuccessMsg.AddSuccessMsgRow(successMessage);
                }
                else messageDS.ErrorMsg.AddErrorMsgRow(rowModify.EmployeeCode + " - " + rowModify.EmployeeName);
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _getLeaveGovernanceInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;
            if (activeStatus.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("getLeaveGovernanceInfo");
            }


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave_Governance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_ACTION_LEAVE_GOVERNANCE_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createleaveGovernance(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Leave Governance Delete

            cmd.CommandText = "PRO_LMS_GOVERNANCE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_GOVERNANCE_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #endregion

            cmd.Dispose();
            cmd.CommandText = "PRO_LMS_GOVERNANCE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            leaveDS.Merge(inputDS.Tables[leaveDS.Leave_Governance.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();


            foreach (lmsLeaveDS.Leave_GovernanceRow row in leaveDS.Leave_Governance.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_LeaveGovernanceID", genPK);
                cmd.Parameters.AddWithValue("p_LeaveTypeID", row.LeaveTypeID);
                cmd.Parameters.AddWithValue("p_ApplicableDays", row.ApplicableDays);
                cmd.Parameters.AddWithValue("p_IsLeavePlanHolder", row.IsLeavePlanHolder);
                cmd.Parameters.AddWithValue("p_IsLeavePlanVarifier", row.IsLeavePlanVarifier);
                cmd.Parameters.AddWithValue("p_OutOfCountry", row.OutOfCountry);


                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_GOVERNANCE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Parameters.Clear();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _loadEmpLeaveEncash_By_EmpTypeSite(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetEmpLeaveEncash_By_EmpTypeSite");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["ActivitiesID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmployeeType"].Value = integerDS.DataIntegers[1].IntegerValue;

            cmd.Parameters["PayrollSiteCode"].Value = stringDS.DataStrings[0].StringValue;

            cmd.Parameters["PayDate"].Value = dateDS.DataDates[0].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.EncashmentVerification.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMP_LEAVE_ENCAS_BY_EMPTYPESITE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getLeaveTypeListForCCMail(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;
            if (activeStatus.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("getLeaveTypeListForCCMail");
            }


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave_Governance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVETYPE_LIST_FOR_CC_MAIL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _LMS_CC_MailUserCreate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Leave Governance Delete

            cmd.CommandText = "PRO_LMS_CCMAIL_USER_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CC_MAIL_USER_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #endregion

            cmd.Dispose();
            cmd.CommandText = "PRO_LMS_CCMAIL_USER_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            leaveDS.Merge(inputDS.Tables[leaveDS.Leave_Governance.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();


            foreach (lmsLeaveDS.Leave_GovernanceRow row in leaveDS.Leave_Governance.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_CCmailUserID", genPK);
                cmd.Parameters.AddWithValue("p_LeaveTypeID", row.LeaveTypeID);

                if (!row.IsDaysNull()) cmd.Parameters.AddWithValue("p_Days", row.Days);
                else cmd.Parameters.AddWithValue("p_Days", DBNull.Value);
                if (!row.IsCcMailModeNull()) cmd.Parameters.AddWithValue("p_CCmailMode", row.CcMailMode);
                else cmd.Parameters.AddWithValue("p_CCmailMode", DBNull.Value);
                if (!row.IsRecipientEmpIDNull()) cmd.Parameters.AddWithValue("p_RecipientEmpID", row.RecipientEmpID);
                else cmd.Parameters.AddWithValue("p_RecipientEmpID", DBNull.Value);
                if (!row.IsCcAddressNull()) cmd.Parameters.AddWithValue("p_CCaddress", row.CcAddress);
                else cmd.Parameters.AddWithValue("p_CCaddress", DBNull.Value);

                //Rony :: 17 May 2017
                if (!row.IsLB_DepartmentalHeadIDNull()) cmd.Parameters.AddWithValue("p_LB_DepartmentalHeadID", row.LB_DepartmentalHeadID);
                else cmd.Parameters.AddWithValue("p_LB_DepartmentalHeadID", DBNull.Value);
                if (!row.IsLB_BranchHeadIDNull()) cmd.Parameters.AddWithValue("p_LB_BranchHeadID", row.LB_BranchHeadID);
                else cmd.Parameters.AddWithValue("p_LB_BranchHeadID", DBNull.Value);
                cmd.Parameters.AddWithValue("p_IsSupervisor", row.IsSupervisor);
                cmd.Parameters.AddWithValue("p_IsLeavePlanHolder", row.IsLeavePlanHolder);
                cmd.Parameters.AddWithValue("p_IsDepartmentHead", row.IsDepartmentHead);
                cmd.Parameters.AddWithValue("p_CC_WhenApprover_LiabilityID", row.CC_WhenApprover_LiabilityID);

                if (!row.IsCC_AppoverEmpIDNull()) cmd.Parameters.AddWithValue("p_CC_AppoverEmpID", row.CC_AppoverEmpID);
                else cmd.Parameters.AddWithValue("p_CC_AppoverEmpID", DBNull.Value);

                //End
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_CC_MAIL_USER_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Parameters.Clear();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private string getDelegateEmailAddress(DataSet inputDS, string pEmpCode)
        {
            string emailAddress = "";
            try
            {

                DBConnectionDS connDS = new DBConnectionDS();
                connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
                connDS.AcceptChanges();

                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDelegateEmailByEmpCode");//Employee.XML
                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                cmd.Parameters["pEmployeeCode"].Value = pEmpCode;

                EmployeeDS employeeDS = new EmployeeDS();

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, employeeDS, employeeDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

                if (employeeDS.Employees.Count > 0)
                {
                    foreach (EmployeeDS.Employee row in employeeDS.Employees)
                    {
                        if (row.IsEmailNull() == false) emailAddress = row.Email;
                        break;
                    }
                }
                return emailAddress;
            }
            catch { emailAddress = ""; return emailAddress; }

            
        }

        private DataSet _saveLMS_LeaveApprovalPolicy(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DesignationDS desDS = new DesignationDS();
            EmployeeDS liabilityDS = new EmployeeDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            leaveDS.Merge(inputDS.Tables[leaveDS.Leave_ApprovalPolicy.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();
            desDS.Merge(inputDS.Tables[desDS.Designations.TableName], false, MissingSchemaAction.Error);
            desDS.AcceptChanges();
            liabilityDS.Merge(inputDS.Tables[liabilityDS.Liability.TableName], false, MissingSchemaAction.Error);
            liabilityDS.AcceptChanges();

            if (leaveDS.Leave_ApprovalPolicy[0].LMS_ApprovalID > 0) // For creation, 0 is sent
            {
                #region Delete existing policy [for this approver + LeaveType]
                cmd.CommandText = "PRO_LMS_APP_POLICY_DELETE";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("p_LMS_ApprovalID", leaveDS.Leave_ApprovalPolicy[0].LMS_ApprovalID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_APPROVAL_POLICY_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion
            }

            #region Save New Policy

            #region Save Approver
            cmd.Dispose();
            cmd.CommandText = "PRO_LMS_APP_POLICY_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            long LMS_ApprovalID = IDGenerator.GetNextGenericPK();
            if (LMS_ApprovalID == -1) return UtilDL.GetDBOperationFailed();

            foreach (lmsLeaveDS.Leave_ApprovalPolicyRow row in leaveDS.Leave_ApprovalPolicy.Rows)
            {
                cmd.Parameters.AddWithValue("p_LMS_ApprovalID", LMS_ApprovalID);

                if (!row.IsApp_DesignationCodeNull())
                {
                    cmd.Parameters.AddWithValue("p_LMS_Approver_Type", "Designation");
                    cmd.Parameters.AddWithValue("p_LMS_Approver_Code", row.App_DesignationCode);
                }
                else if (!row.IsApp_LiabilityCodeNull())
                {
                    cmd.Parameters.AddWithValue("p_LMS_Approver_Type", "Liability");
                    cmd.Parameters.AddWithValue("p_LMS_Approver_Code", row.App_LiabilityCode);
                }
                else if (!row.IsApp_EmployeeCodeNull()) 
                {
                    cmd.Parameters.AddWithValue("p_LMS_Approver_Type", "Employee");
                    cmd.Parameters.AddWithValue("p_LMS_Approver_Code", row.App_EmployeeCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LMS_Approver_Type", DBNull.Value);
                    cmd.Parameters.AddWithValue("p_LMS_Approver_Code", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_LMS_App_SelfAlso", row.LMS_App_SelfAlso);
                cmd.Parameters.AddWithValue("p_Out_Of_Country", row.Out_Of_Country);
                cmd.Parameters.AddWithValue("p_LMS_App_LeaveTypeID", row.LMS_App_LeaveTypeID);
                cmd.Parameters.AddWithValue("p_Max_Approval_Days", row.Max_Approval_Days);
                cmd.Parameters.AddWithValue("p_All_Site", row.All_Site);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_APPROVAL_POLICY_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Parameters.Clear();
            }
            #endregion
            
            #region Save Subordinate Designations
            cmd.Dispose();
            cmd.CommandText = "PRO_LMS_APP_SUBORDINATE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (DesignationDS.Designation des in desDS.Designations)
            {
                cmd.Parameters.AddWithValue("p_LMS_ApprovalID", LMS_ApprovalID);
                cmd.Parameters.AddWithValue("p_Sub_DesignationCode", des.DesignationCode);
                cmd.Parameters.AddWithValue("p_Sub_LiabilityCode", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_APPROVAL_POLICY_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Parameters.Clear();
            }
            #endregion

            #region Save Subordinate Liabilities
            foreach (EmployeeDS.LiabilityRow lia in liabilityDS.Liability.Rows)
            {
                cmd.Parameters.AddWithValue("p_LMS_ApprovalID", LMS_ApprovalID);
                cmd.Parameters.AddWithValue("p_Sub_DesignationCode", DBNull.Value);
                cmd.Parameters.AddWithValue("p_Sub_LiabilityCode", lia.LiabilityCode);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_APPROVAL_POLICY_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Parameters.Clear();
            }
            #endregion

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteLMS_LeaveApprovalPolicy(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            lmsLeaveDS leaveDS = new lmsLeaveDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            leaveDS.Merge(inputDS.Tables[leaveDS.Leave_ApprovalPolicy.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();

            cmd.CommandText = "PRO_LMS_APP_POLICY_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (lmsLeaveDS.Leave_ApprovalPolicyRow row in leaveDS.Leave_ApprovalPolicy.Rows)
            {
                cmd.Parameters.AddWithValue("p_LMS_ApprovalID", row.LMS_ApprovalID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_LEAVE_APPROVAL_POLICY_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Parameters.Clear();
            }

            return errDS;
        }
        private DataSet _getLMS_LeaveApprovalPolicy(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("Get_LeaveApproval_Policy");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave_ApprovalPolicy.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_APPROVAL_POLICY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            leaveDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(leaveDS.Leave_ApprovalPolicy);
            returnDS.Merge(errDS);
            return returnDS;
        }

        private string _getEmpCodeWithDelegates(int employeeID, DBConnectionDS connDS)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmpCode_With_Delegates");
            if (cmd == null) return ""; // UtilDL.GetCommandNotFound();
            cmd.Parameters["EmployeeID"].Value = employeeID;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            string empCodeWithDelegate = UtilDL.GetCode(connDS, cmd);

            return empCodeWithDelegate;
        }

        private DataSet _getPendingApprovalLeaveList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            string activeStatus = stringDS.DataStrings[0].StringValue;
            if (activeStatus.Equals("1"))
            {
                cmd = DBCommandProvider.GetDBCommand("getPendingApprovalLeaveListByEmpID");
            }
            else cmd = DBCommandProvider.GetDBCommand("getPendingApprovalLeaveList");

            cmd.Parameters["FromDate"].Value = Convert.ToDateTime(stringDS.DataStrings[9].StringValue);
            cmd.Parameters["ToDate"].Value = Convert.ToDateTime(stringDS.DataStrings[10].StringValue);
            cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ReceiverEmpCode"].Value = stringDS.DataStrings[8].StringValue;
            cmd.Parameters["ActivitiesID1"].Value = cmd.Parameters["ActivitiesID2"].Value = stringDS.DataStrings[11].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
            cmd.Parameters["DepartmentCode1"].Value = cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode1"].Value = cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[5].StringValue;
            cmd.Parameters["LiabilityCode1"].Value = cmd.Parameters["LiabilityCode2"].Value = stringDS.DataStrings[6].StringValue;
            cmd.Parameters["CompanyDivisionID1"].Value = cmd.Parameters["CompanyDivisionID2"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
            cmd.Parameters["EmpStatus1"].Value = cmd.Parameters["EmpStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveRequests.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PENDING_APPROVAL_LEAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS.LeaveRequests);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getLMS_BypassApprover(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeDS employeeDS = new EmployeeDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLMS_BypassApprover");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeDS, employeeDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_BYPASS_APPROVER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            employeeDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeDS.Employees);
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _getInformationAny_LMS(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "GetEmployee_AbsentDate")
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeInfo_AbsentDate");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["EmployeeID2"].Value = (object)Convert.ToInt32(stringDS.DataStrings[1].StringValue);                
            }
            else if (stringDS.DataStrings[0].StringValue == "GetEmployeeForAbsentDate")
            {
                if (!Convert.ToBoolean(stringDS.DataStrings[1].StringValue))
                {
                    cmd = DBCommandProvider.GetDBCommand("GetEmployeeForAbsentDate");
                    if (cmd == null) return UtilDL.GetCommandNotFound();
                    cmd.Parameters["AdsDate1"].Value = cmd.Parameters["AdsDate2"].Value = cmd.Parameters["AdsDate3"].Value =
                        cmd.Parameters["AdsDate4"].Value = cmd.Parameters["AdsDate5"].Value = (object)Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                }
                else 
                {
                    cmd = DBCommandProvider.GetDBCommand("GetAllEmployeeForAbsentDate");
                    if (cmd == null) return UtilDL.GetCommandNotFound();
                    cmd.Parameters["AdsDate"].Value = (object)Convert.ToDateTime(stringDS.DataStrings[2].StringValue);                
                }
                cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = (object)stringDS.DataStrings[3].StringValue;
                cmd.Parameters["SiteId1"].Value = cmd.Parameters["SiteId2"].Value = (object)Convert.ToInt32(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["DepartmentId1"].Value = cmd.Parameters["DepartmentId2"].Value = (object)Convert.ToInt32(stringDS.DataStrings[5].StringValue);
                cmd.Parameters["DesignationCode1"].Value = cmd.Parameters["DesignationCode2"].Value = (object)stringDS.DataStrings[6].StringValue;
                cmd.Parameters["EmployeeType1"].Value = cmd.Parameters["EmployeeType2"].Value = (object)Convert.ToInt32(stringDS.DataStrings[7].StringValue);
                cmd.Parameters["CompanyDivisionID1"].Value = cmd.Parameters["CompanyDivisionID2"].Value = (object)Convert.ToInt32(stringDS.DataStrings[8].StringValue);
                cmd.Parameters["FunctionCode1"].Value = cmd.Parameters["FunctionCode2"].Value = (object)stringDS.DataStrings[9].StringValue;
            }
            else if (stringDS.DataStrings[0].StringValue == "GetLastFiscalYearAllocationDate")
            {
                cmd = DBCommandProvider.GetDBCommand("GetLastFiscalYearAllocationDate");
                if (cmd == null) return UtilDL.GetCommandNotFound();             
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            DataSet DataDS = new DataSet();
            DataDS.Tables.Add("RecordList");

            nRowAffected = ADOController.Instance.Fill(adapter, DataDS, DataDS.Tables["RecordList"].ToString(), connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_INFORMATION_ANY_LMS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                returnDS.Merge(errDS);
                return returnDS;
            }
            DataDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(DataDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _saveAbsentEmployees(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            lmsLeaveDS leaveDS = new lmsLeaveDS();
            DesignationDS desDS = new DesignationDS();
            EmployeeDS liabilityDS = new EmployeeDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            leaveDS.Merge(inputDS.Tables[leaveDS.AbsentEmployee.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();            
            
            #region Delete existing Data
            if (!Convert.ToBoolean(leaveDS.AbsentEmployee[0].IsForcelyAbsent))
            {
                cmd.CommandText = "PRO_ABSENT_EMPLOYEE_DELETE";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("p_ABSENTDATE", Convert.ToDateTime(leaveDS.AbsentEmployee[0].AbsentDate));
                cmd.Parameters.AddWithValue("p_CREATEBYUSERID", Convert.ToInt32(leaveDS.AbsentEmployee[0].CreateByUserID));
                cmd.Parameters.AddWithValue("p_EmployeeCode", leaveDS.AbsentEmployee[0].EmployeeCode);
                cmd.Parameters.AddWithValue("p_SiteID", Convert.ToInt32(leaveDS.AbsentEmployee[0].SiteID));
                cmd.Parameters.AddWithValue("p_DepartmentID", Convert.ToInt32(leaveDS.AbsentEmployee[0].DepartmentID));
                cmd.Parameters.AddWithValue("p_DesignationCODE", leaveDS.AbsentEmployee[0].DesignationCODE);
                cmd.Parameters.AddWithValue("p_EmployeeType", Convert.ToInt32(leaveDS.AbsentEmployee[0].EmployeeType));
                cmd.Parameters.AddWithValue("p_FunctionCode", leaveDS.AbsentEmployee[0].FunctionCode);
                cmd.Parameters.AddWithValue("p_CompanyDivisionID", Convert.ToInt32(leaveDS.AbsentEmployee[0].CompanyDivisionID));

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ABSENCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            #region Save Approver
            cmd.Dispose();
            cmd.CommandText = "PRO_ABSENT_EMPLOYEE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            long LMS_ApprovalID = IDGenerator.GetNextGenericPK();
            if (LMS_ApprovalID == -1) return UtilDL.GetDBOperationFailed();

            foreach (lmsLeaveDS.AbsentEmployeeRow row in leaveDS.AbsentEmployee.Rows)
            {
                cmd.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);
                cmd.Parameters.AddWithValue("p_ABSENTDATE", row.AbsentDate);
                cmd.Parameters.AddWithValue("p_CREATEBYUSERID", row.CreateByUserID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ABSENCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Parameters.Clear();
            }
            #endregion
            
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getLeaveApprovalReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            string sortingBy = stringDS.DataStrings[0].StringValue;
            if (sortingBy.Equals("1"))
            {
                cmd = DBCommandProvider.GetDBCommand("getLeaveApprovalReport_NRB");
            }
            else cmd = DBCommandProvider.GetDBCommand("getLeaveApprovalReportByDesig_NRB");

            cmd.Parameters["FromDate"].Value = Convert.ToDateTime(stringDS.DataStrings[9].StringValue);
            cmd.Parameters["ToDate"].Value = Convert.ToDateTime(stringDS.DataStrings[10].StringValue);
            cmd.Parameters["ReceiverEmpCode"].Value = stringDS.DataStrings[8].StringValue;
            //cmd.Parameters["ActivitiesID1"].Value = cmd.Parameters["ActivitiesID2"].Value = stringDS.DataStrings[11].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
            cmd.Parameters["DepartmentCode1"].Value = cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode1"].Value = cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[5].StringValue;
            cmd.Parameters["LiabilityCode1"].Value = cmd.Parameters["LiabilityCode2"].Value = stringDS.DataStrings[6].StringValue;
            cmd.Parameters["CompanyDivisionID1"].Value = cmd.Parameters["CompanyDivisionID2"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
            cmd.Parameters["EmpStatus1"].Value = cmd.Parameters["EmpStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS leaveDS = new lmsLeaveDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.LeaveApproveReport.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_APPROVAL_REPORT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS.LeaveApproveReport);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getEmployeeWorkSpaceInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            string empCode = stringDS.DataStrings[0].StringValue;
            cmd = DBCommandProvider.GetDBCommand("getEmployeeWorkSpaceInfo");
            cmd.Parameters["EmployeeCode"].Value = empCode;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            EmployeeDS empDS = new EmployeeDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.EmployeeReport.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_APPROVAL_REPORT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            empDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empDS.EmployeeReport);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}


