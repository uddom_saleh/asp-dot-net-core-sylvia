using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;


namespace Sylvia.DAL.Payroll
{
    class RequisitionCategoryDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_REQUISITION_CATEGORY_CREATE:
                    return this._RequisitionCategoryEntry(inputDS);
                case ActionID.NA_ACTION_GET_CATEGORY_LIST:
                    return this._getCategoryList(inputDS);
                case ActionID.ACTION_REQUISITION_CATEGORY_UPD:
                    return this._updateRequisitionCategories(inputDS);
                case ActionID.ACTION_CATEGORY_DELETE:
                    return this._deleteCategories(inputDS);
                case ActionID.NA_ACTION_GET_REQUISITION_CATEGORY_LIST_ALL:
                    return this._GetRequisitionCategoryListAll(inputDS);
                case ActionID.NA_ACTION_GET_REQUISITION_TYPES_LIST_ALL:
                    return this._GetRequisitionTypesListAll(inputDS);
                case ActionID.NA_ACTION_GET_REQ_TYPE_LIST_BY_CATEGORYEID:
                    return this._GetRequisitionTypeByReqCategoryID(inputDS);
                case ActionID.NA_ACTION_GET_REQ_FIELD_LIST_BY_TYPEID:
                    return this._GetRequisitionFieldByReqTypeID(inputDS);
                case ActionID.ACTION_REQ_CATEGORY_CONFIG_CREATE:
                    return this._RequisitionCategoryConfigEntry(inputDS);
                case ActionID.ACTION_REQ_CATEGORY_CONFIG_DELETE:
                    return this._RequisitionCategoryConfigDelete(inputDS);
                case ActionID.NA_ActionGetFieldBy_TypeCodeCategoryID:
                    return this._GetRequisitionFieldBTypeCodeCategoryID(inputDS);
                case ActionID.NA_ACTION_REQUISITION_GET_LCGRADE_LIST:
                    return this._GetLCGradeListAll_ReqCateConfig(inputDS);
                case ActionID.ACTION_REQUISITION_CATEGORY_ASSIGN:
                    return this._assignGradeToReqCategory(inputDS);
                case ActionID.ACTION_FIELD_LIMIT_CONFIG:
                    return this._LimitMarkableValueConfigEntry(inputDS);
                case ActionID.NA_ACTION_GET_REQ_FIELD_VALUE:
                    return this._GetRequisitionFieldValue(inputDS);
                case ActionID.NA_ACTION_GET_REQ_LIMIT_FIELD_LIST_BY_TYPEID:
                    return this._GetReqLimitFieldListByReqTypeID(inputDS);
                case ActionID.NA_ACTION_GET_REQ_LIMIT_FIELD_LIST_BY_TYPEID2:
                    return this._GetReqLimitFieldListByReqTypeID2(inputDS);
                case ActionID.NA_ACTION_GET_REQUISITION_HISTORY:
                    return this._GetRequisitionDataForReport(inputDS);
                case ActionID.NA_ACTION_GET_REQUISITION_HEALTH_SAFTY:
                    return this._GetReqHealthSafetyForReport(inputDS);

                case ActionID.NA_ACTION_GET_ASSETS_HISTORY:
                    return this._GetAssetsDataForReport(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _RequisitionCategoryEntry(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("RequisitionCategoryEntry");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            RequisitionDS ReqDS = new RequisitionDS();
            ReqDS.Merge(inputDS.Tables[ReqDS.RequisitionCategory.TableName], false, MissingSchemaAction.Error);
            ReqDS.AcceptChanges();

            foreach (RequisitionDS.RequisitionCategoryRow CategoryRow in ReqDS.RequisitionCategory.Rows)
            {

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters["ReqID"].Value = (object)genPK;


                if (CategoryRow.IsREQCATEGORYCODENull() == false)
                {
                    cmd.Parameters["ReqCode"].Value = (object)CategoryRow.REQCATEGORYCODE;
                }
                else
                {
                    cmd.Parameters["ReqCode"].Value = null;
                }

                if (CategoryRow.IsREQCATEGORYNAMENull() == false)
                {
                    cmd.Parameters["ReqName"].Value = (object)CategoryRow.REQCATEGORYNAME;
                }
                else
                {
                    cmd.Parameters["ReqName"].Value = null;
                }

                if (CategoryRow.IsREQCATEGORYREMARKSNull() == false)
                {
                    cmd.Parameters["ReqCateRemarks"].Value = (object)CategoryRow.REQCATEGORYREMARKS;
                }
                else
                {
                    cmd.Parameters["ReqCateRemarks"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQUISITION_CATEGORY_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getCategoryList(DataSet inputDS)
        {

            OleDbCommand cmd = new OleDbCommand();
            //Product_MngtDS ProductDS = new Product_MngtDS();
            RequisitionDS ReqDS = new RequisitionDS();

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string ReqCategoryCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            ReqCategoryCode = stringDS.DataStrings[0].StringValue;


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Get sql command from commandXML folder
            if (ReqCategoryCode != "")
            {
                cmd = DBCommandProvider.GetDBCommand("GetCategoryListList");
                cmd.Parameters["ReqCateCode"].Value = ReqCategoryCode;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetCategoryListListAll");
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, ReqDS, ReqDS.RequisitionCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_CATEGORY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            ReqDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(ReqDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _deleteCategories(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteRequisitionCategories");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["REQCATEGORYCODE"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CATEGORY_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _updateRequisitionCategories(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            RequisitionDS ReqDS = new RequisitionDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateRequisitonCategories");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            ReqDS.Merge(inputDS.Tables[ReqDS.RequisitionCategory.TableName], false, MissingSchemaAction.Error);
            ReqDS.AcceptChanges();



            foreach (RequisitionDS.RequisitionCategoryRow ReqCategoryRow in ReqDS.RequisitionCategory.Rows)
            {

                if (ReqCategoryRow.IsREQCATEGORYNAMENull() == false)
                {
                    cmd.Parameters["REQCATEGORYNAME"].Value = (object)ReqCategoryRow.REQCATEGORYNAME;
                }
                else
                {
                    cmd.Parameters["REQCATEGORYNAME"].Value = null;
                }


                if (ReqCategoryRow.IsREQCATEGORYREMARKSNull() == false)
                {
                    cmd.Parameters["REQCATEGORYREMARKS"].Value = (object)ReqCategoryRow.REQCATEGORYREMARKS;
                }
                else
                {
                    cmd.Parameters["REQCATEGORYREMARKS"].Value = null;
                }


                if (ReqCategoryRow.IsREQCATEGORYCODENull() == false)
                {
                    cmd.Parameters["REQCATEGORYCODE"].Value = (object)ReqCategoryRow.REQCATEGORYCODE;
                }
                else
                {
                    cmd.Parameters["REQCATEGORYCODE"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQUISITION_CATEGORY_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetRequisitionCategoryListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetCategoryListListAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RequisitionCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetRequisitionTypesListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetTypesListListAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RequisitionType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetRequisitionTypeByReqCategoryID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRTypeListByReqCateID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["REQCATEGORYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.ReqCateConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _GetRequisitionFieldByReqTypeID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRFieldListLimitByTypeID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["REQUISITIONTYPEID1"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            cmd.Parameters["REQUISITIONTYPEID2"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            cmd.Parameters["REQCATEGORYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[1].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RIFConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _RequisitionCategoryConfigEntry(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Data Delete from "ReqCateConfig" Table by gridCode wise

            //create command
            OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("RequisitionCategoryConfigDelete");

            if (cmd1 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            RequisitionDS RequisitionDS1 = new RequisitionDS();
            RequisitionDS1.Merge(inputDS.Tables[RequisitionDS1.ReqCateConfig.TableName], false, MissingSchemaAction.Error);
            RequisitionDS1.AcceptChanges();

            foreach (RequisitionDS.ReqCateConfigRow ReqCateConfigRow1 in RequisitionDS1.ReqCateConfig.Rows)
            {


                if (ReqCateConfigRow1.IsREQCATEGORYIDNull() == false)
                {
                    cmd1.Parameters["REQCATEGORYID"].Value = (object)ReqCateConfigRow1.REQCATEGORYID;
                }
                else
                {
                    cmd1.Parameters["REQCATEGORYID"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd1, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQ_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion


            #region Data Insert to "ReqCateConfig" from grdTypeList
            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("RequisitionCategoryConfigEntry");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            RequisitionDS RequisitionDS = new RequisitionDS();
            RequisitionDS.Merge(inputDS.Tables[RequisitionDS.ReqCateConfig.TableName], false, MissingSchemaAction.Error);
            RequisitionDS.AcceptChanges();

            foreach (RequisitionDS.ReqCateConfigRow ReqCateConfigRow in RequisitionDS.ReqCateConfig.Rows)
            {

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters["RCCID"].Value = (object)genPK;

                if (ReqCateConfigRow.IsREQCATEGORYIDNull() == false)
                {
                    cmd.Parameters["REQCATEGORYID"].Value = (object)ReqCateConfigRow.REQCATEGORYID;
                }
                else
                {
                    cmd.Parameters["REQCATEGORYID"].Value = null;
                }


                if (ReqCateConfigRow.IsREQUISITIONTYPECODeNull() == false)
                {
                    cmd.Parameters["REQUISITIONTYPECODe"].Value = (object)ReqCateConfigRow.REQUISITIONTYPECODe;
                }
                else
                {
                    cmd.Parameters["REQUISITIONTYPECODe"].Value = null;
                }



                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);


                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQ_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion



            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _RequisitionCategoryConfigDelete(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Data Delete from database grid code wise
            //create command
            OleDbCommand cmd4 = DBCommandProvider.GetDBCommand("RequisitionCategoryConfigDelete");

            if (cmd4 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            RequisitionDS RequisitionDS1 = new RequisitionDS();
            RequisitionDS1.Merge(inputDS.Tables[RequisitionDS1.ReqCateConfig.TableName], false, MissingSchemaAction.Error);
            RequisitionDS1.AcceptChanges();

            foreach (RequisitionDS.ReqCateConfigRow ReqCateConfigRow1 in RequisitionDS1.ReqCateConfig.Rows)
            {


                if (ReqCateConfigRow1.IsREQCATEGORYIDNull() == false)
                {
                    cmd4.Parameters["REQCATEGORYID"].Value = (object)ReqCateConfigRow1.REQCATEGORYID;
                }
                else
                {
                    cmd4.Parameters["REQCATEGORYID"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd4, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQ_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetRequisitionFieldBTypeCodeCategoryID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRFieldListByTypeCodeCateID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["REQUISITIONTYPECODE"].Value = (object)StringDS.DataStrings[0].StringValue;
            cmd.Parameters["REQCATEGORYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[1].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RIFConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _GetLCGradeListAll_ReqCateConfig(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetReqGradeListAll_ReqCategory");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Grade.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;




        }

        private DataSet _assignGradeToReqCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REQUSITION_GRADE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            //lmsLeaveCategoryDS RequisitionDS = new lmsLeaveCategoryDS();
            RequisitionDS RequisitionDS = new RequisitionDS();
            RequisitionDS.Merge(inputDS.Tables[RequisitionDS.Grade.TableName], false, MissingSchemaAction.Error);
            RequisitionDS.AcceptChanges(); ;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (RequisitionDS.GradeRow row in RequisitionDS.Grade.Rows)
            {
                cmd.Parameters.AddWithValue("p_GradeID", row.GRADEID);
                if (row.IsREQCATEGORYIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ReqCategoryID", row.REQCATEGORYID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ReqCategoryID", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQUISITION_CATEGORY_ASSIGN.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _LimitMarkableValueConfigEntry(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            #region Data Delete from "REQCATECONFIG_WITH_FIELDLIMIT" Database
            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("ReqCateConfigFieldLimitDelete");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            RequisitionDS RequisitionDS = new RequisitionDS();
            RequisitionDS.Merge(inputDS.Tables[RequisitionDS.REQCATECONFIG_WITH_FIELDLIMIT.TableName], false, MissingSchemaAction.Error);
            RequisitionDS.Merge(inputDS.Tables[RequisitionDS.ReqCateConfig.TableName], false, MissingSchemaAction.Error);

            RequisitionDS.AcceptChanges();

            foreach (RequisitionDS.ReqCateConfigRow ReqCateConfigRow in RequisitionDS.ReqCateConfig.Rows)
            {
                if (ReqCateConfigRow.IsREQCATEGORYIDNull() == false)
                {
                    cmd.Parameters["REQCATEGORYID"].Value = (object)ReqCateConfigRow.REQCATEGORYID;
                }
                else
                {
                    cmd.Parameters["REQCATEGORYID"].Value = null;
                }


                if (ReqCateConfigRow.IsREQUISITIONTYPEIDNull() == false)
                {
                    cmd.Parameters["REQUISITIONTYPEid"].Value = (object)ReqCateConfigRow.REQUISITIONTYPEID;
                }
                else
                {
                    cmd.Parameters["REQUISITIONTYPEid"].Value = null;
                }
            }


            foreach (RequisitionDS.REQCATECONFIG_WITH_FIELDLIMITRow ReqCateConfigRow_ in RequisitionDS.REQCATECONFIG_WITH_FIELDLIMIT.Rows)
            {

                if (ReqCateConfigRow_.IsfieldNameNull() == false)
                {
                    cmd.Parameters["fieldName"].Value = (object)ReqCateConfigRow_.fieldName;
                }
                else
                {
                    cmd.Parameters["fieldName"].Value = null;
                }



                bool bError = false;
                int nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);


                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQ_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }




            #endregion


            #region Data Insert to "REQCATECONFIG_WITH_FIELDLIMIT" from grdLimitValueList
            //create command
            OleDbCommand cmd3 = DBCommandProvider.GetDBCommand("ReqCateConfigFieldLimitEntry");

            if (cmd3 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            RequisitionDS RequisitionDS3 = new RequisitionDS();
            RequisitionDS3.Merge(inputDS.Tables[RequisitionDS3.REQCATECONFIG_WITH_FIELDLIMIT.TableName], false, MissingSchemaAction.Error);
            RequisitionDS3.Merge(inputDS.Tables[RequisitionDS3.ReqCateConfig.TableName], false, MissingSchemaAction.Error);

            RequisitionDS3.AcceptChanges();

            foreach (RequisitionDS.ReqCateConfigRow ReqCateConfigRow in RequisitionDS3.ReqCateConfig.Rows)
            {
                if (ReqCateConfigRow.IsREQCATEGORYIDNull() == false)
                {
                    cmd3.Parameters["REQCATEGORYID"].Value = (object)ReqCateConfigRow.REQCATEGORYID;
                }
                else
                {
                    cmd3.Parameters["REQCATEGORYID"].Value = null;
                }


                if (ReqCateConfigRow.IsREQUISITIONTYPEIDNull() == false)
                {
                    cmd3.Parameters["REQUISITIONTYPEid"].Value = (object)ReqCateConfigRow.REQUISITIONTYPEID;
                }
                else
                {
                    cmd3.Parameters["REQUISITIONTYPEid"].Value = null;
                }
            }


            foreach (RequisitionDS.REQCATECONFIG_WITH_FIELDLIMITRow ReqCateConfigRow_ in RequisitionDS3.REQCATECONFIG_WITH_FIELDLIMIT.Rows)
            {

                if (ReqCateConfigRow_.IsfieldNameNull() == false)
                {
                    cmd3.Parameters["fieldName"].Value = (object)ReqCateConfigRow_.fieldName;
                }
                else
                {
                    cmd3.Parameters["fieldName"].Value = null;
                }


                if (ReqCateConfigRow_.IsMinValueNull() == false)
                {
                    cmd3.Parameters["MinValue"].Value = (object)ReqCateConfigRow_.MinValue;
                }
                else
                {
                    cmd3.Parameters["MinValue"].Value = null;
                }


                if (ReqCateConfigRow_.IsMaxValueNull() == false)
                {
                    cmd3.Parameters["MaxValue"].Value = (object)ReqCateConfigRow_.MaxValue;
                }
                else
                {
                    cmd3.Parameters["MaxValue"].Value = null;
                }



                bool bError = false;
                int nRowAffected = -1;

                if (cmd3.Parameters["MinValue"].Value != null && cmd3.Parameters["MaxValue"].Value != null)
                {
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd3, connDS.DBConnections[0].ConnectionID, ref bError);
                }

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQ_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }




            #endregion


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetRequisitionFieldValue(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRFieldvalue");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["REQUISITIONTYPEID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            cmd.Parameters["REQCATEGORYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[1].StringValue);
            cmd.Parameters["FIELDNAME"].Value = (object)StringDS.DataStrings[2].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RIFConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _GetReqLimitFieldListByReqTypeID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetReqListLimitFieldByTypeID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["REQUISITIONTYPEID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RIFConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _GetReqLimitFieldListByReqTypeID2(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetReqListLimitFieldByTypeID2");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            //cmd.Parameters["REQUISITIONTYPEID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RIFConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _GetRequisitionDataForReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetRequisitionHistory");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.ReqHistory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetReqHealthSafetyForReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetReqHealthSafety");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.ReqHistory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetAssetsDataForReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetAssetsHistory");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.ReqHistory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

    }


}


