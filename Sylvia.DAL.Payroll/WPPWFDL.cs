﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using System.Globalization;


namespace Sylvia.DAL.Payroll
{
    public class WPPWFDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_FUND_HEAD_CREATION:
                    return this._FundHeadCreation(inputDS);
                case ActionID.NA_GET_FUND_HEAD_LIST:
                    return this._GetFundHeadList(inputDS);
                case ActionID.ACTION_FUND_HEAD_DELETE:
                    return this._DeleteFundHead(inputDS);

                case ActionID.ACTION_ORGANIZATION_CREATION:
                    return this._CreationOrganization(inputDS);
                case ActionID.NA_GET_ORGANIZATION_LIST:
                    return this._GetOrganizationList(inputDS);
                case ActionID.ACTION_ORGANIZATION_UPDATE:
                    return this._UpdateOrganization(inputDS);
                case ActionID.ACTION_ORGANIZATION_DELETE:
                    return this._DeleteOrganization(inputDS);

                case ActionID.ACTION_SCHEME_CREATE:
                    return this._SchemeCreation(inputDS);
                case ActionID.NA_GET_SCHEME_LIST_BY_ORGANIZATIONID:
                    return this._GetSchemeListByOrganizationID(inputDS);
                case ActionID.ACTION_SCHEME_DELETE:
                    return this._DeleteScheme(inputDS);


                case ActionID.ACTION_TERMS_OF_SEPARATION_CREATE:
                    return this._TermsOfSeperationCreate(inputDS);
                case ActionID.NA_GET_TERMS_OF_SEPARATION:
                    return this._GetTermsOfSeperationByParameter(inputDS);
                case ActionID.ACTION_TERMS_OF_SEPARATION_UPDATE:
                    return this._TermsOfSeperationUpdate(inputDS);
                case ActionID.ACTION_TERMS_OF_SEPARATION_DELETE:
                    return this._DeleteTermsOfSeperation(inputDS);

                case ActionID.NA_GET_ACTIVE_EMPLOYEE_LIST:
                    return this.GetActiveEmployeeList(inputDS);

                case ActionID.ACTION_CREATE_FUND_ALLOCATION:
                    return this._CreateFundAllocation(inputDS);
                case ActionID.NA_GET_FISCAL_YEAR_WPPWF:
                    return this._GetFiscalYearWPPWF(inputDS);
                case ActionID.NA_GET_FUND_ALLOCATION_INFO:
                    return this._GetFundAllocationInfo(inputDS);
                case ActionID.ACTION_UPDATE_FUND_ALLOCATION:
                    return this._UpdateFundAllocation(inputDS);
                case ActionID.ACTION_DELETE_FUND_ALLOCATION:
                    return this._DeleteFundAllocation(inputDS);

                case ActionID.NA_GET_INVESTMENT_ABLE_FUND_INFO:
                    return this._GetInvestmentableFund(inputDS);
                case ActionID.ACTION_CREATE_INVESTMENT:
                    return this._CreateInvestment(inputDS);
                case ActionID.NA_GET_INVESTMENT_LIST:
                    return this._GetInvestmentList(inputDS);
                case ActionID.NA_GET_INVESTMENT_LIST_BY_INVID:
                    return this._GetInvestmentListByInvID(inputDS);
                case ActionID.ACTION_UPDATE_INVESTMENT:
                    return this._UpdateInvestment(inputDS);
                case ActionID.ACTION_FREEZE_INVESTMENT:
                    return this._FreezeInvestment(inputDS);

                case ActionID.ACTION_INVESTMENT_SETTLEMENT_CREATE:
                    return this._InvestmentSettlementCreate(inputDS);

                case ActionID.ACTION_EXTRAORDINARY_INCOME_COST_CREATE:
                    return this._ExtraOrdinaryBeneficiaryCreate(inputDS);
                case ActionID.NA_GET_EXTRAORDINARY_INCOME_COST:
                    return this._GetExtraordinaryIncomeCost(inputDS);
                case ActionID.NA_GET_EXTRAORDINARY_INCOME_COST_BENEFICIARY:
                    return this._GetExtraordinaryIncomeCostBeneficiary(inputDS);
                case ActionID.ACTION_EXTRAORDINARY_INCOME_COST_UPDATE:
                    return this._ExtraOrdinaryBeneficiaryUpdate(inputDS);
                case ActionID.ACTION_EXTRAORDINARY_INCOME_COST_DELETE:
                    return this._ExtraOrdinaryBeneficiaryDelete(inputDS);

                case ActionID.ACTION_PROVISION_CREATION:
                    return this._ProvisionCreation(inputDS);
                case ActionID.NA_GET_INVESTMENT_PROVISION_LIST:
                    return this._GetInvestmentProvisionList(inputDS);
                case ActionID.NA_GET_WP_EMPLOYEE_GL_INFO:
                    return this._GetEmployeeGLInfo(inputDS);
                case ActionID.NA_GET_FUND_HEAD_WISE_BALANCE:
                    return this._GetFundHeadWiseInfo(inputDS);

                case ActionID.ACTION_HEAD_WISE_PAYMENT_CREATE:
                    return this._HeadWisePaymentCreate(inputDS);
                case ActionID.NA_GET_WPPWF_BALANCE_EMP_WISE:
                    return this._GetWPPWFbalanceEmpWise(inputDS);

                case ActionID.NA_GET_WPPWF_FUND_PAYMENT_BENEFICIARY_LIST:
                    return this._GetFundBeneficiaryList(inputDS);
                case ActionID.NA_GET_ALL_BENEFICIARY_LIST:
                    return this._GetBeneficiaryInfo_All(inputDS);
                case ActionID.ACTION_EMPLOYEE_SEPERATION_CREATE:
                    return this._CreateEmployeeSeperation(inputDS);

                case ActionID.ACTION_TERMS_OF_SEPARATION_CREATE_WPPWF:
                    return this._CreateTermsOfSeperation_WPPWF(inputDS);
                case ActionID.ACTION_TERMS_OF_SEPARATION_UPDATE_WPPWF:
                    return this._UpdateTermsOfSeperation_WPPWF(inputDS);
                case ActionID.ACTION_TERMS_OF_SEPARATION_DELETE_WPPWF:
                    return this._DeleteTermsOfSeperation_WPPWF(inputDS);
                case ActionID.NA_GET_TERMS_OF_SEPARATION_WPPWF:
                    return this._GetTermsOfSeperation_WPPWF(inputDS);

                case ActionID.ACTION_DELETE_LAST_INVESTMENT:
                    return this._DeleteLastInvestment_WPPWF(inputDS);
                case ActionID.NA_GET_LAST_FUND_ALLOCATION_DATE_WPPWF:
                    return this._GetLastFundAllocationDate_WPPWF(inputDS);
                case ActionID.NA_GET_WP_PROVISION_BENEFICIARY_LIST:
                    return this._GetProvisionBeneficiaryList_WPPWF(inputDS);

                case ActionID.NA_GET_PROVISION_CALCULATED_DATA_WPPWF:
                    return this._GetProvisionCalculatedData_WPPWF(inputDS);
                case ActionID.ACTION_SAVE_PROVISION_CALCULATED_DATA_WPPWF:
                    return this._SaveProvisionCalculatedData_WPPWF(inputDS);
                case ActionID.NA_GET_PROVISIONED_PROFIT_FOR_EMPLOYEE_WPPWF:
                    return this._GetProvisionedProfit_ForEmployee_WPPWF(inputDS);
                case ActionID.ACTION_DISTRIBUTE_PROVISIONED_PROFIT_WPPWF:
                    return this._DistributeProvisionedProfit_WPPWF(inputDS);

                case ActionID.NA_GET_PENDING_DISTRIBUTION_WPPWF:
                    return this._GetPendingDistributions_WPPWF(inputDS);

                case ActionID.ACTION_FUND_JOURNAL_ENTRY_WPPWF:
                    return this._FundJournalEntry_WPPWF(inputDS);
                case ActionID.ACTION_EMPLOYEE_JOURNAL_ENTRY_WPPWF:
                    return this._EmployeeJournalEntry_WPPWF(inputDS);
                case ActionID.NA_GET_FUND_HEAD_GL_WPPWF:
                    return this._GetFundHeadGL_WPPWF(inputDS);
                case ActionID.NA_GET_TOTAL_PAYABLE_TO_LSC_WPPWF:
                    return this._GetTotalPayableToLSC_WPPWF(inputDS);

                case ActionID.NA_GET_DISTRIBUTED_PROVISION_DATA_WPPWF:
                    return this.GetDistributedProvisionData_WPPWF(inputDS);

                case ActionID.NA_GET_WPPWF_EMPLOYEE_LIST_FOR_SEPERATION:
                    return this._GetEmployeeListForSeperation_WPPWF(inputDS);
                case ActionID.ACTION_WPPWF_EMPLOYEE_PAYMENT:
                    return this._EmployeePayment_WPPWF(inputDS);
                case ActionID.NA_GET_WPPWF_FUNDHEAD_GL_BALANCE:
                    return this._GetFundHeadGL_Balance_WPPWF(inputDS);

                // Balance Modification
                case ActionID.NA_GET_WPPWF_FUNDHEAD_GL_RECORDS:
                    return this._GetFundHeadGL_Records_WPPWF(inputDS);
                case ActionID.ACTION_WPPWF_MODIFY_FUNDHEAD_GL_RECORDS:
                    return this._ModifyFundHeadGL_Records_WPPWF(inputDS);
                case ActionID.NA_GET_WPPWF_EMPLOYEE_GL_RECORDS:
                    return this._GetEmployeeGL_Records_WPPWF(inputDS);
                case ActionID.ACTION_WPPWF_MODIFY_EMPLOYEE_GL_RECORDS:
                    return this._ModifyEmployeeGL_Records_WPPWF(inputDS);



                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _FundHeadCreation(DataSet inputDS)
        {
            int nRowAffected = -1, nRowAffected2 = -1;
            bool bError = false, bError2 = false;

            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd2 = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            wppwfDS.Merge(inputDS.Tables[wppwfDS.FundHead.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();

            /////// Using Procedure
            cmd.CommandText = "PRO_WP_FUND_HEAD_CREATION";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd2.CommandText = "PRO_WP_FUND_HEAD_UPDATE";
            cmd2.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            if (cmd2 == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            foreach (WPPWFDS.FundHeadRow row in wppwfDS.FundHead.Rows)
            {
                if (row.IsFundHeadIDNull() == true)
                {

                    ///// Procedure Parameters. Should Clear Parameters when use loop.
                    cmd.Parameters.Clear();

                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }

                    cmd.Parameters.AddWithValue("p_FUNDHEADID", genPK);
                    cmd.Parameters.AddWithValue("p_FUNDHEADNAME", row.HeadName);
                    cmd.Parameters.AddWithValue("p_FUNDPERCENTAGE", row.Percentage);
                    cmd.Parameters.AddWithValue("p_INVESTMENTRATIO", row.InvRatio);

                    if (row.IsRemarksNull() == false) cmd.Parameters.AddWithValue("p_REMARKS", row.Remarks);
                    else cmd.Parameters.AddWithValue("p_REMARKS", DBNull.Value);

                    cmd.Parameters.AddWithValue("p_BENEFICIARY", row.Beneficiary);
                    cmd.Parameters.AddWithValue("p_DUETOSEPARATION", row.DueToSeparation);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }


                }
                else
                {
                    ///// Procedure Parameters. Should Clear Parameters when use loop.
                    cmd2.Parameters.Clear();

                    cmd2.Parameters.AddWithValue("p_FUNDHEADID", row.FundHeadID);
                    cmd2.Parameters.AddWithValue("p_FUNDHEADNAME", row.HeadName);
                    cmd2.Parameters.AddWithValue("p_FUNDPERCENTAGE", row.Percentage);
                    cmd2.Parameters.AddWithValue("p_INVESTMENTRATIO", row.InvRatio);

                    if (row.IsRemarksNull() == false) cmd2.Parameters.AddWithValue("p_REMARKS", row.Remarks);
                    else cmd2.Parameters.AddWithValue("p_REMARKS", DBNull.Value);

                    cmd2.Parameters.AddWithValue("p_BENEFICIARY", row.Beneficiary);
                    cmd2.Parameters.AddWithValue("p_DUETOSEPARATION", row.DueToSeparation);

                    bError = false;
                    nRowAffected2 = -1;
                    nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd2, connDS.DBConnections[0].ConnectionID, ref bError2);

                    if (bError2)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetFundHeadList(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFundHeadList");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            WPPWFDS wppwfDS = new WPPWFDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.FundHead.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }
        private DataSet _DeleteFundHead(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            /////// Using Procedure 
            cmd.CommandText = "PRO_WP_FUND_HEAD_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            ///// Procedure Parameters. Should Clear Parameters when use loop.
            //cmd.Parameters.Clear();

            cmd.Parameters.AddWithValue("p_FUNDHEADID", (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _CreationOrganization(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();
            DataBoolDS boolDS = new DataBoolDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_ORGANIZATION_CREATION";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            ///// Procedure Parameters. Should Clear Parameters when use loop.
            //cmd.Parameters.Clear();

            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmd.Parameters.AddWithValue("p_ORGANIZATIONID", genPK);
            cmd.Parameters.AddWithValue("p_CODE", stringDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_NAME", stringDS.DataStrings[1].StringValue);
            cmd.Parameters.AddWithValue("p_PHONE", stringDS.DataStrings[2].StringValue);
            cmd.Parameters.AddWithValue("p_FAX", stringDS.DataStrings[3].StringValue);
            cmd.Parameters.AddWithValue("p_EMAIL", stringDS.DataStrings[4].StringValue);
            cmd.Parameters.AddWithValue("p_URL", stringDS.DataStrings[5].StringValue);
            cmd.Parameters.AddWithValue("p_ADDRESS", stringDS.DataStrings[6].StringValue);
            cmd.Parameters.AddWithValue("p_REMARKS", stringDS.DataStrings[7].StringValue);
            cmd.Parameters.AddWithValue("p_CONTACTPERSONNAME", stringDS.DataStrings[8].StringValue);
            cmd.Parameters.AddWithValue("p_ISACTIVE", boolDS.DataBools[0].BoolValue);




            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetOrganizationList(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetOrganizationList");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            WPPWFDS wppwfDS = new WPPWFDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Organization.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }
        private DataSet _UpdateOrganization(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();
            DataBoolDS boolDS = new DataBoolDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_ORGANIZATION_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            ///// Procedure Parameters. Should Clear Parameters when use loop.
            //cmd.Parameters.Clear();

            cmd.Parameters.AddWithValue("p_CODE", stringDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_NAME", stringDS.DataStrings[1].StringValue);
            cmd.Parameters.AddWithValue("p_PHONE", stringDS.DataStrings[2].StringValue);
            cmd.Parameters.AddWithValue("p_FAX", stringDS.DataStrings[3].StringValue);
            cmd.Parameters.AddWithValue("p_EMAIL", stringDS.DataStrings[4].StringValue);
            cmd.Parameters.AddWithValue("p_URL", stringDS.DataStrings[5].StringValue);
            cmd.Parameters.AddWithValue("p_ADDRESS", stringDS.DataStrings[6].StringValue);
            cmd.Parameters.AddWithValue("p_REMARKS", stringDS.DataStrings[7].StringValue);
            cmd.Parameters.AddWithValue("p_CONTACTPERSONNAME", stringDS.DataStrings[8].StringValue);
            cmd.Parameters.AddWithValue("p_ISACTIVE", boolDS.DataBools[0].BoolValue);




            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _DeleteOrganization(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            /////// Using Procedure
            cmd.CommandText = "PRO_WP_ORGANIZATION_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            ///// Procedure Parameters. Should Clear Parameters when use loop.
            //cmd.Parameters.Clear();

            cmd.Parameters.AddWithValue("p_CODE", stringDS.DataStrings[0].StringValue);
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _SchemeCreation(DataSet inputDS)
        {
            int nRowAffected = -1, nRowAffected2 = -1;
            bool bError = false, bError2 = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd2 = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            wppwfDS.Merge(inputDS.Tables[wppwfDS.Product.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_PRODUCT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd2.CommandText = "PRO_WP_PRODUCT_UPDATE";
            cmd2.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            if (cmd2 == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            foreach (WPPWFDS.ProductRow row in wppwfDS.Product.Rows)
            {
                if (row.IsProductIDNull() == true)
                {
                    ///// Procedure Parameters. Should Clear Parameters when use loop.
                    cmd.Parameters.Clear();

                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }

                    cmd.Parameters.AddWithValue("p_PRODUCTID", genPK);
                    cmd.Parameters.AddWithValue("p_ORGANIZATIONID", row.OrganizationID);
                    cmd.Parameters.AddWithValue("p_NAME", row.Name);
                    cmd.Parameters.AddWithValue("p_DURATION", row.Duration);
                    cmd.Parameters.AddWithValue("p_PROFITRATE", row.ProfitRate);

                    if (row.IsRemarksNull() == false) cmd.Parameters.AddWithValue("p_REMARKS", row.Remarks);
                    else cmd.Parameters.AddWithValue("p_REMARKS", DBNull.Value);


                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                else
                {
                    ///// Procedure Parameters. Should Clear Parameters when use loop.
                    cmd2.Parameters.Clear();

                    cmd2.Parameters.AddWithValue("p_PRODUCTID", row.ProductID);
                    cmd2.Parameters.AddWithValue("p_ORGANIZATIONID", row.OrganizationID);
                    cmd2.Parameters.AddWithValue("p_NAME", row.Name);
                    cmd2.Parameters.AddWithValue("p_DURATION", row.Duration);
                    cmd2.Parameters.AddWithValue("p_PROFITRATE", row.ProfitRate);

                    if (row.IsRemarksNull() == false) cmd2.Parameters.AddWithValue("p_REMARKS", row.Remarks);
                    else cmd2.Parameters.AddWithValue("p_REMARKS", DBNull.Value);


                    bError2 = false;
                    nRowAffected2 = -1;
                    nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd2, connDS.DBConnections[0].ConnectionID, ref bError2);

                    if (bError2)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }


            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetSchemeListByOrganizationID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSchemeByOrganizationID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["p_ORGANIZATIONID"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            WPPWFDS wppwfDS = new WPPWFDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Product.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }
        private DataSet _DeleteScheme(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            /////// Using Procedure 
            cmd.CommandText = "PRO_WP_PRODUCT_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            ///// Procedure Parameters. Should Clear Parameters when use loop.
            //cmd.Parameters.Clear();

            cmd.Parameters.AddWithValue("p_PRODUCTID", (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _TermsOfSeperationCreate(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            wppwfDS.Merge(inputDS.Tables[wppwfDS.TermsOfSeparation_WPPWF.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_TERMS_OF_SEP_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            long TermsID = IDGenerator.GetNextGenericPK();
            if (TermsID == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmd.Parameters.AddWithValue("p_TERMSID", TermsID);
            cmd.Parameters.AddWithValue("p_NAME", stringDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_EFFECTDATE", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null)); //Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_DESCRIPTION", stringDS.DataStrings[2].StringValue);
            cmd.Parameters.AddWithValue("p_REMARKS", stringDS.DataStrings[3].StringValue);
            cmd.Parameters.AddWithValue("p_PAYMENTPERCENTAGE_PFSELF", Convert.ToDecimal(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_PAYMENTPERCENTAGE_PFCOM", Convert.ToDecimal(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_PROFITPF", Convert.ToDecimal(stringDS.DataStrings[6].StringValue));

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }



            cmd.Dispose();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_TERMS_OF_SEP_WP_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            foreach (WPPWFDS.TermsOfSeparation_WPPWFRow row in wppwfDS.TermsOfSeparation_WPPWF.Rows)
            {
                ///// Procedure Parameters. Should Clear Parameters when use loop.
                cmd.Parameters.Clear();

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_TERMSOFSEP_WPPWFID", genPK);
                cmd.Parameters.AddWithValue("p_TERMSID", TermsID);
                cmd.Parameters.AddWithValue("p_FUNDHEADID", row.FundHeadID);
                cmd.Parameters.AddWithValue("p_PAYMENTPERCENTAGE", row.PaymentPercentage);
                cmd.Parameters.AddWithValue("p_PROFITWPPWF", row.ProfitWPPWF);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetTermsOfSeperationByParameter(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            WPPWFDS wppwfDS = new WPPWFDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "All")
            {

                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetTermsOfSeperation");
                if (cmd == null)
                {
                    Util.LogInfo("Command is null.");
                    return UtilDL.GetCommandNotFound();
                }

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.TermsOfSeparation_PF.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }

                wppwfDS.AcceptChanges();
            }
            else if (stringDS.DataStrings[0].StringValue != "All")
            {
                #region Get Terms of seperation
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetTermsOfSepByTermsID");
                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                if (cmd == null)
                {
                    Util.LogInfo("Command is null.");
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["p_TERMSID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.TermsOfSeparation_PF.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }

                wppwfDS.AcceptChanges();
                #endregion

                #region Get Terms of seperation WPPWF Head
                OleDbCommand cmd2 = DBCommandProvider.GetDBCommand("GetTermsOfSepWPPWFByTermsID");
                OleDbDataAdapter adapter2 = new OleDbDataAdapter();
                adapter2.SelectCommand = cmd2;

                if (cmd2 == null)
                {
                    Util.LogInfo("Command is null.");
                    return UtilDL.GetCommandNotFound();
                }

                cmd2.Parameters["p_TERMSID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

                bool bError2 = false;
                int nRowAffected2 = -1;
                nRowAffected2 = ADOController.Instance.Fill(adapter2, wppwfDS, wppwfDS.TermsOfSeparation_WPPWF.TableName, connDS.DBConnections[0].ConnectionID, ref bError2);


                if (bError2)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }

                wppwfDS.AcceptChanges();
                #endregion
            }



            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }
        private DataSet _TermsOfSeperationUpdate(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            wppwfDS.Merge(inputDS.Tables[wppwfDS.FundHead.TableName], false, MissingSchemaAction.Error);
            wppwfDS.Merge(inputDS.Tables[wppwfDS.TermsOfSeparation_WPPWF.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_TERMS_OF_SEP_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            int TermsID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            cmd.Parameters.AddWithValue("p_TERMSID", stringDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_EFFECTDATE", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null)); //Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_DESCRIPTION", stringDS.DataStrings[2].StringValue);
            cmd.Parameters.AddWithValue("p_REMARKS", stringDS.DataStrings[3].StringValue);
            cmd.Parameters.AddWithValue("p_PAYMENTPERCENTAGE_PFSELF", Convert.ToDecimal(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_PAYMENTPERCENTAGE_PFCOM", Convert.ToDecimal(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_PROFITPF", Convert.ToDecimal(stringDS.DataStrings[6].StringValue));
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }



            cmd.Dispose();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_TERMS_OF_SEP_WP_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            foreach (WPPWFDS.TermsOfSeparation_WPPWFRow row in wppwfDS.TermsOfSeparation_WPPWF.Rows)
            {
                ///// Procedure Parameters. Should Clear Parameters when use loop.
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("p_TERMSOFSEP_WPPWFID", row.TermsOfSep_WPPWFID);
                cmd.Parameters.AddWithValue("p_TERMSID", TermsID);
                cmd.Parameters.AddWithValue("p_FUNDHEADID", row.FundHeadID);
                cmd.Parameters.AddWithValue("p_PAYMENTPERCENTAGE", row.PaymentPercentage);
                cmd.Parameters.AddWithValue("p_PROFITWPPWF", row.ProfitWPPWF);
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _DeleteTermsOfSeperation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            /////// Using Procedure
            cmd.CommandText = "PRO_WP_TERMS_OF_SEP_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            ///// Procedure Parameters. Should Clear Parameters when use loop.
            //cmd.Parameters.Clear();

            cmd.Parameters.AddWithValue("p_TERMSID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet GetActiveEmployeeList(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            WPPWFDS wppwfDS = new WPPWFDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "FundAllocation")
            {
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter adapter = new OleDbDataAdapter();


                cmd = DBCommandProvider.GetDBCommand("GetEmployeeListByYearWise");
                if (cmd == null)
                {
                    Util.LogInfo("Command is null.");
                    return UtilDL.GetCommandNotFound();
                }
                adapter.SelectCommand = cmd;

                cmd.Parameters["p_CurrentYear1"].Value = (object)Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["p_ServiceDateNew1"].Value = (object)Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["p_ServiceLengthNew1"].Value = (object)Convert.ToInt32(stringDS.DataStrings[4].StringValue);

                cmd.Parameters["p_CurrentYear2"].Value = (object)Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["p_ServiceDateNew2"].Value = (object)Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["p_ServiceLengthNew2"].Value = (object)Convert.ToInt32(stringDS.DataStrings[4].StringValue);

                cmd.Parameters["p_CurrentYear3"].Value = (object)Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["p_ServiceDateOld"].Value = (object)Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["p_ServiceLengthOld"].Value = (object)Convert.ToInt32(stringDS.DataStrings[5].StringValue);



                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Fund_Wise_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }

                wppwfDS.AcceptChanges();
            }
            else if (stringDS.DataStrings[0].StringValue == "EmpListforSep_WPPWF")
            {
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter adapter = new OleDbDataAdapter();


                cmd = DBCommandProvider.GetDBCommand("GetEmpListforSep_WPPWF");
                if (cmd == null)
                {
                    Util.LogInfo("Command is null.");
                    return UtilDL.GetCommandNotFound();
                }
                adapter.SelectCommand = cmd;

                cmd.Parameters["p_EmpName"].Value = (object)stringDS.DataStrings[1].StringValue;
                cmd.Parameters["p_EmployeeCode1"].Value = (object)stringDS.DataStrings[2].StringValue;
                cmd.Parameters["p_EmployeeCode2"].Value = (object)stringDS.DataStrings[2].StringValue;
                cmd.Parameters["p_Department1"].Value = (object)stringDS.DataStrings[3].StringValue;
                cmd.Parameters["p_Department2"].Value = (object)stringDS.DataStrings[3].StringValue;
                cmd.Parameters["p_Designation1"].Value = (object)stringDS.DataStrings[4].StringValue;
                cmd.Parameters["p_Designation2"].Value = (object)stringDS.DataStrings[4].StringValue;
                cmd.Parameters["p_Branch1"].Value = (object)stringDS.DataStrings[5].StringValue;
                cmd.Parameters["p_Branch2"].Value = (object)stringDS.DataStrings[5].StringValue;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Fund_Wise_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }

                wppwfDS.AcceptChanges();
            }


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _CreateFundAllocation(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            wppwfDS.Merge(inputDS.Tables[wppwfDS.FundHead.TableName], false, MissingSchemaAction.Error);
            wppwfDS.Merge(inputDS.Tables[wppwfDS.Fund_Wise_Employee.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_FUND_ALLOCATION_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            long FundID = IDGenerator.GetNextGenericPK();
            if (FundID == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmd.Parameters.AddWithValue("p_FUNDDISTRIBUTIONID", FundID);
            cmd.Parameters.AddWithValue("p_FISCALYEAR", stringDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_FUND_AMOUNT", stringDS.DataStrings[2].StringValue);
            cmd.Parameters.AddWithValue("p_ALLOCATIONDATE", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null)); //Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_REMARKS", stringDS.DataStrings[3].StringValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #region Head wise Amount Distribution
            /////// Using Procedure
            cmd.Dispose();
            cmd.CommandText = "PRO_WP_FUND_ALLOCA_BY_FUNDHEAD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            Int32 TotalEmp = wppwfDS.Fund_Wise_Employee.Count;
            decimal GL_Amount = 0;

            foreach (WPPWFDS.FundHeadRow row in wppwfDS.FundHead.Rows)
            {
                ///// Procedure Parameters. Should Clear Parameters when use loop.
                GL_Amount = row.Distributed_Amount / TotalEmp;

                cmd.Parameters.Clear();

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_FUNDDISTRIBUTIONHISTID", genPK);
                cmd.Parameters.AddWithValue("p_FUNDDISTRIBUTIONID", FundID);
                cmd.Parameters.AddWithValue("p_FUNDHEADID", row.FundHeadID);
                cmd.Parameters.AddWithValue("p_PERCENTAGE", row.Percentage);
                cmd.Parameters.AddWithValue("p_DISTRIBUTED_AMOUNT", row.Distributed_Amount);
                cmd.Parameters.AddWithValue("p_INVRATIO", row.InvRatio);
                cmd.Parameters.AddWithValue("p_ABLETOINVEST", row.AbleToInvest);


                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                #region Fund wise employee GL
                /////// Using Procedure
                if (row.Beneficiary == 2 && row.DueToSeparation == true)
                {
                    OleDbCommand cmd_GL = new OleDbCommand();
                    cmd_GL.CommandText = "PRO_WP_EMP_GL_Transaction";
                    cmd_GL.CommandType = CommandType.StoredProcedure;

                    if (cmd_GL == null)
                    {

                        return UtilDL.GetCommandNotFound();
                    }


                    foreach (WPPWFDS.Fund_Wise_EmployeeRow rows in wppwfDS.Fund_Wise_Employee.Rows)
                    {
                        ///// Procedure Parameters. Should Clear Parameters when use loop.
                        cmd_GL.Parameters.Clear();
                        cmd_GL.Parameters.AddWithValue("p_EmployeeID", rows.EmployeeID);
                        cmd_GL.Parameters.AddWithValue("p_GL_Amount", GL_Amount);
                        cmd_GL.Parameters.AddWithValue("p_Event_Date", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
                        cmd_GL.Parameters.AddWithValue("p_FundHeadID", row.FundHeadID);
                        cmd_GL.Parameters.AddWithValue("p_FundDistributionID", FundID);
                        cmd_GL.Parameters.AddWithValue("p_INVID", System.DBNull.Value);
                        cmd_GL.Parameters.AddWithValue("p_EmployeeID_Other_Ref", System.DBNull.Value);
                        cmd_GL.Parameters.AddWithValue("p_Payment_Reference", System.DBNull.Value);
                        cmd_GL.Parameters.AddWithValue("p_Transaction_Type", Convert.ToInt32(Transaction_Type.Principal));
                        
                        cmd_GL.Parameters.AddWithValue("p_Ref_HeadID", DBNull.Value);

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_GL, connDS.DBConnections[0].ConnectionID, ref bError);

                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }


                    }
                }
                #endregion


            }

            #endregion

            #region Fund wise employee tag
            /////// Using Procedure
            cmd.Dispose();
            cmd.CommandText = "PRO_WP_FUND_ALLOCATE_EMP_WISE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            foreach (WPPWFDS.Fund_Wise_EmployeeRow row in wppwfDS.Fund_Wise_Employee.Rows)
            {
                ///// Procedure Parameters. Should Clear Parameters when use loop.
                cmd.Parameters.Clear();

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_FUNDEMPID", genPK);
                cmd.Parameters.AddWithValue("p_FUNDDISTRIBUTIONID", FundID);
                cmd.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetFiscalYearWPPWF(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFiscalYearWPPWF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            WPPWFDS wppwfDS = new WPPWFDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Fund_Distribution.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }
        private DataSet _GetFundAllocationInfo(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Fund Info
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFundInfoByFundID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["p_FUNDDISTRIBUTIONID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            WPPWFDS wppwfDS = new WPPWFDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Fund_Distribution.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();
            #endregion

            #region Get Fund Head wise Distribution Hist
            cmd = DBCommandProvider.GetDBCommand("GetFundDistHistByFundID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter1 = new OleDbDataAdapter();
            adapter1.SelectCommand = cmd;

            cmd.Parameters["p_FUNDID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter1, wppwfDS, wppwfDS.FundHead.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();
            #endregion

            #region Get Fund Wise Employee List
            cmd = DBCommandProvider.GetDBCommand("GetFundWiseEmpByFundID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter2 = new OleDbDataAdapter();
            adapter2.SelectCommand = cmd;

            cmd.Parameters["p_FUNDID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter2, wppwfDS, wppwfDS.Fund_Wise_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _UpdateFundAllocation(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            wppwfDS.Merge(inputDS.Tables[wppwfDS.FundHead.TableName], false, MissingSchemaAction.Error);
            wppwfDS.Merge(inputDS.Tables[wppwfDS.Fund_Wise_Employee.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_FUND_ALLOCATION_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            Int32 FundID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);// IDGenerator.GetNextGenericPK();
            //if (FundID == -1)
            //{
            //    return UtilDL.GetDBOperationFailed();
            //}

            cmd.Parameters.AddWithValue("p_FUNDDISTRIBUTIONID", FundID);
            //cmd.Parameters.AddWithValue("p_FISCALYEAR", stringDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_FUND_AMOUNT", stringDS.DataStrings[2].StringValue);
            cmd.Parameters.AddWithValue("p_ALLOCATIONDATE", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null)); //Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_REMARKS", stringDS.DataStrings[3].StringValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #region Head wise Amount Distribution Update
            cmd.Dispose();

            cmd.CommandText = "PRO_WP_FUND_ALLOCA_FUNDHEAD_UP";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            foreach (WPPWFDS.FundHeadRow row in wppwfDS.FundHead.Rows)
            {
                ///// Procedure Parameters. Should Clear Parameters when use loop.
                cmd.Parameters.Clear();

                //long genPK = IDGenerator.GetNextGenericPK();
                //if (genPK == -1)
                //{
                //    return UtilDL.GetDBOperationFailed();
                //}
                cmd.Parameters.AddWithValue("p_FUNDDISTRIBUTIONHISTID", row.FundDistributionHistID);
                cmd.Parameters.AddWithValue("p_FUNDDISTRIBUTIONID", FundID);
                cmd.Parameters.AddWithValue("p_FUNDHEADID", row.FundHeadID);
                cmd.Parameters.AddWithValue("p_PERCENTAGE", row.Percentage);
                cmd.Parameters.AddWithValue("p_DISTRIBUTED_AMOUNT", row.Amount);
                cmd.Parameters.AddWithValue("p_INVRATIO", row.InvRatio);
                cmd.Parameters.AddWithValue("p_ABLETOINVEST", row.AbleToInvest);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }

            #endregion

            #region Delete Old Employee by FundID
            cmd.Dispose();
            cmd.CommandText = "PRO_WP_FUND_ALLOCAT_EMP_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters.AddWithValue("p_FUNDID", FundID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Fund wise employee tag

            cmd.Dispose();
            cmd.CommandText = "PRO_WP_FUND_ALLOCATED_EMP_UP";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            foreach (WPPWFDS.Fund_Wise_EmployeeRow row in wppwfDS.Fund_Wise_Employee.Rows)
            {
                ///// Procedure Parameters. Should Clear Parameters when use loop.
                cmd.Parameters.Clear();

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_FUNDEMPID", genPK);
                cmd.Parameters.AddWithValue("p_FUNDID", FundID);
                cmd.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _DeleteFundAllocation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            /////// Using Procedure 
            cmd.CommandText = "PRO_WP_FUND_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_FundDistributionID", (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _GetInvestmentableFund(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Fund Info
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetInvestmentableFundInfo");

            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            WPPWFDS wppwfDS = new WPPWFDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.FundHead.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();
            #endregion


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _CreateInvestment(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            //WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            //wppwfDS.Merge(inputDS.Tables[wppwfDS.Investment.TableName], false, MissingSchemaAction.Error);
            //wppwfDS.AcceptChanges();

            #region Create New Investment
            cmd.CommandText = "PRO_WP_INVESTMENT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            long InvID = IDGenerator.GetNextGenericPK();
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", InvID);
            cmd.Parameters.AddWithValue("p_InvestmentDate", DateTime.ParseExact(stringDS.DataStrings[0].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_MaturityDate", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_ProductID", Convert.ToInt32(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_PrincipalAmount", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_Profit", Convert.ToDecimal(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_Duration", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[6].StringValue);
            cmd.Parameters.AddWithValue("p_Tax", Convert.ToDecimal(stringDS.DataStrings[7].StringValue));
            cmd.Parameters.AddWithValue("p_ExciseDuty", Convert.ToDecimal(stringDS.DataStrings[8].StringValue));
            cmd.Parameters.AddWithValue("p_OthersCost", Convert.ToDecimal(stringDS.DataStrings[9].StringValue));
            cmd.Parameters.AddWithValue("p_Buyer", stringDS.DataStrings[10].StringValue);
            cmd.Parameters.AddWithValue("p_Total_Instrument", stringDS.DataStrings[12].StringValue);

            cmd.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[13].StringValue);       // WALI :: 13-Jan-2015

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_CREATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Update Last Fund Allocation
            cmd.Dispose();      
            cmd.CommandText = "PRO_WP_FUND_ALLOCAT_UPD_INUSE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters.AddWithValue("p_FundDistributionID", 0);
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_CREATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetInvestmentList(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            OleDbCommand cmd = new OleDbCommand();

            if (stringDS.DataStrings[0].StringValue == "NotSettled") cmd = DBCommandProvider.GetDBCommand("GetInvestmentListNotSettled");
            else if (stringDS.DataStrings[0].StringValue == "All") cmd = DBCommandProvider.GetDBCommand("GetInvestmentListAll");


            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            WPPWFDS wppwfDS = new WPPWFDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_INVESTMENT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }
        private DataSet _GetInvestmentListByInvID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            WPPWFDS wppwfDS = new WPPWFDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            #region  Get Investment List By InvID
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetInvestmentListByInvID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["p_INVID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();

            #endregion

            #region  Get Investment Fund Details By InvID
            cmd.Parameters.Clear();
            cmd.Dispose();

            cmd = DBCommandProvider.GetDBCommand("GetInvFundDetailsByInvID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter2 = new OleDbDataAdapter();
            adapter2.SelectCommand = cmd;

            cmd.Parameters["p_INVID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter2, wppwfDS, wppwfDS.FundHead.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();

            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }
        private DataSet _UpdateInvestment(DataSet inputDS)
        {
          
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
     

            #region Update Investment
            cmd.CommandText = "PRO_WP_INVESTMENT_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            Int32 InvID = -1;
            InvID = Convert.ToInt32(stringDS.DataStrings[11].StringValue);
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[11].StringValue));
            cmd.Parameters.AddWithValue("p_InvestmentDate", DateTime.ParseExact(stringDS.DataStrings[0].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_MaturityDate", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_ProductID", Convert.ToInt32(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_PrincipalAmount", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_Profit", Convert.ToDecimal(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_Duration", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[6].StringValue);
            cmd.Parameters.AddWithValue("p_Tax", Convert.ToDecimal(stringDS.DataStrings[7].StringValue));
            cmd.Parameters.AddWithValue("p_ExciseDuty", Convert.ToDecimal(stringDS.DataStrings[8].StringValue));
            cmd.Parameters.AddWithValue("p_OthersCost", Convert.ToDecimal(stringDS.DataStrings[9].StringValue));
            cmd.Parameters.AddWithValue("p_Buyer", stringDS.DataStrings[10].StringValue);
            cmd.Parameters.AddWithValue("p_Total_Instrument", stringDS.DataStrings[12].StringValue);

            cmd.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[13].StringValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_UPDATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;

        }
        private DataSet _FreezeInvestment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            /////// Using Procedure 
            cmd.CommandText = "PRO_WP_FREEZE_INVESTMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            ///// Procedure Parameters. Should Clear Parameters when use loop.
            //cmd.Parameters.Clear();

            cmd.Parameters.AddWithValue("p_INVID", (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FREEZE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _InvestmentSettlementCreate(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            // WALI :: 06-May-2015
            DateTime settlementDate = DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null);
            DateTime provisionDate = new DateTime(settlementDate.Year, settlementDate.Month, 2);  // Second day of 'Settlement Month'

            #region Investment Settlement
            cmd.CommandText = "PRO_WP_INV_SETTLEMENT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            Int32 InvID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_SETTLEMENT_STATUS", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
            //cmd.Parameters.AddWithValue("p_SETTLEMENTDATE", DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_SETTLEMENTDATE", settlementDate);    // WALI :: 07-May-2015
            cmd.Parameters.AddWithValue("p_NetProfit", Convert.ToDecimal(stringDS.DataStrings[5].StringValue));

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_INVESTMENT_SETTLEMENT_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Provision For Mismatch Profit
            if (Convert.ToDecimal(stringDS.DataStrings[3].StringValue) != 0)
            {
                cmd.Dispose();
                //cmd.CommandText = "PRO_WP_PROVISION_CREATE";
                cmd.CommandText = "PRO_WP_PROVISION_CREATE_ST";    // WALI :: 09-Apr-2015
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
                //cmd.Parameters.AddWithValue("p_ProvisionDate", DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null));
                cmd.Parameters.AddWithValue("p_ProvisionDate", provisionDate);     // WALI :: 07-May-2015
                cmd.Parameters.AddWithValue("p_Day_P", 0);
                cmd.Parameters.AddWithValue("p_Profit_P", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
                //cmd.Parameters.AddWithValue("p_Tax_P", 0);        // Commented :: WALI :: 12-Apr-2015
                //cmd.Parameters.AddWithValue("p_Exciseduty_P", 0); // Commented :: WALI :: 12-Apr-2015
                //cmd.Parameters.AddWithValue("p_Others_P", 0);     // Commented :: WALI :: 12-Apr-2015
                cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[4].StringValue);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_INVESTMENT_SETTLEMENT_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _ExtraOrdinaryBeneficiaryCreate(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            wppwfDS.Merge(inputDS.Tables[wppwfDS.Wp_ExtraOrdinary_Beneficiary.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_EXTRA_INCOMECOST_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            long ExtraIncomeCostID = IDGenerator.GetNextGenericPK();
            if (ExtraIncomeCostID == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmd.Parameters.AddWithValue("p_EXTRAINCOMECOSTID", ExtraIncomeCostID);
            cmd.Parameters.AddWithValue("p_POSTINGCODE", stringDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_INCOME", stringDS.DataStrings[1].StringValue);
            cmd.Parameters.AddWithValue("p_COST", stringDS.DataStrings[2].StringValue);
            cmd.Parameters.AddWithValue("p_POSTINGDATE", DateTime.ParseExact(stringDS.DataStrings[3].StringValue, "dd-MM-yyyy", null)); //Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_REMARKS", stringDS.DataStrings[4].StringValue);


            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #region Employee tag
            /////// Using Procedure
            cmd.Dispose();
            cmd.CommandText = "PRO_WP_EXTRAORDINAR_EMP_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            foreach (WPPWFDS.Wp_ExtraOrdinary_BeneficiaryRow row in wppwfDS.Wp_ExtraOrdinary_Beneficiary.Rows)
            {
                ///// Procedure Parameters. Should Clear Parameters when use loop.
                cmd.Parameters.Clear();

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_EXTRABENEFICIARYID", genPK);
                cmd.Parameters.AddWithValue("p_EXTRAINCOMECOSTID", ExtraIncomeCostID);
                cmd.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetExtraordinaryIncomeCost(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetExtraordinaryIncomeCost");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            WPPWFDS wppwfDS = new WPPWFDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Wp_ExtraOrdinaryIncomeCost.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }
        private DataSet _GetExtraordinaryIncomeCostBeneficiary(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Fund Info
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetExtraIncomeCostByID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["p_ExtraIncomeCostID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            WPPWFDS wppwfDS = new WPPWFDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Wp_ExtraOrdinaryIncomeCost.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();
            #endregion

            #region Get Fund Wise Employee List
            cmd = DBCommandProvider.GetDBCommand("GetExtraordinaryEmpByID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter2 = new OleDbDataAdapter();
            adapter2.SelectCommand = cmd;

            cmd.Parameters["p_ExtraIncomeCostID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter2, wppwfDS, wppwfDS.Wp_ExtraOrdinary_Beneficiary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _ExtraOrdinaryBeneficiaryUpdate(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            wppwfDS.Merge(inputDS.Tables[wppwfDS.Wp_ExtraOrdinary_Beneficiary.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();


            /////// Using Procedure
            cmd.CommandText = "PRO_WP_EXTRA_INCOMECOST_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            long ExtraIncomeCostID = Convert.ToInt32(stringDS.DataStrings[0].StringValue); //IDGenerator.GetNextGenericPK();
            //if (ExtraIncomeCostID == -1)
            //{
            //    return UtilDL.GetDBOperationFailed();
            //}

            cmd.Parameters.AddWithValue("p_EXTRAINCOMECOSTID", ExtraIncomeCostID);
            cmd.Parameters.AddWithValue("p_INCOME", stringDS.DataStrings[1].StringValue);
            cmd.Parameters.AddWithValue("p_COST", stringDS.DataStrings[2].StringValue);
            cmd.Parameters.AddWithValue("p_POSTINGDATE", DateTime.ParseExact(stringDS.DataStrings[3].StringValue, "dd-MM-yyyy", null)); //Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_REMARKS", stringDS.DataStrings[4].StringValue);


            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #region Related Employee Delete
            /////// Using Procedure
            cmd.Dispose();
            cmd.CommandText = "PRO_WP_EXTRAORDINAR_EMP_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            cmd.Parameters.AddWithValue("p_EXTRAINCOMECOSTID", ExtraIncomeCostID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }




            #endregion

            #region Employee tag
            /////// Using Procedure
            cmd.Dispose();
            cmd.CommandText = "PRO_WP_EXTRAORDINAR_EMP_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }


            foreach (WPPWFDS.Wp_ExtraOrdinary_BeneficiaryRow row in wppwfDS.Wp_ExtraOrdinary_Beneficiary.Rows)
            {
                ///// Procedure Parameters. Should Clear Parameters when use loop.
                cmd.Parameters.Clear();

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_EXTRABENEFICIARYID", genPK);
                cmd.Parameters.AddWithValue("p_EXTRAINCOMECOSTID", ExtraIncomeCostID);
                cmd.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _ExtraOrdinaryBeneficiaryDelete(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            /////// Using Procedure 
            cmd.CommandText = "PRO_WP_EXTRA_INCOMECOST_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            ///// Procedure Parameters. Should Clear Parameters when use loop.
            //cmd.Parameters.Clear();

            cmd.Parameters.AddWithValue("p_EXTRAINCOMECOSTID", (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _ProvisionCreation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            /////// Using Procedure 
            cmd.CommandText = "PRO_WP_PROVISION_PROCESS";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            ///// Procedure Parameters. Should Clear Parameters when use loop.
            //cmd.Parameters.Clear();

            string ProvisionDate = stringDS.DataStrings[0].StringValue + "/01/" + stringDS.DataStrings[1].StringValue + " 12:00:01 AM";

            cmd.Parameters.AddWithValue("p_ProvisionDate", (object)Convert.ToDateTime(ProvisionDate));
            cmd.Parameters.AddWithValue("p_LoginID", (object)stringDS.DataStrings[2].StringValue);
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _GetInvestmentProvisionList(DataSet inputDS)
        {
            //DataStringDS stringDS = new DataStringDS();
            //ErrorDS errDS = new ErrorDS();
            //DBConnectionDS connDS = new DBConnectionDS();

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();

            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();

            //#region Get Fund Info

            //OleDbDataAdapter adapter = new OleDbDataAdapter();
            //OleDbCommand cmd = new OleDbCommand();

            //if (stringDS.DataStrings[0].StringValue == "ProvisionedMaxDate")
            //{
            //    cmd = DBCommandProvider.GetDBCommand("InvProvisionMaxDate");
            //    adapter.SelectCommand = cmd;
            //}
            //else
            //{
            //    cmd = DBCommandProvider.GetDBCommand("InvProvisionList");
            //    if (cmd == null)
            //    {
            //        Util.LogInfo("Command is null.");
            //        return UtilDL.GetCommandNotFound();
            //    }


            //    adapter.SelectCommand = cmd;

            //    string ProvisionDate = stringDS.DataStrings[0].StringValue + "/01/" + stringDS.DataStrings[1].StringValue + " 12:00:01 AM";
            //    cmd.Parameters["p_ProvisionDate"].Value = (object)Convert.ToDateTime(ProvisionDate);
            //}



            //WPPWFDS wppwfDS = new WPPWFDS();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Wp_Inv_Provision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}
            //wppwfDS.AcceptChanges();
            //cmd.Parameters.Clear();
            //cmd.Dispose();
            //#endregion

            //// now create the packet
            //errDS.Clear();
            //errDS.AcceptChanges();

            //DataSet returnDS = new DataSet();

            //returnDS.Merge(wppwfDS);
            //returnDS.Merge(errDS);
            //returnDS.AcceptChanges();
            //return returnDS;


            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetInvestmentProvisionList_WPPWF");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_INVESTMENT_PROVISION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _GetEmployeeGLInfo(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("EmployeeGLInfo");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["p_EmployeeID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            WPPWFDS wppwfDS = new WPPWFDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Employee_Gl.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _GetFundHeadWiseInfo(DataSet inputDS)
        {
            #region OLD :: Commented :: WALI :: 31-Oct-2014
            //DataStringDS stringDS = new DataStringDS();
            //ErrorDS errDS = new ErrorDS();
            //DBConnectionDS connDS = new DBConnectionDS();

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();

            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();

            //#region Get Government Fund Info
            //OleDbCommand cmd = new OleDbCommand();
            //OleDbDataAdapter adapter = new OleDbDataAdapter();

            //if (stringDS.DataStrings[1].StringValue == "0")
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetFundHeadBelanceAllYear");
            //    if (cmd == null)
            //    {
            //        Util.LogInfo("Command is null.");
            //        return UtilDL.GetCommandNotFound();
            //    }


            //    adapter.SelectCommand = cmd;

            //    cmd.Parameters["p_FUNDHEADID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            //}
            //else
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetFundHeadBelanceByYear");
            //    if (cmd == null)
            //    {
            //        Util.LogInfo("Command is null.");
            //        return UtilDL.GetCommandNotFound();
            //    }


            //    adapter.SelectCommand = cmd;

            //    cmd.Parameters["p_FUNDHEADID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            //    cmd.Parameters["p_FUNDDISTRIBUTIONID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            //}
            //WPPWFDS wppwfDS = new WPPWFDS();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.FundHead.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}
            //wppwfDS.AcceptChanges();
            //cmd.Parameters.Clear();
            //cmd.Dispose();
            //#endregion

            //// now create the packet
            //errDS.Clear();
            //errDS.AcceptChanges();

            //DataSet returnDS = new DataSet();

            //returnDS.Merge(wppwfDS);
            //returnDS.Merge(errDS);
            //returnDS.AcceptChanges();
            //return returnDS;
            #endregion

            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Fund Info
            OleDbCommand cmdFund = new OleDbCommand();
            OleDbDataAdapter adapterFund = new OleDbDataAdapter();

            cmdFund = DBCommandProvider.GetDBCommand("GetFundHeadBelanceByYear");
            if (cmdFund == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            cmdFund.Parameters["p_FUNDDISTRIBUTIONID1"].Value = (object)Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmdFund.Parameters["p_FUNDDISTRIBUTIONID2"].Value = (object)Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmdFund.Parameters["p_FUNDDISTRIBUTIONID3"].Value = (object)Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmdFund.Parameters["p_FUNDDISTRIBUTIONID4"].Value = (object)Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmdFund.Parameters["p_FUNDHEADID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);            
            adapterFund.SelectCommand = cmdFund;

            bool bErrorFund = false;
            int nRowAffectedFund = -1;
            nRowAffectedFund = ADOController.Instance.Fill(adapterFund, wppwfDS, wppwfDS.FundHead.TableName, connDS.DBConnections[0].ConnectionID, ref bErrorFund);
            if (bErrorFund)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_WISE_BALANCE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();
            cmdFund.Dispose();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetFundBeneficiaryList(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            if (integerDS.DataIntegers[1].IntegerValue == 100 || integerDS.DataIntegers[1].IntegerValue == 101 || integerDS.DataIntegers[1].IntegerValue == 104 || integerDS.DataIntegers[1].IntegerValue == 107)   // For 'Lapses & Forfeiture' and other
            {
                if (stringDS.DataStrings[0].StringValue == "All")
                {
                    cmd = DBCommandProvider.GetDBCommand("GetBeneficiary_All_For_LapsesAmount");
                }
                else
                {
                    cmd = DBCommandProvider.GetDBCommand("GetBeneficiary_FiscalYear_For_LapsesAmount");
                    cmd.Parameters["FundDistributionID"].Value = integerDS.DataIntegers[0].IntegerValue;
                }
            }
            else               // For other Heads
            {
                if (stringDS.DataStrings[0].StringValue == "All")
                {
                    cmd = DBCommandProvider.GetDBCommand("GetBeneficiary_All_HeadWise");
                    cmd.Parameters["FundDistributionID"].Value = integerDS.DataIntegers[0].IntegerValue;
                    cmd.Parameters["FundHeadID"].Value = integerDS.DataIntegers[1].IntegerValue;
                }
                else
                {
                    cmd = DBCommandProvider.GetDBCommand("GetBeneficiary_FiscalYear_HeadWise");
                    cmd.Parameters["FundDistributionID1"].Value = integerDS.DataIntegers[0].IntegerValue;
                    cmd.Parameters["FundHeadID"].Value = integerDS.DataIntegers[1].IntegerValue;
                    cmd.Parameters["FundDistributionID2"].Value = integerDS.DataIntegers[0].IntegerValue;
                }
            }

            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Fund_Wise_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_WPPWF_FUND_PAYMENT_BENEFICIARY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _HeadWisePaymentCreate(DataSet inputDS)
        {
            #region OLD :: Commented :: WALI :: 31-Oct-2014
            //int nRowAffected = -1;
            //bool bError = false;
            //ErrorDS errDS = new ErrorDS();
            //OleDbCommand cmd = new OleDbCommand();
            //DataSet returnDS = new DataSet();
            //DataStringDS stringDS = new DataStringDS();
            //DBConnectionDS connDS = new DBConnectionDS();

            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();


            //if (stringDS.DataStrings[5].StringValue == "1")
            //{
            //    cmd.CommandText = "PRO_WP_FUND_PAYMENT_CREATE";
            //    cmd.CommandType = CommandType.StoredProcedure;

            //    if (cmd == null)
            //    {
            //        return UtilDL.GetCommandNotFound();
            //    }

            //    long genPK = IDGenerator.GetNextGenericPK();
            //    if (genPK == -1)
            //    {
            //        return UtilDL.GetDBOperationFailed();
            //    }

            //    cmd.Parameters.AddWithValue("p_FUND_PAYMENTID", genPK);
            //    cmd.Parameters.AddWithValue("p_FUNDHEADID", stringDS.DataStrings[0].StringValue);
            //    cmd.Parameters.AddWithValue("p_AMOUNT", stringDS.DataStrings[1].StringValue);
            //    cmd.Parameters.AddWithValue("p_PAYMENT_REFERENCE", stringDS.DataStrings[3].StringValue);
            //    cmd.Parameters.AddWithValue("p_PAYMENTDATE", DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null));
            //    cmd.Parameters.AddWithValue("p_REMARKS", stringDS.DataStrings[4].StringValue);
            //}
            //else if (stringDS.DataStrings[4].StringValue == "2")
            //{

            //}



            //bError = false;
            //nRowAffected = -1;
            //nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.ACTION_HEAD_WISE_PAYMENT_CREATE.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}


            //cmd.Dispose();

            //errDS.Clear();
            //errDS.AcceptChanges();
            //return errDS;
            #endregion

            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            wppwfDS.Merge(inputDS.Tables[wppwfDS.WPPWF_Balance.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_WP_FUND_PAYMENT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_GL = new OleDbCommand();
            cmd_GL.CommandText = "PRO_WP_EMP_GL_Transaction";
            cmd_GL.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmd_GL == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1) return UtilDL.GetDBOperationFailed();

            #region Create Fund Payment
            cmd.Parameters.AddWithValue("p_FundDistributionID", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_FundHeadID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_Payment_Amount", Convert.ToDecimal(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_Payment_Reference", stringDS.DataStrings[3].StringValue);
            cmd.Parameters.AddWithValue("p_Payment_Date", DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[4].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_HEAD_WISE_PAYMENT_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create GL Entry
            if (wppwfDS.WPPWF_Balance.Rows.Count > 0)
            {
                decimal totalPayent = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                decimal perEmployeePayment = totalPayent / wppwfDS.WPPWF_Balance.Rows.Count;

                foreach (WPPWFDS.WPPWF_BalanceRow row in wppwfDS.WPPWF_Balance.Rows)
                {
                    #region Positive Entry
                    cmd_GL.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    //cmd_GL.Parameters.AddWithValue("p_GL_Amount", perEmployeePayment);
                    cmd_GL.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);    // WALI :: 06-Apr-2015 :: This amount is brought from UI
                    cmd_GL.Parameters.AddWithValue("p_Event_Date", DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null));
                    cmd_GL.Parameters.AddWithValue("p_FundHeadID", DBNull.Value);
                    cmd_GL.Parameters.AddWithValue("p_FundDistributionID", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
                    cmd_GL.Parameters.AddWithValue("p_INVID", DBNull.Value);
                    cmd_GL.Parameters.AddWithValue("p_EmployeeID_Other_Ref", DBNull.Value);
                    cmd_GL.Parameters.AddWithValue("p_Payment_Reference", stringDS.DataStrings[3].StringValue);
                    cmd_GL.Parameters.AddWithValue("p_Transaction_Type", Convert.ToInt32(Transaction_Type.Principal));

                    cmd_GL.Parameters.AddWithValue("p_Ref_HeadID", DBNull.Value);

                    bool bError_GL = false;
                    int nRowAffected_GL = -1;
                    nRowAffected_GL = ADOController.Instance.ExecuteNonQuery(cmd_GL, connDS.DBConnections[0].ConnectionID, ref bError_GL);
                    cmd_GL.Parameters.Clear();

                    if (bError_GL)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_HEAD_WISE_PAYMENT_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion

                    #region Negative Entry
                    cmd_GL.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    //cmd_GL.Parameters.AddWithValue("p_GL_Amount", (-1 * perEmployeePayment));
                    cmd_GL.Parameters.AddWithValue("p_GL_Amount", (-1 * row.GL_Amount));    // WALI :: 06-Apr-2015 :: This amount is brought from UI
                    cmd_GL.Parameters.AddWithValue("p_Event_Date", DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null));
                    cmd_GL.Parameters.AddWithValue("p_FundHeadID", DBNull.Value);
                    cmd_GL.Parameters.AddWithValue("p_FundDistributionID", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
                    cmd_GL.Parameters.AddWithValue("p_INVID", DBNull.Value);
                    cmd_GL.Parameters.AddWithValue("p_EmployeeID_Other_Ref", DBNull.Value);
                    cmd_GL.Parameters.AddWithValue("p_Payment_Reference", stringDS.DataStrings[3].StringValue);
                    cmd_GL.Parameters.AddWithValue("p_Transaction_Type", Convert.ToInt32(Transaction_Type.Principal));

                    cmd_GL.Parameters.AddWithValue("p_Ref_HeadID", DBNull.Value);

                    bool bError_GL_NG = false;
                    int nRowAffected_GL_NG = -1;
                    nRowAffected_GL_NG = ADOController.Instance.ExecuteNonQuery(cmd_GL, connDS.DBConnections[0].ConnectionID, ref bError_GL_NG);
                    cmd_GL.Parameters.Clear();

                    if (bError_GL_NG)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_HEAD_WISE_PAYMENT_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion
                }
            }
            #endregion

            #region Update Fund Allocation
            OleDbCommand cmd_Fund = new OleDbCommand();
            //cmd_Fund.CommandText = "PRO_WP_UPD_LAST_FUND_ALLOCAT";
            cmd_Fund.CommandText = "PRO_WP_FUND_ALLOCAT_UPD_INUSE";
            cmd_Fund.CommandType = CommandType.StoredProcedure;

            if (cmd_Fund == null) return UtilDL.GetCommandNotFound();
            cmd_Fund.Parameters.AddWithValue("p_FundDistributionID", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Fund, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_HEAD_WISE_PAYMENT_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetWPPWFbalanceEmpWise(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get WPPWF Balance Employee wise
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            cmd = DBCommandProvider.GetDBCommand("GetWPPWFbalanceByEmp_TermsID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            adapter.SelectCommand = cmd;

            cmd.Parameters["TermsOfSeparationID1"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["TermsOfSeparationID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["Transaction_Type1"].Value = Convert.ToInt32(Transaction_Type.Principal);
            cmd.Parameters["Transaction_Type2"].Value = Convert.ToInt32(Transaction_Type.Profit);
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

            WPPWFDS wppwfDS = new WPPWFDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.WPPWF_Balance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_WPPWF_BALANCE_EMP_WISE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetBeneficiaryInfo_All(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get All Beneficiary List
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            cmd = DBCommandProvider.GetDBCommand("GetBeneficiaryInfo_All");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            adapter.SelectCommand = cmd;
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

            WPPWFDS wppwfDS = new WPPWFDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.WPPWF_Balance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_ALL_BENEFICIARY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _CreateEmployeeSeperation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            wppwfDS.Merge(inputDS.Tables[wppwfDS.WPPWF_Balance.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();

            OleDbCommand cmd_GL = new OleDbCommand();
            cmd_GL.CommandText = "PRO_WP_EMP_GL_Transaction";
            cmd_GL.CommandType = CommandType.StoredProcedure;

            if (cmd_GL == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            long employeeID = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
            decimal payableProfit = Convert.ToDecimal(stringDS.DataStrings[5].StringValue);
            decimal paidProfit = Convert.ToDecimal(stringDS.DataStrings[3].StringValue);
            decimal payablePrincipal = Convert.ToDecimal(stringDS.DataStrings[7].StringValue);
            decimal paidPrincipal = Convert.ToDecimal(stringDS.DataStrings[2].StringValue);

            #region Add Record to WP_Seperation Table [WALI :: 11-Jan-2015]
            OleDbCommand cmd_Sep = new OleDbCommand();
            cmd_Sep.CommandText = "PRO_WP_SEPERATION_CREATE";
            cmd_Sep.CommandType = CommandType.StoredProcedure;

            if (cmd_Sep == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd_Sep.Parameters.AddWithValue("p_EmployeeID", employeeID);
            cmd_Sep.Parameters.AddWithValue("p_Payment_Date", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
            cmd_Sep.Parameters.AddWithValue("p_Payment_Reference", stringDS.DataStrings[4].StringValue);
            //cmd_Sep.Parameters.AddWithValue("p_Payment_Status", true);
            cmd_Sep.Parameters.AddWithValue("p_Payment_Status", false);     // WALI :: 06-Apr-2015
            cmd_Sep.Parameters.AddWithValue("p_Actual_Payment_Date", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
            cmd_Sep.Parameters.AddWithValue("p_Principal", payablePrincipal);
            cmd_Sep.Parameters.AddWithValue("p_Profit", payableProfit);
            cmd_Sep.Parameters.AddWithValue("p_Paid_Principal", paidPrincipal);
            cmd_Sep.Parameters.AddWithValue("p_Paid_Profit", paidProfit);
            cmd_Sep.Parameters.AddWithValue("p_Paid_By", stringDS.DataStrings[6].StringValue);
             
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Sep, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd_Sep.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_SEPERATION_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Employee Seperation [Principal]
            if (paidPrincipal != 0)
            { 
                cmd_GL.Parameters.AddWithValue("p_EmployeeID", employeeID);
                cmd_GL.Parameters.AddWithValue("p_GL_Amount", (-1 * paidPrincipal));
                cmd_GL.Parameters.AddWithValue("p_Event_Date", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
                cmd_GL.Parameters.AddWithValue("p_FundHeadID", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_FundDistributionID", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_INVID", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_EmployeeID_Other_Ref", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_Payment_Reference", stringDS.DataStrings[4].StringValue);
                cmd_GL.Parameters.AddWithValue("p_Transaction_Type", Convert.ToInt32(Transaction_Type.Principal));
                
                cmd_GL.Parameters.AddWithValue("p_Ref_HeadID", DBNull.Value);

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_GL, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_GL.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_SEPERATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Employee Seperation [Profit]
            if(paidProfit != 0)
            {
                cmd_GL.Parameters.AddWithValue("p_EmployeeID", employeeID);
                cmd_GL.Parameters.AddWithValue("p_GL_Amount", (-1 * paidProfit));
                cmd_GL.Parameters.AddWithValue("p_Event_Date", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
                cmd_GL.Parameters.AddWithValue("p_FundHeadID", 100);
                cmd_GL.Parameters.AddWithValue("p_FundDistributionID", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_INVID", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_EmployeeID_Other_Ref", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_Payment_Reference", stringDS.DataStrings[4].StringValue);
                cmd_GL.Parameters.AddWithValue("p_Transaction_Type", Convert.ToInt32(Transaction_Type.Profit));
                
                cmd_GL.Parameters.AddWithValue("p_Ref_HeadID", DBNull.Value); 

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_GL, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_GL.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_SEPERATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            //Lapses and Forfeiture entry for Profit 
            if (paidProfit < payableProfit)
            {
                #region Rest of profit entry to GL as paid to SEPERATED Employee [To Equalize the amount he/she is charged]
                cmd_GL.Parameters.AddWithValue("p_EmployeeID", employeeID);
                cmd_GL.Parameters.AddWithValue("p_GL_Amount", (-1 * (payableProfit-paidProfit)));
                cmd_GL.Parameters.AddWithValue("p_Event_Date", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
                cmd_GL.Parameters.AddWithValue("p_FundHeadID", 104);    // HeadID for 'Lapses & Forfeiture'
                cmd_GL.Parameters.AddWithValue("p_FundDistributionID", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_INVID", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_EmployeeID_Other_Ref", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_Payment_Reference", stringDS.DataStrings[4].StringValue);
                cmd_GL.Parameters.AddWithValue("p_Transaction_Type", Convert.ToInt32(Transaction_Type.Profit));
                
                cmd_GL.Parameters.AddWithValue("p_Ref_HeadID", 100);
 
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_GL, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_GL.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_SEPERATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                #region Add the difference amount into Head_GL for 'Lapses'
                OleDbCommand cmd_LF = new OleDbCommand();
                cmd_LF.CommandText = "PRO_WP_FUNDHEAD_GL_ADD_LAPSES";
                cmd_LF.CommandType = CommandType.StoredProcedure;

                if (cmd_LF == null) return UtilDL.GetCommandNotFound();

                cmd_LF.Parameters.AddWithValue("p_FundHeadID", 104);    // HeadID for 'Lapses and Forfeiture'
                cmd_LF.Parameters.AddWithValue("p_Amount", (payableProfit - paidProfit));
                cmd_LF.Parameters.AddWithValue("p_Event_Date", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
                cmd_LF.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[4].StringValue);
                cmd_LF.Parameters.AddWithValue("p_Ref_EmployeeID", employeeID);
                
                cmd_LF.Parameters.AddWithValue("p_Ref_HeadID", 100);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_LF, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_LF.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_SEPERATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion
            }


            //Lapses and Forfeiture entry for Principal
            if (paidPrincipal < payablePrincipal)
            {
                #region Rest of profit entry to GL as paid to SEPERATED Employee [To Equalize the amount he/she is charged]
                cmd_GL.Parameters.AddWithValue("p_EmployeeID", employeeID);
                cmd_GL.Parameters.AddWithValue("p_GL_Amount", (-1 * (payablePrincipal - paidPrincipal)));
                cmd_GL.Parameters.AddWithValue("p_Event_Date", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
                cmd_GL.Parameters.AddWithValue("p_FundHeadID", 104);    // HeadID for 'Lapses & Forfeiture'
                cmd_GL.Parameters.AddWithValue("p_FundDistributionID", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_INVID", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_EmployeeID_Other_Ref", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_Payment_Reference", stringDS.DataStrings[4].StringValue);
                cmd_GL.Parameters.AddWithValue("p_Transaction_Type", Convert.ToInt32(Transaction_Type.Profit));
                
                cmd_GL.Parameters.AddWithValue("p_Ref_HeadID", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_GL, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_GL.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_SEPERATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                #region Add the difference amount into Head_GL for 'Lapses'
                OleDbCommand cmd_LF = new OleDbCommand();
                cmd_LF.CommandText = "PRO_WP_FUNDHEAD_GL_ADD_LAPSES";
                cmd_LF.CommandType = CommandType.StoredProcedure;

                if (cmd_LF == null) return UtilDL.GetCommandNotFound();

                cmd_LF.Parameters.AddWithValue("p_FundHeadID", 104);    // HeadID for 'Lapses and Forfeiture'
                cmd_LF.Parameters.AddWithValue("p_Amount", (payablePrincipal - paidPrincipal));
                cmd_LF.Parameters.AddWithValue("p_Event_Date", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
                cmd_LF.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[4].StringValue);
                cmd_LF.Parameters.AddWithValue("p_Ref_EmployeeID", employeeID);
                
                cmd_LF.Parameters.AddWithValue("p_Ref_HeadID", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_LF, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_LF.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_SEPERATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion
            }



            #region Fund Journal Add for 'PAYABLE TO LSC'
            if (stringDS.DataStrings[6].StringValue == "Donor")
            {
                OleDbCommand cmd_Fund = new OleDbCommand();
                cmd_Fund.CommandText = "PRO_WP_DONOR_FUNDHEAD_GL_ADD"; //"PRO_WP_FUNDHEAD_GL_ADD";
                cmd_Fund.CommandType = CommandType.StoredProcedure;

                if (cmd_Fund == null) return UtilDL.GetCommandNotFound();

                cmd_Fund.Parameters.AddWithValue("p_FundHeadID", 102);
                cmd_Fund.Parameters.AddWithValue("p_Amount", paidProfit + paidPrincipal);
                cmd_Fund.Parameters.AddWithValue("p_Event_Date", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
                cmd_Fund.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[4].StringValue);
                cmd_Fund.Parameters.AddWithValue("p_Ref_EmployeeID", employeeID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Fund, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Fund.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_SEPERATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _CreateTermsOfSeperation_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            wppwfDS.Merge(inputDS.Tables[wppwfDS.TermsOfSeparation_WPPWF.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_WP_TERMS_OF_SEPERATION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (WPPWFDS.TermsOfSeparation_WPPWFRow row in wppwfDS.TermsOfSeparation_WPPWF.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_TermsOfSeparationID", genPK);
                cmd.Parameters.AddWithValue("p_Terms", row.Terms);
                cmd.Parameters.AddWithValue("p_Principal_P", row.Principal_P);
                cmd.Parameters.AddWithValue("p_Profit_P", row.Profit_P);

                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TERMS_OF_SEPARATION_CREATE_WPPWF.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }            

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _UpdateTermsOfSeperation_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            wppwfDS.Merge(inputDS.Tables[wppwfDS.TermsOfSeparation_WPPWF.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_WP_TERMS_OF_SEPERATION_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (WPPWFDS.TermsOfSeparation_WPPWFRow row in wppwfDS.TermsOfSeparation_WPPWF.Rows)
            {
                cmd.Parameters.AddWithValue("p_TermsOfSeparationID", row.TermsOfSeparationID);
                cmd.Parameters.AddWithValue("p_Principal_P", row.Principal_P);
                cmd.Parameters.AddWithValue("p_Profit_P", row.Profit_P);

                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TERMS_OF_SEPARATION_UPDATE_WPPWF.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _DeleteTermsOfSeperation_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_WP_TERMS_OF_SEPERATION_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (DataIntegerDS.DataInteger row in integerDS.DataIntegers)
            {
                cmd.Parameters.AddWithValue("p_TermsOfSeparationID", row.IntegerValue);
                
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TERMS_OF_SEPARATION_DELETE_WPPWF.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetTermsOfSeperation_WPPWF(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAllTermsOfSeperation_WPPWF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            adapter.SelectCommand = cmd;

            WPPWFDS wppwfDS = new WPPWFDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.TermsOfSeparation_WPPWF.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_TERMS_OF_SEPARATION_WPPWF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _DeleteLastInvestment_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_WP_DELETE_LAST_INVESTMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_InvID", integerDS.DataIntegers[0].IntegerValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DELETE_LAST_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetLastFundAllocationDate_WPPWF(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            DataDateDS dateDS = new DataDateDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLastFundAllocationDate_WPPWF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, dateDS, dateDS.DataDates.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_LAST_FUND_ALLOCATION_DATE_WPPWF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            dateDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(dateDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetProvisionBeneficiaryList_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetProvisionBeneficiaryList_WPPWF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.WPPWF_Balance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_WP_PROVISION_BENEFICIARY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetProvisionCalculatedData_WPPWF(DataSet inputDS)
        {
            //ErrorDS errDS = new ErrorDS();
            //DBConnectionDS connDS = new DBConnectionDS();
            //WPPWFDS wppwfDS = new WPPWFDS();
            //DataStringDS stringDS = new DataStringDS();

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();
            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();

            //OleDbDataAdapter adapter = new OleDbDataAdapter();
            //OleDbCommand cmd = new OleDbCommand();

            //cmd = DBCommandProvider.GetDBCommand("GetProvisionCalculatedData_WPPWF");
            //if (cmd == null) return UtilDL.GetCommandNotFound();

            //cmd.Parameters["ProvisionDate"].Value = stringDS.DataStrings[0].StringValue;
            //adapter.SelectCommand = cmd;

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.Wp_Inv_Provision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_GET_PROVISION_CALCULATED_DATA_WPPWF.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}
            //wppwfDS.AcceptChanges();
            //cmd.Parameters.Clear();
            //cmd.Dispose();

            //errDS.Clear();
            //errDS.AcceptChanges();

            //DataSet returnDS = new DataSet();
            //returnDS.Merge(wppwfDS);
            //returnDS.Merge(errDS);
            //returnDS.AcceptChanges();
            //return returnDS;


            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetProvisionCalculatedData_WPPWF");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["ProvisionDate"].Value = stringDS.DataStrings[0].StringValue;
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_PROVISION_CALCULATED_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _SaveProvisionCalculatedData_WPPWF(DataSet inputDS)
        {          

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            WPPWFDS wppwfDS = new WPPWFDS();

            wppwfDS.Merge(inputDS.Tables[wppwfDS.InvestmentProvision.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
         
            cmd.CommandText = "PRO_WP_PROVISION_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            string ProvisionDate = stringDS.DataStrings[0].StringValue + "/01/" + stringDS.DataStrings[1].StringValue + " 12:00:01 AM";

            foreach (WPPWFDS.InvestmentProvisionRow row in wppwfDS.InvestmentProvision.Rows)
            {
                cmd.Parameters.AddWithValue("p_INVID", row.InvID);
                cmd.Parameters.AddWithValue("p_ProvisionDate", Convert.ToDateTime(ProvisionDate));
                cmd.Parameters.AddWithValue("p_Day_P", row.Day_P);
                cmd.Parameters.AddWithValue("p_Profit_P", row.Profit_P);
                cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[2].StringValue);
                bool bError = false;
                int nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SAVE_PROVISION_CALCULATED_DATA_WPPWF.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;

        }
        private DataSet _GetProvisionedProfit_ForEmployee_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetProvisionedProfit_ForEmployee_WPPWF");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.WPPWF_Balance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PROVISIONED_PROFIT_FOR_EMPLOYEE_WPPWF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _DistributeProvisionedProfit_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Upd = new OleDbCommand();
            WPPWFDS wppwfDS = new WPPWFDS();
            bool bError = false;
            int nRowAffected = -1;

            wppwfDS.Merge(inputDS.Tables[wppwfDS.WPPWF_Balance.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //cmd.CommandText = "PRO_WP_EMP_GL_Transaction";   // WALI :: 31-Mar-2015 
            cmd.CommandText = "PRO_WP_DISTRIBUTE_PROVISION";  // WALI :: 31-Mar-2015 :: Added new column (Provision_Ref,OpeningBalance,ClosingBalance)
            cmd.CommandType = CommandType.StoredProcedure;

            cmd_Upd.CommandText = "PRO_WP_PROVISION_UPD";
            cmd_Upd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (WPPWFDS.WPPWF_BalanceRow row in wppwfDS.WPPWF_Balance.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                cmd.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                cmd.Parameters.AddWithValue("p_Event_Date", row.ProvisionDate);
                cmd.Parameters.AddWithValue("p_FundHeadID", 100);
                cmd.Parameters.AddWithValue("p_FundDistributionID", DBNull.Value);
                cmd.Parameters.AddWithValue("p_INVID", DBNull.Value);
                cmd.Parameters.AddWithValue("p_EmployeeID_Other_Ref", DBNull.Value);
                cmd.Parameters.AddWithValue("p_Payment_Reference", DBNull.Value);
                cmd.Parameters.AddWithValue("p_Transaction_Type", 1);               
                if (!row.IsOpeningBalanceNull()) cmd.Parameters.AddWithValue("p_OpeningBalance", row.OpeningBalance);
                else cmd.Parameters.AddWithValue("p_OpeningBalance", DBNull.Value);
                if (!row.IsClosingBalanceNull()) cmd.Parameters.AddWithValue("p_ClosingBalance", row.ClosingBalance);
                else cmd.Parameters.AddWithValue("p_ClosingBalance", DBNull.Value);
               

                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DISTRIBUTE_PROVISIONED_PROFIT_WPPWF.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            if (!bError)        // If Profit is Distributed, Update All Provisions as DISTRIBUTED
            {
                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Upd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_DISTRIBUTE_PROVISIONED_PROFIT.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;
        }

        private DataSet _GetPendingDistributions_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            //GetPendingDistributions_PF
            cmd = DBCommandProvider.GetDBCommand("GetPendingDistributions_WPPWF");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PENDING_DISTRIBUTION_WPPWF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _FundJournalEntry_WPPWF(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            wppwfDS.Merge(inputDS.Tables[wppwfDS.FundHead.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_FUNDHEAD_GL_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_FundDistributionID", wppwfDS.FundHead[0].FundDistributionID);
            cmd.Parameters.AddWithValue("p_FundHeadID", wppwfDS.FundHead[0].FundHeadID);
            cmd.Parameters.AddWithValue("p_Amount", wppwfDS.FundHead[0].Amount);
            cmd.Parameters.AddWithValue("p_Event_Date", wppwfDS.FundHead[0].EventDate);

            if (!wppwfDS.FundHead[0].IsReferenceNull()) cmd.Parameters.AddWithValue("p_Reference", wppwfDS.FundHead[0].Reference);
            else cmd.Parameters.AddWithValue("p_Reference", DBNull.Value);

            cmd.Parameters.AddWithValue("p_Ref_EmployeeID", DBNull.Value);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_FUND_JOURNAL_ENTRY_WPPWF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _EmployeeJournalEntry_WPPWF(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd_Fund = new OleDbCommand();
            OleDbCommand cmd_GL = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            OleDbCommand cmd_LF = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            wppwfDS.Merge(inputDS.Tables[wppwfDS.FundHead.TableName], false, MissingSchemaAction.Error);
            wppwfDS.Merge(inputDS.Tables[wppwfDS.Employee_Gl.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();

            bool isPayment = wppwfDS.FundHead[0].IsForPayment;

            cmd_Fund.CommandText = "PRO_WP_FUNDHEAD_GL_ADD";
            cmd_Fund.CommandType = CommandType.StoredProcedure;

            cmd_GL.CommandText = "PRO_WP_EMP_GL_Transaction";
            cmd_GL.CommandType = CommandType.StoredProcedure;

            cmd_LF.CommandText = "PRO_WP_FUNDHEAD_GL_CRE_LAPSES"; //"PRO_WP_FUNDHEAD_GL_ADD_LAPSES";
            cmd_LF.CommandType = CommandType.StoredProcedure;

            if (cmd_Fund == null) return UtilDL.GetCommandNotFound();
                        
            #region Employee GL Entry
            decimal totalAmount = wppwfDS.FundHead[0].Amount;
            decimal perEmployeeAmount = totalAmount / wppwfDS.Employee_Gl.Rows.Count;

            foreach (WPPWFDS.Employee_GlRow row in wppwfDS.Employee_Gl.Rows)
            {
                #region Positive Entry
                cmd_GL.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                cmd_GL.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);    // WALI :: 06-Apr-2015 :: This amount is brought from UI
                cmd_GL.Parameters.AddWithValue("p_Event_Date", wppwfDS.FundHead[0].EventDate);
                if (row.Transaction_Type == 1) cmd_GL.Parameters.AddWithValue("p_FundHeadID", 100); //1 = Profit
                else cmd_GL.Parameters.AddWithValue("p_FundHeadID", wppwfDS.FundHead[0].FundHeadID);
                cmd_GL.Parameters.AddWithValue("p_FundDistributionID", wppwfDS.FundHead[0].FundDistributionID);
                cmd_GL.Parameters.AddWithValue("p_INVID", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_EmployeeID_Other_Ref", DBNull.Value);
                if (!wppwfDS.FundHead[0].IsReferenceNull()) cmd_GL.Parameters.AddWithValue("p_Payment_Reference", wppwfDS.FundHead[0].Reference);
                else cmd_GL.Parameters.AddWithValue("p_Payment_Reference", DBNull.Value);
                cmd_GL.Parameters.AddWithValue("p_Transaction_Type", row.Transaction_Type);
                if (row.Transaction_Type == 1) cmd_GL.Parameters.AddWithValue("p_Ref_HeadID", wppwfDS.FundHead[0].FundHeadID); //1 = Profit
                else cmd_GL.Parameters.AddWithValue("p_Ref_HeadID", DBNull.Value);

                bool bError_GL = false;
                int nRowAffected_GL = -1;
                nRowAffected_GL = ADOController.Instance.ExecuteNonQuery(cmd_GL, connDS.DBConnections[0].ConnectionID, ref bError_GL);
                cmd_GL.Parameters.Clear();
                if (bError_GL)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_JOURNAL_ENTRY_WPPWF.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                #region Negative Entry [For Payment]
                if (isPayment)
                {
                    cmd_GL.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    //cmd_GL.Parameters.AddWithValue("p_GL_Amount", (-1 * perEmployeeAmount));
                    cmd_GL.Parameters.AddWithValue("p_GL_Amount", (-1 * row.GL_Amount));    // WALI :: 06-Apr-2015 :: This amount is brought from UI
                    cmd_GL.Parameters.AddWithValue("p_Event_Date", wppwfDS.FundHead[0].EventDate);
                    cmd_GL.Parameters.AddWithValue("p_FundHeadID", wppwfDS.FundHead[0].FundHeadID);
                    cmd_GL.Parameters.AddWithValue("p_FundDistributionID", wppwfDS.FundHead[0].FundDistributionID);
                    cmd_GL.Parameters.AddWithValue("p_INVID", DBNull.Value);
                    cmd_GL.Parameters.AddWithValue("p_EmployeeID_Other_Ref", DBNull.Value);
                    if (!wppwfDS.FundHead[0].IsReferenceNull()) cmd_GL.Parameters.AddWithValue("p_Payment_Reference", wppwfDS.FundHead[0].Reference);
                    else cmd_GL.Parameters.AddWithValue("p_Payment_Reference", DBNull.Value);
                    cmd_GL.Parameters.AddWithValue("p_Transaction_Type", row.Transaction_Type);

                    cmd_GL.Parameters.AddWithValue("p_Ref_HeadID", DBNull.Value);

                    bool bError_GL_NG = false;
                    int nRowAffected_GL_NG = -1;
                    nRowAffected_GL_NG = ADOController.Instance.ExecuteNonQuery(cmd_GL, connDS.DBConnections[0].ConnectionID, ref bError_GL_NG);
                    cmd_GL.Parameters.Clear();
                    if (bError_GL_NG)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_JOURNAL_ENTRY_WPPWF.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion
            }
            #endregion

            #region Fund Journal
            // Insert a Negative Value Row as this amount is PAID/Distributed
            if (wppwfDS.FundHead[0].FundHeadID == 104)   // for Lapses & Forfeiture, different table
            {
                #region For Lapses and Forfeiture
                cmd_LF.Parameters.AddWithValue("p_FundDistributionID", wppwfDS.FundHead[0].FundDistributionID);
                cmd_LF.Parameters.AddWithValue("p_FundHeadID", 104);    // HeadID for 'Lapses and Forfeiture'
                cmd_LF.Parameters.AddWithValue("p_Amount", (-1 * wppwfDS.FundHead[0].Amount));
                cmd_LF.Parameters.AddWithValue("p_Event_Date", wppwfDS.FundHead[0].EventDate);
                
                if (!wppwfDS.FundHead[0].IsReferenceNull()) cmd_LF.Parameters.AddWithValue("p_Reference", wppwfDS.FundHead[0].Reference);
                else cmd_LF.Parameters.AddWithValue("p_Reference", DBNull.Value);
                
                cmd_LF.Parameters.AddWithValue("p_Ref_EmployeeID", DBNull.Value);

                cmd_LF.Parameters.AddWithValue("p_Ref_HeadID", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_LF, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_LF.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_JOURNAL_ENTRY_WPPWF.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion
            }
            else
            {
                #region For Other Funds
                cmd_Fund.Parameters.AddWithValue("p_FundDistributionID", wppwfDS.FundHead[0].FundDistributionID);
                cmd_Fund.Parameters.AddWithValue("p_FundHeadID", wppwfDS.FundHead[0].FundHeadID);
                cmd_Fund.Parameters.AddWithValue("p_Amount", (-1 * wppwfDS.FundHead[0].Amount));
                cmd_Fund.Parameters.AddWithValue("p_Event_Date", wppwfDS.FundHead[0].EventDate);

                if (!wppwfDS.FundHead[0].IsReferenceNull()) cmd_Fund.Parameters.AddWithValue("p_Reference", wppwfDS.FundHead[0].Reference);
                else cmd_Fund.Parameters.AddWithValue("p_Reference", DBNull.Value);

                cmd_Fund.Parameters.AddWithValue("p_Ref_EmployeeID", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Fund, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Fund.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_JOURNAL_ENTRY_WPPWF.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion
            }

            //if Disciplinary Action then create Lapses & Forfeiture 
            #region For Create Lapses and Forfeiture...
            if (wppwfDS.FundHead[0].FundHeadID == 101)   // for Disciplinary Action
            {
                cmd_LF.Parameters.AddWithValue("p_FundDistributionID", wppwfDS.FundHead[0].FundDistributionID);
                cmd_LF.Parameters.AddWithValue("p_FundHeadID", 104);    // HeadID for 'Lapses and Forfeiture'
                cmd_LF.Parameters.AddWithValue("p_Amount", wppwfDS.FundHead[0].Amount);
                cmd_LF.Parameters.AddWithValue("p_Event_Date", wppwfDS.FundHead[0].EventDate);

                if (!wppwfDS.FundHead[0].IsReferenceNull()) cmd_LF.Parameters.AddWithValue("p_Reference", wppwfDS.FundHead[0].Reference);
                else cmd_LF.Parameters.AddWithValue("p_Reference", DBNull.Value);

                cmd_LF.Parameters.AddWithValue("p_Ref_EmployeeID", DBNull.Value);

                cmd_LF.Parameters.AddWithValue("p_Ref_HeadID", 101);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_LF, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_LF.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_JOURNAL_ENTRY_WPPWF.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetFundHeadGL_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataSet returnDS = new DataSet();
            OleDbCommand cmd = new OleDbCommand();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            Int32 fundHeadID = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            Int32 fundDistributionID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetFundHeadGL_WPPWF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["FundDistributionID1"].Value = fundDistributionID;
            cmd.Parameters["FundDistributionID2"].Value = fundDistributionID;
            cmd.Parameters["FundDistributionID3"].Value = fundDistributionID;
            cmd.Parameters["FundDistributionID4"].Value = fundDistributionID;
            cmd.Parameters["FundDistributionID5"].Value = fundDistributionID;
            cmd.Parameters["FundHeadID"].Value = fundHeadID;

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.FundHead.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_GL_WPPWF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetTotalPayableToLSC_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetTotalPayableToLSC_WPPWF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.FundHead.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_TOTAL_PAYABLE_TO_LSC_WPPWF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();

            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet GetDistributedProvisionData_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetDistributedProvisionData_WPPWF");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.WPPWF_Balance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_DISTRIBUTED_PROVISION_DATA_WPPWF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetEmployeeListForSeperation_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_ForPayment_WPPWF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["PaymentStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);   // Added ::WALI :: 19-Apr-2015
            cmd.Parameters["PaymentStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);   // Added ::WALI :: 19-Apr-2015

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.WPPWF_Balance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_WPPWF_EMPLOYEE_LIST_FOR_SEPERATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _EmployeePayment_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            wppwfDS.Merge(inputDS.Tables[wppwfDS.WPPWF_Balance.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_EMPLOYEE_PAYMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_EmployeeID", wppwfDS.WPPWF_Balance[0].EmployeeID);
            cmd.Parameters.AddWithValue("p_Payment_Status", true);
            cmd.Parameters.AddWithValue("p_Actual_Payment_Date", wppwfDS.WPPWF_Balance[0].Actual_Payment_Date);

            if (!wppwfDS.WPPWF_Balance[0].IsPayment_ReferenceNull()) cmd.Parameters.AddWithValue("p_Payment_Reference", wppwfDS.WPPWF_Balance[0].Payment_Reference);
            else cmd.Parameters.AddWithValue("p_Payment_Reference", DBNull.Value);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_WPPWF_EMPLOYEE_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }
        private DataSet _GetFundHeadGL_Balance_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS inregerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            inregerDS.Merge(inputDS.Tables[inregerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            inregerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFundHeadGL_Balance_WPPWF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["FundDistributionID"].Value = inregerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["FundHeadID"].Value = inregerDS.DataIntegers[1].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            WPPWFDS wppwfDS = new WPPWFDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.FundHead.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        #region Balance Modification
        private DataSet _GetFundHeadGL_Records_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFundHeadGL_Records_WPPWF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["FundHeadID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue, CultureInfo.InvariantCulture);
            cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            WPPWFDS wppwfDS = new WPPWFDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.FundHead.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_WPPWF_FUNDHEAD_GL_RECORDS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ModifyFundHeadGL_Records_WPPWF(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            MessageDS messageDS = new MessageDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            wppwfDS.Merge(inputDS.Tables[wppwfDS.FundHead.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_MODIFY_FUNDHEAD_GL";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (WPPWFDS.FundHeadRow row in wppwfDS.FundHead.Rows)
            {
                cmd.Parameters.AddWithValue("p_HeadGLID", row.HeadGLID);
                cmd.Parameters.AddWithValue("p_Changed_GL_Amount", row.Amount);
                cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_WPPWF_MODIFY_FUNDHEAD_GL_RECORDS.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EventDate.ToString("dd-MMM-yyyy") + "  [" + row.HeadName + "]");
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EventDate.ToString("dd-MMM-yyyy") + "  [" + row.HeadName + "]");
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetEmployeeGL_Records_WPPWF(DataSet inputDS)
        {
            #region Commented
            //ErrorDS errDS = new ErrorDS();
            //DBConnectionDS connDS = new DBConnectionDS();
            //WPPWFDS wppwfDS = new WPPWFDS();
            //DataStringDS stringDS = new DataStringDS();
            //OleDbDataAdapter adapter = new OleDbDataAdapter();
            //OleDbCommand cmd = new OleDbCommand();

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();
            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();

            //if (stringDS.DataStrings[0].StringValue == "Principal") cmd = DBCommandProvider.GetDBCommand("GetEmployeePrincipalAmounts_WPPWF");
            //else if (stringDS.DataStrings[0].StringValue == "Profit") cmd = DBCommandProvider.GetDBCommand("GetEmployeeProvisionDistributions_WPPWF");
            //else if (stringDS.DataStrings[0].StringValue == "Others") cmd = DBCommandProvider.GetDBCommand("GetEmployeeOtherAmounts_WPPWF");
            //if (cmd == null)
            //{
            //    Util.LogInfo("Command is null.");
            //    return UtilDL.GetCommandNotFound();
            //}
            //adapter.SelectCommand = cmd;

            //cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[6].StringValue, CultureInfo.InvariantCulture);
            //cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[7].StringValue, CultureInfo.InvariantCulture);
            //cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[2].StringValue + "%";
            //cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
            //cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
            //cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[3].StringValue;
            //cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[3].StringValue;
            //cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[4].StringValue;
            //cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[4].StringValue;
            //cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[5].StringValue;
            //cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[5].StringValue;

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.WPPWF_Balance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_GET_WPPWF_EMPLOYEE_GL_RECORDS.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}
            //wppwfDS.AcceptChanges();
            //cmd.Parameters.Clear();
            //cmd.Dispose();

            //errDS.Clear();
            //errDS.AcceptChanges();

            //DataSet returnDS = new DataSet();
            //returnDS.Merge(wppwfDS);
            //returnDS.Merge(errDS);
            //returnDS.AcceptChanges();
            //return returnDS;
            #endregion

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "Principal")
            {
                #region Command for PRINCIPAL
                cmd = DBCommandProvider.GetDBCommand("GetEmployeePrincipalAmounts_WPPWF");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["FundDistributionID1"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue, CultureInfo.InvariantCulture);
                cmd.Parameters["FundDistributionID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue, CultureInfo.InvariantCulture);
                cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture);
                cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[3].StringValue, CultureInfo.InvariantCulture);
                cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[4].StringValue + "%";
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[8].StringValue;
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Profit")
            {
                #region Command for PROFIT
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeProvisionDistributions_WPPWF");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture);
                cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[3].StringValue, CultureInfo.InvariantCulture);
                cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[4].StringValue + "%";
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[8].StringValue;
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Others")
            {
                #region Command for OTHERS
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeOtherAmounts_WPPWF");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["FundDistributionID1"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue, CultureInfo.InvariantCulture);
                cmd.Parameters["FundDistributionID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue, CultureInfo.InvariantCulture);
                cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture);
                cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[3].StringValue, CultureInfo.InvariantCulture);
                cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[4].StringValue + "%";
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[8].StringValue;
                #endregion
            }
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, wppwfDS, wppwfDS.WPPWF_Balance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_WPPWF_EMPLOYEE_GL_RECORDS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            wppwfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(wppwfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ModifyEmployeeGL_Records_WPPWF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            WPPWFDS wppwfDS = new WPPWFDS();
            MessageDS messageDS = new MessageDS();
            bool bError = false;
            int nRowAffected = -1;

            wppwfDS.Merge(inputDS.Tables[wppwfDS.WPPWF_Balance.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_MODIFY_EMPLOYEE_GL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (WPPWFDS.WPPWF_BalanceRow row in wppwfDS.WPPWF_Balance.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeGLID", row.EmployeeGLID);
                cmd.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_WPPWF_MODIFY_EMPLOYEE_GL_RECORDS.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("dd-MMM-yyyy") + "]" + " [" + row.HeadName + "]");
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("dd-MMM-yyyy") + "]" + " [" + row.HeadName + "]");
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            return returnDS;
        }
        #endregion

    }
}
