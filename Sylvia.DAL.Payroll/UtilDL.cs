using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using System.Configuration;
//using Sylvia.DAL.Payroll.PO;
using System.Data.OracleClient;

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for UtilDL.
    /// </summary>
    public class UtilDL
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connDS"></param>
        /// <param name="Code"></param>
        /// <returns></returns>
        public static long GetId(DBConnectionDS connDS, OleDbCommand cmd)
        {
            bool bError = false;
            object obj = ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                return (-1);
            }
            if (obj == DBNull.Value)
            {
                return -1;
            }
            long nId = Convert.ToInt32(obj);
            return nId;

        }
        public static string GetCode(DBConnectionDS connDS, OleDbCommand cmd)
        {
            bool bError = false;
            object obj = ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                return null;
            }
            if (obj == DBNull.Value)
            {
                return null;
            }
            string sCode = Convert.ToString(obj);
            return sCode;
        }



        static public ErrorDS GetDBOperationFailed()
        {
            ErrorDS errDS = new ErrorDS();
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }

        #region Get Email Information
        static public string GetHost()
        {
            string host;
            host = ConfigurationSettings.AppSettings["Host"];
            return host;
        }
        static public Int32 GetPort()
        {
            string sPort;
            Int32 port;
            sPort = ConfigurationSettings.AppSettings["Port"];
            try
            {
                port = Convert.ToInt32(sPort);
            }
            catch
            {
                port = -1;
            }
            return port;
        }
        static public string GetFromEMailAddress()
        {
            string fromEMailAddress;
            fromEMailAddress = ConfigurationSettings.AppSettings["FromEAMailAddress"];
            return fromEMailAddress;
        }
        static public string GetEmailType()
        {
            string EmailType;
            EmailType = ConfigurationSettings.AppSettings["IsEMailSendWithCredential"];
            return EmailType;
        }
        static public string GetCredentialEmailAddress()
        {
            string EmailAddress;
            EmailAddress = ConfigurationSettings.AppSettings["CredentialEmailAddress"];
            return EmailAddress;
        }
        static public string GetCredentialEmailPwd()
        {
            string Password;
            Password = ConfigurationSettings.AppSettings["CredentialEmailPwd"];
            return Password;
        }
        static public string GetEnableSslValue()
        {
            string EnableSsl;
            EnableSsl = ConfigurationSettings.AppSettings["EnableSsl"];
            return EnableSsl;
        }
        static public string GetMailSubForDelegate()
        {
            string MailSubject;
            MailSubject = ConfigurationSettings.AppSettings["MailSubForDelegate"];
            return MailSubject;
        }
        static public string GetMailingSubjectForLFA()
        {
            string host;
            host = ConfigurationSettings.AppSettings["MailSubForLeaveWithLFAPending"];
            return host;
        }
        static public string GetMailSubForLeaveWithLFAApprove()
        {
            string host;
            host = ConfigurationSettings.AppSettings["MailSubForLeaveWithLFAApprove"];
            return host;
        }
        static public string GetMailSubForLeaveWithLFAReject()
        {
            string host;
            host = ConfigurationSettings.AppSettings["MailSubForLeaveWithLFAReject"];
            return host;
        }
        static public string GetMailSubForLeaveWithLFACancel()
        {
            string host;
            host = ConfigurationSettings.AppSettings["MailSubForLeaveWithLFACancel"];
            return host;
        }
        #endregion

        public static string base64Decode(string sData)
        {
            System.Text.Decoder utf8Decode = (new System.Text.UTF8Encoding()).GetDecoder();

            byte[] todecode_byte = Convert.FromBase64String(sData);
            char[] decoded_char = new char[utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length)];

            utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);

            return (new String(decoded_char));
        }

        static public ErrorDS GetCommandNotFound()
        {
            ErrorDS errDS = new ErrorDS();
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_COMMAND_NOT_FOUND.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;
        }

        static public long GetAllowDeductId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductId");
            if (cmd == null)
            {
                return -1;
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }

        static public string GetAllowDeductCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }

        static public string GetAllowDeductName(DBConnectionDS connDS, string AllowanceCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllowDeductName");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["AllowanceCode"].Value = AllowanceCode;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }

        static public long GetGradeId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGradeId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }

        static public string GetGradeCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGradeCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }

        static public long GetUserId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUserId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetUserCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUserCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }

        static public long GetAdParamId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAdParamId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetAdParamCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAdParamCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }


        static public long GetBranchId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBranchId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetBranchCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBranchCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public long GetBankId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBankId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public long GetRoleId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRoleId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["RoleName"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }

        static public string GetBankCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBankCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public string GetBranchBankCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBranchBankCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public long GetShiftId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetShiftId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetShiftCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetShiftCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }

        static public long GetCompanyId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCompanyId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetCompanyCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCompanyCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public long GetFunctionId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFunctionId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetFunctionCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFunctionCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public long GetLocationId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLocationId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetLocationCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLocationCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public long GetSiteId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSiteId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetSiteCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSiteCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public long GetDesignationId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDesignationId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetDesignationCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDesignationCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public long GetCostCenterId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCostCenterId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetCostCenterCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCostCenterCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public long GetDepartmentId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDepartmentId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetDepartmentCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDepartmentCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }

        static public long GetIncomeTaxId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetITParamId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["FiscalYear"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }

        static public long GetActivityId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetActivityId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetActivityCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetActivityCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }

        static public long GetRescenterId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetResponsCenterId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetRescenterCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetResponsCenterCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public long GetPayrollSiteId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPayrollSiteId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetPayrollSiteCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPayrollSiteCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }

        static public long GetLoanTypeId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanTypeId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetLoanTypeCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanTypeCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }

        static public long GetLoanIssueId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanIssueId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetLoanIssueCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanIssueCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }

        static public long GetLoanId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetLoanCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        static public long GetPARatingId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPARatingId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public long GetEmployeeId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetEmployeeCodeById(DBConnectionDS connDS, long employeeID)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeCodeById");
            if (cmd == null) return "";
            cmd.Parameters["EmployeeID"].Value = employeeID;

            return UtilDL.GetCode(connDS, cmd);
        }

        //mislbd.Jarif Update Start(07-Apr-2008)
        static public long GetJobHistMaxId(DBConnectionDS connDS, long nEmployeeId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetJobHistMaxId");
            if (cmd == null)
            {
                return (-1);
            }

            cmd.Parameters["EmployeeId"].Value = nEmployeeId;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        //mislbd.Jarif Update End(07-Apr-2008)

        //mislbd.Jarif Update Start(08-Apr-2008)
        static public Boolean DoesExistSalary(DBConnectionDS connDS, long nEmployeeId, DateTime dDate)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalaryId");
            if (cmd == null)
            {
                return true;
            }

            cmd.Parameters["EmployeeId"].Value = nEmployeeId;
            cmd.Parameters["Date"].Value = dDate;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);

            if (nId > 0)
            {
                return true;
            }

            return false;
        }
        //mislbd.Jarif Update End(08-Apr-2008)

        //mislbd.Jarif Update Start(20-Apr-2008)
        static public DateTime GetPfEntryDate(DBConnectionDS connDS, long nEmployeeId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPfEntryDate");

            DateTime dDate = new DateTime(1754, 1, 1);

            if (cmd == null)
            {
                return dDate;
            }

            cmd.Parameters["EmployeeId"].Value = nEmployeeId;

            bool bError = false;
            object obj = ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                return dDate;
            }
            if (obj == DBNull.Value)
            {
                return dDate.AddYears(1);
            }
            dDate = Convert.ToDateTime(obj);
            return dDate.AddDays(-(dDate.Day - 1));
        }
        //mislbd.Jarif Update End(20-Apr-2008)
        //mislbd.Jarif Update Start(01-Mar-2009)
        static public long GetConfigPromotionId(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetConfigPromotionId");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        static public string GetConfigPromotionCode(DBConnectionDS connDS, long nId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetConfigPromotionCode");
            if (cmd == null)
            {
                return null;
            }
            cmd.Parameters["Id"].Value = nId;

            string sCode = null;
            sCode = UtilDL.GetCode(connDS, cmd);
            return sCode;
        }
        //mislbd.Jarif Update End(01-Mar-2009)

        // WALI :: 07-Jun-2015
        static public long GetRescenterId_By_EmployeeCode(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetResponsCenterId_By_Empcode");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        // End :: WALI :: 07-Jun-2015

        // WALI :: 24-Jun-2015
        static public DateTime GetSystemDateTime(DBConnectionDS connDS)
        {
            DateTime dDate = new DateTime(1754, 1, 1);
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSystemDateTime");
            if (cmd == null) return dDate;

            bool bError = false;
            object obj = ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError) return dDate;
            dDate = Convert.ToDateTime(obj);
            return dDate;
        }
        // WALI :: 24-Jun-2015 :: End

        static public Boolean DoesExistSalary_PF(DBConnectionDS connDS, long nEmployeeId)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPF_SalaryId");
            if (cmd == null)
            {
                return true;
            }

            cmd.Parameters["EmployeeId"].Value = nEmployeeId;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);

            if (nId > 0)
            {
                return true;
            }

            return false;
        }

        //public static long GetId_Ora(DBConnectionDS connDS, OracleCommand cmd)
        //{
        //    bool bError = false;
        //    object obj = ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
        //    if (bError == true)
        //    {
        //        return (-1);
        //    }
        //    if (obj == DBNull.Value)
        //    {
        //        return -1;
        //    }
        //    long nId = Convert.ToInt32(obj);
        //    return nId;

        //}
        //static public long GetSiteId_Ora(DBConnectionDS connDS, string sCode)
        //{
        //    OracleCommand cmd = DBCommandProvider.GetDBCommand_Ora("GetSiteId_Ora");
        //    if (cmd == null)
        //    {
        //        return (-1);
        //    }
        //    cmd.Parameters["Code"].Value = sCode;

        //    long nId = -1;
        //    nId = UtilDL.GetId_Ora(connDS, cmd);
        //    return nId;
        //}
        static public long GetEmployeeIdbyUserID(DBConnectionDS connDS, string sCode)
        {
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getEmployeeIdbyUserID");
            if (cmd == null)
            {
                return (-1);
            }
            cmd.Parameters["Code"].Value = sCode;

            long nId = -1;
            nId = UtilDL.GetId(connDS, cmd);
            return nId;
        }
        

    }  // end of class
}  // end of namespace
