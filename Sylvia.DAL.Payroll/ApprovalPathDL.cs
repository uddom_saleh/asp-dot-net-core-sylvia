﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class ApprovalPathDL
    {
        public ApprovalPathDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_ACTION_GET_APPROVALPATH_LIST_BYID:
                    return this._getApprovalPathListByID(inputDS);
                case ActionID.ACTION_APPROVALPATH_ADD:
                    return this._createApprovalPath(inputDS);
                case ActionID.ACTION_APPROVALPATH_DEL:
                    return this._deleteApprovalPath(inputDS);
                case ActionID.NA_ACTION_GET_DEPARTMENTLIST_BY_MODULEID:
                    return this._getDepartmentListByModuleID(inputDS);
                case ActionID.NA_ACTION_BYPASS_DEPARTMENT_GET:
                    return this._getBypassDepartmentList(inputDS);
                case ActionID.ACTION_BYPASS_CONFIGURATION_ADD:
                    return _createBypassConfig(inputDS);
                case ActionID.NA_ACTION_GET_BYPASS_DEPTLIST_BY_MODULEID:
                    return this._getBypassDeptListByModuleID(inputDS);
                

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getApprovalPathListByID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetApprovalPathListByID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["ModuleId"].Value = (object)integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            ApprovalPathDS approvalDS = new ApprovalPathDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, approvalDS, approvalDS.ApprovalPath.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_APPROVALPATH_LIST_BYID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            approvalDS.AcceptChanges();
                       
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(approvalDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createApprovalPath(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();            
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_APPROVALPATH_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            ApprovalPathDS pathDS = new ApprovalPathDS();
            pathDS.Merge(inputDS.Tables[pathDS.ApprovalPath.TableName], false, MissingSchemaAction.Error);
            pathDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool bError = false;
            int nRowAffected = 0;
            #region Approval Path Deletion...
            foreach (ApprovalPathDS.ApprovalPathRow row in pathDS.ApprovalPath.Rows)
            {

                OleDbCommand cmdDel = new OleDbCommand();
                cmdDel.CommandText = "PRO_APPROVALPATH_DELETE";
                cmdDel.CommandType = CommandType.StoredProcedure;

                cmdDel.Parameters.AddWithValue("p_ModuleID", row.ModuleID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_APPROVALPATH_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                break;
            }
            #endregion
            #region Approval Path Creation...
            long genPK = 0;
            foreach (ApprovalPathDS.ApprovalPathRow row in pathDS.ApprovalPath.Rows)
            {
                genPK = IDGenerator.GetNextGenericPK();

                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_ApprovalPathID", genPK);
                cmd.Parameters.AddWithValue("p_ModuleID", row.ModuleID);

                if (!row.IsApproverTypeNull()) cmd.Parameters.AddWithValue("p_ApproverType", row.ApproverType);
                else cmd.Parameters.AddWithValue("p_ApproverType", DBNull.Value);

                if (!row.IsApproval_DepartmentIDNull()) cmd.Parameters.AddWithValue("p_Approval_DepartmentID", row.Approval_DepartmentID);
                else cmd.Parameters.AddWithValue("p_Approval_DepartmentID", DBNull.Value);

                if (!row.IsApproval_LiabilityIDNull()) cmd.Parameters.AddWithValue("p_Approval_LiabilityID", row.Approval_LiabilityID);
                else cmd.Parameters.AddWithValue("p_Approval_LiabilityID", DBNull.Value);

                cmd.Parameters.AddWithValue("P_Priority", row.Priority);

                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("P_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("P_Remarks", DBNull.Value);
                
                cmd.Parameters.AddWithValue("p_AbleToMakeFinal", row.AbleToMakeFinal);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_APPROVALPATH_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteApprovalPath(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_APPROVALPATH_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_ModuleID", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_APPROVALPATH_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getDepartmentListByModuleID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDepartmentListByModuleID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BypassConfigDS bypassConfigDS = new BypassConfigDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bypassConfigDS, bypassConfigDS.BypassConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENTLIST_BY_MODULEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bypassConfigDS.AcceptChanges();
           
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(bypassConfigDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getBypassDepartmentList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBypassDepartmentList");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BypassConfigDS bypassConfigDS = new BypassConfigDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bypassConfigDS, bypassConfigDS.BypassConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENTLIST_BY_MODULEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bypassConfigDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(bypassConfigDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createBypassConfig(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BypassConfigDS bypassConfigDS = new BypassConfigDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmdBypassConfig = new OleDbCommand();
            cmdBypassConfig.CommandText = "PRO_BYPASS_CONFIG_CREATE";
            cmdBypassConfig.CommandType = CommandType.StoredProcedure;

            if (cmdBypassConfig == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            bypassConfigDS.Merge(inputDS.Tables[bypassConfigDS.BypassConfig.TableName], false, MissingSchemaAction.Error);
            bypassConfigDS.Merge(inputDS.Tables[bypassConfigDS.BypassDepartment.TableName], false, MissingSchemaAction.Error);
            bypassConfigDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Save Bypass Cofiguration related information            
            foreach (BypassConfigDS.BypassConfigRow bypassConfigRow in bypassConfigDS.BypassConfig.Rows)
            {
                long bypassConfigID = IDGenerator.GetNextGenericPK();
                if (bypassConfigID == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmdBypassConfig.Parameters.AddWithValue("p_BypassConfigId", bypassConfigID);
                cmdBypassConfig.Parameters.AddWithValue("p_ModuleID", bypassConfigRow.ModuleID);
                cmdBypassConfig.Parameters.AddWithValue("p_FromDepartmentId", bypassConfigRow.FromDepartmentId);

                if (!bypassConfigRow.IsBypassConfigRemarksNull())
                {
                    cmdBypassConfig.Parameters.AddWithValue("p_BypassConfigRemarks", bypassConfigRow.BypassConfigRemarks);
                }
                else
                {
                    cmdBypassConfig.Parameters.AddWithValue("p_BypassConfigRemarks", DBNull.Value);
                }

                if (!bypassConfigRow.IsActivityNull())
                {
                    cmdBypassConfig.Parameters.AddWithValue("p_Activity", bypassConfigRow.Activity);
                }
                else
                {
                    cmdBypassConfig.Parameters.AddWithValue("p_Activity", DBNull.Value);
                }

                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdBypassConfig, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BYPASS_CONFIGURATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                //Save Bypass Department related information
                foreach (BypassConfigDS.BypassDepartmentRow bypassDeptRow in bypassConfigDS.BypassDepartment.Rows)
                {
                    OleDbCommand cmdBypassDept = new OleDbCommand();
                    cmdBypassDept.CommandText = "PRO_BYPASS_DEPT_CREATE";
                    cmdBypassDept.CommandType = CommandType.StoredProcedure;

                    cmdBypassDept.Parameters.AddWithValue("p_BypassConfigId", bypassConfigID);
                    cmdBypassDept.Parameters.AddWithValue("p_ToDepartmentId", bypassDeptRow.ToDepartmentId);

                    if (bypassDeptRow.IsBypassDepartmentRemarksNull() == false)
                    {
                        cmdBypassDept.Parameters.AddWithValue("p_BypassDepartmentRemarks", bypassDeptRow.BypassDepartmentRemarks);
                    }
                    else
                    {
                        cmdBypassDept.Parameters.AddWithValue("p_BypassDepartmentRemarks", DBNull.Value);
                    }

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdBypassDept, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BYPASS_CONFIGURATION_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getBypassDeptListByModuleID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBypassDeptListByModuleID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["ModuleId"].Value = (object)integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BypassConfigDS bypassConfigDS = new BypassConfigDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bypassConfigDS, bypassConfigDS.BypassDepartment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BYPASS_DEPTLIST_BY_MODULEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bypassConfigDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(bypassConfigDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
    }
}
