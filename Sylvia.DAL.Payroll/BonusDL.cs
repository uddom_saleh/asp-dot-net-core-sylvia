using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
  /// <summary>
  /// Summary description for BonusDL.
  /// </summary>
  public class BonusDL
  {
    public BonusDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }
    
    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {
        case ActionID.ACTION_BONUS_ADD:
          return _createBonus( inputDS );
      case ActionID.NA_ACTION_Q_BONUS_ALL:
          return _getBonusList(inputDS);
        case ActionID.ACTION_BONUS_DEL:
          return this._deleteBonus( inputDS );
        case ActionID.ACTION_BONUS_UPD:
          return this._updateBonus( inputDS );
        case ActionID.ACTION_DOES_BONUS_EXIST:
          return this._doesBonusExist( inputDS );
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
    private DataSet _createBonus(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      BonusDS bonusDS = new BonusDS();
      DBConnectionDS connDS = new  DBConnectionDS();

      

      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      
      //create command
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateBonus");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      
      bonusDS.Merge( inputDS.Tables[ bonusDS.Bonuses.TableName ], false, MissingSchemaAction.Error );
      bonusDS.AcceptChanges();
      foreach(BonusDS.Bonus bonus in bonusDS.Bonuses)
      {
      
        long genPK = IDGenerator.GetNextGenericPK();
        

        cmd.Parameters["Id"].Value = (object) genPK;
        if(bonus.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) bonus.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;
        }
        if(bonus.IsNameNull()==false)
        {
          cmd.Parameters["Name"].Value = (object) bonus.Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value = null;
        }
     

        if(bonus.IsIsforConfirmedOnlyNull()==false)
        {
          cmd.Parameters["IsforConfirmedOnly"].Value = (object) bonus.IsforConfirmedOnly;
        }
        else
        {
          cmd.Parameters["IsforConfirmedOnly"].Value = null;
        }

        if(bonus.IsFlatAmountNull()==false)
        {
          cmd.Parameters["FlatAmount"].Value = (object) bonus.FlatAmount ;
        }
        else
        {
          cmd.Parameters["FlatAmount"].Value = null;
        }
        if(bonus.IsNoofBasicNull()==false)
        {
          cmd.Parameters["NoofBasic"].Value = (object) bonus.NoofBasic ;
        }
        else
        {
          cmd.Parameters["NoofBasic"].Value = null;
        }
        if(bonus.IsNoofDaysNull()==false)
        {
          cmd.Parameters["NoofDays"].Value = (object) bonus.NoofDays ;
        }
        else
        {
          cmd.Parameters["NoofDays"].Value = null;
        }
        if(bonus.IsInstallmentNoinYearNull()==false)
        {
          cmd.Parameters["InstallmentNoinYear"].Value = (object) bonus.InstallmentNoinYear;
        }
        else
        {
          cmd.Parameters["InstallmentNoinYear"].Value = null;
        }
      
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_BONUS_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _deleteBonus(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

    
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteBonus");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      foreach(DataStringDS.DataString sValue in stringDS.DataStrings)
      {
        if (sValue.IsStringValueNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) sValue.StringValue;
        }
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_BONUS_DEL.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }

      return errDS;  // return empty ErrorDS
    }
    private DataSet _getBonusList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataLongDS longDS = new DataLongDS();
      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

   

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBonusList");
  
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
    

      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      BonusPO bonusPO = new BonusPO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter, 
        bonusPO, bonusPO.Bonuses.TableName, 
        connDS.DBConnections[0].ConnectionID, 
        ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_BONUS_ALL.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }

      bonusPO.AcceptChanges();

      BonusDS bonusDS = new BonusDS();
      if ( bonusPO.Bonuses.Count > 0 )
      {
       

        foreach ( BonusPO.Bonus poBonus in bonusPO.Bonuses)
        {
          BonusDS.Bonus bonus = bonusDS.Bonuses.NewBonus();
          if(poBonus.IsCodeNull()==false)
          {
            bonus.Code = poBonus.Code;
          }
          if(poBonus.IsNameNull()==false)
          {
            bonus.Name = poBonus.Name;
          }
          if(poBonus.IsFlatAmountNull()==false)
          {
            bonus.FlatAmount = poBonus.FlatAmount;
          }
          if(poBonus.IsNoofBasicNull()==false)
          {
            bonus.NoofBasic = poBonus.NoofBasic;
          }
          if(poBonus.IsNoofDaysNull()==false)
          {
            bonus.NoofDays = poBonus.NoofDays;
          }
          if(poBonus.IsIsforConfirmedOnlyNull()==false)
          {
            bonus.IsforConfirmedOnly=poBonus.IsforConfirmedOnly;
          }
          if(poBonus.IsInstallmentNoinYearNull()==false)
          {
            bonus.InstallmentNoinYear = poBonus.InstallmentNoinYear;
          }
          bonusDS.Bonuses.AddBonus( bonus );
          bonusDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( bonusDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
    
      return returnDS;
    
    
    }
  
    private DataSet _updateBonus(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();
      BonusDS bonusDS = new BonusDS();
      DBConnectionDS connDS = new  DBConnectionDS();

      

      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      
      //create command
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateBonus");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      
      bonusDS.Merge( inputDS.Tables[ bonusDS.Bonuses.TableName ], false, MissingSchemaAction.Error );
      bonusDS.AcceptChanges();

      foreach(BonusDS.Bonus bonus in bonusDS.Bonuses)
      {
      
        
        if(bonus.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) bonus.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;
        }
        if(bonus.IsNameNull()==false)
        {
          cmd.Parameters["Name"].Value = (object) bonus.Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value = null;
        }
     

        if(bonus.IsIsforConfirmedOnlyNull()==false)
        {
          cmd.Parameters["IsforConfirmedOnly"].Value = (object) bonus.IsforConfirmedOnly;
        }
        else
        {
          cmd.Parameters["IsforConfirmedOnly"].Value = null;
        }

        if(bonus.IsFlatAmountNull()==false)
        {
          cmd.Parameters["FlatAmount"].Value = (object) bonus.FlatAmount ;
        }
        else
        {
          cmd.Parameters["FlatAmount"].Value = null;
        }
        if(bonus.IsNoofBasicNull()==false)
        {
          cmd.Parameters["NoofBasic"].Value = (object) bonus.NoofBasic ;
        }
        else
        {
          cmd.Parameters["NoofBasic"].Value = null;
        }
        if(bonus.IsNoofDaysNull()==false)
        {
          cmd.Parameters["NoofDays"].Value = (object) bonus.NoofDays ;
        }
        else
        {
          cmd.Parameters["NoofDays"].Value = null;
        }
        if(bonus.IsInstallmentNoinYearNull()==false)
        {
          cmd.Parameters["InstallmentNoinYear"].Value = (object) bonus.InstallmentNoinYear;
        }
        else
        {
          cmd.Parameters["InstallmentNoinYear"].Value = null;
        }
      
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_BONUS_UPD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }

    private DataSet _doesBonusExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesBonusExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
        cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    } 

   
   
  
  
  }
}
