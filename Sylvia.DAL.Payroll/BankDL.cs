/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: March 27, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class BankDL
    {
        public BankDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_BANK_ADD:
                    return _createBank(inputDS);
                case ActionID.ACTION_BANK_UPD:
                    return this._updateBank(inputDS);
                case ActionID.NA_ACTION_GET_BANK_LIST:
                    return _getBankList(inputDS);
                case ActionID.NA_ACTION_GET_BANK:
                    return _getBank(inputDS);
                case ActionID.ACTION_BANK_DEL:
                    return this._deleteBank(inputDS);
                case ActionID.ACTION_DOES_BANK_EXIST:
                    return this._doesBankExist(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _doesBankExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesBankExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createBank(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            BankDS bankDS = new BankDS();
            bankDS.Merge(inputDS.Tables[bankDS.Banks.TableName], false, MissingSchemaAction.Error);
            bankDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateBank");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            
            foreach (BankDS.Bank bank in bankDS.Banks)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                    return UtilDL.GetDBOperationFailed();

                cmd.Parameters["BankId"].Value = (object)genPK;

                if (bank.IsBankCodeNull() == false)
                {
                    cmd.Parameters["BankCode"].Value = (object)bank.BankCode;
                }
                else
                {
                    cmd.Parameters["BankCode"].Value = null;
                }
                if (bank.IsBankNameNull() == false)
                {
                    cmd.Parameters["BankName"].Value = (object)bank.BankName;
                }
                else
                {
                    cmd.Parameters["BankName"].Value = null;
                }
                if (bank.IsBankDescriptionNull() == false)
                {
                    cmd.Parameters["Description"].Value = (object)bank.BankDescription;
                }
                else
                {
                    cmd.Parameters["Description"].Value = null;
                }
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BANK_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateBank(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            BankDS bankDS = new BankDS();
            bankDS.Merge(inputDS.Tables[bankDS.Banks.TableName], false, MissingSchemaAction.Error);
            bankDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateBank");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            foreach (BankDS.Bank bank in bankDS.Banks)
            {
                if (bank.IsBankCodeNull() == false)
                {
                    cmd.Parameters["BankCode"].Value = (object)bank.BankCode;
                }
                else
                {
                    cmd.Parameters["BankCode"].Value = null;
                }
                if (bank.IsBankNameNull() == false)
                {
                    cmd.Parameters["BankName"].Value = (object)bank.BankName;
                }
                else
                {
                    cmd.Parameters["BankName"].Value = null;
                }
                if (bank.IsBankDescriptionNull() == false)
                {
                    cmd.Parameters["Description"].Value = (object)bank.BankDescription;
                }
                else
                {
                    cmd.Parameters["Description"].Value = null;
                }
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BANK_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS

        }

        private DataSet _deleteBank(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteBank");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {

                cmd.Parameters["BankCode"].Value = stringDS.DataStrings[0].StringValue;
            }
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _getBank(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            string scode = stringDS.DataStrings[0].StringValue;
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBank");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["BankCode"].Value = scode;//(object) stringDS.DataStrings[0].StringValue;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            BankPO bankPO = new BankPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              bankPO,
              bankPO.Banks.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            bankPO.AcceptChanges();
            BankDS bankDS = new BankDS();
            if (bankPO.Banks.Count > 0)
            {
                foreach (BankPO.Bank poBank in bankPO.Banks)
                {
                    BankDS.Bank bank = bankDS.Banks.NewBank();
                    if (poBank.IsBankCodeNull() == false)
                    {
                        bank.BankCode = poBank.BankCode;
                    }
                    if (poBank.IsBankNameNull() == false)
                    {
                        bank.BankName = poBank.BankName;
                    }
                    if (poBank.IsBankDescriptionNull() == false)
                    {
                        bank.BankDescription = poBank.BankDescription;
                    }
                    bankDS.Banks.AddBank(bank);
                    bankDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(bankDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getBankList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBankList");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            BankPO bankPO = new BankPO();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              bankPO,
              bankPO.Banks.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            bankPO.AcceptChanges();
            BankDS bankDS = new BankDS();
            if (bankPO.Banks.Count > 0)
            {
                foreach (BankPO.Bank poBank in bankPO.Banks)
                {
                    BankDS.Bank bank = bankDS.Banks.NewBank();

                    if (poBank.IsBankCodeNull() == false)
                    {
                        bank.BankCode = poBank.BankCode;
                    }
                    if (poBank.IsBankNameNull() == false)
                    {
                        bank.BankName = poBank.BankName;
                    }
                    if (poBank.IsBankDescriptionNull() == false)
                    {
                        bank.BankDescription = poBank.BankDescription;
                    }
                    bankDS.Banks.AddBank(bank);
                    bankDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(bankDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
    }
}
