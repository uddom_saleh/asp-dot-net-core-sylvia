using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class ShiftingPlanDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_SHIFTINPLAN_ADD:
                    return _createShiftingPlan(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEELISTFOR_SHIFTINGPLAN:
                    return _getEmployeeListForShiftingPlan(inputDS);
                case ActionID.NA_ACTION_GET_EFFECTIVE_DATE:
                    return this.getEffectiveDate(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEELIST_FOR_FILTER_SHIFTINGPLAN:
                    return this._getEmployeeListForFilterShiftingPlan(inputDS);
                case ActionID.NA_ACTION_GET_SHIFTING_PLAN_SPECIFIC_EMPLYEE:
                    return this._getShiftingListSpecificEmployee(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_FOR_ASSIGNING_SHIFT:
                    return this._getEmployeeForAssignShift(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_FOR_CHANGING_SHIFTPLAN:
                    return this._getEmployeeForChangingShiftplan(inputDS);
                case ActionID.ACTION_SHIFTING_PLAN_UPD:
                    return this._updateShiftingPlan(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_FOR_ADDITIONAL_SHIFTPLAN:
                    return this._getEmployeeForAdditionalShiftPlan(inputDS);
                case ActionID.ACTION_ADDITIONAL_SHIFTING_PLAN_ADD:
                    return this._createAdditionalShiftingPlan(inputDS);
                case ActionID.NA_ACTION_GET_ADDITIONAL_SHIFTINGPLAN:
                    return this._getAdditionalShiftPlan(inputDS);
                case ActionID.NA_ACTION_GET_ADDITIONAL_SHIFT_EXIST:
                    return this._getAdditionalShiftExists(inputDS);
                case ActionID.ACTION_ADDITIONAL_SHIFTING_PLAN_UPD:
                    return this._updateAdditionalShiftPlan(inputDS);
                case ActionID.ACTION_ADDITIONAL_SHIFTING_PLAN_DEL:
                    return this._deleteAdditionalShiftPlan(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }
        private DataSet _createShiftingPlan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ShiftingPlanDS ShfPlanDS = new ShiftingPlanDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            ShfPlanDS.Merge(inputDS.Tables[ShfPlanDS.ShiftingPlan.TableName], false, MissingSchemaAction.Error);
            ShfPlanDS.Merge(inputDS.Tables[ShfPlanDS.ShiftingPlanForGrid.TableName], false, MissingSchemaAction.Error);

            ShfPlanDS.AcceptChanges();


            int i = 0;
            foreach (ShiftingPlanDS.ShiftingPlanForGridRow ShfPlanRow in ShfPlanDS.ShiftingPlanForGrid)
            {

                OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteShiftingPlan");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["SShiftingCode"].Value = ShfPlanDS.ShiftingPlanForGrid[i].ShiftingCode;//(object)ShfPlanRow.ShiftingCode;

                cmd.Parameters["EffectiveDate"].Value = ShfPlanDS.ShiftingPlanForGrid[i].EffectiveDate;//(object)ShfPlanRow.EffectiveDate;

                cmd.Parameters["EmployeeCode"].Value = ShfPlanDS.ShiftingPlanForGrid[i].EmployeeCode;//(object)ShfPlanRow.EffectiveDate;


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                i++;

            }

            foreach (ShiftingPlanDS.ShiftingPlanRow ShfPlanRow in ShfPlanDS.ShiftingPlan)
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateShiftingPlan");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }


                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["SShiftingPlanID"].Value = (object)genPK;


                if (ShfPlanRow.IsShiftingCodeNull() == false)
                {
                    cmd.Parameters["SShiftingCode"].Value = (object)ShfPlanRow.ShiftingCode;
                }
                else
                {
                    cmd.Parameters["SShiftingCode"].Value = System.DBNull.Value;

                }

                if (ShfPlanRow.IsEmployeeCodeNull() == false)
                {
                    cmd.Parameters["SEmployeeCode"].Value = (object)ShfPlanRow.EmployeeCode;
                }
                else
                {
                    break;

                }


                if (ShfPlanRow.IsEffectiveDateNull() == false)
                {
                    cmd.Parameters["EffectiveDate"].Value = (object)ShfPlanRow.EffectiveDate;
                }
                else
                {
                    cmd.Parameters["EffectiveDate"].Value = System.DBNull.Value;

                }

                /////////////////////
                bool bError = false;

                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            //}
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getEmployeeListForShiftingPlan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();


            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            string strShiftingCode = stringDS.DataStrings[0].StringValue;
            //string strSpecificEmployeeForAtt1 = stringDS.DataStrings[1].StringValue;
            DateTime dtEffDate = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeListForShiftingPlan");
            cmd.Parameters["SHIFTINGCODE"].Value = strShiftingCode;
            cmd.Parameters["EFFECTIVEDATE"].Value = dtEffDate;

            //Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //#region Data Load (Jarif 10-Sep-11 (Update))


            //EmployeeDS empDS = new EmployeeDS();
            //nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            ShiftingPlanDS spds = new ShiftingPlanDS();
            nRowAffected = ADOController.Instance.Fill(adapter, spds, spds.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            spds.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(spds);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }
        private DataSet getEffectiveDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            string ShiftingCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            ShiftingCode = stringDS.DataStrings[0].StringValue;

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEffectiveDate");
            cmd.Parameters["ShiftingCode"].Value = ShiftingCode;

            Debug.Assert(cmd != null);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            EmployeePO empPO = new EmployeePO();

            bool bError = false;
            int nRowAffected = -1;




            ShiftingPlanDS spds = new ShiftingPlanDS();
            nRowAffected = ADOController.Instance.Fill(adapter, spds, spds.GetEffectiveDate.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            spds.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(spds);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        private DataSet _getEmployeeListForFilterShiftingPlan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();


            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = new OleDbCommand();
            string strShiftingCode = stringDS.DataStrings[0].StringValue;
            if (stringDS.DataStrings[1].StringValue != "")
            {
                DateTime dtEffDate = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeListForFilterShiftingPlan");
                cmd.Parameters["SHIFTINGCODE"].Value = strShiftingCode;
                cmd.Parameters["EFFECTIVEDATE"].Value = dtEffDate;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeListForFilterShiftingPlan_OnlyShift");

            }
            //Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //#region Data Load (Jarif 10-Sep-11 (Update))


            //EmployeeDS empDS = new EmployeeDS();
            //nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            ShiftingPlanDS spds = new ShiftingPlanDS();
            nRowAffected = ADOController.Instance.Fill(adapter, spds, spds.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            spds.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(spds);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }
        private DataSet _getShiftingListSpecificEmployee(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            ShiftingPlanDS SpDS = new ShiftingPlanDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();

            string eCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            DateTime AttDate = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            string strLoginID = stringDS.DataStrings[1].StringValue;
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("getShiftingPlanListSpecificEmployee");
            cmd.Parameters["EFFECTIVEDATE"].Value = AttDate;

            cmd.Parameters["LOGINID"].Value = strLoginID;


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, SpDS, SpDS.ScheduleShifting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            SpDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(SpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getEmployeeForAssignShift(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            int EmployeeType = 0;
            int EmployeeCategory = 0;
            int groupID = 0;
            int shiftingID = 0;

            ////get date
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSystemDateTimeForOracle");
            //if (cmd == null)
            //{
            //    return UtilDL.GetCommandNotFound();
            //}
            //OleDbDataAdapter adapter = new OleDbDataAdapter();
            //adapter.SelectCommand = cmd;
            //SystemDatePO datePO = new SystemDatePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, datePO, datePO.SystemDates.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_SYSTEM_DATETIME_GET.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}
            //datePO.AcceptChanges();
            //DateTime systemDate = new DateTime();
            //if (datePO.SystemDates.Count > 0)
            //{
            //    systemDate = datePO.SystemDates[0].SysDate;
            //}

            //main
            OleDbCommand cmd2 = new OleDbCommand();
            groupID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            if (stringDS.DataStrings[1].StringValue != "")
            {
                shiftingID = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            }
            EmployeeType = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
            EmployeeCategory = Convert.ToInt32(stringDS.DataStrings[5].StringValue);

            string employeeCode = stringDS.DataStrings[9].StringValue;
            if (employeeCode == "") employeeCode = "-1";
            DateTime EffectiveDate = dateDS.DataDates[0].DateValue;

            if (cmd2 != null)
            {
                cmd2 = DBCommandProvider.GetDBCommand("GetEmployeeForAssiningShift");
                cmd2.Parameters["GroupID"].Value = groupID;
                cmd2.Parameters["ShiftingID"].Value = shiftingID;
                cmd2.Parameters["EffectiveDate"].Value = EffectiveDate;
                cmd2.Parameters["SiteCode"].Value = stringDS.DataStrings[3].StringValue;
                cmd2.Parameters["GroupID1"].Value = groupID;
                cmd2.Parameters["ShiftingID1"].Value = shiftingID;
                cmd2.Parameters["CompanyCode"].Value = stringDS.DataStrings[2].StringValue;
                cmd2.Parameters["CompanyCode1"].Value = stringDS.DataStrings[2].StringValue;
                cmd2.Parameters["SiteCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd2.Parameters["SiteCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd2.Parameters["EmployeeType"].Value = EmployeeType;
                cmd2.Parameters["EmployeeType1"].Value = EmployeeType;
                cmd2.Parameters["EmployeeCategory"].Value = EmployeeCategory;
                cmd2.Parameters["EmployeeCategory1"].Value = EmployeeCategory;
                cmd2.Parameters["DepartmentCode"].Value = stringDS.DataStrings[6].StringValue;
                cmd2.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd2.Parameters["DesignationCode"].Value = stringDS.DataStrings[7].StringValue;
                cmd2.Parameters["DesignationCode1"].Value = stringDS.DataStrings[7].StringValue;
                cmd2.Parameters["FunctionCode"].Value = stringDS.DataStrings[7].StringValue;
                cmd2.Parameters["FunctionCode1"].Value = stringDS.DataStrings[7].StringValue;
                cmd2.Parameters["EmployeeCode"].Value = employeeCode;
                cmd2.Parameters["EmployeeCode1"].Value = employeeCode;
            }
            else return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter2 = new OleDbDataAdapter();
            adapter2.SelectCommand = cmd2;

            bool bError2 = false;
            int nRowAffected2 = -1;

            ShiftingPlanDS spds = new ShiftingPlanDS();
            nRowAffected2 = ADOController.Instance.Fill(adapter2, spds, spds.ShiftingPlanFilter.TableName, connDS.DBConnections[0].ConnectionID, ref bError2);
            if (bError2)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_FOR_ASSIGNING_SHIFT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            spds.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(spds);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getEmployeeForChangingShiftplan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            int EmployeeType = 0;
            int EmployeeCategory = 0;
            int groupID = 0;
            int shiftingID = 0;
            int newShiftingID = 0;

            OleDbCommand cmd = new OleDbCommand();
            groupID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            if (stringDS.DataStrings[1].StringValue != "")
            {
                shiftingID = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            }
            EmployeeType = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
            EmployeeCategory = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            newShiftingID = Convert.ToInt32(stringDS.DataStrings[10].StringValue);
            string employeeCode = stringDS.DataStrings[9].StringValue;
            if (employeeCode == "") employeeCode = "-1";
            DateTime EffectiveDate = dateDS.DataDates[0].DateValue;

            if (cmd != null)
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeForChangingShiftPlan");
                cmd.Parameters["GroupID"].Value = groupID;
                cmd.Parameters["ShiftingID"].Value = shiftingID;
                cmd.Parameters["SiteCode"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["GroupID1"].Value = groupID;
                cmd.Parameters["ShiftingID1"].Value = shiftingID;
                cmd.Parameters["newShiftingID"].Value = newShiftingID;
                cmd.Parameters["EffectiveDate"].Value = EffectiveDate;
                cmd.Parameters["GroupID2"].Value = groupID;
                cmd.Parameters["GroupID3"].Value = groupID;
                cmd.Parameters["ShiftingID2"].Value = shiftingID;
                cmd.Parameters["ShiftingID3"].Value = shiftingID;
                cmd.Parameters["CompanyCode"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["CompanyCode1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeType"].Value = EmployeeType;
                cmd.Parameters["EmployeeType1"].Value = EmployeeType;
                cmd.Parameters["EmployeeCategory"].Value = EmployeeCategory;
                cmd.Parameters["EmployeeCategory1"].Value = EmployeeCategory;
                cmd.Parameters["DepartmentCode"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DesignationCode"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["FunctionCode"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["FunctionCode1"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["EmployeeCode"].Value = employeeCode;
                cmd.Parameters["EmployeeCode1"].Value = employeeCode;
            }
            else return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            ShiftingPlanDS spds = new ShiftingPlanDS();
            nRowAffected = ADOController.Instance.Fill(adapter, spds, spds.ShiftingPlanFilter.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_FOR_CHANGING_SHIFTPLAN.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            spds.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(spds);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getEmployeeForAdditionalShiftPlan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            int EmployeeType = 0;
            int EmployeeCategory = 0;
            int groupID = 0;
            int shiftingID = 0;
            int newShiftingID = 0;

            OleDbCommand cmd = new OleDbCommand();
            groupID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            if (stringDS.DataStrings[1].StringValue != "")
            {
                shiftingID = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            }
            EmployeeType = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
            EmployeeCategory = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            newShiftingID = Convert.ToInt32(stringDS.DataStrings[10].StringValue);
            string employeeCode = stringDS.DataStrings[9].StringValue;
            if (employeeCode == "") employeeCode = "-1";
            DateTime EffectiveDate = dateDS.DataDates[0].DateValue;

            if (cmd != null)
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeForAdditionalShiftPlan");
                cmd.Parameters["EffectiveDate"].Value = EffectiveDate;
                cmd.Parameters["newShiftingID"].Value = newShiftingID;
                cmd.Parameters["EffectiveDate1"].Value = EffectiveDate;
                cmd.Parameters["GroupID"].Value = groupID;
                cmd.Parameters["GroupID1"].Value = groupID;
                cmd.Parameters["ShiftingID"].Value = shiftingID;
                cmd.Parameters["ShiftingID1"].Value = shiftingID;
                cmd.Parameters["CompanyCode"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["CompanyCode1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["SiteCode"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeType"].Value = EmployeeType;
                cmd.Parameters["EmployeeType1"].Value = EmployeeType;
                cmd.Parameters["EmployeeCategory"].Value = EmployeeCategory;
                cmd.Parameters["EmployeeCategory1"].Value = EmployeeCategory;
                cmd.Parameters["DepartmentCode"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DesignationCode"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["FunctionCode"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["FunctionCode1"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["EmployeeCode"].Value = employeeCode;
                cmd.Parameters["EmployeeCode1"].Value = employeeCode;
            }
            else return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            ShiftingPlanDS spds = new ShiftingPlanDS();
            nRowAffected = ADOController.Instance.Fill(adapter, spds, spds.ShiftingPlanFilter.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_FOR_ADDITIONAL_SHIFTPLAN.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            spds.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(spds);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateShiftingPlan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ShiftingPlanDS planDS = new ShiftingPlanDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            planDS.Merge(inputDS.Tables[planDS.ShiftingPlanFilter.TableName], false, MissingSchemaAction.Error);
            planDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            foreach (ShiftingPlanDS.ShiftingPlanFilterRow row in planDS.ShiftingPlanFilter.Rows)
            {
                OleDbCommand cmdNo = new OleDbCommand();
                cmdNo = DBCommandProvider.GetDBCommand("GetAddShiftPlanForRestrictChangingPlan");

                if (cmdNo == null) return UtilDL.GetCommandNotFound();

                OleDbDataAdapter adapterNo = new OleDbDataAdapter();
                adapterNo.SelectCommand = cmdNo;

                cmdNo.Parameters["EmployeeID"].Value = row.EmployeeID;
                cmdNo.Parameters["Effect_Date_From"].Value = stringDS.DataStrings[0].StringValue;
                cmdNo.Parameters["Effect_Date_To"].Value = stringDS.DataStrings[1].StringValue;

                bool bErrorCheck = false;
                int nRowAffectedCheck = -1;

                ShiftingPlanDS spds = new ShiftingPlanDS();
                nRowAffectedCheck = ADOController.Instance.Fill(adapterNo, spds, spds.ShiftingPlanFilter.TableName, connDS.DBConnections[0].ConnectionID, ref bErrorCheck);
                if (bErrorCheck)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SHIFTING_PLAN_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                spds.AcceptChanges();

                if (spds.ShiftingPlanFilter[0].AdditionalShiftCount > 0)
                {
                    messageDS.SuccessMsg.AddSuccessMsgRow("Additional Shift Assinged On That Day");

                    DataSet retDS = new DataSet();
                    retDS.Merge(errDS);
                    retDS.Merge(messageDS);
                    return retDS;
                }
                else
                {

                    OleDbCommand cmd = new OleDbCommand();
                    cmd.CommandText = "PRO_SHIFTING_PLAN_UPD";
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (cmd == null) return UtilDL.GetCommandNotFound();

                    long userID = UtilDL.GetUserId(connDS, stringDS.DataStrings[2].StringValue);

                    cmd.Parameters.AddWithValue("p_GroupID", row.GroupID);
                    cmd.Parameters.AddWithValue("p_ShiftingID", row.ShiftingID);
                    cmd.Parameters.AddWithValue("p_Update_User", userID);
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    if (!stringDS.DataStrings[0].IsStringValueNull()) cmd.Parameters.AddWithValue("p_Effect_Date_From", stringDS.DataStrings[0].StringValue);
                    else cmd.Parameters.AddWithValue("p_Effect_Date_From", DBNull.Value);
                    if (!stringDS.DataStrings[1].IsStringValueNull()) cmd.Parameters.AddWithValue("p_Effect_Date_To", stringDS.DataStrings[1].StringValue);
                    else cmd.Parameters.AddWithValue("p_Effect_Date_To", DBNull.Value);
                    cmd.Parameters.AddWithValue("p_LoginStatus", row.LoginStatus);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_SHIFTING_PLAN_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(stringDS.DataStrings[0].StringValue);
                    else messageDS.ErrorMsg.AddErrorMsgRow(stringDS.DataStrings[0].StringValue);
                    cmd.Parameters.Clear();
                }
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;

        }
        private DataSet _createAdditionalShiftingPlan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ShiftingPlanDS planDS = new ShiftingPlanDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            planDS.Merge(inputDS.Tables[planDS.ShiftingPlanFilter.TableName], false, MissingSchemaAction.Error);
            planDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            foreach (ShiftingPlanDS.ShiftingPlanFilterRow row in planDS.ShiftingPlanFilter.Rows)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ADDITIONAL_SHIFT_PLAN_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                long userID = UtilDL.GetUserId(connDS, stringDS.DataStrings[2].StringValue);

                cmd.Parameters.AddWithValue("p_GroupID", row.GroupID);
                cmd.Parameters.AddWithValue("p_ShiftingID", row.ShiftingID);
                cmd.Parameters.AddWithValue("p_Create_User", userID);
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                if (!stringDS.DataStrings[1].IsStringValueNull()) cmd.Parameters.AddWithValue("p_Holidays_TypeID", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
                else cmd.Parameters.AddWithValue("p_Holidays_TypeID", DBNull.Value);
                if (!stringDS.DataStrings[0].IsStringValueNull()) cmd.Parameters.AddWithValue("p_EffectDate", stringDS.DataStrings[0].StringValue);
                else cmd.Parameters.AddWithValue("p_EffectDate", DBNull.Value);
                cmd.Parameters.AddWithValue("p_IsConsecutive", row.IsConsecutive);
                cmd.Parameters.AddWithValue("p_IsNightShift", row.IsNightShift);
                cmd.Parameters.AddWithValue("p_MaxOvertime", row.MaxOvertimeLimit);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ADDITIONAL_SHIFTING_PLAN_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(stringDS.DataStrings[0].StringValue);
                else messageDS.ErrorMsg.AddErrorMsgRow(stringDS.DataStrings[0].StringValue);
                cmd.Parameters.Clear();
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _getAdditionalShiftPlan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            int EmployeeType = 0;
            int EmployeeCategory = 0;
            int groupID = 0;
            int shiftingID = 0;

            OleDbCommand cmd = new OleDbCommand();
            groupID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            if (stringDS.DataStrings[1].StringValue != "")
            {
                shiftingID = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            }
            EmployeeType = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
            EmployeeCategory = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            string employeeCode = stringDS.DataStrings[9].StringValue;
            if (employeeCode == "") employeeCode = "-1";
            DateTime FromDate = dateDS.DataDates[0].DateValue;
            DateTime ToDate = dateDS.DataDates[1].DateValue;

            if (cmd != null)
            {
                cmd = DBCommandProvider.GetDBCommand("GetAdditionalShiftPlan");
                cmd.Parameters["FromDate"].Value = FromDate;
                cmd.Parameters["ToDate"].Value = ToDate;
                cmd.Parameters["GroupID"].Value = groupID;
                cmd.Parameters["GroupID1"].Value = groupID;
                cmd.Parameters["ShiftingID"].Value = shiftingID;
                cmd.Parameters["ShiftingID1"].Value = shiftingID;
                cmd.Parameters["CompanyCode"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["CompanyCode1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["SiteCode"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeType"].Value = EmployeeType;
                cmd.Parameters["EmployeeType1"].Value = EmployeeType;
                cmd.Parameters["EmployeeCategory"].Value = EmployeeCategory;
                cmd.Parameters["EmployeeCategory1"].Value = EmployeeCategory;
                cmd.Parameters["DepartmentCode"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DesignationCode"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["FunctionCode"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["FunctionCode1"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["EmployeeCode"].Value = employeeCode;
                cmd.Parameters["EmployeeCode1"].Value = employeeCode;
            }
            else return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            ShiftingPlanDS spds = new ShiftingPlanDS();
            nRowAffected = ADOController.Instance.Fill(adapter, spds, spds.ShiftingPlanFilter.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ADDITIONAL_SHIFTINGPLAN.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            spds.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(spds);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAdditionalShiftExists(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            int shiftingID = Convert.ToInt32(stringDS.DataStrings[1].StringValue);

            string employeeCode = stringDS.DataStrings[0].StringValue;

            DateTime EffectDate = dateDS.DataDates[0].DateValue;

            if (cmd != null)
            {
                cmd = DBCommandProvider.GetDBCommand("GetAdditionalExistence");
                cmd.Parameters["EffectDate"].Value = EffectDate;
                cmd.Parameters["EffectDate1"].Value = EffectDate;
                cmd.Parameters["ShiftingID"].Value = shiftingID;
                cmd.Parameters["EmployeeCode"].Value = employeeCode;
            }
            else return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            ShiftingPlanDS spds = new ShiftingPlanDS();
            nRowAffected = ADOController.Instance.Fill(adapter, spds, spds.ShiftingPlanFilter.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ADDITIONAL_SHIFT_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            spds.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(spds);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateAdditionalShiftPlan(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            ShiftingPlanDS planDS = new ShiftingPlanDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            planDS.Merge(inputDS.Tables[planDS.ShiftingPlanFilter.TableName], false, MissingSchemaAction.Error);
            planDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            foreach (ShiftingPlanDS.ShiftingPlanFilterRow row in planDS.ShiftingPlanFilter.Rows)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ADDITIONAL_SHIFT_PLAN_UPD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                long userID = UtilDL.GetUserId(connDS, stringDS.DataStrings[2].StringValue);

                cmd.Parameters.AddWithValue("p_AdditionalID", Convert.ToInt32(stringDS.DataStrings[3].StringValue));
                cmd.Parameters.AddWithValue("p_GroupID", row.GroupID);
                cmd.Parameters.AddWithValue("p_ShiftingID", row.ShiftingID);
                cmd.Parameters.AddWithValue("p_Create_User", userID);
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                if (!stringDS.DataStrings[1].IsStringValueNull()) cmd.Parameters.AddWithValue("p_Holidays_TypeID", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
                else cmd.Parameters.AddWithValue("p_Holidays_TypeID", DBNull.Value);
                if (!stringDS.DataStrings[0].IsStringValueNull()) cmd.Parameters.AddWithValue("p_EffectDate", stringDS.DataStrings[0].StringValue);
                else cmd.Parameters.AddWithValue("p_EffectDate", DBNull.Value);
                //cmd.Parameters.AddWithValue("p_IsConsecutive", row.IsConsecutive);
                cmd.Parameters.AddWithValue("p_IsNightShift", row.IsNightShift);
                cmd.Parameters.AddWithValue("p_MaxOvertime", row.MaxOvertimeLimit);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ADDITIONAL_SHIFTING_PLAN_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(stringDS.DataStrings[0].StringValue);
                else messageDS.ErrorMsg.AddErrorMsgRow(stringDS.DataStrings[0].StringValue);
                cmd.Parameters.Clear();
            }
            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _deleteAdditionalShiftPlan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ShiftingPlanDS planDS = new ShiftingPlanDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            planDS.Merge(inputDS.Tables[planDS.ShiftingPlanFilter.TableName], false, MissingSchemaAction.Error);
            planDS.AcceptChanges();

            foreach (ShiftingPlanDS.ShiftingPlanFilterRow row in planDS.ShiftingPlanFilter.Rows)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ADDITIONAL_SHIFT_PLAN_DEL";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_AdditionalID", row.Att_Additional_ID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ADDITIONAL_SHIFTING_PLAN_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(planDS.ShiftingPlanFilter[0].EmployeeCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(planDS.ShiftingPlanFilter[0].EmployeeCode);
                cmd.Parameters.Clear();
            }
            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
    }
}
