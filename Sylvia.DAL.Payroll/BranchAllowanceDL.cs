using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class BranchAllowanceDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_BRANCH_ALLOWANCE_ADD:
                    return this.createBranchAllowance(inputDS);

                case ActionID.NA_ACTION_GET_BRANCH_ALLOWANCE_BY_PARAM:
                    return this.getBanchAllowanceListByParam(inputDS);

                case ActionID.NA_ACTION_GET_ALL_BRANCH_ALLOWANCE_SP:
                    return this.getAllBanchAllowanceList(inputDS);

                case ActionID.NA_ACTION_GET_LIABILITY_ALLOWANCE_BY_PARAM:
                    return this.getLiabilityAllowanceListByParam(inputDS);
                case ActionID.ACTION_BRANCH_LIABILITY_ALLOWANCE_ADD:
                    return this.createBranch_LiabilityAllowance(inputDS);

                case ActionID.NA_ACTION_GET_ALL_LIABILITY_ALLOWANCE_SP:
                    return this.getAllLiabilityAllowanceList(inputDS);

                case ActionID.NA_ACTION_GET_DESIGNATION_ALLOWANCE_BY_PARAM:
                    return this.getDesignationAllowanceListByParam(inputDS);

                case ActionID.ACTION_BRANCH_DESIGNATION_ALLOWANCE_ADD:
                    return this.createBranch_DesignationAllowance(inputDS);

                case ActionID.NA_ACTION_GET_DEPARTMENT_ALLOWANCE_BY_PARAM:
                    return this.getDepartmentAllowanceListByParam(inputDS);

                case ActionID.ACTION_BRANCH_DEPARTMENT_ALLOWANCE_ADD:
                    return this.createBranch_DepartmentAllowance(inputDS);

                case ActionID.NA_ACTION_GET_ALL_DESIGNATION_ALLOWANCE_SP:
                    return this.getAllDesignationAllowanceList(inputDS);

                case ActionID.NA_ACTION_GET_ALL_DEPARTMENT_ALLOWANCE_SP:
                    return this.getAllDepartmentAllowanceList(inputDS);


                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet createBranchAllowance(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BranchAllowanceDS branchAllowDS = new BranchAllowanceDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            branchAllowDS.Merge(inputDS.Tables[branchAllowDS.BranchAllowance.TableName], false, MissingSchemaAction.Error);
            branchAllowDS.AcceptChanges();

            bool bError;
            int nRowAffected;   

            #region Previous Data Deletion...
            foreach (BranchAllowanceDS.BranchAllowanceRow Row in branchAllowDS.BranchAllowance)
            {
 
                cmd_Del.CommandText = "PRO_BRANCH_ALLOWANCE_DEL";
                cmd_Del.CommandType = CommandType.StoredProcedure;
                cmd_Del.Parameters.AddWithValue("p_ADID", Row.ADID);
                cmd_Del.Parameters.AddWithValue("p_LogedinUserID", Row.LogedinUserID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BRANCH_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd_Del.Parameters.Clear();
                break;
            }
            #endregion


            #region Data Insert...
            cmd.CommandText = "PRO_BRANCH_ALLOWANCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (BranchAllowanceDS.BranchAllowanceRow branchAllow in branchAllowDS.BranchAllowance)
            {
                if (branchAllow.SiteID == 0) continue;

                cmd.Parameters.AddWithValue("p_ADID", branchAllow.ADID);
                cmd.Parameters.AddWithValue("p_SiteID", branchAllow.SiteID);

                if (!branchAllow.IsFlatAmount_0Null()) cmd.Parameters.AddWithValue("p_FlatAmount_0", branchAllow.FlatAmount_0);
                else cmd.Parameters.AddWithValue("p_FlatAmount_0", DBNull.Value);

                if (!branchAllow.IsPercentOfBasic_0Null()) cmd.Parameters.AddWithValue("p_PercentOfBasic_0", branchAllow.PercentOfBasic_0);
                else cmd.Parameters.AddWithValue("p_PercentOfBasic_0", DBNull.Value);

                if (!branchAllow.IsUpToMonth_0Null()) cmd.Parameters.AddWithValue("p_UpToMonth_0", branchAllow.UpToMonth_0);
                else cmd.Parameters.AddWithValue("p_UpToMonth_0", DBNull.Value);

                if (!branchAllow.IsBasedOn_0Null()) cmd.Parameters.AddWithValue("p_BasedOn_0", branchAllow.BasedOn_0);
                else cmd.Parameters.AddWithValue("p_BasedOn_0", DBNull.Value);



                if (!branchAllow.IsFlatAmount_1Null()) cmd.Parameters.AddWithValue("p_FlatAmount_1", branchAllow.FlatAmount_1);
                else cmd.Parameters.AddWithValue("p_FlatAmount_1", DBNull.Value);

                if (!branchAllow.IsPercentOfBasic_1Null()) cmd.Parameters.AddWithValue("p_PercentOfBasic_1", branchAllow.PercentOfBasic_1);
                else cmd.Parameters.AddWithValue("p_PercentOfBasic_1", DBNull.Value);

                if (!branchAllow.IsUpToMonth_1Null()) cmd.Parameters.AddWithValue("p_UpToMonth_1", branchAllow.UpToMonth_1);
                else cmd.Parameters.AddWithValue("p_UpToMonth_1", DBNull.Value);

                if (!branchAllow.IsBasedOn_1Null()) cmd.Parameters.AddWithValue("p_BasedOn_1", branchAllow.BasedOn_1);
                else cmd.Parameters.AddWithValue("p_BasedOn_1", DBNull.Value);



                if (!branchAllow.IsFlatAmount_2Null()) cmd.Parameters.AddWithValue("p_FlatAmount_2", branchAllow.FlatAmount_2);
                else cmd.Parameters.AddWithValue("p_FlatAmount_2", DBNull.Value);

                if (!branchAllow.IsPercentOfBasic_2Null()) cmd.Parameters.AddWithValue("p_PercentOfBasic_2", branchAllow.PercentOfBasic_2);
                else cmd.Parameters.AddWithValue("p_PercentOfBasic_2", DBNull.Value);

                if (!branchAllow.IsUpToMonth_2Null()) cmd.Parameters.AddWithValue("p_UpToMonth_2", branchAllow.UpToMonth_2);
                else cmd.Parameters.AddWithValue("p_UpToMonth_2", DBNull.Value);

                if (!branchAllow.IsBasedOn_2Null()) cmd.Parameters.AddWithValue("p_BasedOn_2", branchAllow.BasedOn_2);
                else cmd.Parameters.AddWithValue("p_BasedOn_2", DBNull.Value);

                cmd.Parameters.AddWithValue("p_LogedinUserID", branchAllow.LogedinUserID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BRANCH_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet getBanchAllowanceListByParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetBranchWiseAllowanceByADID");
            cmd.Parameters["ADID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
  
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BranchAllowanceDS BranchAllowDS = new BranchAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, BranchAllowDS, BranchAllowDS.BranchAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BRANCH_ALLOWANCE_BY_PARAM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            BranchAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(BranchAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet getAllBanchAllowanceList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAllBranchAllowanceList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BranchAllowanceDS BranchAllowDS = new BranchAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, BranchAllowDS, BranchAllowDS.BranchAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALL_BRANCH_ALLOWANCE_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            BranchAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(BranchAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet getLiabilityAllowanceListByParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLiabilityWiseAllowByADID_LiID");
            cmd.Parameters["ADID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["LiabilityID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
           

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BranchAllowanceDS BranchAllowDS = new BranchAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, BranchAllowDS, BranchAllowDS.BranchAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BRANCH_ALLOWANCE_BY_PARAM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            BranchAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(BranchAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet createBranch_LiabilityAllowance(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BranchAllowanceDS branchAllowDS = new BranchAllowanceDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            branchAllowDS.Merge(inputDS.Tables[branchAllowDS.BranchAllowance.TableName], false, MissingSchemaAction.Error);
            branchAllowDS.AcceptChanges();

            bool bError;
            int nRowAffected;

            #region Previous Data Deletion...
            foreach (BranchAllowanceDS.BranchAllowanceRow Row in branchAllowDS.BranchAllowance)
            {

                cmd_Del.CommandText = "PRO_LIABILITY_ALLOWANCE_DEL";
                cmd_Del.CommandType = CommandType.StoredProcedure;
                cmd_Del.Parameters.AddWithValue("p_ADID", Row.ADID);
                cmd_Del.Parameters.AddWithValue("p_LiabilityID", Row.LiabilityID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BRANCH_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd_Del.Parameters.Clear();
                break;
            }
            #endregion


            #region Data Insert...
            cmd.CommandText = "PRO_LIABILITY_ALLOWANCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (BranchAllowanceDS.BranchAllowanceRow branchAllow in branchAllowDS.BranchAllowance)
            {
                if (branchAllow.SiteID == 0) continue;

                cmd.Parameters.AddWithValue("p_ADID", branchAllow.ADID);
                cmd.Parameters.AddWithValue("p_LiabilityID", branchAllow.LiabilityID);
                cmd.Parameters.AddWithValue("p_SiteID", branchAllow.SiteID);

                if (!branchAllow.IsFlatAmountNull()) cmd.Parameters.AddWithValue("p_FlatAmount", branchAllow.FlatAmount);
                else cmd.Parameters.AddWithValue("p_FlatAmount", DBNull.Value);

                if (!branchAllow.IsPercentOfBasicNull()) cmd.Parameters.AddWithValue("p_PercentOfBasic", branchAllow.PercentOfBasic);
                else cmd.Parameters.AddWithValue("p_PercentOfBasic", DBNull.Value);

                cmd.Parameters.AddWithValue("p_LogedinUserID", branchAllow.LogedinUserID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BRANCH_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet getAllLiabilityAllowanceList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAllLiabilityAllowanceList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BranchAllowanceDS BranchAllowDS = new BranchAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, BranchAllowDS, BranchAllowDS.BranchAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALL_LIABILITY_ALLOWANCE_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            BranchAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(BranchAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet getDesignationAllowanceListByParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetDesignationWiseAllowByADID_DESID");
            cmd.Parameters["ADID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["DesignationID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BranchAllowanceDS BranchAllowDS = new BranchAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, BranchAllowDS, BranchAllowDS.BranchAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DESIGNATION_ALLOWANCE_BY_PARAM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            BranchAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(BranchAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet createBranch_DesignationAllowance(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BranchAllowanceDS branchAllowDS = new BranchAllowanceDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            branchAllowDS.Merge(inputDS.Tables[branchAllowDS.BranchAllowance.TableName], false, MissingSchemaAction.Error);
            branchAllowDS.AcceptChanges();

            bool bError;
            int nRowAffected;

            #region Previous Data Deletion...
            foreach (BranchAllowanceDS.BranchAllowanceRow Row in branchAllowDS.BranchAllowance)
            {

                cmd_Del.CommandText = "PRO_DESIGNATION_ALLOWANCE_DEL";
                cmd_Del.CommandType = CommandType.StoredProcedure;
                cmd_Del.Parameters.AddWithValue("p_ADID", Row.ADID);
                cmd_Del.Parameters.AddWithValue("p_DesignationID", Row.DesignationID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BRANCH_DESIGNATION_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd_Del.Parameters.Clear();
                break;
            }
            #endregion


            #region Data Insert...
            cmd.CommandText = "PRO_DESIGNATION_ALLOWANCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (BranchAllowanceDS.BranchAllowanceRow branchAllow in branchAllowDS.BranchAllowance)
            {
                if (branchAllow.SiteID == 0) continue;

                cmd.Parameters.AddWithValue("p_ADID", branchAllow.ADID);
                cmd.Parameters.AddWithValue("p_DesignationID", branchAllow.DesignationID);
                cmd.Parameters.AddWithValue("p_SiteID", branchAllow.SiteID);

                if (!branchAllow.IsFlatAmountNull()) cmd.Parameters.AddWithValue("p_FlatAmount", branchAllow.FlatAmount);
                else cmd.Parameters.AddWithValue("p_FlatAmount", DBNull.Value);

                if (!branchAllow.IsPercentOfBasicNull()) cmd.Parameters.AddWithValue("p_PercentOfBasic", branchAllow.PercentOfBasic);
                else cmd.Parameters.AddWithValue("p_PercentOfBasic", DBNull.Value);

                cmd.Parameters.AddWithValue("p_LogedinUserID", branchAllow.LogedinUserID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BRANCH_DESIGNATION_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet getDepartmentAllowanceListByParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetDepartmentWiseAllowByADID_DEPID");
            cmd.Parameters["ADID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BranchAllowanceDS BranchAllowDS = new BranchAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, BranchAllowDS, BranchAllowDS.BranchAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENT_ALLOWANCE_BY_PARAM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            BranchAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(BranchAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet createBranch_DepartmentAllowance(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BranchAllowanceDS branchAllowDS = new BranchAllowanceDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            branchAllowDS.Merge(inputDS.Tables[branchAllowDS.BranchAllowance.TableName], false, MissingSchemaAction.Error);
            branchAllowDS.AcceptChanges();

            bool bError;
            int nRowAffected;

            #region Previous Data Deletion...
            foreach (BranchAllowanceDS.BranchAllowanceRow Row in branchAllowDS.BranchAllowance)
            {

                cmd_Del.CommandText = "PRO_DEPARTMENT_ALLOWANCE_DEL";
                cmd_Del.CommandType = CommandType.StoredProcedure;
                cmd_Del.Parameters.AddWithValue("p_ADID", Row.ADID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BRANCH_DEPARTMENT_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd_Del.Parameters.Clear();
                break;
            }
            #endregion


            #region Data Insert...
            cmd.CommandText = "PRO_DEPARTMENT_ALLOWANCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (BranchAllowanceDS.BranchAllowanceRow branchAllow in branchAllowDS.BranchAllowance)
            {
                if (branchAllow.DepartmentID == 0) continue;

                cmd.Parameters.AddWithValue("p_ADID", branchAllow.ADID);
                cmd.Parameters.AddWithValue("p_DepartmentID", branchAllow.DepartmentID);

                if (!branchAllow.IsFlatAmountNull()) cmd.Parameters.AddWithValue("p_FlatAmount", branchAllow.FlatAmount);
                else cmd.Parameters.AddWithValue("p_FlatAmount", DBNull.Value);

                if (!branchAllow.IsPercentOfBasicNull()) cmd.Parameters.AddWithValue("p_PercentOfBasic", branchAllow.PercentOfBasic);
                else cmd.Parameters.AddWithValue("p_PercentOfBasic", DBNull.Value);

                cmd.Parameters.AddWithValue("p_LogedinUserID", branchAllow.LogedinUserID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BRANCH_DEPARTMENT_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet getAllDesignationAllowanceList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAllDesigAllowList_SP");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BranchAllowanceDS BranchAllowDS = new BranchAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, BranchAllowDS, BranchAllowDS.BranchAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALL_DESIGNATION_ALLOWANCE_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            BranchAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(BranchAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet getAllDepartmentAllowanceList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAllDeptAllowList_SP");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            BranchAllowanceDS BranchAllowDS = new BranchAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, BranchAllowDS, BranchAllowDS.BranchAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALL_DEPARTMENT_ALLOWANCE_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            BranchAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(BranchAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

    }
}
