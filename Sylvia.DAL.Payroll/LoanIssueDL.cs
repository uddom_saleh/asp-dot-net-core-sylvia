using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for LoanIssueDL.
    /// </summary>
    public class LoanIssueDL
    {
        public LoanIssueDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_LOANISSUE_ADD:
                    return _createLoanIssue(inputDS);
                case ActionID.NA_ACTION_Q_LOANISSUE_ALL:
                    return _getLoanIssueList(inputDS);
                case ActionID.ACTION_LOANISSUE_DEL:
                    return this._deleteLoanIssue(inputDS);
                case ActionID.NA_ACTION_LOANISSUE_GET:
                    return this._getLoanIssue(inputDS);
                case ActionID.ACTION_LOANISSUE_UPD:
                    return this._updateLoanIssue(inputDS);
                case ActionID.ACTION_DOES_LOANISSUE_EXIST:
                    return this._doesLoanIssueExist(inputDS);
                case ActionID.NA_ACTION_GET_LOANISSUE:
                    return this._getLoanIssueListByParam(inputDS);
                case ActionID.ACTION_LOANISSUE_ADD_BULK:
                    return this._createBulkLoanIssue(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }
        private DataSet _createLoanIssue(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            LoanIssueDS loanIssueDS = new LoanIssueDS();
            DBConnectionDS connDS = new DBConnectionDS();
            long nId = -1;



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();



            loanIssueDS.Merge(inputDS.Tables[loanIssueDS.LoanIssues.TableName], false, MissingSchemaAction.Error);
            loanIssueDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateLoanIssue");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            long loanIssueID;
            foreach (LoanIssueDS.LoanIssue loanIssue in loanIssueDS.LoanIssues)
            {

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["Id"].Value = (object)genPK;
                loanIssueID = genPK;
                if (loanIssue.IsLoanIssueCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)loanIssue.LoanIssueCode;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }

                if (loanIssue.IsLoanCodeNull() == false)
                {
                    nId = UtilDL.GetLoanId(connDS, loanIssue.LoanCode);
                    if (nId == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }
                    cmd.Parameters["LoanId"].Value = (object)nId;
                }
                else
                {
                    cmd.Parameters["LoanId"].Value = null;
                }
                if (loanIssue.IsEmployeeCodeNull() == false)
                {
                    nId = UtilDL.GetEmployeeId(connDS, loanIssue.EmployeeCode);
                    if (nId == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }
                    cmd.Parameters["EmployeeId"].Value = nId;
                }
                else
                {
                    cmd.Parameters["EmployeeId"].Value = null;
                }



                if (loanIssue.IsInterestRateNull() == false)
                {
                    cmd.Parameters["InterestRate"].Value = (object)loanIssue.InterestRate;
                }
                else
                {
                    cmd.Parameters["InterestRate"].Value = null;
                }

                if (loanIssue.IsLoanInstallmentNull() == false)
                {
                    cmd.Parameters["NoOfInstallments"].Value = (object)loanIssue.LoanInstallment;
                }
                else
                {
                    cmd.Parameters["NoOfInstallments"].Value = null;
                }

                if (loanIssue.IsLoanAmountNull() == false)
                {
                    cmd.Parameters["LoanAmount"].Value = (object)loanIssue.LoanAmount;
                }
                else
                {
                    cmd.Parameters["LoanAmount"].Value = null;
                }

                if (loanIssue.IsIssueDateNull() == false)
                {
                    cmd.Parameters["IssueDate"].Value = (object)loanIssue.IssueDate;
                }
                else
                {
                    cmd.Parameters["IssueDate"].Value = System.DBNull.Value;
                }

                if (loanIssue.IsInstallmentPrincipalNull() == false)
                {
                    cmd.Parameters["InstallmentPrincipal"].Value = (object)loanIssue.InstallmentPrincipal;
                }
                else
                {
                    cmd.Parameters["InstallmentPrincipal"].Value = null;
                }
                if (loanIssue.IsStartPaybackDateNull() == false)
                {
                    cmd.Parameters["StartPaybackDate"].Value = (object)loanIssue.StartPaybackDate;
                }
                else
                {
                    cmd.Parameters["StartPaybackDate"].Value = System.DBNull.Value;
                }

                if (loanIssue.IsCreate_UserNull() == false) cmd.Parameters["Create_User"].Value = loanIssue.Create_User;
                else cmd.Parameters["Create_User"].Value = System.DBNull.Value;

                if (loanIssue.IsIssueSiteIDNull() == false) cmd.Parameters["IssueSiteID"].Value = loanIssue.IssueSiteID;
                else cmd.Parameters["IssueSiteID"].Value = System.DBNull.Value;

                if (loanIssue.IsGuarantorNull() == false) cmd.Parameters["Guarantor"].Value = loanIssue.Guarantor;
                else cmd.Parameters["Guarantor"].Value = System.DBNull.Value;

                if (loanIssue.IsNoteNull() == false) cmd.Parameters["Note"].Value = loanIssue.Note;
                else cmd.Parameters["Note"].Value = System.DBNull.Value;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                #region Update Loan Requests
                if (!bError)
                {
                    nRowAffected = -1;
                    cmd.Parameters.Clear();
                    cmd.Dispose();
                    cmd = DBCommandProvider.GetDBCommand("UpdateLoanBenefitRequests");

                    cmd.Parameters["LoanIssueID"].Value = (object)genPK;
                    if (loanIssue.IsBenefitRequestIDNull() == false) cmd.Parameters["BenefitRequestID"].Value = loanIssue.BenefitRequestID;
                    else cmd.Parameters["BenefitRequestID"].Value = System.DBNull.Value;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                }
                #endregion

                if (!bError && !loanIssue.IsIsNotifyByMailNull() && loanIssue.IsNotifyByMail)
                {
                    #region Local Variable Declaration
                    string messageBody = "", returnMessage = "";
                    #endregion

                    #region Get Email Information from UtilDL
                    string host = UtilDL.GetHost();
                    Int32 port = UtilDL.GetPort();
                    bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
                    string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
                    string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
                    bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
                    string fromEmailAddress = loanIssue.SenderEmail;
                    string toEmailAddress = loanIssue.ReceiverEmail;
                    #endregion

                    #region Mail Body or Structure

                    string senderName = "";
                    if (!loanIssue.IsSenderNameNull()) senderName = loanIssue.SenderName;
                    else senderName = "Human Resource Management Division";
                    string mailingSubject = "Loan Issue for " + loanIssue.Description;

                    messageBody += "<b>Dear Concern,</b><br/><br/> This is system generated email to inform you that your requested loan has been issued.<br/>";
                    messageBody += "Here are the details of the issued loan: <br/><br/>";

                    messageBody += "<b><li>Issue Date: " + loanIssue.IssueDate.ToString("MMMM dd, yyyy") + "</li></b>";
                    messageBody += "<b><li>Loan Issue No: " + loanIssue.LoanIssueCode + "</li></b>";
                    messageBody += "<b><li>Loan Description: " + loanIssue.Description + "</li></b>";
                    messageBody += "<b><li>Loan Amount: " + loanIssue.LoanAmount + "</li></b>";
                    messageBody += "<b><li>No. of Installments : " + loanIssue.LoanInstallment + "</li></b>";
                    messageBody += "<b><li>Start Payback From: " + loanIssue.StartPaybackDate.ToString("MMMM yyyy") + "</li></b>";

                    messageBody += "<br/><br/>Thanks.";
                    Mail.Mail mail = new Mail.Mail();
                    fromEmailAddress = credentialEmailAddress;

                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, fromEmailAddress, toEmailAddress, "", mailingSubject, messageBody, senderName);
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, fromEmailAddress, toEmailAddress, "", mailingSubject, messageBody, senderName);
                    }

                    if (returnMessage != "Email successfully sent.")
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LOANISSUE_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion

                }

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LOANISSUE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteLoanIssue(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            #region Old...
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteLoanIssue");
            //if (cmd == null)
            //{
            //    return UtilDL.GetCommandNotFound();
            //}
            //foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            //{
            //    if (sValue.IsStringValueNull() == false)
            //    {
            //        cmd.Parameters["Code"].Value = (object)sValue.StringValue;
            //    }

            //    bool bError = false;
            //    int nRowAffected = -1;
            //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            //    if (bError == true)
            //    {
            //        ErrorDS.Error err = errDS.Errors.NewError();
            //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //        err.ErrorInfo1 = ActionID.ACTION_LOANISSUE_DEL.ToString();
            //        errDS.Errors.AddError(err);
            //        errDS.AcceptChanges();
            //        return errDS;
            //    }
            //}
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LoanIssue_Del";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            if (!stringDS.DataStrings[0].IsStringValueNull())
            {
                cmd.Parameters.AddWithValue("p_LoanNo", stringDS.DataStrings[0].StringValue);
            }
            else cmd.Parameters.AddWithValue("p_LoanNo", null);

            if (!stringDS.DataStrings[1].IsStringValueNull())
            {
                cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[1].StringValue);
            }
            else cmd.Parameters.AddWithValue("p_LoginID", null);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LOANISSUE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            return errDS;  // return empty ErrorDS
        }

        private DataSet _getLoanIssueList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            string sCode = null;


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanIssueList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            LoanIssuePO loanIssuePO = new LoanIssuePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              loanIssuePO, loanIssuePO.LoanIssues.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_LOANISSUE_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            loanIssuePO.AcceptChanges();

            LoanIssueDS loanIssueDS = new LoanIssueDS();
            if (loanIssuePO.LoanIssues.Count > 0)
            {


                foreach (LoanIssuePO.LoanIssue poLoanIssue in loanIssuePO.LoanIssues)
                {
                    LoanIssueDS.LoanIssue loanIssue = loanIssueDS.LoanIssues.NewLoanIssue();
                    if (poLoanIssue.IsLoanIssueCodeNull() == false)
                    {
                        loanIssue.LoanIssueCode = poLoanIssue.LoanIssueCode;
                    }

                    if (poLoanIssue.IsLoanIdNull() == false)
                    {
                        sCode = UtilDL.GetLoanCode(connDS, poLoanIssue.LoanId);
                        if (sCode == null)
                        {
                            return UtilDL.GetDBOperationFailed();
                        }
                        loanIssue.LoanCode = sCode;
                    }
                    if (poLoanIssue.IsInterestRateNull() == false)
                    {
                        loanIssue.InterestRate = poLoanIssue.InterestRate;
                    }
                    if (poLoanIssue.IsLoanInstallmentNull() == false)
                    {
                        loanIssue.LoanInstallment = poLoanIssue.LoanInstallment;
                    }
                    if (poLoanIssue.IsLoanAmountNull() == false)
                    {
                        loanIssue.LoanAmount = poLoanIssue.LoanAmount;
                    }
                    if (poLoanIssue.IsInstallmentPrincipalNull() == false)
                    {
                        loanIssue.InstallmentPrincipal = poLoanIssue.InstallmentPrincipal;
                    }

                    if (poLoanIssue.IsIssueDateNull() == false)
                    {
                        loanIssue.IssueDate = poLoanIssue.IssueDate;
                    }

                    if (poLoanIssue.IsStartPaybackDateNull() == false)
                    {
                        loanIssue.StartPaybackDate = poLoanIssue.StartPaybackDate;
                    }
                    if (poLoanIssue.IsEmployeeCodeNull() == false)
                    {

                        loanIssue.EmployeeCode = poLoanIssue.EmployeeCode;
                    }



                    loanIssueDS.LoanIssues.AddLoanIssue(loanIssue);
                    loanIssueDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(loanIssueDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _updateLoanIssue(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            LoanIssueDS loanIssueDS = new LoanIssueDS();
            DBConnectionDS connDS = new DBConnectionDS();
            string loanNo = "";


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Update command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateLoanIssue");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            loanIssueDS.Merge(inputDS.Tables[loanIssueDS.LoanIssues.TableName], false, MissingSchemaAction.Error);
            loanIssueDS.Merge(inputDS.Tables[loanIssueDS.LoanSchedules.TableName], false, MissingSchemaAction.Error);
            loanIssueDS.AcceptChanges();

            foreach (LoanIssueDS.LoanIssue loanIssue in loanIssueDS.LoanIssues)
            {

                if (loanIssue.IsEmployeeCodeNull() == false)
                {
                    long nId = UtilDL.GetEmployeeId(connDS, loanIssue.EmployeeCode);
                    if (nId == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }
                    cmd.Parameters["EmployeeId"].Value = nId;
                }
                else
                {
                    cmd.Parameters["EmployeeId"].Value = null;
                }

                cmd.Parameters["NoOfInstallments"].Value = (object)loanIssue.LoanInstallment;


                if (loanIssue.IsCreate_UserNull() == false) cmd.Parameters["Last_Update_User"].Value = loanIssue.Create_User;
                else cmd.Parameters["Last_Update_User"].Value = System.DBNull.Value;

                if (loanIssue.IsGuarantorNull() == false) cmd.Parameters["Guarantor"].Value = loanIssue.Guarantor;
                else cmd.Parameters["Guarantor"].Value = System.DBNull.Value;

                if (loanIssue.IsLoanIssueCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)loanIssue.LoanIssueCode;
                    loanNo = loanIssue.LoanIssueCode;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LOANISSUE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _doesLoanIssueExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesLoanIssueExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getLoanIssue(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            string sCode = null;
            //extract connectionDS
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            //extract Allowance/deduction code
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanIssue");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            LoanIssuePO loanIssuePO = new LoanIssuePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              loanIssuePO, loanIssuePO.LoanIssues.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_LOANISSUE_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            loanIssuePO.AcceptChanges();

            LoanIssueDS loanIssueDS = new LoanIssueDS();



            foreach (LoanIssuePO.LoanIssue poLoanIssue in loanIssuePO.LoanIssues)
            {
                LoanIssueDS.LoanIssue loanIssue = loanIssueDS.LoanIssues.NewLoanIssue();
                if (poLoanIssue.IsLoanIssueCodeNull() == false)
                {
                    loanIssue.LoanIssueCode = poLoanIssue.LoanIssueCode;
                }

                if (poLoanIssue.IsLoanIdNull() == false)
                {
                    sCode = UtilDL.GetLoanCode(connDS, poLoanIssue.LoanId);
                    if (sCode == null)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }
                    loanIssue.LoanCode = sCode;
                }
                if (poLoanIssue.IsInterestRateNull() == false)
                {
                    loanIssue.InterestRate = poLoanIssue.InterestRate;
                }
                if (poLoanIssue.IsLoanInstallmentNull() == false)
                {
                    loanIssue.LoanInstallment = poLoanIssue.LoanInstallment;
                }
                if (poLoanIssue.IsLoanAmountNull() == false)
                {
                    loanIssue.LoanAmount = poLoanIssue.LoanAmount;
                }
                if (poLoanIssue.IsInstallmentPrincipalNull() == false)
                {
                    loanIssue.InstallmentPrincipal = poLoanIssue.InstallmentPrincipal;
                }

                if (poLoanIssue.IsIssueDateNull() == false)
                {
                    loanIssue.IssueDate = poLoanIssue.IssueDate;
                }

                if (poLoanIssue.IsStartPaybackDateNull() == false)
                {
                    loanIssue.StartPaybackDate = poLoanIssue.StartPaybackDate;
                }
                if (poLoanIssue.IsEmployeeCodeNull() == false)
                {

                    loanIssue.EmployeeCode = poLoanIssue.EmployeeCode;
                }

                if (poLoanIssue.IsMaxSalaryDateNull() == false)
                {

                    loanIssue.MaxSalaryDate = poLoanIssue.MaxSalaryDate;
                }



                loanIssueDS.LoanIssues.AddLoanIssue(loanIssue);
                loanIssueDS.AcceptChanges();

                //get schedules
                cmd.Dispose();
                cmd = DBCommandProvider.GetDBCommand("GetLoanSchedules");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                long nLoanIssueId = UtilDL.GetLoanIssueId(connDS, loanIssue.LoanIssueCode);
                if (nLoanIssueId == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters["Id"].Value = nLoanIssueId;

                adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;



                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter,
                  loanIssuePO,
                  loanIssuePO.LoanSchedules.TableName,
                  connDS.DBConnections[0].ConnectionID,
                  ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_ALLOWDEDUCT_GET.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }

                loanIssuePO.AcceptChanges();


                if (loanIssuePO.LoanSchedules.Count > 0)
                {

                    foreach (LoanIssuePO.LoanSchedule poLoanSchedule in loanIssuePO.LoanSchedules)
                    {
                        LoanIssueDS.LoanSchedule loanSchedule = loanIssueDS.LoanSchedules.NewLoanSchedule();
                        if (poLoanSchedule.IsScheduleNoNull() == false)
                        {
                            loanSchedule.ScheduleNo = poLoanSchedule.ScheduleNo;
                        }
                        if (poLoanSchedule.IsInstallmentPrincipalNull() == false)
                        {
                            loanSchedule.InstallmentPrincipal = poLoanSchedule.InstallmentPrincipal;
                        }
                        if (poLoanSchedule.IsInstallmentInterestNull() == false)
                        {
                            loanSchedule.InstallmentInterest = poLoanSchedule.InstallmentInterest;
                        }
                        if (poLoanSchedule.IsDueInstallmentDateNull() == false)
                        {
                            loanSchedule.DueInstallmentDate = poLoanSchedule.DueInstallmentDate;
                        }
                        if (poLoanSchedule.IsPaymentDateNull() == false)
                        {
                            loanSchedule.PaymentDate = poLoanSchedule.PaymentDate;
                        }
                        if (poLoanSchedule.IsSalaryIDNull() == false)
                        {
                            loanSchedule.SalaryID = poLoanSchedule.SalaryID;
                        }
                        if (poLoanSchedule.IsVoucherNoNull() == false)
                        {
                            loanSchedule.VoucherNo = poLoanSchedule.VoucherNo;
                        }
                        if (poLoanSchedule.IsCashAmountNull() == false)
                        {
                            loanSchedule.CashAmount = poLoanSchedule.CashAmount;
                        }

                        loanSchedule.LoanIssue = loanIssue;
                        loanIssueDS.LoanSchedules.AddLoanSchedule(loanSchedule);
                        loanIssueDS.AcceptChanges();
                    }//end of foreach
                    loanIssueDS.AcceptChanges();
                }//end of if

            }



            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(loanIssueDS);

            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _getLoanIssueListByParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            Int32 GradeID = 0, DesignationID = 0, CompanyID = 0;
            bool bError = false;
            int nRowAffected = -1;

            #region Merge Input DS...
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            #endregion

            #region Get ID's from DataBase
            GradeID = Convert.ToInt32(UtilDL.GetGradeId(connDS, stringDS.DataStrings[1].StringValue));
            DesignationID = Convert.ToInt32(UtilDL.GetDesignationId(connDS, stringDS.DataStrings[2].StringValue));
            CompanyID = Convert.ToInt32(UtilDL.GetCompanyId(connDS, stringDS.DataStrings[3].StringValue));
            #endregion

            #region Select DB Command...
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            if (integerDS.DataIntegers[0].IntegerValue == 1)
            {
                if (stringDS.DataStrings[5].StringValue == "01-01-1900") cmd = DBCommandProvider.GetDBCommand("GetLoanIssueListByParam_Settlement");
                else cmd = DBCommandProvider.GetDBCommand("GetLoanIssueListByParam_Settlement_2");
            }
            else
            {
                if (stringDS.DataStrings[5].StringValue == "01-01-1900") cmd = DBCommandProvider.GetDBCommand("GetLoanIssueListByParam");
                else cmd = DBCommandProvider.GetDBCommand("GetLoanIssueListByParam_2");
            }

            if (cmd == null) return UtilDL.GetCommandNotFound();


            if (stringDS.DataStrings[5].StringValue != "01-01-1900")
            {
                cmd.Parameters["DueInstallmentDate2"].Value = Convert.ToDateTime(stringDS.DataStrings[5].StringValue);
                cmd.Parameters["DueInstallmentDate3"].Value = Convert.ToDateTime(stringDS.DataStrings[5].StringValue);
            }
            cmd.Parameters["DueInstallmentDate"].Value = Convert.ToDateTime(stringDS.DataStrings[5].StringValue);
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["LoanCode"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["LoanCode1"].Value = stringDS.DataStrings[4].StringValue;

            cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["DepartmentID1"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["SiteID1"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["Is_IssueSiteID"].Value = integerDS.DataIntegers[3].IntegerValue;

            cmd.Parameters["GradeID"].Value = GradeID;
            cmd.Parameters["GradeID1"].Value = GradeID;
            cmd.Parameters["DesignationID"].Value = DesignationID;
            cmd.Parameters["DesignationID1"].Value = DesignationID;
            cmd.Parameters["CompanyID"].Value = CompanyID;
            cmd.Parameters["CompanyID1"].Value = CompanyID;

            if (integerDS.DataIntegers[0].IntegerValue == 1 && stringDS.DataStrings[5].StringValue != "01-01-1900")
                cmd.Parameters["DueInstallmentDate1"].Value = Convert.ToDateTime(stringDS.DataStrings[5].StringValue);

            
            cmd.Parameters["IssueDateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[6].StringValue);
            cmd.Parameters["IssueDateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[7].StringValue);
            cmd.Parameters["LocationID1"].Value = integerDS.DataIntegers[4].IntegerValue;
            cmd.Parameters["LocationID2"].Value = integerDS.DataIntegers[4].IntegerValue;
            cmd.Parameters["ZoneID1"].Value = integerDS.DataIntegers[5].IntegerValue;
            cmd.Parameters["ZoneID2"].Value = integerDS.DataIntegers[5].IntegerValue;
            #endregion

            adapter.SelectCommand = cmd;

            LoanIssueDS loanIssueDS = new LoanIssueDS();

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, loanIssueDS, loanIssueDS.LoanIssues.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LOANISSUE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            loanIssueDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(loanIssueDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }
        private DataSet _createBulkLoanIssue(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            LoanIssueDS loanIssueDS = new LoanIssueDS();
            DBConnectionDS connDS = new DBConnectionDS();
            long nId = -1;



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();



            loanIssueDS.Merge(inputDS.Tables[loanIssueDS.BulkLoan.TableName], false, MissingSchemaAction.Error);
            loanIssueDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateBulkLoanIssue");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            string PrevLoanNo = "";
            long genPK = 0;

            foreach (LoanIssueDS.BulkLoanRow loanIssue in loanIssueDS.BulkLoan)
            {
                              

                if (PrevLoanNo != loanIssue.LoanNo)
                {
                    genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }

                    cmd.Parameters["Id"].Value = (object)genPK;

                    if (loanIssue.IsCodeNull() == false)
                    {
                        nId = UtilDL.GetLoanId(connDS, loanIssue.Code);
                        if (nId == -1)
                        {
                            return UtilDL.GetDBOperationFailed();
                        }
                        cmd.Parameters["LoanId"].Value = (object)nId;
                    }
                    else cmd.Parameters["LoanId"].Value = null;

                    if (loanIssue.IsLoanNoNull() == false) cmd.Parameters["LoanNo"].Value = (object)loanIssue.LoanNo;
                    else cmd.Parameters["LoanNo"].Value = null;


                    if (loanIssue.IsEmployeeCodeNull() == false)
                    {
                        nId = UtilDL.GetEmployeeId(connDS, loanIssue.EmployeeCode);
                        if (nId == -1)
                        {
                            return UtilDL.GetDBOperationFailed();
                        }
                        cmd.Parameters["EmployeeId"].Value = nId;
                    }
                    else cmd.Parameters["EmployeeId"].Value = null;


                    if (loanIssue.IsLoanAmountNull() == false) cmd.Parameters["LoanAmount"].Value = (object)loanIssue.LoanAmount;
                    else cmd.Parameters["LoanAmount"].Value = null;

                    if (loanIssue.IsIssueDateNull() == false) cmd.Parameters["IssueDate"].Value = (object)loanIssue.IssueDate;
                    else cmd.Parameters["IssueDate"].Value = System.DBNull.Value;

                    if (loanIssue.IsNoOfInstallmentsNull() == false) cmd.Parameters["NoOfInstallments"].Value = (object)loanIssue.NoOfInstallments;
                    else cmd.Parameters["NoOfInstallments"].Value = null;

                    if (loanIssue.IsInstallmentPrincipalNull() == false) cmd.Parameters["InstallmentPrincipal"].Value = (object)loanIssue.InstallmentPrincipal;
                    else cmd.Parameters["InstallmentPrincipal"].Value = null;

                    cmd.Parameters["InterestRate"].Value = 0;

                    if (loanIssue.IsDueInstallmentDateNull() == false) cmd.Parameters["StartPaybackDate"].Value = (object)loanIssue.DueInstallmentDate;
                    else cmd.Parameters["StartPaybackDate"].Value = System.DBNull.Value;


                    if (loanIssue.IsCreate_UserNull() == false) cmd.Parameters["Create_User"].Value = loanIssue.Create_User;
                    else cmd.Parameters["Create_User"].Value = System.DBNull.Value;

                    if (loanIssue.IsIssueSiteCodeNull() == false) cmd.Parameters["IssueSiteCode"].Value = loanIssue.IssueSiteCode;
                    else cmd.Parameters["IssueSiteCode"].Value = System.DBNull.Value;

                    if (loanIssue.IsGuarantorCodeNull() == false) cmd.Parameters["GuarantorCode"].Value = loanIssue.GuarantorCode;
                    else cmd.Parameters["GuarantorCode"].Value = System.DBNull.Value;

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LOANISSUE_ADD_BULK.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    else PrevLoanNo = loanIssue.LoanNo;
                }




                #region Schedule Add
                OleDbCommand cmdSchedule = DBCommandProvider.GetDBCommand("CreateBulkLoanSchedule");
                cmdSchedule.Parameters["Id"].Value = (object)genPK;

                if (loanIssue.IsScheduleNoNull() == false) cmdSchedule.Parameters["ScheduleNo"].Value = (object)loanIssue.ScheduleNo;
                else cmdSchedule.Parameters["ScheduleNo"].Value = null;

                if (loanIssue.IsInstallmentPrincipalNull() == false) cmdSchedule.Parameters["InstallmentPrincipal"].Value = (object)loanIssue.InstallmentPrincipal;
                else cmdSchedule.Parameters["InstallmentPrincipal"].Value = null;

                if (loanIssue.IsInstallmentInterestNull() == false) cmdSchedule.Parameters["InstallmentInterest"].Value = (object)loanIssue.InstallmentInterest;
                else cmdSchedule.Parameters["InstallmentInterest"].Value = null;

                if (loanIssue.IsDueInstallmentDateNull() == false) cmdSchedule.Parameters["DueInstallmentDate"].Value = (object)loanIssue.DueInstallmentDate;
                else cmdSchedule.Parameters["DueInstallmentDate"].Value = null;

                bool b_Error = false;
                int n_RowAffected = -1;
                n_RowAffected = ADOController.Instance.ExecuteNonQuery(cmdSchedule, connDS.DBConnections[0].ConnectionID, ref b_Error);

                if (b_Error == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LOANISSUE_ADD_BULK.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }



    }
}
