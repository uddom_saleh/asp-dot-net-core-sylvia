/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: April 17, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class ResponsibilityCenterDL
    {
        public ResponsibilityCenterDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_RESPONSIBILITYCENTER_ADD:
                    return _createResCenter(inputDS);
                case ActionID.ACTION_RESPONSIBILITYCENTER_UPD:
                    return this._updateResCenter(inputDS);
                case ActionID.NA_ACTION_Q_RESPONSIBILITYCENTER_ALL:
                    return _getResCenterList(inputDS);
                case ActionID.ACTION_RESPONSIBILITYCENTER_DEL:
                    return this._deleteResCenter(inputDS);
                case ActionID.ACTION_DOES_RESPONSIBILITYCENTER_EXIST:
                    return this._doesResCenterExist(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _doesResCenterExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesResponsCenterExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createResCenter(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ResponsibilityCenterDS resenterDS = new ResponsibilityCenterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateResponsCenter");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();

            resenterDS.Merge(inputDS.Tables[resenterDS.ResponsibilityCenters.TableName], false, MissingSchemaAction.Error);
            resenterDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            foreach (ResponsibilityCenterDS.ResponsibilityCenter rescenter in resenterDS.ResponsibilityCenters)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                    return UtilDL.GetDBOperationFailed();

                cmd.Parameters["Id"].Value = genPK;

                if (rescenter.IsCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = rescenter.Code;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }
                if (rescenter.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = rescenter.Name;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }

                cmd.Parameters["DefaultSelection"].Value = rescenter.DefaultSelection;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RESPONSIBILITYCENTER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateResCenter(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            ResponsibilityCenterDS resenterDS = new ResponsibilityCenterDS();
            resenterDS.Merge(inputDS.Tables[resenterDS.ResponsibilityCenters.TableName], false, MissingSchemaAction.Error);
            resenterDS.AcceptChanges();



            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_ResCen";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (ResponsibilityCenterDS.ResponsibilityCenter rescenter in resenterDS.ResponsibilityCenters)
            {
                if (rescenter.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_RESPONSIBILITYCENTER_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion



            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateResponsCenter");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            foreach (ResponsibilityCenterDS.ResponsibilityCenter rescenter in resenterDS.ResponsibilityCenters)
            {
                if (rescenter.IsCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = rescenter.Code;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }
                if (rescenter.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = rescenter.Name;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }
                cmd.Parameters["DefaultSelection"].Value = rescenter.DefaultSelection;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RESPONSIBILITYCENTER_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS

        }

        private DataSet _deleteResCenter(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteResponsCenter");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {

                cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue;
            }
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_RESPONSIBILITYCENTER_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getResCenterList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            ResponsibilityCenterPO rescenterPO = new ResponsibilityCenterPO();
            ResponsibilityCenterDS resenterDS = new ResponsibilityCenterDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetResponsCenterList");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, rescenterPO, rescenterPO.ResponsibilityCenters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            nRowAffected = ADOController.Instance.Fill(adapter, resenterDS, resenterDS.ResponsibilityCenters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_RESPONSIBILITYCENTER_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            //rescenterPO.AcceptChanges();
            resenterDS.AcceptChanges();

            #region Commented :: WALI :: 29-Jul-2015
            //if ( rescenterPO.ResponsibilityCenters.Count > 0 )
            //{
            //    foreach ( ResponsibilityCenterPO.ResponsibilityCenter poRescenter in rescenterPO.ResponsibilityCenters)
            //    {
            //        ResponsibilityCenterDS.ResponsibilityCenter rescenter = resenterDS.ResponsibilityCenters.NewResponsibilityCenter();

            //        if (poRescenter.IsCodeNull() == false) rescenter.Code = poRescenter.Code;
            //        if (poRescenter.IsNameNull() == false) rescenter.Name = poRescenter.Name;
            //        resenterDS.ResponsibilityCenters.AddResponsibilityCenter( rescenter );
            //        resenterDS.AcceptChanges();
            //    }
            //}
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(resenterDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
    }
}
