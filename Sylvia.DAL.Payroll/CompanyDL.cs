/* Compamy: Milllennium Information Solution Limited
 * Author: Mahbubur Rahman Khan
 * Comment Date: March 27, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for CompanyDL.
    /// </summary>
    public class CompanyDL
    {
        public CompanyDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_COMPANY_ADD:
                    return _createCompany(inputDS);
                case ActionID.NA_ACTION_GET_COMPANY_LIST:
                    return _getCompanyList(inputDS);
                case ActionID.ACTION_COMPANY_DEL:
                    return this._deleteCompany(inputDS);
                case ActionID.ACTION_COMPANY_UPD:
                    return this._updateCompany(inputDS);
                case ActionID.ACTION_DOES_COMPANY_EXIST:
                    return this._doesCompanyExist(inputDS);

                case ActionID.ACTION_ORGANOGRAM_NODE_SAVE:
                    return this._saveOrganogramNode(inputDS);
                case ActionID.NA_ACTION_GET_COMPANY_ORGANOGRAM:
                    return this._getCompanyOrganogram(inputDS);
                case ActionID.ACTION_ORGANOGRAM_NODE_DELETE:
                    return this._deleteOrganogramNode(inputDS);
                case ActionID.NA_ACTION_GET_COM_ORGANOGRAM_FOREXCELREPORT:
                    return this._getCompanyOrganogram_ForExcelReport(inputDS);

                case ActionID.NA_ACTION_GET_ORGANOGRAM_MANPOWER_LIST:
                    return this.GetOrganogramManpowerList(inputDS);
                case ActionID.ACTION_ORGANOGRAM_MANPOWER_ADD:
                    return this.SaveOrganogramManpower(inputDS);

                case ActionID.ACTION_DIVISION_LOCATION_ADD:
                    return this._createDivisionLocation(inputDS);
                case ActionID.ACTION_DIVISION_LOCATION_UPDATE:
                    return this._UpdateDivisionLocation(inputDS);
                case ActionID.ACTION_DIVISION_LOCATION_DELETE:
                    return this._DeleteDivisionLocation(inputDS);
                    
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _createCompany(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CompanyDS comDS = new CompanyDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateCompany");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            comDS.Merge(inputDS.Tables[comDS.Companies.TableName], false, MissingSchemaAction.Error);
            comDS.AcceptChanges();

            foreach (CompanyDS.Company com in comDS.Companies)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["Id"].Value = (object)genPK;
                //code
                if (com.IsCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)com.Code;
                }
                else
                {
                    cmd.Parameters["Code"].Value = DBNull.Value;
                }
                //name
                if (com.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = (object)com.Name;

                }
                else
                {
                    cmd.Parameters["Name"].Value = DBNull.Value;
                }
                //description
                if (com.IsDescriptionNull() == false)
                {
                    cmd.Parameters["Description"].Value = (object)com.Description;
                }
                else
                {
                    cmd.Parameters["Description"].Value = DBNull.Value;
                }
                //isactive
                if (com.IsActiveNull() == false)
                {
                    cmd.Parameters["Active"].Value = (object)com.Active;
                }
                else
                {
                    cmd.Parameters["Active"].Value = DBNull.Value;
                    ;

                }
                //Shakir, 12-3-2013
                if (com.IsIncrementMonthNull() == false)
                {
                    cmd.Parameters["IncrementMonth"].Value = (object)com.IncrementMonth;
                }
                else
                {
                    cmd.Parameters["IncrementMonth"].Value = DBNull.Value;

                }

                if (com.IsProbationMonthNull() == false)
                {
                    cmd.Parameters["ProbationMonth"].Value = (object)com.ProbationMonth;
                }
                else
                {
                    cmd.Parameters["ProbationMonth"].Value = DBNull.Value;

                }

                if (com.IsCountryidNull() == false)
                {
                    cmd.Parameters["Countryid"].Value = (object)com.Countryid;
                }
                else
                {
                    cmd.Parameters["Countryid"].Value = DBNull.Value;

                }

                if (com.IsProbationroundNull() == false)
                {
                    cmd.Parameters["Probationround"].Value = (object)com.Probationround;
                }
                else
                {
                    cmd.Parameters["Probationround"].Value = DBNull.Value;

                }

                if (!com.IsKPIGroupID_CNull())
                {
                    cmd.Parameters["KPIGroupID_C"].Value = com.KPIGroupID_C;
                }
                else cmd.Parameters["KPIGroupID_C"].Value = DBNull.Value;

                if (!com.IsGradePositionMethodNull()) cmd.Parameters["p_GradePositionMethod"].Value = com.GradePositionMethod;
                else cmd.Parameters["p_GradePositionMethod"].Value = System.DBNull.Value;

                if (!com.IsOrganizationTypeIDNull()) cmd.Parameters["p_OrganizationTypeID"].Value = com.OrganizationTypeID;
                else cmd.Parameters["p_OrganizationTypeID"].Value = System.DBNull.Value;

                if (!com.IsPinCodeNull()) cmd.Parameters["p_PinCode"].Value = com.PinCode;
                else cmd.Parameters["p_PinCode"].Value = System.DBNull.Value;

                if (!com.IsBin_NoNull()) cmd.Parameters["p_Bin_No"].Value = com.Bin_No;
                else cmd.Parameters["p_Bin_No"].Value = System.DBNull.Value;

                if (!com.IsTin_NoNull()) cmd.Parameters["p_Tin_No"].Value = com.Tin_No;
                else cmd.Parameters["p_Tin_No"].Value = System.DBNull.Value;

                if (!com.IsVat_NoNull()) cmd.Parameters["p_Vat_No"].Value = com.Vat_No;
                else cmd.Parameters["p_Vat_No"].Value = System.DBNull.Value;

                if (!com.IsIdentification_NoNull()) cmd.Parameters["p_Identification_No"].Value = com.Identification_No;
                else cmd.Parameters["p_Identification_No"].Value = System.DBNull.Value;

                if (!com.IsTelephoneNull()) cmd.Parameters["p_Telephone"].Value = com.Telephone;
                else cmd.Parameters["p_Telephone"].Value = System.DBNull.Value;

                if (!com.IsEmailNull()) cmd.Parameters["p_Email"].Value = com.Email;
                else cmd.Parameters["p_Email"].Value = System.DBNull.Value;

                if (!com.IsWebNull()) cmd.Parameters["p_Web"].Value = com.Web;
                else cmd.Parameters["p_Web"].Value = System.DBNull.Value;

                if (!com.IsCurrencyIDNull()) cmd.Parameters["p_CurrencyID"].Value = com.CurrencyID;
                else cmd.Parameters["p_CurrencyID"].Value = System.DBNull.Value;

                if (!com.IsMaintain_AccountNull()) cmd.Parameters["p_Maintain_Account"].Value = com.Maintain_Account;
                else cmd.Parameters["p_Maintain_Account"].Value = System.DBNull.Value;

                if (!com.IsFinancial_Year_FromNull()) cmd.Parameters["p_Financial_Year_From"].Value = com.Financial_Year_From;
                else cmd.Parameters["p_Financial_Year_From"].Value = System.DBNull.Value;

                if (!com.IsDaysPerYearNull()) cmd.Parameters["p_DaysPerYear"].Value = com.DaysPerYear;
                else cmd.Parameters["p_DaysPerYear"].Value = System.DBNull.Value;

                cmd.Parameters["p_DefaultSelection"].Value = com.DefaultSelection;            
                cmd.Parameters["p_Follow_Leave_Policy"].Value = com.Follow_Leave_Policy;
                cmd.Parameters["p_IncrementDurationMonth"].Value = com.IncrementDurationMonth;
                cmd.Parameters["p_IncrmentDayConvention"].Value = com.IncrmentDayConvention;
                

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMPANY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteCompany(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteCompany");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BONUS_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getCompanyList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCompanyList");//Jarif(28 Sep 11)
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCompanyList_New");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(28 Sep 11)...
            #region Old...
            //CompanyPO comPO = new CompanyPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, comPO, comPO.Companies.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_COMPANY_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //comPO.AcceptChanges();

            //CompanyDS comDS = new CompanyDS();

            //if (comPO.Companies.Count > 0)
            //{


            //    foreach (CompanyPO.Company poCom in comPO.Companies)
            //    {
            //        CompanyDS.Company com = comDS.Companies.NewCompany();
            //        if (poCom.IsCodeNull() == false)
            //        {
            //            com.Code = poCom.Code;
            //        }
            //        if (poCom.IsNameNull() == false)
            //        {
            //            com.Name = poCom.Name;
            //        }
            //        if (poCom.IsDescriptionNull() == false)
            //        {
            //            com.Description = poCom.Description;
            //        }
            //        if (poCom.IsActiveNull() == false)
            //        {
            //            com.Active = poCom.Active;
            //        }

            //        comDS.Companies.AddCompany(com);
            //        comDS.AcceptChanges();
            //    }
            //}
            #endregion

            CompanyDS comDS = new CompanyDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, comDS, comDS.Companies.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COMPANY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            comDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(comDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _updateCompany(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CompanyDS comDS = new CompanyDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            comDS.Merge(inputDS.Tables[comDS.Companies.TableName], false, MissingSchemaAction.Error);
            comDS.AcceptChanges();


            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_COM";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (CompanyDS.Company com in comDS.Companies)
            {
                if (com.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_COMPANY_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion



            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateCompany");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            foreach (CompanyDS.Company com in comDS.Companies)
            {

                //code
                if (com.IsCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)com.Code;
                }
                else
                {
                    cmd.Parameters["Code"].Value = DBNull.Value;
                }
                //name
                if (com.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = (object)com.Name;

                }
                else
                {
                    cmd.Parameters["Name"].Value = DBNull.Value;
                }
                //description
                if (com.IsDescriptionNull() == false)
                {
                    cmd.Parameters["Description"].Value = (object)com.Description;
                }
                else
                {
                    cmd.Parameters["Description"].Value = DBNull.Value;
                }
                //isactive
                if (com.IsActiveNull() == false)
                {
                    cmd.Parameters["Active"].Value = (object)com.Active;
                }
                else
                {
                    cmd.Parameters["Active"].Value = DBNull.Value;

                }
                //Shakir, 12-3-2013
                if (com.IsIncrementMonthNull() == false)
                {
                    cmd.Parameters["IncrementMonth"].Value = (object)com.IncrementMonth;
                }
                else
                {
                    cmd.Parameters["IncrementMonth"].Value = DBNull.Value;

                }

                if (com.IsProbationMonthNull() == false)
                {
                    cmd.Parameters["ProbationMonth"].Value = (object)com.ProbationMonth;
                }
                else
                {
                    cmd.Parameters["ProbationMonth"].Value = DBNull.Value;
                }

                if (com.IsCountryidNull() == false)
                {
                    cmd.Parameters["Countryid"].Value = (object)com.Countryid;
                }
                else
                {
                    cmd.Parameters["Countryid"].Value = DBNull.Value;
                }

                if (com.IsProbationroundNull() == false)
                {
                    cmd.Parameters["Probationround"].Value = (object)com.Probationround;
                }
                else
                {
                    cmd.Parameters["Probationround"].Value = DBNull.Value;

                }

                if (!com.IsKPIGroupID_CNull())
                {
                    cmd.Parameters["KPIGroupID_C"].Value = com.KPIGroupID_C;
                }
                else cmd.Parameters["KPIGroupID_C"].Value = DBNull.Value;
                //

                if (!com.IsGradePositionMethodNull()) cmd.Parameters["p_GradePositionMethod"].Value = com.GradePositionMethod;
                else cmd.Parameters["p_GradePositionMethod"].Value = System.DBNull.Value;

                if (!com.IsOrganizationTypeIDNull()) cmd.Parameters["p_OrganizationTypeID"].Value = com.OrganizationTypeID;
                else cmd.Parameters["p_OrganizationTypeID"].Value = System.DBNull.Value;

                if (!com.IsPinCodeNull()) cmd.Parameters["p_PinCode"].Value = com.PinCode;
                else cmd.Parameters["p_PinCode"].Value = System.DBNull.Value;

                if (!com.IsBin_NoNull()) cmd.Parameters["p_Bin_No"].Value = com.Bin_No;
                else cmd.Parameters["p_Bin_No"].Value = System.DBNull.Value;

                if (!com.IsTin_NoNull()) cmd.Parameters["p_Tin_No"].Value = com.Tin_No;
                else cmd.Parameters["p_Tin_No"].Value = System.DBNull.Value;

                if (!com.IsVat_NoNull()) cmd.Parameters["p_Vat_No"].Value = com.Vat_No;
                else cmd.Parameters["p_Vat_No"].Value = System.DBNull.Value;

                if (!com.IsIdentification_NoNull()) cmd.Parameters["p_Identification_No"].Value = com.Identification_No;
                else cmd.Parameters["p_Identification_No"].Value = System.DBNull.Value;

                if (!com.IsTelephoneNull()) cmd.Parameters["p_Telephone"].Value = com.Telephone;
                else cmd.Parameters["p_Telephone"].Value = System.DBNull.Value;

                if (!com.IsEmailNull()) cmd.Parameters["p_Email"].Value = com.Email;
                else cmd.Parameters["p_Email"].Value = System.DBNull.Value;

                if (!com.IsWebNull()) cmd.Parameters["p_Web"].Value = com.Web;
                else cmd.Parameters["p_Web"].Value = System.DBNull.Value;

                if (!com.IsCurrencyIDNull()) cmd.Parameters["p_CurrencyID"].Value = com.CurrencyID;
                else cmd.Parameters["p_CurrencyID"].Value = System.DBNull.Value;

                if (!com.IsMaintain_AccountNull()) cmd.Parameters["p_Maintain_Account"].Value = com.Maintain_Account;
                else cmd.Parameters["p_Maintain_Account"].Value = System.DBNull.Value;

                if (!com.IsFinancial_Year_FromNull()) cmd.Parameters["p_Financial_Year_From"].Value = com.Financial_Year_From;
                else cmd.Parameters["p_Financial_Year_From"].Value = System.DBNull.Value;

                if (!com.IsDaysPerYearNull()) cmd.Parameters["p_DaysPerYear"].Value = com.DaysPerYear;
                else cmd.Parameters["p_DaysPerYear"].Value = System.DBNull.Value;

                cmd.Parameters["p_DefaultSelection"].Value = com.DefaultSelection;
                cmd.Parameters["p_Follow_Leave_Policy"].Value = com.Follow_Leave_Policy;
                cmd.Parameters["p_IncrementDurationMonth"].Value = com.IncrementDurationMonth;
                cmd.Parameters["p_IncrmentDayConvention"].Value = com.IncrmentDayConvention;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMPANY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _doesCompanyExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesCompanyExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _saveOrganogramNode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CompanyDS orgDS = new CompanyDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            orgDS.Merge(inputDS.Tables[orgDS.Organogram.TableName], false, MissingSchemaAction.Error);
            orgDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (orgDS.Organogram[0].IsUpdate)
            {
                #region Update Node
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ORGANOGRAM_NODE_UPD";
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (CompanyDS.OrganogramRow row in orgDS.Organogram.Rows)
                {
                    cmd.Parameters.AddWithValue("p_NodeID", row.NodeID);

                    if (!row.IsNode_NameNull()) cmd.Parameters.AddWithValue("p_Node_Name", row.Node_Name);
                    else cmd.Parameters.AddWithValue("p_Node_Name", DBNull.Value);

                    if (!row.IsParent_NodeIDNull()) cmd.Parameters.AddWithValue("p_Parent_NodeID", row.Parent_NodeID);
                    else cmd.Parameters.AddWithValue("p_Parent_NodeID", DBNull.Value);

                    if (!row.IsManpowerNull()) cmd.Parameters.AddWithValue("p_Manpower", row.Manpower);
                    else cmd.Parameters.AddWithValue("p_Manpower", DBNull.Value);

                    if (!row.IsNode_TypeNull()) cmd.Parameters.AddWithValue("p_Node_Type", row.Node_Type);
                    else cmd.Parameters.AddWithValue("p_Node_Type", DBNull.Value);

                    if (!row.IsNode_CodeNull()) cmd.Parameters.AddWithValue("p_Node_Code", row.Node_Code);
                    else cmd.Parameters.AddWithValue("p_Node_Code", DBNull.Value);

                    if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                    else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ORGANOGRAM_NODE_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion
            }
            else
            {
                #region Create Node
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ORGANOGRAM_NODE_ADD";
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (CompanyDS.OrganogramRow row in orgDS.Organogram.Rows)
                {
                    if (!row.IsNode_NameNull()) cmd.Parameters.AddWithValue("p_Node_Name", row.Node_Name);
                    else cmd.Parameters.AddWithValue("p_Node_Name", DBNull.Value);

                    if (!row.IsParent_NodeIDNull()) cmd.Parameters.AddWithValue("p_Parent_NodeID", row.Parent_NodeID);
                    else cmd.Parameters.AddWithValue("p_Parent_NodeID", DBNull.Value);

                    if (!row.IsManpowerNull()) cmd.Parameters.AddWithValue("p_Manpower", row.Manpower);
                    else cmd.Parameters.AddWithValue("p_Manpower", DBNull.Value);

                    if (!row.IsNode_TypeNull()) cmd.Parameters.AddWithValue("p_Node_Type", row.Node_Type);
                    else cmd.Parameters.AddWithValue("p_Node_Type", DBNull.Value);

                    if (!row.IsNode_CodeNull()) cmd.Parameters.AddWithValue("p_Node_Code", row.Node_Code);
                    else cmd.Parameters.AddWithValue("p_Node_Code", DBNull.Value);

                    if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                    else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ORGANOGRAM_NODE_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteOrganogramNode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ORGANOGRAM_NODE_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("p_NodeId", Convert.ToInt32(stringDS.DataStrings[0].StringValue));

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ORGANOGRAM_NODE_DELETE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getCompanyOrganogram(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            CompanyDS comDS = new CompanyDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCompanyOrganogram");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, comDS, comDS.Organogram.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COMPANY_ORGANOGRAM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            comDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(comDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getCompanyOrganogram_ForExcelReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            CompanyDS comDS = new CompanyDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            if (stringDS.DataStrings[0].StringValue == "All") cmd = DBCommandProvider.GetDBCommand("GetCompanyOrganogram_ForExcelReport_ALL");
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetCompanyOrganogram_ForExcelReport");
                cmd.Parameters["Node_Code"].Value = cmd.Parameters["Node_Code2"].Value = (object)stringDS.DataStrings[1].StringValue;
            }

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, comDS, comDS.Organogram.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COM_ORGANOGRAM_FOREXCELREPORT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            comDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(comDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        
        private DataSet GetOrganogramManpowerList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetOrganogramManPowerList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["DepartmentCode"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            CompanyDS comDS = new CompanyDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, comDS, comDS.OrganogramManpower.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ORGANOGRAM_MANPOWER_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            comDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(comDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;            
        }
        private DataSet SaveOrganogramManpower(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CompanyDS orgDS = new CompanyDS();
            DBConnectionDS connDS = new DBConnectionDS();

            orgDS.Merge(inputDS.Tables[orgDS.OrganogramManpower.TableName], false, MissingSchemaAction.Error);
            orgDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Manpower Delete
            OleDbCommand cmdDel = new OleDbCommand();
            cmdDel.CommandText = "PRO_ORGANOGRAM_MANPOWER_DELETE";
            cmdDel.CommandType = CommandType.StoredProcedure;

            cmdDel.Parameters.AddWithValue("P_DepartmentCode", stringDS.DataStrings[0].StringValue);
            bool bError_del = false;
            int nRowAffected_del = -1;
            nRowAffected_del = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bError_del);
            cmdDel.Parameters.Clear();
            if (bError_del)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ORGANOGRAM_MANPOWER_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Manpower Add
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ORGANOGRAM_MANPOWER_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (CompanyDS.OrganogramManpowerRow row in orgDS.OrganogramManpower.Rows)
            {
                if (!row.IsFunctionCodeNull()) { cmd.Parameters.AddWithValue("p_FUNCTIONCODE", row.FunctionCode); }
                else { cmd.Parameters.AddWithValue("p_FUNCTIONCODE", DBNull.Value); }
                if (!row.IsDesignationCodeNull()) { cmd.Parameters.AddWithValue("p_DESIGNATIONCODE", row.DesignationCode); }
                else { cmd.Parameters.AddWithValue("p_DESIGNATIONCODE", DBNull.Value); }
                if (!row.IsManpowerNull()) { cmd.Parameters.AddWithValue("p_MANPOWER", row.Manpower); }
                else { cmd.Parameters.AddWithValue("p_MANPOWER", DBNull.Value); }
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ORGANOGRAM_MANPOWER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createDivisionLocation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CompanyDS comDS = new CompanyDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateDivisionLocation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            comDS.Merge(inputDS.Tables[comDS.Division_Location.TableName], false, MissingSchemaAction.Error);
            comDS.AcceptChanges();

            foreach (CompanyDS.Division_LocationRow row in comDS.Division_Location)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["Division_LocationID"].Value = (object)genPK;

                if (!row.IsCompanyIDNull()) cmd.Parameters["CompanyID"].Value = (object)row.CompanyID;
                else cmd.Parameters["CompanyID"].Value = DBNull.Value;

                if (!row.IsCompanyDivisionCodeNull()) cmd.Parameters["CompanyDivisionCode"].Value = (object)row.CompanyDivisionCode;
                else cmd.Parameters["CompanyDivisionCode"].Value = DBNull.Value;

                if (!row.IsCompanyDivisionNameNull()) cmd.Parameters["CompanyDivisionName"].Value = (object)row.CompanyDivisionName;
                else cmd.Parameters["CompanyDivisionName"].Value = DBNull.Value;

                if (!row.IsCompanyDivisionNameNativeLangNull()) cmd.Parameters["CompanyDivisionNameNativeLang"].Value = (object)row.CompanyDivisionNameNativeLang;
                else cmd.Parameters["CompanyDivisionNameNativeLang"].Value = DBNull.Value;

                cmd.Parameters["Active"].Value = row.Active;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DIVISION_LOCATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _DeleteDivisionLocation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteDivisionLocation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["CompanyDivisionCode"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DIVISION_LOCATION_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;
        }
        private DataSet _UpdateDivisionLocation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CompanyDS comDS = new CompanyDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            comDS.Merge(inputDS.Tables[comDS.Division_Location.TableName], false, MissingSchemaAction.Error);
            comDS.AcceptChanges();

           
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateDivisionLocation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            foreach (CompanyDS.Division_LocationRow row in comDS.Division_Location)
            {

                if (!row.IsCompanyIDNull()) cmd.Parameters["CompanyID"].Value = (object)row.CompanyID;
                else cmd.Parameters["CompanyID"].Value = DBNull.Value;
                
                if (!row.IsCompanyDivisionNameNull()) cmd.Parameters["CompanyDivisionName"].Value = (object)row.CompanyDivisionName;
                else cmd.Parameters["CompanyDivisionName"].Value = DBNull.Value;

                if (!row.IsCompanyDivisionNameNativeLangNull()) cmd.Parameters["CompanyDivisionNameNativeLang"].Value = (object)row.CompanyDivisionNameNativeLang;
                else cmd.Parameters["CompanyDivisionNameNativeLang"].Value = DBNull.Value;

                cmd.Parameters["Active"].Value = (object)row.Active;

                if (!row.IsCompanyDivisionCodeNull()) cmd.Parameters["CompanyDivisionCode"].Value = (object)row.CompanyDivisionCode;
                else cmd.Parameters["CompanyDivisionCode"].Value = DBNull.Value;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DIVISION_LOCATION_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
    }
}
