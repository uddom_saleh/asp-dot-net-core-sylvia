using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for SalaryHeadDL.
    /// </summary>
    public class SalaryHeadDL
    {
        public SalaryHeadDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {

                case ActionID.NA_ACTION_GET_SALARY_HEAD_LIST:
                    return _getSalaryHeadList(inputDS);


                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getSalaryHeadList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalaryHeadList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //SalaryHeadPO headPO = new SalaryHeadPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, headPO, headPO.SalaryHeads.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_SALARY_HEAD_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //headPO.AcceptChanges();

            //SalaryHeadDS headDS = new SalaryHeadDS();
            //if (headPO.SalaryHeads.Count > 0)
            //{


            //    foreach (SalaryHeadPO.SalaryHead poHead in headPO.SalaryHeads)
            //    {
            //        SalaryHeadDS.SalaryHead head = headDS.SalaryHeads.NewSalaryHead();
            //        if (poHead.IsHeadIdNull() == false)
            //        {
            //            head.HeadId = poHead.HeadId;
            //        }
            //        if (poHead.IsHeadTypeNull() == false)
            //        {
            //            head.HeadType = poHead.HeadType;
            //        }
            //        if (poHead.IsDefaultDescriptionNull() == false)
            //        {
            //            head.DefaultDescription = poHead.DefaultDescription;
            //        }
            //        if (poHead.IsUserDescriptionNull() == false)
            //        {
            //            head.UserDescription = poHead.UserDescription;
            //        }
            //        if (poHead.IsPositionNull() == false)
            //        {
            //            head.Position = poHead.Position;
            //        }
            //        if (poHead.IsEmployeeTypeNull() == false)
            //        {
            //            head.EmployeeType = poHead.EmployeeType;
            //        }
            //        if (poHead.IsPayrollSiteCodeNull() == false)
            //        {
            //            head.PayrollSiteCode = poHead.PayrollSiteCode;
            //        }


            //        headDS.SalaryHeads.AddSalaryHead(head);
            //        headDS.AcceptChanges();
            //    }
            //}
            #endregion

            SalaryHeadDS headDS = new SalaryHeadDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, headDS, headDS.SalaryHeads.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SALARY_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            headDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(headDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

    }
}
