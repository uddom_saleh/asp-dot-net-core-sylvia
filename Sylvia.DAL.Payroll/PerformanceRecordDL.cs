/* Compamy: Milllennium Information Solution Limited
 * Author: Khandaker Md Jarif
 * Comment Date: November 28, 2010
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.Common.Reports;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using System.Collections.Generic;

namespace Sylvia.DAL.Payroll
{
    class PerformanceRecordDL
    {
        public PerformanceRecordDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_KPI_ADD:
                    return this._createKPI(inputDS);
                case ActionID.ACTION_KPI_UPD:
                    return this._updateKPI(inputDS);
                case ActionID.ACTION_ITEM_ADD:
                    return this._createItem(inputDS);
                case ActionID.ACTION_KPI_DEL:
                    return this._deleteKPI(inputDS);
                case ActionID.NA_ACTION_GET_KPI:
                    return this._getKPI(inputDS);
                case ActionID.NA_ACTION_GET_KPI_LIST:
                    return this._getKPIList(inputDS);
                case ActionID.NA_ACTION_GET_KPI_ITEM:
                    return this._getKPIItem(inputDS);
                case ActionID.ACTION_DOES_KPI_EXIST:
                    return this._doesKPICodeExist(inputDS);
                case ActionID.NA_ACTION_GET_KPI_LIST_BY_KPI_GROUP:
                    return this._getKPIListByKPIGroup(inputDS);
                case ActionID.NA_ACTION_GET_KPI_GROUP_BY_DEPT:
                    return this._getKPIGroupByDept(inputDS);
                case ActionID.NA_ACTION_GET_KPI_LIST_BY_EMP:
                    return this._getKPIListByEmp(inputDS);
                case ActionID.ACTION_KPI_GROUP_ADD:
                    return this._createKPIGroup(inputDS);
                case ActionID.ACTION_KPI_GROUP_UPD:
                    return this._updateKPIGroup(inputDS);
                case ActionID.ACTION_KPI_GROUP_DEL:
                    return this._deleteKPIGroup(inputDS);
                case ActionID.ACTION_EMP_SCORING_ADD:
                    return this._createScoring(inputDS);
                case ActionID.NA_ACTION_GET_SCORED_PERIOD_LIST:
                    return this._getScoredPeriodList(inputDS);
                case ActionID.NA_ACTION_GET_EMP_SCORING_LIST:
                    return this._getEmpScoredList(inputDS);
                case ActionID.ACTION_EMP_SCORING_DEL:
                    return this._deleteScore(inputDS);
                case ActionID.NA_ACTION_GET_DETAIL_SCORED_LIST_RPT:
                    return this._getDetailScoredList(inputDS);
                case ActionID.NA_ACTION_GET_SCORED_LIST_RPT:
                    return this._getScoredList(inputDS);
                case ActionID.NA_ACTION_GET_SCORED_LIST_CHT_RPT:
                    return this._getScoredListForChart(inputDS);
                case ActionID.ACTION_TARGET_ADD:
                    return this._createTarget(inputDS);
                case ActionID.ACTION_TARGET_DEL:
                    return this._deleteTarget(inputDS);
                case ActionID.NA_ACTION_GET_TARGET:
                    return this._getTarget(inputDS);
                case ActionID.ACTION_SIMPARAMETER_ADD:
                    return this._createSIMParam(inputDS);
                case ActionID.NA_ACTION_Q_ALL_SIMPARAMETER:
                    return this._getSIMParam(inputDS);
                case ActionID.NA_ACTION_GET_IncreaseSalary_KPI:
                    return this._getIncreaseSalary_KPI(inputDS);
                case ActionID.ACTION_PA_KPI_MAPPING_DEPTWISE_ADD:
                    return this._createKPI_MappingDepartmentwise(inputDS);
                case ActionID.NA_ACTION_GET_KPI_MAPPING_INFO:
                    return this._getKPI_MappingInfo(inputDS);

                case ActionID.NA_ACTION_GET_KPI_TARGET_SETUP:
                    return this._getKPI_TargetSetup(inputDS);
                case ActionID.ACTION_KPI_GENERAL_TARGET_SAVE:
                    return this._saveKPI_TargetSetup(inputDS);
                case ActionID.ACTION_KPI_GENERAL_TARGET_DEL:
                    return this._deleteKPI_TargetSetup(inputDS);

                case ActionID.ACTION_PA_KPI_MAPPING_EMPWISE_ADD:
                    return this._createKPI_MappingEmployeewise(inputDS);
                case ActionID.NA_GET_KPI_APPROVAL_AUTHORITY:
                    return this._getKPIApprovalAuthority(inputDS);
                case ActionID.ACTION_KPI_APPROVAL_AUTHORITY_SAVE:
                    return this._saveKPIApprovalAuthority(inputDS);

                case ActionID.ACTION_KPI_RANKING_ADD:
                    return this._createKPIRanking(inputDS);
                case ActionID.NA_ACTION_GET_KPI_RANKING:
                    return this._getKPIRanking(inputDS);

                case ActionID.NA_ACTION_GET_EMP_KPI_SCORING:
                    return this._getKPIScoring_Employee(inputDS);
                case ActionID.ACTION_EMP_KPI_SCORING_SAVE:
                    return this._saveKPIScoring_Employee(inputDS);
                case ActionID.ACTION_KPI_ITEM_CONFIG_BY_DEPT:
                    return this._createKPI_ItemConfigByDept(inputDS);
                case ActionID.ACTION_KPI_ITEM_CONFIG_BY_EMP:
                    return this._createKPI_ItemConfigByEmployee(inputDS);

                case ActionID.ACTION_KPI_RECOMMENDATION_ADD:
                    return this._createKPI_Recommendation(inputDS);
                case ActionID.ACTION_KPI_RECOMMENDATION_UPD:
                    return this._updateKPI_Recommendation(inputDS);
                case ActionID.NA_GET_ACTION_KPI_RECOMMENDATION:
                    return this._getKPI_Recommendation(inputDS);
                case ActionID.ACTION_KPI_RECOMMENDATION_DEL:
                    return this._deleteKPI_Recommendation(inputDS);
                case ActionID.NA_ACTION_GET_EMP_WISE_TARGET:
                    return this._getEmp_wise_KPI_Target(inputDS);
                case ActionID.NA_ACTION_GET_EMP_WISE_TARGET_ADD:
                    return this._createEmpWiseTarget(inputDS);
                case ActionID.NA_ACTION_GET_KPI_APPROVAL_INFO:
                    return this._getKPI_ApprovalInfo(inputDS);
                case ActionID.ACTION_PA_KPI_APPROVAL_ADD:
                    return this._createKPI_Approval(inputDS);
                case ActionID.NA_ACTION_GET_KPI_SCORE_RECOMMENDATION:
                    return this._getKPIScoreRecommendation(inputDS);

                case ActionID.ACTION_KPI_SCORE_RECOMMENDATION_SAVE:
                    return this._saveKPIScorRecommendation(inputDS);
                case ActionID.NA_GET_ASSESSMENT_PERIOD:
                    return this._getKPIAssessmentPeriod(inputDS);
                case ActionID.NA_GET_KPI_SCORING_AUTHORITY:
                    return this._getKPIScoringAuthority(inputDS);
                case ActionID.ACTION_KPI_SCORIMG_AUTHORITY_SAVE:
                    return this._saveKPIScoringAuthority(inputDS);
                case ActionID.ACTION_PA_KPI_CHECK:
                    return this._checkKPI_Score(inputDS);
                case ActionID.ACTION_KPI_PROMOTION_POLICY_ADD:
                    return this._createKPI_PromotionPolicy(inputDS);
                case ActionID.NA_GET_KPI_PROMOTION_POLICY:
                    return this._getKPIPromotionPolicy(inputDS);
                case ActionID.ACTION_KPI_PROMOTION_POLICY_DELETE:
                    return _deleteKPIPromotionPolicy(inputDS);

                case ActionID.NA_ACTION_GET_KPIRank_LIST:
                    return this._getKPIRankList(inputDS);
                case ActionID.NA_ACTION_GET_Assesment_Period:
                    return this._getAssesmentPeriod(inputDS);
                case ActionID.NA_ACTION_GET_YEAR_WISE_PERFORMANCE_SCORE:
                    return this._getYearWisePerformance_Score(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _createKPI(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmdKPI = new OleDbCommand();
            cmdKPI.CommandText = "PRO_KPI_CREATE";
            cmdKPI.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmdItem = new OleDbCommand();
            cmdItem.CommandText = "PRO_ITEM_CREATE";
            cmdItem.CommandType = CommandType.StoredProcedure;

            if (cmdKPI == null || cmdItem == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            PerformanceRecordDS prDS = new PerformanceRecordDS();
            prDS.Merge(inputDS.Tables[prDS.PA_ITEM.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Cteate KPI...
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmdKPI.Parameters.AddWithValue("p_KPIID", genPK);
            cmdKPI.Parameters.AddWithValue("p_KPIName", (object)stringDS.DataStrings[0].StringValue);
            cmdKPI.Parameters.AddWithValue("p_IsActive", (object)Convert.ToBoolean(stringDS.DataStrings[1].StringValue));

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdKPI, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create Item...
            long genItemPK;
            foreach (PerformanceRecordDS.PA_ITEMRow row in prDS.PA_ITEM.Rows)
            {
                genItemPK = IDGenerator.GetNextGenericPK();

                cmdItem.Parameters.AddWithValue("p_ItemID", genItemPK);
                cmdItem.Parameters.AddWithValue("p_KPIID", genPK);
                cmdItem.Parameters.AddWithValue("p_ItemName", row.ItemName);
                cmdItem.Parameters.AddWithValue("p_ItemWeightage", row.ItemWeightage);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdItem, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdItem.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateKPI(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmdKPI = new OleDbCommand();
            cmdKPI.CommandText = "PRO_KPI_UPDATE";
            cmdKPI.CommandType = CommandType.StoredProcedure;

            if (cmdKPI == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Update KPI...
            Int32 kpiID = Convert.ToInt32(stringDS.DataStrings[2].StringValue);

            cmdKPI.Parameters.AddWithValue("p_IsActive", (object)Convert.ToBoolean(stringDS.DataStrings[1].StringValue));
            cmdKPI.Parameters.AddWithValue("p_KPIID", (object)kpiID);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdKPI, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createItem(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmdItem = new OleDbCommand();
            cmdItem.CommandText = "PRO_ITEM_CREATE";
            cmdItem.CommandType = CommandType.StoredProcedure;

            if (cmdItem == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            PerformanceRecordDS prDS = new PerformanceRecordDS();
            prDS.Merge(inputDS.Tables[prDS.PA_ITEM.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            Int32 kpiID = Convert.ToInt32(stringDS.DataStrings[2].StringValue);

            #region For Dalete Old Item...
            OleDbCommand cmdDelete = new OleDbCommand();
            cmdDelete.CommandText = "PRO_ITEM_DELETE";
            cmdDelete.CommandType = CommandType.StoredProcedure;

            if (cmdDelete == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmdDelete.Parameters.AddWithValue("p_KPIID", (object)kpiID);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDelete, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ITEM_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create Item...
            long genItemPK;
            foreach (PerformanceRecordDS.PA_ITEMRow row in prDS.PA_ITEM.Rows)
            {
                genItemPK = IDGenerator.GetNextGenericPK();

                cmdItem.Parameters.AddWithValue("p_ItemID", genItemPK);
                cmdItem.Parameters.AddWithValue("p_KPIID", kpiID);
                cmdItem.Parameters.AddWithValue("p_ItemName", row.ItemName);
                cmdItem.Parameters.AddWithValue("p_ItemWeightage", row.ItemWeightage);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdItem, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ITEM_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdItem.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteKPI(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_KPIID", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getKPI(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetKPI");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["KPIID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordPO prPO = new PerformanceRecordPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prPO, prPO.PA_KPI.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            prPO.AcceptChanges();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            foreach (PerformanceRecordPO.PA_KPIRow poKPI in prPO.PA_KPI.Rows)
            {
                PerformanceRecordDS.PA_KPIRow kpi = prDS.PA_KPI.NewPA_KPIRow();

                if (poKPI.IsKPIIDNull() == false)
                {
                    kpi.KPIID = poKPI.KPIID;
                }
                if (poKPI.IsKPINameNull() == false)
                {
                    kpi.KPIName = poKPI.KPIName;
                }
                if (poKPI.IsKPIRemarksNull() == false)
                {
                    kpi.KPIRemarks = poKPI.KPIRemarks;
                }
                if (poKPI.IsIsActiveNull() == false)
                {
                    kpi.IsActive = poKPI.IsActive;
                }
                prDS.PA_KPI.AddPA_KPIRow(kpi);
                prDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getKPIList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd;
            if (stringDS.DataStrings[0].StringValue.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetKPIListAll");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetKPIList");
                cmd.Parameters["IsActive"].Value = (object)(stringDS.DataStrings[0].StringValue.Equals("Active") ? true : false);
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordPO prPO = new PerformanceRecordPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prPO, prPO.PA_KPI.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            prPO.AcceptChanges();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            foreach (PerformanceRecordPO.PA_KPIRow poKPI in prPO.PA_KPI.Rows)
            {
                PerformanceRecordDS.PA_KPIRow kpi = prDS.PA_KPI.NewPA_KPIRow();

                if (poKPI.IsKPIIDNull() == false)
                {
                    kpi.KPIID = poKPI.KPIID;
                }
                if (poKPI.IsKPINameNull() == false)
                {
                    kpi.KPIName = poKPI.KPIName;
                }
                if (poKPI.IsKPIRemarksNull() == false)
                {
                    kpi.KPIRemarks = poKPI.KPIRemarks;
                }
                if (poKPI.IsIsActiveNull() == false)
                {
                    kpi.IsActive = poKPI.IsActive;
                }

                prDS.PA_KPI.AddPA_KPIRow(kpi);
                prDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getKPIItem(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetKPIItem");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordPO prPO = new PerformanceRecordPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prPO, prPO.PA_ITEM.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI_ITEM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            prPO.AcceptChanges();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            foreach (PerformanceRecordPO.PA_ITEMRow poKPI in prPO.PA_ITEM.Rows)
            {
                PerformanceRecordDS.PA_ITEMRow kpi = prDS.PA_ITEM.NewPA_ITEMRow();

                if (poKPI.IsKPIIDNull() == false)
                {
                    kpi.KPIID = poKPI.KPIID;
                }
                if (poKPI.IsKPINameNull() == false)
                {
                    kpi.KPIName = poKPI.KPIName;
                }
                if (poKPI.IsKPIRemarksNull() == false)
                {
                    kpi.KPIRemarks = poKPI.KPIRemarks;
                }
                if (poKPI.IsIsActiveNull() == false)
                {
                    kpi.IsActive = poKPI.IsActive;
                }
                if (poKPI.IsItemIDNull() == false)
                {
                    kpi.ItemID = poKPI.ItemID;
                }
                if (poKPI.IsItemNameNull() == false)
                {
                    kpi.ItemName = poKPI.ItemName;
                }
                if (poKPI.IsItemWeightageNull() == false)
                {
                    kpi.ItemWeightage = poKPI.ItemWeightage;
                }

                prDS.PA_ITEM.AddPA_ITEMRow(kpi);
                prDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _doesKPICodeExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DoesKPIExist");
            cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_KPI_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getKPIListByKPIGroup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetKPIListByKPIGroup"); ;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["KPIGroupID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordPO prPO = new PerformanceRecordPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prPO, prPO.PA_KPI_GROUP.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI_LIST_BY_KPI_GROUP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            prPO.AcceptChanges();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            foreach (PerformanceRecordPO.PA_KPI_GROUPRow poKPI in prPO.PA_KPI_GROUP.Rows)
            {
                PerformanceRecordDS.PA_KPI_GROUPRow kpi = prDS.PA_KPI_GROUP.NewPA_KPI_GROUPRow();

                if (poKPI.IsKPIGroupIDNull() == false)
                {
                    kpi.KPIGroupID = poKPI.KPIGroupID;
                }
                if (poKPI.IsKPIGroupNameNull() == false)
                {
                    kpi.KPIGroupName = poKPI.KPIGroupName;
                }
                if (poKPI.IsDepartmentIDNull() == false)
                {
                    kpi.DepartmentID = poKPI.DepartmentID;
                }
                if (poKPI.IsKPIIDNull() == false)
                {
                    kpi.KPIID = poKPI.KPIID;
                }
                if (poKPI.IsKPINameNull() == false)
                {
                    kpi.KPIName = poKPI.KPIName;
                }
                if (poKPI.IsKPIWeightageNull() == false)
                {
                    kpi.KPIWeightage = poKPI.KPIWeightage;
                }
                if (poKPI.IsRemarksNull() == false)
                {
                    kpi.Remarks = poKPI.Remarks;
                }

                prDS.PA_KPI_GROUP.AddPA_KPI_GROUPRow(kpi);
                prDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getKPIGroupByDept(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetKPIGroupByDept"); ;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["DeptCode"].Value = (object)stringDS.DataStrings[0].StringValue;
            cmd.Parameters["DeptCode1"].Value = (object)stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordPO prPO = new PerformanceRecordPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prPO, prPO.PA_KPI_GROUP.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI_GROUP_BY_DEPT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            prPO.AcceptChanges();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            foreach (PerformanceRecordPO.PA_KPI_GROUPRow poKPI in prPO.PA_KPI_GROUP.Rows)
            {
                PerformanceRecordDS.PA_KPI_GROUPRow kpi = prDS.PA_KPI_GROUP.NewPA_KPI_GROUPRow();

                if (poKPI.IsKPIGroupIDNull() == false)
                {
                    kpi.KPIGroupID = poKPI.KPIGroupID;
                }
                if (poKPI.IsKPIGroupNameNull() == false)
                {
                    kpi.KPIGroupName = poKPI.KPIGroupName;
                }
                if (poKPI.IsDepartmentIDNull() == false)
                {
                    kpi.DepartmentID = poKPI.DepartmentID;
                }
                if (poKPI.IsKPIIDNull() == false)
                {
                    kpi.KPIID = poKPI.KPIID;
                }
                if (poKPI.IsKPINameNull() == false)
                {
                    kpi.KPIName = poKPI.KPIName;
                }
                if (poKPI.IsKPIWeightageNull() == false)
                {
                    kpi.KPIWeightage = poKPI.KPIWeightage;
                }
                if (poKPI.IsRemarksNull() == false)
                {
                    kpi.Remarks = poKPI.Remarks;
                }

                prDS.PA_KPI_GROUP.AddPA_KPI_GROUPRow(kpi);
                prDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getKPIListByEmp(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetKPIListByEmp"); ;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmpCode"].Value = (object)stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordPO prPO = new PerformanceRecordPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prPO, prPO.PA_KPI.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI_LIST_BY_EMP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            prPO.AcceptChanges();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            foreach (PerformanceRecordPO.PA_KPIRow poKPI in prPO.PA_KPI.Rows)
            {
                PerformanceRecordDS.PA_KPIRow kpi = prDS.PA_KPI.NewPA_KPIRow();

                if (poKPI.IsKPIIDNull() == false)
                {
                    kpi.KPIID = poKPI.KPIID;
                }
                if (poKPI.IsKPINameNull() == false)
                {
                    kpi.KPIName = poKPI.KPIName;
                }
                if (poKPI.IsKPIRemarksNull() == false)
                {
                    kpi.KPIRemarks = poKPI.KPIRemarks;
                }
                if (poKPI.IsIsActiveNull() == false)
                {
                    kpi.IsActive = poKPI.IsActive;
                }
                if (poKPI.IsEmployeeIDNull() == false)
                {
                    kpi.EmployeeID = poKPI.EmployeeID;
                }
                if (poKPI.IsKPIWeightageNull() == false)
                {
                    kpi.KPIWeightage = poKPI.KPIWeightage;
                }
                prDS.PA_KPI.AddPA_KPIRow(kpi);
                prDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createKPIGroup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmdKPIGroup = new OleDbCommand();
            cmdKPIGroup.CommandText = "PRO_KPI_GROUP_CREATE";
            cmdKPIGroup.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmdKPIGroupDetail = new OleDbCommand();
            cmdKPIGroupDetail.CommandText = "PRO_KPI_GROUP_DETAIL_CREATE";
            cmdKPIGroupDetail.CommandType = CommandType.StoredProcedure;

            if (cmdKPIGroup == null || cmdKPIGroupDetail == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            PerformanceRecordDS prDS = new PerformanceRecordDS();
            prDS.Merge(inputDS.Tables[prDS.PA_KPI_GROUP.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Cteate KPI Group...
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmdKPIGroup.Parameters.AddWithValue("p_KPIGroupID", genPK);
            cmdKPIGroup.Parameters.AddWithValue("p_KPIGroupName", (object)stringDS.DataStrings[0].StringValue);
            cmdKPIGroup.Parameters.AddWithValue("p_KPIGroupRemarks", stringDS.DataStrings[1].StringValue.Equals("") ? DBNull.Value : (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdKPIGroup, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_GROUP_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create KPI Group Detail...
            foreach (PerformanceRecordDS.PA_KPI_GROUPRow row in prDS.PA_KPI_GROUP.Rows)
            {
                cmdKPIGroupDetail.Parameters.AddWithValue("p_KPIGroupID", genPK);
                cmdKPIGroupDetail.Parameters.AddWithValue("p_DepartmentCode", row.DepartmentCode);
                cmdKPIGroupDetail.Parameters.AddWithValue("p_KPIID", row.KPIID);
                cmdKPIGroupDetail.Parameters.AddWithValue("p_KPIWeightage", row.KPIWeightage);
                if (row.IsRemarksNull() == false)
                {
                    cmdKPIGroupDetail.Parameters.AddWithValue("p_GroupDetailRemarks", row.Remarks);
                }
                else
                {
                    cmdKPIGroupDetail.Parameters.AddWithValue("p_GroupDetailRemarks", DBNull.Value);
                }

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdKPIGroupDetail, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_GROUP_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdKPIGroupDetail.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateKPIGroup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_GROUP_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmdDelete = new OleDbCommand();
            cmdDelete.CommandText = "PRO_KPI_GROUP_DETAIL_DELETE";
            cmdDelete.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmdDelete == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            PerformanceRecordDS prDS = new PerformanceRecordDS();
            prDS.Merge(inputDS.Tables[prDS.PA_KPI_GROUP.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Delete KPI Group Detail...
            cmdDelete.Parameters.AddWithValue("p_KPIGroupID", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDelete, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_GROUP_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Update KPI Group Detail...
            foreach (PerformanceRecordDS.PA_KPI_GROUPRow row in prDS.PA_KPI_GROUP.Rows)
            {
                cmd.Parameters.AddWithValue("p_KPIGroupID", (object)integerDS.DataIntegers[0].IntegerValue);
                cmd.Parameters.AddWithValue("p_DepartmentCode", row.DepartmentCode);
                cmd.Parameters.AddWithValue("p_KPIID", row.KPIID);
                cmd.Parameters.AddWithValue("p_KPIWeightage", row.KPIWeightage);
                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_GroupDetailRemarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_GroupDetailRemarks", DBNull.Value);
                }

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_GROUP_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteKPIGroup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_GROUP_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_KPIGroupID", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_GROUP_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _createScoring(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError;
            int nRowAffected;
            long genScorePK = 0, genKPIScorePK = 0, appraisalID = 0;
            Int32 appraisalType = 0;

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            PerformanceRecordDS prDS = new PerformanceRecordDS();
            prDS.Merge(inputDS.Tables[prDS.PA_SCORE.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            #region For Dalete Old Scored...
            OleDbCommand cmdDelete = new OleDbCommand();
            cmdDelete.CommandText = "PRO_EMP_SCORED_DELETE";
            cmdDelete.CommandType = CommandType.StoredProcedure;

            if (cmdDelete == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (PerformanceRecordDS.PA_SCORERow row in prDS.PA_SCORE.Rows)
            {
                #region Get Value...
                if (row.AppraisalType == AppraisalType.Employee.ToString())
                {
                    appraisalID = UtilDL.GetEmployeeId(connDS, row.AppraisalCode);
                    appraisalType = Convert.ToInt32(AppraisalType.Employee);
                }
                else if (row.AppraisalType == AppraisalType.Department.ToString())
                {
                    appraisalID = UtilDL.GetDepartmentId(connDS, row.AppraisalCode);
                    appraisalType = Convert.ToInt32(AppraisalType.Department);
                }
                else if (row.AppraisalType == AppraisalType.Branch.ToString())
                {
                    appraisalID = UtilDL.GetSiteId(connDS, row.AppraisalCode);
                    appraisalType = Convert.ToInt32(AppraisalType.Branch);
                }
                else if (row.AppraisalType == AppraisalType.Zone.ToString())
                {
                    appraisalID = UtilDL.GetPayrollSiteId(connDS, row.AppraisalCode);
                    appraisalType = Convert.ToInt32(AppraisalType.Zone);
                }
                else if (row.AppraisalType == AppraisalType.Institution.ToString())
                {
                    appraisalID = UtilDL.GetCompanyId(connDS, row.AppraisalCode);
                    appraisalType = Convert.ToInt32(AppraisalType.Institution);
                }
                #endregion

                cmdDelete.Parameters.AddWithValue("p_AppraisalID", appraisalID);
                cmdDelete.Parameters.AddWithValue("p_AppraisalType", appraisalType);
                cmdDelete.Parameters.AddWithValue("p_Period", row.Period);

                break;
            }

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDelete, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region For Create Score...
            OleDbCommand cmdScore = new OleDbCommand();
            cmdScore.CommandText = "PRO_SCORE_CREATE";
            cmdScore.CommandType = CommandType.StoredProcedure;

            if (cmdScore == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (PerformanceRecordDS.PA_SCORERow row in prDS.PA_SCORE.Rows)
            {
                genScorePK = IDGenerator.GetNextGenericPK();
                if (genScorePK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmdScore.Parameters.AddWithValue("p_ScoreID", genScorePK);

                if (row.IsPeriodNull() == false)
                {
                    cmdScore.Parameters.AddWithValue("p_Period", row.Period);
                }
                else
                {
                    cmdScore.Parameters.AddWithValue("p_Period", DBNull.Value);
                }

                cmdScore.Parameters.AddWithValue("p_AppraisalID", appraisalID);

                if (row.IsLoginIDNull() == false)
                {
                    cmdScore.Parameters.AddWithValue("p_LoginID", row.LoginID);
                }
                else
                {
                    cmdScore.Parameters.AddWithValue("p_LoginID", DBNull.Value);
                }

                if (row.IsScoreRemarksNull() == false)
                {
                    cmdScore.Parameters.AddWithValue("p_ScoreRemarks", row.ScoreRemarks);
                }
                else
                {
                    cmdScore.Parameters.AddWithValue("p_ScoreRemarks", DBNull.Value);
                }

                if (row.IsKPIGroupIDNull() == false)
                {
                    cmdScore.Parameters.AddWithValue("p_KPIGroupID", row.KPIGroupID);
                }
                else
                {
                    cmdScore.Parameters.AddWithValue("p_KPIGroupID", DBNull.Value);
                }

                cmdScore.Parameters.AddWithValue("p_AppraisalType", appraisalType);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdScore, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                break;
            }
            #endregion

            #region For Create KPI Score With KPI Score Detail...
            Int32 PreviousKPIID = -1;

            OleDbCommand cmdKPIScore = new OleDbCommand();
            cmdKPIScore.CommandText = "PRO_KPI_SCORE_CREATE";
            cmdKPIScore.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmdKPIScoreDetail = new OleDbCommand();
            cmdKPIScoreDetail.CommandText = "PRO_KPI_SCORE_DETAIL_CREATE";
            cmdKPIScoreDetail.CommandType = CommandType.StoredProcedure;

            if (cmdKPIScore == null || cmdKPIScoreDetail == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (PerformanceRecordDS.PA_SCORERow row in prDS.PA_SCORE.Rows)
            {
                if (PreviousKPIID == row.KPIID) continue;
                PreviousKPIID = row.KPIID;

                cmdKPIScore.Parameters.AddWithValue("p_ScoreID", genScorePK);

                if (row.IsKPIIDNull() == false)
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPIID", row.KPIID);
                }
                else
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPIID", DBNull.Value);
                }

                if (row.IsKPIWeightagedNull() == false)
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPIWeightaged", row.KPIWeightaged);
                }
                else
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPIWeightaged", DBNull.Value);
                }

                if (row.IsKPIObtainedPNull() == false)
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPIObtainedP", row.KPIObtainedP);
                }
                else
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPIObtainedP", DBNull.Value);
                }

                if (row.IsKPIObtainedNull() == false)
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPIObtained", row.KPIObtained);
                }
                else
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPIObtained", DBNull.Value);
                }

                genKPIScorePK = IDGenerator.GetNextGenericPK();
                if (genKPIScorePK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmdKPIScore.Parameters.AddWithValue("p_KPIScoreID", genKPIScorePK);

                if (row.IsKPITargetedNull() == false)
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPITargeted", row.KPITargeted);
                }
                else
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPITargeted", DBNull.Value);
                }

                if (row.IsKPITargetObtainedNull() == false)
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPITargetObtained", row.KPITargetObtained);
                }
                else
                {
                    cmdKPIScore.Parameters.AddWithValue("p_KPITargetObtained", DBNull.Value);
                }

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdKPIScore, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdKPIScore.Parameters.Clear();

                #region For Create KPI Score Detail...
                PerformanceRecordDS.PA_SCORERow[] foundRows = (PerformanceRecordDS.PA_SCORERow[])prDS.PA_SCORE.Select("KPIID = " + PreviousKPIID);

                foreach (PerformanceRecordDS.PA_SCORERow detailRow in foundRows)
                {
                    if (detailRow.IsItemIDNull()) continue;

                    cmdKPIScoreDetail.Parameters.AddWithValue("p_KPIScoreID", genKPIScorePK);

                    if (detailRow.IsItemIDNull() == false)
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemID", detailRow.ItemID);
                    }
                    else
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemID", DBNull.Value);
                    }

                    if (detailRow.IsItemWeightagedNull() == false)
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemWeightaged", detailRow.ItemWeightaged);
                    }
                    else
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemWeightaged", DBNull.Value);
                    }

                    if (detailRow.IsItemObtainedPNull() == false)
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemObtainedP", detailRow.ItemObtainedP);
                    }
                    else
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemObtainedP", DBNull.Value);
                    }

                    if (detailRow.IsItemObtainedNull() == false)
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemObtained", detailRow.ItemObtained);
                    }
                    else
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemObtained", DBNull.Value);
                    }

                    if (detailRow.IsItemTargetedNull() == false)
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemTargeted", detailRow.ItemTargeted);
                    }
                    else
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemTargeted", DBNull.Value);
                    }

                    if (detailRow.IsItemTargetObtainedNull() == false)
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemTargetObtained", detailRow.ItemTargetObtained);
                    }
                    else
                    {
                        cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemTargetObtained", DBNull.Value);
                    }
                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdKPIScoreDetail, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                    cmdKPIScoreDetail.Parameters.Clear();
                }
                #endregion
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getScoredPeriodList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetScoredPeriodList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordPO prPO = new PerformanceRecordPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prPO, prPO.PA_SCORE.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SCORED_PERIOD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            prPO.AcceptChanges();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            foreach (PerformanceRecordPO.PA_SCORERow poScore in prPO.PA_SCORE.Rows)
            {
                PerformanceRecordDS.PA_SCORERow score = prDS.PA_SCORE.NewPA_SCORERow();

                if (poScore.IsPeriodNull() == false)
                {
                    score.Period = poScore.Period;
                }

                prDS.PA_SCORE.AddPA_SCORERow(score);
                prDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getEmpScoredList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            long appraisalID = 0;
            Int32 appraisalType = 0;

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetScoredList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            #region Get Value...
            if (stringDS.DataStrings[1].StringValue == AppraisalType.Employee.ToString())
            {
                appraisalID = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Employee);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Department.ToString())
            {
                appraisalID = UtilDL.GetDepartmentId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Department);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Branch.ToString())
            {
                appraisalID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Branch);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Zone.ToString())
            {
                appraisalID = UtilDL.GetPayrollSiteId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Zone);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Institution.ToString())
            {
                appraisalID = UtilDL.GetCompanyId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Institution);
            }
            #endregion

            cmd.Parameters["AppraisalID"].Value = cmd.Parameters["AppraisalID1"].Value = cmd.Parameters["AppraisalID2"].Value = appraisalID;
            cmd.Parameters["Period"].Value = (object)stringDS.DataStrings[2].StringValue;

            cmd.Parameters["AppraisalType"].Value = appraisalType;
            cmd.Parameters["AppraisalCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["AppraisalType1"].Value = appraisalType;
            cmd.Parameters["AppraisalCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["AppraisalType2"].Value = appraisalType;
            cmd.Parameters["AppraisalCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["AppraisalType3"].Value = appraisalType;
            cmd.Parameters["AppraisalCode3"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["AppraisalType4"].Value = appraisalType;
            cmd.Parameters["AppraisalCode4"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordDS prDS = new PerformanceRecordDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PA_SCORE.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMP_SCORING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _deleteScore(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            long appraisalID = 0;
            Int32 appraisalType = 0;

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            Int32 dataStringValue = stringDS.DataStrings.Count;

            OleDbCommand cmd = new OleDbCommand();
            if (dataStringValue == 1) cmd.CommandText = "PRO_SCORED_DELETE";
            else cmd.CommandText = "PRO_EMP_SCORED_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (dataStringValue == 1) cmd.Parameters.AddWithValue("p_Period", (object)stringDS.DataStrings[0].StringValue);
            else
            {
                #region Get Value...
                if (stringDS.DataStrings[1].StringValue == AppraisalType.Employee.ToString())
                {
                    appraisalID = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
                    appraisalType = Convert.ToInt32(AppraisalType.Employee);
                }
                else if (stringDS.DataStrings[1].StringValue == AppraisalType.Department.ToString())
                {
                    appraisalID = UtilDL.GetDepartmentId(connDS, stringDS.DataStrings[0].StringValue);
                    appraisalType = Convert.ToInt32(AppraisalType.Department);
                }
                else if (stringDS.DataStrings[1].StringValue == AppraisalType.Branch.ToString())
                {
                    appraisalID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[0].StringValue);
                    appraisalType = Convert.ToInt32(AppraisalType.Branch);
                }
                else if (stringDS.DataStrings[1].StringValue == AppraisalType.Zone.ToString())
                {
                    appraisalID = UtilDL.GetPayrollSiteId(connDS, stringDS.DataStrings[0].StringValue);
                    appraisalType = Convert.ToInt32(AppraisalType.Zone);
                }
                else if (stringDS.DataStrings[1].StringValue == AppraisalType.Institution.ToString())
                {
                    appraisalID = UtilDL.GetCompanyId(connDS, stringDS.DataStrings[0].StringValue);
                    appraisalType = Convert.ToInt32(AppraisalType.Institution);
                }
                #endregion

                cmd.Parameters.AddWithValue("p_AppraisalID", appraisalID);
                cmd.Parameters.AddWithValue("p_AppraisalType", appraisalType);
                cmd.Parameters.AddWithValue("p_Period", (object)stringDS.DataStrings[2].StringValue);
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getDetailScoredList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Set Cmd Value...
            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetEmpDetailScoredList_RPT");

            //Int32 appraisalType = 0;
            //if (stringDS.DataStrings[1].StringValue == AppraisalType.Employee.ToString())
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetEmpDetailScoredList_RPT");
            //    appraisalType = Convert.ToInt32(AppraisalType.Employee);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Department.ToString())
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetDepartmentDetailScoredList_RPT");
            //    appraisalType = Convert.ToInt32(AppraisalType.Department);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Branch.ToString())
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetBranchDetailScoredList_RPT");
            //    appraisalType = Convert.ToInt32(AppraisalType.Branch);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Zone.ToString())
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetZoneDetailScoredList_RPT");
            //    appraisalType = Convert.ToInt32(AppraisalType.Zone);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Institution.ToString())
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetInstitutionDetailScoredList_RPT");
            //    appraisalType = Convert.ToInt32(AppraisalType.Institution);
            //}
            #endregion

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["Period"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["EMPLOYEETYPE1"].Value = cmd.Parameters["EMPLOYEETYPE2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["COMPANYCODE1"].Value = cmd.Parameters["COMPANYCODE2"].Value = (object)stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DEPARTMENTCODE1"].Value = cmd.Parameters["DEPARTMENTCODE2"].Value = (object)stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DESIGNATIONCODE1"].Value = cmd.Parameters["DESIGNATIONCODE2"].Value = (object)stringDS.DataStrings[4].StringValue;
            cmd.Parameters["LOCATIONCODE1"].Value = cmd.Parameters["LOCATIONCODE2"].Value = (object)stringDS.DataStrings[5].StringValue;
            cmd.Parameters["GRADECODE1"].Value = cmd.Parameters["GRADECODE2"].Value = (object)stringDS.DataStrings[6].StringValue;
            cmd.Parameters["COSTCENTERCODE1"].Value = cmd.Parameters["COSTCENTERCODE2"].Value = (object)stringDS.DataStrings[7].StringValue;
            cmd.Parameters["FUNCTIONCODE1"].Value = cmd.Parameters["FUNCTIONCODE2"].Value = (object)stringDS.DataStrings[8].StringValue;
            cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[9].StringValue);
            cmd.Parameters["EMPLOYEESTATUS1"].Value = cmd.Parameters["EMPLOYEESTATUS2"].Value = Convert.ToInt32(stringDS.DataStrings[10].StringValue);
            //cmd.Parameters["AppraisalType"].Value = appraisalType;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordPO prPO = new PerformanceRecordPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prPO, prPO.PA_SCORE.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DETAIL_SCORED_LIST_RPT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            prPO.AcceptChanges();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            foreach (PerformanceRecordPO.PA_SCORERow row in prPO.PA_SCORE.Rows)
            {
                PerformanceRecordDS.PA_SCORERow newRow = prDS.PA_SCORE.NewPA_SCORERow();

                if (row.IsScoreIDNull() == false)
                {
                    newRow.ScoreID = row.ScoreID;
                }
                if (row.IsPeriodNull() == false)
                {
                    newRow.Period = row.Period;
                }
                if (row.IsEmployeeCodeNull() == false)
                {
                    newRow.EmployeeCode = row.EmployeeCode;
                }
                if (row.IsEmployeeNameNull() == false)
                {
                    newRow.EmployeeName = row.EmployeeName;
                }
                if (row.IsDepartmentIDNull() == false)
                {
                    newRow.DepartmentID = row.DepartmentID;
                }
                if (row.IsDepartmentNameNull() == false)
                {
                    newRow.DepartmentName = row.DepartmentName;
                }
                if (row.IsEvaluatorNull() == false)
                {
                    newRow.Evaluator = row.Evaluator;
                }
                if (row.IsKPIGroupNameNull() == false)
                {
                    newRow.KPIGroupName = row.KPIGroupName;
                }
                if (row.IsKPIIDNull() == false)
                {
                    newRow.KPIID = row.KPIID;
                }
                if (row.IsKPINameNull() == false)
                {
                    newRow.KPIName = row.KPIName;
                }
                if (row.IsKPIWeightagedNull() == false)
                {
                    newRow.KPIWeightaged = row.KPIWeightaged;
                    newRow.ItemWeightaged = row.KPIWeightaged;
                }
                if (row.IsKPIObtainedPNull() == false)
                {
                    newRow.KPIObtainedP = row.KPIObtainedP;
                    newRow.ItemObtainedP = row.KPIObtainedP;
                }
                if (row.IsKPIObtainedNull() == false)
                {
                    newRow.KPIObtained = row.KPIObtained;
                    newRow.ItemObtained = row.KPIObtained;
                }
                if (row.IsItemIDNull() == false)
                {
                    newRow.ItemID = row.ItemID;
                }
                if (row.IsItemNameNull() == false)
                {
                    newRow.ItemName = row.ItemName;
                }
                else
                {
                    newRow.ItemName = "N/A";
                }
                if (row.IsItemWeightagedNull() == false)
                {
                    newRow.ItemWeightaged = row.ItemWeightaged;
                }
                if (row.IsItemObtainedPNull() == false)
                {
                    newRow.ItemObtainedP = row.ItemObtainedP;
                }
                if (row.IsItemObtainedNull() == false)
                {
                    newRow.ItemObtained = row.ItemObtained;
                }
                if (row.IsCreateDateNull() == false)
                {
                    newRow.CreateDate = row.CreateDate;
                }
                if (row.IsScoreRemarksNull() == false)
                {
                    newRow.ScoreRemarks = row.ScoreRemarks;
                }
                if (row.IsKPIGroupIDNull() == false)
                {
                    newRow.KPIGroupID = row.KPIGroupID;
                }

                prDS.PA_SCORE.AddPA_SCORERow(newRow);
                prDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getScoredList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Set Cmd Value...
            OleDbCommand cmd = new OleDbCommand();
            Int32 appraisalType = 0;
            cmd = DBCommandProvider.GetDBCommand("GetEmpScoredList_RPT");
            #region Old
            //if (stringDS.DataStrings[1].StringValue == AppraisalType.Employee.ToString())
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetEmpScoredList_RPT");
            //    appraisalType = Convert.ToInt32(AppraisalType.Employee);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Department.ToString())
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetDepartmentScoredList_RPT");
            //    appraisalType = Convert.ToInt32(AppraisalType.Department);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Branch.ToString())
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetBranchScoredList_RPT");
            //    appraisalType = Convert.ToInt32(AppraisalType.Branch);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Zone.ToString())
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetZoneScoredList_RPT");
            //    appraisalType = Convert.ToInt32(AppraisalType.Zone);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Institution.ToString())
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetInstitutionScoredList_RPT");
            //    appraisalType = Convert.ToInt32(AppraisalType.Institution);
            //}
            #endregion
            #endregion

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["Period"].Value = (object)stringDS.DataStrings[0].StringValue;

            cmd.Parameters["Period"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["EMPLOYEETYPE1"].Value = cmd.Parameters["EMPLOYEETYPE2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["COMPANYCODE1"].Value = cmd.Parameters["COMPANYCODE2"].Value = (object)stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DEPARTMENTCODE1"].Value = cmd.Parameters["DEPARTMENTCODE2"].Value = (object)stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DESIGNATIONCODE1"].Value = cmd.Parameters["DESIGNATIONCODE2"].Value = (object)stringDS.DataStrings[4].StringValue;
            cmd.Parameters["LOCATIONCODE1"].Value = cmd.Parameters["LOCATIONCODE2"].Value = (object)stringDS.DataStrings[5].StringValue;
            cmd.Parameters["GRADECODE1"].Value = cmd.Parameters["GRADECODE2"].Value = (object)stringDS.DataStrings[6].StringValue;
            cmd.Parameters["COSTCENTERCODE1"].Value = cmd.Parameters["COSTCENTERCODE2"].Value = (object)stringDS.DataStrings[7].StringValue;
            cmd.Parameters["FUNCTIONCODE1"].Value = cmd.Parameters["FUNCTIONCODE2"].Value = (object)stringDS.DataStrings[8].StringValue;
            cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[9].StringValue);
            cmd.Parameters["EMPLOYEESTATUS1"].Value = cmd.Parameters["EMPLOYEESTATUS2"].Value = Convert.ToInt32(stringDS.DataStrings[10].StringValue);
            ////cmd.Parameters["AppraisalType"].Value = appraisalType;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordPO prPO = new PerformanceRecordPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prPO, prPO.PA_SCORE.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SCORED_LIST_RPT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            prPO.AcceptChanges();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            foreach (PerformanceRecordPO.PA_SCORERow row in prPO.PA_SCORE.Rows)
            {
                PerformanceRecordDS.PA_SCORERow newRow = prDS.PA_SCORE.NewPA_SCORERow();

                if (row.IsScoreIDNull() == false)
                {
                    newRow.ScoreID = row.ScoreID;
                }
                if (row.IsPeriodNull() == false)
                {
                    newRow.Period = row.Period;
                }
                if (row.IsEmployeeCodeNull() == false)
                {
                    newRow.EmployeeCode = row.EmployeeCode;
                }
                if (row.IsEmployeeNameNull() == false)
                {
                    newRow.EmployeeName = row.EmployeeName;
                }
                if (row.IsDepartmentIDNull() == false)
                {
                    newRow.DepartmentID = row.DepartmentID;
                }
                if (row.IsDepartmentNameNull() == false)
                {
                    newRow.DepartmentName = row.DepartmentName;
                }
                if (row.IsEvaluatorNull() == false)
                {
                    newRow.Evaluator = row.Evaluator;
                }
                if (row.IsKPIGroupNameNull() == false)
                {
                    newRow.KPIGroupName = row.KPIGroupName;
                }
                if (row.IsKPIIDNull() == false)
                {
                    newRow.KPIID = row.KPIID;
                }
                if (row.IsKPINameNull() == false)
                {
                    newRow.KPIName = row.KPIName;
                }
                if (row.IsTotalKPIWeightagedNull() == false)
                {
                    newRow.TotalKPIWeightaged = row.TotalKPIWeightaged;
                }
                if (row.IsTotalKPIObtainedNull() == false)
                {
                    newRow.TotalKPIObtained = row.TotalKPIObtained;
                }
                if (row.IsCreateDateNull() == false)
                {
                    newRow.CreateDate = row.CreateDate;
                }
                if (row.IsScoreRemarksNull() == false)
                {
                    newRow.ScoreRemarks = row.ScoreRemarks;
                }
                if (row.IsKPIGroupIDNull() == false)
                {
                    newRow.KPIGroupID = row.KPIGroupID;
                }
                if (row.IsPerformanceByGradeNull() == false)
                {
                    newRow.PerformanceByGrade = row.PerformanceByGrade;
                }
                if (row.IsgradepositionNull() == false)
                {
                    newRow.gradeposition = row.gradeposition;
                }
                if (!row.IsKPIRankNull()) newRow.KPIRank = row.KPIRank;
                if (!row.IsDesignationNameNull()) newRow.DesignationName = row.DesignationName;
                if (!row.IsBasicSalaryNull()) newRow.BasicSalary = row.BasicSalary;
                if (!row.IsGradeCodeNull()) newRow.GradeCode = row.GradeCode;

                prDS.PA_SCORE.AddPA_SCORERow(newRow);
                prDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getScoredListForChart(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetScoredList_Cht_RPT");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["Period"].Value = (object)stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordPO prPO = new PerformanceRecordPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prPO, prPO.PA_SCORE.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SCORED_LIST_CHT_RPT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            prPO.AcceptChanges();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            foreach (PerformanceRecordPO.PA_SCORERow row in prPO.PA_SCORE.Rows)
            {
                PerformanceRecordDS.PA_SCORERow newRow = prDS.PA_SCORE.NewPA_SCORERow();

                if (row.IsScoreIDNull() == false)
                {
                    newRow.ScoreID = row.ScoreID;
                }
                if (row.IsPeriodNull() == false)
                {
                    newRow.Period = row.Period;
                }
                if (row.IsEmployeeCodeNull() == false)
                {
                    newRow.EmployeeCode = row.EmployeeCode;
                }
                if (row.IsEmployeeNameNull() == false)
                {
                    newRow.EmployeeName = row.EmployeeName;
                }
                if (row.IsDepartmentIDNull() == false)
                {
                    newRow.DepartmentID = row.DepartmentID;
                }
                if (row.IsDepartmentNameNull() == false)
                {
                    newRow.DepartmentName = row.DepartmentName;
                }
                if (row.IsEvaluatorNull() == false)
                {
                    newRow.Evaluator = row.Evaluator;
                }
                if (row.IsKPIGroupNameNull() == false)
                {
                    newRow.KPIGroupName = row.KPIGroupName;
                }
                if (row.IsKPIIDNull() == false)
                {
                    newRow.KPIID = row.KPIID;
                }
                if (row.IsKPINameNull() == false)
                {
                    newRow.KPIName = row.KPIName;
                }
                if (row.IsTotalKPIWeightagedNull() == false)
                {
                    newRow.TotalKPIWeightaged = row.TotalKPIWeightaged;
                }
                if (row.IsTotalKPIObtainedNull() == false)
                {
                    newRow.TotalKPIObtained = row.TotalKPIObtained;
                }
                if (row.IsCreateDateNull() == false)
                {
                    newRow.CreateDate = row.CreateDate;
                }
                if (row.IsScoreRemarksNull() == false)
                {
                    newRow.ScoreRemarks = row.ScoreRemarks;
                }
                if (row.IsKPIGroupIDNull() == false)
                {
                    newRow.KPIGroupID = row.KPIGroupID;
                }

                prDS.PA_SCORE.AddPA_SCORERow(newRow);
                prDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createTarget(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            long appraisalID = 0;
            Int32 appraisalType = 0;

            OleDbCommand cmdTarget = new OleDbCommand();
            cmdTarget.CommandText = "PRO_TARGET_CREATE";
            cmdTarget.CommandType = CommandType.StoredProcedure;

            if (cmdTarget == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            PerformanceRecordDS prDS = new PerformanceRecordDS();
            prDS.Merge(inputDS.Tables[prDS.PA_TARGET.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region For Dalete Old Target...
            OleDbCommand cmdDelete = new OleDbCommand();
            cmdDelete.CommandText = "PRO_TARGET_DELETE";
            cmdDelete.CommandType = CommandType.StoredProcedure;

            if (cmdDelete == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (PerformanceRecordDS.PA_TARGETRow TargetRow in prDS.PA_TARGET.Rows)
            {
                #region Get Value...
                if (TargetRow.AppraisalType == AppraisalType.Employee.ToString())
                {
                    appraisalID = UtilDL.GetEmployeeId(connDS, TargetRow.AppraisalCode);
                    appraisalType = Convert.ToInt32(AppraisalType.Employee);
                }
                else if (TargetRow.AppraisalType == AppraisalType.Department.ToString())
                {
                    appraisalID = UtilDL.GetDepartmentId(connDS, TargetRow.AppraisalCode);
                    appraisalType = Convert.ToInt32(AppraisalType.Department);
                }
                else if (TargetRow.AppraisalType == AppraisalType.Branch.ToString())
                {
                    appraisalID = UtilDL.GetSiteId(connDS, TargetRow.AppraisalCode);
                    appraisalType = Convert.ToInt32(AppraisalType.Branch);
                }
                else if (TargetRow.AppraisalType == AppraisalType.Zone.ToString())
                {
                    appraisalID = UtilDL.GetPayrollSiteId(connDS, TargetRow.AppraisalCode);
                    appraisalType = Convert.ToInt32(AppraisalType.Zone);
                }
                else if (TargetRow.AppraisalType == AppraisalType.Institution.ToString())
                {
                    appraisalID = UtilDL.GetCompanyId(connDS, TargetRow.AppraisalCode);
                    appraisalType = Convert.ToInt32(AppraisalType.Institution);
                }
                #endregion

                cmdDelete.Parameters.AddWithValue("p_AppraisalID", appraisalID);
                cmdDelete.Parameters.AddWithValue("p_AppraisalType", appraisalType);
                break;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDelete, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_TARGET_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create Target...
            foreach (PerformanceRecordDS.PA_TARGETRow row in prDS.PA_TARGET.Rows)
            {
                cmdTarget.Parameters.AddWithValue("p_AppraisalID", appraisalID);
                cmdTarget.Parameters.AddWithValue("p_LoginID", row.LoginID);
                if (!row.IsKPIIDNull())
                {
                    cmdTarget.Parameters.AddWithValue("p_KPIID", row.KPIID);
                }
                else
                {
                    cmdTarget.Parameters.AddWithValue("p_KPIID", DBNull.Value);
                }
                if (!row.IsItemIDNull())
                {
                    cmdTarget.Parameters.AddWithValue("p_ItemID", row.ItemID);
                }
                else
                {
                    cmdTarget.Parameters.AddWithValue("p_ItemID", DBNull.Value);
                }
                cmdTarget.Parameters.AddWithValue("p_Target", row.Target);
                if (!row.IsTargetRemarksNull())
                {
                    cmdTarget.Parameters.AddWithValue("p_TargetRemarks", row.TargetRemarks);
                }
                else
                {
                    cmdTarget.Parameters.AddWithValue("p_TargetRemarks", DBNull.Value);
                }
                cmdTarget.Parameters.AddWithValue("p_AppraisalType", appraisalType);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdTarget, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TARGET_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdTarget.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteTarget(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            long appraisalID = 0;
            Int32 appraisalType = 0;

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TARGET_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            #region Get Value...
            if (stringDS.DataStrings[1].StringValue == AppraisalType.Employee.ToString())
            {
                appraisalID = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Employee);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Department.ToString())
            {
                appraisalID = UtilDL.GetDepartmentId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Department);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Branch.ToString())
            {
                appraisalID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Branch);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Zone.ToString())
            {
                appraisalID = UtilDL.GetPayrollSiteId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Zone);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Institution.ToString())
            {
                appraisalID = UtilDL.GetCompanyId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Institution);
            }
            #endregion

            cmd.Parameters.AddWithValue("p_AppraisalID", appraisalID);
            cmd.Parameters.AddWithValue("p_AppraisalType", appraisalType);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_TARGET_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getTarget(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            long appraisalID = 0;
            Int32 appraisalType = 0;

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetTarget");

            #region Get Value...
            if (stringDS.DataStrings[1].StringValue == AppraisalType.Employee.ToString())
            {
                appraisalID = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Employee);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Department.ToString())
            {
                appraisalID = UtilDL.GetDepartmentId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Department);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Branch.ToString())
            {
                appraisalID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Branch);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Zone.ToString())
            {
                appraisalID = UtilDL.GetPayrollSiteId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Zone);
            }
            else if (stringDS.DataStrings[1].StringValue == AppraisalType.Institution.ToString())
            {
                appraisalID = UtilDL.GetCompanyId(connDS, stringDS.DataStrings[0].StringValue);
                appraisalType = Convert.ToInt32(AppraisalType.Institution);
            }
            #endregion

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["AppraisalID"].Value = cmd.Parameters["AppraisalID1"].Value = appraisalID;

            cmd.Parameters["AppraisalType"].Value = appraisalType;
            cmd.Parameters["AppraisalCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["AppraisalType1"].Value = appraisalType;
            cmd.Parameters["AppraisalCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["AppraisalType2"].Value = appraisalType;
            cmd.Parameters["AppraisalCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["AppraisalType3"].Value = appraisalType;
            cmd.Parameters["AppraisalCode3"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["AppraisalType4"].Value = appraisalType;
            cmd.Parameters["AppraisalCode4"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordDS prDS = new PerformanceRecordDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PA_TARGET.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TARGET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createSIMParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError;
            int nRowAffected;
            long genPK = 0;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_CREATE_SIMPARAMETER";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            PerformanceRecordDS prDS = new PerformanceRecordDS();
            prDS.Merge(inputDS.Tables[prDS.SIMarginParameter.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region For Dalete Old Param...
            OleDbCommand cmdDelete = new OleDbCommand();
            cmdDelete.CommandText = "PRO_SIMPARAMETER_DELETE";
            cmdDelete.CommandType = CommandType.StoredProcedure;

            if (cmdDelete == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDelete, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SIMPARAMETER_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create New SIMarginParameter...
            foreach (PerformanceRecordDS.SIMarginParameterRow row in prDS.SIMarginParameter.Rows)
            {
                genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("SIMarginID", genPK);
                cmd.Parameters.AddWithValue("p_F_Range", row.F_Range);
                cmd.Parameters.AddWithValue("p_T_Range", row.T_Range);
                cmd.Parameters.AddWithValue("p_Increment_P", row.Increment_P);
                //cmd.Parameters.AddWithValue("p_SIMRemarks", row.SIMRemarks);
                if (!row.IsSIMRemarksNull()) cmd.Parameters.AddWithValue("p_SIMRemarks", row.SIMRemarks);
                else cmd.Parameters.AddWithValue("p_SIMRemarks", DBNull.Value);
                if (!row.IsBonusMultiplierNull()) cmd.Parameters.AddWithValue("p_BonusMultiplier", row.BonusMultiplier);
                else cmd.Parameters.AddWithValue("p_BonusMultiplier", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SIMPARAMETER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getSIMParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllSIMParameter");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordDS prDS = new PerformanceRecordDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.SIMarginParameter.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_SIMPARAMETER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getIncreaseSalary_KPI(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            DateTime EffectDate_PS = new DateTime(1754, 1, 1);
            Int32 GradeID = 0, DesignationID = 0, PayrollSiteID = 0;
            bool bError = false;
            int nRowAffected = -1;

            #region Merge Input DS...
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            #endregion

            #region Get ID's from DataBase
            GradeID = Convert.ToInt32(UtilDL.GetGradeId(connDS, stringDS.DataStrings[1].StringValue));
            DesignationID = Convert.ToInt32(UtilDL.GetDesignationId(connDS, stringDS.DataStrings[2].StringValue));
            PayrollSiteID = Convert.ToInt32(UtilDL.GetPayrollSiteId(connDS, stringDS.DataStrings[3].StringValue));
            #endregion

            #region Select DB Command...
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            #region Increment...
            cmd = DBCommandProvider.GetDBCommand("GetIncreaseSalary_KPI");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EffectDate"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["Period"].Value = stringDS.DataStrings[4].StringValue;

            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;

            cmd.Parameters["LiabilityID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["LiabilityID1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["DepartmentID1"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["SiteID1"].Value = integerDS.DataIntegers[2].IntegerValue;

            cmd.Parameters["GradeID"].Value = GradeID;
            cmd.Parameters["GradeID1"].Value = GradeID;
            cmd.Parameters["DesignationID"].Value = DesignationID;
            cmd.Parameters["DesignationID1"].Value = DesignationID;
            cmd.Parameters["PayrollSiteID"].Value = PayrollSiteID;
            cmd.Parameters["PayrollSiteID1"].Value = PayrollSiteID;
            #endregion

            #endregion

            adapter.SelectCommand = cmd;

            ScaleOfPayDS payFixationDS = new ScaleOfPayDS();

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, payFixationDS, payFixationDS.PayFixation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_IncreaseSalary_KPI.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            payFixationDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(payFixationDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createKPI_MappingDepartmentwise(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DataStringDS stringDS = new DataStringDS();

            OleDbCommand cmd_del = new OleDbCommand();
            cmd_del.CommandText = "PRO_KPI_MAPPING_DEL";
            cmd_del.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_MAPPING_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd_del == null || cmd == null) return UtilDL.GetCommandNotFound();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            prDS.Merge(inputDS.Tables[prDS.PA_KPI_Mapping.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd_del.Parameters.AddWithValue("p_DepartmentID", prDS.PA_KPI_Mapping[0].DepartmentID);

            bool bError_del = false;
            int nRowAffected_del = -1;
            nRowAffected_del = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bError_del);
            cmd_del.Parameters.Clear();
            if (bError_del == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PA_KPI_MAPPING_DEPTWISE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            #region Add KPI Mapping
            if (Convert.ToBoolean(stringDS.DataStrings[0].StringValue)) //Is valid to Save or not
            {
                foreach (PerformanceRecordDS.PA_KPI_MappingRow row in prDS.PA_KPI_Mapping.Rows)
                {
                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) UtilDL.GetDBOperationFailed();
                    else cmd.Parameters.AddWithValue("p_KPI_MappingID", (object)genPK);

                    cmd.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                    cmd.Parameters.AddWithValue("p_KPIGroupID", row.KPIGroupID);

                    if (!row.IsDesignationIDNull()) cmd.Parameters.AddWithValue("p_DesignationID", row.DesignationID);
                    else cmd.Parameters.AddWithValue("p_DesignationID", DBNull.Value);

                    if (!row.IsFunctionIDNull()) cmd.Parameters.AddWithValue("p_FunctionID", row.FunctionID);
                    else cmd.Parameters.AddWithValue("p_FunctionID", DBNull.Value);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PA_KPI_MAPPING_DEPTWISE_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getKPI_MappingInfo(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "KPI_MAPPING_DEPARTMENTWISE")
            {
                cmd = DBCommandProvider.GetDBCommand("GetKPI_Mapping_Departmentwise");
                cmd.Parameters["DepartmentID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            }
            else if (stringDS.DataStrings[0].StringValue == "KPI_MAPPING_EMPLOYEEWISE")
            {
                cmd = DBCommandProvider.GetDBCommand("GetKPI_Mapping_Employeewise");
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["SiteID1"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["DepartmentID1"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["DesignationID1"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["DesignationID2"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
            }
            else if (stringDS.DataStrings[0].StringValue == "KPI_ITEM_SETUP_BY_DEPT")
            {
                cmd = DBCommandProvider.GetDBCommand("GetKPIGroupWithItemByDept");
                cmd.Parameters["DepartmentID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            }
            else if (stringDS.DataStrings[0].StringValue == "KPI_ITEM_SETUP_BY_EMP")
            {
                cmd = DBCommandProvider.GetDBCommand("GetKPIGroupWithItemByEmployee");
                cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = cmd.Parameters["EmployeeCode3"].Value = stringDS.DataStrings[1].StringValue;
            }

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PA_KPI_Mapping.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI_MAPPING_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
            
        }
        private DataSet _createKPI_MappingEmployeewise(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();

            OleDbCommand cmd_del = new OleDbCommand();
            cmd_del.CommandText = "PRO_KPI_MAP_EMPWISE_DEL";
            cmd_del.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_MAP_EMPWISE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd_del == null || cmd == null) return UtilDL.GetCommandNotFound();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            prDS.Merge(inputDS.Tables[prDS.PA_KPI_Mapping.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd_del.Parameters.AddWithValue("p_EmployeeID", stringDS.DataStrings[1].StringValue);

            bool bError_del = false;
            int nRowAffected_del = -1;
            nRowAffected_del = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bError_del);
            cmd_del.Parameters.Clear();
            if (bError_del == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PA_KPI_MAPPING_EMPWISE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            #region Add KPI Mapping
            if (Convert.ToBoolean(stringDS.DataStrings[0].StringValue)) //Is valid to Save or not
            {
                foreach (PerformanceRecordDS.PA_KPI_MappingRow row in prDS.PA_KPI_Mapping.Rows)
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_KPIGroupID", row.KPIGroupID);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PA_KPI_MAPPING_EMPWISE_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getKPI_TargetSetup(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetKPI_Target_Setup");
            cmd.Parameters["DepartmentID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["FunctionID"].Value = cmd.Parameters["FunctionID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["DesignationID"].Value = cmd.Parameters["DesignationID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PA_TARGET.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI_TARGET_SETUP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _saveKPI_TargetSetup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();

            prDS.Merge(inputDS.Tables[prDS.PA_TARGET.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region DELETE old setup...
            OleDbCommand cmd_del = new OleDbCommand();
            cmd_del.CommandText = "PRO_KPI_TARGET_DEL";
            cmd_del.CommandType = CommandType.StoredProcedure;

            cmd_del.Parameters.AddWithValue("p_KPI_MappingID", prDS.PA_TARGET[0].KPI_MappingID);

            bool bError_del = false;
            int nRowAffected_del = -1;
            nRowAffected_del = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bError_del);
            cmd_del.Parameters.Clear();
            if (bError_del == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_GENERAL_TARGET_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region SAVE new setup...
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_TARGET_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (PerformanceRecordDS.PA_TARGETRow row in prDS.PA_TARGET.Rows)
            {
                cmd.Parameters.AddWithValue("p_KPI_MappingID", row.KPI_MappingID);
                cmd.Parameters.AddWithValue("p_ItemID", row.ItemID);
                cmd.Parameters.AddWithValue("p_Target", row.Target);

                if (!row.IsTargetRemarksNull()) cmd.Parameters.AddWithValue("p_TargetRemarks", row.TargetRemarks);
                else cmd.Parameters.AddWithValue("p_TargetRemarks", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_GENERAL_TARGET_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteKPI_TargetSetup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS(); DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd_del = new OleDbCommand();
            cmd_del.CommandText = "PRO_KPI_TARGET_DEL";
            cmd_del.CommandType = CommandType.StoredProcedure;

            cmd_del.Parameters.AddWithValue("p_KPI_MappingID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));

            bool bError_del = false;
            int nRowAffected_del = -1;
            nRowAffected_del = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bError_del);
            cmd_del.Parameters.Clear();
            if (bError_del == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_GENERAL_TARGET_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _createKPIRanking(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError;
            int nRowAffected;
            long genPK = 0;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_CREATE_KPIRANKING";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            PerformanceRecordDS prDS = new PerformanceRecordDS();
            prDS.Merge(inputDS.Tables[prDS.KPIRanking.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region For Dalete Old Param...
            OleDbCommand cmdDelete = new OleDbCommand();
            cmdDelete.CommandText = "PRO_KPIRANKING_DELETE";
            cmdDelete.CommandType = CommandType.StoredProcedure;

            if (cmdDelete == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDelete, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_RANKING_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create New SIMarginParameter...
            foreach (PerformanceRecordDS.KPIRankingRow row in prDS.KPIRanking.Rows)
            {
                genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_KPIRankingID", genPK);
                cmd.Parameters.AddWithValue("p_KPISerialNo", row.KPISerialNo);
                cmd.Parameters.AddWithValue("p_KPIRank", row.KPIRank);
                cmd.Parameters.AddWithValue("p_ScoreFrom", row.ScoreFrom);
                cmd.Parameters.AddWithValue("p_ScoreTo", row.ScoreTo);
                if (!row.IsKPIDetailsNull()) cmd.Parameters.AddWithValue("p_KPIDetails", row.KPIDetails);
                else cmd.Parameters.AddWithValue("p_KPIDetails", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_RANKING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getKPIRanking(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllKPIRanking");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PerformanceRecordDS prDS = new PerformanceRecordDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.KPIRanking.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI_RANKING.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        
        private DataSet _getKPIApprovalAuthority(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Approval Path Config Info
            cmd = DBCommandProvider.GetDBCommand("GetKPI_ApprovalAuthorityAll");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.KPIApprovalPath.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_KPI_APPROVAL_AUTHORITY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();
            #endregion

            #region Get PriorityWise Activity
            cmd = DBCommandProvider.GetDBCommand("GetKPI_PriorityWiseActivityList");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.KPIApprovalAuthority.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_KPI_APPROVAL_AUTHORITY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet KPIActivityListCreate(long ConfigurationID, string[] activityList, int ConnectionID)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_APP_ACT_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 0; i < activityList.Length; i++)
            {
                long pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_App_ActivityID", pKey);
                cmd.Parameters.AddWithValue("p_Activity", activityList[i]);
                cmd.Parameters.AddWithValue("p_ConfigurationID", ConfigurationID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_APPROVAL_AUTHORITY_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _saveKPIApprovalAuthority(DataSet inputDS)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            OleDbCommand cmd;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            prDS.Merge(inputDS.Tables[prDS.KPIApprovalPath.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            #region Delete Old Data
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_APP_PATH_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_APPROVAL_AUTHORITY_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Save new data
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_APP_PATH_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (PerformanceRecordDS.KPIApprovalPathRow row in prDS.KPIApprovalPath.Rows)
            {
                // Get LoginID
                string[] Authority = row.Authority.Split(' ', '@');
                int loginID = 0;
                if (Authority.Length == 1) loginID = 1;
                else loginID = 0;

                //string[] DevidedActivity = row.Activity.Split(' ', ',');

                long pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_ConfigurationID", pKey);
                cmd.Parameters.AddWithValue("p_Priority", row.Priority);
                cmd.Parameters.AddWithValue("p_Authority", row.Authority);
                cmd.Parameters.AddWithValue("p_IsLoginUserID", loginID);

                if (!row.IsDepartmentCodeNull()) cmd.Parameters.AddWithValue("p_DepartmentCode", row.DepartmentCode);
                else cmd.Parameters.AddWithValue("p_DepartmentCode", DBNull.Value);

                if (!row.IsLiabilityIDNull()) cmd.Parameters.AddWithValue("p_LiabilityID", row.LiabilityID);
                else cmd.Parameters.AddWithValue("p_LiabilityID", DBNull.Value);

                cmd.Parameters.AddWithValue("p_AccessPrivilege", row.AccessPrivilege);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_APPROVAL_AUTHORITY_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                // Now Save this user's activities
                //DataSet act_Response = KPIActivityListCreate(pKey, DevidedActivity, connDS.DBConnections[0].ConnectionID);

                //ErrorDS errDS_Act = new ErrorDS();
                //errDS_Act.Merge(act_Response.Tables[errDS_Act.Errors.TableName], false, MissingSchemaAction.Error);
                //errDS_Act.AcceptChanges();
                //if (errDS_Act.Errors.Count > 0) return errDS_Act;
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getKPIScoring_Employee(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetKPIScoring_Employee");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            //cmd.Parameters["EvaluationDate"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["ScorerEmpCode"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["DepartmentID1"].Value = cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);

            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PA_SCORE.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMP_KPI_SCORING.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _saveKPIScoring_Employee(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError;
            int nRowAffected;
            long genSummaryID = 0, genScorePK = 0, genKPIScorePK = 0, appraisalID = 0;
            Int32 appraisalType = 0;
            bool isPreviouslyScored = false;
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            PerformanceRecordDS.PA_SCORERow summaryRow;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            prDS.Merge(inputDS.Tables[prDS.PA_SCORE.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            summaryRow = prDS.PA_SCORE[0];

            #region To create Score_Summary...
            OleDbCommand cmdSum = new OleDbCommand();
            cmdSum.CommandText = "PRO_SCORE_SUMMARY_CREATE";
            cmdSum.CommandType = CommandType.StoredProcedure;

            if (cmdSum == null) return UtilDL.GetCommandNotFound();

            // Continue if it's the FIRST POSTING.
            if (!summaryRow.IsScore_SummaryIDNull() && summaryRow.Score_SummaryID > 0)
            { genSummaryID = summaryRow.Score_SummaryID; }
            else
            {
                genSummaryID = IDGenerator.GetNextGenericPK();
                if (genSummaryID == -1) return UtilDL.GetDBOperationFailed();

                cmdSum.Parameters.AddWithValue("p_Score_SummaryID", genSummaryID);
                cmdSum.Parameters.AddWithValue("p_Pa_AssessmentID", summaryRow.PA_AssessmentID);
                cmdSum.Parameters.AddWithValue("p_EmployeeCode", summaryRow.EmployeeCode);
                cmdSum.Parameters.AddWithValue("p_IsKpiSubmitted", summaryRow.IsKPISubmitted);
                cmdSum.Parameters.AddWithValue("p_IsKpiApprroved", summaryRow.IsKPIApprroved);
                cmdSum.Parameters.AddWithValue("p_New_Score", summaryRow.New_Score);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdSum, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdSum.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region To Create/Update Score...
            OleDbCommand cmdScore = new OleDbCommand();
            cmdScore.CommandText = "PRO_SCORE_CREATE_NEW";
            cmdScore.CommandType = CommandType.StoredProcedure;

            if (cmdScore == null) return UtilDL.GetCommandNotFound();

            foreach (PerformanceRecordDS.PA_SCORERow row in prDS.PA_SCORE.Rows)
            {
                // If score was previously posted, then SKIP this step.
                if (!row.IsScoreIDNull() && row.ScoreID > 0)
                {
                    genScorePK = row.ScoreID;
                    isPreviouslyScored = true;
                    break;
                }

                genScorePK = IDGenerator.GetNextGenericPK();
                if (genScorePK == -1) return UtilDL.GetDBOperationFailed();

                cmdScore.Parameters.AddWithValue("p_ScoreID", genScorePK);

                if (!row.IsPeriodNull()) cmdScore.Parameters.AddWithValue("p_Period", row.Period);
                else cmdScore.Parameters.AddWithValue("p_Period", DBNull.Value);

                cmdScore.Parameters.AddWithValue("p_AppraisalID", appraisalID);

                if (!row.IsLoginIDNull()) cmdScore.Parameters.AddWithValue("p_LoginID", row.LoginID);
                else cmdScore.Parameters.AddWithValue("p_LoginID", DBNull.Value);

                if (!row.IsScoreRemarksNull()) cmdScore.Parameters.AddWithValue("p_ScoreRemarks", row.ScoreRemarks);
                else cmdScore.Parameters.AddWithValue("p_ScoreRemarks", DBNull.Value);

                if (!row.IsKPIGroupIDNull()) cmdScore.Parameters.AddWithValue("p_KPIGroupID", row.KPIGroupID);
                else cmdScore.Parameters.AddWithValue("p_KPIGroupID", DBNull.Value);

                cmdScore.Parameters.AddWithValue("p_AppraisalType", appraisalType);

                if (!row.IsEmployeeCodeNull()) cmdScore.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                else cmdScore.Parameters.AddWithValue("p_EmployeeCode", DBNull.Value);

                cmdScore.Parameters.AddWithValue("p_Score_SummaryID", genSummaryID);

                if (!row.IsEval_Start_DateNull()) cmdScore.Parameters.AddWithValue("p_Eval_Start_Date", row.Eval_Start_Date);
                else cmdScore.Parameters.AddWithValue("p_Eval_Start_Date", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdScore, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdScore.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                break;
            }
            #endregion

            #region For Create/Update KPI Score With KPI Score Detail...
            Int32 PreviousKPIID = -1;

            OleDbCommand cmdKPIScore = new OleDbCommand();
            cmdKPIScore.CommandText = "PRO_KPI_SCORE_CREATE";
            cmdKPIScore.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmdKPIScoreDetail = new OleDbCommand();
            cmdKPIScoreDetail.CommandText = "PRO_KPI_SCORE_DETAIL_ADD_NEW";
            cmdKPIScoreDetail.CommandType = CommandType.StoredProcedure;

            if (cmdKPIScore == null || cmdKPIScoreDetail == null) return UtilDL.GetCommandNotFound();

            foreach (PerformanceRecordDS.PA_SCORERow row in prDS.PA_SCORE.Rows)
            {
                #region Create KPI_Score
                if (PreviousKPIID == row.KPIID) continue;
                PreviousKPIID = row.KPIID;

                cmdKPIScore.Parameters.AddWithValue("p_ScoreID", genScorePK);

                if (!row.IsKPIIDNull()) cmdKPIScore.Parameters.AddWithValue("p_KPIID", row.KPIID);
                else cmdKPIScore.Parameters.AddWithValue("p_KPIID", DBNull.Value);

                if (!row.IsKPIWeightagedNull()) cmdKPIScore.Parameters.AddWithValue("p_KPIWeightaged", row.KPIWeightaged);
                else cmdKPIScore.Parameters.AddWithValue("p_KPIWeightaged", DBNull.Value);

                if (!row.IsKPIObtainedPNull()) cmdKPIScore.Parameters.AddWithValue("p_KPIObtainedP", row.KPIObtainedP);
                else cmdKPIScore.Parameters.AddWithValue("p_KPIObtainedP", DBNull.Value);

                if (!row.IsKPIObtainedNull()) cmdKPIScore.Parameters.AddWithValue("p_KPIObtained", row.KPIObtained);
                else cmdKPIScore.Parameters.AddWithValue("p_KPIObtained", DBNull.Value);

                if (!row.IsKPITargetedNull()) cmdKPIScore.Parameters.AddWithValue("p_KPITargeted", row.KPITargeted);
                else cmdKPIScore.Parameters.AddWithValue("p_KPITargeted", DBNull.Value);

                if (!row.IsKPITargetObtainedNull()) cmdKPIScore.Parameters.AddWithValue("p_KPITargetObtained", row.KPITargetObtained);
                else cmdKPIScore.Parameters.AddWithValue("p_KPITargetObtained", DBNull.Value);

                cmdKPIScore.Parameters.AddWithValue("p_EvaluationDate", row.EvaluationDate);
                cmdKPIScore.Parameters.AddWithValue("p_Evaluator", row.EvaluatorID);
                cmdKPIScore.Parameters.AddWithValue("p_Score_Serial", row.Score_Serial);
                cmdKPIScore.Parameters.AddWithValue("p_Score_Serial_Old", row.Score_Serial_Old);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdKPIScore, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdKPIScore.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                #region For Create KPI Score Detail...
                PerformanceRecordDS.PA_SCORERow[] foundRows = (PerformanceRecordDS.PA_SCORERow[])prDS.PA_SCORE.Select("KPIID = " + PreviousKPIID);

                foreach (PerformanceRecordDS.PA_SCORERow detailRow in foundRows)
                {
                    if (detailRow.IsItemIDNull()) continue;

                    if (!detailRow.IsItemIDNull()) cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemID", detailRow.ItemID);
                    else cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemID", DBNull.Value);

                    if (!detailRow.IsItemWeightagedNull()) cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemWeightaged", detailRow.ItemWeightaged);
                    else cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemWeightaged", DBNull.Value);

                    if (!detailRow.IsItemObtainedPNull()) cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemObtainedP", detailRow.ItemObtainedP);
                    else cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemObtainedP", DBNull.Value);

                    if (!detailRow.IsItemObtainedNull()) cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemObtained", detailRow.ItemObtained);
                    else cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemObtained", DBNull.Value);

                    if (!detailRow.IsItemTargetedNull()) cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemTargeted", detailRow.ItemTargeted);
                    else cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemTargeted", DBNull.Value);

                    if (!detailRow.IsItemTargetObtainedNull()) cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemTargetObtained", detailRow.ItemTargetObtained);
                    else cmdKPIScoreDetail.Parameters.AddWithValue("p_ItemTargetObtained", DBNull.Value);

                    if (!detailRow.IsCum_TargetObtainedNull()) cmdKPIScoreDetail.Parameters.AddWithValue("p_Cum_TargetObtained", detailRow.Cum_TargetObtained);
                    else cmdKPIScoreDetail.Parameters.AddWithValue("p_Cum_TargetObtained", DBNull.Value);
                    if (!detailRow.IsCum_ItemObtainedNull()) cmdKPIScoreDetail.Parameters.AddWithValue("p_Cum_ItemObtained", detailRow.Cum_ItemObtained);
                    else cmdKPIScoreDetail.Parameters.AddWithValue("p_Cum_ItemObtained", DBNull.Value);
                    if (!detailRow.IsCum_DurationNull()) cmdKPIScoreDetail.Parameters.AddWithValue("p_Cum_Duration", detailRow.Cum_Duration);
                    else cmdKPIScoreDetail.Parameters.AddWithValue("p_Cum_Duration", DBNull.Value);
                    if (!detailRow.IsDurationNull()) cmdKPIScoreDetail.Parameters.AddWithValue("p_Duration", detailRow.Duration);
                    else cmdKPIScoreDetail.Parameters.AddWithValue("p_Duration", DBNull.Value);

                    cmdKPIScoreDetail.Parameters.AddWithValue("p_ScoreID", genScorePK);
                    cmdKPIScoreDetail.Parameters.AddWithValue("p_KpiID", detailRow.KPIID);
                    cmdKPIScoreDetail.Parameters.AddWithValue("p_Score_Serial", detailRow.Score_Serial);
                    cmdKPIScoreDetail.Parameters.AddWithValue("p_Score_Serial_Old", detailRow.Score_Serial_Old);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdKPIScoreDetail, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                    cmdKPIScoreDetail.Parameters.Clear();
                }
                #endregion
            }
            #endregion

            #region To UPDATE Score_Summary...
            cmdSum = new OleDbCommand();
            cmdSum.CommandText = "PRO_SCORE_SUMMARY_UPDATE";
            cmdSum.CommandType = CommandType.StoredProcedure;

            if (cmdSum == null) return UtilDL.GetCommandNotFound();

            cmdSum.Parameters.AddWithValue("p_Score_SummaryID", genSummaryID);

            if (!summaryRow.IsEmployeeTotalScoreNull()) cmdSum.Parameters.AddWithValue("p_Score_Total", summaryRow.EmployeeTotalScore);
            else cmdSum.Parameters.AddWithValue("p_Score_Total", DBNull.Value);
            if (!summaryRow.IsKPIRankNull()) cmdSum.Parameters.AddWithValue("p_KPIRank", summaryRow.KPIRank);
            else cmdSum.Parameters.AddWithValue("p_KPIRank", DBNull.Value);

            cmdSum.Parameters.AddWithValue("p_IsKpiSubmitted", summaryRow.IsKPISubmitted);
            cmdSum.Parameters.AddWithValue("p_IsKpiApprroved", summaryRow.IsKPIApprroved);
            cmdSum.Parameters.AddWithValue("p_New_Score", summaryRow.New_Score);

            if (!summaryRow.IsCheckerNull()) cmdSum.Parameters.AddWithValue("p_Checker", summaryRow.Checker);
            else cmdSum.Parameters.AddWithValue("p_Checker", DBNull.Value);
            if (!summaryRow.IsChecked_ByNull()) cmdSum.Parameters.AddWithValue("p_Checked_By", summaryRow.Checked_By);
            else cmdSum.Parameters.AddWithValue("p_Checked_By", DBNull.Value);
            if (!summaryRow.IsChecked_SerialNull()) cmdSum.Parameters.AddWithValue("p_Checked_Serial", summaryRow.Checked_Serial);
            else cmdSum.Parameters.AddWithValue("p_Checked_Serial", DBNull.Value);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdSum, connDS.DBConnections[0].ConnectionID, ref bError);
            cmdSum.Parameters.Clear();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region To create Score_Summary History...

            OleDbCommand cmd_Hist = new OleDbCommand();
            cmd_Hist.CommandText = "PRO_KPI_SCORE_SUM_HIST_ADD";
            cmd_Hist.CommandType = CommandType.StoredProcedure;

            if (cmd_Hist == null) return UtilDL.GetCommandNotFound();

            if (!summaryRow.IsIsRequestFromApprovalNull() && summaryRow.IsRequestFromApproval)
            {
                long genID = IDGenerator.GetNextGenericPK();
                if (genID == -1) return UtilDL.GetDBOperationFailed();

                cmd_Hist.Parameters.AddWithValue("p_Score_HistoryID", genID);
                cmd_Hist.Parameters.AddWithValue("p_Score_SummaryID", genSummaryID);

                if (!summaryRow.IsChanged_ScoreNull()) cmd_Hist.Parameters.AddWithValue("p_Changed_Score", summaryRow.Changed_Score);
                else cmd_Hist.Parameters.AddWithValue("p_Changed_Score", DBNull.Value);

                if (!summaryRow.IsChanged_KPIRankNull()) cmd_Hist.Parameters.AddWithValue("p_KPIRank", summaryRow.Changed_KPIRank);
                else cmd_Hist.Parameters.AddWithValue("p_KPIRank", DBNull.Value);

                if (!summaryRow.IsSenderEmployeeCodeNull()) cmd_Hist.Parameters.AddWithValue("p_SenderEmployeeCode", summaryRow.SenderEmployeeCode);
                else cmd_Hist.Parameters.AddWithValue("p_SenderEmployeeCode", DBNull.Value);

                if (!summaryRow.IsReceiverUserIDNull()) cmd_Hist.Parameters.AddWithValue("p_ReceiverUserID", summaryRow.ReceiverUserID);
                else cmd_Hist.Parameters.AddWithValue("p_ReceiverUserID", DBNull.Value);

                if (!summaryRow.IsPriorityNull()) cmd_Hist.Parameters.AddWithValue("p_Priority", summaryRow.Priority);
                else cmd_Hist.Parameters.AddWithValue("p_Priority", DBNull.Value);

                if (!summaryRow.IsScore_ActivityNull()) cmd_Hist.Parameters.AddWithValue("p_Score_Activity", summaryRow.Score_Activity);
                else cmd_Hist.Parameters.AddWithValue("p_Score_Activity", DBNull.Value);

                if (!summaryRow.IsIsKPISubmittedNull()) cmd_Hist.Parameters.AddWithValue("p_IsKPISubmitted", summaryRow.IsKPISubmitted);
                else cmd_Hist.Parameters.AddWithValue("p_IsKPISubmitted", false);

                if (!summaryRow.IsIsKPIApprrovedNull()) cmd_Hist.Parameters.AddWithValue("p_IsKPIApprroved", summaryRow.IsKPIApprroved);
                else cmd_Hist.Parameters.AddWithValue("p_IsKPIApprroved", false);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Hist, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdSum.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SCORING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createKPI_ItemConfigByDept(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DataStringDS stringDS = new DataStringDS();

            OleDbCommand cmd_del = new OleDbCommand();
            cmd_del.CommandText = "PRO_KPI_ITEM_CONFIG_DEL";
            cmd_del.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_ITEM_CONFIG_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd_del == null || cmd == null) return UtilDL.GetCommandNotFound();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            prDS.Merge(inputDS.Tables[prDS.PA_KPI_Mapping.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd_del.Parameters.AddWithValue("p_DepartmentID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            cmd_del.Parameters.AddWithValue("p_KPIGroupID", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
            //cmd_del.Parameters.AddWithValue("p_ItemID", stringDS.DataStrings[2].StringValue);
            cmd_del.Parameters.AddWithValue("p_Kpiid", stringDS.DataStrings[2].StringValue);

            bool bError_del = false;
            int nRowAffected_del = -1;
            nRowAffected_del = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bError_del);
            cmd_del.Parameters.Clear();
            if (bError_del == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_ITEM_CONFIG_BY_DEPT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            #region Add...
            foreach (PerformanceRecordDS.PA_KPI_MappingRow row in prDS.PA_KPI_Mapping.Rows)
            {
                cmd.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                cmd.Parameters.AddWithValue("p_KPIGroupID", row.KPIGroupID);
                cmd.Parameters.AddWithValue("p_ItemID", row.ItemID);
                cmd.Parameters.AddWithValue("p_ItemWeightage", row.ItemWeightage);
                cmd.Parameters.AddWithValue("p_Kpiid", row.KPIID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_ITEM_CONFIG_BY_DEPT.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _createKPI_ItemConfigByEmployee(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            prDS.Merge(inputDS.Tables[prDS.PA_KPI_Mapping.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region DELETE old setup..
            OleDbCommand cmd_del = new OleDbCommand();
            cmd_del.CommandText = "PRO_KPI_ITEM_CONFIG_EMP_DEL";
            cmd_del.CommandType = CommandType.StoredProcedure;

            cmd_del.Parameters.AddWithValue("p_EmployeeCode", stringDS.DataStrings[0].StringValue);
            cmd_del.Parameters.AddWithValue("p_KPIGroupID", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
            //cmd_del.Parameters.AddWithValue("p_ItemID", stringDS.DataStrings[2].StringValue);
            cmd_del.Parameters.AddWithValue("p_KpiId", Convert.ToInt32(stringDS.DataStrings[2].StringValue));

            bool bError_del = false;
            int nRowAffected_del = -1;
            nRowAffected_del = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bError_del);
            cmd_del.Parameters.Clear();
            if (bError_del == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_ITEM_CONFIG_BY_EMP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Add...
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_ITEM_CONFIG_EMP_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (PerformanceRecordDS.PA_KPI_MappingRow row in prDS.PA_KPI_Mapping.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeCode", stringDS.DataStrings[0].StringValue);
                cmd.Parameters.AddWithValue("p_KPIGroupID", row.KPIGroupID);
                cmd.Parameters.AddWithValue("p_ItemID", row.ItemID);
                cmd.Parameters.AddWithValue("p_ItemWeightage", row.ItemWeightage);
                cmd.Parameters.AddWithValue("p_KpiId", row.KPIID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_ITEM_CONFIG_BY_EMP.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createKPI_Recommendation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            #region SAVE new setup...
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_RECOMMENDATION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;


            cmd.Parameters.AddWithValue("p_Recommendation", stringDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[1].StringValue);


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_RECOMMENDATION_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateKPI_Recommendation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            #region SAVE new setup...
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_RECOMMENDATION_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[1].StringValue);
            cmd.Parameters.AddWithValue("p_Recommendation", stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_RECOMMENDATION_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getKPI_Recommendation(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            cmd = DBCommandProvider.GetDBCommand("GetKPI_RecommendationAll");

            if (cmd == null) return UtilDL.GetCommandNotFound();


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            DataSet DataDS = new DataSet();
            DataDS.Tables.Add("RecordList");

            nRowAffected = ADOController.Instance.Fill(adapter, DataDS, DataDS.Tables["RecordList"].ToString(), connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_ACTION_KPI_RECOMMENDATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                returnDS.Merge(errDS);
                return returnDS;
            }
            DataDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(DataDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _deleteKPI_Recommendation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            #region SAVE new setup...
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_RECOMMENDATION_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("p_Recommendation", stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_RECOMMENDATION_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getEmp_wise_KPI_Target(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            long appraisalID = 0;
            Int32 appraisalType = 0;
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region ...OLD...
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmp_KPI_Item");

            //#region Get Value...OLD
            //if (stringDS.DataStrings[1].StringValue == AppraisalType.Employee.ToString())
            //{
            //    appraisalID = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
            //    appraisalType = Convert.ToInt32(AppraisalType.Employee);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Department.ToString())
            //{
            //    appraisalID = UtilDL.GetDepartmentId(connDS, stringDS.DataStrings[0].StringValue);
            //    appraisalType = Convert.ToInt32(AppraisalType.Department);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Branch.ToString())
            //{
            //    appraisalID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[0].StringValue);
            //    appraisalType = Convert.ToInt32(AppraisalType.Branch);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Zone.ToString())
            //{
            //    appraisalID = UtilDL.GetPayrollSiteId(connDS, stringDS.DataStrings[0].StringValue);
            //    appraisalType = Convert.ToInt32(AppraisalType.Zone);
            //}
            //else if (stringDS.DataStrings[1].StringValue == AppraisalType.Institution.ToString())
            //{
            //    appraisalID = UtilDL.GetCompanyId(connDS, stringDS.DataStrings[0].StringValue);
            //    appraisalType = Convert.ToInt32(AppraisalType.Institution);
            //}
            //#endregion

            //if (cmd == null) return UtilDL.GetCommandNotFound();

            //cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = 
            //    cmd.Parameters["EmployeeCode3"].Value = stringDS.DataStrings[0].StringValue;
            //cmd.Parameters["DepartmentID1"].Value = cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
            #endregion

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmp_KPI_Target");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PA_TARGET.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMP_WISE_TARGET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createEmpWiseTarget(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            long appraisalID = 0;
            Int32 appraisalType = 0;

            OleDbCommand cmdTarget = new OleDbCommand();
            cmdTarget.CommandText = "PRO_EMP_WISE_TARGET_CREATE";
            cmdTarget.CommandType = CommandType.StoredProcedure;

            if (cmdTarget == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            PerformanceRecordDS prDS = new PerformanceRecordDS();
            prDS.Merge(inputDS.Tables[prDS.PA_TARGET.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool bError = false;
            int nRowAffected = -1;

            #region Create Target...
            foreach (PerformanceRecordDS.PA_TARGETRow row in prDS.PA_TARGET.Rows)
            {
                cmdTarget.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                cmdTarget.Parameters.AddWithValue("p_ItemID", row.ItemID);
                cmdTarget.Parameters.AddWithValue("p_Target", row.Target);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdTarget, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMP_WISE_TARGET_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdTarget.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getKPI_ApprovalInfo(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "KPIApproval")
            {
                long employeeID = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[1].StringValue);
                if (employeeID == 0) employeeID = -1;
                long receiveUserID = UtilDL.GetUserId(connDS, stringDS.DataStrings[2].StringValue);
                if (receiveUserID == 0) receiveUserID = -1;

                if (stringDS.DataStrings[10].StringValue == "Checker") cmd = DBCommandProvider.GetDBCommand("GetKPI_ForCheck");
                else cmd = DBCommandProvider.GetDBCommand("GetKPI_ForApproval");

                cmd.Parameters["EvaluatorID1"].Value = employeeID;
                cmd.Parameters["EvaluatorID2"].Value = employeeID;
                cmd.Parameters["ReceivedUserID1"].Value = receiveUserID;
                cmd.Parameters["ReceivedUserID2"].Value = receiveUserID;
                cmd.Parameters["CheckerEmployee1"].Value = cmd.Parameters["CheckerEmployee2"].Value = stringDS.DataStrings[9].StringValue;
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[4].StringValue.ToUpper() + "%";
                cmd.Parameters["SiteID1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
                cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
                cmd.Parameters["DepartmentID1"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                cmd.Parameters["DesignationID1"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);
                cmd.Parameters["DesignationID2"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);
            }
            else if (stringDS.DataStrings[0].StringValue == "KPIApproval_ScoreDetails")
            {
                cmd = DBCommandProvider.GetDBCommand("GetKPI_ScoreDetails_ForApproval");
                cmd.Parameters["ScoreSummaryID_All"].Value = stringDS.DataStrings[1].StringValue;
            }


            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PA_SCORE.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI_APPROVAL_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createKPI_Approval(DataSet inputDS)
        {
            bool bError;
            int nRowAffected;
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DataBoolDS boolDS = new DataBoolDS();
            DataStringDS stringDS = new DataStringDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            string messageBody = "", returnMessage = "", URL = "";

            #region Get Email Information from UtilDL
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_SCORE_DETAIL_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Hist = new OleDbCommand();
            cmd_Hist.CommandText = "PRO_KPI_SCORE_SUM_HIST_ADD";
            cmd_Hist.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmd_Hist == null) return UtilDL.GetCommandNotFound();

            prDS.Merge(inputDS.Tables[prDS.PA_SCORE.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Add KPI Approval
            bError = false;
            nRowAffected = -1;

            if (!boolDS.DataBools[0].BoolValue) //If NotifyByMail is  not Checked
            {
                foreach (PerformanceRecordDS.PA_SCORERow detailRow in prDS.PA_SCORE.Rows)
                {
                    if (!detailRow.IsIsRequestFromApprovalNull() && detailRow.IsRequestFromApproval)
                    {
                        long genID = IDGenerator.GetNextGenericPK();
                        if (genID == -1) return UtilDL.GetDBOperationFailed();

                        cmd_Hist.Parameters.AddWithValue("p_Score_HistoryID", genID);

                        if (!detailRow.IsScore_SummaryIDNull()) cmd_Hist.Parameters.AddWithValue("p_Score_SummaryID", detailRow.Score_SummaryID);
                        else cmd_Hist.Parameters.AddWithValue("p_Score_SummaryID", DBNull.Value);

                        if (!detailRow.IsChanged_ScoreNull()) cmd_Hist.Parameters.AddWithValue("p_Changed_Score", detailRow.Changed_Score);
                        else cmd_Hist.Parameters.AddWithValue("p_Changed_Score", DBNull.Value);

                        if (!detailRow.IsChanged_KPIRankNull()) cmd_Hist.Parameters.AddWithValue("p_KPIRank", detailRow.Changed_KPIRank);
                        else cmd_Hist.Parameters.AddWithValue("p_KPIRank", DBNull.Value);

                        if (!detailRow.IsSenderEmployeeCodeNull()) cmd_Hist.Parameters.AddWithValue("p_SenderEmployeeCode", detailRow.SenderEmployeeCode);
                        else cmd_Hist.Parameters.AddWithValue("p_SenderEmployeeCode", DBNull.Value);

                        if (!detailRow.IsReceiverUserIDNull()) cmd_Hist.Parameters.AddWithValue("p_ReceiverUserID", detailRow.ReceiverUserID);
                        else cmd_Hist.Parameters.AddWithValue("p_ReceiverUserID", DBNull.Value);

                        if (!detailRow.IsPriorityNull()) cmd_Hist.Parameters.AddWithValue("p_Priority", detailRow.Priority);
                        else cmd_Hist.Parameters.AddWithValue("p_Priority", DBNull.Value);

                        if (!detailRow.IsScore_ActivityNull()) cmd_Hist.Parameters.AddWithValue("p_Score_Activity", detailRow.Score_Activity);
                        else cmd_Hist.Parameters.AddWithValue("p_Score_Activity", DBNull.Value);

                        if (!detailRow.IsIsKPISubmittedNull()) cmd_Hist.Parameters.AddWithValue("p_IsKPISubmitted", detailRow.IsKPISubmitted);
                        else cmd_Hist.Parameters.AddWithValue("p_IsKPISubmitted", false);

                        if (!detailRow.IsIsKPIApprrovedNull()) cmd_Hist.Parameters.AddWithValue("p_IsKPIApprroved", detailRow.IsKPIApprroved);
                        else cmd_Hist.Parameters.AddWithValue("p_IsKPIApprroved", false);

                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Hist, connDS.DBConnections[0].ConnectionID, ref bError);

                    }
                    //else
                    //{
                    //    if (detailRow.IsItemIDNull()) continue;

                    //    cmd.Parameters.AddWithValue("p_KPIScoreID", detailRow.KPIScoreID);

                    //    if (!detailRow.IsItemIDNull()) cmd.Parameters.AddWithValue("p_ItemID", detailRow.ItemID);
                    //    else cmd.Parameters.AddWithValue("p_ItemID", DBNull.Value);

                    //    if (!detailRow.IsItemObtainedPNull()) cmd.Parameters.AddWithValue("p_ItemObtainedP", detailRow.ItemObtainedP);
                    //    else cmd.Parameters.AddWithValue("p_ItemObtainedP", DBNull.Value);

                    //    if (!detailRow.IsItemObtainedNull()) cmd.Parameters.AddWithValue("p_ItemObtained", detailRow.ItemObtained);
                    //    else cmd.Parameters.AddWithValue("p_ItemObtained", DBNull.Value);

                    //    if (!detailRow.IsItemTargetObtainedNull()) cmd.Parameters.AddWithValue("p_ItemTargetObtained", detailRow.ItemTargetObtained);
                    //    else cmd.Parameters.AddWithValue("p_ItemTargetObtained", DBNull.Value);

                    //    if (!detailRow.IsLastEvaluationDateNull()) cmd.Parameters.AddWithValue("p_EvaluationDate", detailRow.LastEvaluationDate);
                    //    else cmd.Parameters.AddWithValue("p_EvaluationDate", DBNull.Value);

                    //    if (!detailRow.IsSenderEmployeeCodeNull()) cmd.Parameters.AddWithValue("p_SenderEmployeeCode", detailRow.SenderEmployeeCode);
                    //    else cmd.Parameters.AddWithValue("p_SenderEmployeeCode", DBNull.Value);

                    //    if (!detailRow.IsReceiverUserIDNull()) cmd.Parameters.AddWithValue("p_ReceiverUserID", detailRow.ReceiverUserID);
                    //    else cmd.Parameters.AddWithValue("p_ReceiverUserID", DBNull.Value);

                    //    if (!detailRow.IsPriorityNull()) cmd.Parameters.AddWithValue("p_Priority", detailRow.Priority);
                    //    else cmd.Parameters.AddWithValue("p_Priority", DBNull.Value);

                    //    if (!detailRow.IsIsKPISubmittedNull()) cmd.Parameters.AddWithValue("p_IsKPISubmitted", detailRow.IsKPISubmitted);
                    //    else cmd.Parameters.AddWithValue("p_IsKPISubmitted", false);

                    //    if (!detailRow.IsIsKPIApprrovedNull()) cmd.Parameters.AddWithValue("p_IsKPIApprroved", detailRow.IsKPIApprroved);
                    //    else cmd.Parameters.AddWithValue("p_IsKPIApprroved", false);

                    //    if (!detailRow.IsScore_ActivityNull()) cmd.Parameters.AddWithValue("p_Score_Activity", detailRow.Score_Activity);
                    //    else cmd.Parameters.AddWithValue("p_Score_Activity", DBNull.Value);

                    //    if (!detailRow.IsCum_TargetObtainedNull()) cmd.Parameters.AddWithValue("p_Cum_TargetObtained", detailRow.Cum_TargetObtained);
                    //    else cmd.Parameters.AddWithValue("p_Cum_TargetObtained", DBNull.Value);
                    //    if (!detailRow.IsCum_ItemObtainedNull()) cmd.Parameters.AddWithValue("p_Cum_ItemObtained", detailRow.Cum_ItemObtained);
                    //    else cmd.Parameters.AddWithValue("p_Cum_ItemObtained", DBNull.Value);
                    //    if (!detailRow.IsCum_DurationNull()) cmd.Parameters.AddWithValue("p_Cum_Duration", detailRow.Cum_Duration);
                    //    else cmd.Parameters.AddWithValue("p_Cum_Duration", DBNull.Value);
                    //    if (!detailRow.IsDurationNull()) cmd.Parameters.AddWithValue("p_Duration", detailRow.Duration);
                    //    else cmd.Parameters.AddWithValue("p_Duration", DBNull.Value);

                    //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    //}

                    //nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PA_KPI_APPROVAL_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();

                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(detailRow.EmployeeName + " - " + detailRow.EmployeeName);
                    else messageDS.ErrorMsg.AddErrorMsgRow(detailRow.EmployeeName + " - " + detailRow.EmployeeName);
                }
            }
            else
            {
                #region If NotifyByMail is Checked....
                URL = stringDS.DataStrings[0].StringValue;
                foreach (PerformanceRecordDS.PA_SCORERow row in prDS.PA_SCORE.Rows)
                {
                    returnMessage = "";
                    string fromEmailAddress = "", emailAddress = "", ccEmailAddress = "", mailingSubject = "";

                    URL += "?pVal=KPI";
                    emailAddress = row.AuthorityEmail.Trim();
                    fromEmailAddress = UtilDL.GetFromEMailAddress();
                    mailingSubject = "KPI Approval";

                    messageBody += "<b>Dispatcher: </b>" + row.ReceiverEmployeeCode + "<br><br>";
                    messageBody += "<b><u><i>Following Employee's KPI has been requested:<i></u></b><br><br>";
                    messageBody += "<b>Event: " + " <u>KPI Score Revision</u>" + "</b><br><br>";

                    messageBody += "<b><u>Employee Details (upon whom the above action has been performed):</u></b><br><br>";
                    messageBody += "<b>Employee Code: </b>" + row.EmployeeCode.Trim() + "<br><br>";
                    messageBody += "<b>Name: </b>" + row.EmployeeName.Trim() + "<br><br>";
                    messageBody += "<b>Department: </b>" + row.DepartmentName.Trim() + "<br><br>";
                    messageBody += "<b>Designation: </b>" + row.DesignationName.Trim() + "<br><br><br>";
                    messageBody += "Click the following link to see KPI Score.";
                    messageBody += "<br>";
                    messageBody += "<a href='" + URL + "'>" + URL + "</a>";

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, "Human Resource Division");
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, "Human Resource Division");
                    }
                    if (returnMessage == "Email successfully sent.")
                    {
                        #region Create
                        foreach (PerformanceRecordDS.PA_SCORERow detailRow in prDS.PA_SCORE.Rows)
                        {
                            if (!detailRow.IsIsRequestFromApprovalNull() && detailRow.IsRequestFromApproval)
                            {
                                long genID = IDGenerator.GetNextGenericPK();
                                if (genID == -1) return UtilDL.GetDBOperationFailed();

                                cmd_Hist.Parameters.AddWithValue("p_Score_HistoryID", genID);

                                if (!detailRow.IsScore_SummaryIDNull()) cmd_Hist.Parameters.AddWithValue("p_Score_SummaryID", detailRow.Score_SummaryID);
                                else cmd_Hist.Parameters.AddWithValue("p_Score_SummaryID", DBNull.Value);

                                if (!detailRow.IsChanged_ScoreNull()) cmd_Hist.Parameters.AddWithValue("p_Changed_Score", detailRow.Changed_Score);
                                else cmd_Hist.Parameters.AddWithValue("p_Changed_Score", DBNull.Value);

                                if (!detailRow.IsSenderEmployeeCodeNull()) cmd_Hist.Parameters.AddWithValue("p_SenderEmployeeCode", detailRow.SenderEmployeeCode);
                                else cmd_Hist.Parameters.AddWithValue("p_SenderEmployeeCode", DBNull.Value);

                                if (!detailRow.IsReceiverUserIDNull()) cmd_Hist.Parameters.AddWithValue("p_ReceiverUserID", detailRow.ReceiverUserID);
                                else cmd_Hist.Parameters.AddWithValue("p_ReceiverUserID", DBNull.Value);

                                if (!detailRow.IsPriorityNull()) cmd_Hist.Parameters.AddWithValue("p_Priority", detailRow.Priority);
                                else cmd_Hist.Parameters.AddWithValue("p_Priority", DBNull.Value);

                                if (!detailRow.IsScore_ActivityNull()) cmd_Hist.Parameters.AddWithValue("p_Score_Activity", detailRow.Score_Activity);
                                else cmd_Hist.Parameters.AddWithValue("p_Score_Activity", DBNull.Value);

                                if (!detailRow.IsIsKPISubmittedNull()) cmd_Hist.Parameters.AddWithValue("p_IsKPISubmitted", detailRow.IsKPISubmitted);
                                else cmd_Hist.Parameters.AddWithValue("p_IsKPISubmitted", false);

                                if (!detailRow.IsIsKPIApprrovedNull()) cmd_Hist.Parameters.AddWithValue("p_IsKPIApprroved", detailRow.IsKPIApprroved);
                                else cmd_Hist.Parameters.AddWithValue("p_IsKPIApprroved", false);

                                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Hist, connDS.DBConnections[0].ConnectionID, ref bError);

                            }
                            if (bError)
                            {
                                ErrorDS.Error err = errDS.Errors.NewError();
                                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                                err.ErrorInfo1 = ActionID.ACTION_PA_KPI_APPROVAL_ADD.ToString();
                                errDS.Errors.AddError(err);
                                errDS.AcceptChanges();
                                return errDS;
                            }
                            cmd.Parameters.Clear();

                            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(detailRow.EmployeeName + " - " + detailRow.EmployeeName);
                            else messageDS.ErrorMsg.AddErrorMsgRow(detailRow.EmployeeName + " - " + detailRow.EmployeeName);
                        }
                        //foreach (PerformanceRecordDS.PA_SCORERow detailRow in prDS.PA_SCORE.Rows)
                        //{
                        //    if (detailRow.IsItemIDNull()) continue;

                        //    cmd.Parameters.AddWithValue("p_KPIScoreID", detailRow.KPIScoreID);

                        //    if (!detailRow.IsItemIDNull()) cmd.Parameters.AddWithValue("p_ItemID", detailRow.ItemID);
                        //    else cmd.Parameters.AddWithValue("p_ItemID", DBNull.Value);

                        //    if (!detailRow.IsItemObtainedPNull()) cmd.Parameters.AddWithValue("p_ItemObtainedP", detailRow.ItemObtainedP);
                        //    else cmd.Parameters.AddWithValue("p_ItemObtainedP", DBNull.Value);

                        //    if (!detailRow.IsItemObtainedNull()) cmd.Parameters.AddWithValue("p_ItemObtained", detailRow.ItemObtained);
                        //    else cmd.Parameters.AddWithValue("p_ItemObtained", DBNull.Value);

                        //    if (!detailRow.IsItemTargetObtainedNull()) cmd.Parameters.AddWithValue("p_ItemTargetObtained", detailRow.ItemTargetObtained);
                        //    else cmd.Parameters.AddWithValue("p_ItemTargetObtained", DBNull.Value);

                        //    if (!detailRow.IsLastEvaluationDateNull()) cmd.Parameters.AddWithValue("p_EvaluationDate", detailRow.LastEvaluationDate);
                        //    else cmd.Parameters.AddWithValue("p_EvaluationDate", DBNull.Value);

                        //    if (!detailRow.IsSenderEmployeeCodeNull()) cmd.Parameters.AddWithValue("p_SenderEmployeeCode", detailRow.SenderEmployeeCode);
                        //    else cmd.Parameters.AddWithValue("p_SenderEmployeeCode", DBNull.Value);

                        //    if (!detailRow.IsReceiverUserIDNull()) cmd.Parameters.AddWithValue("p_ReceiverUserID", detailRow.ReceiverUserID);
                        //    else cmd.Parameters.AddWithValue("p_ReceiverUserID", DBNull.Value);

                        //    if (!detailRow.IsPriorityNull()) cmd.Parameters.AddWithValue("p_Priority", detailRow.Priority);
                        //    else cmd.Parameters.AddWithValue("p_Priority", DBNull.Value);

                        //    if (!detailRow.IsIsKPISubmittedNull()) cmd.Parameters.AddWithValue("p_IsKPISubmitted", detailRow.IsKPISubmitted);
                        //    else cmd.Parameters.AddWithValue("p_IsKPISubmitted", false);

                        //    if (!detailRow.IsIsKPIApprrovedNull()) cmd.Parameters.AddWithValue("p_IsKPIApprroved", detailRow.IsKPIApprroved);
                        //    else cmd.Parameters.AddWithValue("p_IsKPIApprroved", false);

                        //    if (!detailRow.IsScore_ActivityNull()) cmd.Parameters.AddWithValue("p_Score_Activity", detailRow.Score_Activity);
                        //    else cmd.Parameters.AddWithValue("p_Score_Activity", DBNull.Value);

                        //    bError = false;
                        //    nRowAffected = -1;
                        //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                        //    if (bError)
                        //    {
                        //        ErrorDS.Error err = errDS.Errors.NewError();
                        //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        //        err.ErrorInfo1 = ActionID.ACTION_PA_KPI_APPROVAL_ADD.ToString();
                        //        errDS.Errors.AddError(err);
                        //        errDS.AcceptChanges();
                        //        return errDS;
                        //    }
                        //    cmd.Parameters.Clear();

                        //    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(detailRow.EmployeeName + " - " + detailRow.EmployeeName);
                        //    else messageDS.ErrorMsg.AddErrorMsgRow(detailRow.EmployeeName + " - " + detailRow.EmployeeName);
                        //}

                        #endregion
                    }
                    else
                    {
                        messageDS.ErrorMsg.AddErrorMsgRow("Mail sending failed.");
                        messageDS.AcceptChanges();
                        errDS.Clear();
                    }
                }
                #endregion
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _getKPIScoreRecommendation(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetKPIScoringRecommendation");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["ScoreID1"].Value = cmd.Parameters["ScoreID2"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["SenderUserID1"].Value = cmd.Parameters["SenderUserID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.Pa_Kpi_Recommendation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPI_SCORE_RECOMMENDATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _saveKPIScorRecommendation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError;
            int nRowAffected;
            DataStringDS stringDS = new DataStringDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            prDS.Merge(inputDS.Tables[prDS.PA_SCORE.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            #region Delete  Score Recommendation...
            OleDbCommand cmdDel = new OleDbCommand();
            cmdDel.CommandText = "PRO_KPI_PA_RECOMMENDATION_DEL";
            cmdDel.CommandType = CommandType.StoredProcedure;

            cmdDel.Parameters.AddWithValue("p_ScoreID", stringDS.DataStrings[0].StringValue);
            cmdDel.Parameters.AddWithValue("p_SenderUserID", stringDS.DataStrings[1].StringValue);
            bError = false; nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_SCORE_RECOMMENDATION_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region To Create Score Recommendation...
            OleDbCommand cmdScore = new OleDbCommand();
            cmdScore.CommandText = "PRO_KPI_RECOMMENDATION_SAVE";
            cmdScore.CommandType = CommandType.StoredProcedure;

            if (cmdScore == null) return UtilDL.GetCommandNotFound();

            foreach (PerformanceRecordDS.PA_SCORERow row in prDS.PA_SCORE.Rows)
            {
                if (!row.IsScoreIDNull()) cmdScore.Parameters.AddWithValue("p_ScoreID", row.ScoreID);
                else cmdScore.Parameters.AddWithValue("p_ScoreID", DBNull.Value);

                if (!row.IsSenderUserIDNull()) cmdScore.Parameters.AddWithValue("p_SenderUserID", row.SenderUserID);
                else cmdScore.Parameters.AddWithValue("p_SenderUserID", DBNull.Value);

                if (!row.IsRecommendationIDNull()) cmdScore.Parameters.AddWithValue("p_RcommendationID", row.RecommendationID);
                else cmdScore.Parameters.AddWithValue("p_RcommendationID", DBNull.Value);

                if (!row.IsKPIRemarksNull()) cmdScore.Parameters.AddWithValue("p_Remarks", row.KPIRemarks);
                else cmdScore.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                if (!row.IsRemarks_NativeLangNull()) cmdScore.Parameters.AddWithValue("p_Remarks_NativeLang", row.Remarks_NativeLang);
                else cmdScore.Parameters.AddWithValue("p_Remarks_NativeLang", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdScore, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdScore.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_SCORE_RECOMMENDATION_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getKPIAssessmentPeriod(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetKPIAssessmentPeriod");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PA_SCORE.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_ASSESSMENT_PERIOD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _getKPIScoringAuthority(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetKPIScoringAuthority");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.ScoringAuthority.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_KPI_SCORING_AUTHORITY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prDS.AcceptChanges();

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _saveKPIScoringAuthority(DataSet inputDS)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            OleDbCommand cmd;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            prDS.Merge(inputDS.Tables[prDS.ScoringAuthority.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            #region Delete Old Data
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_SCORE_AUTHORITY_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_KPI_SCORIMG_AUTHORITY_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Save new data
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_SCORE_AUTHORITY_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (PerformanceRecordDS.ScoringAuthorityRow row in prDS.ScoringAuthority.Rows)
            {
                cmd.Parameters.AddWithValue("p_Score_Auth_ID", row.Score_Auth_ID);
                cmd.Parameters.AddWithValue("p_Authority", row.Authority);
                cmd.Parameters.AddWithValue("p_Emp_WeightSetup", row.Emp_WeightSetup);
                cmd.Parameters.AddWithValue("p_Emp_TargetSetup", row.Emp_TargetSetup);
                cmd.Parameters.AddWithValue("p_IsChecker", row.IsChecker);
                if (!row.IsEmployeeCodeNull()) cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                else cmd.Parameters.AddWithValue("p_EmployeeCode", DBNull.Value);
                if (!row.IsLiabilityIDNull()) cmd.Parameters.AddWithValue("p_LiabilityID", row.LiabilityID);
                else cmd.Parameters.AddWithValue("p_LiabilityID", DBNull.Value);
                cmd.Parameters.AddWithValue("p_Auth_Serial", row.Auth_Serial);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_SCORIMG_AUTHORITY_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _checkKPI_Score(DataSet inputDS)
        {
            bool bError;
            int nRowAffected;
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DataBoolDS boolDS = new DataBoolDS();
            DataStringDS stringDS = new DataStringDS();
            DataSet returnDS = new DataSet();
            string messageBody = "", returnMessage = "", URL = "";

            #region Get Email Information from UtilDL
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_SCORE_CHECK";
            cmd.CommandType = CommandType.StoredProcedure;

            prDS.Merge(inputDS.Tables[prDS.PA_SCORE.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Check KPI Score
            bError = false;
            nRowAffected = -1;

            foreach (PerformanceRecordDS.PA_SCORERow row in prDS.PA_SCORE.Rows)
            {
                if (!row.IsScore_SummaryIDNull()) cmd.Parameters.AddWithValue("p_Score_SummaryID", row.Score_SummaryID);
                else cmd.Parameters.AddWithValue("p_Score_SummaryID", DBNull.Value);
                if (!row.IsChecked_ByNull()) cmd.Parameters.AddWithValue("p_Checked_By", row.Checked_By);
                else cmd.Parameters.AddWithValue("p_Checked_By", DBNull.Value);
                if (!row.IsCheckerNull()) cmd.Parameters.AddWithValue("p_Checker", row.Checker);
                else cmd.Parameters.AddWithValue("p_Checker", DBNull.Value);

                if (!boolDS.DataBools[0].BoolValue) //If NotifyByMail is  not Checked
                {
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                }
                else
                {
                    #region Operation after mail send
                    returnMessage = "";
                    string fromEmailAddress = "", emailAddress = "", ccEmailAddress = "", mailingSubject = "";

                    URL += "?pVal=KPI";
                    emailAddress = row.AuthorityEmail.Trim();
                    fromEmailAddress = UtilDL.GetFromEMailAddress();
                    mailingSubject = "KPI Score Check";

                    messageBody += "<b>Dispatcher: </b>" + row.ReceiverEmployeeCode + "<br><br>";
                    messageBody += "<b><u><i>Following Employee's KPI has been checked:<i></u></b><br><br>";
                    messageBody += "<b>Event: " + " <u>KPI Score Check</u>" + "</b><br><br>";

                    messageBody += "<b><u>Employee Details (upon whom the above action has been performed):</u></b><br><br>";
                    messageBody += "<b>Employee Code: </b>" + row.EmployeeCode.Trim() + "<br><br>";
                    messageBody += "<b>Name: </b>" + row.EmployeeName.Trim() + "<br><br>";
                    messageBody += "<b>Department: </b>" + row.DepartmentName.Trim() + "<br><br>";
                    messageBody += "<b>Designation: </b>" + row.DesignationName.Trim() + "<br><br><br>";
                    messageBody += "Click the following link to see KPI Score.";
                    messageBody += "<br>";
                    messageBody += "<a href='" + URL + "'>" + URL + "</a>";

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, "Human Resource Division");
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, "Human Resource Division");
                    }

                    if (returnMessage == "Email successfully sent.")
                    {
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    }
                    else
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PA_KPI_CHECK.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion
                }
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PA_KPI_CHECK.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            return returnDS;
        }

        private DataSet _createKPI_PromotionPolicy(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();

            OleDbCommand cmd_Policy_Del = new OleDbCommand();
            cmd_Policy_Del.CommandText = "PRO_KPI_PROMOTION_POLICY_DEL";
            cmd_Policy_Del.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Policy = new OleDbCommand();
            cmd_Policy.CommandText = "PRO_KPI_PROMOTION_POLICY_ADD";
            cmd_Policy.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Policy_Grade = new OleDbCommand();
            cmd_Policy_Grade.CommandText = "PRO_KPI_PROMOTION_POLICY_GRADE";
            cmd_Policy_Grade.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Policy_Rank = new OleDbCommand();
            cmd_Policy_Rank.CommandText = "PRO_KPI_PROMOTION_POLICY_RANK";
            cmd_Policy_Rank.CommandType = CommandType.StoredProcedure;

            if (cmd_Policy_Del == null || cmd_Policy == null || cmd_Policy_Grade == null || cmd_Policy_Rank == null) return UtilDL.GetCommandNotFound();

            prDS.Merge(inputDS.Tables[prDS.PromotionPolicy.TableName], false, MissingSchemaAction.Error);
            prDS.Merge(inputDS.Tables[prDS.PromotionPolicy_Grade.TableName], false, MissingSchemaAction.Error);
            prDS.Merge(inputDS.Tables[prDS.PromotionPolicy_Rank.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Delete  policy Record
            if (!prDS.PromotionPolicy[0].IsPolicyGroupIDNull())
            {
                cmd_Policy_Del.Parameters.AddWithValue("p_PolicyGroup", prDS.PromotionPolicy[0].PolicyGroupID);

                bool bError_Del = false;
                int nRowAffected_Del = -1;
                nRowAffected_Del = ADOController.Instance.ExecuteNonQuery(cmd_Policy_Del, connDS.DBConnections[0].ConnectionID, ref bError_Del);
                cmd_Policy_Del.Parameters.Clear();

                if (bError_Del == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_PROMOTION_POLICY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
            }
            #endregion

            DataView dv = new DataView(prDS.PromotionPolicy);
            dv.Sort = "ServiceLength ASC";
            prDS.AcceptChanges();
            DataTable dt = dv.ToTable(prDS.PromotionPolicy.TableName);
            PerformanceRecordDS prDS_Temp = new PerformanceRecordDS();
            prDS_Temp.PromotionPolicy.Merge(dt);

            long genPK_Policy = IDGenerator.GetNextGenericPK();
            if (genPK_Policy == -1) return UtilDL.GetDBOperationFailed();

            foreach (PerformanceRecordDS.PromotionPolicyRow row in prDS_Temp.PromotionPolicy.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();


                if (!row.IsPromotionPolicyIDNull()) cmd_Policy.Parameters.AddWithValue("p_PromotionPolicyID", row.PromotionPolicyID);
                else cmd_Policy.Parameters.AddWithValue("p_PromotionPolicyID", genPK);

                if (!row.IsPolicyGroupIDNull()) cmd_Policy.Parameters.AddWithValue("p_PolicyGroup", row.PolicyGroupID);
                else cmd_Policy.Parameters.AddWithValue("p_PolicyGroup", genPK_Policy);

                if (!row.IsServiceLengthNull()) cmd_Policy.Parameters.AddWithValue("p_ServiceLength", row.ServiceLength);
                else cmd_Policy.Parameters.AddWithValue("p_ServiceLength", DBNull.Value);

                if (!row.IsFunctionNameNull()) cmd_Policy.Parameters.AddWithValue("p_FunctionName", row.FunctionName);
                else cmd_Policy.Parameters.AddWithValue("p_FunctionName", DBNull.Value);

                if (!row.IsSiteCategoryIDNull()) cmd_Policy.Parameters.AddWithValue("p_SiteCategoryID", row.SiteCategoryID);
                else cmd_Policy.Parameters.AddWithValue("p_SiteCategoryID", DBNull.Value);

                if (!row.IsServiceLength_FunctionNull()) cmd_Policy.Parameters.AddWithValue("p_ServiceLength_Function", row.ServiceLength_Function);
                else cmd_Policy.Parameters.AddWithValue("p_ServiceLength_Function", DBNull.Value);

                if (!row.IsLiability_Nature_IDNull()) cmd_Policy.Parameters.AddWithValue("p_LiabilityNatureID", row.Liability_Nature_ID);
                else cmd_Policy.Parameters.AddWithValue("p_LiabilityNatureID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Policy, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Policy.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_PROMOTION_POLICY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                foreach (PerformanceRecordDS.PromotionPolicy_GradeRow rowGrade in prDS.PromotionPolicy_Grade.Rows)
                {
                    if (!row.IsPromotionPolicyIDNull()) cmd_Policy_Grade.Parameters.AddWithValue("p_PromotionPolicyID", row.PromotionPolicyID);
                    else cmd_Policy_Grade.Parameters.AddWithValue("p_PromotionPolicyID", genPK);
                    cmd_Policy_Grade.Parameters.AddWithValue("p_GradeID", rowGrade.GradeID);

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Policy_Grade, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Policy_Grade.Parameters.Clear();

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_KPI_PROMOTION_POLICY_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }

                PerformanceRecordDS.PromotionPolicy_RankRow[] foundRows = (PerformanceRecordDS.PromotionPolicy_RankRow[])prDS.PromotionPolicy_Rank.Select("ServiceLength =" + row.ServiceLength);
                PerformanceRecordDS rankDS = new PerformanceRecordDS();
                if (foundRows.Length > 0)
                {
                    foreach (PerformanceRecordDS.PromotionPolicy_RankRow rowRank in foundRows)
                    {
                        if (!row.IsPromotionPolicyIDNull()) cmd_Policy_Rank.Parameters.AddWithValue("p_PromotionPolicyID", row.PromotionPolicyID);
                        else cmd_Policy_Rank.Parameters.AddWithValue("p_PromotionPolicyID", genPK);
                        if (!rowRank.IsKPI_PAR_RankIDNull()) cmd_Policy_Rank.Parameters.AddWithValue("p_KPIRankingID", rowRank.KPI_PAR_RankID);
                        else cmd_Policy_Rank.Parameters.AddWithValue("p_KPIRankingID", DBNull.Value);
                        if (!rowRank.IsKPI_PAR_PointNull()) cmd_Policy_Rank.Parameters.AddWithValue("p_PAR_Point", rowRank.KPI_PAR_Point);
                        else cmd_Policy_Rank.Parameters.AddWithValue("p_PAR_Point", DBNull.Value);

                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Policy_Rank, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd_Policy_Rank.Parameters.Clear();

                        if (bError == true)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_KPI_PROMOTION_POLICY_ADD.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getKPIPromotionPolicy(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            if (stringDS.DataStrings[0].StringValue == "GetKPI_PromotionPolicy_All")
            {
                cmd = DBCommandProvider.GetDBCommand("GetKPI_PromotionPolicyAll");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PromotionPolicy.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_KPI_PROMOTION_POLICY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                prDS.AcceptChanges();

            }
            else if (stringDS.DataStrings[0].StringValue == "GetKPI_PromotionPolicy_ByID")
            {
                bool bError = false;
                int nRowAffected = -1;

                cmd = DBCommandProvider.GetDBCommand("GetKPI_PromotionPolicy_GradeByID");
                cmd.Parameters["PolicyGroup"].Value = stringDS.DataStrings[1].StringValue;

                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;

                nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PromotionPolicy_Grade.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                cmd.Dispose();
                adapter.Dispose();

                cmd = DBCommandProvider.GetDBCommand("GetKPI_PromotionPolicy_RankByID");
                cmd.Parameters["PolicyGroup"].Value = stringDS.DataStrings[1].StringValue;

                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;

                nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PromotionPolicy_Rank.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_KPI_PROMOTION_POLICY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            else if (stringDS.DataStrings[0].StringValue == "GetKPI_PromotionPolicy_GradeAll")
            {
                cmd = DBCommandProvider.GetDBCommand("GetKPI_PromotionPolicy_GradeAll");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, prDS, prDS.PromotionPolicy_Grade.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_GET_KPI_PROMOTION_POLICY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                prDS.AcceptChanges();
            }

            returnDS.Merge(prDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _deleteKPIPromotionPolicy(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            PerformanceRecordDS prDS = new PerformanceRecordDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            prDS.Merge(inputDS.Tables[prDS.PromotionPolicy.TableName], false, MissingSchemaAction.Error);
            prDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_KPI_PROMOTION_POLICY_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            foreach (PerformanceRecordDS.PromotionPolicyRow row in prDS.PromotionPolicy.Rows)
            {
                cmd.Parameters.AddWithValue("p_PolicyGroup", row.PolicyGroupID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_KPI_PROMOTION_POLICY_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow("Selected Policy");
                else messageDS.ErrorMsg.AddErrorMsgRow("Selected Policy");
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _getKPIRankList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLocationList");//By Jarif(28 Sep 11)
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetKPIRankList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            rptPerformanceDS perDS = new rptPerformanceDS();
            //LocationDS locDS = new LocationDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, perDS, perDS.KpiRank.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_KPIRank_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            perDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(perDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _getAssesmentPeriod(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAssesmentPeriod");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            rptPerformanceDS perDS = new rptPerformanceDS();


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, perDS, perDS.PA_ASSESSMENT_PERIOD.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_Assesment_Period.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            perDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(perDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _getYearWisePerformance_Score(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            // DataBoolDS boolDS = new DataBoolDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (cmd == null) return UtilDL.GetCommandNotFound();

            if (stringDS.DataStrings[15].StringValue == "ScoreFilterBy_SeparationDate")
            {
                cmd = DBCommandProvider.GetDBCommand("ScoreFilterBy_SeparationDate");
            }

            else cmd = DBCommandProvider.GetDBCommand("GetYearWise_Performance_Score");

            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue.ToUpper() + "%";
            cmd.Parameters["YearFrom"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
            cmd.Parameters["YearTo"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
            cmd.Parameters["SiteCode1"].Value = cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["ZoneCode1"].Value = cmd.Parameters["ZoneCode2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            cmd.Parameters["LocationCode1"].Value = cmd.Parameters["LocationCode2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
            cmd.Parameters["DivisionCode1"].Value = cmd.Parameters["DivisionCode2"].Value = stringDS.DataStrings[7].StringValue;
            cmd.Parameters["EmployeeType1"].Value = cmd.Parameters["EmployeeType2"].Value = Convert.ToInt32(stringDS.DataStrings[8].StringValue);
            cmd.Parameters["DepartmentCode1"].Value = cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[9].StringValue;
            cmd.Parameters["FunctionCode1"].Value = cmd.Parameters["FunctionCode2"].Value = stringDS.DataStrings[10].StringValue;
            cmd.Parameters["LiabilityCode1"].Value = cmd.Parameters["LiabilityCode2"].Value = stringDS.DataStrings[11].StringValue;
            cmd.Parameters["ScoreLimitFrom"].Value = Convert.ToInt32(stringDS.DataStrings[12].StringValue);
            cmd.Parameters["ScoreLimitTo"].Value = Convert.ToInt32(stringDS.DataStrings[13].StringValue);
            cmd.Parameters["PerformanceRank1"].Value = cmd.Parameters["PerformanceRank2"].Value = Convert.ToInt32(stringDS.DataStrings[14].StringValue);

            if (stringDS.DataStrings[15].StringValue == "ScoreFilterBy_SeparationDate")
            {
                cmd.Parameters["SeparationFromDate"].Value = Convert.ToDateTime(stringDS.DataStrings[16].StringValue);
                cmd.Parameters["SeparationToDate"].Value = Convert.ToDateTime(stringDS.DataStrings[17].StringValue);
            }

            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //DataSet DataDS = new DataSet();
            //DataDS.Tables.Add("RecordList");
            rptPerformanceDS perDS = new rptPerformanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, perDS, perDS.PerformanceRecord.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_YEAR_WISE_PERFORMANCE_SCORE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            perDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(perDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}
