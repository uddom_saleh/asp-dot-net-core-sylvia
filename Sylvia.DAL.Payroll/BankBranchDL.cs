/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: March 27, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
  /// <summary>
  /// Summary description for DepartmentDL.
  /// </summary>
  public class BankBranchDL
  {
    public BankBranchDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }
    
    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {
        case ActionID.ACTION_BANKBRANCH_ADD:
          return _createBranch( inputDS );    
        case ActionID.ACTION_BANKBRANCH_UPD:
          return this._updateBranch( inputDS );
      case ActionID.NA_ACTION_GET_BANKBRANCH_LIST:
          return _getBranchList(inputDS);
      case ActionID.NA_ACTION_GET_BANKBRANCH_BY_BANKCODE:
          return _getBranchListByBankCode(inputDS);
      case ActionID.NA_ACTION_GET_BANKBRANCH:
          return _getBranch(inputDS);
        case ActionID.ACTION_BANKBRANCH_DEL:
          return this._deleteBranch( inputDS );
        case ActionID.ACTION_DOES_BANKBRANCH_EXIST:
          return this._doesBankBranchExist( inputDS );
       
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
    private DataSet _createBranch(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      BankBranchDS branchDS = new BankBranchDS();
      DBConnectionDS connDS = new  DBConnectionDS();

      //      long genPK = IDGenerator.GetNextGenericPK();
      //      Debug.Assert( genPK != -1);

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateBranch");
      if( cmd == null )
      {
      return UtilDL.GetCommandNotFound();
      
      }
      
      branchDS.Merge( inputDS.Tables[ branchDS.Branches.TableName ], false, MissingSchemaAction.Error );
      branchDS.AcceptChanges();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      
      foreach(BankBranchDS.Branch branch in branchDS.Branches)
      {
        long genPK = IDGenerator.GetNextGenericPK();
        if( genPK == -1)
        {
          return UtilDL.GetDBOperationFailed();
        
        }

      
        cmd.Parameters["BranchId"].Value = genPK; 
        if(branch.IsBranchCodeNull()==false)
        {
          cmd.Parameters["BranchCode"].Value = branch.BranchCode;
        }
        else
        {
          cmd.Parameters["BranchCode"].Value = null;
        
        }

        if(branch.IsBranchNameNull()==false)
        {
          cmd.Parameters["BranchName"].Value = branch.BranchName;
        }
        else
        {
          cmd.Parameters["BranchName"].Value = null;
        
        }
        //cmd.Parameters["BranchName"].Value = (object) branchDS.Branches[0].BranchName;
        if(branch.IsBranchAddressNull()==false)
        {
          cmd.Parameters["Address"].Value = branch.BranchAddress;
        }
        else
        {
          cmd.Parameters["Address"].Value = null;
        
        }
        //cmd.Parameters["Address"].Value = (object) branchDS.Branches[0].BranchAddress;
        if(branch.IsBranchTelephoneNull()==false)
        {
          cmd.Parameters["Telephone"].Value = branch.BranchTelephone;
        }
        else
        {
          cmd.Parameters["Telephone"].Value = null;
        
        }
        //cmd.Parameters["Telephone"].Value = (object) branchDS.Branches[0].BranchTelephone;
        if(branch.IsBankCodeNull()==false)
        {
          cmd.Parameters["BankCode"].Value = branch.BankCode;
        }
        else
        {
          cmd.Parameters["BankCode"].Value = null;
        
        }
        if (branch.IsRoutingNoNull() == false)
        {
            cmd.Parameters["RoutingNo"].Value = branch.RoutingNo;
        }
        else
        {
            cmd.Parameters["RoutingNo"].Value = null;

        }
        if (branch.IsAccountNoNull() == false)
        {
            cmd.Parameters["AccountNo"].Value = branch.AccountNo;
        }
        else
        {
            cmd.Parameters["AccountNo"].Value = null;

        }
        //cmd.Parameters["BankCode"].Value = (object) branchDS.Branches[0].BankID;
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_BANKBRANCH_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _updateBranch(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      BankBranchDS branchDS = new BankBranchDS();
      branchDS.Merge( inputDS.Tables[ branchDS.Branches.TableName ], false, MissingSchemaAction.Error );   
      branchDS.AcceptChanges();


      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateBranch");
      if( cmd == null )
        {
          return UtilDL.GetCommandNotFound();
        
        }
      
      foreach(BankBranchDS.Branch branch in branchDS.Branches)
      {
        if(branch.IsBranchCodeNull()==false)
        {
          cmd.Parameters["BranchCode"].Value = branch.BranchCode;
        }
        else
        {
          cmd.Parameters["BranchCode"].Value = null;
        
        }

        if(branch.IsBranchNameNull()==false)
        {
          cmd.Parameters["BranchName"].Value = branch.BranchName;
        }
        else
        {
          cmd.Parameters["BranchName"].Value = null;
        
        }
        //cmd.Parameters["BranchName"].Value = (object) branchDS.Branches[0].BranchName;
        if(branch.IsBranchAddressNull()==false)
        {
          cmd.Parameters["Address"].Value = branch.BranchAddress;
        }
        else
        {
          cmd.Parameters["Address"].Value = null;
        
        }
        //cmd.Parameters["Address"].Value = (object) branchDS.Branches[0].BranchAddress;
        if(branch.IsBranchTelephoneNull()==false)
        {
          cmd.Parameters["Telephone"].Value = branch.BranchTelephone;
        }
        else
        {
          cmd.Parameters["Telephone"].Value = null;
        
        }
        //cmd.Parameters["Telephone"].Value = (object) branchDS.Branches[0].BranchTelephone;
        
        //if(branch.IsBankCodeNull()==false)
        //{
        //  cmd.Parameters["BankCode"].Value = branch.BankCode;
        //}
        //else
        //{
        //  cmd.Parameters["BankCode"].Value = null;
        //}

        if (branch.IsBankCodeNull() == false)
        {
          cmd.Parameters["BankId"].Value = UtilDL.GetBankId(connDS,branch.BankCode);
        }
        else
        {
          cmd.Parameters["BankId"].Value = null;
        }

        if (branch.IsRoutingNoNull() == false)
        {
            cmd.Parameters["RoutingNo"].Value = branch.RoutingNo;
        }
        else
        {
            cmd.Parameters["RoutingNo"].Value = null;
        }
        if (branch.IsAccountNoNull() == false)
        {
            cmd.Parameters["AccountNo"].Value = branch.AccountNo;
        }
        else
        {
            cmd.Parameters["AccountNo"].Value = null;

        }
        //cmd.Parameters["BankCode"].Value = (object) branchDS.Branches[0].BankID;
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_BANKBRANCH_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }

      
      return errDS;  // return empty ErrorDS
    
    }

    private DataSet _deleteBranch(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

    
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteBranch");
      if( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }
      cmd.Parameters["BranchCode"].Value = (object) stringDS.DataStrings[0].StringValue;
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }

      return errDS;  // return empty ErrorDS
    }
    private DataSet _getBranch(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataStringDS stringDS = new DataStringDS();
      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();
      string scode= stringDS.DataStrings[0].StringValue;
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBranch");
      if( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }
      
      cmd.Parameters["BranchCode"].Value = scode;//(object) stringDS.DataStrings[0].StringValue;
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      BankBranchPO branchPO = new BankBranchPO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter, 
        branchPO, 
        branchPO.BankBranches.TableName, 
        connDS.DBConnections[0].ConnectionID, 
        ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_GET_BANKBRANCH.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }

      branchPO.AcceptChanges();
      BankBranchDS branchDS = new BankBranchDS();
      if ( branchPO.BankBranches.Count > 0 )
      {
        foreach ( BankBranchPO.BankBranch poBranch in branchPO.BankBranches)
        {
          BankBranchDS.Branch branch = branchDS.Branches.NewBranch();
          if (poBranch.IsBranchCodeNull() == false )
          {
            branch.BranchCode = poBranch.BranchCode;
          }
          if (poBranch.IsBranchNameNull() == false)
          {
            branch.BranchName = poBranch.BranchName;
          }
          if (poBranch.IsBranchAddressNull() == false)
          {
            branch.BranchAddress = poBranch.BranchAddress;
          }
          if (poBranch.IsBranchTelephoneNull() == false)
          {
            branch.BranchTelephone = poBranch.BranchTelephone;
          }
          if (poBranch.IsAccountNoNull() == false)
          {
              branch.AccountNo = poBranch.AccountNo;
          }
         /* if (poBranch.IsBankCodeNull() == false)
          {
            branch.BankCode = poBranch.BankCode;
          }
          */
          branchDS.Branches.AddBranch( branch );
          branchDS.AcceptChanges();
        }

        
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( branchDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;

    }

    private DataSet _getBranchList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
     
      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBranchList");
      if (cmd == null)
      {

        Util.LogInfo("Command is null.");
      }
    
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      BankBranchPO branchPO = new BankBranchPO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter, 
        branchPO, 
        branchPO.BankBranches.TableName, 
        connDS.DBConnections[0].ConnectionID, 
        ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_GET_BANKBRANCH_LIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }
      branchPO.AcceptChanges();
      BankBranchDS branchDS = new BankBranchDS();
      if ( branchPO.BankBranches.Count > 0 )
      {
        foreach ( BankBranchPO.BankBranch poBranch in branchPO.BankBranches)
        {
          BankBranchDS.Branch branch = branchDS.Branches.NewBranch();
         
          if (poBranch.IsBranchCodeNull() == false)
          {
            branch.BranchCode = poBranch.BranchCode;
          }
          if (poBranch.IsBranchNameNull() == false)
          {
            branch.BranchName = poBranch.BranchName;
          }
          if (poBranch.IsBranchAddressNull() == false)
          {
            branch.BranchAddress = poBranch.BranchAddress;
          }
          if (poBranch.IsBranchTelephoneNull() == false)
          {
            branch.BranchTelephone = poBranch.BranchTelephone;
          }
          if (poBranch.IsBankNameNull() == false)
          {
            branch.BankName = poBranch.BankName;
          }
          if (poBranch.IsBankCodeNull() == false)
          {
            branch.BankCode = poBranch.BankCode;
          }
          if (poBranch.IsRoutingNoNull() == false)
          {
              branch.RoutingNo = poBranch.RoutingNo;
          }
          if (poBranch.IsAccountNoNull() == false)
          {
              branch.AccountNo = poBranch.AccountNo;
          }
          branchDS.Branches.AddBranch( branch );
          branchDS.AcceptChanges();
        }

        
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( branchDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    
    }

    private DataSet _getBranchListByBankCode(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();

        DBConnectionDS connDS = new DBConnectionDS();
        DataStringDS stringDS = new DataStringDS();

        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();

        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();
        string scode = stringDS.DataStrings[0].StringValue;

        OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBranchListByBankCode");
        if (cmd == null)
        {

            Util.LogInfo("Command is null.");
        }

        OleDbDataAdapter adapter = new OleDbDataAdapter();
        adapter.SelectCommand = cmd;

        cmd.Parameters["Code"].Value = scode;

        BankBranchPO branchPO = new BankBranchPO();

        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.Fill(adapter,
          branchPO,
          branchPO.BankBranches.TableName,
          connDS.DBConnections[0].ConnectionID,
          ref bError);
        if (bError == true)
        {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            err.ErrorInfo1 = ActionID.NA_ACTION_GET_BANKBRANCH_BY_BANKCODE.ToString();
            errDS.Errors.AddError(err);
            errDS.AcceptChanges();
            return errDS;

        }
        branchPO.AcceptChanges();
        BankBranchDS branchDS = new BankBranchDS();
        if (branchPO.BankBranches.Count > 0)
        {
            foreach (BankBranchPO.BankBranch poBranch in branchPO.BankBranches)
            {
                BankBranchDS.Branch branch = branchDS.Branches.NewBranch();

                if (poBranch.IsBranchCodeNull() == false)
                {
                    branch.BranchCode = poBranch.BranchCode;
                }
                if (poBranch.IsBranchNameNull() == false)
                {
                    branch.BranchName = poBranch.BranchName;
                }
                if (poBranch.IsBranchAddressNull() == false)
                {
                    branch.BranchAddress = poBranch.BranchAddress;
                }
                if (poBranch.IsBranchTelephoneNull() == false)
                {
                    branch.BranchTelephone = poBranch.BranchTelephone;
                }
                if (poBranch.IsBankNameNull() == false)
                {
                    branch.BankName = poBranch.BankName;
                }
                if (poBranch.IsBankCodeNull() == false)
                {
                    branch.BankCode = poBranch.BankCode;
                }
                if (poBranch.IsRoutingNoNull() == false)
                {
                    branch.RoutingNo = poBranch.RoutingNo;
                }
                if (poBranch.IsAccountNoNull() == false)
                {
                    branch.AccountNo = poBranch.AccountNo;
                }
                branchDS.Branches.AddBranch(branch);
                branchDS.AcceptChanges();
            }


        }

        // now create the packet
        errDS.Clear();
        errDS.AcceptChanges();

        DataSet returnDS = new DataSet();

        returnDS.Merge(branchDS);
        returnDS.Merge(errDS);
        returnDS.AcceptChanges();
        return returnDS;

    }
  
    private DataSet _doesBankBranchExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesBankBranchExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
        cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    } 

  }
}
