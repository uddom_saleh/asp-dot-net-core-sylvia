using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class ShiftingDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_SHIFTING_ADD:
                    return this._createShifting(inputDS);
                case ActionID.ACTION_SHIFTING_UPD:
                    return this._updateShifting(inputDS);
                case ActionID.ACTION_SHIFTING_DEL:
                    return this._deleteShifting(inputDS);
                case ActionID.NA_ACTION_GET_SHIFTING_LIST:
                    return this._getShiftingList(inputDS);
                case ActionID.NA_ACTION_GET_SHIFTING_LIST_ALL:
                    return this._getShiftingListAll(inputDS);

                case ActionID.NA_ACTION_GET_SHIFTING_LIST_ALL_DROPDOWN:
                    return this._getShiftingListAllDropdownlist(inputDS);

                case ActionID.NA_ACTION_GET_SHIFTING_LIST_GRID:
                    return this._getShiftingListGrid(inputDS);
                case ActionID.NA_ACTION_GET_SHIFTING_LIST_ALL_GRID:
                    return this._getShiftingListAllGrid(inputDS);

                case ActionID.NA_ACTION_GET_ACCOMOTDATION_FILTER_BY_ALL:
                    return this._getAccommodationFilterByAll(inputDS);

                case ActionID.ACTION_ACCOMMODATION_PLANNING_SAVE:
                    return this._createAccommodationPlanning(inputDS);

                case ActionID.ACTION_OVERTIME_CATEGORY_UPDATE:
                    return this._updateOvertimeCategory(inputDS);  

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }


        private DataSet _createShifting(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateShifting");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            ShiftingDS ShfDS = new ShiftingDS();
            ShfDS.Merge(inputDS.Tables[ShfDS.DefineShifting.TableName], false, MissingSchemaAction.Error);
            ShfDS.AcceptChanges();

            foreach (ShiftingDS.DefineShiftingRow ShfDSRow in ShfDS.DefineShifting.Rows)
            {
                //SHITING ID
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["ShiftingID"].Value = (object)genPK;


                //SHIFTING CODE
                if (ShfDSRow.IsShiftingCodeNull() == false)
                {
                    cmd.Parameters["ShiftingCode"].Value = (object)ShfDSRow.ShiftingCode;
                }
                else
                {
                    cmd.Parameters["ShiftingCode"].Value = null;
                }


                //SHIFTING NAME

                if (ShfDSRow.IsShiftingNameNull() == false)
                {
                    cmd.Parameters["ShiftingName"].Value = (object)ShfDSRow.ShiftingName;
                }
                else
                {
                    cmd.Parameters["ShiftingName"].Value = null;
                }


                //LOGIN TIME HOUR

                if (ShfDSRow.IsLoginTime_HourNull() == false)
                {
                    cmd.Parameters["LoginTime_Hour"].Value = (object)ShfDSRow.LoginTime_Hour;
                }
                else
                {
                    cmd.Parameters["LoginTime_Hour"].Value = null;
                }


                //LOGIN TIME MIN

                if (ShfDSRow.IsLoginTime_MinuteNull() == false)
                {
                    cmd.Parameters["LoginTime_Minute"].Value = (object)ShfDSRow.LoginTime_Minute;
                }
                else
                {
                    cmd.Parameters["LoginTime_Minute"].Value = null;
                }


                //LOGIN TIME HOUR HH
                if (ShfDSRow.IsLoginTime_Hour_HHNull() == false)
                {
                    cmd.Parameters["LoginTime_Hour_HH"].Value = (object)ShfDSRow.LoginTime_Hour_HH;
                }
                else
                {
                    cmd.Parameters["LoginTime_Hour_HH"].Value = null;
                }


                //LOGIN TIME MIN HH
                if (ShfDSRow.IsLoginTime_Minute_HHNull() == false)
                {
                    cmd.Parameters["LoginTime_Minute_HH"].Value = (object)ShfDSRow.LoginTime_Minute_HH;
                }
                else
                {
                    cmd.Parameters["LoginTime_Minute_HH"].Value = null;
                }
                /////////////////////////////////
                //LOGOUT TIME HOUR 
                if (ShfDSRow.IsLogoutTime_HourNull() == false)
                {
                    cmd.Parameters["LogoutTime_Hour"].Value = (object)ShfDSRow.LogoutTime_Hour;
                }
                else
                {
                    cmd.Parameters["LogoutTime_Hour"].Value = null;
                }
                //LOGOUT TIME MIN
                if (ShfDSRow.IsLogoutTime_MinuteNull() == false)
                {
                    cmd.Parameters["LogoutTime_Minute"].Value = (object)ShfDSRow.LogoutTime_Minute;
                }
                else
                {
                    cmd.Parameters["LogoutTime_Minute"].Value = null;
                }
                //LOGOUT TIME HOUR HH
                if (ShfDSRow.IsLogoutTime_HourNull() == false)
                {
                    cmd.Parameters["LogoutTime_Hour_HH"].Value = (object)ShfDSRow.LogoutTime_Hour_HH;
                }
                else
                {
                    cmd.Parameters["LogoutTime_Hour_HH"].Value = null;
                }
                //LOGOUT TIME MIN HH
                if (ShfDSRow.IsLogoutTime_MinuteNull() == false)
                {
                    cmd.Parameters["LogoutTime_Minute_HH"].Value = (object)ShfDSRow.LogoutTime_Minute_HH;
                }
                else
                {
                    cmd.Parameters["LogoutTime_Minute_HH"].Value = null;
                }
                //FLEXIBLE TIME HOUR
                if (ShfDSRow.IsFlexibleTime_HourNull() == false)
                {
                    cmd.Parameters["FlexibleTime_Hour"].Value = (object)ShfDSRow.FlexibleTime_Hour;
                }
                else
                {
                    cmd.Parameters["FlexibleTime_Hour"].Value = null;
                }
                //FLEXIBLE TIME MIN
                if (ShfDSRow.IsFlexibleTime_MinuteNull() == false)
                {
                    cmd.Parameters["FlxibleTime_Minute"].Value = (object)ShfDSRow.FlexibleTime_Minute;
                }
                else
                {
                    cmd.Parameters["FlxibleTime_Minute"].Value = null;
                }
                //BREAK TIME HOUR
                if (ShfDSRow.IsBreakTime_HourNull() == false)
                {
                    cmd.Parameters["BreakTime_Hour"].Value = (object)ShfDSRow.BreakTime_Hour;
                }
                else
                {
                    cmd.Parameters["BreakTime_Hour"].Value = null;
                }
                //BREAK TIME MIN
                if (ShfDSRow.IsBreakTime_MinuteNull() == false)
                {
                    cmd.Parameters["BreakTime_Minute"].Value = (object)ShfDSRow.BreakTime_Minute;
                }
                else
                {
                    cmd.Parameters["BreakTime_Minute"].Value = null;
                }
                //BREAK TIME INCLUDE
                if (ShfDSRow.IsBreakTime_IncludeNull() == false)
                {
                    cmd.Parameters["BreakTime_Include"].Value = (object)ShfDSRow.BreakTime_Include;
                }
                else
                {
                    cmd.Parameters["BreakTime_Include"].Value = null;
                }
                //CONSI TIME HOUR
                if (ShfDSRow.IsConsiderableTime_HourNull() == false)
                {
                    cmd.Parameters["ConsiderableTime_Hour"].Value = (object)ShfDSRow.ConsiderableTime_Hour;
                }
                else
                {
                    cmd.Parameters["ConsiderableTime_Hour"].Value = null;
                }
                //CONSI TIME MIN
                if (ShfDSRow.IsConsiderableTime_MinuteNull() == false)
                {
                    cmd.Parameters["ConsiderableTime_Minute"].Value = (object)ShfDSRow.ConsiderableTime_Minute;
                }
                else
                {
                    cmd.Parameters["ConsiderableTime_Minute"].Value = null;
                }
                //CONSI INCLUDE
                if (ShfDSRow.IsConsiderableTime_IncludeNull() == false)
                {
                    cmd.Parameters["ConsiderableTime_Include"].Value = (object)ShfDSRow.ConsiderableTime_Include;
                }
                else
                {
                    cmd.Parameters["ConsiderableTime_Include"].Value = null;
                }
                ////MAX OVERTIME HOUR
                //if (ShfDSRow.IsMaxOvertime_HourNull() == false)
                //{
                //    cmd.Parameters["MaxOvertime_Hour"].Value = (object)ShfDSRow.MaxOvertime_Hour;
                //}
                //else
                //{
                //    cmd.Parameters["MaxOvertime_Hour"].Value = null;
                //}
                ////MAX OVERTIME MIN
                //if (ShfDSRow.IsMaxOvertime_MinuteNull() == false)
                //{
                //    cmd.Parameters["MaxOvertime_Minute"].Value = (object)ShfDSRow.MaxOvertime_Minute;
                //}
                //else
                //{
                //    cmd.Parameters["MaxOvertime_Minute"].Value = null;
                //}

                //MIN OVERTIME HOUR
                if (ShfDSRow.IsMinOvertime_MinNull() == false) cmd.Parameters["MinOvertime_Min"].Value = (object)ShfDSRow.MinOvertime_Min;
                else cmd.Parameters["MinOvertime_Min"].Value = null;
                //MAX OVERTIME MIN
                if (ShfDSRow.IsMaxOvertime_MinNull() == false) cmd.Parameters["MaxOvertime_Min"].Value = (object)ShfDSRow.MaxOvertime_Min;
                else cmd.Parameters["MaxOvertime_Min"].Value = null;

                //LOGINLIMT HOUR
                if (ShfDSRow.IsLoginLimit_HourNull() == false)
                {
                    cmd.Parameters["LoginLimit_Hour"].Value = (object)ShfDSRow.LoginLimit_Hour;
                }
                else
                {
                    cmd.Parameters["LoginLimit_Hour"].Value = null;
                }
                //LOGINLIMIT MIN
                if (ShfDSRow.IsLoginLimit_MinNull() == false)
                {
                    cmd.Parameters["LoginLimit_Min"].Value = (object)ShfDSRow.LoginLimit_Min;
                }
                else
                {
                    cmd.Parameters["LoginLimit_Min"].Value = null;
                }
                //LOGOUT LIMIT HOUR
                if (ShfDSRow.IsLogoutLimit_HourNull() == false)
                {
                    cmd.Parameters["LogoutLimit_Hour"].Value = (object)ShfDSRow.LogoutLimit_Hour;
                }
                else
                {
                    cmd.Parameters["LogoutLimit_Hour"].Value = null;
                }
                //LOGOUT LIMIT MIN
                if (ShfDSRow.IsLogoutLimit_MinNull() == false)
                {
                    cmd.Parameters["LogoutLimit_Min"].Value = (object)ShfDSRow.LogoutLimit_Min;
                }
                else
                {
                    cmd.Parameters["LogoutLimit_Min"].Value = null;
                }
                //MIN WORKTIME MIN
                if (!ShfDSRow.IsMinWorkingTime_MinNull()) cmd.Parameters["MinWorkingTime_Min"].Value = (object)ShfDSRow.MinWorkingTime_Min;
                else cmd.Parameters["MinWorkingTime_Min"].Value = null;

                cmd.Parameters["LogintimeWithFlexiTime_Hour"].Value = (object)ShfDSRow.LogintimeWithFlexiTime_Hour;
                cmd.Parameters["LogintimeWithFlexiTime_Min"].Value = (object)ShfDSRow.LogintimeWithFlexiTime_Min;
                cmd.Parameters["StdWorkingTime_Hour"].Value = (object)ShfDSRow.StdWorkingTime_Hour;
                cmd.Parameters["StdWorkingTime_Min"].Value = (object)ShfDSRow.StdWorkingTime_Min;
                cmd.Parameters["DefaultSelection"].Value = (object)ShfDSRow.DefaultSelection;
                cmd.Parameters["IsNightShift"].Value = (object)ShfDSRow.IsNightShift;

                #region DefineShifting Reset
                if (ShfDSRow.DefaultSelection)
                {
                    OleDbCommand cmd_Def = DBCommandProvider.GetDBCommand("DefaultSelectionShiftingPlan");
                    if (cmd_Def == null) { return UtilDL.GetCommandNotFound(); }
                    bool bError_Def = false;
                    int nRowAffected_Def = -1;
                    nRowAffected_Def = ADOController.Instance.ExecuteNonQuery(cmd_Def, connDS.DBConnections[0].ConnectionID, ref bError_Def);

                    if (bError_Def == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_SHIFTING_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SHIFTING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateShifting(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            //TestDS PersonDS = new TestDS();
            ShiftingDS ShfDS = new ShiftingDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateShifting");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            //PersonDS.Merge(inputDS.Tables[PersonDS.Person.TableName], false, MissingSchemaAction.Error);
            //PersonDS.AcceptChanges();

            ShfDS.Merge(inputDS.Tables[ShfDS.DefineShifting.TableName], false, MissingSchemaAction.Error);
            ShfDS.AcceptChanges();
            //foreach (TestDS.PersonRow PrnDSRow in PersonDS.Person.Rows)

            foreach (ShiftingDS.DefineShiftingRow ShfDSRow in ShfDS.DefineShifting.Rows)
            {
                //code

                //if (PrnDSRow.IsPcodeNull() == false)
                //{
                //    cmd.Parameters["PrnCode"].Value = (object)PrnDSRow.Pcode;
                //}
                //else
                //{
                //    cmd.Parameters["PrnCode"].Value = null;
                //}

                //CODE
                if (ShfDSRow.IsShiftingCodeNull() == false)
                {
                    cmd.Parameters["ShiftingCode"].Value = (object)ShfDSRow.ShiftingCode;
                }
                else
                {
                    cmd.Parameters["ShiftingCode"].Value = null;
                }

                //NAME
                if (ShfDSRow.IsShiftingNameNull() == false)
                {
                    cmd.Parameters["ShiftingName"].Value = (object)ShfDSRow.ShiftingName;
                }
                else
                {
                    cmd.Parameters["ShiftingName"].Value = null;
                }



                //LOGIN TIME HOUR

                if (ShfDSRow.IsLoginTime_HourNull() == false)
                {
                    cmd.Parameters["LoginTime_Hour"].Value = (object)ShfDSRow.LoginTime_Hour;
                }
                else
                {
                    cmd.Parameters["LoginTime_Hour"].Value = null;
                }


                //LOGIN TIME MIN

                if (ShfDSRow.IsLoginTime_MinuteNull() == false)
                {
                    cmd.Parameters["LoginTime_Minute"].Value = (object)ShfDSRow.LoginTime_Minute;
                }
                else
                {
                    cmd.Parameters["LoginTime_Minute"].Value = null;
                }


                //LOGIN TIME HOUR HH
                if (ShfDSRow.IsLoginTime_Hour_HHNull() == false)
                {
                    cmd.Parameters["LoginTime_Hour_HH"].Value = (object)ShfDSRow.LoginTime_Hour_HH;
                }
                else
                {
                    cmd.Parameters["LoginTime_Hour_HH"].Value = null;
                }


                //LOGIN TIME MIN HH
                if (ShfDSRow.IsLoginTime_Minute_HHNull() == false)
                {
                    cmd.Parameters["LoginTime_Minute_HH"].Value = (object)ShfDSRow.LoginTime_Minute_HH;
                }
                else
                {
                    cmd.Parameters["LoginTime_Minute_HH"].Value = null;
                }
                /////////////////////////////////
                //LOGOUT TIME HOUR 
                if (ShfDSRow.IsLogoutTime_HourNull() == false)
                {
                    cmd.Parameters["LogoutTime_Hour"].Value = (object)ShfDSRow.LogoutTime_Hour;
                }
                else
                {
                    cmd.Parameters["LogoutTime_Hour"].Value = null;
                }
                //LOGOUT TIME MIN
                if (ShfDSRow.IsLogoutTime_MinuteNull() == false)
                {
                    cmd.Parameters["LogoutTime_Minute"].Value = (object)ShfDSRow.LogoutTime_Minute;
                }
                else
                {
                    cmd.Parameters["LogoutTime_Minute"].Value = null;
                }
                //LOGOUT TIME HOUR HH
                if (ShfDSRow.IsLogoutTime_HourNull() == false)
                {
                    cmd.Parameters["LogoutTime_Hour_HH"].Value = (object)ShfDSRow.LogoutTime_Hour_HH;
                }
                else
                {
                    cmd.Parameters["LogoutTime_Hour_HH"].Value = null;
                }
                //LOGOUT TIME MIN HH
                if (ShfDSRow.IsLogoutTime_MinuteNull() == false)
                {
                    cmd.Parameters["LogoutTime_Minute_HH"].Value = (object)ShfDSRow.LogoutTime_Minute_HH;
                }
                else
                {
                    cmd.Parameters["LogoutTime_Minute_HH"].Value = null;
                }
                //FLEXIBLE TIME HOUR
                if (ShfDSRow.IsFlexibleTime_HourNull() == false)
                {
                    cmd.Parameters["FlexibleTime_Hour"].Value = (object)ShfDSRow.FlexibleTime_Hour;
                }
                else
                {
                    cmd.Parameters["FlexibleTime_Hour"].Value = null;
                }
                //FLEXIBLE TIME MIN
                if (ShfDSRow.IsFlexibleTime_MinuteNull() == false)
                {
                    cmd.Parameters["FlxibleTime_Minute"].Value = (object)ShfDSRow.FlexibleTime_Minute;
                }
                else
                {
                    cmd.Parameters["FlxibleTime_Minute"].Value = null;
                }
                //BREAK TIME HOUR
                if (ShfDSRow.IsBreakTime_HourNull() == false)
                {
                    cmd.Parameters["BreakTime_Hour"].Value = (object)ShfDSRow.BreakTime_Hour;
                }
                else
                {
                    cmd.Parameters["BreakTime_Hour"].Value = null;
                }
                //BREAK TIME MIN
                if (ShfDSRow.IsBreakTime_MinuteNull() == false)
                {
                    cmd.Parameters["BreakTime_Minute"].Value = (object)ShfDSRow.BreakTime_Minute;
                }
                else
                {
                    cmd.Parameters["BreakTime_Minute"].Value = null;
                }
                //BREAK TIME INCLUDE
                if (ShfDSRow.IsBreakTime_IncludeNull() == false)
                {
                    cmd.Parameters["BreakTime_Include"].Value = (object)ShfDSRow.BreakTime_Include;
                }
                else
                {
                    cmd.Parameters["BreakTime_Include"].Value = null;
                }
                //CONSI TIME HOUR
                if (ShfDSRow.IsConsiderableTime_HourNull() == false)
                {
                    cmd.Parameters["ConsiderableTime_Hour"].Value = (object)ShfDSRow.ConsiderableTime_Hour;
                }
                else
                {
                    cmd.Parameters["ConsiderableTime_Hour"].Value = null;
                }
                //CONSI TIME MIN
                if (ShfDSRow.IsConsiderableTime_MinuteNull() == false)
                {
                    cmd.Parameters["ConsiderableTime_Minute"].Value = (object)ShfDSRow.ConsiderableTime_Minute;
                }
                else
                {
                    cmd.Parameters["ConsiderableTime_Minute"].Value = null;
                }
                //CONSI INCLUDE
                if (ShfDSRow.IsConsiderableTime_IncludeNull() == false)
                {
                    cmd.Parameters["ConsiderableTime_Include"].Value = (object)ShfDSRow.ConsiderableTime_Include;
                }
                else
                {
                    cmd.Parameters["ConsiderableTime_Include"].Value = null;
                }
                ////MAX OVERTIME HOUR
                //if (ShfDSRow.IsMaxOvertime_HourNull() == false)
                //{
                //    cmd.Parameters["MaxOvertime_Hour"].Value = (object)ShfDSRow.MaxOvertime_Hour;
                //}
                //else
                //{
                //    cmd.Parameters["MaxOvertime_Hour"].Value = null;
                //}
                ////MAX OVERTIME MIN
                //if (ShfDSRow.IsMaxOvertime_MinuteNull() == false)
                //{
                //    cmd.Parameters["MaxOvertime_Minute"].Value = (object)ShfDSRow.MaxOvertime_Minute;
                //}
                //else
                //{
                //    cmd.Parameters["MaxOvertime_Minute"].Value = null;
                //}

                //MIN OVERTIME HOUR
                if (ShfDSRow.IsMinOvertime_MinNull() == false) cmd.Parameters["MinOvertime_Min"].Value = (object)ShfDSRow.MinOvertime_Min;
                else cmd.Parameters["MinOvertime_Min"].Value = null;
                //MAX OVERTIME MIN
                if (ShfDSRow.IsMaxOvertime_MinNull() == false) cmd.Parameters["MaxOvertime_Min"].Value = (object)ShfDSRow.MaxOvertime_Min;
                else cmd.Parameters["MaxOvertime_Min"].Value = null;
                //LOGINLIMT HOUR
                if (ShfDSRow.IsLoginLimit_HourNull() == false)
                {
                    cmd.Parameters["LoginLimit_Hour"].Value = (object)ShfDSRow.LoginLimit_Hour;
                }
                else
                {
                    cmd.Parameters["LoginLimit_Hour"].Value = null;
                }
                //LOGINLIMIT MIN
                if (ShfDSRow.IsLoginLimit_MinNull() == false)
                {
                    cmd.Parameters["LoginLimit_Min"].Value = (object)ShfDSRow.LoginLimit_Min;
                }
                else
                {
                    cmd.Parameters["LoginLimit_Min"].Value = null;
                }
                //LOGOUT LIMIT HOUR
                if (ShfDSRow.IsLogoutLimit_HourNull() == false)
                {
                    cmd.Parameters["LogoutLimit_Hour"].Value = (object)ShfDSRow.LogoutLimit_Hour;
                }
                else
                {
                    cmd.Parameters["LogoutLimit_Hour"].Value = null;
                }
                //LOGOUT LIMIT MIN
                if (ShfDSRow.IsLogoutLimit_MinNull() == false)
                {
                    cmd.Parameters["LogoutLimit_Min"].Value = (object)ShfDSRow.LogoutLimit_Min;
                }
                else
                {
                    cmd.Parameters["LogoutLimit_Min"].Value = null;
                }
                //MIN WORKTIME MIN
                if (!ShfDSRow.IsMinWorkingTime_MinNull()) cmd.Parameters["MinWorkingTime_Min"].Value = (object)ShfDSRow.MinWorkingTime_Min;
                else cmd.Parameters["MinWorkingTime_Min"].Value = null;

                cmd.Parameters["LogintimeWithFlexiTime_Hour"].Value = (object)ShfDSRow.LogintimeWithFlexiTime_Hour;
                cmd.Parameters["LogintimeWithFlexiTime_Min"].Value = (object)ShfDSRow.LogintimeWithFlexiTime_Min;
                cmd.Parameters["StdWorkingTime_Hour"].Value = (object)ShfDSRow.StdWorkingTime_Hour;
                cmd.Parameters["StdWorkingTime_Min"].Value = (object)ShfDSRow.StdWorkingTime_Min;
                cmd.Parameters["DefaultSelection"].Value = (object)ShfDSRow.DefaultSelection;
                cmd.Parameters["IsNightShift"].Value = (object)ShfDSRow.IsNightShift;

                #region DefineShifting Reset
                if (ShfDSRow.DefaultSelection)
                {
                    OleDbCommand cmd_Def = DBCommandProvider.GetDBCommand("DefaultSelectionShiftingPlan");
                    if (cmd_Def == null) { return UtilDL.GetCommandNotFound(); }
                    bool bError_Def = false;
                    int nRowAffected_Def = -1;
                    nRowAffected_Def = ADOController.Instance.ExecuteNonQuery(cmd_Def, connDS.DBConnections[0].ConnectionID, ref bError_Def);

                    if (bError_Def == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_SHIFTING_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SHIFTING_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteShifting(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteShifting");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["ShiftingCode"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SHIFTING_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _getShiftingList(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            //TestDS PersonDS = new TestDS();
            ShiftingDS ShfDS = new ShiftingDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string ShiftingCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            ShiftingCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Get sql command from commandXML folder
            if (ShiftingCode != "")
            {
                cmd = DBCommandProvider.GetDBCommand("GetShiftingList");
                cmd.Parameters["ShiftingCode"].Value = ShiftingCode;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetShiftingListAll");
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, PersonDS, PersonDS.Person.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            nRowAffected = ADOController.Instance.Fill(adapter, ShfDS, ShfDS.DefineShifting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            //PersonDS.AcceptChanges();
            ShfDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(ShfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;





        }
        private DataSet _getShiftingListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetShiftingListAll");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(28 Sep 11)...


            ShiftingDS ShfDS = new ShiftingDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, ShfDS, ShfDS.DefineShifting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            ShfDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(ShfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getShiftingListAllDropdownlist(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetShiftingListAllForDropDown");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(28 Sep 11)...


            ShiftingDS ShfDS = new ShiftingDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, ShfDS, ShfDS.DefineShifting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            ShfDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(ShfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getShiftingListGrid(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            //TestDS PersonDS = new TestDS();
            ShiftingDS ShfDS = new ShiftingDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string ShiftingCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            ShiftingCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Get sql command from commandXML folder
            if (ShiftingCode != "")
            {
                cmd = DBCommandProvider.GetDBCommand("GetShiftingListGrid");
                cmd.Parameters["ShiftingCode"].Value = ShiftingCode;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetShiftingListAllGrid");
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, PersonDS, PersonDS.Person.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            nRowAffected = ADOController.Instance.Fill(adapter, ShfDS, ShfDS.DefineShiftingGrid.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            //PersonDS.AcceptChanges();
            ShfDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(ShfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;





        }
        private DataSet _getShiftingListAllGrid(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetShiftingListAllGrid");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(28 Sep 11)...


            ShiftingDS ShfDS = new ShiftingDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, ShfDS, ShfDS.DefineShiftingGrid.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            ShfDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(ShfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }


        private DataSet _getAccommodationFilterByAll(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            //TestDS PersonDS = new TestDS();
            ShiftingDS ShfDS = new ShiftingDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            int SiteID, GroupID;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            SiteID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            GroupID = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            cmd = DBCommandProvider.GetDBCommand("GetShiftingAccommodationFilterByAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["SiteID"].Value = SiteID;
            cmd.Parameters["GroupID1"].Value = GroupID;
            cmd.Parameters["GroupId2"].Value = GroupID;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, ShfDS, ShfDS.ShiftingAccommodationPlan.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACCOMOTDATION_FILTER_BY_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            ShfDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(ShfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createAccommodationPlanning(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ATT_ACC_PLAN_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            ShiftingDS ShfDS = new ShiftingDS();
            ShfDS.Merge(inputDS.Tables[ShfDS.ShiftingAccommodationPlan.TableName], false, MissingSchemaAction.Error);
            ShfDS.AcceptChanges();

            foreach (ShiftingDS.ShiftingAccommodationPlanRow ShfDSRow in ShfDS.ShiftingAccommodationPlan.Rows)
            {

                if (!ShfDSRow.IsATT_WAPIDNull())
                {
                    if (ShfDSRow.ATT_WAPID == 0)
                    {
                        cmd.Parameters.AddWithValue("p_ATT_WAPID", DBNull.Value);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("p_ATT_WAPID", (object)ShfDSRow.ATT_WAPID);
                    }

                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ATT_WAPID", DBNull.Value);
                }
                if (!ShfDSRow.IsSiteIDNull()) cmd.Parameters.AddWithValue("p_SiteID", (object)ShfDSRow.SiteID);
                else cmd.Parameters.AddWithValue("p_SiteID", DBNull.Value);
                if (!ShfDSRow.IsGroupIDNull()) cmd.Parameters.AddWithValue("p_GroupID", (object)ShfDSRow.GroupID);
                else cmd.Parameters.AddWithValue("p_GroupID", DBNull.Value);
                if (!ShfDSRow.IsShiftingIDNull()) cmd.Parameters.AddWithValue("p_ShiftingID", (object)ShfDSRow.ShiftingID);
                else cmd.Parameters.AddWithValue("p_ShiftingID", DBNull.Value);
                if (!ShfDSRow.IsEMP_AccommodationNull()) cmd.Parameters.AddWithValue("p_Emp_Accommodation", (object)ShfDSRow.EMP_Accommodation);
                else cmd.Parameters.AddWithValue("p_Emp_Accommodation", DBNull.Value);
                if (!ShfDSRow.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", (object)ShfDSRow.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACCOMMODATION_PLANNING_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }


        private DataSet _updateOvertimeCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ATT_OT_CATEGORY_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            AttendanceMonthDS AttDS = new AttendanceMonthDS();
            AttDS.Merge(inputDS.Tables[AttDS.OverTimeCategory.TableName], false, MissingSchemaAction.Error);
            AttDS.AcceptChanges();

            foreach (AttendanceMonthDS.OverTimeCategoryRow row in AttDS.OverTimeCategory.Rows)
            {

                if (row.IsOverTime_CategoryIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_OTCategoryID", row.OverTime_CategoryID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_OTCategoryID", DBNull.Value);
                }

                if (row.IsSalary_NONull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SalaryNo", row.Salary_NO);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SalaryNo", DBNull.Value);
                }

                if (row.IsBasedOnBasicNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_BasedOnBasic", row.BasedOnBasic);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_BasedOnBasic", DBNull.Value);
                }
                if (row.IsHour_Per_MonthNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HourPerMonth", row.Hour_Per_Month);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HourPerMonth", DBNull.Value);
                }
                if (row.IsEntitled_PercentNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EntitledPercent", row.Entitled_Percent);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EntitledPercent", DBNull.Value);
                }
                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                }
                if (row.IsLast_UpdateUserNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LastUpdateUser", row.Last_UpdateUser);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LastUpdateUser", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OVERTIME_CATEGORY_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
    }
}