﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;

namespace Sylvia.DAL.Payroll
{
    class ShiftingGroupDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }
            switch (actionID)
            {
                case ActionID.NA_ACTION_GET_SHIFTING_GROUP:
                    return this._getShiftingGroup(inputDS);
                case ActionID.ACTION_SHIFTING_GROUP_UPD:
                    return this._updateShiftingGroup(inputDS);
                case ActionID.NA_ACTION_GET_SHIFTING_GROUP_LIST:
                    return this._getShiftingGroupList(inputDS);
                case ActionID.ACTION_SHIFTING_GROUP_DEL:
                    return _deleteGroup(inputDS);
                case ActionID.NA_ACTION_GET_SHIFTING_GROUP_DROPDOWN:
                    return this._getShiftingGroupDropDown(inputDS);
                case ActionID.ACTION_SHIFTING_GROUP_ADD:
                    return this._addShiftingGroup(inputDS);
                case ActionID.ACTION_DOES_GROUPNAME_EXIST:
                    return this._doesGroupNameExist(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }

        private DataSet _getShiftingGroup(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            ShiftingGroupDS groupDS = new ShiftingGroupDS();
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            string GroupName = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            GroupName = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            if (GroupName != null && GroupName != "")
            {
                cmd = DBCommandProvider.GetDBCommand("GetShiftingGroup");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
                cmd.Parameters["GroupName"].Value = GroupName;
            }
            else cmd = DBCommandProvider.GetDBCommand("GetShiftingGroupAll");

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, groupDS, groupDS.Shifting_Group_Grid.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_GROUP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            groupDS.AcceptChanges();

            // now create the packet
            DataSet returnDS = new DataSet();
            returnDS.Merge(groupDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _updateShiftingGroup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ShiftingGroupDS groupDS = new ShiftingGroupDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError;
            int nRowAffected;

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            groupDS.Merge(inputDS.Tables[groupDS.Shifting_Group.TableName], false, MissingSchemaAction.Error);
            groupDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            int groupID = Convert.ToInt32(groupDS.Shifting_Group.Rows[0]["GroupID"]);

            #region First Delete Old Records...

            OleDbCommand cmd_upd = new OleDbCommand();
            cmd_upd.CommandText = "PRO_SHIFTING_GROUP_UPD";
            cmd_upd.CommandType = CommandType.StoredProcedure;
            if (cmd_upd == null) return UtilDL.GetCommandNotFound();
            cmd_upd.Parameters.AddWithValue("p_GroupID", groupID);
            cmd_upd.Parameters.AddWithValue("p_GroupName", Convert.ToString(groupDS.Shifting_Group.Rows[0]["GroupName"]));
            cmd_upd.Parameters.AddWithValue("p_IsActive", Convert.ToBoolean(groupDS.Shifting_Group.Rows[0]["IsActive"]));
            cmd_upd.Parameters.AddWithValue("p_DefaultSelection", Convert.ToBoolean(groupDS.Shifting_Group.Rows[0]["DefaultSelection"]));
            if (Convert.ToInt32(groupDS.Shifting_Group.Rows[0]["RecyclingDay"]) == 0) cmd_upd.Parameters.AddWithValue("p_RecyclingDay", System.DBNull.Value);
            else cmd_upd.Parameters.AddWithValue("p_RecyclingDay", Convert.ToInt32(groupDS.Shifting_Group.Rows[0]["RecyclingDay"]));

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_upd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SHIFTING_GROUP_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
            }
            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(stringDS.DataStrings[0].StringValue);
            else messageDS.ErrorMsg.AddErrorMsgRow(stringDS.DataStrings[0].StringValue);

            #endregion

            #region Insert new Records...

            foreach (ShiftingGroupDS.Shifting_GroupRow details in groupDS.Shifting_Group.Rows)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_SHIFTING_GROUP_DETAILS_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_GroupID", groupID);
                cmd.Parameters.AddWithValue("p_Duration", details.Duration);
                cmd.Parameters.AddWithValue("p_ShiftingID", details.ShiftingID);
                cmd.Parameters.AddWithValue("p_Serial", details.Serial);

                if (details.IsNextShiftingIDNull()) cmd.Parameters.AddWithValue("p_NextShiftingID", System.DBNull.Value);
                else cmd.Parameters.AddWithValue("p_NextShiftingID", details.NextShiftingID);

                if (details.IsPrevShiftingIDNull()) cmd.Parameters.AddWithValue("p_PrevShiftingID", System.DBNull.Value);
                else cmd.Parameters.AddWithValue("p_PrevShiftingID", details.PrevShiftingID);

                if (details.IsPrevShiftIsNightNull()) cmd.Parameters.AddWithValue("p_PrevShiftIsNight", System.DBNull.Value);
                else cmd.Parameters.AddWithValue("p_PrevShiftIsNight", details.PrevShiftIsNight);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SHIFTING_GROUP_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0)
                {
                    if (messageDS.SuccessMsg.Count == 0 && messageDS.ErrorMsg.Count == 0) messageDS.SuccessMsg.AddSuccessMsgRow(stringDS.DataStrings[0].StringValue);
                }
                else messageDS.ErrorMsg.AddErrorMsgRow(stringDS.DataStrings[0].StringValue);
                cmd.Parameters.Clear();
            }
            #endregion

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _getShiftingGroupList(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            ShiftingGroupDS groupDS = new ShiftingGroupDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetShiftingGroupList");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, groupDS, groupDS.Shifting_Group.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_GROUP_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            groupDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(groupDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _deleteGroup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            ShiftingGroupDS groupDS = new ShiftingGroupDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            groupDS.Merge(inputDS.Tables[groupDS.Shifting_Group.TableName], false, MissingSchemaAction.Error);
            groupDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SHIFTING_GROUP_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            foreach (ShiftingGroupDS.Shifting_GroupRow row in groupDS.Shifting_Group.Rows)
            {
                cmd.Parameters.AddWithValue("p_GroupName", row.GroupName);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SHIFTING_GROUP_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.GroupName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.GroupName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _getShiftingGroupDropDown(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            ShiftingGroupDS groupDS = new ShiftingGroupDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetShiftingGroupDropDown");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, groupDS, groupDS.Shifting_Group.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_GROUP_DROPDOWN.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            groupDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(groupDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _addShiftingGroup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ShiftingGroupDS groupDS = new ShiftingGroupDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError;
            int nRowAffected;

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            groupDS.Merge(inputDS.Tables[groupDS.Shifting_Group.TableName], false, MissingSchemaAction.Error);
            groupDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1) return UtilDL.GetDBOperationFailed();

            foreach (ShiftingGroupDS.Shifting_GroupRow row in groupDS.Shifting_Group.Rows)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_SHIFTING_GROUP_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                //long genPK = IDGenerator.GetNextGenericPK();
                //if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_GroupID", genPK);
                cmd.Parameters.AddWithValue("p_GroupName", row.GroupName);
                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("p_DefaultSelection", row.DefaultSelection);

                if (row.RecyclingDay == 0) cmd.Parameters.AddWithValue("p_RecyclingDay", System.DBNull.Value);
                else cmd.Parameters.AddWithValue("p_RecyclingDay", row.RecyclingDay);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SHIFTING_GROUP_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0)
                {
                    if (messageDS.SuccessMsg.Count == 0 && messageDS.ErrorMsg.Count == 0) messageDS.SuccessMsg.AddSuccessMsgRow(stringDS.DataStrings[0].StringValue);
                }
                else messageDS.ErrorMsg.AddErrorMsgRow(stringDS.DataStrings[0].StringValue);
                break;

            }

            foreach (ShiftingGroupDS.Shifting_GroupRow details in groupDS.Shifting_Group.Rows)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_SHIFTING_GROUP_DETAILS_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_GroupID", genPK);
                cmd.Parameters.AddWithValue("p_Duration", details.Duration);
                cmd.Parameters.AddWithValue("p_ShiftingID", details.ShiftingID);
                cmd.Parameters.AddWithValue("p_Serial", details.Serial);

                if (details.IsNextShiftingIDNull()) cmd.Parameters.AddWithValue("p_NextShiftingID", System.DBNull.Value);
                else cmd.Parameters.AddWithValue("p_NextShiftingID", details.NextShiftingID);

                if (details.IsPrevShiftingIDNull()) cmd.Parameters.AddWithValue("p_PrevShiftingID", System.DBNull.Value);
                else cmd.Parameters.AddWithValue("p_PrevShiftingID", details.PrevShiftingID);

                if (details.IsPrevShiftIsNightNull()) cmd.Parameters.AddWithValue("p_PrevShiftIsNight", System.DBNull.Value);
                else cmd.Parameters.AddWithValue("p_PrevShiftIsNight", details.PrevShiftIsNight);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SHIFTING_GROUP_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0)
                {
                    if (messageDS.SuccessMsg.Count == 0 && messageDS.ErrorMsg.Count == 0) messageDS.SuccessMsg.AddSuccessMsgRow(stringDS.DataStrings[0].StringValue);
                }
                else messageDS.ErrorMsg.AddErrorMsgRow(stringDS.DataStrings[0].StringValue);
                cmd.Parameters.Clear();
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _doesGroupNameExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesGroupNameExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["GroupName"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_GROUPNAME_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}