/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: April 10, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
  /// <summary>
  /// Summary description for DepartmentDL.
  /// </summary>
  public class PFParameterDL
  {
    public PFParameterDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }
    
    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {
        case ActionID.ACTION_PFPARAMETER_ADD:
          return _createPFParameter( inputDS );
        case ActionID.ACTION_PFPARAMETER_DEL:
          return _deletePFParameter(inputDS);
      case ActionID.NA_ACTION_Q_ALL_PFPARAMETER:
          return _getPFParameterList( inputDS );    
        case ActionID.ACTION_DOES_PFPARAMETER_EXIST:
          return _doesPFParameterExist( inputDS );
      case ActionID.NA_ACTION_Q_ALL_PFVALUE_FOR_EMPLOYEE:
          return _getPFParameterForEmployee(inputDS);    
        
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
    private DataSet _doesPFParameterExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataDateDS dateDS = new DataDateDS();
      dateDS.Merge( inputDS.Tables[ dateDS.DataDates.TableName ], false, MissingSchemaAction.Error );
      dateDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesPFParameterExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(dateDS.DataDates[0].IsDateValueNull()==false)
      {
        cmd.Parameters["EffectDate"].Value = dateDS.DataDates[0].DateValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    }
    private DataSet _deletePFParameter(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new DBConnectionDS();
      DataDateDS dateDS = new DataDateDS();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
      dateDS.AcceptChanges();


      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeletePFParameter");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if (dateDS.DataDates[0].IsDateValueNull() == false)
      {

        cmd.Parameters["EffectDate"].Value = dateDS.DataDates[0].DateValue;
      }
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.ACTION_PFPARAMETER_DEL.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;
      }

      return errDS;  // return empty ErrorDS
    }
    private DataSet _createPFParameter(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      PFParameterDS paramDS = new PFParameterDS();
      DBConnectionDS connDS = new  DBConnectionDS();
  
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreatePFParameter");
      if( cmd == null )
      {
      
        return UtilDL.GetCommandNotFound();
      }
      
      paramDS.Merge( inputDS.Tables[ paramDS.PFParameters.TableName ], false, MissingSchemaAction.Error );
      paramDS.AcceptChanges();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      foreach( PFParameterDS.PFParameter param in paramDS.PFParameters)
      {
        long genPK = IDGenerator.GetNextGenericPK();
        if ( genPK == -1)
        {
          return UtilDL.GetDBOperationFailed();
        }

  
        cmd.Parameters["ParameterId"].Value = genPK;
        
        if(param.IsEffectDateNull()==false)
        {
          cmd.Parameters["EffectDate"].Value = param.EffectDate;
        }
        else
        {
          cmd.Parameters["EffectDate"].Value = null;
        }
        if(param.IsSelfContributionNull()==false)
        {
          cmd.Parameters["SelfContribution"].Value = param.SelfContribution;
        }
        else
        {
          cmd.Parameters["SelfContribution"].Value = null;
        }
        if(param.IsCompanyContributionNull()==false)
        {
          cmd.Parameters["CompanyContribution"].Value = param.CompanyContribution;
        }
        else
        {
          cmd.Parameters["CompanyContribution"].Value = null;
        }
 
       
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _getPFParameterList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

   

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPFParameterList");
  
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      PFParameterPO paramPO = new PFParameterPO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter, 
        paramPO, paramPO.PFParameters.TableName, 
        connDS.DBConnections[0].ConnectionID, 
        ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_PFPARAMETER.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }

      paramPO.AcceptChanges();

      PFParameterDS paramDS = new PFParameterDS();
  
      if ( paramPO.PFParameters.Count > 0 )
      {
       
        foreach ( PFParameterPO.PFParameter poParam in paramPO.PFParameters)
        {
          PFParameterDS.PFParameter param = paramDS.PFParameters.NewPFParameter();
          if(poParam.IsEffectDateNull()==false)
          {
            param.EffectDate= poParam.EffectDate;
          }
          if(poParam.IsSelfContributionNull()==false)
          {
            param.SelfContribution= poParam.SelfContribution;
          }
          if(poParam.IsCompanyContributionNull()==false)
          {
            param.CompanyContribution= poParam.CompanyContribution;
          }
          paramDS.PFParameters.AddPFParameter( param );
          paramDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( paramDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
    
      return returnDS;
    
    
    }
    private DataSet _getPFParameterForEmployee(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new DBConnectionDS();
      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
      stringDS.AcceptChanges();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();



      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPFForEmployee");

      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      if (stringDS.DataStrings[0].IsStringValueNull() == false)
      {

        cmd.Parameters["EmployeeId"].Value = UtilDL.GetEmployeeId(connDS,stringDS.DataStrings[0].StringValue);
      }
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;
      
      PFEmployeePO paramPO = new PFEmployeePO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter,
        paramPO, paramPO.PFEmployee.TableName,
        connDS.DBConnections[0].ConnectionID,
        ref bError);
      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_PFVALUE_FOR_EMPLOYEE.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;

      }

      paramPO.AcceptChanges();

      PFForEmployeeDS paramDS = new PFForEmployeeDS();

      if (paramPO.PFEmployee.Count > 0)
      {

        foreach (PFEmployeePO.PFEmployeeRow poParam in paramPO.PFEmployee)
        {
          PFForEmployeeDS.PFEmployeeRow param = paramDS.PFEmployee.NewPFEmployeeRow();
          if (poParam.IsHeadIdNull() == false)
          {
            param.SalaryHeadId = poParam.HeadId;
          }
          if (poParam.IsHeadTypeNull() == false)
          {
            param.SalaryHeadType = poParam.HeadType;
          }
          if (poParam.IsDescriptionNull() == false)
          {
            param.Description = poParam.Description;
          }
          if (poParam.IsAmountNull() == false)
          {
            param.Amount = poParam.Amount;
          }
          if (poParam.IsOpenningBalanceNull() == false)
          {
              param.OpenningBalance = poParam.OpenningBalance;
          }
          paramDS.PFEmployee.AddPFEmployeeRow(param);
          paramDS.AcceptChanges();
        }
      }

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();

      DataSet returnDS = new DataSet();

      returnDS.Merge(paramDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();

      return returnDS;


    }
  }
}
