/* *********************************************************************
 *  Compamy: Milllennium Information Solution Limited		 		           *  
 *  Author: Md. Hasinur Rahman Likhon				 				                   *
 *  Comment Date: March 29, 2006									                     *
 ***********************************************************************/



using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
	/// <summary>
	/// Summary description for CostCenterDL.
	/// </summary>
	public class CostCenterDL
	{
		public CostCenterDL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		public DataSet Execute(DataSet inputDS)
		{
			DataSet returnDS = new DataSet();
			ErrorDS errDS = new ErrorDS();

			ActionDS actionDS = new  ActionDS();
			actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
			actionDS.AcceptChanges();

			ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
			try
			{
				actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
			}
			catch
			{
				ErrorDS.Error err = errDS.Errors.NewError();
				err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
				err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
				err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
				err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
				errDS.Errors.AddError(err );
				errDS.AcceptChanges();

				returnDS.Merge( errDS );
				returnDS.AcceptChanges();
				return returnDS;
			}

			switch ( actionID )
			{
				case ActionID.ACTION_COSTCENTER_ADD:
					return _createCostCenter( inputDS );
                case ActionID.NA_ACTION_GET_COSTCENTER_LIST:
					return _getCostCenterList(inputDS);
				case ActionID.ACTION_COSTCENTER_DEL:
					return this._deleteCostCenter( inputDS );
				case ActionID.ACTION_COSTCENTER_UPD:
					return this._updateCostCenter( inputDS );
        case ActionID.ACTION_DOES_COSTCENTER_EXIST:
          return this._doesCostCenterExist( inputDS );
				default:
					Util.LogInfo("Action ID is not supported in this class."); 
					ErrorDS.Error err = errDS.Errors.NewError();
					err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
					err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
					err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
					err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
					errDS.Errors.AddError( err );
					returnDS.Merge ( errDS );
					returnDS.AcceptChanges();
					return returnDS;
			}  // end of switch statement

		}
		private DataSet _createCostCenter(DataSet inputDS)
		{
      ErrorDS errDS = new ErrorDS();
      CostCenterDS costDS = new CostCenterDS();
      DBConnectionDS connDS = new  DBConnectionDS();
      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      //create command
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateCostCenter");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      costDS.Merge( inputDS.Tables[ costDS.CostCenters.TableName ], false, MissingSchemaAction.Error );
      costDS.AcceptChanges();
      foreach(CostCenterDS.CostCenter cost in costDS.CostCenters)
      {
        long genPK = IDGenerator.GetNextGenericPK();
        if( genPK == -1)
        {
          UtilDL.GetDBOperationFailed();
        }
        cmd.Parameters["Id"].Value = (object) genPK;
        //code
        if(cost.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) cost.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;
        }
        //name
        if(cost.IsNameNull()==false)
        {
          cmd.Parameters["Name"].Value = (object) cost.Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value=null;
        }
        //isactive
        if(cost.IsIsActiveNull()==false)
        {
          cmd.Parameters["IsActive"].Value = (object)cost.IsActive;
        }
        else
        {
          cmd.Parameters["IsActive"].Value = null;
        }
        cmd.Parameters["DefaultSelection"].Value = (object)cost.DefaultSelection;
 
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_COSTCENTER_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
		}
		private DataSet _deleteCostCenter(DataSet inputDS )
		{
			ErrorDS errDS = new ErrorDS();
			DBConnectionDS connDS = new  DBConnectionDS();      
			DataStringDS stringDS = new DataStringDS();
			connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
			connDS.AcceptChanges();
			stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
			stringDS.AcceptChanges();
			OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteCostCenter");
      if( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }
			cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue;
			bool bError = false;
			int nRowAffected = -1;
			nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
			if (bError == true )
			{
				ErrorDS.Error err = errDS.Errors.NewError();
				err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
				err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
				err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
				err.ErrorInfo1 = ActionID.ACTION_COSTCENTER_DEL.ToString();
				errDS.Errors.AddError( err );
				errDS.AcceptChanges();
				return errDS;
			}
			return errDS;  // return empty ErrorDS
		}
        private DataSet _getCostCenterList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCostCenterList");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            CostCenterPO costPO = new CostCenterPO();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
                costPO, costPO.CostCenters.TableName,
                connDS.DBConnections[0].ConnectionID,
                ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COSTCENTER_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            costPO.AcceptChanges();
            CostCenterDS costDS = new CostCenterDS();
            if (costPO.CostCenters.Count > 0)
            {
                foreach (CostCenterPO.CostCenter poCost in costPO.CostCenters)
                {
                    CostCenterDS.CostCenter cost = costDS.CostCenters.NewCostCenter();
                    cost.ID = poCost.ID;
                    cost.Code = poCost.CostCentercode;
                    cost.Name = poCost.CostCenterName;
                    cost.IsActive = poCost.IsActive;
                    cost.DefaultSelection = poCost.DefaultSelection;

                    costDS.CostCenters.AddCostCenter(cost);
                    costDS.AcceptChanges();
                }
            }
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(costDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateCostCenter(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CostCenterDS costDS = new CostCenterDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            costDS.Merge(inputDS.Tables[costDS.CostCenters.TableName], false, MissingSchemaAction.Error);
            costDS.AcceptChanges();


            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_ErpCC";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (CostCenterDS.CostCenter cost in costDS.CostCenters)
            {
                if (cost.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_COSTCENTER_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion


            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateCostCenter");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (CostCenterDS.CostCenter cost in costDS.CostCenters)
            {
                if (cost.IsCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)cost.Code;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }
                if (cost.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = (object)cost.Name;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }
                if (cost.IsIsActiveNull() == false)
                {
                    cmd.Parameters["IsActive"].Value = (object)cost.IsActive;
                }
                else
                {
                    cmd.Parameters["IsActive"].Value = null;
                }

                cmd.Parameters["DefaultSelection"].Value = (object)cost.DefaultSelection;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COSTCENTER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _doesCostCenterExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesCostCenterExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
        cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    } 


	}
}
