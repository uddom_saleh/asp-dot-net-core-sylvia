﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class HealthSafetyDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_HSM_TYPE_CREATE:
                    return this._createHSMType(inputDS);
                case ActionID.NA_ACTION_GET_HSM_TYPE_LIST_ALL:
                    return this._GetHSMTypeList(inputDS);
                case ActionID.ACTION_HSM_INPUT_FIELD_CREATE:
                    return this._createHSMInputField(inputDS);
                case ActionID.NA_ACTION_GET_HSM_INPUT_FIELD_LIST:
                    return this._GetHSMInputFieldList(inputDS);
                case ActionID.NA_ACTION_GET_HSM_INPUTFIELD_LIST_BY_HSMTYPEID:
                    return this._GetHSMInputListByHSMTypeID(inputDS);
                case ActionID.ACTION_HSMIFCONFIG_CREATE:
                    return this._createHSMIFConfig(inputDS);
                case ActionID.ACTION_HSM_SUBMIT:
                    return this._createHSMSubmit(inputDS);
                case ActionID.NA_ACTION_GET_HSM_SUBMISSION_DATA:
                    return this._getHSMSubmissionData(inputDS);
                case ActionID.ACTION_EMP_INJURY_RECORD_ADD:
                    return this._createInjuryRecord(inputDS);
                case ActionID.ACTION_HEALTHSAFTY_VERIFIER_ADD:
                    return this._createVerifier(inputDS);
                case ActionID.NA_ACTION_GET_HEALTHSAFTY_DATA:
                    return this._GetHealthSaftyData(inputDS);
                case ActionID.ACTION_HEALTHSAFTY_APPROVAL:
                    return this._createInjuryRecordApproval(inputDS);
                    
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        } // end of Execute Method

        private DataSet _createHSMType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            HealthSafetyDS healthDS = new HealthSafetyDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_HSM_TYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            healthDS.Merge(inputDS.Tables[healthDS.HSMType.TableName], false, MissingSchemaAction.Error);
            healthDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (HealthSafetyDS.HSMTypeRow row in healthDS.HSMType.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_REQUISITIONTYPEID", genPK);

                if (row.IsHSMTypeCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HSMTYPECODE", row.HSMTypeCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HSMTYPECODE", DBNull.Value);
                }

                if (row.IsHSMTypeNameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HSMTYPENAME", row.HSMTypeName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HSMTYPENAME", DBNull.Value);
                }

                if (row.IsHSMTypeRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HSMTYPEREMARKS", row.HSMTypeRemarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HSMTYPEREMARKS", DBNull.Value);
                }

                if (row.IsIsActiveNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ISACTIVE", row.IsActive);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ISACTIVE", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_HSM_TYPE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetHSMTypeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            HealthSafetyDS healthDS = new HealthSafetyDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetHSMtypeListAll");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, healthDS, healthDS.HSMType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_HSM_TYPE_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(healthDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createHSMInputField(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            HealthSafetyDS healthDS = new HealthSafetyDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_HSM_INPUTFIELD_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            healthDS.Merge(inputDS.Tables[healthDS.HSMInputField.TableName], false, MissingSchemaAction.Error);
            healthDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (HealthSafetyDS.HSMInputFieldRow row in healthDS.HSMInputField.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_HSMIFID", genPK);

                if (row.IsFieldNameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_FIELDNAME", row.FieldName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_FIELDNAME", DBNull.Value);
                }

                if (row.IsDataTypeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DATATYPE", row.DataType);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DATATYPE", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_REMARKS", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_REMARKS", DBNull.Value);
                }

                if (row.IsHSMIFCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HSMIFCODE", row.HSMIFCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HSMIFCODE", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_HSM_INPUT_FIELD_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetHSMInputFieldList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            HealthSafetyDS healthDS = new HealthSafetyDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetHSMInputFieldList");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, healthDS, healthDS.HSMInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_HSM_INPUT_FIELD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(healthDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _GetHSMInputListByHSMTypeID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBenefitFieldListByBenefitIDLoginID");
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetHSMFieldListByHSMTypeID");
            //cmd.Parameters["UserLoginID"].Value = (object)StringDS.DataStrings[1].StringValue;
            cmd.Parameters["HSMTypeID"].Value = Convert.ToInt32(StringDS.DataStrings[0].StringValue);

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            HealthSafetyDS healthDS = new HealthSafetyDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, healthDS, healthDS.HSMInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //err.ErrorInfo1 = ActionID.NA_ACTION_GET_BENEFIT_INPUTFIELD_LIST_BY_BENEFITID.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_HSM_INPUTFIELD_LIST_BY_HSMTYPEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            healthDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(healthDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        } 
        private DataSet _createHSMIFConfig(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            HealthSafetyDS healthDS = new HealthSafetyDS();
            healthDS.Merge(inputDS.Tables[healthDS.HIFConfig.TableName], false, MissingSchemaAction.Error);
            healthDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool bError = false;
            int nRowAffected = -1;

            #region Delete HSMIF Config

            //cmd.CommandText = "PRO_BIFCONFIG_DELETE";
            cmd.CommandText = "PRO_HSMIFCONFIG_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (HealthSafetyDS.HIFConfigRow row in healthDS.HIFConfig.Rows)
            {
                cmd.Parameters.AddWithValue("p_HSMTypeID", row.HSMTypeID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_HSMIFCONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                break;
            }
            cmd.Dispose();
            #endregion

            #region Create HSMIF Config
            cmd.CommandText = "PRO_HSMIFCONFIG_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (HealthSafetyDS.HIFConfigRow row in healthDS.HIFConfig.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_HSMTypeID", row.HSMTypeID);
                cmd.Parameters.AddWithValue("p_HSMIFID", row.HSMIFID);
                cmd.Parameters.AddWithValue("p_IsMandatory", row.IsMandatory);
                cmd.Parameters.AddWithValue("p_HSMIFConfigID", genPK);
                cmd.Parameters.AddWithValue("p_Limit_Markable", row.Limit_Markable);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_HSMIFCONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _createHSMSubmit(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            HealthSafetyDS healthDS = new HealthSafetyDS();
            DBConnectionDS connDS = new DBConnectionDS();

            healthDS.Merge(inputDS.Tables[healthDS.HSMSubmit.TableName], false, MissingSchemaAction.Error);
            healthDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create Request
            OleDbCommand cmdReq = new OleDbCommand();
            cmdReq.CommandText = "PRO_HSM_REQUEST_CREATE";
            cmdReq.CommandType = CommandType.StoredProcedure;

            if (cmdReq == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            long genPK2 = IDGenerator.GetNextGenericPK();
            if (genPK2 == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmdReq.Parameters.AddWithValue("p_HSMREQUESTID", genPK2);

            if (healthDS.HSMSubmit[0].IsHSMTypeIDNull() == false)
            {
                cmdReq.Parameters.AddWithValue("p_HSMTYPEID", healthDS.HSMSubmit[0].HSMTypeID);
            }
            else
            {
                cmdReq.Parameters.AddWithValue("p_HSMTYPEID", DBNull.Value);
            }


            if (healthDS.HSMSubmit[0].IsCurrentUserIDNull() == false)
            {
                cmdReq.Parameters.AddWithValue("p_USERID", healthDS.HSMSubmit[0].CurrentUserID);
            }
            else
            {
                cmdReq.Parameters.AddWithValue("p_USERID", DBNull.Value);
            }

            bool bErrorReq = false;
            int nRowAffectedReq = -1;
            nRowAffectedReq = ADOController.Instance.ExecuteNonQuery(cmdReq, connDS.DBConnections[0].ConnectionID, ref bErrorReq);
            cmdReq.Parameters.Clear();

            if (bErrorReq)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_HSM_SUBMIT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create Submission
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_HSM_SUBMIT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (HealthSafetyDS.HSMSubmitRow row in healthDS.HSMSubmit.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_HSMSubmitID", genPK);

                if (row.IsHSMTypeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HSMTypeID", row.HSMTypeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HSMTypeID", DBNull.Value);
                }

                if (row.IsOriginalValueNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_OriginalValue", row.OriginalValue);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_OriginalValue", DBNull.Value);
                }


                if (row.IsChangingValueNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ChangeValue", row.ChangingValue);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ChangeValue", DBNull.Value);
                }

                if (row.IsCurrentUserIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_UserID", row.CurrentUserID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_UserID", DBNull.Value);
                }

                if (row.IsHSMIFIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HSMIFID", row.HSMIFID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HSMIFID", DBNull.Value);
                }

                if (row.IsStatusNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Status", row.Status);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Status", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("p_HSMRequestID", genPK2);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_HSM_SUBMIT.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
            // Update end
        }
        private DataSet _getHSMSubmissionData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            HealthSafetyDS healthDS = new HealthSafetyDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetHSMSubmissionData");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, healthDS, healthDS.HSMSubmit.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_HSM_SUBMISSION_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            healthDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(healthDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createInjuryRecord(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            HealthSafetyDS healthDS = new HealthSafetyDS();
            HealthSafetyDS attachDS = new HealthSafetyDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            healthDS.Merge(inputDS.Tables[healthDS.InjuryRecord.TableName], false, MissingSchemaAction.Error);
            attachDS.Merge(inputDS.Tables[attachDS.HSM_FileAttach.TableName], false, MissingSchemaAction.Error);
            healthDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create Request
            bool bErrorReq = false;
            int nRowAffectedReq = -1;
            OleDbCommand cmdReq = new OleDbCommand();
            cmdReq.CommandText = "PRO_HSM_INJURYRECORD_ADD";
            cmdReq.CommandType = CommandType.StoredProcedure;
            long genPK = -1 ;
            foreach (HealthSafetyDS.InjuryRecordRow row in healthDS.InjuryRecord.Rows)
            {
                if (cmdReq == null) return UtilDL.GetCommandNotFound();
                
                genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmdReq.Parameters.AddWithValue("p_InjuryRecordID", genPK);
                cmdReq.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);

                if (!row.IsDiseaseIDNull()) cmdReq.Parameters.AddWithValue("p_DiseaseID", row.DiseaseID);
                else cmdReq.Parameters.AddWithValue("p_DiseaseID", DBNull.Value);

                if (!row.IsMedicalNameNull()) cmdReq.Parameters.AddWithValue("p_MedicalName", row.MedicalName);
                else cmdReq.Parameters.AddWithValue("p_MedicalName", DBNull.Value);

                if (!row.IsPhysicianNameNull()) cmdReq.Parameters.AddWithValue("p_PhysicianName", row.PhysicianName);
                else cmdReq.Parameters.AddWithValue("p_PhysicianName", DBNull.Value);

                if (!row.IsDateFromNull()) cmdReq.Parameters.AddWithValue("p_DateFrom", row.DateFrom);
                else cmdReq.Parameters.AddWithValue("p_DateFrom", DBNull.Value);

                if (!row.IsDateToNull()) cmdReq.Parameters.AddWithValue("p_DateTo", row.DateTo);
                else cmdReq.Parameters.AddWithValue("p_DateTo", DBNull.Value);

                if (!row.IsDateTo_IsContinueNull()) cmdReq.Parameters.AddWithValue("p_DateTo_IsContinue", row.DateTo_IsContinue);
                else cmdReq.Parameters.AddWithValue("p_DateTo_IsContinue", DBNull.Value);

                if (!row.IsReportSummaryNull()) cmdReq.Parameters.AddWithValue("p_ReportSummary", row.ReportSummary);
                else cmdReq.Parameters.AddWithValue("p_ReportSummary", DBNull.Value);

                if (!row.IsVerifierNull()) cmdReq.Parameters.AddWithValue("p_Verifier", row.Verifier);
                else cmdReq.Parameters.AddWithValue("p_Verifier", DBNull.Value);

                if (!row.IsCreatedByNull()) cmdReq.Parameters.AddWithValue("p_CreatedBy", row.CreatedBy);
                else cmdReq.Parameters.AddWithValue("p_CreatedBy", DBNull.Value);

               
                nRowAffectedReq = ADOController.Instance.ExecuteNonQuery(cmdReq, connDS.DBConnections[0].ConnectionID, ref bErrorReq);
                cmdReq.Parameters.Clear();

                if (bErrorReq)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_INJURY_RECORD_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                if (nRowAffectedReq > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + "[" + row.DiseaseName + "]" + " ==> " + row.VerifierCode + " - " + row.VerifierName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + "[" + row.DiseaseName + "]" + " ==> " + row.VerifierCode + " - " + row.VerifierName);
                messageDS.AcceptChanges();
            }
            #endregion

            #region
            if (!bErrorReq)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_HSM_FILE_ATTACHMENT";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (HealthSafetyDS.HSM_FileAttachRow row in attachDS.HSM_FileAttach.Rows)
                {
                    long genPK_Attach = IDGenerator.GetNextGenericPK();
                    if (genPK_Attach == -1) return UtilDL.GetDBOperationFailed();

                    cmd.Parameters.AddWithValue("p_AttachmentID", genPK_Attach);
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_InjuryRecordID", genPK);
                    cmd.Parameters.AddWithValue("p_ActualFileName", row.ActualFileName);
                    cmd.Parameters.AddWithValue("p_FileName", row.FileName);
                    cmd.Parameters.AddWithValue("p_FileType", row.FileType);
                   
                    bool bError2 = false;
                    int nRowAffected2 = -1;
                    nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError2);
                    cmd.Parameters.Clear();

                    if (bError2)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EMP_INJURY_RECORD_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createVerifier(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            HealthSafetyDS healthDS = new HealthSafetyDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;

            healthDS.Merge(inputDS.Tables[healthDS.HealthSaftyVerifier.TableName], false, MissingSchemaAction.Error);
            healthDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Delete Old Data
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_HSM_VERIFIER_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_HEALTHSAFTY_VERIFIER_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Save new data
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_HSM_VERIFIER_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (HealthSafetyDS.HealthSaftyVerifierRow row in healthDS.HealthSaftyVerifier.Rows)
            {
                string[] Authority = row.Verifier.Split(' ', '@');

                long pKey = IDGenerator.GetNextGenericPK();
                if (pKey == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_VerifierID", pKey);
                cmd.Parameters.AddWithValue("p_Verifier", row.Verifier);
                cmd.Parameters.AddWithValue("p_IsLoginUserID", row.LoginID);

                if (!row.IsDepartmentIDNull()) cmd.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                else cmd.Parameters.AddWithValue("p_DepartmentID", DBNull.Value);
                if (!row.IsLiabilityIDNull()) cmd.Parameters.AddWithValue("p_LiabilityID", row.LiabilityID);
                else cmd.Parameters.AddWithValue("p_LiabilityID", DBNull.Value);

                cmd.Parameters.AddWithValue("p_VerifierType", row.VerifierType);
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_HEALTHSAFTY_VERIFIER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetHealthSaftyData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            HealthSafetyDS healthDS = new HealthSafetyDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;
            string dTableName = "";

            if (stringDS.DataStrings[0].StringValue == "HSM_VerifierConfiguration")
            {
                cmd = DBCommandProvider.GetDBCommand("GetHealthSaftyVerifierConfig");
                dTableName = healthDS.HealthSaftyVerifier.TableName;
            }
            else if (stringDS.DataStrings[0].StringValue == "HSM_Verifier")
            {
                cmd = DBCommandProvider.GetDBCommand("GetHealthSaftyVerifier");
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
                dTableName = healthDS.HealthSaftyVerifier.TableName;
            }
            else if (stringDS.DataStrings[0].StringValue == "HealthSaftyApproval")
            {
                if (Convert.ToBoolean(stringDS.DataStrings[12].StringValue)) stringDS.DataStrings[4].StringValue = "-1";

                //cmd = DBCommandProvider.GetDBCommand("GetHealthSaftyApprovalData_Self");

                cmd = DBCommandProvider.GetDBCommand("GetHealthSaftyApprovalData");
                cmd.Parameters["IsVerified1"].Value = Convert.ToBoolean(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["IsVerified2"].Value = Convert.ToBoolean(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["IsRejected1"].Value = Convert.ToBoolean(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["IsRejected2"].Value = Convert.ToBoolean(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["AppHolderCode1"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["AppHolderCode2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["ApplicantName"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["SiteID1"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                cmd.Parameters["CompanyDivisionID1"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);
                cmd.Parameters["CompanyDivisionID2"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);
                cmd.Parameters["DepartmentID1"].Value = Convert.ToInt32(stringDS.DataStrings[8].StringValue);
                cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[8].StringValue);
                cmd.Parameters["DesignationID1"].Value = Convert.ToInt32(stringDS.DataStrings[9].StringValue);
                cmd.Parameters["DesignationID2"].Value = Convert.ToInt32(stringDS.DataStrings[9].StringValue);
                cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[10].StringValue);
                cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[11].StringValue);
                dTableName = healthDS.InjuryRecord.TableName;
            }
            else if (stringDS.DataStrings[0].StringValue == "InjuryRecordData")
            {
                cmd = DBCommandProvider.GetDBCommand("GetHealthSaftyInjuryRecordData");
                cmd.Parameters["InjuryRecordID1"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["InjuryRecordID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;
                dTableName = healthDS.InjuryRecord.TableName;
            }
            else if (stringDS.DataStrings[0].StringValue == "HSM_FileAttachmentInfo")
            {
                cmd = DBCommandProvider.GetDBCommand("GetHealthSaftyAttachmentInfo");
                cmd.Parameters["InjuryRecordID1"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["InjuryRecordID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;
                dTableName = healthDS.HSM_FileAttach.TableName;
            }

            if (cmd == null) return UtilDL.GetCommandNotFound();
            
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            nRowAffected = ADOController.Instance.Fill(adapter, healthDS, dTableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_HEALTHSAFTY_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(healthDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _createInjuryRecordApproval(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            HealthSafetyDS healthDS = new HealthSafetyDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            healthDS.Merge(inputDS.Tables[healthDS.InjuryRecord.TableName], false, MissingSchemaAction.Error);
            healthDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create Request
            OleDbCommand cmdReq = new OleDbCommand();
            cmdReq.CommandText = "PRO_HSM_INJURYRECORD_APPROVE";
            cmdReq.CommandType = CommandType.StoredProcedure;

            foreach (HealthSafetyDS.InjuryRecordRow row in healthDS.InjuryRecord.Rows)
            {
                cmdReq.Parameters.AddWithValue("p_InjuryRecordID", row.InjuryRecordID);

                if (!row.IsIsVerifiedNull()) cmdReq.Parameters.AddWithValue("p_IsVerified", row.IsVerified);
                else cmdReq.Parameters.AddWithValue("p_IsVerified", DBNull.Value);

                if (!row.IsIsRejectedNull()) cmdReq.Parameters.AddWithValue("p_IsRejected", row.IsRejected);
                else cmdReq.Parameters.AddWithValue("p_IsRejected", DBNull.Value);

                if (!row.IsVerifiedByNull()) cmdReq.Parameters.AddWithValue("p_VerifiedBy", row.VerifiedBy);
                else cmdReq.Parameters.AddWithValue("p_VerifiedBy", DBNull.Value);

                bool bErrorReq = false;
                int nRowAffectedReq = -1;
                nRowAffectedReq = ADOController.Instance.ExecuteNonQuery(cmdReq, connDS.DBConnections[0].ConnectionID, ref bErrorReq);
                cmdReq.Parameters.Clear();

                if (bErrorReq)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_HEALTHSAFTY_APPROVAL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                if (nRowAffectedReq > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + "[" + row.DiseaseName + "]" + " ==> " + row.VerifierCode + " - " + row.VerifierName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + "[" + row.DiseaseName + "]" + " ==> " + row.VerifierCode + " - " + row.VerifierName);
                messageDS.AcceptChanges();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}
