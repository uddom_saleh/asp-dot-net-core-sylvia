/* *********************************************************************
 *  Compamy: Milllennium Information Solution Limited		 		           *  
 *  Author: Md. Hasinur Rahman Likhon				 				                   *
 *  Comment Date: April 13, 2006									                     *
 ***********************************************************************/

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    public class SiteDL
    {
        public SiteDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {

                case ActionID.NA_ACTION_GET_SITE_LIST:
                    return _getSiteList(inputDS);
                case ActionID.ACTION_DOES_SITE_EXIST:
                    return _doesSiteExist(inputDS);
                case ActionID.ACTION_SITE_ADD:
                    return _createSite(inputDS);
                case ActionID.ACTION_SITE_DEL:
                    return _deleteSite(inputDS);
                case ActionID.ACTION_SITE_UPD:
                    return _updateSite(inputDS);
                case ActionID.ACTION_SITE_CCMAIL_USER_UPD:
                    return this._updateSiteCCMailUser(inputDS);

                case ActionID.NA_ACTION_GET_ZONE_LIST:
                    return this._getZoneList(inputDS);
                case ActionID.ACTION_ZONE_CREATE:
                    return this._createZone(inputDS);
                case ActionID.ACTION_ZONE_UPDATE:
                    return this._updateZone(inputDS);
                case ActionID.ACTION_ZONE_DELETE:
                    return this._deleteZone(inputDS);
                case ActionID.NA_ACTION_GET_SITE_LIST_BYFILTER:
                    return this._getSiteList_ByFilter(inputDS);
                //Site Category
                case ActionID.ACTION_Site_Category_ADD:
                    return _createSiteCategory(inputDS);
                case ActionID.NA_ACTION_GET_Site_Category_LIST:
                    return _getSiteCategory(inputDS);
                case ActionID.ACTION_Site_Category_DELETE:
                    return _deleteSiteCategory(inputDS);
                case ActionID.ACTION_Site_Category_UPDATE:
                    return _updateSiteCategory(inputDS);
                case ActionID.NA_ACTION_GET_SITE_BY_COMPANY:
                    return _getSiteByCompany(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getSiteList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            SiteDS sitDS = new SiteDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSiteList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sitDS, sitDS.Sites.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SITE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            sitDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(sitDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        private DataSet _doesSiteExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesSiteExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createSite(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SiteDS sitDS = new SiteDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            //create command
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateSite");

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SITE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            sitDS.Merge(inputDS.Tables[sitDS.Sites.TableName], false, MissingSchemaAction.Error);
            sitDS.AcceptChanges();

            long payrollSiteID = 0;

            #region OLD :: Commented by :: Rony
            //foreach (SiteDS.Site sit in sitDS.Sites)
            //{
            //    payrollSiteID = UtilDL.GetPayrollSiteId(connDS, sit.PayrollSiteCode);

            //    long genPK = IDGenerator.GetNextGenericPK();
            //    if (genPK == -1)
            //    {
            //        UtilDL.GetDBOperationFailed();
            //    }
            //    cmd.Parameters["Id"].Value = (object)genPK;
            //    //code
            //    if (sit.IsCodeNull() == false)
            //    {
            //        cmd.Parameters["Code"].Value = (object)sit.Code;
            //    }
            //    else
            //    {
            //        cmd.Parameters["Code"].Value = null;
            //    }
            //    //name
            //    if (sit.IsNameNull() == false)
            //    {
            //        cmd.Parameters["Name"].Value = (object)sit.Name;
            //    }
            //    else
            //    {
            //        cmd.Parameters["Name"].Value = null;
            //    }
            //    //Address
            //    if (sit.IsAddressNull() == false)
            //    {
            //        cmd.Parameters["Address"].Value = (object)sit.Address;
            //    }
            //    else
            //    {
            //        cmd.Parameters["Address"].Value = null;
            //    }

            //    if (!sit.IsKPIGroupID_SNull())
            //    {
            //        cmd.Parameters["KPIGroupID_S"].Value = sit.KPIGroupID_S;
            //    }
            //    else cmd.Parameters["KPIGroupID_S"].Value = DBNull.Value;


            //    #region Update :: WALI :: 10-Jul-2014
            //    cmd.Parameters["ZoneID"].Value = sit.ZoneID;
            //    if (!sit.IsSite_TelephoneNull()) cmd.Parameters["Telephone"].Value = sit.Site_Telephone;
            //    else cmd.Parameters["Telephone"].Value = DBNull.Value;
            //    if (!sit.IsSite_MobileNull()) cmd.Parameters["Mobile"].Value = sit.Site_Mobile;
            //    else cmd.Parameters["Mobile"].Value = DBNull.Value;
            //    if (!sit.IsSite_FaxNull()) cmd.Parameters["Fax"].Value = sit.Site_Fax;
            //    else cmd.Parameters["Fax"].Value = DBNull.Value;
            //    if (!sit.IsSite_EmailNull()) cmd.Parameters["Email"].Value = sit.Site_Email;
            //    else cmd.Parameters["Email"].Value = DBNull.Value;
            //    #endregion

            //    #region Payroll Site :: Rony :: 05-Apr-2015
            //    cmd.Parameters["PayrollSiteID"].Value = payrollSiteID;
            //    #endregion

            //    cmd.Parameters["IsMain_HeadOffice"].Value = sit.IsMain_HeadOffice;
            //    cmd.Parameters["IsZonal_HeadOffice"].Value = sit.IsZonal_HeadOffice;
            //    cmd.Parameters["IsRegional_HeadOffice"].Value = sit.IsRegional_HeadOffice;

            //    //cmd.Parameters.AddWithValue("p_IsMain_HeadOffice", sit.IsMain_HeadOffice);
            //    //cmd.Parameters.AddWithValue("p_IsZonal_HeadOffice", sit.IsZonal_HeadOffice);
            //    //cmd.Parameters.AddWithValue("p_IsRegional_HeadOffice", sit.IsRegional_HeadOffice);

            //    bool bError = false;
            //    int nRowAffected = -1;
            //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            //    if (bError == true)
            //    {
            //        ErrorDS.Error err = errDS.Errors.NewError();
            //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //        err.ErrorInfo1 = ActionID.ACTION_SITE_ADD.ToString();
            //        errDS.Errors.AddError(err);
            //        errDS.AcceptChanges();
            //        return errDS;
            //    }
            //}
            #endregion

            #region Updated :: RONY :: 18 May 2016
            foreach (SiteDS.Site sit in sitDS.Sites)
            {
                payrollSiteID = UtilDL.GetPayrollSiteId(connDS, sit.PayrollSiteCode);

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_SiteID", (object)genPK);

                if (sit.IsCodeNull() == false) cmd.Parameters.AddWithValue("p_Code", sit.Code);
                else cmd.Parameters.AddWithValue("p_Code", DBNull.Value);
                //name
                if (sit.IsNameNull() == false) cmd.Parameters.AddWithValue("p_Name", sit.Name);
                else cmd.Parameters.AddWithValue("p_Name", DBNull.Value);
                //Address
                if (sit.IsAddressNull() == false) cmd.Parameters.AddWithValue("p_Address", sit.Address);
                else cmd.Parameters.AddWithValue("p_Address", DBNull.Value);

                if (!sit.IsKPIGroupID_SNull()) cmd.Parameters.AddWithValue("p_KPIGroupID_S", sit.KPIGroupID_S);
                else cmd.Parameters.AddWithValue("p_KPIGroupID_S", DBNull.Value);  //cmd.Parameters["KPIGroupID_S"].Value = DBNull.Value;


                #region Update :: WALI :: 10-Jul-2014
                cmd.Parameters.AddWithValue("p_ZoneID", sit.ZoneID);
                if (!sit.IsSite_TelephoneNull()) cmd.Parameters.AddWithValue("p_Telephone", sit.Site_Telephone);//cmd.Parameters["Telephone"].Value = sit.Site_Telephone;
                else cmd.Parameters.AddWithValue("p_Telephone", DBNull.Value);
                if (!sit.IsSite_MobileNull()) cmd.Parameters.AddWithValue("p_Mobile", sit.Site_Mobile); //cmd.Parameters["Mobile"].Value = sit.Site_Mobile;
                else cmd.Parameters.AddWithValue("p_Mobile", DBNull.Value);
                if (!sit.IsSite_FaxNull()) cmd.Parameters.AddWithValue("p_Fax", sit.Site_Fax); //cmd.Parameters["Fax"].Value = sit.Site_Fax;
                else cmd.Parameters.AddWithValue("p_Fax", DBNull.Value);
                if (!sit.IsSite_EmailNull()) cmd.Parameters.AddWithValue("p_Email", sit.Site_Email); //cmd.Parameters["Email"].Value = sit.Site_Email;
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);
                #endregion

                #region Payroll Site :: Rony :: 05-Apr-2015
                cmd.Parameters.AddWithValue("p_PayrollSiteID", payrollSiteID);
                #endregion

                #region Head Office :: Rony :: 16-May-2016
                cmd.Parameters.AddWithValue("p_IsMain_HeadOffice", sit.IsMain_HeadOffice);
                cmd.Parameters.AddWithValue("p_IsZonal_HeadOffice", sit.IsZonal_HeadOffice);
                cmd.Parameters.AddWithValue("p_IsRegional_HeadOffice", sit.IsRegional_HeadOffice);
                #endregion

                #region Updated by :: Rony :: 14 Jun 2016
                if (!sit.IsSiteName_NativeLangNull()) cmd.Parameters.AddWithValue("p_Name_NativeLang", sit.SiteName_NativeLang);
                else cmd.Parameters.AddWithValue("p_Name_NativeLang", DBNull.Value);

                if (!sit.IsCountryIDNull()) cmd.Parameters.AddWithValue("p_Site_CountryID", sit.CountryID);
                else cmd.Parameters.AddWithValue("p_Site_CountryID", DBNull.Value);
                if (!sit.IsDivisionIDNull()) cmd.Parameters.AddWithValue("p_Site_DivisionID", sit.DivisionID);
                else cmd.Parameters.AddWithValue("p_Site_DivisionID", DBNull.Value);
                if (!sit.IsDistrictIDNull()) cmd.Parameters.AddWithValue("p_Site_DistrictID", sit.DistrictID);
                else cmd.Parameters.AddWithValue("p_Site_DistrictID", DBNull.Value);
                if (!sit.IsThanaIDNull()) cmd.Parameters.AddWithValue("p_Site_ThanaID", sit.ThanaID);
                else cmd.Parameters.AddWithValue("p_Site_ThanaID", DBNull.Value);

                if (!sit.IsOpeningDateNull()) cmd.Parameters.AddWithValue("p_OpeningDate", sit.OpeningDate);
                else cmd.Parameters.AddWithValue("p_OpeningDate", DBNull.Value);
                if (!sit.IsClosingDateNull()) cmd.Parameters.AddWithValue("p_ClosingDate", sit.ClosingDate);
                else cmd.Parameters.AddWithValue("p_ClosingDate", DBNull.Value);
                #endregion

                cmd.Parameters.AddWithValue("p_DefaultSelection", sit.DefaultSelection);

                if (!sit.IsSiteCategoryIDNull()) cmd.Parameters.AddWithValue("p_SiteCategoryID", sit.SiteCategoryID);
                else cmd.Parameters.AddWithValue("p_SiteCategoryID", DBNull.Value);

                cmd.Parameters.AddWithValue("p_CorporateBranch", sit.CorporateBranch);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SITE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteSite(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteSite");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SITE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;  // return empty ErrorDS
        }
        private DataSet _updateSite(DataSet inputDS)
        {
            //Update :: Rony :: 06-Apr-2015
            //OLD :: CommandXML

            ErrorDS errDS = new ErrorDS();
            SiteDS sitDS = new SiteDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            sitDS.Merge(inputDS.Tables[sitDS.Sites.TableName], false, MissingSchemaAction.Error);
            sitDS.AcceptChanges();


            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_SITE";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (SiteDS.Site sit in sitDS.Sites)
            {
                if (sit.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_SITE_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SITE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();


            long payrollSiteID = 0;
            foreach (SiteDS.Site sit in sitDS.Sites)
            {
                payrollSiteID = UtilDL.GetPayrollSiteId(connDS, sit.PayrollSiteCode);

                if (sit.IsNameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SiteName", (object)sit.Name);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SiteName", DBNull.Value);
                }

                if (!sit.IsSiteName_NativeLangNull()) cmd.Parameters.AddWithValue("p_Name_NativeLang", sit.SiteName_NativeLang);
                else cmd.Parameters.AddWithValue("p_Name_NativeLang", DBNull.Value);

                //Address
                if (sit.IsAddressNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Address", (object)sit.Address);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Address", DBNull.Value);
                }
                if (!sit.IsKPIGroupID_SNull())
                {
                    cmd.Parameters.AddWithValue("p_KPIGroupID_S", (object)sit.KPIGroupID_S);
                }
                else cmd.Parameters.AddWithValue("p_KPIGroupID_S", DBNull.Value);

                #region Update :: WALI :: 10-Jul-2014
                cmd.Parameters.AddWithValue("p_ZoneID", (object)sit.ZoneID);
                if (!sit.IsSite_TelephoneNull()) cmd.Parameters.AddWithValue("p_Site_Telephone", (object)sit.Site_Telephone);
                else cmd.Parameters.AddWithValue("p_Site_Telephone", DBNull.Value);
                if (!sit.IsSite_MobileNull()) cmd.Parameters.AddWithValue("p_Site_Mobile", (object)sit.Site_Mobile);
                else cmd.Parameters.AddWithValue("p_Site_Mobile", DBNull.Value);
                if (!sit.IsSite_FaxNull()) cmd.Parameters.AddWithValue("p_Site_Fax", (object)sit.Site_Fax);
                else cmd.Parameters.AddWithValue("p_Site_Fax", DBNull.Value);
                if (!sit.IsSite_EmailNull()) cmd.Parameters.AddWithValue("p_Site_Email", (object)sit.Site_Email);
                else cmd.Parameters.AddWithValue("p_Site_Email", DBNull.Value);
                #endregion

                #region Payroll Site :: Rony :: 05-Apr-2015
                cmd.Parameters.AddWithValue("p_PayrollSiteID", payrollSiteID);

                if (!sit.IsCodeNull()) cmd.Parameters.AddWithValue("p_SiteCode", (object)sit.Code);
                else cmd.Parameters.AddWithValue("p_SiteCode", DBNull.Value);

                if (!sit.IsPayrollSiteCode_OldNull()) cmd.Parameters.AddWithValue("p_PayrollSiteCode_Old", (object)sit.PayrollSiteCode_Old);
                else cmd.Parameters.AddWithValue("p_PayrollSiteCode_Old", DBNull.Value);

                #endregion

                #region Head Office :: Rony :: 16-May-2016
                cmd.Parameters.AddWithValue("p_IsMain_HeadOffice", sit.IsMain_HeadOffice);
                cmd.Parameters.AddWithValue("p_IsZonal_HeadOffice", sit.IsZonal_HeadOffice);
                cmd.Parameters.AddWithValue("p_IsRegional_HeadOffice", sit.IsRegional_HeadOffice);
                #endregion

                #region Updated by :: Rony :: 14 Jun 2016
                if (!sit.IsCountryIDNull()) cmd.Parameters.AddWithValue("p_Site_CountryID", sit.CountryID);
                else cmd.Parameters.AddWithValue("p_Site_CountryID", DBNull.Value);
                if (!sit.IsDivisionIDNull()) cmd.Parameters.AddWithValue("p_Site_DivisionID", sit.DivisionID);
                else cmd.Parameters.AddWithValue("p_Site_DivisionID", DBNull.Value);
                if (!sit.IsDistrictIDNull()) cmd.Parameters.AddWithValue("p_Site_DistrictID", sit.DistrictID);
                else cmd.Parameters.AddWithValue("p_Site_DistrictID", DBNull.Value);
                if (!sit.IsThanaIDNull()) cmd.Parameters.AddWithValue("p_Site_ThanaID", sit.ThanaID);
                else cmd.Parameters.AddWithValue("p_Site_ThanaID", DBNull.Value);

                if (!sit.IsOpeningDateNull()) cmd.Parameters.AddWithValue("p_OpeningDate", sit.OpeningDate);
                else cmd.Parameters.AddWithValue("p_OpeningDate", DBNull.Value);
                if (!sit.IsClosingDateNull()) cmd.Parameters.AddWithValue("p_ClosingDate", sit.ClosingDate);
                else cmd.Parameters.AddWithValue("p_ClosingDate", DBNull.Value);
                #endregion

                if (!sit.IsModifyEmployeeNull()) cmd.Parameters.AddWithValue("p_ModifyAllEmployee", sit.ModifyEmployee);
                else cmd.Parameters.AddWithValue("p_ModifyAllEmployee", DBNull.Value);

                cmd.Parameters.AddWithValue("p_DefaultSelection", sit.DefaultSelection);
                if (!sit.IsSiteCategoryIDNull()) cmd.Parameters.AddWithValue("p_SiteCategoryID", sit.SiteCategoryID);
                else cmd.Parameters.AddWithValue("p_SiteCategoryID", DBNull.Value);

                cmd.Parameters.AddWithValue("p_CorporateBranch", sit.CorporateBranch);

                if (!sit.IsEffectDateNull()) cmd.Parameters.AddWithValue("p_EffectDate", sit.EffectDate);
                else cmd.Parameters.AddWithValue("p_EffectDate", DBNull.Value);

                if (!sit.IsLast_Update_UserNull()) cmd.Parameters.AddWithValue("p_Last_Update_User", sit.Last_Update_User);
                else cmd.Parameters.AddWithValue("p_Last_Update_User", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SITE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateSiteCCMailUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            SiteDS sitDS = new SiteDS();
            sitDS.Merge(inputDS.Tables[sitDS.Sites.TableName], false, MissingSchemaAction.Error);
            sitDS.AcceptChanges();

            OleDbCommand cmdUpdate = new OleDbCommand();
            cmdUpdate.CommandText = "PRO_SITE_CCMAIL_USER_UPDATE";
            cmdUpdate.CommandType = CommandType.StoredProcedure;

            if (cmdUpdate == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Update Site CC Mail User...
            bool bError = false;
            int nRowAffected = -1;
            foreach (SiteDS.Site row in sitDS.Sites)
            {
                cmdUpdate.Parameters.AddWithValue("p_SiteID", row.SiteID);
                if (row.EmployeeID_CCMail != 0)
                    cmdUpdate.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID_CCMail);
                else cmdUpdate.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);
                cmdUpdate.Parameters.AddWithValue("p_DayForCCMail", row.DayForCCMail);
                cmdUpdate.Parameters.AddWithValue("p_CCMailMode", row.CCMailMode);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdate, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SITE_CCMAIL_USER_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                cmdUpdate.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getZoneList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SiteDS sitDS = new SiteDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetZoneList");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sitDS, sitDS.Zone.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ZONE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            sitDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(sitDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createZone(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            SiteDS siteDS = new SiteDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            siteDS.Merge(inputDS.Tables[siteDS.Zone.TableName], false, MissingSchemaAction.Error);
            siteDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ZONE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (SiteDS.ZoneRow row in siteDS.Zone.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("ZoneId", genPK);

                cmd.Parameters.AddWithValue("p_ZoneCode", row.ZoneCode);
                cmd.Parameters.AddWithValue("p_ZoneName", row.ZoneName);

                if (!row.IsDescriptionNull()) cmd.Parameters.AddWithValue("p_Description", row.Description);
                else cmd.Parameters.AddWithValue("p_Description", DBNull.Value);

                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("p_LocationID", row.LocationID);

                cmd.Parameters.AddWithValue("p_DefaultSelection", row.DefaultSelection);

                if (!row.IsZoneName_NativeLangNull()) cmd.Parameters.AddWithValue("p_ZoneName_NativeLang", row.ZoneName_NativeLang);
                else cmd.Parameters.AddWithValue("p_ZoneName_NativeLang", DBNull.Value);
                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ZONE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.ZoneCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.ZoneCode);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateZone(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SiteDS sitDS = new SiteDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            //extract Data
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            sitDS.Merge(inputDS.Tables[sitDS.Zone.TableName], false, MissingSchemaAction.Error);
            sitDS.AcceptChanges();


            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_ZONE";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (SiteDS.ZoneRow row in sitDS.Zone.Rows)
            {
                if (row.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ZONE_UPDATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ZONE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (SiteDS.ZoneRow row in sitDS.Zone.Rows)
            {
                cmd.Parameters.AddWithValue("p_ZoneName", row.ZoneName);
                cmd.Parameters.AddWithValue("p_Description", row.Description);
                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("p_LocationID", row.LocationID);
                cmd.Parameters.AddWithValue("p_ZoneCode", row.ZoneCode);
                cmd.Parameters.AddWithValue("p_DefaultSelection", row.DefaultSelection);
                if (!row.IsZoneName_NativeLangNull()) cmd.Parameters.AddWithValue("p_ZoneName_NativeLang", row.ZoneName_NativeLang);
                else cmd.Parameters.AddWithValue("p_ZoneName_NativeLang", DBNull.Value);
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ZONE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.ZoneCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.ZoneCode);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _deleteZone(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SiteDS sitDS = new SiteDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            //extract Data
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            sitDS.Merge(inputDS.Tables[sitDS.Zone.TableName], false, MissingSchemaAction.Error);
            sitDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ZONE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (SiteDS.ZoneRow row in sitDS.Zone.Rows)
            {
                cmd.Parameters.AddWithValue("p_ZoneCode", row.ZoneCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ZONE_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.ZoneCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.ZoneCode);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _getSiteList_ByFilter(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataIntegerDS intDS = new DataIntegerDS();
            //DataDateDS dateDS = new DataDateDS();
            SiteDS sitDS = new SiteDS();
            DataSet returnDS = new DataSet();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            intDS.Merge(inputDS.Tables[intDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            intDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetSiteList_ByFilter");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["OpeningDate"].Value = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["ClosingDate"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["ZoneID1"].Value = intDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["ZoneID2"].Value = intDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollsiteID1"].Value = intDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["PayrollsiteID2"].Value = intDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["CountryID1"].Value = intDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["CountryID2"].Value = intDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["DivisionID1"].Value = intDS.DataIntegers[3].IntegerValue;
            cmd.Parameters["DivisionID2"].Value = intDS.DataIntegers[3].IntegerValue;
            cmd.Parameters["DistrictID1"].Value = intDS.DataIntegers[4].IntegerValue;
            cmd.Parameters["DistrictID2"].Value = intDS.DataIntegers[4].IntegerValue;
            cmd.Parameters["ThanaID1"].Value = intDS.DataIntegers[5].IntegerValue;
            cmd.Parameters["ThanaID2"].Value = intDS.DataIntegers[5].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sitDS, sitDS.Sites.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SITE_LIST_BYFILTER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            sitDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(sitDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        #region Site Category :: Hasinul :: 1-Nov-2017
        private DataSet _createSiteCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SiteDS sitDS = new SiteDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SITE_CATEGORY_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            sitDS.Merge(inputDS.Tables[sitDS.SiteCategory.TableName], false, MissingSchemaAction.Error);
            sitDS.AcceptChanges();

            foreach (SiteDS.SiteCategoryRow row in sitDS.SiteCategory)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_SiteCategoryId", genPK);
                cmd.Parameters.AddWithValue("p_SiteCategoryName", row.SiteCategoryName);
                if (!row.IsSiteCategoryDetailsNull()) cmd.Parameters.AddWithValue("p_SiteCategoryDetails", row.SiteCategoryDetails);
                else cmd.Parameters.AddWithValue("p_SiteCategoryDetails", DBNull.Value);
                cmd.Parameters.AddWithValue("p_SiteCat_Serial", row.SiteCat_Serial);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_Site_Category_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getSiteCategory(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            SiteDS sitDS = new SiteDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSiteCategoryList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sitDS, sitDS.SiteCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_Site_Category_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            sitDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(sitDS.SiteCategory);
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _deleteSiteCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            SiteDS sitDS = new SiteDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            sitDS.Merge(inputDS.Tables[sitDS.SiteCategory.TableName], false, MissingSchemaAction.Error);
            sitDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SITE_CATEGORY_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            foreach (SiteDS.SiteCategoryRow row in sitDS.SiteCategory.Rows)
            {
                cmd.Parameters.AddWithValue("p_SiteCategoryId", row.SiteCategoryID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_Site_Category_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.SiteCategoryName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.SiteCategoryName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateSiteCategory(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            SiteDS sitDS = new SiteDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SITE_CATEGORY_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            sitDS.Merge(inputDS.Tables[sitDS.SiteCategory.TableName], false, MissingSchemaAction.Error);
            sitDS.AcceptChanges();

            foreach (SiteDS.SiteCategoryRow row in sitDS.SiteCategory.Rows)
            {
                cmd.Parameters.AddWithValue("p_SiteCategoryName", row.SiteCategoryName);

                if (!row.IsSiteCategoryDetailsNull()) cmd.Parameters.AddWithValue("p_SiteCategoryDetails", row.SiteCategoryDetails);
                else cmd.Parameters.AddWithValue("p_SiteCategoryDetails", DBNull.Value);

                cmd.Parameters.AddWithValue("p_SiteCategoryId", row.SiteCategoryID);
                cmd.Parameters.AddWithValue("p_SiteCat_Serial", row.SiteCat_Serial);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_Site_Category_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
            }
            returnDS.Merge(errDS);
            return returnDS;
        }
        #endregion
        private DataSet _getSiteByCompany(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            SiteDS sitDS = new SiteDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSiteByCompany");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, sitDS, sitDS.Sites.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SITE_BY_COMPANY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            sitDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(sitDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}
