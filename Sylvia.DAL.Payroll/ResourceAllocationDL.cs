/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: March 27, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class ResourceAllocationDL
    {
        public ResourceAllocationDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_SERVICE_TYPE_CREATE:
                    return _createServiceType(inputDS);
                case ActionID.ACTION_SERVICE_TYPE_UPDATE:
                    return this._updateServiceType(inputDS);
                case ActionID.NA_ACTION_GET_SERVICE_TYPE_LIST:
                    return _getServiceTypeList(inputDS);
                case ActionID.ACTION_SERVICE_TYPE_DEL:
                    return this._deleteServiceType(inputDS);

                case ActionID.ACTION_CHECKLIST_CREATE:
                    return _createCheckList(inputDS);
                case ActionID.ACTION_CHECKLIST_UPDATE:
                    return this._updateCheckList(inputDS);
                case ActionID.NA_ACTION_GET_CHECKLIST_LIST:
                    return _getCheckList(inputDS);
                case ActionID.ACTION_CHECKLIST_DEL:
                    return this._deleteCheckList(inputDS);

                case ActionID.ACTION_TASK_CREATE:
                    return _createTaskCategory(inputDS);
                case ActionID.ACTION_TASK_UPDATE:
                    return this._updateTaskCategory(inputDS);
                case ActionID.NA_ACTION_GET_TASK_LIST:
                    return _getTaskCategoryList(inputDS);
                case ActionID.ACTION_TASK_DEL:
                    return this._deleteTaskCategory(inputDS);


                case ActionID.ACTION_TASKTYPE_ADD:
                    return _createTaskType(inputDS);
                case ActionID.ACTION_TASKTYPE_UPD:
                    return this._updateTaskType(inputDS);
                case ActionID.NA_ACTION_GET_TASK_TYPE_ALL_LIST:
                    return _getAllTaskTypeList(inputDS);
                case ActionID.NA_ACTION_GET_TASK_CHECK_LIST:
                    return _getTaskCheck(inputDS);
                case ActionID.ACTION_TASKTYPE_DEL:
                    return this._deleteTaskType(inputDS);

                case ActionID.ACTION_VENDOR_ADD:
                    return _createVendor(inputDS);
                case ActionID.ACTION_VENDOR_UPD:
                    return this._updateVendor(inputDS);
                case ActionID.NA_ACTION_GET_VENDOR_ALL_LIST:
                    return _getAllVendorList(inputDS);
                case ActionID.NA_ACTION_GET_VENDOR_SERVICE_LIST:
                    return _getVendorTask(inputDS);
                case ActionID.ACTION_VENDOR_DEL:
                    return this._deleteVendor(inputDS);


                case ActionID.NA_ACTION_GET_TASK_LIST_OF_VENDOR:
                    return _getTaskListOfVendor(inputDS);
                case ActionID.NA_ACTION_GET_ALLOCATION_ACTIVITIES:
                    return _getAllocationActivities(inputDS);
                case ActionID.ACTION_ALLOCATION_REQUEST_ADD:
                    return this._createAllocationRequest(inputDS);
                case ActionID.ACTION_ALLOCATION_REQUEST_EXECUTE:
                    return this._executeAllocationTask(inputDS);
                case ActionID.NA_ACTION_GET_ALLOCATION_FOR_EXECUTION:
                    return this._getAllocationForExecution(inputDS);
                case ActionID.NA_ACTION_GET_ASSIGNED_JOB_STATUS:
                    return this._getAssignedJobStatus(inputDS);
                case ActionID.ACTION_ASSIGNED_JOB_STATUS_UPDATE:
                    return this._updateAssignedJobStatus(inputDS);
                case ActionID.NA_ACTION_GET_TASK_FILTER_ALL:
                    return this._get_FilteredTask_All(inputDS);

                case ActionID.NA_ACTION_GET_ALLOCATION_FOR_EXECUTION_ALL_INFO:
                    return this._getAllocateForExecutionAllInfo(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        #region Service Type :: Rony :: 09-Oct-2014
        private DataSet _getServiceTypeList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS serviceDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getServiceTypeList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, serviceDS, serviceDS.ServiceType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SERVICE_TYPE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            serviceDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(serviceDS.ServiceType);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createServiceType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS serviceDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SERVICE_TYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            serviceDS.Merge(inputDS.Tables[serviceDS.ServiceType.TableName], false, MissingSchemaAction.Error);
            serviceDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (ResourceAllocationDS.ServiceTypeRow row in serviceDS.ServiceType.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_ServiceTypeID", (object)genPK);
                }
                cmd.Parameters.AddWithValue("@p_ServiceType", row.ServiceType);

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("@p_IsActive", row.IsActive);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SERVICE_TYPE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(serviceDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateServiceType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS serviceDS = new ResourceAllocationDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SERVICE_TYPE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            serviceDS.Merge(inputDS.Tables[serviceDS.ServiceType.TableName], false, MissingSchemaAction.Error);
            serviceDS.AcceptChanges();

            foreach (ResourceAllocationDS.ServiceTypeRow row in serviceDS.ServiceType.Rows)
            {
                cmd.Parameters.AddWithValue("p_ServiceType", row.ServiceType);

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("p_ServiceTypeID", row.ServiceTypeID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SERVICE_TYPE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteServiceType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SERVICE_TYPE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_ServiceType", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SERVICE_TYPE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;  // return empty ErrorDS
        }
        #endregion

        private DataSet _getCheckList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS checkDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getCheckList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, checkDS, checkDS.CheckList.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_CHECKLIST_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            checkDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(checkDS.CheckList);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createCheckList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS checkDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_CHECK_LIST_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            checkDS.Merge(inputDS.Tables[checkDS.CheckList.TableName], false, MissingSchemaAction.Error);
            checkDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (ResourceAllocationDS.CheckListRow row in checkDS.CheckList.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_CheckListID", (object)genPK);
                }
                cmd.Parameters.AddWithValue("@p_CheckListName", row.CheckListName);

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("@p_IsActive", row.IsActive);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHECKLIST_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(checkDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateCheckList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS checkDS = new ResourceAllocationDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_CHECK_LIST_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            checkDS.Merge(inputDS.Tables[checkDS.CheckList.TableName], false, MissingSchemaAction.Error);
            checkDS.AcceptChanges();

            foreach (ResourceAllocationDS.CheckListRow row in checkDS.CheckList.Rows)
            {
                cmd.Parameters.AddWithValue("p_CheckListName", row.CheckListName);

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("p_CheckListID", row.CheckListID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHECKLIST_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteCheckList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_CHECK_LIST_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_CheckListName", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_CHECKLIST_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;  // return empty ErrorDS
        }

        private DataSet _getTaskCategoryList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS taskDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getTaskCategoryList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, taskDS, taskDS.TaskCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TASK_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            taskDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(taskDS.TaskCategory);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createTaskCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS taskDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TASK_CATEGORY_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            taskDS.Merge(inputDS.Tables[taskDS.TaskCategory.TableName], false, MissingSchemaAction.Error);
            taskDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (ResourceAllocationDS.TaskCategoryRow row in taskDS.TaskCategory.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_TaskCategoryID", (object)genPK);
                }
                cmd.Parameters.AddWithValue("@p_TaskName", row.TaskName);

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("@p_IsActive", row.IsActive);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TASK_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(taskDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateTaskCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS taskDS = new ResourceAllocationDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TASK_CATEGORY_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            taskDS.Merge(inputDS.Tables[taskDS.TaskCategory.TableName], false, MissingSchemaAction.Error);
            taskDS.AcceptChanges();

            foreach (ResourceAllocationDS.TaskCategoryRow row in taskDS.TaskCategory.Rows)
            {
                cmd.Parameters.AddWithValue("p_TaskName", row.TaskName);

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("p_TaskCategoryID", row.TaskCategoryID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TASK_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteTaskCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TASK_CATEGORY_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_TaskName", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_TASK_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;  // return empty ErrorDS
        }

        private DataSet _createTaskType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmdTask = new OleDbCommand();
            cmdTask.CommandText = "PRO_TASKTYPE_CREATE";
            cmdTask.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmdTaskCheck = new OleDbCommand();
            cmdTaskCheck.CommandText = "PRO_TASK_CHECKLIST_CREATE";
            cmdTaskCheck.CommandType = CommandType.StoredProcedure;

            if (cmdTask == null || cmdTaskCheck == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            ResourceAllocationDS taskDS = new ResourceAllocationDS();
            taskDS.Merge(inputDS.Tables[taskDS.TaskType.TableName], false, MissingSchemaAction.Error);
            taskDS.AcceptChanges();
            taskDS.Merge(inputDS.Tables[taskDS.TaskCheckList.TableName], false, MissingSchemaAction.Error);
            taskDS.AcceptChanges();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Cteate Task Type..
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmdTask.Parameters.AddWithValue("p_TaskTypeID", genPK);
            cmdTask.Parameters.AddWithValue("p_TaskType", taskDS.TaskType[0].TaskType);
            cmdTask.Parameters.AddWithValue("p_TaskCategoryID", taskDS.TaskType[0].TaskCategoryID);
            if (!taskDS.TaskType[0].IsRemarksNull()) cmdTask.Parameters.AddWithValue("@p_Remarks", taskDS.TaskType[0].Remarks);
            else cmdTask.Parameters.AddWithValue("@p_Remarks", DBNull.Value);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdTask, connDS.DBConnections[0].ConnectionID,
                ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_TASKTYPE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(taskDS.TaskType[0].TaskType);
            else messageDS.ErrorMsg.AddErrorMsgRow(taskDS.TaskType[0].TaskType);
            messageDS.AcceptChanges();
            #endregion

            #region Create Task Check List...
            long genTaskCheckPK;
            foreach (ResourceAllocationDS.TaskCheckListRow row in taskDS.TaskCheckList.Rows)
            {
                genTaskCheckPK = IDGenerator.GetNextGenericPK();

                cmdTaskCheck.Parameters.AddWithValue("p_CheckListID", row.CheckListID);
                cmdTaskCheck.Parameters.AddWithValue("p_TaskTypeID", genPK);
                cmdTaskCheck.Parameters.AddWithValue("p_CheckListName", row.CheckListName);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdTaskCheck, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdTaskCheck.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TASKTYPE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateTaskType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            ResourceAllocationDS taskDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();

            taskDS.Merge(inputDS.Tables[taskDS.TaskType.TableName], false, MissingSchemaAction.Error);
            taskDS.AcceptChanges();
            taskDS.Merge(inputDS.Tables[taskDS.TaskCheckList.TableName], false, MissingSchemaAction.Error);
            taskDS.AcceptChanges();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Task Update
            OleDbCommand cmdTask = new OleDbCommand();
            cmdTask.CommandText = "PRO_TASKTYPE_UPDATE";
            cmdTask.CommandType = CommandType.StoredProcedure;

            if (cmdTask == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }
            foreach (ResourceAllocationDS.TaskTypeRow row in taskDS.TaskType.Rows)
            {
                cmdTask.Parameters.AddWithValue("p_TaskType", row.TaskType);
                cmdTask.Parameters.AddWithValue("p_TaskCategoryID", row.TaskCategoryID);
                if (!row.IsRemarksNull()) cmdTask.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                else cmdTask.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                cmdTask.Parameters.AddWithValue("p_TaskTypeID", row.TaskTypeID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdTask, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdTask.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TASKTYPE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region First Delete Old Task CheckList Records
            OleDbCommand cmdDel = new OleDbCommand();
            cmdDel.CommandText = "PRO_TASK_CHECKLIST_DELETE";
            cmdDel.CommandType = CommandType.StoredProcedure;

            cmdDel.Parameters.AddWithValue("p_TaskTypeID", taskDS.TaskType[0].TaskTypeID);

            if (cmdDel == null) return UtilDL.GetCommandNotFound();

            bool bErrorDel = false;
            int nRowAffectedDel = -1;
            nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);
            cmdDel.Parameters.Clear();
            if (bErrorDel)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_TASKTYPE_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            if (nRowAffectedDel > 0) messageDS.SuccessMsg.AddSuccessMsgRow(taskDS.TaskType[0].TaskType);
            else messageDS.ErrorMsg.AddErrorMsgRow(taskDS.TaskType[0].TaskType);
            messageDS.AcceptChanges();

            #endregion

            #region Then Create New Task CheckList
            if (taskDS.TaskType.Rows.Count > 0)
            {
                OleDbCommand cmdCheck = new OleDbCommand();
                cmdCheck.CommandText = "PRO_TASK_CHECKLIST_CREATE";
                cmdCheck.CommandType = CommandType.StoredProcedure;

                if (cmdCheck == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                long genCheckPK;
                foreach (ResourceAllocationDS.TaskCheckListRow row in taskDS.TaskCheckList.Rows)
                {
                    genCheckPK = IDGenerator.GetNextGenericPK();

                    cmdCheck.Parameters.AddWithValue("p_CheckListID", row.CheckListID);
                    cmdCheck.Parameters.AddWithValue("p_TaskTypeID", row.TaskTypeID);
                    cmdCheck.Parameters.AddWithValue("p_CheckListName", row.CheckListName);

                    bool bError2 = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdCheck, connDS.DBConnections[0].ConnectionID, ref bError2);
                    cmdCheck.Parameters.Clear();
                    if (bError2)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_TASKTYPE_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteTaskType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            ResourceAllocationDS taskDS = new ResourceAllocationDS();

            taskDS.Merge(inputDS.Tables[taskDS.TaskType.TableName], false, MissingSchemaAction.Error);
            taskDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TASKTYPE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (ResourceAllocationDS.TaskTypeRow row in taskDS.TaskType.Rows)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("p_TaskTypeID", row.TaskTypeID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID,
                    ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TASKTYPE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.TaskType);
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.TaskType);
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAllTaskTypeList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS taskDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllTaskTypeList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, taskDS, taskDS.TaskType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TASK_TYPE_ALL_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            taskDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(taskDS.TaskType);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getTaskCheck(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS taskCheckDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetTaskCheckListAll");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, taskCheckDS, taskCheckDS.TaskCheckList.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TASK_CHECK_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            taskCheckDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(taskCheckDS.TaskCheckList);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createVendor(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmdVendor = new OleDbCommand();
            cmdVendor.CommandText = "PRO_VENDOR_CREATE";
            cmdVendor.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmdVenTask = new OleDbCommand();
            cmdVenTask.CommandText = "PRO_VENDOR_TASK_CREATE";
            cmdVenTask.CommandType = CommandType.StoredProcedure;

            if (cmdVendor == null || cmdVenTask == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            ResourceAllocationDS vendorDS = new ResourceAllocationDS();
            vendorDS.Merge(inputDS.Tables[vendorDS.Vendor.TableName], false, MissingSchemaAction.Error);
            vendorDS.AcceptChanges();
            vendorDS.Merge(inputDS.Tables[vendorDS.VendorTaskType.TableName], false, MissingSchemaAction.Error);
            vendorDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Cteate  Vendor..
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            cmdVendor.Parameters.AddWithValue("p_VendorID", genPK);
            cmdVendor.Parameters.AddWithValue("p_VendorName", vendorDS.Vendor[0].VendorName);
            cmdVendor.Parameters.AddWithValue("p_ServiceTypeID", vendorDS.Vendor[0].ServiceTypeID);
            if (!vendorDS.Vendor[0].IsRemarksNull()) cmdVendor.Parameters.AddWithValue("@p_Remarks", vendorDS.Vendor[0].Remarks);
            else cmdVendor.Parameters.AddWithValue("@p_Remarks", DBNull.Value);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdVendor, connDS.DBConnections[0].ConnectionID,
                ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_VENDOR_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(vendorDS.Vendor[0].VendorName);
            else messageDS.ErrorMsg.AddErrorMsgRow(vendorDS.Vendor[0].VendorName);
            messageDS.AcceptChanges();
            #endregion

            #region Create Vendor Task Type ...
            long genTaskCheckPK;
            foreach (ResourceAllocationDS.VendorTaskTypeRow row in vendorDS.VendorTaskType.Rows)
            {
                genTaskCheckPK = IDGenerator.GetNextGenericPK();
                cmdVenTask.Parameters.AddWithValue("p_VendorTaskID", genTaskCheckPK);
                cmdVenTask.Parameters.AddWithValue("p_TaskTypeID", row.TaskTypeID);
                cmdVenTask.Parameters.AddWithValue("p_VendorID", genPK);


                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdVenTask, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdVenTask.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_VENDOR_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateVendor(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            ResourceAllocationDS vendorDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();

            vendorDS.Merge(inputDS.Tables[vendorDS.Vendor.TableName], false, MissingSchemaAction.Error);
            vendorDS.AcceptChanges();
            vendorDS.Merge(inputDS.Tables[vendorDS.VendorTaskType.TableName], false, MissingSchemaAction.Error);
            vendorDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Vendor Update
            OleDbCommand cmdVendor = new OleDbCommand();
            cmdVendor.CommandText = "PRO_VENDOR_UPDATE";
            cmdVendor.CommandType = CommandType.StoredProcedure;

            if (cmdVendor == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }
            foreach (ResourceAllocationDS.VendorRow row in vendorDS.Vendor.Rows)
            {
                cmdVendor.Parameters.AddWithValue("p_VendorName", row.VendorName);
                cmdVendor.Parameters.AddWithValue("p_ServiceTypeID", row.ServiceTypeID);
                if (!row.IsRemarksNull()) cmdVendor.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                else cmdVendor.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                cmdVendor.Parameters.AddWithValue("p_VendorID", row.VendorID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdVendor, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdVendor.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_VENDOR_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }
            #endregion

            #region First Delete Old Vendor Task type  Records
            OleDbCommand cmdDel = new OleDbCommand();
            cmdDel.CommandText = "PRO_VENDOR_TASK_DELETE";
            cmdDel.CommandType = CommandType.StoredProcedure;

            cmdDel.Parameters.AddWithValue("p_VendorID", vendorDS.Vendor[0].VendorID);

            if (cmdDel == null) return UtilDL.GetCommandNotFound();

            bool bErrorDel = false;
            int nRowAffectedDel = -1;
            nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);
            cmdDel.Parameters.Clear();
            if (bErrorDel)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_VENDOR_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            if (nRowAffectedDel > 0) messageDS.SuccessMsg.AddSuccessMsgRow(vendorDS.Vendor[0].VendorName);
            else messageDS.ErrorMsg.AddErrorMsgRow(vendorDS.Vendor[0].VendorName);
            messageDS.AcceptChanges();
            #endregion

            #region Then Create New Vendor Task type
            if (vendorDS.Vendor.Rows.Count > 0)
            {
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_VENDOR_TASK_CREATE";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                long genVendorTaskPK;
                foreach (ResourceAllocationDS.VendorTaskTypeRow row in vendorDS.VendorTaskType.Rows)
                {
                    genVendorTaskPK = IDGenerator.GetNextGenericPK();
                    cmd.Parameters.AddWithValue("p_VendorTaskID", genVendorTaskPK);
                    cmd.Parameters.AddWithValue("p_TaskTypeID", row.TaskTypeID);
                    cmd.Parameters.AddWithValue("p_VendorID", row.VendorID);

                    bool bError2 = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError2);
                    cmd.Parameters.Clear();
                    if (bError2)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_VENDOR_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteVendor(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            ResourceAllocationDS vendorDS = new ResourceAllocationDS();

            vendorDS.Merge(inputDS.Tables[vendorDS.Vendor.TableName], false, MissingSchemaAction.Error);
            vendorDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmdDelete = new OleDbCommand();
            cmdDelete.CommandText = "PRO_VENDOR_DELETE";
            cmdDelete.CommandType = CommandType.StoredProcedure;

            if (cmdDelete == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (ResourceAllocationDS.VendorRow row in vendorDS.Vendor.Rows)
            {
                cmdDelete.Parameters.Clear();
                cmdDelete.Parameters.AddWithValue("p_VendorID", row.VendorID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdDelete, connDS.DBConnections[0].ConnectionID,
                    ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_VENDOR_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.VendorName);
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.VendorName);
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAllVendorList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS vendorDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllVendorList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, vendorDS, vendorDS.Vendor.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_VENDOR_ALL_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            vendorDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(vendorDS.Vendor);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getVendorTask(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS vendorTaskDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetVendorTaskList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, vendorTaskDS, vendorTaskDS.VendorTaskType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_VENDOR_SERVICE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            vendorTaskDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(vendorTaskDS.VendorTaskType);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getTaskListOfVendor(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ResourceAllocationDS resDS = new ResourceAllocationDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetTaskListForVendor");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["VendorID1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["VendorID2"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, resDS, resDS.Vendor.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TASK_LIST_OF_VENDOR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            resDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(resDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createAllocationRequest(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ResourceAllocationDS reqDS = new ResourceAllocationDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            DataBoolDS boolDS = new DataBoolDS();
            DataStringDS stringDS = new DataStringDS();
            string messageBody = "", returnMessage = "", URL = "";
            bool bError = false;
            int nRowAffected = -1;
            #endregion

            #region Merge Data
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            reqDS.Merge(inputDS.Tables[reqDS.AllocationRequest.TableName], false, MissingSchemaAction.Error);
            reqDS.AcceptChanges();
            #endregion

            #region Get Email Information from UtilDL
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            #region Select Commands
            OleDbCommand cmd_Request = new OleDbCommand();
            cmd_Request.CommandText = "PRO_ALLOCATION_REQUEST_CREATE";
            cmd_Request.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Task = new OleDbCommand();
            cmd_Task.CommandText = "PRO_ALLOCATION_TASK_CREATE";
            cmd_Task.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Hist = new OleDbCommand();
            cmd_Hist.CommandText = "PRO_ALLOCATION_HIST_CREATE";
            cmd_Hist.CommandType = CommandType.StoredProcedure;

            if (cmd_Request == null || cmd_Task == null || cmd_Hist == null) return UtilDL.GetCommandNotFound();
            #endregion

            #region Create Allocation Request

            long genPK_Req = IDGenerator.GetNextGenericPK();
            if (genPK_Req == -1) return UtilDL.GetDBOperationFailed();

            cmd_Request.Parameters.AddWithValue("p_AllocationReqID", genPK_Req);
            cmd_Request.Parameters.AddWithValue("p_EntryDate", reqDS.AllocationRequest[0].AssignedDate);
            cmd_Request.Parameters.AddWithValue("p_AssignedBy", reqDS.AllocationRequest[0].AssignedBy);
            cmd_Request.Parameters.AddWithValue("p_VendorID", reqDS.AllocationRequest[0].VendorID);
            cmd_Request.Parameters.AddWithValue("p_ServiceTypeID", reqDS.AllocationRequest[0].ServiceTypeID);
            cmd_Request.Parameters.AddWithValue("p_TaskCategoryID", reqDS.AllocationRequest[0].TaskCategoryID);
            if (!reqDS.AllocationRequest[0].IsReceivableAmountNull()) cmd_Request.Parameters.AddWithValue("p_ReceivableAmount", reqDS.AllocationRequest[0].ReceivableAmount);
            else cmd_Request.Parameters.AddWithValue("p_ReceivableAmount", DBNull.Value);

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Request, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ALLOCATION_REQUEST_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
            }

            if (bError)
            {
                returnDS.Merge(errDS);
                return returnDS;
            }
            #endregion

            if (!boolDS.DataBools[0].BoolValue) //If NotifyByMail is  not Checked
            {
                #region Create Allocation Task  & History Entry
                foreach (ResourceAllocationDS.AllocationRequestRow row in reqDS.AllocationRequest.Rows)
                {
                    long genPK_Task = IDGenerator.GetNextGenericPK();
                    long genPK_Hist = IDGenerator.GetNextGenericPK();
                    if (genPK_Task == -1 || genPK_Hist == -1) return UtilDL.GetDBOperationFailed();

                    #region Add Allocation Task
                    cmd_Task.Parameters.AddWithValue("p_AllocationTaskID", genPK_Task);
                    cmd_Task.Parameters.AddWithValue("p_TaskTypeID", row.TaskTypeID);
                    cmd_Task.Parameters.AddWithValue("p_StartDate", row.StartDate);
                    cmd_Task.Parameters.AddWithValue("p_EndDate", row.EndDate);
                    cmd_Task.Parameters.AddWithValue("p_AssignDate", row.AssignedDate);
                    cmd_Task.Parameters.AddWithValue("p_Priority", row.Priority);
                    cmd_Task.Parameters.AddWithValue("p_AllocationReqID", genPK_Req);
                    cmd_Task.Parameters.AddWithValue("p_RealizedAmount", 0);
                    cmd_Task.Parameters.AddWithValue("p_Employeecode", row.EmployeeCode);
                    cmd_Task.Parameters.AddWithValue("p_IsTeamLeader", row.IsTeamLeader);


                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Task, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Task.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ALLOCATION_REQUEST_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.TaskType + " ==> " + row.EmployeeName);
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.TaskType + " ==> " + row.EmployeeName);

                    #endregion

                    #region Allocation Task History
                    cmd_Hist.Parameters.AddWithValue("p_AllocationHistoryID", genPK_Hist);
                    cmd_Hist.Parameters.AddWithValue("p_AllocationTaskID", genPK_Task);
                    cmd_Hist.Parameters.AddWithValue("p_SenderUserID", row.AssignedBy);
                    cmd_Hist.Parameters.AddWithValue("p_Employeecode", row.EmployeeCode);
                    cmd_Hist.Parameters.AddWithValue("p_ReceivedDate", row.EntryDate);

                    if (!row.IsAlloHistoryCommentsNull()) cmd_Hist.Parameters.AddWithValue("p_AlloHistoryComments", row.AlloHistoryComments);
                    else cmd_Hist.Parameters.AddWithValue("p_AlloHistoryComments", DBNull.Value);

                    cmd_Hist.Parameters.AddWithValue("p_AlloActivitiesID", row.AlloActivitiesID);
                    cmd_Hist.Parameters.AddWithValue("p_RealizedAmount", 0);
                    cmd_Hist.Parameters.AddWithValue("p_EndDate", row.EndDate);


                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Hist, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Hist.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ALLOCATION_REQUEST_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion
                }
                #endregion
            }
            else
            {
                #region If NotifyByMail is Checked....
                URL = stringDS.DataStrings[0].StringValue;
                foreach (ResourceAllocationDS.AllocationRequestRow row in reqDS.AllocationRequest.Rows)
                {
                    returnMessage = "";
                    string fromEmailAddress = "", emailAddress = "", ccEmailAddress = "", mailingSubject = "";

                    URL += "?pVal=HRAS";
                    emailAddress = row.EmployeeEmail;
                    fromEmailAddress = UtilDL.GetFromEMailAddress();

                    string[] splitedTask = row.TaskType.Split('(', '[');
                    mailingSubject = "Allocated Task is Pending now.";

                    messageBody += "<br>";
                    //messageBody += "<b> Dear </b>" + row.EmployeeName + ",";
                    messageBody += "<b> Dear Concern, </b>";
                    messageBody += "<br>";
                    messageBody += "<b> The following job has been allocated to you :- </b>";
                    messageBody += "<br>";
                    messageBody += "<b>Client : </b>" + row.VendorName;
                    messageBody += "<br>";
                    messageBody += "<b>Task : </b>" + splitedTask[0];
                    messageBody += "<br>";
                    messageBody += "<b>Year End : </b>" + row.TaskName;
                    messageBody += "<br>";
                    messageBody += "Click the following link to see the job.";
                    messageBody += "<br>";
                    messageBody += "<a href='" + URL + "'>" + URL + "</a>";

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, row.SenderUserName);
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, row.SenderUserName);
                    }
                    if (returnMessage == "Email successfully sent.")
                    {
                        #region Create Allocation Task  & History Entry
                        //foreach (ResourceAllocationDS.AllocationRequestRow row in reqDS.AllocationRequest.Rows)
                        //{
                            long genPK_Task = IDGenerator.GetNextGenericPK();
                            long genPK_Hist = IDGenerator.GetNextGenericPK();
                            if (genPK_Task == -1 || genPK_Hist == -1) return UtilDL.GetDBOperationFailed();

                            #region Add Allocation Task
                            cmd_Task.Parameters.AddWithValue("p_AllocationTaskID", genPK_Task);
                            cmd_Task.Parameters.AddWithValue("p_TaskTypeID", row.TaskTypeID);
                            cmd_Task.Parameters.AddWithValue("p_StartDate", row.StartDate);
                            cmd_Task.Parameters.AddWithValue("p_EndDate", row.EndDate);
                            cmd_Task.Parameters.AddWithValue("p_AssignDate", row.AssignedDate);
                            cmd_Task.Parameters.AddWithValue("p_Priority", row.Priority);
                            cmd_Task.Parameters.AddWithValue("p_AllocationReqID", genPK_Req);
                            cmd_Task.Parameters.AddWithValue("p_RealizedAmount", 0);
                            cmd_Task.Parameters.AddWithValue("p_Employeecode", row.EmployeeCode);
                            cmd_Task.Parameters.AddWithValue("p_IsTeamLeader", row.IsTeamLeader);


                            bError = false;
                            nRowAffected = -1;
                            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Task, connDS.DBConnections[0].ConnectionID, ref bError);
                            cmd_Task.Parameters.Clear();
                            if (bError == true)
                            {
                                ErrorDS.Error err = errDS.Errors.NewError();
                                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                                err.ErrorInfo1 = ActionID.ACTION_ALLOCATION_REQUEST_ADD.ToString();
                                errDS.Errors.AddError(err);
                                errDS.AcceptChanges();
                                return errDS;
                            }

                            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.TaskType + " ==> " + row.EmployeeName);
                            else messageDS.ErrorMsg.AddErrorMsgRow(row.TaskType + " ==> " + row.EmployeeName);

                            #endregion

                            #region Allocation Task History
                            cmd_Hist.Parameters.AddWithValue("p_AllocationHistoryID", genPK_Hist);
                            cmd_Hist.Parameters.AddWithValue("p_AllocationTaskID", genPK_Task);
                            cmd_Hist.Parameters.AddWithValue("p_SenderUserID", row.AssignedBy);
                            cmd_Hist.Parameters.AddWithValue("p_Employeecode", row.EmployeeCode);
                            cmd_Hist.Parameters.AddWithValue("p_ReceivedDate", row.EntryDate);

                            if (!row.IsAlloHistoryCommentsNull()) cmd_Hist.Parameters.AddWithValue("p_AlloHistoryComments", row.AlloHistoryComments);
                            else cmd_Hist.Parameters.AddWithValue("p_AlloHistoryComments", DBNull.Value);

                            cmd_Hist.Parameters.AddWithValue("p_AlloActivitiesID", row.AlloActivitiesID);
                            cmd_Hist.Parameters.AddWithValue("p_RealizedAmount", 0);
                            cmd_Hist.Parameters.AddWithValue("p_EndDate", row.EndDate);


                            bError = false;
                            nRowAffected = -1;
                            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Hist, connDS.DBConnections[0].ConnectionID, ref bError);
                            cmd_Hist.Parameters.Clear();
                            if (bError == true)
                            {
                                ErrorDS.Error err = errDS.Errors.NewError();
                                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                                err.ErrorInfo1 = ActionID.ACTION_ALLOCATION_REQUEST_ADD.ToString();
                                errDS.Errors.AddError(err);
                                errDS.AcceptChanges();
                                return errDS;
                            }
                            #endregion
                        //}
                        #endregion
                    }
                    else
                    {
                        messageDS.ErrorMsg.AddErrorMsgRow("Mail sending failed.");
                        messageDS.AcceptChanges();
                        errDS.Clear();
                        returnDS.Merge(messageDS);
                        returnDS.Merge(errDS);
                        return returnDS;
                    }
                }
                #endregion
            }

            

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _getAllocationActivities(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            ResourceAllocationDS reqDS = new ResourceAllocationDS();
            DataStringDS stringDS = new DataStringDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "All") cmd = DBCommandProvider.GetDBCommand("GetAllocationActivities_All");
            else cmd = DBCommandProvider.GetDBCommand("GetAllocationActivities_Active");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, reqDS, reqDS.AllocationActivity.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOCATION_ACTIVITIES.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            reqDS.AcceptChanges();

            returnDS.Merge(reqDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAllocationForExecution(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            ResourceAllocationDS resDS = new ResourceAllocationDS();
            DataSet returnDS = new DataSet();
            DataIntegerDS intDS = new DataIntegerDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();


            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            intDS.Merge(inputDS.Tables[intDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            intDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAllocationTaskForExecution");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["StartDate"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EndDate"].Value = dateDS.DataDates[1].DateValue;
            cmd.Parameters["VendorID1"].Value = intDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["VendorID2"].Value = intDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["TaskTypeID1"].Value = intDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["TaskTypeID2"].Value = intDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["TaskCategoryID1"].Value = intDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["TaskCategoryID2"].Value = intDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["Priority1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["Priority2"].Value = stringDS.DataStrings[0].StringValue;

            if (intDS.DataIntegers[3].IntegerValue != 0) cmd.Parameters["EmployeeID1"].Value = intDS.DataIntegers[3].IntegerValue;
            else cmd.Parameters["EmployeeID1"].Value = DBNull.Value;
            if (intDS.DataIntegers[3].IntegerValue != 0) cmd.Parameters["EmployeeID2"].Value = intDS.DataIntegers[3].IntegerValue;
            else cmd.Parameters["EmployeeID2"].Value = DBNull.Value;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, resDS, resDS.AllocationRequest.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOCATION_FOR_EXECUTION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            resDS.AcceptChanges();

            returnDS.Merge(resDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _executeAllocationTask(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ResourceAllocationDS resDS = new ResourceAllocationDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            DataBoolDS boolDS = new DataBoolDS();
            DataStringDS stringDS = new DataStringDS();
            string messageBody = "", returnMessage = "", URL = "";
            bool bError = false;
            int nRowAffected = -1;
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            resDS.Merge(inputDS.Tables[resDS.AllocationRequest.TableName], false, MissingSchemaAction.Error);
            resDS.AcceptChanges();

            #region Get Email Information from UtilDL
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            OleDbCommand cmd_Hist = new OleDbCommand();
            cmd_Hist.CommandText = "PRO_ALLOCATION_HIST_CREATE";
            cmd_Hist.CommandType = CommandType.StoredProcedure;

            if (cmd_Hist == null) return UtilDL.GetCommandNotFound();

            if (!boolDS.DataBools[0].BoolValue) //If NotifyByMail is  not Checked
            {
                #region Allocation Task History
                foreach (ResourceAllocationDS.AllocationRequestRow row in resDS.AllocationRequest.Rows)
                {
                    long genPK_Hist = IDGenerator.GetNextGenericPK();
                    if (genPK_Hist == -1) return UtilDL.GetDBOperationFailed();

                    cmd_Hist.Parameters.AddWithValue("p_AllocationHistoryID", genPK_Hist);
                    cmd_Hist.Parameters.AddWithValue("p_AllocationTaskID", row.AllocationTaskID);
                    cmd_Hist.Parameters.AddWithValue("p_SenderUserID", row.AssignedBy);
                    cmd_Hist.Parameters.AddWithValue("p_Employeecode", row.AssignByEmpCode);
                    cmd_Hist.Parameters.AddWithValue("p_ReceivedDate", row.EntryDate);

                    if (!row.IsAlloHistoryCommentsNull()) cmd_Hist.Parameters.AddWithValue("p_AlloHistoryComments", row.AlloHistoryComments);
                    else cmd_Hist.Parameters.AddWithValue("p_AlloHistoryComments", DBNull.Value);

                    cmd_Hist.Parameters.AddWithValue("p_AlloActivitiesID", row.AlloActivitiesID);
                    cmd_Hist.Parameters.AddWithValue("p_RealizedAmount", row.RealizedAmount);
                    cmd_Hist.Parameters.AddWithValue("p_EndDate", row.EndDate);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Hist, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Hist.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ALLOCATION_REQUEST_EXECUTE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;

                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.TaskType.ToString() + " ==> " + row.EmployeeName);
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.TaskType.ToString() + " ==> " + row.EmployeeName);

                    messageDS.AcceptChanges();
                }
                #endregion
            }
            else
            {
                #region If NotifyByMail is Checked....
                URL = stringDS.DataStrings[0].StringValue;
                foreach (ResourceAllocationDS.AllocationRequestRow row in resDS.AllocationRequest.Rows)
                {
                    returnMessage = "";
                    string fromEmailAddress = "", emailAddress = "", ccEmailAddress = "", mailingSubject = "";

                    URL += "?pVal=HRAS";
                    emailAddress = row.EmployeeEmail;
                    fromEmailAddress = UtilDL.GetFromEMailAddress();

                    string[] splitedTask = row.TaskType.Split('(', '[');
                    mailingSubject = "Job Execution Status";

                    messageBody += "<br>";
                    //messageBody += "<b> Dear </b>" + row.EmployeeName + ",";
                    messageBody += "<b> Dear Concern, </b>";
                    messageBody += "<br>";
                    messageBody += "<b> The following job has been executed :- </b>";
                    messageBody += "<br>";
                    messageBody += "<b>Client : </b>" + row.VendorName;
                    messageBody += "<br>";
                    messageBody += "<b>Task : </b>" + splitedTask[0];
                    messageBody += "<br>";
                    messageBody += "<b>Year End : </b>" + row.TaskName;
                    messageBody += "<br>";
                    messageBody += "<br>";
                    messageBody += "<b>Status : </b>" + row.AlloHistoryComments;
                    messageBody += "<br>";
                    messageBody += "Click the following link to see the job status.";
                    messageBody += "<br>";
                    messageBody += "<a href='" + URL + "'>" + URL + "</a>";

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, row.SenderUserName);
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, row.SenderUserName);
                    }
                    if (returnMessage == "Email successfully sent.")
                    {
                        #region Allocation Task History
                        //foreach (ResourceAllocationDS.AllocationRequestRow row in resDS.AllocationRequest.Rows)
                        //{
                            long genPK_Hist = IDGenerator.GetNextGenericPK();
                            if (genPK_Hist == -1) return UtilDL.GetDBOperationFailed();

                            cmd_Hist.Parameters.AddWithValue("p_AllocationHistoryID", genPK_Hist);
                            cmd_Hist.Parameters.AddWithValue("p_AllocationTaskID", row.AllocationTaskID);
                            cmd_Hist.Parameters.AddWithValue("p_SenderUserID", row.AssignedBy);
                            cmd_Hist.Parameters.AddWithValue("p_Employeecode", row.AssignByEmpCode);
                            cmd_Hist.Parameters.AddWithValue("p_ReceivedDate", row.EntryDate);

                            if (!row.IsAlloHistoryCommentsNull()) cmd_Hist.Parameters.AddWithValue("p_AlloHistoryComments", row.AlloHistoryComments);
                            else cmd_Hist.Parameters.AddWithValue("p_AlloHistoryComments", DBNull.Value);

                            cmd_Hist.Parameters.AddWithValue("p_AlloActivitiesID", row.AlloActivitiesID);
                            cmd_Hist.Parameters.AddWithValue("p_RealizedAmount", row.RealizedAmount);
                            cmd_Hist.Parameters.AddWithValue("p_EndDate", row.EndDate);

                            bError = false;
                            nRowAffected = -1;
                            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Hist, connDS.DBConnections[0].ConnectionID, ref bError);
                            cmd_Hist.Parameters.Clear();
                            if (bError == true)
                            {
                                ErrorDS.Error err = errDS.Errors.NewError();
                                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                                err.ErrorInfo1 = ActionID.ACTION_ALLOCATION_REQUEST_EXECUTE.ToString();
                                errDS.Errors.AddError(err);
                                errDS.AcceptChanges();
                                return errDS;
                            }
                            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.TaskType.ToString() + " ==> " + row.EmployeeName);
                            else messageDS.ErrorMsg.AddErrorMsgRow(row.TaskType.ToString() + " ==> " + row.EmployeeName);

                            messageDS.AcceptChanges();
                        //}
                        #endregion
                    }
                    else
                    {
                        messageDS.ErrorMsg.AddErrorMsgRow("Mail sending failed.");
                        messageDS.AcceptChanges();
                        errDS.Clear();
                        returnDS.Merge(messageDS);
                        returnDS.Merge(errDS);
                        return returnDS;
                    }
                }
                #endregion
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _getAssignedJobStatus(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            ResourceAllocationDS resDS = new ResourceAllocationDS();
            DataSet returnDS = new DataSet();
            DataIntegerDS intDS = new DataIntegerDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();


            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            intDS.Merge(inputDS.Tables[intDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            intDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAssignedJobStatus");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["StartDate"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EndDate"].Value = dateDS.DataDates[1].DateValue;
            cmd.Parameters["VendorID1"].Value = intDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["VendorID2"].Value = intDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["TaskTypeID1"].Value = intDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["TaskTypeID2"].Value = intDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["TaskCategoryID1"].Value = intDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["TaskCategoryID2"].Value = intDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["DepartmentID1"].Value = intDS.DataIntegers[3].IntegerValue;
            cmd.Parameters["DepartmentID2"].Value = intDS.DataIntegers[3].IntegerValue;
            cmd.Parameters["Priority1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["Priority2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode3"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode4"].Value = stringDS.DataStrings[1].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, resDS, resDS.AllocationRequest.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ASSIGNED_JOB_STATUS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            resDS.AcceptChanges();

            returnDS.Merge(resDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateAssignedJobStatus(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS resDS = new ResourceAllocationDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ASSIGN_JOB_STATUS_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            resDS.Merge(inputDS.Tables[resDS.AllocationRequest.TableName], false, MissingSchemaAction.Error);
            resDS.AcceptChanges();

            foreach (ResourceAllocationDS.AllocationRequestRow row in resDS.AllocationRequest.Rows)
            {
                cmd.Parameters.AddWithValue("p_ReceivableAmount", row.ReceivableAmount);

                cmd.Parameters.AddWithValue("p_AllocationTaskID", row.AllocationTaskID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSIGNED_JOB_STATUS_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.TaskType + " => " + row.AssignByEmpCode.ToString());
                else messageDS.ErrorMsg.AddErrorMsgRow(row.TaskType + " => " + row.AssignByEmpCode.ToString());

                messageDS.AcceptChanges();
            }
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            return returnDS;
        }

        private DataSet _get_FilteredTask_All(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ResourceAllocationDS resDS = new ResourceAllocationDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("Get_FilteredTask");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, resDS, resDS.JobFilterAll.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TASK_FILTER_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            resDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(resDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getAllocateForExecutionAllInfo(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            ResourceAllocationDS resDS = new ResourceAllocationDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllocationTaskForExecutionAllInfo");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, resDS, resDS.AllocationRequest.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ALLOCATION_FOR_EXECUTION_ALL_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            resDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(resDS.AllocationRequest);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}
