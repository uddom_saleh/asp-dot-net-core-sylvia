using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;


namespace Sylvia.DAL.Payroll
{
	/// <summary>
	/// Summary description for UserAuthenticationDL.
	/// </summary>
	public class UserAuthenticationDL
	{
		public UserAuthenticationDL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_ACTION_GET_USER_BY_IPADDRESS:
                    return this._getUserByIPAddress(inputDS);
                case ActionID.NA_KILL_INACTIVE_SESSIONS:
                    return this._KillInactiveSessions(inputDS);

                case ActionID.NA_ACTION_SEND_TEMP_PASSWORD_BY_EMAIL:
                    return _updateUserTempPassword(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }


        private DataSet _getUserByIPAddress(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetUserByIPAddress");  //for current fiscal year's data   

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            if (StringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["IPAddress"].Value = StringDS.DataStrings[0].StringValue;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            UserDS userDS = new UserDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.RememberMe.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_USER_BY_IPADDRESS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _KillInactiveSessions(DataSet inputDS)
        {
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "KILL_SESSION";
            cmd.Parameters.AddWithValue("p_ClientMachineName", stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            UserDS userDS = new UserDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
            
        }
        private DataSet _updateUserTempPassword(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();
            DBConnectionDS connDS = new DBConnectionDS();

            #region Get Email Information
            string messageBody = "", returnMessage = "";
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion            

            //extract dbconnection
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TEMP_PASSWORD_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            userDS.Merge(inputDS.Tables[userDS.Users.TableName], false, MissingSchemaAction.Error);
            userDS.AcceptChanges();

            foreach (UserDS.User row in userDS.Users.Rows)
            {
                #region Send Email

                returnMessage = "";

                messageBody = "<b>Employee : </b>" + row.EmployeeCode + " - " + row.EmployeeName + " [" + row.DesignationName + "]" + "<br><br>";
                messageBody += "<b>Your temporary password is: </b>" + row.EmailPassword;
                messageBody += "<br><b>You must change your password first.</b>";
                messageBody += "<br><br><br>";
                messageBody += "<br>";


                Mail.Mail mail = new Mail.Mail();
                string FromEmailAddress = credentialEmailAddress;
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, FromEmailAddress, row.EMAIL, "",
                                                   "Password Recovery Message", messageBody, "Recovery Password");
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, FromEmailAddress, row.EMAIL, "", "Password Recovery Message", messageBody, "Recovery Password");
                }

                if (returnMessage != "Email successfully sent.")
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMP_SALARY_PARKING_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                cmd.Parameters.AddWithValue("p_LoginID", row.LoginId);
                cmd.Parameters.AddWithValue("p_Temp_Password", row.Temp_Password);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_SEND_TEMP_PASSWORD_BY_EMAIL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
            }
            returnDS.Merge(errDS);
            return returnDS;
        }
	}
}
