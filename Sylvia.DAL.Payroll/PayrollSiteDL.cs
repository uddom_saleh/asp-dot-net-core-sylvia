/* *********************************************************************
 *  Compamy: Milllennium Information Solution Limited		 		           *  
 *  Author: Md. Hasinur Rahman Likhon				 				                   *
 *  Comment Date: April 17, 2006									                     *
 ***********************************************************************/

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
	/// <summary>
	/// Summary description for PayrollSiteDL.
	/// </summary>
	public class PayrollSiteDL
	{
		public PayrollSiteDL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {

          case ActionID.NA_ACTION_GET_PAYROLLSITE_LIST:
          return _getPayrollSiteList(inputDS);
        case ActionID.ACTION_DOES_PAYROLLSITE_EXIST:
          return _doesPayrollSiteExist(inputDS);
        case ActionID.ACTION_PAYROLLSITE_ADD:
          return _createPayrollSite(inputDS);
        case ActionID.ACTION_PAYROLLSITE_DEL:
          return _deletePayrollSite(inputDS);
        case ActionID.ACTION_PAYROLLSITE_UPD:
          return _updatePayrollSite(inputDS);
        
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
  
    private DataSet _getPayrollSiteList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataLongDS longDS = new DataLongDS();
      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

   

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPayrollSiteList");
  
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      PayrollSitePO payPO = new PayrollSitePO();
      PayrollSiteDS payDS = new PayrollSiteDS();

      bool bError = false;
      int nRowAffected = -1;
      //nRowAffected = ADOController.Instance.Fill(adapter, payPO, payPO.PayrollSites.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
      nRowAffected = ADOController.Instance.Fill(adapter, payDS, payDS.PayrollSites.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_GET_PAYROLLSITE_LIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }

      //payPO.AcceptChanges();
      payDS.AcceptChanges();        // WALI :: 28-Jul-2015

      #region Commented :: WALI :: 28-Jul-2015
      //if ( payPO.PayrollSites.Count > 0 )
      //{
      //  foreach ( PayrollSitePO.PayrollSite poPay in payPO.PayrollSites)
      //  {
      //    PayrollSiteDS.PayrollSite pay = payDS.PayrollSites.NewPayrollSite();
      //    if(poPay.IsPayrollSiteCodeNull()==false)
      //    {
      //      pay.Code = poPay.PayrollSiteCode;
      //    }
      //    if(poPay.IsPayrollSiteNameNull()==false)
      //    {
      //      pay.Name = poPay.PayrollSiteName;
      //    }
      //    if (!poPay.IsKPIGroupID_PSNull())
      //    {
      //        pay.KPIGroupID_PS = poPay.KPIGroupID_PS;
      //    }
      //    payDS.PayrollSites.AddPayrollSite( pay );
      //    payDS.AcceptChanges();
      //  }
      //}
      #endregion

      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( payDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
    
      return returnDS;
    
    }
  
    private DataSet _doesPayrollSiteExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesPayrollSiteExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
        cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    } 

    private DataSet _createPayrollSite(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      PayrollSiteDS sitDS = new PayrollSiteDS();
      DBConnectionDS connDS = new  DBConnectionDS();
      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      //create command
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreatePayrollSite");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      sitDS.Merge( inputDS.Tables[ sitDS.PayrollSites.TableName ], false, MissingSchemaAction.Error );
      sitDS.AcceptChanges();
      foreach(PayrollSiteDS.PayrollSite sit in sitDS.PayrollSites)
      {
        long genPK = IDGenerator.GetNextGenericPK();
        if( genPK == -1)
        {
          UtilDL.GetDBOperationFailed();
        }
        cmd.Parameters["Id"].Value = (object) genPK;
        //code
        if(sit.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) sit.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;
        }
        //name
        if(sit.IsNameNull()==false)
        {
          cmd.Parameters["Name"].Value = (object) sit.Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value=null;
        }
        if (!sit.IsKPIGroupID_PSNull())
        {
            cmd.Parameters["KPIGroupID_PS"].Value = sit.KPIGroupID_PS;
        }
        else cmd.Parameters["KPIGroupID_PS"].Value = DBNull.Value;
        
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_PAYROLLSITE_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _deletePayrollSite(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();
      DBConnectionDS connDS = new  DBConnectionDS();      
      DataStringDS stringDS = new DataStringDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeletePayrollSite");
      if( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }
      cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue;
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_PAYROLLSITE_DEL.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }
      return errDS;  // return empty ErrorDS
    }
    private DataSet _updatePayrollSite(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();
      PayrollSiteDS sitDS = new PayrollSiteDS();
      DBConnectionDS connDS = new  DBConnectionDS();
      //extract dbconnection 
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      //create command
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdatePayrollSite");
      if ( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }
      sitDS.Merge( inputDS.Tables[ sitDS.PayrollSites.TableName ], false, MissingSchemaAction.Error );
      sitDS.AcceptChanges();
      foreach(PayrollSiteDS.PayrollSite sit in sitDS.PayrollSites)
      {
        if (sit.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) sit.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value =null;
        }
        if (sit.IsNameNull()== false)
        {
          cmd.Parameters["Name"].Value = (object) sit.Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value = null;
        }
        if (!sit.IsKPIGroupID_PSNull())
        {
            cmd.Parameters["KPIGroupID_PS"].Value = sit.KPIGroupID_PS;
        }
        else cmd.Parameters["KPIGroupID_PS"].Value = DBNull.Value;
        
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_PAYROLLSITE_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
	}
}
