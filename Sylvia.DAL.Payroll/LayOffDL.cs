/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: April 23, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
  /// <summary>
  /// Summary description for DepartmentDL.
  /// </summary>
  public class LayOffDL
  {
    public LayOffDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }
    
    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {
        case ActionID.ACTION_LAYOFFEMPLOYEE_ADD:
          return _createLayOffEmployee( inputDS );    
        case ActionID.ACTION_LAYOFFPARAMETER_ADD:
          return _createLayOffParameter( inputDS );
      case ActionID.NA_ACTION_Q_ALL_LAYOFFPARAMETER:
          return _getLayOffParameterList(inputDS);
        case ActionID.ACTION_DOES_LAYOFFPARAMETER_EXIST:
          return this._doesLayOffParameterExist( inputDS );
      case ActionID.NA_ACTION_Q_ALL_LAYOFFRULE:
          return this._getLayOffRuleList( inputDS );
      case ActionID.NA_ACTION_Q_ALL_LAYOFFEMPLOYEE:
          return this._getLayOffEmployeeList( inputDS );
        case ActionID.ACTION_LAYOFFPARAMETER_DEL:
          return _deleteLayOffParameter( inputDS );    
     
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
    private DataSet _doesLayOffParameterExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesLayOffParamExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
        cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    } 
    private DataSet _createLayOffEmployee(DataSet inputDS)
    {
      DataSet responseDS = new DataSet();
      ErrorDS errDS = new ErrorDS();
      LayOffEmployeeDS empDS = new LayOffEmployeeDS();
      DBConnectionDS connDS = new  DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateLayOffEmployee");
      if( cmd == null )
        return UtilDL.GetCommandNotFound();
      empDS.Merge( inputDS.Tables[ empDS.LaidOffEmployees.TableName ], false, MissingSchemaAction.Error );
      empDS.AcceptChanges();
    
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
     
      foreach(LayOffEmployeeDS.LaidOffEmployee emp in empDS.LaidOffEmployees)
      {
        
        
        if(emp.IsEmployeeCodeNull()==false)
        {
          cmd.Parameters["EmployeeCode"].Value =  emp.EmployeeCode;
        }
        else
        {
          cmd.Parameters["EmployeeCode"].Value = null;
        }

        if(emp.IsLaidOffCodeNull()==false)
        {
          cmd.Parameters["LayOffCode"].Value =  emp.LaidOffCode;
        }
        else
        {
          cmd.Parameters["LayOffCode"].Value = null;
        }
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
        
        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_LAYOFFEMPLOYEE_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }

    private DataSet _createLayOffRule(DataSet inputDS, string sParameterCode)
    {
      DataSet responseDS = new DataSet();
      ErrorDS errDS = new ErrorDS();
      LayOffRoleDS ruleDS = new LayOffRoleDS();
      DBConnectionDS connDS = new  DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateLayOffRule");
      if( cmd == null )
        return UtilDL.GetCommandNotFound();
      ruleDS.Merge( inputDS.Tables[ ruleDS.LaidOffRoles.TableName ], false, MissingSchemaAction.Error );
      ruleDS.AcceptChanges();
    
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
     
      foreach(LayOffRoleDS.LaidOffRole rule in ruleDS.LaidOffRoles)
      {
        
        cmd.Parameters["LayOffCode"].Value =  sParameterCode;
      
        if(rule.IsSlabNoNull()==false)
        {
          cmd.Parameters["SlabNo"].Value =  rule.SlabNo;
        }
        else
        {
          cmd.Parameters["SlabNo"].Value = null;
        }

        if(rule.IsRoleDaysNull()==false)
        {
          cmd.Parameters["RuleDays"].Value =  rule.RoleDays;
        }
        else
        {
          cmd.Parameters["RuleDays"].Value = null;
        }

        if(rule.IsAllowDeducCodeNull()==false)
        {
          
          long nADId = UtilDL.GetAllowDeductId(connDS, rule.AllowDeducCode);
          if(nADId == -1)
          {
            cmd.Parameters["AlloeDeductId"].Value =  1;
          
          }
          else
          {
            cmd.Parameters["AlloeDeductId"].Value =  nADId;
          }
        }
        else
        {
          cmd.Parameters["AlloeDeductId"].Value = null;
        }

        if(rule.IsPercentageNull()==false)
        {
          cmd.Parameters["Percentage"].Value =  rule.Percentage;
        }
        else
        {
          cmd.Parameters["Percentage"].Value = null;
        }
       
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
        
        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_LAYOFFPARAMETER_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _createLayOffParameter(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      LayOffParameterDS layoffDS = new LayOffParameterDS();
      LayOffRoleDS ruleDS = new LayOffRoleDS();
      DataSet responseDS = new DataSet();
      DBConnectionDS connDS = new  DBConnectionDS();
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateLayOffParameter");
      if( cmd == null )
        return UtilDL.GetCommandNotFound();
      
      ruleDS.Merge( inputDS.Tables[ ruleDS.LaidOffRoles.TableName ], false, MissingSchemaAction.Error );
      ruleDS.AcceptChanges();
      layoffDS.Merge( inputDS.Tables[ layoffDS.LayOffParameters.TableName ], false, MissingSchemaAction.Error );
      layoffDS.AcceptChanges();
     
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      foreach(LayOffParameterDS.LayOffParameter param in layoffDS.LayOffParameters)
      {
        long genPK = IDGenerator.GetNextGenericPK();
        if( genPK == -1)
          return UtilDL.GetDBOperationFailed();

        cmd.Parameters["LayOffId"].Value =  genPK;
    
        
        if(param.IsCodeNull()==false)
        {
          cmd.Parameters["Code"].Value =  param.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value = null;
        }
        if(param.IsDescriptionNull()==false)
        {
          cmd.Parameters["Description"].Value =  param.Description;
        }
        else
        {
          cmd.Parameters["Description"].Value = null;
        }
        if(param.IsEffectDateNull()==false)
        {
          cmd.Parameters["EffectDate"].Value =  param.EffectDate;
        }
        else
        {
          cmd.Parameters["EffectDate"].Value = null;
        }
        if(param.IsDurationNull()==false)
        {
          cmd.Parameters["Duration"].Value =  param.Duration;
        }
        else
        {
          cmd.Parameters["Duration"].Value = null;
        }
        
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
        
        responseDS = _createLayOffRule(inputDS, param.Code);
        errDS.Clear();
        errDS.Merge(responseDS.Tables[ errDS.Errors.TableName ], false, MissingSchemaAction.Error );
        errDS.AcceptChanges();
        if (errDS.Errors.Count > 0 )
        {
          bError = true;
          
        }
       
        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_LAYOFFPARAMETER_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
   
    private DataSet _deleteLayOffParameter(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();
      string sCode = stringDS.DataStrings[0].StringValue;
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteLayOffEmployee");
      if(cmd == null)
        return UtilDL.GetCommandNotFound();
      
      cmd.Parameters["Code"].Value = sCode;
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
      if(bError == false)
      {
        OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("DeleteLayOffRule");
        if(cmd == null)
          return UtilDL.GetCommandNotFound();
        
        cmd1.Parameters["Code"].Value = sCode;
        
        nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd1, connDS.DBConnections[0].ConnectionID, ref bError );
      
        if(bError == false)
        {
          OleDbCommand cmd2 = DBCommandProvider.GetDBCommand("DeleteLayOffParameter");
          if(cmd == null)
            return UtilDL.GetCommandNotFound();
        
          cmd2.Parameters["Code"].Value = sCode;
        
          nRowAffected = -1;
          nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd2, connDS.DBConnections[0].ConnectionID, ref bError );
      
        }
      
      }
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_LAYOFFPARAMETER_DEL.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }

      return errDS;  // return empty ErrorDS
    }
  
    private DataSet _getLayOffParameterList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
     
      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLayOffParameterList");
      if (cmd == null)
      {

        Util.LogInfo("Command is null.");
        return UtilDL.GetCommandNotFound();
      }
      
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      LayOffParameterPO paramPO = new LayOffParameterPO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter, 
        paramPO, 
        paramPO.LayOffParameters.TableName, 
        connDS.DBConnections[0].ConnectionID, 
        ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_LAYOFFPARAMETER.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }
      paramPO.AcceptChanges();
      LayOffParameterDS paramDS = new LayOffParameterDS();
      if ( paramPO.LayOffParameters.Count > 0 )
      {
        foreach ( LayOffParameterPO.LayOffParameter poParam in paramPO.LayOffParameters)
        {
          LayOffParameterDS.LayOffParameter param = paramDS.LayOffParameters.NewLayOffParameter();
          
          if (poParam.IsCodeNull() == false )
          {
            param.Code = poParam.Code;
          }
          if (poParam.IsEffectDateNull() == false )
          {
            param.EffectDate= poParam.EffectDate;
          }
          if (poParam.IsDescriptionNull() == false )
          {
            param.Description= poParam.Description;
          }
          if (poParam.IsDurationNull() == false )
          {
            param.Duration= poParam.Duration;
          }

          paramDS.LayOffParameters.AddLayOffParameter( param );
          paramDS.AcceptChanges();
        }
      }


      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( paramDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;

    
    }
    private DataSet _getLayOffEmployeeList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      DataStringDS stringDS = new DataStringDS(); 
      DBConnectionDS connDS = new  DBConnectionDS();      
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      OleDbCommand cmd = null;
      if(stringDS.DataStrings[0].IsStringValueNull()== false)
      {
        string st = stringDS.DataStrings[0].StringValue;
        if(st == "All")
        {
          cmd = DBCommandProvider.GetDBCommand("GetLayOffEmployeeListAll");
        
        }
        else
        {
          cmd = DBCommandProvider.GetDBCommand("GetLayOffEmployeeList");
          cmd.Parameters["Code"].Value =  stringDS.DataStrings[0].StringValue;
        }
      }
      if (cmd == null)
      {

        Util.LogInfo("Command is null.");
        return UtilDL.GetCommandNotFound();
      }
      
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;
      LayOffEmployeePO empPO = new LayOffEmployeePO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter, 
        empPO, 
        empPO.LaidOffEmployees.TableName, 
        connDS.DBConnections[0].ConnectionID, 
        ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_LAYOFFEMPLOYEE.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }

      empPO.AcceptChanges();
      LayOffEmployeeDS empDS = new LayOffEmployeeDS();
      if ( empPO.LaidOffEmployees.Count > 0 )
      {
        foreach ( LayOffEmployeePO.LaidOffEmployee poLayOffEmployee in empPO.LaidOffEmployees)
        {
          LayOffEmployeeDS.LaidOffEmployee emp = empDS.LaidOffEmployees.NewLaidOffEmployee();
          if (poLayOffEmployee.IsEmployeeCodeNull() == false )
          {
            emp.EmployeeCode = poLayOffEmployee.EmployeeCode;
          }
          if (poLayOffEmployee.IsEmployeeNameNull() == false )
          {
            emp.EmployeeName= poLayOffEmployee.EmployeeName;
          }
          if (poLayOffEmployee.IsLaidOffCodeNull() == false )
          {
            emp.LaidOffCode= poLayOffEmployee.LaidOffCode;
          }


          empDS.LaidOffEmployees.AddLaidOffEmployee( emp );
          empDS.AcceptChanges();
        }
      }


      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( empDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;

    
    }
  
    private DataSet _getLayOffRuleList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      DataStringDS stringDS = new DataStringDS(); 
      DBConnectionDS connDS = new  DBConnectionDS();      
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();      
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();
      OleDbCommand cmd = null;
      if(stringDS.DataStrings[0].IsStringValueNull()== false)
      {
        string st = stringDS.DataStrings[0].StringValue;
        if(st == "All")
        {
          cmd = DBCommandProvider.GetDBCommand("GetLayOffRuleListAll");
        
        }
        else
        {
          cmd = DBCommandProvider.GetDBCommand("GetLayOffRuleList");
          cmd.Parameters["Code"].Value =  stringDS.DataStrings[0].StringValue;
        }
      }
      
      if (cmd == null)
      {

        Util.LogInfo("Command is null.");
        return UtilDL.GetCommandNotFound();
      }
      
     
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;
      LayOffRulePO rulePO = new LayOffRulePO();

      bool bError = false;
      int nRowAffected = -1;
       nRowAffected = ADOController.Instance.Fill(adapter, 
        rulePO, 
        rulePO.LaidOffRoles.TableName, 
        connDS.DBConnections[0].ConnectionID, 
        ref bError);
      if (bError == true )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_LAYOFFRULE.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;

      }

      rulePO.AcceptChanges();
      LayOffRoleDS  ruleDS = new LayOffRoleDS();
      if ( rulePO.LaidOffRoles.Count > 0 )
      {
        foreach ( LayOffRulePO.LaidOffRole poLayOffRule in rulePO.LaidOffRoles)
        {
          LayOffRoleDS.LaidOffRole rule = ruleDS.LaidOffRoles.NewLaidOffRole();
         
          if (poLayOffRule.IsLaidOffCodeNull() == false )
          {
            rule.LaidOffCode = poLayOffRule.LaidOffCode;
          }
          if (poLayOffRule.IsSlabNoNull() == false )
          {
            rule.SlabNo = poLayOffRule.SlabNo;
          }
          if (poLayOffRule.IsRoleDaysNull() == false )
          {
            rule.RoleDays= poLayOffRule.RoleDays;
          }
          if (poLayOffRule.IsAllowDeducIdNull() == false )
          {
            if(poLayOffRule.AllowDeducId == 1)
            {
              rule.AllowDeducCode= "Basic";
           
            }
            else
            {
            
              rule.AllowDeducCode= UtilDL.GetAllowDeductCode(connDS, poLayOffRule.AllowDeducId);
            }
          }
          if (poLayOffRule.IsPercentageNull() == false )
          {
            rule.Percentage= poLayOffRule.Percentage;
          }

          ruleDS.LaidOffRoles.AddLaidOffRole( rule );
          ruleDS.AcceptChanges();
        }
      }

    
      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();
      
      DataSet returnDS = new DataSet();

      returnDS.Merge( ruleDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    }
  
   
  
  }
}
