﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using System.Globalization;
using System.Configuration;


namespace Sylvia.DAL.Payroll
{
    public class ProvidentFundDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_GET_PF_ABLE_TO_INVEST_AMOUNT:
                    return this._GetAbleToInvestAmount(inputDS);
                case ActionID.ACTION_PF_CREATE_INVESTMENT:
                    return this._CreateInvestment_PF(inputDS);
                case ActionID.ACTION_PF_UPDATE_INVESTMENT:
                    return this._UpdateInvestment_PF(inputDS);
                case ActionID.NA_GET_PF_INVESTMENT_LIST:
                    return this._GetInvestmentList_PF(inputDS);
                case ActionID.NA_GET_PF_INVESTMENT_DETAILS_BY_INVID:
                    return this._GetInvestmentDetailsByInvID_PF(inputDS);
                case ActionID.ACTION_PF_DELETE_LAST_INVESTMENT:
                    return this._CancelLastInvestment_PF(inputDS);
                case ActionID.ACTION_PF_INVESTMENT_SETTLEMENT_CREATE:
                    return this._InvestmentSettlementCreate_PF(inputDS);

                case ActionID.NA_GET_PF_PROVISION_BENEFICIARY_LIST:
                    return this._GetProvisionBeneficiaryList_PF(inputDS);
                case ActionID.NA_GET_PF_INVESTMENT_PROVISION_LIST:
                    return this._GetInvestmentProvisionList_PF(inputDS);
                case ActionID.ACTION_PF_PROVISION_CREATION:
                    return this._ProvisionCreation_PF(inputDS);

                case ActionID.ACTION_PF_TERMS_OF_SEPARATION_SAVE:
                    return this._SaveTermsOfSeperation_PF(inputDS);
                case ActionID.NA_GET_PF_TERMS_OF_SEPARATION:
                    return this._GetTermsOfSeperation_PF(inputDS);

                case ActionID.NA_GET_PF_EMPLOYEE_LIST_FOR_SEPERATION:
                    return this._GetEmployeeListForSeperation_PF(inputDS);
                case ActionID.NA_GET_PF_EMPLOYEE_LIST_FOR_PAYMENT:
                    return this._GetEmployeeListForPayment_PF(inputDS);
                case ActionID.NA_GET_PF_PAYMENT_AMOUNT_FOR_SEPERATION:
                    return this._GetPaymentAmountsForSeperation_PF(inputDS);
                case ActionID.ACTION_PF_EMPLOYEE_SEPARATION:
                    return this._EmployeeSeperation_PF(inputDS);
                case ActionID.NA_GET_PF_PAYMENT_AMOUNT_FOR_PAYMENT:
                    return this._GetPaymentAmountsForPayment_PF(inputDS);
                case ActionID.ACTION_PF_EMPLOYEE_PAYMENT:
                    return this._EmployeePayment_PF(inputDS);

                case ActionID.NA_GET_PF_FUND_HEAD_LIST:
                    return this._GetFundHeadList_PF(inputDS);
                case ActionID.NA_GET_PF_FUND_HEAD_LIST_FOR_EMP_JOURNAL:
                    return this._GetFundHeadList_PF_ForEmpJournal(inputDS);
                case ActionID.NA_GET_PF_FUND_RECIEVE_DETAILS:
                    return this._GetFundRecieveDetails_PF(inputDS);
                case ActionID.ACTION_PF_FUND_RECIEVE:
                    return this._FundRecieve_PF(inputDS);

                case ActionID.NA_GET_PF_PROVISION_CALCULATED_DATA:
                    return this._GetProvisionCalculatedData_PF(inputDS);
                case ActionID.ACTION_PF_SAVE_PROVISION_CALCULATED_DATA:
                    return this._SaveProvisionCalculatedData_PF(inputDS);
                case ActionID.NA_GET_PF_PROVISIONED_PROFIT_FOR_EMPLOYEE:
                    return this._GetProvisionedProfit_ForEmployee_PF(inputDS);
                case ActionID.ACTION_PF_DISTRIBUTE_PROVISIONED_PROFIT:
                    return this._DistributeProvisionedProfit_PF(inputDS);
                case ActionID.NA_GET_PF_DISTRIBUTED_PROVISION_DATA:
                    return this.GetDistributedProvisionData_PF(inputDS);

                case ActionID.NA_GET_PF_PENDING_DISTRIBUTION:
                    return this._GetPendingDistributions_PF(inputDS);

                case ActionID.ACTION_PF_FUND_JOURNAL_ENTRY:
                    return this._FundJournalEntry_PF(inputDS);
                case ActionID.NA_GET_PF_HEADWISE_AVAILABLE_AMOUNT:
                    return this._GetHeadWiseAvailableAmount_PF(inputDS);
                case ActionID.NA_GET_PF_JOURNAL_EMPLOYEE_BY_PF_MEMBDATE:
                    return this._GetEmployeeList_By_PFMembDate(inputDS);
                case ActionID.ACTION_PF_EMPLOYEE_JOURNAL_ENTRY:
                    return this._EmployeeJournalEntry_PF(inputDS);

                case ActionID.NA_GET_DISTINCT_FUND_YEAR_PF:
                    return this._GetDistinctFundYear_PF(inputDS);
                case ActionID.NA_GET_PF_RECIEVED_FUND_LIST:
                    return this._GetRecievedFundList_PF(inputDS);
                case ActionID.NA_GET_PF_TOTAL_PAYABLE_TO_LSC:
                    return this.GetTotalPayableToLSC_PF(inputDS);

                case ActionID.NA_GET_PF_EMPLOYEE_BALANCE:
                    return this._GetEmployeeBalanceRecord_PF(inputDS);
                case ActionID.ACTION_PF_MODIFY_EMPLOYEE_BALANCE:
                    return this._ModifyEmployeeBalanceRecord_PF(inputDS);
                case ActionID.NA_GET_PF_FUND_BALANCE:
                    return this._GetFundHeadBalanceRecord_PF(inputDS);
                case ActionID.ACTION_PF_MODIFY_FUND_BALANCE:
                    return this._ModifyFundHeadBalanceRecord_PF(inputDS);
                case ActionID.NA_GET_PF_BENEFICIARY_LIST_TO_FUND_RECIEVE:
                    return this._GetBeneficiaryList_To_FundRecieve_PF(inputDS);
                case ActionID.ACTION_PF_FUND_RECIEVE_EMPLOYEE_WISE:
                    return this._Exceptional_FundRecieve_PF(inputDS);
                case ActionID.NA_GET_PF_EMPLOYEE_LIST_FOR_GEN_PAYMENT:
                    return this._GetPF_EmpListForGenaralPayment(inputDS);
                case ActionID.ACTION_PF_PAYMENT_GENERAL:
                    return this._PF_Payment_General(inputDS);
                case ActionID.NA_GET_PF_PAYMENT_AMOUNT_GENERAL:
                    return this._GetPF_PaymentAmountsGeneral(inputDS);
                case ActionID.NA_GET_PF_SEPARATION_INFO:
                    return this._GetPF_SeparationInfo(inputDS);
                case ActionID.NA_ACTION_GET_INFORMATION_ANY_PF:
                    return this._getInformationAny_PF(inputDS);
                case ActionID.ACTION_PF_FUND_JOURNAL_ENTRY_MULTIPLE:
                    return this._FundJournalEntryMultiple_PF(inputDS);
                case ActionID.ACTION_PF_FUND_JOURNAL_DESCRIPTION_ADD:
                    return this._FundJournalDescriptionEntry(inputDS);
                case ActionID.ACTION_PF_FUND_JOURNAL_DESCRIPTION_UPD:
                    return this._FundJournalDescriptionUpdate(inputDS);
                case ActionID.ACTION_PF_FUND_JOURNAL_DESCRIPTION_DEL:
                    return this._FundJournalDescriptionDelete(inputDS);

                    
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }


        private DataSet _GetAbleToInvestAmount(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAbleToInvestAmount_PF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_ABLE_TO_INVEST_AMOUNT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _CreateInvestment_PF(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            pfDS.Merge(inputDS.Tables[pfDS.Investment.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            #region Create New Investment
            cmd.CommandText = "PRO_WP_PF_INVESTMENT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            long InvID = IDGenerator.GetNextGenericPK();
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", InvID);
            cmd.Parameters.AddWithValue("p_InvestmentDate", DateTime.ParseExact(stringDS.DataStrings[0].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_MaturityDate", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_ProductID", Convert.ToInt32(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_PrincipalAmount", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_Profit", Convert.ToDecimal(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_Duration", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[6].StringValue);
            cmd.Parameters.AddWithValue("p_Tax", Convert.ToDecimal(stringDS.DataStrings[7].StringValue));
            cmd.Parameters.AddWithValue("p_ExciseDuty", Convert.ToDecimal(stringDS.DataStrings[8].StringValue));
            cmd.Parameters.AddWithValue("p_OthersCost", Convert.ToDecimal(stringDS.DataStrings[9].StringValue));
            cmd.Parameters.AddWithValue("p_Buyer", stringDS.DataStrings[10].StringValue);
            cmd.Parameters.AddWithValue("p_Total_Instrument", stringDS.DataStrings[12].StringValue);

            cmd.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[13].StringValue);       // WALI :: 13-Jan-2015

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_CREATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Add Product_Ins_Reference
            cmd.Dispose();
            cmd.CommandText = "PRO_WP_PF_INV_REFERENCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ProvidentFundDS.InvestmentRow row in pfDS.Investment.Rows)
            {
                cmd.Parameters.AddWithValue("p_INVID", InvID);
                cmd.Parameters.AddWithValue("p_Product_Ins_Reference", row.Product_Ins_Reference);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_CREATE_INVESTMENT.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetInvestmentList_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "NotSettled") cmd = DBCommandProvider.GetDBCommand("GetInvestmentListNotSettled_PF");
            else if (stringDS.DataStrings[0].StringValue == "All" && stringDS.DataStrings[1].StringValue == "1") cmd = DBCommandProvider.GetDBCommand("GetInvestmentListAll_PF");
            else if (stringDS.DataStrings[0].StringValue == "All" && stringDS.DataStrings[1].StringValue == "2") cmd = DBCommandProvider.GetDBCommand("GetNonProvisionedInvListAll_PF");

            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_INVESTMENT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetInvestmentDetailsByInvID_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetInvestmentDetailsByInvID_PF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["p_INVID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_INVESTMENT_DETAILS_BY_INVID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _UpdateInvestment_PF(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            pfDS.Merge(inputDS.Tables[pfDS.Investment.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            #region Update Investment
            cmd.CommandText = "PRO_WP_PF_INVESTMENT_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            Int32 InvID = -1;
            InvID = Convert.ToInt32(stringDS.DataStrings[11].StringValue);
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[11].StringValue));
            cmd.Parameters.AddWithValue("p_InvestmentDate", DateTime.ParseExact(stringDS.DataStrings[0].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_MaturityDate", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_ProductID", Convert.ToInt32(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_PrincipalAmount", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_Profit", Convert.ToDecimal(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_Duration", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[6].StringValue);
            cmd.Parameters.AddWithValue("p_Tax", Convert.ToDecimal(stringDS.DataStrings[7].StringValue));
            cmd.Parameters.AddWithValue("p_ExciseDuty", Convert.ToDecimal(stringDS.DataStrings[8].StringValue));
            cmd.Parameters.AddWithValue("p_OthersCost", Convert.ToDecimal(stringDS.DataStrings[9].StringValue));
            cmd.Parameters.AddWithValue("p_Buyer", stringDS.DataStrings[10].StringValue);
            cmd.Parameters.AddWithValue("p_Total_Instrument", stringDS.DataStrings[12].StringValue);

            cmd.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[13].StringValue);       // WALI :: 13-Jan-2015

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_UPDATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Delete OLD Product_Ins_Reference
            cmd.Dispose();
            cmd.CommandText = "PRO_WP_PF_INV_REFERENCE_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_INVID", InvID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_UPDATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Add New Product_Ins_Reference
            cmd.Dispose();
            cmd.CommandText = "PRO_WP_PF_INV_REFERENCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ProvidentFundDS.InvestmentRow row in pfDS.Investment.Rows)
            {
                cmd.Parameters.AddWithValue("p_INVID", InvID);
                cmd.Parameters.AddWithValue("p_Product_Ins_Reference", row.Product_Ins_Reference);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_CREATE_INVESTMENT.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _CancelLastInvestment_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_WP_PF_DEL_LAST_INVESTMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_InvID", integerDS.DataIntegers[0].IntegerValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_DELETE_LAST_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _FreezeInvestment_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_FREEZE_INVESTMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //err.ErrorInfo1 = ActionID.ACTION_PF_FREEZE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _InvestmentSettlementCreate_PF(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            // WALI :: 06-May-2015
            DateTime settlementDate = DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null);
            DateTime provisionDate = new DateTime(settlementDate.Year, settlementDate.Month, 2);  // Second day of 'Settlement Month'

            #region Investment Settlement
            cmd.CommandText = "PRO_WP_PF_INV_SETTLEMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            Int32 InvID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_Settlement_Status", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_SettlementDate", settlementDate);  // WALI :: 06-May-2015

            cmd.Parameters.AddWithValue("p_Tax", Convert.ToDecimal(stringDS.DataStrings[6].StringValue));
            cmd.Parameters.AddWithValue("p_ExciseDuty", Convert.ToDecimal(stringDS.DataStrings[7].StringValue));
            cmd.Parameters.AddWithValue("p_OthersCost", Convert.ToDecimal(stringDS.DataStrings[8].StringValue));

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_INVESTMENT_SETTLEMENT_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Provision For Mismatched Profit
            if (Convert.ToDecimal(stringDS.DataStrings[3].StringValue) != 0)
            {
                cmd.Dispose();
                cmd.CommandText = "PRO_WP_PF_PROVISION_CREATE_ST";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
                //cmd.Parameters.AddWithValue("p_ProvisionDate", DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null));
                cmd.Parameters.AddWithValue("p_ProvisionDate", provisionDate);   // WALI :: 06-May-2015
                cmd.Parameters.AddWithValue("p_Day_P", 0);
                cmd.Parameters.AddWithValue("p_Profit_P", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
                cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[4].StringValue);
                cmd.Parameters.AddWithValue("p_InvProvisionedType", stringDS.DataStrings[9].StringValue);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_INVESTMENT_SETTLEMENT_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetProvisionBeneficiaryList_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetProvisionBeneficiaryList_PF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_PROVISION_BENEFICIARY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetInvestmentProvisionList_PF(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            #region Old
            //if (stringDS.DataStrings[0].StringValue == "ProvisionedMaxDate")
            //{
            //    cmd = DBCommandProvider.GetDBCommand("InvProvisionMaxDate_PF");
            //    adapter.SelectCommand = cmd;
            //}
            //else
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetInvestmentProvisionList_PF");
            //    if (cmd == null)
            //    {
            //        Util.LogInfo("Command is null.");
            //        return UtilDL.GetCommandNotFound();
            //    }
            //    adapter.SelectCommand = cmd;

            //    string ProvisionDate = stringDS.DataStrings[0].StringValue + "/01/" + stringDS.DataStrings[1].StringValue + " 12:00:01 AM";
            //    cmd.Parameters["p_ProvisionDate"].Value = (object)Convert.ToDateTime(ProvisionDate);
            //}
            #endregion

            cmd = DBCommandProvider.GetDBCommand("GetInvestmentProvisionList_PF");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_INVESTMENT_PROVISION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ProvisionCreation_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_PROVISION_PROCESS";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            string ProvisionDate = stringDS.DataStrings[0].StringValue + "/01/" + stringDS.DataStrings[1].StringValue + " 12:00:01 AM";

            cmd.Parameters.AddWithValue("p_ProvisionDate", (object)Convert.ToDateTime(ProvisionDate));
            cmd.Parameters.AddWithValue("p_LoginID", (object)stringDS.DataStrings[2].StringValue);
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_PROVISION_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }

        private DataSet _SaveTermsOfSeperation_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            pfDS.Merge(inputDS.Tables[pfDS.TermsOfSeparation_PF.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_TERM_OF_SEPARATE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd_Del.CommandText = "PRO_WP_PF_TERM_OF_SEPARATE_DEL";
            cmd_Del.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmd_Del == null) return UtilDL.GetCommandNotFound();

            #region Delete Old Terms
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_TERMS_OF_SEPARATION_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create New Terms
            if (pfDS.TermsOfSeparation_PF.Rows.Count > 0)
            {
                foreach (ProvidentFundDS.TermsOfSeparation_PFRow row in pfDS.TermsOfSeparation_PF.Rows)
                {
                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) return UtilDL.GetDBOperationFailed();

                    cmd.Parameters.AddWithValue("p_TermsOfSeparationID", genPK);
                    cmd.Parameters.AddWithValue("p_ServiceYear", row.ServiceYear);
                    cmd.Parameters.AddWithValue("p_PaymentPercent_ProfiTself", row.PaymentPercent_ProfitSelf);
                    cmd.Parameters.AddWithValue("p_PaymentPercent_ProfitComp", row.PaymentPercent_ProfitComp);
                    cmd.Parameters.AddWithValue("p_PaymentPercent_PrincipalSelf", row.PaymentPercent_PrincipalSelf);
                    cmd.Parameters.AddWithValue("p_PaymentPercent_PrincipalComp", row.PaymentPercent_PrincipalComp);
                    cmd.Parameters.AddWithValue("p_DependsOn", pfDS.TermsOfSeparation_PF[0].DependsOn);                 // Only first row has value

                    cmd.Parameters.AddWithValue("p_LoanPercent_ProfitSelf", row.LoanPercent_ProfitSelf);
                    cmd.Parameters.AddWithValue("p_LoanPercent_ProfitComp", row.LoanPercent_ProfitComp);
                    cmd.Parameters.AddWithValue("p_LoanPercent_PrincipalSelf", row.LoanPercent_PrincipalSelf);
                    cmd.Parameters.AddWithValue("p_LoanPercent_PrincipalComp", row.LoanPercent_PrincipalComp);
                    bError = false;
                    nRowAffected = -1;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PF_TERMS_OF_SEPARATION_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion
            return errDS;
        }
        private DataSet _GetTermsOfSeperation_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetTermsOfSeparation_PF");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.TermsOfSeparation_PF.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_TERMS_OF_SEPARATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetEmployeeListForSeperation_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_ForSeperation_PF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_EMPLOYEE_LIST_FOR_SEPERATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetEmployeeListForPayment_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_ForPayment_PF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["PaymentStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);   // Added ::WALI :: 19-Apr-2015
            cmd.Parameters["PaymentStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);   // Added ::WALI :: 19-Apr-2015

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_EMPLOYEE_LIST_FOR_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetPaymentAmountsForSeperation_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPaymentAmountsForSeperation_PF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_PAYMENT_AMOUNT_FOR_SEPERATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _EmployeeSeperation_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            OleDbCommand cmd_Sep = new OleDbCommand();
            OleDbCommand cmd_Head = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            pfDS.Merge(inputDS.Tables[pfDS.PF_Employee_Payment.TableName], false, MissingSchemaAction.Error);
            pfDS.Merge(inputDS.Tables[pfDS.PF_Head_GL.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            cmd_Sep.CommandText = "PRO_WP_PF_EMPLOYEE_SEPERATION";
            cmd_Sep.CommandType = CommandType.StoredProcedure;

            cmd_Head.CommandText = "PRO_WP_PF_HEADGL_ADD_FOR_LF";
            cmd_Head.CommandType = CommandType.StoredProcedure;

            if (cmd_Sep == null || cmd_Head == null) return UtilDL.GetCommandNotFound();

            #region Employee Seperation
            cmd_Sep.Parameters.AddWithValue("p_EmployeeID", pfDS.PF_Employee_Payment[0].EmployeeID);
            cmd_Sep.Parameters.AddWithValue("p_PF_Comp", pfDS.PF_Employee_Payment[0].PF_Comp);
            cmd_Sep.Parameters.AddWithValue("p_PF_Self", pfDS.PF_Employee_Payment[0].PF_Self);
            cmd_Sep.Parameters.AddWithValue("p_PF_Comp_Profit", pfDS.PF_Employee_Payment[0].PF_Comp_Profit);
            cmd_Sep.Parameters.AddWithValue("p_PF_Self_Profit", pfDS.PF_Employee_Payment[0].PF_Self_Profit);
            cmd_Sep.Parameters.AddWithValue("p_Payment_Date", pfDS.PF_Employee_Payment[0].SeperationDate);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Sep, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd_Sep.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_EMPLOYEE_SEPARATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region For Differences [Forfeirute]
            if (pfDS.PF_Head_GL.Rows.Count > 0)
            {
                foreach (ProvidentFundDS.PF_Head_GLRow row in pfDS.PF_Head_GL.Rows)
                {
                    cmd_Head.Parameters.AddWithValue("p_RefHeadCode", row.RefHeadCode);
                    cmd_Head.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                    cmd_Head.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                    cmd_Head.Parameters.AddWithValue("p_Ref_EmployeeID", row.Ref_EmployeeID);
                    cmd_Head.Parameters.AddWithValue("p_Calculated_GL_Amount", row.GL_Amount);      // WALI :: 09-Mar-2015

                    bError = false;
                    nRowAffected = -1;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Head, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Head.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PF_EMPLOYEE_SEPARATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion
           
            #region Fund Journal Add for 'PAYABLE TO LSC'
            if (pfDS.PF_Employee_Payment[0].Payment_Source != "Cash")
            {
                OleDbCommand cmd_Fund = new OleDbCommand();
                cmd_Fund.CommandText = "PRO_WP_PF_FUND_RECIEVE";
                cmd_Fund.CommandType = CommandType.StoredProcedure;
                if (cmd_Fund == null) return UtilDL.GetCommandNotFound();

                decimal gl_Amount = pfDS.PF_Employee_Payment[0].PF_Comp + pfDS.PF_Employee_Payment[0].PF_Self +
                                    pfDS.PF_Employee_Payment[0].PF_Comp_Profit + pfDS.PF_Employee_Payment[0].PF_Self_Profit;

                cmd_Fund.Parameters.AddWithValue("p_HeadID", 108);          // HeadID for 'Payable to LSC'
                cmd_Fund.Parameters.AddWithValue("p_GL_Amount", gl_Amount);
                cmd_Fund.Parameters.AddWithValue("p_Event_Date", pfDS.PF_Employee_Payment[0].SeperationDate);
                cmd_Fund.Parameters.AddWithValue("p_Ref_EmployeeID", pfDS.PF_Employee_Payment[0].EmployeeID);

                if (!pfDS.PF_Employee_Payment[0].IsDonor_ReferenceNull()) cmd_Fund.Parameters.AddWithValue("p_Ref_Other", pfDS.PF_Employee_Payment[0].Donor_Reference);
                else cmd_Fund.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);

                cmd_Fund.Parameters.AddWithValue("p_SalaryDate", DBNull.Value);
                cmd_Fund.Parameters.AddWithValue("p_Calculated_GL_Amount", gl_Amount);     // WALI :: 09-Mar-2015

                if (!pfDS.PF_Employee_Payment[0].IsRemarksNull()) cmd_Fund.Parameters.AddWithValue("p_Remarks", pfDS.PF_Employee_Payment[0].Remarks);
                else cmd_Fund.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Fund, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Fund.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_EMPLOYEE_SEPARATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            return errDS;
        }
        private DataSet _GetPaymentAmountsForPayment_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPaymentAmountsForPayment_PF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_PAYMENT_AMOUNT_FOR_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _EmployeePayment_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            pfDS.Merge(inputDS.Tables[pfDS.PF_Employee_Payment.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_EMPLOYEE_PAYMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_EmployeeID", pfDS.PF_Employee_Payment[0].EmployeeID);
            cmd.Parameters.AddWithValue("p_Payment_Status", true);
            cmd.Parameters.AddWithValue("p_Actual_Payment_Date", pfDS.PF_Employee_Payment[0].Actual_Payment_Date);

            if (!pfDS.PF_Employee_Payment[0].IsPayment_ReferenceNull()) cmd.Parameters.AddWithValue("p_Payment_Reference", pfDS.PF_Employee_Payment[0].Payment_Reference);
            else cmd.Parameters.AddWithValue("p_Payment_Reference", DBNull.Value);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_EMPLOYEE_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }

        private DataSet _GetFundHeadList_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetFundHeadlist_PF");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Fund_Head.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetFundHeadList_PF_ForEmpJournal(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetFundHeadlist_PF_For_EmpJournal");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Fund_Head.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_FUND_HEAD_LIST_FOR_EMP_JOURNAL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetFundRecieveDetails_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataDateDS dateDS = new DataDateDS();
            DataStringDS stringDS = new DataStringDS();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            long companyID = 0;
            if (stringDS.DataStrings[1].StringValue != "0") companyID = UtilDL.GetCompanyId(connDS, stringDS.DataStrings[1].StringValue);

            cmd = DBCommandProvider.GetDBCommand("GetFundRecieveDetails_PF");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["SalaryDate1"].Value = Convert.ToDateTime(dateDS.DataDates[0].DateValue);
            cmd.Parameters["SalaryDate2"].Value = Convert.ToDateTime(dateDS.DataDates[0].DateValue);
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["CompanyID1"].Value = companyID;
            cmd.Parameters["CompanyID2"].Value = companyID;

            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_FUND_RECIEVE_DETAILS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _FundRecieve_PF(DataSet inputDS)
        {
            #region wali vai was done

            //ErrorDS errDS = new ErrorDS();
            //DBConnectionDS connDS = new DBConnectionDS();
            //ProvidentFundDS pfDS = new ProvidentFundDS();
            //OleDbCommand cmd = new OleDbCommand();
            //OleDbCommand cmd_Del = new OleDbCommand();
            //bool bError;
            //int nRowAffected;

            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();
            //pfDS.Merge(inputDS.Tables[pfDS.PF_Head_GL.TableName], false, MissingSchemaAction.Error);
            //pfDS.AcceptChanges();

            //#region First Delete all prior FundRecieve of this SalaryMonth
            //cmd_Del.CommandText = "PRO_WP_PF_FUND_RECIEVE_DEL";
            //cmd_Del.CommandType = CommandType.StoredProcedure;
            //if (cmd_Del == null) return UtilDL.GetCommandNotFound();

            //cmd_Del.Parameters.AddWithValue("p_SalaryDate", pfDS.PF_Head_GL[0].SalaryDate);
            //bError = false;
            //nRowAffected = -1;

            //nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);
            //cmd_Del.Parameters.Clear();
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.ACTION_PF_FUND_RECIEVE.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}
            //#endregion

            //#region Then, Create new Fund Recieve
            //cmd.CommandText = "PRO_WP_PF_FUND_RECIEVE";
            //cmd.CommandType = CommandType.StoredProcedure;

            //foreach (ProvidentFundDS.PF_Head_GLRow row in pfDS.PF_Head_GL.Rows)
            //{
            //    cmd.Parameters.AddWithValue("p_HeadID", row.HeadID);
            //    cmd.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
            //    cmd.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
            //    cmd.Parameters.AddWithValue("p_Ref_EmployeeID", row.Ref_EmployeeID);

            //    if (!row.IsRef_OtherNull()) cmd.Parameters.AddWithValue("p_Ref_Other", row.Ref_Other);
            //    else cmd.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);

            //    cmd.Parameters.AddWithValue("p_SalaryDate", row.SalaryDate);
            //    cmd.Parameters.AddWithValue("p_Calculated_GL_Amount", row.GL_Amount);     // WALI :: 09-Mar-2015

            //    bError = false;
            //    nRowAffected = -1;

            //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            //    cmd.Parameters.Clear();
            //    if (bError == true)
            //    {
            //        ErrorDS.Error err = errDS.Errors.NewError();
            //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //        err.ErrorInfo1 = ActionID.ACTION_PF_FUND_RECIEVE.ToString();
            //        errDS.Errors.AddError(err);
            //        errDS.AcceptChanges();
            //        return errDS;
            //    }
            //}
            //#endregion
            //return errDS;

            #endregion

            #region Shakir 30-12-15
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "Pro_PF_FUND_RECEIVE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_SalaryDate", Convert.ToDateTime(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_Event_Date", Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_Ref_Other", stringDS.DataStrings[2].StringValue);
            cmd.Parameters.AddWithValue("p_SiteCode", stringDS.DataStrings[3].StringValue);
            cmd.Parameters.AddWithValue("p_CompanyCode", stringDS.DataStrings[4].StringValue);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_FUND_RECIEVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
            #endregion
        }

        private DataSet _GetProvisionCalculatedData_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetProvisionCalculatedData_PF");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["ProvisionDate"].Value = stringDS.DataStrings[0].StringValue;
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_PROVISION_CALCULATED_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _SaveProvisionCalculatedData_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            ProvidentFundDS pfDS = new ProvidentFundDS();

            pfDS.Merge(inputDS.Tables[pfDS.InvestmentProvision.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_PROVISION_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            string ProvisionDate = stringDS.DataStrings[0].StringValue + "/01/" + stringDS.DataStrings[1].StringValue + " 12:00:01 AM";

            foreach (ProvidentFundDS.InvestmentProvisionRow row in pfDS.InvestmentProvision.Rows)
            {
                cmd.Parameters.AddWithValue("p_INVID", row.InvID);
                cmd.Parameters.AddWithValue("p_ProvisionDate", Convert.ToDateTime(ProvisionDate));
                cmd.Parameters.AddWithValue("p_Day_P", row.Day_P);
                cmd.Parameters.AddWithValue("p_Profit_P", row.Profit_P);
                cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[2].StringValue);
                bool bError = false;
                int nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_SAVE_PROVISION_CALCULATED_DATA.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;
        }
        private DataSet _GetProvisionedProfit_ForEmployee_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetProvisionedProfit_ForEmployee_PF");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["p_SettledInvOnly"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_PROVISIONED_PROFIT_FOR_EMPLOYEE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _DistributeProvisionedProfit_PF(DataSet inputDS)
        {

            #region Shakir 30-12-15
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Upd = new OleDbCommand();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;


            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "Pro_PF_INV_PROFIT_DISTRIBUTION";
            cmd.CommandType = CommandType.StoredProcedure;


            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_Event_Date", Convert.ToDateTime(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_SettledInvOnly", Convert.ToInt32(stringDS.DataStrings[1].StringValue));


            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_DISTRIBUTE_PROVISIONED_PROFIT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
            #endregion
        }
        private DataSet GetDistributedProvisionData_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetDistributedProvisionData_PF");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_DISTRIBUTED_PROVISION_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetPendingDistributions_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetPendingDistributions_PF");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_PENDING_DISTRIBUTION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _FundJournalEntry_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_JOURNAL_ENTRY";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_HeadID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_GL_Amount", Convert.ToDecimal(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_Event_Date", Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture));
            cmd.Parameters.AddWithValue("p_Ref_EmployeeID", DBNull.Value);

            if (stringDS.DataStrings[3].StringValue.Trim() == "") cmd.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);
            else cmd.Parameters.AddWithValue("p_Ref_Other", stringDS.DataStrings[3].StringValue);

            cmd.Parameters.AddWithValue("p_SalaryDate", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Calculated_GL_Amount", Convert.ToDecimal(stringDS.DataStrings[1].StringValue));     // WALI :: 09-Mar-2015

            if (stringDS.DataStrings[4].StringValue != "0") cmd.Parameters.AddWithValue("p_descriptionID", Convert.ToInt32(stringDS.DataStrings[4].StringValue));
            else cmd.Parameters.AddWithValue("p_descriptionID", DBNull.Value);

            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_FUND_JOURNAL_ENTRY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            if (stringDS.DataStrings[5].StringValue == "True") 
            {
                OleDbCommand cmd1 = new OleDbCommand();
                cmd1.CommandText = "Pro_PF_EMP_JOURNAL_ENTRY";
                cmd1.CommandType = CommandType.StoredProcedure;

                if (cmd1 == null) return UtilDL.GetCommandNotFound();


                cmd1.Parameters.AddWithValue("p_SalaryYearMonthDate", Convert.ToDateTime(stringDS.DataStrings[7].StringValue));
                cmd1.Parameters.AddWithValue("p_PF_membershipDate", Convert.ToDateTime(stringDS.DataStrings[8].StringValue));
                cmd1.Parameters.AddWithValue("p_TransactionDate", Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture));
                cmd1.Parameters.AddWithValue("p_FundHeadID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
                cmd1.Parameters.AddWithValue("p_WithSeperated", Convert.ToBoolean(stringDS.DataStrings[6].StringValue));
                cmd1.Parameters.AddWithValue("p_TransactionAmount", Convert.ToDecimal(stringDS.DataStrings[9].StringValue));
                if (stringDS.DataStrings[4].StringValue != "0") cmd1.Parameters.AddWithValue("p_DescriptionID", Convert.ToInt32(stringDS.DataStrings[4].StringValue));
                else cmd1.Parameters.AddWithValue("p_DescriptionID", DBNull.Value);

                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd1, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_EMPLOYEE_JOURNAL_ENTRY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            return errDS;
        }
        private DataSet _GetHeadWiseAvailableAmount_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetHeadWiseAvailableAmount_ByFundYear_PF");
            adapter.SelectCommand = cmd;

            cmd.Parameters["HeadID1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["DescriptionID1"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["DescriptionID2"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["HeadID2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["FundYear"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["DescriptionID3"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["DescriptionID4"].Value = integerDS.DataIntegers[2].IntegerValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Fund_Head.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_HEADWISE_AVAILABLE_AMOUNT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetEmployeeList_By_PFMembDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataDateDS dateDS = new DataDateDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_By_PFMembDate");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["PFMembDate1"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmpStatus1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmpStatus2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PFMembDate2"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmpStatus3"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmpStatus4"].Value = integerDS.DataIntegers[0].IntegerValue;

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_JOURNAL_EMPLOYEE_BY_PF_MEMBDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _EmployeeJournalEntry_PF(DataSet inputDS)
        {

            #region Shakir 28-12-15

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;


            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "Pro_PF_EMP_JOURNAL_ENTRY";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();


            cmd.Parameters.AddWithValue("p_SalaryYearMonthDate", Convert.ToDateTime(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_PF_membershipDate", Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_TransactionDate", Convert.ToDateTime(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_FundHeadID", Convert.ToInt32(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_WithSeperated", Convert.ToBoolean(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_TransactionAmount", Convert.ToDecimal(stringDS.DataStrings[5].StringValue));
            if (stringDS.DataStrings[6].StringValue != "0") cmd.Parameters.AddWithValue("p_DescriptionID", Convert.ToInt32(stringDS.DataStrings[6].StringValue));
            else cmd.Parameters.AddWithValue("p_DescriptionID", DBNull.Value);

            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_EMPLOYEE_JOURNAL_ENTRY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            return errDS;
            #endregion
        }

        private DataSet _GetDistinctFundYear_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetDistinctFundYear_PF");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, integerDS, integerDS.DataIntegers.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_DISTINCT_FUND_YEAR_PF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            integerDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(integerDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetRecievedFundList_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetRecievedFundList_PF");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, dateDS, dateDS.DataDates.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_RECIEVED_FUND_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            dateDS.AcceptChanges();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(dateDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet GetTotalPayableToLSC_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetTotalPayableToLSC_PF");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_TOTAL_PAYABLE_TO_LSC.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        #region Balance Modification :: WALI :: 05-Mar-2015
        private DataSet _GetEmployeeBalanceRecord_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "Principal") cmd = DBCommandProvider.GetDBCommand("GetEmployeePrincipalAmounts_PF");
            else if (stringDS.DataStrings[0].StringValue == "Profit") cmd = DBCommandProvider.GetDBCommand("GetEmployeeProvisionDistributions_PF");
            else if (stringDS.DataStrings[0].StringValue == "Others") cmd = DBCommandProvider.GetDBCommand("GetEmployeeOtherAmounts_PF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[6].StringValue, CultureInfo.InvariantCulture);
            cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[7].StringValue, CultureInfo.InvariantCulture);
            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[2].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[5].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[5].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_EMPLOYEE_BALANCE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ModifyEmployeeBalanceRecord_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            MessageDS messageDS = new MessageDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            pfDS.Merge(inputDS.Tables[pfDS.PF_Employee.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "Principal")
            {
                #region Modify Principal Amount(s)
                cmd.CommandText = "PRO_WP_PF_EDIT_EMP_PRINCIPAL";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (ProvidentFundDS.PF_EmployeeRow row in pfDS.PF_Employee.Rows)
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_SalaryDate", row.Event_Date);
                    cmd.Parameters.AddWithValue("p_Changed_PFComp", row.CompProfit);
                    cmd.Parameters.AddWithValue("p_Changed_PFSelf", row.SelfProfit);
                    cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PF_MODIFY_EMPLOYEE_BALANCE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                }
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Profit")
            {
                #region Modify Profit Amount(s)
                cmd.CommandText = "PRO_WP_PF_EDIT_EMP_PROFIT";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (ProvidentFundDS.PF_EmployeeRow row in pfDS.PF_Employee.Rows)
                {
                    cmd.Parameters.AddWithValue("p_ProfitGLID", row.Row_ID);
                    cmd.Parameters.AddWithValue("p_Changed_PFComp", row.CompProfit);
                    cmd.Parameters.AddWithValue("p_Changed_PFSelf", row.SelfProfit);
                    cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PF_MODIFY_EMPLOYEE_BALANCE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                }
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Others")
            {
                #region Modify Other Amount(s)
                cmd.CommandText = "PRO_WP_PF_EDIT_EMP_OTHER_AMNT";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (ProvidentFundDS.PF_EmployeeRow row in pfDS.PF_Employee.Rows)
                {
                    cmd.Parameters.AddWithValue("p_ProfitGLID", row.Row_ID);
                    cmd.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                    cmd.Parameters.AddWithValue("p_Changed_PFComp", row.CompProfit);
                    cmd.Parameters.AddWithValue("p_Changed_PFSelf", row.SelfProfit);
                    cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PF_MODIFY_EMPLOYEE_BALANCE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                }
                #endregion
            }

            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetFundHeadBalanceRecord_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetFundHeadBalanceRecord_PF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["HeadID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue, CultureInfo.InvariantCulture);
            cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_FUND_BALANCE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ModifyFundHeadBalanceRecord_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            MessageDS messageDS = new MessageDS();
            bool bError = false;
            int nRowAffected = -1;

            pfDS.Merge(inputDS.Tables[pfDS.PF_Head_GL.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_EDIT_FUND_BALANCE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ProvidentFundDS.PF_Head_GLRow row in pfDS.PF_Head_GL.Rows)
            {
                cmd.Parameters.AddWithValue("p_HeadGLID", row.Row_ID);
                cmd.Parameters.AddWithValue("p_Changed_GL_Amount", row.GL_Amount);
                cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_MODIFY_EMPLOYEE_BALANCE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.Event_Date.ToString("dd-MMM-yyyy") + "  [" + row.Description + "]");
                else messageDS.ErrorMsg.AddErrorMsgRow(row.Event_Date.ToString("dd-MMM-yyyy") + "  [" + row.Description + "]");
            }

            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        private DataSet _GetBeneficiaryList_To_FundRecieve_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetBeneficiaryList_To_FundRecieve_PF");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            long siteID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[2].StringValue);

            cmd.Parameters["Salarydate"].Value = DateTime.ParseExact(stringDS.DataStrings[6].StringValue, "dd-MM-yyyy", CultureInfo.InvariantCulture);
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue.ToUpper() + "%";
            cmd.Parameters["SiteID1"].Value = siteID;
            cmd.Parameters["SiteID2"].Value = siteID;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["EmployeeStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            cmd.Parameters["EmployeeStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);


            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_BENEFICIARY_LIST_TO_FUND_RECIEVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _Exceptional_FundRecieve_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            pfDS.Merge(inputDS.Tables[pfDS.PF_Head_GL.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            cmd_Del.CommandText = "PRO_WP_PF_FUND_RECIEVE_DEL_EMP";
            cmd_Del.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRO_WP_PF_FUND_RECIEVE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd_Del == null || cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ProvidentFundDS.PF_Head_GLRow row in pfDS.PF_Head_GL.Rows)
            {
                #region First Delete prior FundRecieve of this (SalaryMonth+Employee)
                cmd_Del.Parameters.AddWithValue("p_SalaryDate", row.SalaryDate);
                cmd_Del.Parameters.AddWithValue("p_EmployeeID", row.Ref_EmployeeID);
                cmd_Del.Parameters.AddWithValue("p_HeadID", row.HeadID);
                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Del.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_FUND_RECIEVE_EMPLOYEE_WISE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                #region Then, Create new Fund Recieve
                cmd.Parameters.AddWithValue("p_HeadID", row.HeadID);
                cmd.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                cmd.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                cmd.Parameters.AddWithValue("p_Ref_EmployeeID", row.Ref_EmployeeID);

                if (!row.IsRef_OtherNull()) cmd.Parameters.AddWithValue("p_Ref_Other", row.Ref_Other);
                else cmd.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);

                cmd.Parameters.AddWithValue("p_SalaryDate", row.SalaryDate);
                cmd.Parameters.AddWithValue("p_Calculated_GL_Amount", row.GL_Amount);     // WALI :: 09-Mar-2015

                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_FUND_RECIEVE_EMPLOYEE_WISE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion
            }
            return errDS;
        }
        private DataSet _GetPF_EmpListForGenaralPayment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPF_EmpListForGenaralPayment");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_EMPLOYEE_LIST_FOR_SEPERATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _PF_Payment_General(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            OleDbCommand cmd_Sep = new OleDbCommand();
            OleDbCommand cmd_Head = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            pfDS.Merge(inputDS.Tables[pfDS.PF_Employee_Payment.TableName], false, MissingSchemaAction.Error);
            pfDS.Merge(inputDS.Tables[pfDS.PF_Head_GL.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            cmd_Sep.CommandText = "PRO_PF_PAYMENT_GENERAL";
            cmd_Sep.CommandType = CommandType.StoredProcedure;

            cmd_Head.CommandText = "PRO_WP_PF_HEADGL_ADD_FOR_LF";
            cmd_Head.CommandType = CommandType.StoredProcedure;

            if (cmd_Sep == null || cmd_Head == null) return UtilDL.GetCommandNotFound();

            #region Employee Seperation
            cmd_Sep.Parameters.AddWithValue("p_EmployeeID", pfDS.PF_Employee_Payment[0].EmployeeID);
            cmd_Sep.Parameters.AddWithValue("p_PF_Comp", pfDS.PF_Employee_Payment[0].PF_Comp);
            cmd_Sep.Parameters.AddWithValue("p_PF_Self", pfDS.PF_Employee_Payment[0].PF_Self);
            cmd_Sep.Parameters.AddWithValue("p_PF_Comp_Profit", pfDS.PF_Employee_Payment[0].PF_Comp_Profit);
            cmd_Sep.Parameters.AddWithValue("p_PF_Self_Profit", pfDS.PF_Employee_Payment[0].PF_Self_Profit);
            cmd_Sep.Parameters.AddWithValue("p_Payment_Date", pfDS.PF_Employee_Payment[0].SeperationDate);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Sep, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd_Sep.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PF_EMPLOYEE_SEPARATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region For Differences [Forfeirute]
            if (pfDS.PF_Head_GL.Rows.Count > 0)
            {
                foreach (ProvidentFundDS.PF_Head_GLRow row in pfDS.PF_Head_GL.Rows)
                {
                    cmd_Head.Parameters.AddWithValue("p_RefHeadCode", row.RefHeadCode);
                    cmd_Head.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                    cmd_Head.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                    cmd_Head.Parameters.AddWithValue("p_Ref_EmployeeID", row.Ref_EmployeeID);
                    cmd_Head.Parameters.AddWithValue("p_Calculated_GL_Amount", row.GL_Amount);      // WALI :: 09-Mar-2015

                    bError = false;
                    nRowAffected = -1;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Head, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Head.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PF_EMPLOYEE_SEPARATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion

            #region Fund Journal Add for 'PAYABLE TO LSC'
            //if (pfDS.PF_Employee_Payment[0].Payment_Source == "Donor")
            //{
            //    OleDbCommand cmd_Fund = new OleDbCommand();
            //    cmd_Fund.CommandText = "PRO_WP_PF_FUND_RECIEVE";
            //    cmd_Fund.CommandType = CommandType.StoredProcedure;
            //    if (cmd_Fund == null) return UtilDL.GetCommandNotFound();

            //    decimal gl_Amount = pfDS.PF_Employee_Payment[0].PF_Comp + pfDS.PF_Employee_Payment[0].PF_Self +
            //                        pfDS.PF_Employee_Payment[0].PF_Comp_Profit + pfDS.PF_Employee_Payment[0].PF_Self_Profit;

            //    cmd_Fund.Parameters.AddWithValue("p_HeadID", 108);          // HeadID for 'Payable to LSC'
            //    cmd_Fund.Parameters.AddWithValue("p_GL_Amount", gl_Amount);
            //    cmd_Fund.Parameters.AddWithValue("p_Event_Date", pfDS.PF_Employee_Payment[0].SeperationDate);
            //    cmd_Fund.Parameters.AddWithValue("p_Ref_EmployeeID", pfDS.PF_Employee_Payment[0].EmployeeID);

            //    if (!pfDS.PF_Employee_Payment[0].IsDonor_ReferenceNull()) cmd_Fund.Parameters.AddWithValue("p_Ref_Other", pfDS.PF_Employee_Payment[0].Donor_Reference);
            //    else cmd_Fund.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);

            //    cmd_Fund.Parameters.AddWithValue("p_SalaryDate", DBNull.Value);
            //    cmd_Fund.Parameters.AddWithValue("p_Calculated_GL_Amount", gl_Amount);     // WALI :: 09-Mar-2015

            //    bError = false;
            //    nRowAffected = -1;

            //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Fund, connDS.DBConnections[0].ConnectionID, ref bError);
            //    cmd_Fund.Parameters.Clear();
            //    if (bError == true)
            //    {
            //        ErrorDS.Error err = errDS.Errors.NewError();
            //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //        err.ErrorInfo1 = ActionID.ACTION_PF_EMPLOYEE_SEPARATION.ToString();
            //        errDS.Errors.AddError(err);
            //        errDS.AcceptChanges();
            //        return errDS;
            //    }
            //}
            #endregion

            return errDS;
        }
        private DataSet _GetPF_PaymentAmountsGeneral(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPF_PaymentAmountsGeneral");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_PAYMENT_AMOUNT_GENERAL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetPF_SeparationInfo(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "PF_SeparationInfo")
            {
                cmd = DBCommandProvider.GetDBCommand("GetPF_SeparationInformtion");
                adapter.SelectCommand = cmd;

                cmd.Parameters["Date_From"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
            }



            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_SeparationInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_SEPARATION_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getInformationAny_PF(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "LoanDisbursementBranchWise")
            {
                #region Loan Disbursement Branch Wise
                cmd = DBCommandProvider.GetDBCommand("GetLoanDisbursementBranchWise");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["Date_From"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);

                cmd.Parameters["CompanyCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["CompanyCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["LocationCode1"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["LocationCode2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["ZoneCode1"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["ZoneCode2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["Code1"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["Code2"].Value = stringDS.DataStrings[7].StringValue;
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "LoanScheduleReport")
            {
                #region Loan Disbursement Branch Wise
                cmd = DBCommandProvider.GetDBCommand("GetLoanScheduleReport");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["ConcatedLoanIssueCode"].Value = stringDS.DataStrings[1].StringValue;
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "PFContributionAndCollectionBranchWise")
            {
                #region PF Contribution & Collection
                cmd = DBCommandProvider.GetDBCommand("GetPFContrAndCollectionBranchWise");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["Date_From"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);

                cmd.Parameters["CompanyCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["CompanyCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["LocationCode1"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["LocationCode2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["ZoneCode1"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["ZoneCode2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[6].StringValue;
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "PF_ReceivableFromCompany")
            {
                #region PF Contribution & Collection
                cmd = DBCommandProvider.GetDBCommand("GetPF_ReceivableFromCompany");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["Date_From1"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_From2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "PF_StaffLoan")
            {
                #region PF Contribution & Collection
                cmd = DBCommandProvider.GetDBCommand("GetPF_StaffLoan");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["Date_From1"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_From2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "EmployeePF")
            {
                #region PF Contribution & Collection
                cmd = DBCommandProvider.GetDBCommand("Get_EmployeePF");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["Date_From1"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_From2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "CashAtBank")
            {
                #region PF Contribution & Collection
                cmd = DBCommandProvider.GetDBCommand("Get_CashAtBank");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["Date_From1"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_From2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Investment")
            {
                #region PF Contribution & Collection
                cmd = DBCommandProvider.GetDBCommand("Get_PF_Investment");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["Date_From1"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_From2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Current Profit")
            {
                #region Current Profit
                cmd = DBCommandProvider.GetDBCommand("Get_CurrentProfit");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["Date_From1"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_From2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Loan Profit")
            {
                #region Current Profit
                cmd = DBCommandProvider.GetDBCommand("Get_LoanProfit");

                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["Date_From1"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_From2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "PF_JournalDescriotion_101_102")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_PF_JournalDescriotion_101_102");
                if (cmd == null) return UtilDL.GetCommandNotFound();
            }
            else if (stringDS.DataStrings[0].StringValue == "PF_JournalDescriotion_All")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_PF_JournalDescriotion_All");
                if (cmd == null) return UtilDL.GetCommandNotFound();
            }
            else if (stringDS.DataStrings[0].StringValue == "PF_JournalDesWithout_Fixed")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_PF_JournalDesWithout_Fixed");
                if (cmd == null) return UtilDL.GetCommandNotFound();
            }
            else if (stringDS.DataStrings[0].StringValue == "PF_JournalList")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_PF_JournalList");
                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["Reference1"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["Reference2"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["Date_From"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[3].StringValue);

            }
            else if (stringDS.DataStrings[0].StringValue == "PF_JournalReferenceCode")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_PF_JournalReferenceCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();
            }
            else if (stringDS.DataStrings[0].StringValue == "PF_ReceivedInfo")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_PF_ReceivedInfo");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["SalaryMonthYear1"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["SalaryMonthYear2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["SalaryMonthYear3"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["SalaryMonthYear4"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["SiteID1"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["ZoneID1"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["ZoneID2"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["LocationID1"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["LocationID2"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);

            }
            else if (stringDS.DataStrings[0].StringValue == "General_Received_Status")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_General_Received_Status");
                if (cmd == null) return UtilDL.GetCommandNotFound();
                             
                cmd.Parameters["SalaryMonthYear1"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["SalaryMonthYear2"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);

            }
            else if (stringDS.DataStrings[0].StringValue == "PF_AuditInfo")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_PF_AuditReceivedInfo");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["p_YEAR1"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["p_YEAR2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["p_YEAR3"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["p_YEAR4"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["p_YEAR5"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["p_YEAR6"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["p_YEAR7"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            }
            else if (stringDS.DataStrings[0].StringValue == "SalaryExistForSpecificMonth")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSalaryExistForSpecMonth");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["MonthYearDate"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            }
            else if (stringDS.DataStrings[0].StringValue == "SystemNotification")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSystemNotification");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["Event1"].Value = cmd.Parameters["Event2"].Value = cmd.Parameters["Event3"].Value = stringDS.DataStrings[1].StringValue;
            }
            else if (stringDS.DataStrings[0].StringValue == "CheckMailNotification")
            {
                cmd = DBCommandProvider.GetDBCommand("CheckMailNotification");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["Event1"].Value = cmd.Parameters["Event2"].Value = cmd.Parameters["Event3"].Value = stringDS.DataStrings[1].StringValue;
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            DataSet DataDS = new DataSet();
            DataDS.Tables.Add("RecordList");

            nRowAffected = ADOController.Instance.Fill(adapter, DataDS, DataDS.Tables["RecordList"].ToString(), connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_INFORMATION_ANY_PF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                returnDS.Merge(errDS);
                return returnDS;
            }
            DataDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(DataDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _FundJournalEntryMultiple_PF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            bool bError = false;
            int nRowAffected = -1;


            pfDS.Merge(inputDS.Tables[pfDS.JournalEntry.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_JOURNAL_ENTRY_MULTI";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ProvidentFundDS.JournalEntryRow row in pfDS.JournalEntry.Rows)
            {
                cmd.Parameters.AddWithValue("p_HeadID", Convert.ToInt32(row.HeadID));
                if (!row.IsDebitNull()) 
                    cmd.Parameters.AddWithValue("p_GL_Amount", Convert.ToDecimal(row.Debit));
                else cmd.Parameters.AddWithValue("p_GL_Amount", Convert.ToDecimal(row.Credit));
                cmd.Parameters.AddWithValue("p_TransectionDate", Convert.ToDateTime(row.TransectionDate));
                cmd.Parameters.AddWithValue("p_Reference", Convert.ToString(row.Reference));

                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_FUND_JOURNAL_ENTRY_MULTIPLE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            return errDS;
        }
        private DataSet _FundJournalDescriptionEntry(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            bool bError = false;
            int nRowAffected = -1;


            pfDS.Merge(inputDS.Tables[pfDS.JournalEntry.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_JOURNAL_DES_ADD";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ProvidentFundDS.JournalEntryRow row in pfDS.JournalEntry.Rows)
            {
                cmd.Parameters.AddWithValue("p_DescriptionID", IDGenerator.GetNextGenericPK());
                cmd.Parameters.AddWithValue("p_JournalDescription", row.JournalDescription);
                
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_FUND_JOURNAL_DESCRIPTION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            return errDS;
        }
        private DataSet _FundJournalDescriptionUpdate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            bool bError = false;
            int nRowAffected = -1;


            pfDS.Merge(inputDS.Tables[pfDS.JournalEntry.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_JOURNAL_DES_UPD";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ProvidentFundDS.JournalEntryRow row in pfDS.JournalEntry.Rows)
            {
                cmd.Parameters.AddWithValue("p_DescriptionID", row.DescriptionID);
                cmd.Parameters.AddWithValue("p_JournalDescription", row.JournalDescription);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_FUND_JOURNAL_DESCRIPTION_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            return errDS;
        }
        private DataSet _FundJournalDescriptionDelete(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            ProvidentFundDS pfDS = new ProvidentFundDS();
            bool bError = false;
            int nRowAffected = -1;


            pfDS.Merge(inputDS.Tables[pfDS.JournalEntry.TableName], false, MissingSchemaAction.Error);
            pfDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_WP_PF_JOURNAL_DES_DEL";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ProvidentFundDS.JournalEntryRow row in pfDS.JournalEntry.Rows)
            {                
                cmd.Parameters.AddWithValue("p_JournalDescription", row.JournalDescription);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PF_FUND_JOURNAL_DESCRIPTION_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            return errDS;
        }
    }
}
