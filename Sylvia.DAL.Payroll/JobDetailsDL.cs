using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class JobDetailsDL
    {
        public JobDetailsDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                #region Job Application
                case ActionID.ACTION_JOB_APPLICATION_CREATE:
                    return _createJobApplication(inputDS);
                case ActionID.NA_ACTION_JOB_APPLICATION_LIST_ALL:
                    return _getJobApplicationAll(inputDS);
                case ActionID.NA_ACTION_GET_JOB_APP_BY_APPID:
                    return _getJobAppByAppID(inputDS);
                #endregion

                #region Job History
                case ActionID.ACTION_JOB_HISTORY_CREATE:
                    return _createJobHistory(inputDS);
                case ActionID.NA_ACTION_GET_JOB_HISTORY_BY_LASTHISTORY:
                    return _getJobHistoryByLastHistory(inputDS);
                case ActionID.NA_ACTION_GET_JOB_HISTORY_BY_APPID:
                    return _getJobHistoryByAppID(inputDS);
                case ActionID.ACTION_JOB_HISTORY_UPD:
                    return _updateJobHistory(inputDS);
                case ActionID.ACTION_JOB_HISTORY_STATUS_UPDATE:
                    return _updateJobHistoryByActivity(inputDS);
                #endregion

                case ActionID.ACTION_JOB_APPLICATION_SHORTING:
                    return ApplicationShortListing(inputDS);

                case ActionID.ACTION_JOB_APPLICATION_SHORTING_AD:
                    return _ApplicationShortListingAd(inputDS);

                #region Job Details :: WALI :: 01-Jul-2014
                case ActionID.ACTION_JOB_DETAILS_CREATE:
                    return _createJobDetails(inputDS);
                case ActionID.NA_ACTION_GET_JOB_DETAILS_LIST:
                    return _getAllJobDetailsList(inputDS);
                case ActionID.NA_ACTION_GET_GRADEWISE_JOB_DETAILS:
                    return _getJobDetailGradeList(inputDS);
                case ActionID.ACTION_SAVE_GRADEWISE_JOB_DETAILS:
                    return _saveGradeWiseJobDetails(inputDS);
                case ActionID.NA_ACTION_GET_COMPETENCY_FOR_JOB_DETAILS:
                    return _getCompetencyForJobDetail(inputDS);
                case ActionID.ACTION_JOB_DETAILS_UPD:
                    return _updateJobDetails(inputDS);
                case ActionID.ACTION_JOB_DETAILS_DELETE:
                    return _deleteJobDetail(inputDS);
                #endregion

                #region WALI :: 20-Jun-2015
                case ActionID.ACTION_JOB_APPLICANT_CREATE:
                    return _createJobApplicant(inputDS);
                case ActionID.ACTION_JOB_APPLICANT_UPDATE:
                    return _updateJobApplicant(inputDS);

                case ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID:
                    return _getJobApplicantInfo_By_ApplicantUserID(inputDS);
                case ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_JOBAPPLICATIONID:
                    return _getJobApplicantInfo_By_JobApplicationID(inputDS);

                case ActionID.ACTION_JOB_APPLICANT_CREATE_AND_JOB_APPLY:
                    return _createJobApplicant_And_JobApply(inputDS);
                case ActionID.ACTION_JOB_APPLICANT_UPDATE_AND_JOB_APPLY:
                    return _updateJobApplicant_And_JobApply(inputDS);
                #endregion

                case ActionID.NA_ACTION_GET_JOB_APPLICANT_DETAILS:
                    return _getJobApplicantDetails(inputDS);
                case ActionID.NA_ACTION_GET_JOB_USER_HISTORY_BY_TRACKINGID:
                    return _getJobApplicantHistory_By_ApplicationTrackingID(inputDS);
                case ActionID.NA_ACTION_GET_JOB_APPLICANT_SHORTLISTING:
                    return _getJobApplicant_ShortListing(inputDS);
                case ActionID.ACTION_JOB_EXAM_ORGANIZER:
                    return _createJob_ExamOrganizer(inputDS);
                case ActionID.NA_ACTION_JOB_EXAM_ORGANIZER:
                    return _getJob_ExamOrganizer(inputDS);
                case ActionID.ACTION_JOB_EXAM_RESULT_UPLOAD:
                    return _createJob_ExamResultUpload(inputDS);
                case ActionID.ACTION_JOB_EXAM_ORGANIZER_DELETE:
                    return _deleteJobExamOrganizer(inputDS);
                case ActionID.ACTION_EXAMHALL_INFO_ADD:
                    return _createExamHallInfoUpload(inputDS);
                case ActionID.NA_ACTION_GET_EXAM_INFO_ALL:
                    return _getExamInfoAll(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_LIST_REC:
                    return _getEmpolyeeListLatest(inputDS);
                case ActionID.NA_ACTION_DOES_ROLLNUMBER_EXIST:
                    return _doesRollNumberExist(inputDS);
                case ActionID.NA_ACTION_DOES_APPLICANT_DATA_EXIST_IN_EXAM_RESULT:
                    return _doesApplicantDataExistInExamResult(inputDS);
                case ActionID.NA_ACTION_GET_ATTENDANCE_SHEET:
                    return _getAttendanceSheet(inputDS);
                case ActionID.NA_ACTION_GET_JOB_APPLICANT_SHORTLISTING_AD:
                    return _getJobApplicant_ShortListingAdvanced(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }


        #region Job Application
        private DataSet _createJobApplication(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "pro_job_application_create";
            cmd.CommandType = CommandType.StoredProcedure;


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Application.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            // Update by Asif, 11-mar-12
            string JobApplicationID = "";
            // Update end

            foreach (JobDetailsDS.Job_ApplicationRow row in jobDetailsDS.Job_Application.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                // Update by Asif, 11-mar-12
                JobApplicationID = genPK.ToString();
                // Update end

                cmd.Parameters.AddWithValue("p_jobapplicationid", genPK);

                if (row.IsAPPLICANTNAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_applicantname", row.APPLICANTNAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_employeename", DBNull.Value);
                }

                if (row.IsPHOTOGRAPHNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_photograph", row.PHOTOGRAPH);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_photograph", DBNull.Value);
                }

                if (row.IsGENDERNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_gender", row.GENDER);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_gender", DBNull.Value);
                }

                if (row.IsDATEOFBIRTHNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_dateofbirth", row.DATEOFBIRTH);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_dateofbirth", DBNull.Value);
                }

                if (row.IsPRESENTADDRESSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_presentaddress", row.PRESENTADDRESS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_presentaddress", DBNull.Value);
                }

                if (row.IsPERMANENTADDRESSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_permanentaddress", row.PERMANENTADDRESS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_permanentaddress", DBNull.Value);
                }

                if (row.IsTELEPHONENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_telephone", row.TELEPHONE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_telephone", DBNull.Value);
                }

                if (row.IsMOBILENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_mobile", row.MOBILE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_mobile", DBNull.Value);
                }

                if (row.IsEMAILNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_email", row.EMAIL);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_email", DBNull.Value);
                }

                if (row.IsNATIONALITYNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_nationality", row.NATIONALITY);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_nationality", DBNull.Value);
                }

                if (row.IsPLACEOFBIRTHNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_placeofbirth", row.PLACEOFBIRTH);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_placeofbirth", DBNull.Value);
                }

                if (row.IsRELIGIONNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_religion", row.RELIGION);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_religion", DBNull.Value);
                }

                if (row.IsMARITALSTATUSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_maritalstatus", row.MARITALSTATUS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_maritalstatus", DBNull.Value);
                }

                if (row.IsBLOODGROUPNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_bloodgroup", row.BLOODGROUP);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_bloodgroup", DBNull.Value);
                }

                if (row.IsPASSPORTNONull() == false)
                {
                    cmd.Parameters.AddWithValue("p_passportno", row.PASSPORTNO);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_passportno", DBNull.Value);
                }

                if (row.IsTINNONull() == false)
                {
                    cmd.Parameters.AddWithValue("p_tinno", row.TINNO);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_tinno", DBNull.Value);
                }

                if (row.IsFATHERNAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_fathername", row.FATHERNAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_fathername", DBNull.Value);
                }

                if (row.IsMOTHERNAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_mothername", row.MOTHERNAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_mothername", DBNull.Value);
                }

                if (row.IsNATIONALIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_nationalid", row.NATIONALID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_nationalid", DBNull.Value);
                }

                if (row.IsCITYIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_cityid", row.CITYID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_cityid", DBNull.Value);
                }

                if (row.IsCOUNTRYCODENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_countrycode", row.COUNTRYCODE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_countrycode", DBNull.Value);
                }

                if (row.IsRESUMEFILEPATHNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_resumefilepath", row.RESUMEFILEPATH);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_resumefilepath", DBNull.Value);
                }

                if (row.IsVACANCYIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_vacancyid", row.VACANCYID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_vacancyid", DBNull.Value);
                }

                if (row.IsYearOfExperienceNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_YearOfExperience", row.YearOfExperience);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_YearOfExperience", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            #region Applicant Eduinformation
            OleDbCommand cmd2 = DBCommandProvider.GetDBCommand("JobApplicantEduInfoCreate");
            //OleDbCommand cmd2 = new OleDbCommand();
            //cmd2.CommandText = "Pro_JobApplicantEduInfoCreate";
            //cmd2.CommandType = CommandType.StoredProcedure;


            if (cmd2 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            foreach (JobDetailsDS.Job_Applicant_EduInfoRow row in jobDetailsDS.Job_Applicant_EduInfo.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd2.Parameters["EDUCATIONID"].Value = (object)genPK;

                cmd2.Parameters["JOBAPPLICATIONID"].Value = (object)Convert.ToInt32(JobApplicationID);



                if (row.IsExamNameNull() == false) cmd2.Parameters["EXAMNAME"].Value = (object)row.ExamName;
                else cmd2.Parameters["EXAMNAME"].Value = DBNull.Value;

                if (row.IsPassingYearNull() == false) cmd2.Parameters["PASSINGYEAR"].Value = (object)row.PassingYear;
                else cmd2.Parameters["PASSINGYEAR"].Value = DBNull.Value;

                if (row.IsSubjectGroupNull() == false) cmd2.Parameters["SUBJECTGROUP"].Value = (object)row.SubjectGroup;
                else cmd2.Parameters["SUBJECTGROUP"].Value = DBNull.Value;

                if (row.IsResultTypeNull() == false) cmd2.Parameters["RESULTTYPE"].Value = (object)row.ResultType;
                else cmd2.Parameters["RESULTTYPE"].Value = DBNull.Value;

                if (row.IsDivisionResultNull() == false) cmd2.Parameters["DIVISIONRESULT"].Value = (object)row.DivisionResult;
                else cmd2.Parameters["DIVISIONRESULT"].Value = DBNull.Value;

                if (row.IsGPAreasultNull() == false) cmd2.Parameters["GPAREASULT"].Value = (object)row.GPAreasult;
                else cmd2.Parameters["GPAREASULT"].Value = DBNull.Value;

                //if (row.IsGPAOutofNull() == false) cmd2.Parameters["GPAOUTOF"].Value = (object)row.GPAoutof;
                //else cmd2.Parameters["GPAOUTOF"].Value = DBNull.Value;

                if (row.IsInstituteNull() == false) cmd2.Parameters["INSTITUTE"].Value = (object)row.Institute;
                else cmd2.Parameters["INSTITUTE"].Value = DBNull.Value;

                if (row.IsBoardUnivercityNull() == false) cmd2.Parameters["BOARDUNIVERCITY"].Value = (object)row.BoardUnivercity;
                else cmd2.Parameters["BOARDUNIVERCITY"].Value = DBNull.Value;



                //cmd2.Parameters.AddWithValue("EDUCATIONID", genPK);
                //cmd2.Parameters.AddWithValue("JOBAPPLICATIONID",Convert.ToInt32(JobApplicationID));                

                //if (row.IsExamNameNull() == false) cmd2.Parameters.AddWithValue("EXAMNAME", row.ExamName);
                //else cmd2.Parameters.AddWithValue("EXAMNAME", DBNull.Value);

                //if (row.IsPassingYearNull() == false) cmd2.Parameters.AddWithValue("PASSINGYEAR", row.PassingYear);
                //else cmd2.Parameters.AddWithValue("PASSINGYEAR", DBNull.Value);

                //if (row.IsSubjectGroupNull() == false) cmd2.Parameters.AddWithValue("SUBJECTGROUP", row.SubjectGroup);
                //else cmd2.Parameters.AddWithValue("SUBJECTGROUP", DBNull.Value);

                //if (row.IsResultTypeNull() == false) cmd2.Parameters.AddWithValue("RESULTTYPE", row.ResultType);
                //else cmd2.Parameters.AddWithValue("RESULTTYPE", DBNull.Value);

                //if (row.IsDivisionResultNull() == false) cmd2.Parameters.AddWithValue("DIVISIONRESULT", row.DivisionResult);
                //else cmd2.Parameters.AddWithValue("DIVISIONRESULT", " ");

                //if (row.IsGPAreasultNull() == false) cmd2.Parameters.AddWithValue("GPAREASULT", row.GPAreasult);
                //else cmd2.Parameters.AddWithValue("GPAREASULT", " ");

                //if (row.IsGPAoutofNull() == false) cmd2.Parameters.AddWithValue("GPAOUTOF", row.GPAoutof);
                //else cmd2.Parameters.AddWithValue("GPAOUTOF", DBNull.Value);

                //if (row.IsInstituteNull() == false) cmd2.Parameters.AddWithValue("INSTITUTE", row.Institute);
                //else cmd2.Parameters.AddWithValue("INSTITUTE", DBNull.Value);

                //if (row.IsBoardUnivercityNull() == false) cmd2.Parameters.AddWithValue("BOARDUNIVERCITY", row.BoardUnivercity);
                //else cmd2.Parameters.AddWithValue("BOARDUNIVERCITY", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd2, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            #endregion



            errDS.Clear();
            errDS.AcceptChanges();
            // Update by Asif, 11-mar-12
            //return errDS;
            //Update end

            // Update by Asif, 11-mar-12
            DataStringDS stringDS1 = new DataStringDS();
            stringDS1.DataStrings.AddDataString(JobApplicationID);
            stringDS1.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(stringDS1);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
            // Update end
        }

        private DataSet _getJobApplicationAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetJobVacancyAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            JobDetailsPO jobDetailsPO = new JobDetailsPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsPO, jobDetailsPO.Job_Application.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_JOB_APPLICATION_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            jobDetailsPO.AcceptChanges();

            JobDetailsDS JobDetailsDS = new JobDetailsDS();
            if (jobDetailsPO.Job_Application.Count > 0)
            {
                foreach (JobDetailsPO.Job_ApplicationRow poRow in jobDetailsPO.Job_Application.Rows)
                {
                    JobDetailsDS.Job_ApplicationRow row = JobDetailsDS.Job_Application.NewJob_ApplicationRow();

                    if (poRow.IsJOBAPPLICATIONIDNull() == false)
                    {
                        row.JOBAPPLICATIONID = poRow.JOBAPPLICATIONID;
                    }
                    if (poRow.IsAPPLICANTNAMENull() == false)
                    {
                        row.APPLICANTNAME = poRow.APPLICANTNAME;
                    }
                    if (poRow.IsPHOTOGRAPHNull() == false)
                    {
                        row.PHOTOGRAPH = poRow.PHOTOGRAPH;
                    }
                    if (poRow.IsGENDERNull() == false)
                    {
                        row.GENDER = poRow.GENDER;
                    }
                    if (poRow.IsDATEOFBIRTHNull() == false)
                    {
                        row.DATEOFBIRTH = poRow.DATEOFBIRTH;
                    }
                    if (poRow.IsPRESENTADDRESSNull() == false)
                    {
                        row.PRESENTADDRESS = poRow.PRESENTADDRESS;
                    }
                    if (poRow.IsPERMANENTADDRESSNull() == false)
                    {
                        row.PERMANENTADDRESS = poRow.PERMANENTADDRESS;
                    }
                    if (poRow.IsTELEPHONENull() == false)
                    {
                        row.TELEPHONE = poRow.TELEPHONE;
                    }
                    if (poRow.IsMOBILENull() == false)
                    {
                        row.MOBILE = poRow.MOBILE;
                    }
                    if (poRow.IsEMAILNull() == false)
                    {
                        row.EMAIL = poRow.EMAIL;
                    }
                    if (poRow.IsNATIONALITYNull() == false)
                    {
                        row.NATIONALITY = poRow.NATIONALITY;
                    }
                    if (poRow.IsPLACEOFBIRTHNull() == false)
                    {
                        row.PLACEOFBIRTH = poRow.PLACEOFBIRTH;
                    }
                    if (poRow.IsRELIGIONNull() == false)
                    {
                        row.RELIGION = poRow.RELIGION;
                    }
                    if (poRow.IsMARITALSTATUSNull() == false)
                    {
                        row.MARITALSTATUS = poRow.MARITALSTATUS;
                    }
                    if (poRow.IsBLOODGROUPNull() == false)
                    {
                        row.BLOODGROUP = poRow.BLOODGROUP;
                    }
                    if (poRow.IsPASSPORTNONull() == false)
                    {
                        row.PASSPORTNO = poRow.PASSPORTNO;
                    }
                    if (poRow.IsTINNONull() == false)
                    {
                        row.TINNO = poRow.TINNO;
                    }
                    if (poRow.IsFATHERNAMENull() == false)
                    {
                        row.FATHERNAME = poRow.FATHERNAME;
                    }
                    if (poRow.IsMOTHERNAMENull() == false)
                    {
                        row.MOTHERNAME = poRow.MOTHERNAME;
                    }
                    if (poRow.IsNATIONALIDNull() == false)
                    {
                        row.NATIONALID = poRow.NATIONALID;
                    }
                    if (poRow.IsCITYIDNull() == false)
                    {
                        row.CITYID = poRow.CITYID;
                    }
                    if (poRow.IsCOUNTRYCODENull() == false)
                    {
                        row.COUNTRYCODE = poRow.COUNTRYCODE;
                    }
                    if (poRow.IsRESUMEFILEPATHNull() == false)
                    {
                        row.RESUMEFILEPATH = poRow.RESUMEFILEPATH;
                    }
                    if (poRow.IsVACANCYIDNull() == false)
                    {
                        row.VACANCYID = poRow.VACANCYID;
                    }
                    if (poRow.IsCityNameNull() == false)
                    {
                        row.CityName = poRow.CityName;
                    }
                    if (poRow.IsCountryNameNull() == false)
                    {
                        row.CountryName = poRow.CountryName;
                    }
                    if (poRow.IsJobTitleNull() == false)
                    {
                        row.JobTitle = poRow.JobTitle;
                    }
                    if (poRow.IsJobTrackingIDNull() == false)
                    {
                        row.JobTrackingID = poRow.JobTrackingID;
                    }
                    if (poRow.IsActivitiesIDNull() == false)
                    {
                        row.ActivitiesID = poRow.ActivitiesID;
                    }

                    JobDetailsDS.Job_Application.AddJob_ApplicationRow(row);
                    JobDetailsDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(JobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getJobAppByAppID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            int jobAppID = integerDS.DataIntegers[0].IntegerValue;



            cmd = DBCommandProvider.GetDBCommand("GetJobAppByAppID");
            cmd.Parameters["JobAppID"].Value = jobAppID;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            JobDetailsPO jobDetailsPO = new JobDetailsPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsPO, jobDetailsPO.Job_Application.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APP_BY_APPID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            jobDetailsPO.AcceptChanges();

            JobDetailsDS JobDetailsDS = new JobDetailsDS();
            if (jobDetailsPO.Job_Application.Count > 0)
            {
                foreach (JobDetailsPO.Job_ApplicationRow poRow in jobDetailsPO.Job_Application.Rows)
                {
                    JobDetailsDS.Job_ApplicationRow row = JobDetailsDS.Job_Application.NewJob_ApplicationRow();

                    if (poRow.IsYearOfExperienceNull() == false)
                    {
                        row.YearOfExperience = poRow.YearOfExperience;
                    }
                    if (poRow.IsJOBAPPLICATIONIDNull() == false)
                    {
                        row.JOBAPPLICATIONID = poRow.JOBAPPLICATIONID;
                    }
                    if (poRow.IsAPPLICANTNAMENull() == false)
                    {
                        row.APPLICANTNAME = poRow.APPLICANTNAME;
                    }
                    if (poRow.IsPHOTOGRAPHNull() == false)
                    {
                        row.PHOTOGRAPH = poRow.PHOTOGRAPH;
                    }
                    if (poRow.IsGENDERNull() == false)
                    {
                        row.GENDER = poRow.GENDER;
                    }
                    if (poRow.IsDATEOFBIRTHNull() == false)
                    {
                        row.DATEOFBIRTH = poRow.DATEOFBIRTH;
                    }
                    if (poRow.IsPRESENTADDRESSNull() == false)
                    {
                        row.PRESENTADDRESS = poRow.PRESENTADDRESS;
                    }
                    if (poRow.IsPERMANENTADDRESSNull() == false)
                    {
                        row.PERMANENTADDRESS = poRow.PERMANENTADDRESS;
                    }
                    if (poRow.IsTELEPHONENull() == false)
                    {
                        row.TELEPHONE = poRow.TELEPHONE;
                    }
                    if (poRow.IsMOBILENull() == false)
                    {
                        row.MOBILE = poRow.MOBILE;
                    }
                    if (poRow.IsEMAILNull() == false)
                    {
                        row.EMAIL = poRow.EMAIL;
                    }
                    if (poRow.IsNATIONALITYNull() == false)
                    {
                        row.NATIONALITY = poRow.NATIONALITY;
                    }
                    if (poRow.IsPLACEOFBIRTHNull() == false)
                    {
                        row.PLACEOFBIRTH = poRow.PLACEOFBIRTH;
                    }
                    if (poRow.IsRELIGIONNull() == false)
                    {
                        row.RELIGION = poRow.RELIGION;
                    }
                    if (poRow.IsMARITALSTATUSNull() == false)
                    {
                        row.MARITALSTATUS = poRow.MARITALSTATUS;
                    }
                    if (poRow.IsBLOODGROUPNull() == false)
                    {
                        row.BLOODGROUP = poRow.BLOODGROUP;
                    }
                    if (poRow.IsPASSPORTNONull() == false)
                    {
                        row.PASSPORTNO = poRow.PASSPORTNO;
                    }
                    if (poRow.IsTINNONull() == false)
                    {
                        row.TINNO = poRow.TINNO;
                    }
                    if (poRow.IsFATHERNAMENull() == false)
                    {
                        row.FATHERNAME = poRow.FATHERNAME;
                    }
                    if (poRow.IsMOTHERNAMENull() == false)
                    {
                        row.MOTHERNAME = poRow.MOTHERNAME;
                    }
                    if (poRow.IsNATIONALIDNull() == false)
                    {
                        row.NATIONALID = poRow.NATIONALID;
                    }
                    if (poRow.IsCITYIDNull() == false)
                    {
                        row.CITYID = poRow.CITYID;
                    }
                    if (poRow.IsCOUNTRYCODENull() == false)
                    {
                        row.COUNTRYCODE = poRow.COUNTRYCODE;
                    }
                    if (poRow.IsRESUMEFILEPATHNull() == false)
                    {
                        row.RESUMEFILEPATH = poRow.RESUMEFILEPATH;
                    }
                    if (poRow.IsVACANCYIDNull() == false)
                    {
                        row.VACANCYID = poRow.VACANCYID;
                    }
                    if (poRow.IsCityNameNull() == false)
                    {
                        row.CityName = poRow.CityName;
                    }
                    if (poRow.IsCountryNameNull() == false)
                    {
                        row.CountryName = poRow.CountryName;
                    }
                    if (poRow.IsJobTitleNull() == false)
                    {
                        row.JobTitle = poRow.JobTitle;
                    }
                    if (poRow.IsJobTrackingIDNull() == false)
                    {
                        row.JobTrackingID = poRow.JobTrackingID;
                    }
                    if (poRow.IsActivitiesIDNull() == false)
                    {
                        row.ActivitiesID = poRow.ActivitiesID;
                    }

                    JobDetailsDS.Job_Application.AddJob_ApplicationRow(row);
                    JobDetailsDS.AcceptChanges();
                }
            }

            #region Get Applicant Education Information

            OleDbCommand cmd2 = new OleDbCommand();
            cmd2 = DBCommandProvider.GetDBCommand("GetJobApplicantEduInfo");
            cmd2.Parameters["JOBAPPLICATIONID"].Value = Convert.ToInt32(integerDS.DataIntegers[0].IntegerValue);

            OleDbDataAdapter adapter2 = new OleDbDataAdapter();
            adapter2.SelectCommand = cmd2;

            if (cmd2 == null)
            {
                return UtilDL.GetCommandNotFound();
            }




            nRowAffected = ADOController.Instance.Fill(adapter2, JobDetailsDS, JobDetailsDS.Job_Applicant_EduInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APP_BY_APPID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            JobDetailsDS.AcceptChanges();

            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(JobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        #endregion

        #region Job History
        private DataSet _createJobHistory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();

            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_History.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Locked
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_EMPLOYEE_LOCKED";
            cmd.CommandType = CommandType.StoredProcedure;
            foreach (JobDetailsDS.Job_HistoryRow row in jobDetailsDS.Job_History.Rows)
            {
                cmd.Parameters.AddWithValue("p_IsLocked", row.Is_Locked);
                cmd.Parameters.AddWithValue("p_ApplicationId", row.APPLICATIONID);
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Dispose();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAJOR_TOPICS_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
            }
            #endregion

            //cmd.CommandText = "pro_job_history_create";   
            cmd.CommandText = "PRO_JOB_APPLY_HISTORY_CREATE";   // WALI :: 24-Jun-2015 :: Renamed procedure to reduce confusion
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (JobDetailsDS.Job_HistoryRow row in jobDetailsDS.Job_History.Rows)
            {
                cmd.Parameters.AddWithValue("p_applicationid", row.APPLICATIONID);
                cmd.Parameters.AddWithValue("p_senderuserid", row.SENDERUSERID);

                if (row.IsRECEIVEDUSERIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_receiveduserid", row.RECEIVEDUSERID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_receiveduserid", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_activitiesid", row.ACTIVITIESID);

                if (row.IsJOBHISTORYCOMMENTSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_jobhistorycomments", row.JOBHISTORYCOMMENTS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_jobhistorycomments", DBNull.Value);
                }

                if (row.IsEVENTDATE_TIMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_eventdate_time", row.EVENTDATE_TIME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_eventdate_time", DBNull.Value);
                }
                // Update by Asif, 11-mar-12
                if (row.IsINTERVIEWERCODENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_interviewer_code", row.INTERVIEWERCODE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_interviewer_code", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;

                // Update by Asif, 11-mar-12
                DataBoolDS NotifyByEmailDS = new DataBoolDS();
                NotifyByEmailDS.Merge(inputDS.Tables[NotifyByEmailDS.DataBools.TableName], false, MissingSchemaAction.Error);
                NotifyByEmailDS.AcceptChanges();

                if (NotifyByEmailDS.DataBools[0].BoolValue == true)
                {
                    string messageBody = "", returnMessage = "", MailingSubject = "Application Confirmation";
                    string host = UtilDL.GetHost();
                    Int32 port = UtilDL.GetPort();
                    bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
                    string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
                    string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
                    bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());

                    DataStringDS EmailInfoDS = new DataStringDS();
                    EmailInfoDS.Merge(inputDS.Tables[EmailInfoDS.DataStrings.TableName], false, MissingSchemaAction.Error);
                    EmailInfoDS.AcceptChanges();

                    messageBody = EmailInfoDS.DataStrings[3].StringValue.ToString() + "<br><br>";
                    messageBody += "<b>Your application is received successfuly and your activity is:  </b>" + EmailInfoDS.DataStrings[2].StringValue.ToString();

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, EmailInfoDS.DataStrings[1].StringValue.ToString(), EmailInfoDS.DataStrings[0].StringValue.ToString(), "", MailingSubject, messageBody, "Recruitment Department");
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, EmailInfoDS.DataStrings[0].StringValue.ToString(), EmailInfoDS.DataStrings[1].StringValue.ToString(), "", MailingSubject, messageBody, "Recruitment Department");
                    }

                    if (returnMessage == "Email successfully sent.")
                    {
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    }
                    else
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICATION_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                else
                {
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                }
                // Update end

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateJobHistory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            //cmd.CommandText = "PRO_JOB_HISTORY_UPDATE";
            cmd.CommandText = "PRO_JOB_APPLY_HISTORY_UPDATE";   // WALI :: 24-Jun-2015 :: Renamed procedure to reduce confusion
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_History.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (JobDetailsDS.Job_HistoryRow row in jobDetailsDS.Job_History.Rows)
            {
                if (!row.IsJOBHISTORYCOMMENTSNull()) cmd.Parameters.AddWithValue("p_jobhistorycomments", row.JOBHISTORYCOMMENTS);
                else cmd.Parameters.AddWithValue("p_jobhistorycomments", DBNull.Value);

                cmd.Parameters.AddWithValue("p_jobhistoryid", row.JOBHISTORYID);

                if (row.IsResultNull() == false) cmd.Parameters.AddWithValue("p_result", row.Result);
                else cmd.Parameters.AddWithValue("p_result", DBNull.Value);

                if (row.IsACTIVITIESIDNull() == false) cmd.Parameters.AddWithValue("p_ActivitiesID", row.ACTIVITIESID);
                else cmd.Parameters.AddWithValue("p_ActivitiesID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_HISTORY_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getJobHistoryByLastHistory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            JobDetailsDS JobDetailsDS = new JobDetailsDS();
            DataSet returnDS = new DataSet();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataBoolDS boolDS = new DataBoolDS();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            if (boolDS.DataBools[0].BoolValue == true)   // 'true' for "ShortListing"
            {
                #region Query for Short Listing
                cmd = DBCommandProvider.GetDBCommand("GetJobHistForShortListing_New");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["DegreeID3"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);
                cmd.Parameters["DegreeID4"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);

                cmd.Parameters["ApplicantName"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["ApplicantAgeYear_Max"].Value = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["ApplicantAgeYear_Max2"].Value = Convert.ToDecimal(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["ApplicantAgeYear_Min"].Value = Convert.ToDecimal(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["ApplicantAgeYear_Min2"].Value = Convert.ToDecimal(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["Religion"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["Religion2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["Gender"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["Gender2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["MaritalStatus"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["MaritalStatus2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["CityID"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                cmd.Parameters["CityID2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);

                cmd.Parameters["DegreeID"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);
                cmd.Parameters["DegreeID2"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue);
                cmd.Parameters["DivisionResult"].Value = Convert.ToInt32(stringDS.DataStrings[8].StringValue);
                cmd.Parameters["DivisionResult2"].Value = Convert.ToInt32(stringDS.DataStrings[8].StringValue);
                cmd.Parameters["GPAResult1"].Value = Convert.ToDecimal(stringDS.DataStrings[9].StringValue);
                cmd.Parameters["GPAResult2"].Value = Convert.ToDecimal(stringDS.DataStrings[9].StringValue);

                cmd.Parameters["SumOfGPA1"].Value = Convert.ToDecimal(stringDS.DataStrings[22].StringValue);
                cmd.Parameters["SumOfGPA2"].Value = Convert.ToDecimal(stringDS.DataStrings[22].StringValue);

                //cmd.Parameters["GPAResult2"].Value = Convert.ToDecimal(stringDS.DataStrings[9].StringValue);
                cmd.Parameters["UniversityID"].Value = Convert.ToInt32(stringDS.DataStrings[10].StringValue);
                cmd.Parameters["UniversityID2"].Value = Convert.ToInt32(stringDS.DataStrings[10].StringValue);
                cmd.Parameters["UniversityName"].Value = stringDS.DataStrings[11].StringValue;
                cmd.Parameters["BoardID"].Value = Convert.ToInt32(stringDS.DataStrings[12].StringValue);
                cmd.Parameters["BoardID2"].Value = Convert.ToInt32(stringDS.DataStrings[12].StringValue);
                cmd.Parameters["MajorSubjectID"].Value = Convert.ToInt32(stringDS.DataStrings[13].StringValue);
                cmd.Parameters["MajorSubjectID2"].Value = Convert.ToInt32(stringDS.DataStrings[13].StringValue);
                cmd.Parameters["GroupID"].Value = Convert.ToInt32(stringDS.DataStrings[14].StringValue);
                cmd.Parameters["GroupID2"].Value = Convert.ToInt32(stringDS.DataStrings[14].StringValue);

                cmd.Parameters["YearOfExperience"].Value = Convert.ToDecimal(stringDS.DataStrings[15].StringValue);
                cmd.Parameters["YearOfExperience2"].Value = Convert.ToDecimal(stringDS.DataStrings[15].StringValue);
                cmd.Parameters["JobTitleID"].Value = Convert.ToInt32(stringDS.DataStrings[16].StringValue);
                cmd.Parameters["JobTitleID2"].Value = Convert.ToInt32(stringDS.DataStrings[16].StringValue);
                cmd.Parameters["Application_TrackingID"].Value = stringDS.DataStrings[17].StringValue;
                cmd.Parameters["Application_TrackingID2"].Value = stringDS.DataStrings[17].StringValue;
                cmd.Parameters["ApplicationResult1"].Value = Convert.ToDecimal(stringDS.DataStrings[18].StringValue);
                cmd.Parameters["ApplicationResult2"].Value = Convert.ToDecimal(stringDS.DataStrings[18].StringValue);
                //cmd.Parameters["JobTrackingID"].Value = Convert.ToInt32(stringDS.DataStrings[19].StringValue);
                //cmd.Parameters["JobTrackingID2"].Value = Convert.ToInt32(stringDS.DataStrings[19].StringValue);
                cmd.Parameters["JobTrackingID"].Value = stringDS.DataStrings[19].StringValue;
                cmd.Parameters["JobTrackingID2"].Value = stringDS.DataStrings[19].StringValue;
                //cmd.Parameters["IsShroted"].Value = Convert.ToBoolean(stringDS.DataStrings[20].StringValue);
                //cmd.Parameters["IsShroted2"].Value = Convert.ToBoolean(stringDS.DataStrings[20].StringValue);
                cmd.Parameters["IsShroted"].Value = Convert.ToInt32(stringDS.DataStrings[20].StringValue);
                cmd.Parameters["IsShroted2"].Value = Convert.ToInt32(stringDS.DataStrings[20].StringValue);

                cmd.Parameters["ActivitiesID"].Value = stringDS.DataStrings[21].StringValue;
                cmd.Parameters["ActivitiesID2"].Value = stringDS.DataStrings[21].StringValue;



                #endregion
            }
            else cmd = DBCommandProvider.GetDBCommand("GetJobHistoryByLastHistory");

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, JobDetailsDS, JobDetailsDS.Job_History.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_HISTORY_BY_LASTHISTORY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            JobDetailsDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(JobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getJobHistoryByAppID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            JobDetailsDS JobDetailsDS = new JobDetailsDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            int jobAppID = integerDS.DataIntegers[0].IntegerValue;

            cmd = DBCommandProvider.GetDBCommand("GetJobHistoryByAppID");
            cmd.Parameters["JobAppID"].Value = jobAppID;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, JobDetailsDS, JobDetailsDS.Job_History.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_HISTORY_BY_APPID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            JobDetailsDS.AcceptChanges();

            #region Commented :: WALI :: 25-Jun-2015
            //if (jobDetailsPO.Job_History.Count > 0)
            //{
            //    foreach (JobDetailsPO.Job_HistoryRow poRow in jobDetailsPO.Job_History.Rows)
            //    {
            //        JobDetailsDS.Job_HistoryRow row = JobDetailsDS.Job_History.NewJob_HistoryRow();

            //        row.JOBHISTORYID = poRow.JOBHISTORYID;

            //        if (poRow.IsAPPLICATIONIDNull() == false)
            //        {
            //            row.APPLICATIONID = poRow.APPLICATIONID;
            //        }
            //        if (poRow.IsSENDERUSERIDNull() == false)
            //        {
            //            row.SENDERUSERID = poRow.SENDERUSERID;
            //        }
            //        if (poRow.IsRECEIVEDUSERIDNull() == false)
            //        {
            //            row.RECEIVEDUSERID = poRow.RECEIVEDUSERID;
            //        }
            //        if (poRow.IsACTIVITIESIDNull() == false)
            //        {
            //            row.ACTIVITIESID = poRow.ACTIVITIESID;
            //        }
            //        if (poRow.IsRECEIVEDDATE_TIMENull() == false)
            //        {
            //            row.RECEIVEDDATE_TIME = poRow.RECEIVEDDATE_TIME;
            //        }
            //        if (poRow.IsJOBHISTORYCOMMENTSNull() == false)
            //        {
            //            row.JOBHISTORYCOMMENTS = poRow.JOBHISTORYCOMMENTS;
            //        }
            //        if (poRow.IsEVENTDATE_TIMENull() == false)
            //        {
            //            row.EVENTDATE_TIME = poRow.EVENTDATE_TIME;
            //        }
            //        if (poRow.IsApplyDateNull() == false)
            //        {
            //            row.ApplyDate = poRow.ApplyDate;
            //        }
            //        if (poRow.IsApplicantNameNull() == false)
            //        {
            //            row.ApplicantName = poRow.ApplicantName;
            //        }
            //        if (poRow.IsTitleNameNull() == false)
            //        {
            //            row.TitleName = poRow.TitleName;
            //        }
            //        if (poRow.IsHiringManagerNull() == false)
            //        {
            //            row.HiringManager = poRow.HiringManager;
            //        }
            //        if (poRow.IsJobTrackingIDNull() == false)
            //        {
            //            row.JobTrackingID = poRow.JobTrackingID;
            //        }
            //        if (poRow.IsSenderUserNameNull() == false)
            //        {
            //            row.SenderUserName = poRow.SenderUserName;
            //        }

            //        // Update by Asif, 11-mar-12
            //        if (poRow.IsINTERVIEWERCODENull() == false)
            //        {
            //            row.INTERVIEWERCODE = poRow.INTERVIEWERCODE;
            //        }
            //        if (poRow.IsJOBHISTORYCOMMENTSNull() == false)
            //        {
            //            row.JOBHISTORYCOMMENTS = poRow.JOBHISTORYCOMMENTS;
            //        }
            //        if (poRow.IsINTERVIEWERNull() == false)
            //        {
            //            row.INTERVIEWER = poRow.INTERVIEWER;
            //        }

            //        if (poRow.IsResultNull() == false)
            //        {
            //            row.Result = poRow.Result;
            //        }
            //        // Update end

            //        JobDetailsDS.Job_History.AddJob_HistoryRow(row);
            //        JobDetailsDS.AcceptChanges();
            //    }
            //}
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(JobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        private DataSet ApplicationShortListing(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("DeleteAppShortListingAll");

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();


            //if (stringDS.DataStrings[0].StringValue != "")
            //{
            //    cmd = DBCommandProvider.GetDBCommand("AppShortListing");
            //    cmd.Parameters["ApplicationID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            //}

            //if (cmd == null)
            //{
            //    return UtilDL.GetCommandNotFound();
            //}
            //bool bError = false;
            //int nRowAffected = -1;

            for (int i = 0; i < stringDS.DataStrings.Rows.Count; i++)
            {
                cmd = DBCommandProvider.GetDBCommand("AppShortListing");
                cmd.Parameters["ApplicationID"].Value = Convert.ToInt32(stringDS.DataStrings[i].StringValue);

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
                cmd.Dispose();
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _ApplicationShortListingAd(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSetForReqment _DataSetForReqment = new DataSetForReqment();

            DataStringDS stringDS = new DataStringDS();
            _DataSetForReqment.Merge(inputDS.Tables[_DataSetForReqment.ReqParamVal.TableName], false, MissingSchemaAction.Error);
            _DataSetForReqment.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            bool bError = false;
            int nRowAffected = -1;

            for (int i = 0; i < _DataSetForReqment.ReqParamVal.Rows.Count; i++)
            {
                if (_DataSetForReqment.ReqParamVal[i].ActionId == "1")
                {
                    cmd = DBCommandProvider.GetDBCommand("AppShortListing");
                    cmd.Parameters["ApplicationID"].Value = Convert.ToInt32(_DataSetForReqment.ReqParamVal[i].AppTrackId);

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICATION_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();
                    cmd.Dispose();
                }

                if (_DataSetForReqment.ReqParamVal[i].ActionId == "7" || _DataSetForReqment.ReqParamVal[i].ActionId == "1")
                {

                    cmd.CommandText = "PRO_JOB_HISTORY_CREATE_DB";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("p_jobtrackingid", _DataSetForReqment.ReqParamVal[i].ApplicaTrackId);
                    if (_DataSetForReqment.ReqParamVal[i].IsSendUserIdNull() == false)
                    {
                        cmd.Parameters.AddWithValue("p_senderuserid", _DataSetForReqment.ReqParamVal[i].SendUserId);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("p_senderuserid", DBNull.Value);
                    }

                    cmd.Parameters.AddWithValue("p_receiveduserid", DBNull.Value);

                    if (_DataSetForReqment.ReqParamVal[i].ActionId == "7")
                    {
                        cmd.Parameters.AddWithValue("p_activitiesid", 105);
                        cmd.Parameters.AddWithValue("p_jobhistorycomments", "Rejected");
                    }

                    if (_DataSetForReqment.ReqParamVal[i].ActionId == "1")
                    {
                        cmd.Parameters.AddWithValue("p_activitiesid", 111);
                        cmd.Parameters.AddWithValue("p_jobhistorycomments", "Shorted");
                    }



                    cmd.Parameters.AddWithValue("p_eventdate_time", DateTime.Now.Date);

                    cmd.Parameters.AddWithValue("p_interviewer_code", DBNull.Value);


                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_HISTORY_STATUS_UPDATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();
                    cmd.Dispose();


                }


            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;

            
        }

        #region Job Details :: WALI :: 01-Jul-2014
        private DataSet _createJobDetails(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistoryDS employeeHistoryDS = new EmployeeHistoryDS();
            DBConnectionDS connDS = new DBConnectionDS();

            #region Job Details Create
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_DETAILS_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistoryDS.Merge(inputDS.Tables[employeeHistoryDS.JobDetails.TableName], false, MissingSchemaAction.Error);
            employeeHistoryDS.Merge(inputDS.Tables[employeeHistoryDS.JobDetailCompetency.TableName], false, MissingSchemaAction.Error);
            employeeHistoryDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            foreach (EmployeeHistoryDS.JobDetailsRow row in employeeHistoryDS.JobDetails.Rows)
            {
                cmd.Parameters.AddWithValue("p_JobDetailsID", genPK);
                cmd.Parameters.AddWithValue("p_JobTitle", row.JobTitle);
                cmd.Parameters.AddWithValue("p_Specification", row.Specification);

                if (row.IsEmployeeTypeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EmployeeTypeID", row.EmployeeTypeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EmployeeTypeID", DBNull.Value);
                }

                if (row.IsDegreeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DegreeID", row.DegreeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DegreeID", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_ExperienceYear", row.ExperienceYear);

                if (row.IsAgeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Age", row.Age);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Age", DBNull.Value);
                }


                //New Shakir 15.02.2015
                if (row.IsGradeCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_GradeCode", row.GradeCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_GradeCode", DBNull.Value);
                }


                if (row.IsJobCategoryNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Category", row.JobCategory);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Category", DBNull.Value);
                }


                if (row.IsAgeMaxNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_AgeMax", row.AgeMax);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_AgeMax", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_DETAILS_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Competency Entry
            if (employeeHistoryDS.JobDetailCompetency.Rows.Count > 0)
            {
                OleDbCommand cmd2 = new OleDbCommand();
                cmd2.CommandText = "PRO_JOB_DETAIL_COMP_CREATE";
                cmd2.CommandType = CommandType.StoredProcedure;

                if (cmd2 == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                foreach (EmployeeHistoryDS.JobDetailCompetencyRow com in employeeHistoryDS.JobDetailCompetency.Rows)
                {
                    cmd2.Parameters.Clear();
                    cmd2.Parameters.AddWithValue("p_JobDetailsID", genPK);
                    cmd2.Parameters.AddWithValue("p_CmpTypeID", com.CmpTypeID);

                    bool bError2 = false;
                    int nRowAffected2 = -1;
                    nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd2, connDS.DBConnections[0].ConnectionID, ref bError2);

                    if (bError2)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_DETAILS_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getAllJobDetailsList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistoryDS empHistDS = new EmployeeHistoryDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetAllJobDetailsList");

            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empHistDS, empHistDS.JobDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_DETAILS_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empHistDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(empHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getJobDetailGradeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistoryDS empHistDS = new EmployeeHistoryDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetJobDetailsGradeList");

            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empHistDS, empHistDS.JobDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRADEWISE_JOB_DETAILS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empHistDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(empHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _saveGradeWiseJobDetails(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistoryDS employeeHistoryDS = new EmployeeHistoryDS();
            DBConnectionDS connDS = new DBConnectionDS();

            employeeHistoryDS.Merge(inputDS.Tables[employeeHistoryDS.JobDetails.TableName], false, MissingSchemaAction.Error);
            employeeHistoryDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_DETAIL_GRADEWISE_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistoryDS.JobDetailsRow row in employeeHistoryDS.JobDetails.Rows)
            {
                cmd.Parameters.AddWithValue("p_JobDetailsID", row.JobDetailsID);
                cmd.Parameters.AddWithValue("p_GradeID", row.GradeID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SAVE_GRADEWISE_JOB_DETAILS.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getCompetencyForJobDetail(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistoryDS empHistDS = new EmployeeHistoryDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetCompetencyForJobDetail");
            cmd.Parameters["JobDetailsID"].Value = integerDS.DataIntegers[0].IntegerValue;

            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empHistDS, empHistDS.JobDetailCompetency.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_DETAILS_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empHistDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(empHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateJobDetails(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistoryDS employeeHistoryDS = new EmployeeHistoryDS();
            DBConnectionDS connDS = new DBConnectionDS();

            employeeHistoryDS.Merge(inputDS.Tables[employeeHistoryDS.JobDetails.TableName], false, MissingSchemaAction.Error);
            employeeHistoryDS.Merge(inputDS.Tables[employeeHistoryDS.JobDetailCompetency.TableName], false, MissingSchemaAction.Error);
            employeeHistoryDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Job Details Update
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_DETAILS_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            Int32 p_JobDetailsID = 0;
            foreach (EmployeeHistoryDS.JobDetailsRow row in employeeHistoryDS.JobDetails.Rows)
            {
                cmd.Parameters.AddWithValue("p_JobDetailsID", row.JobDetailsID);
                p_JobDetailsID = row.JobDetailsID;
                cmd.Parameters.AddWithValue("p_Specification", row.Specification);

                if (row.IsEmployeeTypeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EmployeeTypeID", row.EmployeeTypeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EmployeeTypeID", DBNull.Value);
                }

                if (row.IsDegreeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DegreeID", row.DegreeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DegreeID", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_ExperienceYear", row.ExperienceYear);

                if (row.IsAgeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Age", row.Age);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Age", DBNull.Value);
                }


                //New Shakir 15.02.2015
                if (row.IsGradeCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_GradeCode", row.GradeCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_GradeCode", DBNull.Value);
                }


                if (row.IsJobCategoryNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Category", row.JobCategory);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Category", DBNull.Value);
                }


                if (row.IsAgeMaxNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_AgeMax", row.AgeMax);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_AgeMax", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_DETAILS_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region First Delete Old Competency Records
            OleDbCommand cmdDel = new OleDbCommand();
            cmdDel.CommandText = "PRO_JOB_DETAIL_COMP_DEL";
            cmdDel.CommandType = CommandType.StoredProcedure;
            //cmdDel.Parameters.AddWithValue("p_JobDetailsID", employeeHistoryDS.JobDetailCompetency[0].JobDetailsID);
            cmdDel.Parameters.AddWithValue("p_JobDetailsID", p_JobDetailsID);

            if (cmdDel == null) return UtilDL.GetCommandNotFound();

            bool bErrorDel = false;
            int nRowAffectedDel = -1;
            nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);

            if (bErrorDel)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_DETAILS_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Then New Competency Data Entry
            if (employeeHistoryDS.JobDetailCompetency.Rows.Count > 0)
            {
                OleDbCommand cmd2 = new OleDbCommand();
                cmd2.CommandText = "PRO_JOB_DETAIL_COMP_CREATE";
                cmd2.CommandType = CommandType.StoredProcedure;

                if (cmd2 == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                foreach (EmployeeHistoryDS.JobDetailCompetencyRow com in employeeHistoryDS.JobDetailCompetency.Rows)
                {
                    cmd2.Parameters.Clear();
                    cmd2.Parameters.AddWithValue("p_JobDetailsID", com.JobDetailsID);
                    cmd2.Parameters.AddWithValue("p_CmpTypeID", com.CmpTypeID);

                    bool bError2 = false;
                    int nRowAffected2 = -1;
                    nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd2, connDS.DBConnections[0].ConnectionID, ref bError2);

                    if (bError2)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_DETAILS_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteJobDetail(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistoryDS empHistDS = new EmployeeHistoryDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_DETAILS_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            empHistDS.Merge(inputDS.Tables[empHistDS.JobDetails.TableName], false, MissingSchemaAction.Error);
            empHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (EmployeeHistoryDS.JobDetailsRow row in empHistDS.JobDetails.Rows)
            {
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("p_JobDetailsID", row.JobDetailsID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_DETAILS_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.JobTitle);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.JobTitle);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region WALI :: 20-Jun-2015
        private DataSet _doesUserExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesUserExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LogniId"].Value = stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _saveJobApplicantOtherInfo(DataSet inputDS, long ApplicantUserID)
        {
            #region Declarations
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            bool bError = false;
            long genPK = -1;
            int nRowAffected = -1;
            #endregion

            #region Merge DataSets
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Application.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ComputerLiteracy.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Experience.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ExtraCurricular.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_LanguageProficiency.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ProfessionalTraining.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Reference.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            #endregion

            #region Applicant's Educational Information
            cmd.CommandText = "PRO_JOB_APP_EDUCATION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (JobDetailsDS.Job_Applicant_EduInfoRow eduRow in jobDetailsDS.Job_Applicant_EduInfo.Rows)
            {
                //genPK = IDGenerator.GetNextGenericPK();
                //if (genPK == -1) return UtilDL.GetDBOperationFailed();

                //cmd.Parameters.AddWithValue("p_Job_EducationID", genPK);
                cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                if (!eduRow.IsDegreeIDNull()) cmd.Parameters.AddWithValue("p_DegreeID", eduRow.DegreeID);
                else cmd.Parameters.AddWithValue("p_DegreeID", DBNull.Value);

                if (!eduRow.IsPassingYearNull()) cmd.Parameters.AddWithValue("p_PassingYear", eduRow.PassingYear);
                else cmd.Parameters.AddWithValue("p_PassingYear", DBNull.Value);

                if (!eduRow.IsResultTypeNull()) cmd.Parameters.AddWithValue("p_ResultType", eduRow.ResultType);
                else cmd.Parameters.AddWithValue("p_ResultType", DBNull.Value);

                if (!eduRow.IsDivisionResultNull()) cmd.Parameters.AddWithValue("p_DivisionResult", eduRow.DivisionResult);
                else cmd.Parameters.AddWithValue("p_DivisionResult", DBNull.Value);

                if (!eduRow.IsGPAResultNull()) cmd.Parameters.AddWithValue("p_GPAResult", eduRow.GPAResult);
                else cmd.Parameters.AddWithValue("p_GPAResult", DBNull.Value);

                if (!eduRow.IsGPAOutofNull()) cmd.Parameters.AddWithValue("p_GPAOutOf", eduRow.GPAOutof);
                else cmd.Parameters.AddWithValue("p_GPAOutOf", DBNull.Value);

                if (!eduRow.IsInstituteNull()) cmd.Parameters.AddWithValue("p_Institute", eduRow.Institute);
                else cmd.Parameters.AddWithValue("p_Institute", DBNull.Value);

                if (!eduRow.IsMajorSubjectIDNull()) cmd.Parameters.AddWithValue("p_MajorSubjectID", eduRow.MajorSubjectID);
                else cmd.Parameters.AddWithValue("p_MajorSubjectID", DBNull.Value);

                if (!eduRow.IsGroupIDNull()) cmd.Parameters.AddWithValue("p_GroupID", eduRow.GroupID);
                else cmd.Parameters.AddWithValue("p_GroupID", DBNull.Value);

                if (!eduRow.IsBoardIDNull()) cmd.Parameters.AddWithValue("p_BoardID", eduRow.BoardID);
                else cmd.Parameters.AddWithValue("p_BoardID", DBNull.Value);

                if (!eduRow.IsUniversityIDNull()) cmd.Parameters.AddWithValue("p_UniversityID", eduRow.UniversityID);
                else cmd.Parameters.AddWithValue("p_UniversityID", DBNull.Value);

                if (!eduRow.IsUniversityNameNull()) cmd.Parameters.AddWithValue("p_University", eduRow.UniversityName);
                else cmd.Parameters.AddWithValue("p_University", DBNull.Value);

                if (!eduRow.IsRollNoNull()) cmd.Parameters.AddWithValue("p_RollNumber", eduRow.RollNo);
                else cmd.Parameters.AddWithValue("p_RollNumber", DBNull.Value);

                if (!eduRow.IsRegistrationNoNull()) cmd.Parameters.AddWithValue("p_RegistrationNumber", eduRow.RegistrationNo);
                else cmd.Parameters.AddWithValue("p_RegistrationNumber", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;

                    messageDS.ErrorMsg.AddErrorMsgRow("Educational record entry failed.");
                    messageDS.AcceptChanges();
                    //returnDS.Merge(messageDS);
                    //returnDS.Merge(errDS);
                    //return returnDS;
                }
            }
            cmd.Dispose();
            #endregion

            #region Applicant's Professional Training
            if (jobDetailsDS.Job_ProfessionalTraining.Rows.Count > 0)
            {
                cmd.CommandText = "PRO_JOB_APP_TRAINING_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (JobDetailsDS.Job_ProfessionalTrainingRow trRow in jobDetailsDS.Job_ProfessionalTraining.Rows)
                {
                    //genPK = IDGenerator.GetNextGenericPK();
                    //if (genPK == -1) return UtilDL.GetDBOperationFailed();

                    //cmd.Parameters.AddWithValue("P_Job_TrainingID", genPK);
                    cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                    if (!trRow.IsCertificateNull()) cmd.Parameters.AddWithValue("p_Certificate", trRow.Certificate);
                    else cmd.Parameters.AddWithValue("p_Certificate", DBNull.Value);

                    if (!trRow.IsInstituteNull()) cmd.Parameters.AddWithValue("p_Institute", trRow.Institute);
                    else cmd.Parameters.AddWithValue("p_Institute", DBNull.Value);

                    if (!trRow.IsLocationNull()) cmd.Parameters.AddWithValue("p_Location", trRow.Location);
                    else cmd.Parameters.AddWithValue("p_Location", DBNull.Value);

                    if (!trRow.IsTrainingDateNull()) cmd.Parameters.AddWithValue("p_TrainingDate", trRow.TrainingDate);
                    else cmd.Parameters.AddWithValue("p_TrainingDate", DBNull.Value);

                    if (!trRow.IsMajorTopicIDNull()) cmd.Parameters.AddWithValue("p_MajorTopicID", trRow.MajorTopicID);
                    else cmd.Parameters.AddWithValue("p_MajorTopicID", DBNull.Value);

                    if (!trRow.IsMajorTopicOthersNull()) cmd.Parameters.AddWithValue("p_MajorTopicOthers", trRow.MajorTopicOthers);
                    else cmd.Parameters.AddWithValue("p_MajorTopicOthers", DBNull.Value);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();

                        messageDS.ErrorMsg.AddErrorMsgRow("Proffessional Training record entry failed.");
                        messageDS.AcceptChanges();
                    }
                }
                cmd.Dispose();
            }
            #endregion

            #region Applicant's Experience
            if (jobDetailsDS.Job_Experience.Rows.Count > 0)
            {
                cmd.CommandText = "PRO_JOB_APP_EXPERIENCE_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (JobDetailsDS.Job_ExperienceRow expRow in jobDetailsDS.Job_Experience.Rows)
                {
                    //genPK = IDGenerator.GetNextGenericPK();
                    //if (genPK == -1) return UtilDL.GetDBOperationFailed();

                    //cmd.Parameters.AddWithValue("P_Job_ExperienceID", genPK);
                    cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                    if (!expRow.IsCompanyLocationNull()) cmd.Parameters.AddWithValue("p_CompanyLocation", expRow.CompanyLocation);
                    else cmd.Parameters.AddWithValue("p_CompanyLocation", DBNull.Value);

                    if (!expRow.IsPositionHeldNull()) cmd.Parameters.AddWithValue("p_PositionHeld", expRow.PositionHeld);
                    else cmd.Parameters.AddWithValue("p_PositionHeld", DBNull.Value);

                    if (!expRow.IsDepartmentNameNull()) cmd.Parameters.AddWithValue("p_DepartmentName", expRow.DepartmentName);
                    else cmd.Parameters.AddWithValue("p_DepartmentName", DBNull.Value);

                    if (!expRow.IsResponsibilityNull()) cmd.Parameters.AddWithValue("p_Responsibility", expRow.Responsibility);
                    else cmd.Parameters.AddWithValue("p_Responsibility", DBNull.Value);

                    if (!expRow.IsFromDateNull()) cmd.Parameters.AddWithValue("p_FromDate", expRow.FromDate);
                    else cmd.Parameters.AddWithValue("p_FromDate", DBNull.Value);

                    if (!expRow.IsToDateNull()) cmd.Parameters.AddWithValue("p_ToDate", expRow.ToDate);
                    else cmd.Parameters.AddWithValue("p_ToDate", DBNull.Value);

                    if (!expRow.IsStillWorkingNull()) cmd.Parameters.AddWithValue("p_StillWorking", expRow.StillWorking);
                    else cmd.Parameters.AddWithValue("p_StillWorking", DBNull.Value);

                    if (!expRow.IsCompanyNameNull()) cmd.Parameters.AddWithValue("p_CompanyName", expRow.CompanyName);
                    else cmd.Parameters.AddWithValue("p_CompanyName", DBNull.Value);

                    if (!expRow.IsOrganizationTypeIDNull()) cmd.Parameters.AddWithValue("p_OrganizationTypeID", expRow.OrganizationTypeID);
                    else cmd.Parameters.AddWithValue("p_OrganizationTypeID", DBNull.Value);

                    if (!expRow.IsFunctionNameNull()) cmd.Parameters.AddWithValue("p_FunctionName", expRow.FunctionName);
                    else cmd.Parameters.AddWithValue("p_FunctionName", DBNull.Value);

                    if (!expRow.IsLiabilityIDNull()) cmd.Parameters.AddWithValue("p_LiabilityID", expRow.LiabilityID);
                    else cmd.Parameters.AddWithValue("p_LiabilityID", DBNull.Value);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();

                        messageDS.ErrorMsg.AddErrorMsgRow("Experience record entry failed.");
                        messageDS.AcceptChanges();
                    }
                }
                cmd.Dispose();
            }
            #endregion

            #region Applicant's Language Proficiency
            if (jobDetailsDS.Job_LanguageProficiency.Rows.Count > 0)
            {
                cmd.CommandText = "PRO_JOB_APP_LANGUAGE_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (JobDetailsDS.Job_LanguageProficiencyRow lngRow in jobDetailsDS.Job_LanguageProficiency.Rows)
                {
                    genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) return UtilDL.GetDBOperationFailed();

                    cmd.Parameters.AddWithValue("P_Job_LanguageID", genPK);
                    cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                    if (!lngRow.IsLanguageNull()) cmd.Parameters.AddWithValue("p_Language", lngRow.Language);
                    else cmd.Parameters.AddWithValue("p_Language", DBNull.Value);

                    if (!lngRow.IsReadingNull()) cmd.Parameters.AddWithValue("p_Reading", lngRow.Reading);
                    else cmd.Parameters.AddWithValue("p_Reading", DBNull.Value);

                    if (!lngRow.IsWritingNull()) cmd.Parameters.AddWithValue("p_Writing", lngRow.Writing);
                    else cmd.Parameters.AddWithValue("p_Writing", DBNull.Value);

                    if (!lngRow.IsSpeakingNull()) cmd.Parameters.AddWithValue("p_Speaking", lngRow.Speaking);
                    else cmd.Parameters.AddWithValue("p_Speaking", DBNull.Value);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();

                        messageDS.ErrorMsg.AddErrorMsgRow("Language record entry failed.");
                        messageDS.AcceptChanges();
                    }
                }
                cmd.Dispose();
            }
            #endregion

            #region Applicant's Computer Literacy
            if (jobDetailsDS.Job_ComputerLiteracy.Rows.Count > 0)
            {
                cmd.CommandText = "PRO_JOB_APP_COM_LITERACY_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (JobDetailsDS.Job_ComputerLiteracyRow comRow in jobDetailsDS.Job_ComputerLiteracy.Rows)
                {
                    genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) return UtilDL.GetDBOperationFailed();

                    cmd.Parameters.AddWithValue("P_Job_ComLiteracyID", genPK);
                    cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                    if (!comRow.IsComputerSkillNull()) cmd.Parameters.AddWithValue("p_ComputerSkill", comRow.ComputerSkill);
                    else cmd.Parameters.AddWithValue("p_ComputerSkill", DBNull.Value);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();

                        messageDS.ErrorMsg.AddErrorMsgRow("Computer literacy record entry failed.");
                        messageDS.AcceptChanges();
                    }
                }
                cmd.Dispose();
            }
            #endregion

            #region Applicant's Extra Curricular Activity
            if (jobDetailsDS.Job_ExtraCurricular.Rows.Count > 0)
            {
                cmd.CommandText = "PRO_JOB_APP_EXTRA_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (JobDetailsDS.Job_ExtraCurricularRow extrRow in jobDetailsDS.Job_ExtraCurricular.Rows)
                {
                    genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) return UtilDL.GetDBOperationFailed();

                    cmd.Parameters.AddWithValue("P_Job_ExtraCurricularID", genPK);
                    cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                    if (!extrRow.IsActivityNull()) cmd.Parameters.AddWithValue("p_Activity", extrRow.Activity);
                    else cmd.Parameters.AddWithValue("p_Activity", DBNull.Value);

                    if (!extrRow.IsFromDateNull()) cmd.Parameters.AddWithValue("p_FromDate", extrRow.FromDate);
                    else cmd.Parameters.AddWithValue("p_FromDate", DBNull.Value);

                    if (!extrRow.IsToDateNull()) cmd.Parameters.AddWithValue("p_ToDate", extrRow.ToDate);
                    else cmd.Parameters.AddWithValue("p_ToDate", DBNull.Value);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();

                        messageDS.ErrorMsg.AddErrorMsgRow("Extra curricular record entry failed.");
                        messageDS.AcceptChanges();
                    }
                }
                cmd.Dispose();
            }
            #endregion

            #region Applicant's Reference
            if (jobDetailsDS.Job_Reference.Rows.Count > 0)
            {
                cmd.CommandText = "PRO_JOB_APP_REFERENCE_ADD";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (JobDetailsDS.Job_ReferenceRow refRow in jobDetailsDS.Job_Reference.Rows)
                {
                    //genPK = IDGenerator.GetNextGenericPK();
                    //if (genPK == -1) return UtilDL.GetDBOperationFailed();

                    //cmd.Parameters.AddWithValue("p_Job_ReferenceID", genPK);
                    cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                    if (!refRow.IsReferenceNameNull()) cmd.Parameters.AddWithValue("p_ReferenceName", refRow.ReferenceName);
                    else cmd.Parameters.AddWithValue("p_ReferenceName", DBNull.Value);

                    if (!refRow.IsOrganizationNull()) cmd.Parameters.AddWithValue("p_Organization", refRow.Organization);
                    else cmd.Parameters.AddWithValue("p_Organization", DBNull.Value);

                    if (!refRow.IsDesignaitonNameNull()) cmd.Parameters.AddWithValue("p_DesignaitonName", refRow.DesignaitonName);
                    else cmd.Parameters.AddWithValue("p_DesignaitonName", DBNull.Value);

                    if (!refRow.IsPhoneNull()) cmd.Parameters.AddWithValue("p_Phone", refRow.Phone);
                    else cmd.Parameters.AddWithValue("p_Phone", DBNull.Value);

                    if (!refRow.IsEmailNull()) cmd.Parameters.AddWithValue("p_Email", refRow.Email);
                    else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                    if (!refRow.IsRelationNull()) cmd.Parameters.AddWithValue("p_Relation", refRow.Relation);
                    else cmd.Parameters.AddWithValue("p_Relation", DBNull.Value);

                    if (!refRow.IsAddressNull()) cmd.Parameters.AddWithValue("p_Address", refRow.Address);
                    else cmd.Parameters.AddWithValue("p_Address", DBNull.Value);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();

                        messageDS.ErrorMsg.AddErrorMsgRow("Reference record entry failed.");
                        messageDS.AcceptChanges();
                    }
                }
                cmd.Dispose();
            }
            #endregion

            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;         // Return all the ouptput info to the callling method.
        }
        private DataSet _deleteJobApplicantOtherInfo(DataSet inputDS, long ApplicantUserID)
        {
            #region Declarations
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            bool bError = false;
            long genPK = -1;
            int nRowAffected = -1;
            #endregion

            #region Merge DataSets
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Application.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ComputerLiteracy.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Experience.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ExtraCurricular.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_LanguageProficiency.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ProfessionalTraining.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Reference.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            #endregion

            #region Delete Applicant's Educational Information
            cmd.CommandText = "PRO_JOB_APP_EDUCATION_DEL";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                //return errDS;

                messageDS.ErrorMsg.AddErrorMsgRow("Educational record delete failed.");
                messageDS.AcceptChanges();
                //returnDS.Merge(messageDS);
                //returnDS.Merge(errDS);
                //return returnDS;
            }

            #endregion

            #region Delete Applicant's Proffessional Training
            cmd.CommandText = "PRO_JOB_APP_TRAINING_DEL";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                messageDS.ErrorMsg.AddErrorMsgRow("Proffessional Training record delete failed.");
                messageDS.AcceptChanges();
            }
            #endregion

            #region Delete Applicant's Experience
            cmd.CommandText = "PRO_JOB_APP_EXPERIENCE_DEL";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                messageDS.ErrorMsg.AddErrorMsgRow("Experience record delete failed.");
                messageDS.AcceptChanges();
            }
            #endregion

            #region Delete Applicant's Language Proficiency
            cmd.CommandText = "PRO_JOB_APP_LANGUAGE_DEL";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                messageDS.ErrorMsg.AddErrorMsgRow("Language record delete failed.");
                messageDS.AcceptChanges();
            }
            #endregion

            #region Delete Applicant's Computer Literacy
            cmd.CommandText = "PRO_JOB_APP_COM_LITERACY_DEL";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                messageDS.ErrorMsg.AddErrorMsgRow("Computer literacy record delete failed.");
                messageDS.AcceptChanges();
            }
            #endregion

            #region Delete Applicant's Extra Curricular Activity
            cmd.CommandText = "PRO_JOB_APP_EXTRA_DEL";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                messageDS.ErrorMsg.AddErrorMsgRow("Extra curricular record delete failed.");
                messageDS.AcceptChanges();
            }
            #endregion

            #region Delete Applicant's Reference
            cmd.CommandText = "PRO_JOB_APP_REFERENCE_DEL";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                messageDS.ErrorMsg.AddErrorMsgRow("Reference record delete failed.");
                messageDS.AcceptChanges();
            }
            #endregion

            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;         // Return all the ouptput info to the callling method.
        }
        private DataSet _createJobApplicant(DataSet inputDS)
        {
            #region Declarations
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Exist = new OleDbCommand();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            bool bError = false, doesExist = false;
            int nRowAffected = -1, nRowAffectedExist = -1;
            string applicantInfo = "";
            #endregion

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            #region Merge DataSets
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Application.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ComputerLiteracy.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Experience.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ExtraCurricular.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_LanguageProficiency.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ProfessionalTraining.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Reference.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            #endregion

            long genPK = -1;
            long ApplicantUserID = -1;
            string ApplicantLoginID = "";
            #region Create Applicant User
            cmd.CommandText = "PRO_JOB_APPLICANT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (JobDetailsDS.Job_ApplicationRow row in jobDetailsDS.Job_Application.Rows)
            {
                #region Check if the user exists
                doesExist = false;

                cmd_Exist = DBCommandProvider.GetDBCommand("DoesApplicantUserExist");
                if (cmd_Exist == null) return UtilDL.GetCommandNotFound();

                cmd_Exist.Parameters["ApplicantName"].Value = row.APPLICANTNAME;
                cmd_Exist.Parameters["Gender"].Value = row.GENDER;
                cmd_Exist.Parameters["DateOfBirth"].Value = row.DATEOFBIRTH;
                cmd_Exist.Parameters["FatherName"].Value = row.FATHERNAME;
                cmd_Exist.Parameters["MotherName"].Value = row.MOTHERNAME;
                if (!row.IsNATIONALIDNull()) cmd_Exist.Parameters["NationalID"].Value = row.NATIONALID;
                else cmd_Exist.Parameters["NationalID"].Value = DBNull.Value;

                bError = false;
                nRowAffectedExist = -1;
                nRowAffectedExist = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd_Exist, connDS.DBConnections[0].ConnectionID, ref bError));
                if (bError)
                {
                    errDS.Clear();
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                if (nRowAffectedExist == 0)
                {
                    doesExist = false;
                    ApplicantLoginID = row.ApplicantLogInID;
                    //ApplicantUserID = IDGenerator.GetNextGenericPK();
                    //if (ApplicantUserID == -1) return UtilDL.GetDBOperationFailed();

                    //cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                    if (!row.IsAPPLICANTNAMENull()) cmd.Parameters.AddWithValue("p_ApplicantName", row.APPLICANTNAME);
                    else cmd.Parameters.AddWithValue("p_ApplicantName", DBNull.Value);

                    if (!row.IsGENDERNull()) cmd.Parameters.AddWithValue("p_Gender", row.GENDER);
                    else cmd.Parameters.AddWithValue("p_Gender", DBNull.Value);

                    if (!row.IsDATEOFBIRTHNull()) cmd.Parameters.AddWithValue("p_DateOfBirth", row.DATEOFBIRTH);
                    else cmd.Parameters.AddWithValue("p_DateOfBirth", DBNull.Value);

                    if (!row.IsPRESENTADDRESSNull()) cmd.Parameters.AddWithValue("p_PresentAddress", row.PRESENTADDRESS);
                    else cmd.Parameters.AddWithValue("p_PresentAddress", DBNull.Value);

                    if (!row.IsPERMANENTADDRESSNull()) cmd.Parameters.AddWithValue("p_PermanentAddress", row.PERMANENTADDRESS);
                    else cmd.Parameters.AddWithValue("p_PermanentAddress", DBNull.Value);

                    if (!row.IsMOBILENull()) cmd.Parameters.AddWithValue("p_Mobile", row.MOBILE);
                    else cmd.Parameters.AddWithValue("p_Mobile", DBNull.Value);

                    if (!row.IsEMAILNull()) cmd.Parameters.AddWithValue("p_Email", row.EMAIL);
                    else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                    if (!row.IsPLACEOFBIRTHNull()) cmd.Parameters.AddWithValue("p_PlaceOfBirth", row.PLACEOFBIRTH);
                    else cmd.Parameters.AddWithValue("p_PlaceOfBirth", DBNull.Value);

                    if (!row.IsRELIGIONNull()) cmd.Parameters.AddWithValue("p_Religion", row.RELIGION);
                    else cmd.Parameters.AddWithValue("p_Religion", DBNull.Value);

                    if (!row.IsMARITALSTATUSNull()) cmd.Parameters.AddWithValue("p_MaritalStatus", row.MARITALSTATUS);
                    else cmd.Parameters.AddWithValue("p_MaritalStatus", DBNull.Value);

                    if (!row.IsBLOODGROUPNull()) cmd.Parameters.AddWithValue("p_BloodGroup", row.BLOODGROUP);
                    else cmd.Parameters.AddWithValue("p_BloodGroup", DBNull.Value);

                    if (!row.IsPASSPORTNONull()) cmd.Parameters.AddWithValue("p_PassportNo", row.PASSPORTNO);
                    else cmd.Parameters.AddWithValue("p_PassportNo", DBNull.Value);

                    if (!row.IsTINNONull()) cmd.Parameters.AddWithValue("p_TinNo", row.TINNO);
                    else cmd.Parameters.AddWithValue("p_TinNo", DBNull.Value);

                    if (!row.IsFATHERNAMENull()) cmd.Parameters.AddWithValue("p_FatherName", row.FATHERNAME);
                    else cmd.Parameters.AddWithValue("p_FatherName", DBNull.Value);

                    if (!row.IsMOTHERNAMENull()) cmd.Parameters.AddWithValue("p_MotherName", row.MOTHERNAME);
                    else cmd.Parameters.AddWithValue("p_MotherName", DBNull.Value);

                    if (!row.IsNATIONALIDNull()) cmd.Parameters.AddWithValue("p_NationalID", row.NATIONALID);
                    else cmd.Parameters.AddWithValue("p_NationalID", DBNull.Value);

                    if (!row.IsCITYIDNull()) cmd.Parameters.AddWithValue("p_CityID", row.CITYID);
                    else cmd.Parameters.AddWithValue("p_CityID", DBNull.Value);

                    if (!row.IsCOUNTRYCODENull()) cmd.Parameters.AddWithValue("p_CountryCode", row.COUNTRYCODE);
                    else cmd.Parameters.AddWithValue("p_CountryCode", DBNull.Value);

                    if (!row.IsRESUMEFILEPATHNull()) cmd.Parameters.AddWithValue("p_ResumeFilePath", row.RESUMEFILEPATH);
                    else cmd.Parameters.AddWithValue("p_ResumeFilePath", DBNull.Value);

                    if (!row.IsYearOfExperienceNull()) cmd.Parameters.AddWithValue("p_YearOfExperience", row.YearOfExperience);
                    else cmd.Parameters.AddWithValue("p_YearOfExperience", DBNull.Value);

                    if (!row.IsTELEPHONENull()) cmd.Parameters.AddWithValue("p_Telephone", row.TELEPHONE);
                    else cmd.Parameters.AddWithValue("p_Telephone", DBNull.Value);

                    if (!row.IsPasswordNull()) cmd.Parameters.AddWithValue("p_Password", Base64Encode(row.Password));
                    else cmd.Parameters.AddWithValue("p_Password", DBNull.Value);

                    if (!row.IsApplicantAgeYearNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeYear", row.ApplicantAgeYear);
                    else cmd.Parameters.AddWithValue("p_ApplicantAgeYear", DBNull.Value);

                    if (!row.IsApplicantAgeMonthNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", row.ApplicantAgeMonth);
                    else cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", DBNull.Value);

                    if (!row.IsApplicantAgeDateNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeDate", row.ApplicantAgeDate);
                    else cmd.Parameters.AddWithValue("p_ApplicantAgeDate", DBNull.Value);

                    if (!row.IsFirstNameNull()) cmd.Parameters.AddWithValue("p_FirstName", row.FirstName);
                    else cmd.Parameters.AddWithValue("p_FirstName", DBNull.Value);
                    if (!row.IsMiddleNameNull()) cmd.Parameters.AddWithValue("p_MiddleName", row.MiddleName);
                    else cmd.Parameters.AddWithValue("p_MiddleName", DBNull.Value);
                    if (!row.IsLastNameNull()) cmd.Parameters.AddWithValue("p_LastName", row.LastName);
                    else cmd.Parameters.AddWithValue("p_LastName", DBNull.Value);

                    if (!row.IsDivisionIDNull()) cmd.Parameters.AddWithValue("p_DivisionID", row.DivisionID);
                    else cmd.Parameters.AddWithValue("p_DivisionID", DBNull.Value);

                    if (!row.IsThanaIDNull()) cmd.Parameters.AddWithValue("p_ThanaID", row.ThanaID);
                    else cmd.Parameters.AddWithValue("p_ThanaID", DBNull.Value);

                    if (!row.IsThanaID_PermanentNull()) cmd.Parameters.AddWithValue("p_ThanaID_Permanent", row.ThanaID_Permanent);
                    else cmd.Parameters.AddWithValue("p_ThanaID_Permanent", DBNull.Value);

                    if (!row.IsNATIONALITYNull()) cmd.Parameters.AddWithValue("p_Nationality", row.NATIONALITY);
                    else cmd.Parameters.AddWithValue("p_Nationality", DBNull.Value);

                    if (!row.IsApplicantLogInIDNull()) cmd.Parameters.AddWithValue("p_ApplicantLogInID", row.ApplicantLogInID);
                    else cmd.Parameters.AddWithValue("p_ApplicantLogInID", DBNull.Value);

                    cmd.Parameters.Add("p_ApplicantUserID", OleDbType.Numeric, 22).Direction = ParameterDirection.Output;

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    ApplicantUserID = Convert.ToInt64(cmd.Parameters["p_ApplicantUserID"].Value.ToString());

                    cmd.Dispose();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                    applicantInfo = "Applicant Name : " + row.APPLICANTNAME + "<br>" +
                                    "Applicant Login ID : " + ApplicantLoginID + "<br>" +
                                    "Password : " + row.Password;
                }
                else
                {
                    doesExist = true;
                    messageDS.ErrorMsg.AddErrorMsgRow("User already exists.");
                    messageDS.AcceptChanges();
                }
            }
            #endregion

            #region Save Other Information
            if (!doesExist)
            {
                DataSet responseDS = _saveJobApplicantOtherInfo(inputDS, ApplicantUserID);   // SAVE applicant's other info

                errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                errDS.AcceptChanges();

                try
                {
                    messageDS.Merge(responseDS.Tables[messageDS.SuccessMsg.TableName], false, MissingSchemaAction.Error);
                    messageDS.Merge(responseDS.Tables[messageDS.ErrorMsg.TableName], false, MissingSchemaAction.Error);
                    messageDS.AcceptChanges();
                }
                catch (Exception exp) { }
            }
            #endregion

            if (errDS.Errors.Count == 0 && !doesExist)
            {
                #region Send Mail [Mandatory]
                string returnMessage = "", messageBody = "";
                //string companyName = ConfigurationManager.AppSettings["CompanyName"].Trim();
                //string displayMsg = companyName + " : " + "Carrier account information";
                string displayMsg = "Carrier account information";
                string toEmailAddress = jobDetailsDS.Job_Application[0].EMAIL;

                messageBody = "Congratulations! Your carrier account has been created successfully." + "<br><br>";
                messageBody += "<b>Applicant Name : </b>" + jobDetailsDS.Job_Application[0].APPLICANTNAME + "<br>";
                messageBody += "<b>Applicant Login ID : </b>" + ApplicantLoginID + "<br>";
                messageBody += "<b>Password : </b>" + jobDetailsDS.Job_Application[0].Password + "<br>";
                messageBody += "<br><br>";

                Mail.Mail mail = new Mail.Mail();
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, credentialEmailAddress, toEmailAddress, "", displayMsg, messageBody, displayMsg);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, credentialEmailAddress, toEmailAddress, "", displayMsg, messageBody, displayMsg);
                }

                if (returnMessage != "Email successfully sent.")  // Mail sending failed.
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();

                    messageDS.ErrorMsg.AddErrorMsgRow("Mail sending failed.");
                    messageDS.AcceptChanges();

                    returnDS.Merge(errDS);
                    returnDS.Merge(messageDS);
                    returnDS.AcceptChanges();
                    return returnDS;
                }
                else
                {
                    errDS.Clear();
                    errDS.AcceptChanges();

                    messageDS.Clear();
                    messageDS.SuccessMsg.AddSuccessMsgRow(applicantInfo);  // To Show in 'Confirmation Page'
                    messageDS.AcceptChanges();

                    jobDetailsDS = new JobDetailsDS();
                    JobDetailsDS.Job_ApplicationRow newRow = jobDetailsDS.Job_Application.NewJob_ApplicationRow();
                    newRow.ApplicantUserID = (Int32)ApplicantUserID;
                    jobDetailsDS.Job_Application.AddJob_ApplicationRow(newRow);
                    jobDetailsDS.AcceptChanges();
                }
                #endregion
            }

            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateJobApplicant(DataSet inputDS)
        {
            #region Declarations
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            bool bError = false;
            int nRowAffected = -1;
            #endregion

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            #region Merge DataSets
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Application.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ComputerLiteracy.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Experience.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ExtraCurricular.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_LanguageProficiency.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ProfessionalTraining.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Reference.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            #endregion

            long genPK = -1;
            long ApplicantUserID = -1;
            string ApplicantLoginID = "";
            #region UPDATE Applicant User
            cmd.CommandText = "PRO_JOB_APPLICANT_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (JobDetailsDS.Job_ApplicationRow row in jobDetailsDS.Job_Application.Rows)
            {
                ApplicantUserID = row.ApplicantUserID;
                ApplicantLoginID = row.ApplicantLogInID;

                cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                if (!row.IsAPPLICANTNAMENull()) cmd.Parameters.AddWithValue("p_ApplicantName", row.APPLICANTNAME);
                else cmd.Parameters.AddWithValue("p_ApplicantName", DBNull.Value);

                if (!row.IsGENDERNull()) cmd.Parameters.AddWithValue("p_Gender", row.GENDER);
                else cmd.Parameters.AddWithValue("p_Gender", DBNull.Value);

                if (!row.IsDATEOFBIRTHNull()) cmd.Parameters.AddWithValue("p_DateOfBirth", row.DATEOFBIRTH);
                else cmd.Parameters.AddWithValue("p_DateOfBirth", DBNull.Value);

                if (!row.IsPRESENTADDRESSNull()) cmd.Parameters.AddWithValue("p_PresentAddress", row.PRESENTADDRESS);
                else cmd.Parameters.AddWithValue("p_PresentAddress", DBNull.Value);

                if (!row.IsPERMANENTADDRESSNull()) cmd.Parameters.AddWithValue("p_PermanentAddress", row.PERMANENTADDRESS);
                else cmd.Parameters.AddWithValue("p_PermanentAddress", DBNull.Value);

                if (!row.IsMOBILENull()) cmd.Parameters.AddWithValue("p_Mobile", row.MOBILE);
                else cmd.Parameters.AddWithValue("p_Mobile", DBNull.Value);

                if (!row.IsEMAILNull()) cmd.Parameters.AddWithValue("p_Email", row.EMAIL);
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                if (!row.IsPLACEOFBIRTHNull()) cmd.Parameters.AddWithValue("p_PlaceOfBirth", row.PLACEOFBIRTH);
                else cmd.Parameters.AddWithValue("p_PlaceOfBirth", DBNull.Value);

                if (!row.IsRELIGIONNull()) cmd.Parameters.AddWithValue("p_Religion", row.RELIGION);
                else cmd.Parameters.AddWithValue("p_Religion", DBNull.Value);

                if (!row.IsMARITALSTATUSNull()) cmd.Parameters.AddWithValue("p_MaritalStatus", row.MARITALSTATUS);
                else cmd.Parameters.AddWithValue("p_MaritalStatus", DBNull.Value);

                if (!row.IsBLOODGROUPNull()) cmd.Parameters.AddWithValue("p_BloodGroup", row.BLOODGROUP);
                else cmd.Parameters.AddWithValue("p_BloodGroup", DBNull.Value);

                if (!row.IsPASSPORTNONull()) cmd.Parameters.AddWithValue("p_PassportNo", row.PASSPORTNO);
                else cmd.Parameters.AddWithValue("p_PassportNo", DBNull.Value);

                if (!row.IsTINNONull()) cmd.Parameters.AddWithValue("p_TinNo", row.TINNO);
                else cmd.Parameters.AddWithValue("p_TinNo", DBNull.Value);

                if (!row.IsFATHERNAMENull()) cmd.Parameters.AddWithValue("p_FatherName", row.FATHERNAME);
                else cmd.Parameters.AddWithValue("p_FatherName", DBNull.Value);

                if (!row.IsMOTHERNAMENull()) cmd.Parameters.AddWithValue("p_MotherName", row.MOTHERNAME);
                else cmd.Parameters.AddWithValue("p_MotherName", DBNull.Value);

                if (!row.IsNATIONALIDNull()) cmd.Parameters.AddWithValue("p_NationalID", row.NATIONALID);
                else cmd.Parameters.AddWithValue("p_NationalID", DBNull.Value);

                if (!row.IsCITYIDNull()) cmd.Parameters.AddWithValue("p_CityID", row.CITYID);
                else cmd.Parameters.AddWithValue("p_CityID", DBNull.Value);

                if (!row.IsCOUNTRYCODENull()) cmd.Parameters.AddWithValue("p_CountryCode", row.COUNTRYCODE);
                else cmd.Parameters.AddWithValue("p_CountryCode", DBNull.Value);

                if (!row.IsRESUMEFILEPATHNull()) cmd.Parameters.AddWithValue("p_ResumeFilePath", row.RESUMEFILEPATH);
                else cmd.Parameters.AddWithValue("p_ResumeFilePath", DBNull.Value);

                if (!row.IsYearOfExperienceNull()) cmd.Parameters.AddWithValue("p_YearOfExperience", row.YearOfExperience);
                else cmd.Parameters.AddWithValue("p_YearOfExperience", DBNull.Value);

                if (!row.IsTELEPHONENull()) cmd.Parameters.AddWithValue("p_Telephone", row.TELEPHONE);
                else cmd.Parameters.AddWithValue("p_Telephone", DBNull.Value);

                if (!row.IsPasswordNull()) cmd.Parameters.AddWithValue("p_Password", Base64Encode(row.Password));
                else cmd.Parameters.AddWithValue("p_Password", DBNull.Value);

                if (!row.IsApplicantAgeYearNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeYear", row.ApplicantAgeYear);
                else cmd.Parameters.AddWithValue("p_ApplicantAgeYear", DBNull.Value);

                if (!row.IsApplicantAgeMonthNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", row.ApplicantAgeMonth);
                else cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", DBNull.Value);

                if (!row.IsApplicantAgeDateNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeDate", row.ApplicantAgeDate);
                else cmd.Parameters.AddWithValue("p_ApplicantAgeDate", DBNull.Value);

                if (!row.IsFirstNameNull()) cmd.Parameters.AddWithValue("p_FirstName", row.FirstName);
                else cmd.Parameters.AddWithValue("p_FirstName", DBNull.Value);
                if (!row.IsMiddleNameNull()) cmd.Parameters.AddWithValue("p_MiddleName", row.MiddleName);
                else cmd.Parameters.AddWithValue("p_MiddleName", DBNull.Value);
                if (!row.IsLastNameNull()) cmd.Parameters.AddWithValue("p_LastName", row.LastName);
                else cmd.Parameters.AddWithValue("p_LastName", DBNull.Value);

                if (!row.IsDivisionIDNull()) cmd.Parameters.AddWithValue("p_DivisionID", row.DivisionID);
                else cmd.Parameters.AddWithValue("p_DivisionID", DBNull.Value);

                if (!row.IsThanaIDNull()) cmd.Parameters.AddWithValue("p_ThanaID", row.ThanaID);
                else cmd.Parameters.AddWithValue("p_ThanaID", DBNull.Value);

                if (!row.IsThanaID_PermanentNull()) cmd.Parameters.AddWithValue("p_ThanaID_Permanent", row.ThanaID_Permanent);
                else cmd.Parameters.AddWithValue("p_ThanaID_Permanent", DBNull.Value);

                if (!row.IsNATIONALITYNull()) cmd.Parameters.AddWithValue("p_Nationality", row.NATIONALITY);
                else cmd.Parameters.AddWithValue("p_Nationality", DBNull.Value);

                if (!row.IsApplicantLogInIDNull()) cmd.Parameters.AddWithValue("p_ApplicantLogInID", row.ApplicantLogInID);
                else cmd.Parameters.AddWithValue("p_ApplicantLogInID", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Dispose();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                break;
            }
            #endregion

            string applicantInfo = "Applicant Name : " + jobDetailsDS.Job_Application[0].APPLICANTNAME + "<br>" +
                                   "Applicant Login ID : " + ApplicantLoginID + "<br>";
            if (!jobDetailsDS.Job_Application[0].IsPasswordNull()) applicantInfo += "Password : " + jobDetailsDS.Job_Application[0].Password;

            #region DELETE & Re-INSERT applicant's info
            DataSet responseDS = _deleteJobApplicantOtherInfo(inputDS, ApplicantUserID);

            errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
            errDS.AcceptChanges();

            try
            {
                messageDS.Merge(responseDS.Tables[messageDS.SuccessMsg.TableName], false, MissingSchemaAction.Error);
                messageDS.Merge(responseDS.Tables[messageDS.ErrorMsg.TableName], false, MissingSchemaAction.Error);
                messageDS.AcceptChanges();
            }
            catch (Exception exp)
            { }

            if (errDS.Errors.Count == 0)
            {
                #region Re-INSERT applicant's new info
                responseDS.Clear();
                errDS.Clear();
                responseDS = _saveJobApplicantOtherInfo(inputDS, ApplicantUserID);

                errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                errDS.AcceptChanges();

                try
                {
                    messageDS.Merge(responseDS.Tables[messageDS.SuccessMsg.TableName], false, MissingSchemaAction.Error);
                    messageDS.Merge(responseDS.Tables[messageDS.ErrorMsg.TableName], false, MissingSchemaAction.Error);
                    messageDS.AcceptChanges();
                }
                catch (Exception exp)
                { }

                if (errDS.Errors.Count == 0)
                {
                    #region Send Mail [Mandatory]
                    string returnMessage = "", messageBody = "";
                    //string companyName = ConfigurationManager.AppSettings["CompanyName"].Trim();
                    //string displayMsg = companyName + " : " + "Carrier account information";
                    string displayMsg = "Carrier account information";
                    string toEmailAddress = jobDetailsDS.Job_Application[0].EMAIL;

                    messageBody = "Congratulations! Your carrier account has been updated successfully." + "<br><br>";
                    messageBody += "<b>Applicant Name : </b>" + jobDetailsDS.Job_Application[0].APPLICANTNAME + "<br>";
                    messageBody += "<b>Applicant Login ID : </b>" + ApplicantLoginID + "<br>";
                    if (!jobDetailsDS.Job_Application[0].IsPasswordNull()) messageBody += "<b>Password : </b>" + jobDetailsDS.Job_Application[0].Password + "<br>";
                    messageBody += "<br><br>";

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        //returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, reqDS.RequisitionRequest[0].FromEmailAddress, toEmailAddress, reqDS.RequisitionRequest[0].CcEmailAddress, reqDS.RequisitionRequest[0].MailingSubject, messageBody, "MIR Automation Process");
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, credentialEmailAddress, toEmailAddress, "", displayMsg, messageBody, displayMsg);
                    }
                    else
                    {
                        //returnMessage = mail.SendEmail(host, port, reqDS.RequisitionRequest[0].FromEmailAddress, toEmailAddress, reqDS.RequisitionRequest[0].CcEmailAddress, reqDS.RequisitionRequest[0].MailingSubject, messageBody, "MIR Automation Process");
                        returnMessage = mail.SendEmail(host, port, credentialEmailAddress, toEmailAddress, "", displayMsg, messageBody, displayMsg);
                    }

                    if (returnMessage != "Email successfully sent.")  // Mail sending failed.
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();

                        messageDS.ErrorMsg.AddErrorMsgRow("Mail sending failed.");
                        messageDS.AcceptChanges();

                        returnDS.Merge(errDS);
                        returnDS.Merge(messageDS);
                        returnDS.AcceptChanges();
                        return returnDS;
                    }
                    else
                    {
                        errDS.Clear();
                        errDS.AcceptChanges();

                        messageDS.Clear();
                        messageDS.SuccessMsg.AddSuccessMsgRow(applicantInfo);  // To Show in 'Confirmation Page'
                        messageDS.AcceptChanges();

                        jobDetailsDS = new JobDetailsDS();
                        JobDetailsDS.Job_ApplicationRow newRow = jobDetailsDS.Job_Application.NewJob_ApplicationRow();
                        newRow.ApplicantUserID = (Int32)ApplicantUserID;
                        jobDetailsDS.Job_Application.AddJob_ApplicationRow(newRow);
                        jobDetailsDS.AcceptChanges();
                    }
                    #endregion
                }
                #endregion
            }
            #endregion

            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getJobApplicantInfo_By_ApplicantUserID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            DataStringDS stringDS = new DataStringDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            JobDetailsDS infoDS = new JobDetailsDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            bool bError;
            int nRowAffected;
            int applicantUserID = 0;
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Get Applicant UserID By Applicant LoginID Kamal :: 05-July-2018

            string applicationUserEmail = stringDS.DataStrings[0].StringValue.ToLower();
            cmd = DBCommandProvider.GetDBCommand("GetJobApplicantUserID_By_APPLICANTLOGINID");
            cmd.Parameters["ApplicantUserEmail"].Value = applicationUserEmail;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            adapter.SelectCommand = cmd;
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.Job_Application.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            jobDetailsDS.AcceptChanges();
            if (jobDetailsDS.Job_Application.Rows.Count > 0)
            {
                DataRow row = jobDetailsDS.Job_Application.Rows[0];
                if (row["ApplicantUserID"].ToString() != "")
                {
                    applicantUserID = Convert.ToInt32(row["ApplicantUserID"].ToString());
                }
            }
            #endregion

            if (stringDS.DataStrings[3].StringValue == true.ToString())// For Single applicant user.
            {

                string _appStatus = stringDS.DataStrings[5].StringValue;

                if (_appStatus == "No Apply")
                {

                    string password = Base64Encode(stringDS.DataStrings[1].StringValue);
                    string queryMadeBy = stringDS.DataStrings[2].StringValue;
                    string applicationTrackingID = stringDS.DataStrings[4].StringValue;

                    #region Get Applicant BasicData
                    if (queryMadeBy == "applicant")
                    {
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantBasicInfo_By_AppUserID");
                        cmd.Parameters["ApplicationTrackingID"].Value = applicationTrackingID;
                        cmd.Parameters["Password"].Value = password;
                    }
                    else if (queryMadeBy == "reqruitment_Admin")
                    {
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantBasicInfo_Not_apply_By_AppUserID_Admin");
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                    }
                    if (cmd == null) return UtilDL.GetCommandNotFound();
                    adapter = new OleDbDataAdapter();
                    adapter.SelectCommand = cmd;
                    bError = false;
                    nRowAffected = -1;
                    jobDetailsDS = new JobDetailsDS();
                    nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.Job_Application.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Dispose();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    jobDetailsDS.AcceptChanges();
                    #endregion

                    /* This method is called from two pages.
             * When the applicant calls this method, he/she provides a password.
             * When 'reqruitment_Admin' calls this method, he/she provides only ApplicantUserID, no password.
             * For this reason, password is not used in the 2nd and 3rd cases.
             * Otherwise, 2 commands would be necessary for all 'Other Info' as of 'BasicData'.
             * So, whoever calls this method, if he/she gets 'BasicData', will be eligible to get all 'Other Info' also.
             */

                    if (jobDetailsDS.Job_Application.Rows.Count > 0)
                    {
                        #region Get Applicant Education
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantEducation_Not_Apply_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        ////cmd.Parameters["Password"].Value = password;
                        //cmd.Parameters["ApplicantUserID"].Value = applicantUserID;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Applicant_EduInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Proffessional Trainings
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantTraining_not_apply_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        ////cmd.Parameters["Password"].Value = password;
                        //cmd.Parameters["ApplicationTrackingID"].Value = applicationTrackingID;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ProfessionalTraining.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ProfessionalTraining.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Experience
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantExperience_Not_apply__By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                   

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Experience.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Experience.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Reference
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantReference_Not_Apply_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                     

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Reference.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Reference.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Language Proficiency
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantLanguage_Not_Apply_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                       

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_LanguageProficiency.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_LanguageProficiency.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Computer Literacy
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantComLiteracy_Not_Apply_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                      

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ComputerLiteracy.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ComputerLiteracy.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Extra Curricular Activity
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantExtra_Not_Apply_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ExtraCurricular.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ExtraCurricular.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Attachment file

                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicant_Attachment_Not_Apply_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                       

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Attachment_Rec.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Attachment_Rec.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();

                        #endregion
                    }
                }
                else 
                {
                    //Int32 applicationUserID = integerDS.DataIntegers[0].IntegerValue;
                    //Int32 applicationUserID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

                    string password = Base64Encode(stringDS.DataStrings[1].StringValue);
                    string queryMadeBy = stringDS.DataStrings[2].StringValue;
                    string applicationTrackingID = stringDS.DataStrings[4].StringValue;

                    #region Get Applicant BasicData
                    if (queryMadeBy == "applicant")
                    {
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantBasicInfo_By_AppUserID");
                        //cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        cmd.Parameters["ApplicationTrackingID"].Value = applicationTrackingID;
                        cmd.Parameters["Password"].Value = password;
                    }
                    else if (queryMadeBy == "reqruitment_Admin")
                    {
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantBasicInfo_By_AppUserID_Admin");
                        //cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        cmd.Parameters["ApplicationTrackingID"].Value = applicationTrackingID;
                    }
                    if (cmd == null) return UtilDL.GetCommandNotFound();
                    adapter = new OleDbDataAdapter();
                    adapter.SelectCommand = cmd;
                    bError = false;
                    nRowAffected = -1;
                    jobDetailsDS = new JobDetailsDS();
                    nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.Job_Application.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Dispose();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    jobDetailsDS.AcceptChanges();
                    #endregion

                    /* This method is called from two pages.
             * When the applicant calls this method, he/she provides a password.
             * When 'reqruitment_Admin' calls this method, he/she provides only ApplicantUserID, no password.
             * For this reason, password is not used in the 2nd and 3rd cases.
             * Otherwise, 2 commands would be necessary for all 'Other Info' as of 'BasicData'.
             * So, whoever calls this method, if he/she gets 'BasicData', will be eligible to get all 'Other Info' also.
             */

                    if (jobDetailsDS.Job_Application.Rows.Count > 0)
                    {
                        #region Get Applicant Education
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantEducation_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        //cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        ////cmd.Parameters["Password"].Value = password;
                        cmd.Parameters["ApplicationTrackingID"].Value = applicationTrackingID;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Applicant_EduInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Proffessional Trainings
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantTraining_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        //cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        ////cmd.Parameters["Password"].Value = password;
                        cmd.Parameters["ApplicationTrackingID"].Value = applicationTrackingID;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ProfessionalTraining.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ProfessionalTraining.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Experience
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantExperience_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        //cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        ////cmd.Parameters["Password"].Value = password;
                        cmd.Parameters["ApplicationTrackingID"].Value = applicationTrackingID;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Experience.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Experience.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Reference
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantReference_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        //cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        ////cmd.Parameters["Password"].Value = password;
                        cmd.Parameters["ApplicationTrackingID"].Value = applicationTrackingID;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Reference.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Reference.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Language Proficiency
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantLanguage_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        //cmd.Parameters["Password"].Value = password;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_LanguageProficiency.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_LanguageProficiency.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Computer Literacy
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantComLiteracy_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        //cmd.Parameters["Password"].Value = password;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ComputerLiteracy.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ComputerLiteracy.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Extra Curricular Activity
                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicantExtra_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        //cmd.Parameters["Password"].Value = password;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ExtraCurricular.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ExtraCurricular.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();
                        #endregion

                        #region Get Applicant Attachment file

                        cmd = DBCommandProvider.GetDBCommand("GetJobApplicant_Attachment_By_AppUserID");
                        if (cmd == null) return UtilDL.GetCommandNotFound();
                        //cmd.Parameters["ApplicantUserID"].Value = applicantUserID;
                        ////cmd.Parameters["Password"].Value = password;
                        cmd.Parameters["ApplicationTrackingID"].Value = applicationTrackingID;

                        adapter.SelectCommand = cmd;
                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Attachment_Rec.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        infoDS.AcceptChanges();

                        jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Attachment_Rec.TableName], false, MissingSchemaAction.Error);
                        jobDetailsDS.AcceptChanges();

                        infoDS.Clear();
                        infoDS.AcceptChanges();

                        #endregion
                    }
                }


                
            }
            else // For multiple applicant user.
            {


                 string _appStatus = stringDS.DataStrings[5].StringValue;

                 if (_appStatus == "No Apply")
                 {
                     string applicationUserID = stringDS.DataStrings[0].StringValue;
                     string applicationTrackingID = stringDS.DataStrings[4].StringValue;

                     #region Get Applicant BasicData
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantBasicInfoNoApplyByAppUserID");
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;


                     if (cmd == null) return UtilDL.GetCommandNotFound();

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.Job_Application.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     jobDetailsDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Education
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantEduInfoNotApplyByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;
                   

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Applicant_EduInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Proffessional Trainings
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantTrainingNotApplyByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ProfessionalTraining.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ProfessionalTraining.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Experience
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantExperNotApplyByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Experience.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Experience.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Reference
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantRefNotApplyByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;
                 

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Reference.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Reference.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Language Proficiency
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantLangNotApplyByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;
        

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_LanguageProficiency.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_LanguageProficiency.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Computer Literacy
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantComLitNotApplyByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ComputerLiteracy.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ComputerLiteracy.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Extra Curricular Activity
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantExtraNotApplyByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ExtraCurricular.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ExtraCurricular.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Attachment File
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicant_Attachment_Not_Apply_ByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;


                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Attachment_Rec.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Attachment_Rec.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion
                 }
                 else 
                 {
                     string applicationUserID = stringDS.DataStrings[0].StringValue;
                     string applicationTrackingID = stringDS.DataStrings[4].StringValue;

                     #region Get Applicant BasicData
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantBasicInfoByAppUserID");
                     //cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     //cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;
                     cmd.Parameters["ApplicationTrackingID1"].Value = applicationTrackingID;
                     cmd.Parameters["ApplicationTrackingID2"].Value = applicationTrackingID;

                     if (cmd == null) return UtilDL.GetCommandNotFound();

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.Job_Application.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     jobDetailsDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Education
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantEduInfoByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     //cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     //cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;
                     cmd.Parameters["ApplicationTrackingID1"].Value = applicationTrackingID;
                     cmd.Parameters["ApplicationTrackingID2"].Value = applicationTrackingID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Applicant_EduInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Proffessional Trainings
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantTrainingByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     //cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     //cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;
                     cmd.Parameters["ApplicationTrackingID1"].Value = applicationTrackingID;
                     cmd.Parameters["ApplicationTrackingID2"].Value = applicationTrackingID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ProfessionalTraining.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ProfessionalTraining.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Experience
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantExperByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     //cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     //cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;
                     cmd.Parameters["ApplicationTrackingID1"].Value = applicationTrackingID;
                     cmd.Parameters["ApplicationTrackingID2"].Value = applicationTrackingID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Experience.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Experience.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Reference
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantRefByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     //cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     //cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;
                     cmd.Parameters["ApplicationTrackingID1"].Value = applicationTrackingID;
                     cmd.Parameters["ApplicationTrackingID2"].Value = applicationTrackingID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Reference.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Reference.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Language Proficiency
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantLangByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;
                     //cmd.Parameters["ApplicationTrackingID1"].Value = applicationTrackingID;
                     //cmd.Parameters["ApplicationTrackingID2"].Value = applicationTrackingID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_LanguageProficiency.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_LanguageProficiency.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Computer Literacy
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantComLitByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ComputerLiteracy.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ComputerLiteracy.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Extra Curricular Activity
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicantExtraByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_ExtraCurricular.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_ExtraCurricular.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion

                     #region Get Applicant Attachment File
                     cmd = DBCommandProvider.GetDBCommand("GetMultipleApplicant_Attachment_ByAppUserID");
                     if (cmd == null) return UtilDL.GetCommandNotFound();
                     //cmd.Parameters["ApplicantUserID1"].Value = applicationUserID;
                     //cmd.Parameters["ApplicantUserID2"].Value = applicationUserID;
                     cmd.Parameters["ApplicationTrackingID1"].Value = applicationTrackingID;
                     cmd.Parameters["ApplicationTrackingID2"].Value = applicationTrackingID;

                     adapter.SelectCommand = cmd;
                     bError = false;
                     nRowAffected = -1;
                     nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Job_Attachment_Rec.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                     cmd.Dispose();
                     if (bError)
                     {
                         ErrorDS.Error err = errDS.Errors.NewError();
                         err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                         err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                         err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                         err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_APPLICANT_USERID.ToString();
                         errDS.Errors.AddError(err);
                         errDS.AcceptChanges();
                         return errDS;
                     }
                     infoDS.AcceptChanges();

                     jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Job_Attachment_Rec.TableName], false, MissingSchemaAction.Error);
                     jobDetailsDS.AcceptChanges();

                     infoDS.Clear();
                     infoDS.AcceptChanges();
                     #endregion
                 }
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getJobApplicantInfo_By_JobApplicationID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            JobDetailsDS infoDS = new JobDetailsDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
                        
            cmd = DBCommandProvider.GetDBCommand("GetJobApplicantBasicInfo_By_JobApplicationID");
            cmd.Parameters["Application_TrackingID"].Value = stringDS.DataStrings[0].StringValue;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            adapter.SelectCommand = cmd;
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.Job_Application.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_BY_JOBAPPLICATIONID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            jobDetailsDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createJobApplicant_And_JobApply(DataSet inputDS)
        {
            #region Declarations
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Hist = new OleDbCommand();
            OleDbCommand cmd_Exist = new OleDbCommand();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            bool bError = false, doesExist = false;
            int nRowAffected = -1, nRowAffectedExist = -1;
            string applicationInfo = "";
            long JobApplicationID = -1;
            #endregion

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            #region Merge DataSets
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Application.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ComputerLiteracy.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Experience.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ExtraCurricular.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_LanguageProficiency.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ProfessionalTraining.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Reference.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            #endregion

            long genPK = -1;
            long ApplicantUserID = -1;
            //long JobApplicationID = -1;
            string ApplicationTrackingID = "";
            string ApplicantLogInID = "";
            #region Create Applicant User
            cmd.CommandText = "PRO_JOB_APPLICANT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (JobDetailsDS.Job_ApplicationRow row in jobDetailsDS.Job_Application.Rows)
            {
                #region Check if the user exists
                doesExist = false;

                cmd_Exist = DBCommandProvider.GetDBCommand("DoesApplicantUserExist");
                if (cmd_Exist == null) return UtilDL.GetCommandNotFound();

                cmd_Exist.Parameters["ApplicantName"].Value = row.APPLICANTNAME;
                cmd_Exist.Parameters["Gender"].Value = row.GENDER;
                cmd_Exist.Parameters["DateOfBirth"].Value = row.DATEOFBIRTH;
                cmd_Exist.Parameters["FatherName"].Value = row.FATHERNAME;
                cmd_Exist.Parameters["MotherName"].Value = row.MOTHERNAME;
                if (!row.IsNATIONALIDNull()) cmd_Exist.Parameters["NationalID"].Value = row.NATIONALID;
                else cmd_Exist.Parameters["NationalID"].Value = DBNull.Value;

                bError = false;
                nRowAffectedExist = -1;
                nRowAffectedExist = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd_Exist, connDS.DBConnections[0].ConnectionID, ref bError));
                if (bError)
                {
                    errDS.Clear();
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                if (nRowAffectedExist == 0)
                {
                    doesExist = false;
                    ApplicantLogInID = row.ApplicantLogInID;
                    //ApplicantUserID = IDGenerator.GetNextGenericPK();
                    //if (ApplicantUserID == -1) return UtilDL.GetDBOperationFailed();

                    //cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                    if (!row.IsAPPLICANTNAMENull()) cmd.Parameters.AddWithValue("p_ApplicantName", row.APPLICANTNAME);
                    else cmd.Parameters.AddWithValue("p_ApplicantName", DBNull.Value);

                    if (!row.IsGENDERNull()) cmd.Parameters.AddWithValue("p_Gender", row.GENDER);
                    else cmd.Parameters.AddWithValue("p_Gender", DBNull.Value);

                    if (!row.IsDATEOFBIRTHNull()) cmd.Parameters.AddWithValue("p_DateOfBirth", row.DATEOFBIRTH);
                    else cmd.Parameters.AddWithValue("p_DateOfBirth", DBNull.Value);

                    if (!row.IsPRESENTADDRESSNull()) cmd.Parameters.AddWithValue("p_PresentAddress", row.PRESENTADDRESS);
                    else cmd.Parameters.AddWithValue("p_PresentAddress", DBNull.Value);

                    if (!row.IsPERMANENTADDRESSNull()) cmd.Parameters.AddWithValue("p_PermanentAddress", row.PERMANENTADDRESS);
                    else cmd.Parameters.AddWithValue("p_PermanentAddress", DBNull.Value);

                    if (!row.IsMOBILENull()) cmd.Parameters.AddWithValue("p_Mobile", row.MOBILE);
                    else cmd.Parameters.AddWithValue("p_Mobile", DBNull.Value);

                    if (!row.IsEMAILNull()) cmd.Parameters.AddWithValue("p_Email", row.EMAIL);
                    else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                    if (!row.IsPLACEOFBIRTHNull()) cmd.Parameters.AddWithValue("p_PlaceOfBirth", row.PLACEOFBIRTH);
                    else cmd.Parameters.AddWithValue("p_PlaceOfBirth", DBNull.Value);

                    if (!row.IsRELIGIONNull()) cmd.Parameters.AddWithValue("p_Religion", row.RELIGION);
                    else cmd.Parameters.AddWithValue("p_Religion", DBNull.Value);

                    if (!row.IsMARITALSTATUSNull()) cmd.Parameters.AddWithValue("p_MaritalStatus", row.MARITALSTATUS);
                    else cmd.Parameters.AddWithValue("p_MaritalStatus", DBNull.Value);

                    if (!row.IsBLOODGROUPNull()) cmd.Parameters.AddWithValue("p_BloodGroup", row.BLOODGROUP);
                    else cmd.Parameters.AddWithValue("p_BloodGroup", DBNull.Value);

                    if (!row.IsPASSPORTNONull()) cmd.Parameters.AddWithValue("p_PassportNo", row.PASSPORTNO);
                    else cmd.Parameters.AddWithValue("p_PassportNo", DBNull.Value);

                    if (!row.IsTINNONull()) cmd.Parameters.AddWithValue("p_TinNo", row.TINNO);
                    else cmd.Parameters.AddWithValue("p_TinNo", DBNull.Value);

                    if (!row.IsFATHERNAMENull()) cmd.Parameters.AddWithValue("p_FatherName", row.FATHERNAME);
                    else cmd.Parameters.AddWithValue("p_FatherName", DBNull.Value);

                    if (!row.IsMOTHERNAMENull()) cmd.Parameters.AddWithValue("p_MotherName", row.MOTHERNAME);
                    else cmd.Parameters.AddWithValue("p_MotherName", DBNull.Value);

                    if (!row.IsNATIONALIDNull()) cmd.Parameters.AddWithValue("p_NationalID", row.NATIONALID);
                    else cmd.Parameters.AddWithValue("p_NationalID", DBNull.Value);

                    if (!row.IsCITYIDNull()) cmd.Parameters.AddWithValue("p_CityID", row.CITYID);
                    else cmd.Parameters.AddWithValue("p_CityID", DBNull.Value);

                    if (!row.IsCOUNTRYCODENull()) cmd.Parameters.AddWithValue("p_CountryCode", row.COUNTRYCODE);
                    else cmd.Parameters.AddWithValue("p_CountryCode", DBNull.Value);

                    if (!row.IsRESUMEFILEPATHNull()) cmd.Parameters.AddWithValue("p_ResumeFilePath", row.RESUMEFILEPATH);
                    else cmd.Parameters.AddWithValue("p_ResumeFilePath", DBNull.Value);

                    if (!row.IsYearOfExperienceNull()) cmd.Parameters.AddWithValue("p_YearOfExperience", row.YearOfExperience);
                    else cmd.Parameters.AddWithValue("p_YearOfExperience", DBNull.Value);

                    if (!row.IsTELEPHONENull()) cmd.Parameters.AddWithValue("p_Telephone", row.TELEPHONE);
                    else cmd.Parameters.AddWithValue("p_Telephone", DBNull.Value);

                    if (!row.IsPasswordNull()) cmd.Parameters.AddWithValue("p_Password", Base64Encode(row.Password));
                    else cmd.Parameters.AddWithValue("p_Password", DBNull.Value);

                    if (!row.IsApplicantAgeYearNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeYear", row.ApplicantAgeYear);
                    else cmd.Parameters.AddWithValue("p_ApplicantAgeYear", DBNull.Value);

                    if (!row.IsApplicantAgeMonthNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", row.ApplicantAgeMonth);
                    else cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", DBNull.Value);

                    if (!row.IsApplicantAgeDateNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeDate", row.ApplicantAgeDate);
                    else cmd.Parameters.AddWithValue("p_ApplicantAgeDate", DBNull.Value);

                    if (!row.IsFirstNameNull()) cmd.Parameters.AddWithValue("p_FirstName", row.FirstName);
                    else cmd.Parameters.AddWithValue("p_FirstName", DBNull.Value);
                    if (!row.IsMiddleNameNull()) cmd.Parameters.AddWithValue("p_MiddleName", row.MiddleName);
                    else cmd.Parameters.AddWithValue("p_MiddleName", DBNull.Value);
                    if (!row.IsLastNameNull()) cmd.Parameters.AddWithValue("p_LastName", row.LastName);
                    else cmd.Parameters.AddWithValue("p_LastName", DBNull.Value);

                    if (!row.IsDivisionIDNull()) cmd.Parameters.AddWithValue("p_DivisionID", row.DivisionID);
                    else cmd.Parameters.AddWithValue("p_DivisionID", DBNull.Value);

                    if (!row.IsThanaIDNull()) cmd.Parameters.AddWithValue("p_ThanaID", row.ThanaID);
                    else cmd.Parameters.AddWithValue("p_ThanaID", DBNull.Value);
                    if (!row.IsThanaID_PermanentNull()) cmd.Parameters.AddWithValue("p_ThanaID_Permanent", row.ThanaID_Permanent);
                    else cmd.Parameters.AddWithValue("p_ThanaID_Permanent", DBNull.Value);
                    if (!row.IsNATIONALITYNull()) cmd.Parameters.AddWithValue("p_Nationality", row.NATIONALITY);
                    else cmd.Parameters.AddWithValue("p_Nationality", DBNull.Value);

                    if (!row.IsApplicantLogInIDNull()) cmd.Parameters.AddWithValue("p_ApplicantLogInID", row.ApplicantLogInID);
                    else cmd.Parameters.AddWithValue("p_ApplicantLogInID", DBNull.Value);

                    cmd.Parameters.Add("p_ApplicantUserID", OleDbType.Numeric, 22).Direction = ParameterDirection.Output;

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    ApplicantUserID = Convert.ToInt64(cmd.Parameters["p_ApplicantUserID"].Value.ToString());
                    cmd.Dispose();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE_AND_JOB_APPLY.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                else
                {
                    doesExist = true;
                    messageDS.ErrorMsg.AddErrorMsgRow("User already exists.");
                    messageDS.AcceptChanges();
                }

                break; // Though, there will be only one row. Still to be confirm.
            }
            #endregion

            if (!doesExist)
            {
                #region Create Applicant Other Info
                DataSet responseDS = _saveJobApplicantOtherInfo(inputDS, ApplicantUserID);   // SAVE applicant's other info

                errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                errDS.AcceptChanges();

                try
                {
                    messageDS.Merge(responseDS.Tables[messageDS.SuccessMsg.TableName], false, MissingSchemaAction.Error);
                    messageDS.Merge(responseDS.Tables[messageDS.ErrorMsg.TableName], false, MissingSchemaAction.Error);
                    messageDS.AcceptChanges();
                }
                catch (Exception exp) { }
                #endregion
            }

            if (errDS.Errors.Count == 0 && !doesExist)   // No Error occured, now proceed to JOB APPLICATION
            {
                #region Create Job Application
                cmd.CommandText = "PRO_JOB_APPLICATION_CREATE_NEW";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (JobDetailsDS.Job_ApplicationRow row in jobDetailsDS.Job_Application.Rows)
                {
                    #region Job Application
                    ApplicationTrackingID = row.Application_TrackingID;
                    //JobApplicationID = IDGenerator.GetNextGenericPK();
                    //if (JobApplicationID == -1) return UtilDL.GetDBOperationFailed();

                    //cmd.Parameters.AddWithValue("p_JobApplicationID", JobApplicationID);
                    cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                    if (!row.IsVACANCYIDNull()) cmd.Parameters.AddWithValue("p_VacancyID", row.VACANCYID);
                    else cmd.Parameters.AddWithValue("p_VacancyID", DBNull.Value);

                    cmd.Parameters.AddWithValue("p_IsShroted", false);

                    if (!row.IsDepositAmountNull()) cmd.Parameters.AddWithValue("p_DepositAmount", row.DepositAmount);
                    else cmd.Parameters.AddWithValue("p_DepositAmount", DBNull.Value);
                    if (!row.IsDepositDateNull()) cmd.Parameters.AddWithValue("p_DepositDate", row.DepositDate);
                    else cmd.Parameters.AddWithValue("p_DepositDate", DBNull.Value);
                    if (!row.IsDepositVoucherNoNull()) cmd.Parameters.AddWithValue("p_DepositVoucherNo", row.DepositVoucherNo);
                    else cmd.Parameters.AddWithValue("p_DepositVoucherNo", DBNull.Value);

                    if (!row.IsApplicantAgeYearNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeYear", row.ApplicantAgeYear);
                    else cmd.Parameters.AddWithValue("p_ApplicantAgeYear", DBNull.Value);
                    if (!row.IsApplicantAgeMonthNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", row.ApplicantAgeMonth);
                    else cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", DBNull.Value);
                    if (!row.IsApplicantAgeDateNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeDATE", row.ApplicantAgeDate);
                    else cmd.Parameters.AddWithValue("p_ApplicantAgeDATE", DBNull.Value);

                    //cmd.Parameters.AddWithValue("p_JobTrackingID", row.JobTrackingID);
                    if (!row.IsApplication_TrackingIDNull()) cmd.Parameters.AddWithValue("p_Application_TrackingID", row.Application_TrackingID);
                    else cmd.Parameters.AddWithValue("p_Application_TrackingID", DBNull.Value);
                    cmd.Parameters.Add("p_JobApplicationID", OleDbType.Numeric, 22).Direction = ParameterDirection.Output;

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    JobApplicationID = Convert.ToInt64(cmd.Parameters["p_JobApplicationID"].Value.ToString());
                    cmd.Dispose();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE_AND_JOB_APPLY.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                    applicationInfo = "Applicant Name : " + row.APPLICANTNAME + "<br>" +
                                       "Applicant Login ID : " + row.ApplicantLogInID + "<br>" +
                                      "Password : " + row.Password + "<br>" +
                        //"Application Tracking ID : " + JobApplicationID.ToString() + "<br>";
                                      "Application Tracking ID : " + ApplicationTrackingID + "<br>";
                    #endregion

                    #region CREATE Job History
                    DateTime sysDateTime = UtilDL.GetSystemDateTime(connDS);
                    if (sysDateTime == (new DateTime(1754, 1, 1))) return UtilDL.GetDBOperationFailed();

                    cmd.CommandText = "PRO_JOB_APPLY_HISTORY_CREATE";
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (cmd == null) return UtilDL.GetCommandNotFound();

                    //long genPK_Hist = IDGenerator.GetNextGenericPK();
                    //if (genPK_Hist == -1) return UtilDL.GetDBOperationFailed();

                    //cmd.Parameters.AddWithValue("p_jobhistoryid", genPK_Hist);
                    cmd.Parameters.AddWithValue("p_applicationid", JobApplicationID);
                    cmd.Parameters.AddWithValue("p_senderuserid", DBNull.Value);
                    cmd.Parameters.AddWithValue("p_receiveduserid", DBNull.Value);
                    cmd.Parameters.AddWithValue("p_activitiesid", Convert.ToInt32(JobApplicationActivities.ApplicationSubmit));
                    cmd.Parameters.AddWithValue("p_jobhistorycomments", "Job Application is submitted");
                    cmd.Parameters.AddWithValue("p_eventdate_time", DBNull.Value);
                    cmd.Parameters.AddWithValue("p_interviewer_code", DBNull.Value);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Dispose();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE_AND_JOB_APPLY.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    #endregion
                }
                #endregion

                #region Send Mail [Mandatory]
                string returnMessage = "", messageBody = "";
                //string companyName = ConfigurationManager.AppSettings["CompanyName"].Trim();
                //string displayMsg = companyName + " : " + "Carrier account information";
                string displayMsg = "Carrier account information";
                string toEmailAddress = jobDetailsDS.Job_Application[0].EMAIL;

                //messageBody = "Congratulations! Your <b>Career Account</b> and <b>Job Application</b> has been created successfully." + "<br><br>";
                //messageBody += "<b>Job Title : </b>" + jobDetailsDS.Job_Application[0].JobTitle + "<br><br>";
                //messageBody += "<b>Applicant Name : </b>" + jobDetailsDS.Job_Application[0].APPLICANTNAME + "<br>";
                //messageBody += "<b>Applicant ID : </b>" + ApplicantUserID.ToString() + "<br>";
                //messageBody += "<b>Password : </b>" + jobDetailsDS.Job_Application[0].Password + "<br><br>";
                //messageBody += "<b>Applicant Tracking ID : </b>" + JobApplicationID.ToString() + "<br><br>";
                //messageBody += "<br><br>";
                messageBody = "Congratulations! Your <b>Career Account</b> and <b>Job Application</b> has been created successfully." + "<br><br>";
                messageBody += "<b>Job Title : </b>" + jobDetailsDS.Job_Application[0].JobTitle + "<br><br>";
                messageBody += "<b>Applicant Name : </b>" + jobDetailsDS.Job_Application[0].APPLICANTNAME + "<br>";
                messageBody += "<b>Applicant Login ID : </b>" + ApplicantLogInID + "<br>";
                messageBody += "<b>Password : </b>" + jobDetailsDS.Job_Application[0].Password + "<br><br>";
                messageBody += "<b>Application Tracking ID : </b>" + ApplicationTrackingID + "<br><br>";
                messageBody += "<br><br>";

                Mail.Mail mail = new Mail.Mail();
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, credentialEmailAddress, toEmailAddress, "", displayMsg, messageBody, displayMsg);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, credentialEmailAddress, toEmailAddress, "", displayMsg, messageBody, displayMsg);
                }

                if (returnMessage != "Email successfully sent.")  // Mail sending failed.
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE_AND_JOB_APPLY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();

                    messageDS.ErrorMsg.AddErrorMsgRow("Mail sending failed.");
                    messageDS.AcceptChanges();

                    returnDS.Merge(errDS);
                    returnDS.Merge(messageDS);
                    returnDS.AcceptChanges();
                    return returnDS;
                }
                else
                {
                    errDS.Clear();
                    errDS.AcceptChanges();

                    messageDS.Clear();
                    messageDS.SuccessMsg.AddSuccessMsgRow(applicationInfo);  // To Show in 'Confirmation Page'
                    messageDS.AcceptChanges();

                    jobDetailsDS = new JobDetailsDS();
                    JobDetailsDS.Job_ApplicationRow newRow = jobDetailsDS.Job_Application.NewJob_ApplicationRow();
                    newRow.ApplicantUserID = (Int32)ApplicantUserID;
                    newRow.JOBAPPLICATIONID = (Int32)JobApplicationID;
                    jobDetailsDS.Job_Application.AddJob_ApplicationRow(newRow);
                    jobDetailsDS.AcceptChanges();
                }
                #endregion
            }

            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateJobApplicant_And_JobApply(DataSet inputDS)
        {
            #region Declarations
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Exist = new OleDbCommand();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            bool bError = false, doesExist = false;
            long JobApplicationID = -1;
            int nRowAffected = -1, nRowAffectedExist = -1;
            #endregion

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            #region Merge DataSets
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Application.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Applicant_EduInfo.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ComputerLiteracy.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Experience.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ExtraCurricular.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_LanguageProficiency.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_ProfessionalTraining.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.Job_Reference.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            #endregion

            long genPK = -1;
            long ApplicantUserID = -1;
            //long JobApplicationID = -1;
            string ApplicantEmail;
            string ApplicationTrackingID = "";
            string ApplicantLogInID = "";

            #region UPDATE Applicant User
            cmd.CommandText = "PRO_JOB_APPLICANT_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (JobDetailsDS.Job_ApplicationRow row in jobDetailsDS.Job_Application.Rows)
            {
                ApplicantUserID = row.ApplicantUserID;
                ApplicantEmail = row.EMAIL;
                ApplicantLogInID = row.ApplicantLogInID;

                cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                if (!row.IsAPPLICANTNAMENull()) cmd.Parameters.AddWithValue("p_ApplicantName", row.APPLICANTNAME);
                else cmd.Parameters.AddWithValue("p_ApplicantName", DBNull.Value);

                if (!row.IsGENDERNull()) cmd.Parameters.AddWithValue("p_Gender", row.GENDER);
                else cmd.Parameters.AddWithValue("p_Gender", DBNull.Value);

                if (!row.IsDATEOFBIRTHNull()) cmd.Parameters.AddWithValue("p_DateOfBirth", row.DATEOFBIRTH);
                else cmd.Parameters.AddWithValue("p_DateOfBirth", DBNull.Value);

                if (!row.IsPRESENTADDRESSNull()) cmd.Parameters.AddWithValue("p_PresentAddress", row.PRESENTADDRESS);
                else cmd.Parameters.AddWithValue("p_PresentAddress", DBNull.Value);

                if (!row.IsPERMANENTADDRESSNull()) cmd.Parameters.AddWithValue("p_PermanentAddress", row.PERMANENTADDRESS);
                else cmd.Parameters.AddWithValue("p_PermanentAddress", DBNull.Value);

                if (!row.IsMOBILENull()) cmd.Parameters.AddWithValue("p_Mobile", row.MOBILE);
                else cmd.Parameters.AddWithValue("p_Mobile", DBNull.Value);

                if (!row.IsEMAILNull()) cmd.Parameters.AddWithValue("p_Email", row.EMAIL);
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                if (!row.IsPLACEOFBIRTHNull()) cmd.Parameters.AddWithValue("p_PlaceOfBirth", row.PLACEOFBIRTH);
                else cmd.Parameters.AddWithValue("p_PlaceOfBirth", DBNull.Value);

                if (!row.IsRELIGIONNull()) cmd.Parameters.AddWithValue("p_Religion", row.RELIGION);
                else cmd.Parameters.AddWithValue("p_Religion", DBNull.Value);

                if (!row.IsMARITALSTATUSNull()) cmd.Parameters.AddWithValue("p_MaritalStatus", row.MARITALSTATUS);
                else cmd.Parameters.AddWithValue("p_MaritalStatus", DBNull.Value);

                if (!row.IsBLOODGROUPNull()) cmd.Parameters.AddWithValue("p_BloodGroup", row.BLOODGROUP);
                else cmd.Parameters.AddWithValue("p_BloodGroup", DBNull.Value);

                if (!row.IsPASSPORTNONull()) cmd.Parameters.AddWithValue("p_PassportNo", row.PASSPORTNO);
                else cmd.Parameters.AddWithValue("p_PassportNo", DBNull.Value);

                if (!row.IsTINNONull()) cmd.Parameters.AddWithValue("p_TinNo", row.TINNO);
                else cmd.Parameters.AddWithValue("p_TinNo", DBNull.Value);

                if (!row.IsFATHERNAMENull()) cmd.Parameters.AddWithValue("p_FatherName", row.FATHERNAME);
                else cmd.Parameters.AddWithValue("p_FatherName", DBNull.Value);

                if (!row.IsMOTHERNAMENull()) cmd.Parameters.AddWithValue("p_MotherName", row.MOTHERNAME);
                else cmd.Parameters.AddWithValue("p_MotherName", DBNull.Value);

                if (!row.IsNATIONALIDNull()) cmd.Parameters.AddWithValue("p_NationalID", row.NATIONALID);
                else cmd.Parameters.AddWithValue("p_NationalID", DBNull.Value);

                if (!row.IsCITYIDNull()) cmd.Parameters.AddWithValue("p_CityID", row.CITYID);
                else cmd.Parameters.AddWithValue("p_CityID", DBNull.Value);

                if (!row.IsCOUNTRYCODENull()) cmd.Parameters.AddWithValue("p_CountryCode", row.COUNTRYCODE);
                else cmd.Parameters.AddWithValue("p_CountryCode", DBNull.Value);

                if (!row.IsRESUMEFILEPATHNull()) cmd.Parameters.AddWithValue("p_ResumeFilePath", row.RESUMEFILEPATH);
                else cmd.Parameters.AddWithValue("p_ResumeFilePath", DBNull.Value);

                if (!row.IsYearOfExperienceNull()) cmd.Parameters.AddWithValue("p_YearOfExperience", row.YearOfExperience);
                else cmd.Parameters.AddWithValue("p_YearOfExperience", DBNull.Value);

                if (!row.IsTELEPHONENull()) cmd.Parameters.AddWithValue("p_Telephone", row.TELEPHONE);
                else cmd.Parameters.AddWithValue("p_Telephone", DBNull.Value);

                if (!row.IsPasswordNull()) cmd.Parameters.AddWithValue("p_Password", Base64Encode(row.Password));
                else cmd.Parameters.AddWithValue("p_Password", DBNull.Value);

                if (!row.IsApplicantAgeYearNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeYear", row.ApplicantAgeYear);
                else cmd.Parameters.AddWithValue("p_ApplicantAgeYear", DBNull.Value);

                if (!row.IsApplicantAgeMonthNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", row.ApplicantAgeMonth);
                else cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", DBNull.Value);

                if (!row.IsApplicantAgeDateNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeDate", row.ApplicantAgeDate);
                else cmd.Parameters.AddWithValue("p_ApplicantAgeDate", DBNull.Value);

                if (!row.IsFirstNameNull()) cmd.Parameters.AddWithValue("p_FirstName", row.FirstName);
                else cmd.Parameters.AddWithValue("p_FirstName", DBNull.Value);
                if (!row.IsMiddleNameNull()) cmd.Parameters.AddWithValue("p_MiddleName", row.MiddleName);
                else cmd.Parameters.AddWithValue("p_MiddleName", DBNull.Value);
                if (!row.IsLastNameNull()) cmd.Parameters.AddWithValue("p_LastName", row.LastName);
                else cmd.Parameters.AddWithValue("p_LastName", DBNull.Value);

                if (!row.IsDivisionIDNull()) cmd.Parameters.AddWithValue("p_DivisionID", row.DivisionID);
                else cmd.Parameters.AddWithValue("p_DivisionID", DBNull.Value);

                if (!row.IsThanaIDNull()) cmd.Parameters.AddWithValue("p_ThanaID", row.ThanaID);
                else cmd.Parameters.AddWithValue("p_ThanaID", DBNull.Value);
                if (!row.IsThanaID_PermanentNull()) cmd.Parameters.AddWithValue("p_ThanaID_Permanent", row.ThanaID_Permanent);
                else cmd.Parameters.AddWithValue("p_ThanaID_Permanent", DBNull.Value);
                if (!row.IsNATIONALITYNull()) cmd.Parameters.AddWithValue("p_Nationality", row.NATIONALITY);
                else cmd.Parameters.AddWithValue("p_Nationality", DBNull.Value);

                if (!row.IsApplicantLogInIDNull()) cmd.Parameters.AddWithValue("p_ApplicantLogInID", row.ApplicantLogInID);
                else cmd.Parameters.AddWithValue("p_ApplicantLogInID", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Dispose();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE_AND_JOB_APPLY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                break;
            }
            #endregion

            #region DELETE & Re-INSERT applicant's info && Apply for Job
            DataSet responseDS = _deleteJobApplicantOtherInfo(inputDS, ApplicantUserID);

            errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
            errDS.AcceptChanges();

            try
            {
                messageDS.Merge(responseDS.Tables[messageDS.SuccessMsg.TableName], false, MissingSchemaAction.Error);
                messageDS.Merge(responseDS.Tables[messageDS.ErrorMsg.TableName], false, MissingSchemaAction.Error);
                messageDS.AcceptChanges();
            }
            catch (Exception exp)
            { }

            if (errDS.Errors.Count == 0)
            {
                #region Re-INSERT applicant's new info && Apply for Job
                responseDS.Clear();
                errDS.Clear();
                responseDS = _saveJobApplicantOtherInfo(inputDS, ApplicantUserID);

                errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                errDS.AcceptChanges();

                try
                {
                    messageDS.Merge(responseDS.Tables[messageDS.SuccessMsg.TableName], false, MissingSchemaAction.Error);
                    messageDS.Merge(responseDS.Tables[messageDS.ErrorMsg.TableName], false, MissingSchemaAction.Error);
                    messageDS.AcceptChanges();
                }
                catch (Exception exp)
                { }

                if (errDS.Errors.Count == 0)
                {
                    #region Create Job Application
                    cmd.CommandText = "PRO_JOB_APPLICATION_CREATE_NEW";
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (cmd == null) return UtilDL.GetCommandNotFound();

                    foreach (JobDetailsDS.Job_ApplicationRow row in jobDetailsDS.Job_Application.Rows)
                    {
                        #region Check if already applied
                        doesExist = false;

                        cmd_Exist = DBCommandProvider.GetDBCommand("DoesApplicationExist");
                        if (cmd_Exist == null) return UtilDL.GetCommandNotFound();

                        cmd_Exist.Parameters["ApplicantUserID"].Value = ApplicantUserID;
                        cmd_Exist.Parameters["VacancyID"].Value = row.VACANCYID;

                        bError = false;
                        nRowAffectedExist = -1;
                        nRowAffectedExist = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd_Exist, connDS.DBConnections[0].ConnectionID, ref bError));
                        if (bError)
                        {
                            errDS.Clear();
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        #endregion

                        if (nRowAffectedExist == 0)
                        {
                            doesExist = false;
                            ApplicationTrackingID = row.Application_TrackingID;
                            //JobTrackingID = row.JobTrackingID;
                            //JobApplicationID = IDGenerator.GetNextGenericPK();
                            //if (JobApplicationID == -1) return UtilDL.GetDBOperationFailed();

                            //cmd.Parameters.AddWithValue("p_JobApplicationID", JobApplicationID);
                            cmd.Parameters.AddWithValue("p_ApplicantUserID", ApplicantUserID);

                            if (!row.IsVACANCYIDNull()) cmd.Parameters.AddWithValue("p_VacancyID", row.VACANCYID);
                            else cmd.Parameters.AddWithValue("p_VacancyID", DBNull.Value);

                            cmd.Parameters.AddWithValue("p_IsShroted", false);

                            if (!row.IsDepositAmountNull()) cmd.Parameters.AddWithValue("p_DepositAmount", row.DepositAmount);
                            else cmd.Parameters.AddWithValue("p_DepositAmount", DBNull.Value);
                            if (!row.IsDepositDateNull()) cmd.Parameters.AddWithValue("p_DepositDate", row.DepositDate);
                            else cmd.Parameters.AddWithValue("p_DepositDate", DBNull.Value);
                            if (!row.IsDepositVoucherNoNull()) cmd.Parameters.AddWithValue("p_DepositVoucherNo", row.DepositVoucherNo);
                            else cmd.Parameters.AddWithValue("p_DepositVoucherNo", DBNull.Value);

                            if (!row.IsApplicantAgeYearNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeYear", row.ApplicantAgeYear);
                            else cmd.Parameters.AddWithValue("p_ApplicantAgeYear", DBNull.Value);
                            if (!row.IsApplicantAgeMonthNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", row.ApplicantAgeMonth);
                            else cmd.Parameters.AddWithValue("p_ApplicantAgeMonth", DBNull.Value);
                            if (!row.IsApplicantAgeDateNull()) cmd.Parameters.AddWithValue("p_ApplicantAgeDATE", row.ApplicantAgeDate);
                            else cmd.Parameters.AddWithValue("p_ApplicantAgeDATE", DBNull.Value);

                            //cmd.Parameters.AddWithValue("p_Application_TrackingID", row.JobTrackingID);
                            if (!row.IsApplication_TrackingIDNull()) cmd.Parameters.AddWithValue("p_Application_TrackingID", row.Application_TrackingID);
                            else cmd.Parameters.AddWithValue("p_Application_TrackingID", DBNull.Value);
                            cmd.Parameters.Add("p_JobApplicationID", OleDbType.Numeric, 22).Direction = ParameterDirection.Output;

                            bError = false;
                            nRowAffected = -1;
                            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                            JobApplicationID = Convert.ToInt64(cmd.Parameters["p_JobApplicationID"].Value.ToString());
                            cmd.Dispose();
                            if (bError)
                            {
                                ErrorDS.Error err = errDS.Errors.NewError();
                                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                                err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE_AND_JOB_APPLY.ToString();
                                errDS.Errors.AddError(err);
                                errDS.AcceptChanges();
                                return errDS;
                            }
                        }
                        else
                        {
                            doesExist = true;
                            messageDS.ErrorMsg.AddErrorMsgRow("You have already applied to this job.");
                            messageDS.AcceptChanges();
                        }
                        break;
                    }
                    #endregion

                    if (!doesExist)
                    {
                        #region CREATE Job History
                        DateTime sysDateTime = UtilDL.GetSystemDateTime(connDS);
                        if (sysDateTime == (new DateTime(1754, 1, 1))) return UtilDL.GetDBOperationFailed();

                        cmd.CommandText = "PRO_JOB_APPLY_HISTORY_CREATE";
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (cmd == null) return UtilDL.GetCommandNotFound();

                        //long genPK_Hist = IDGenerator.GetNextGenericPK();
                        //if (genPK_Hist == -1) return UtilDL.GetDBOperationFailed();

                        //cmd.Parameters.AddWithValue("p_jobhistoryid", genPK_Hist);
                        cmd.Parameters.AddWithValue("p_applicationid", JobApplicationID);
                        cmd.Parameters.AddWithValue("p_senderuserid", DBNull.Value);
                        cmd.Parameters.AddWithValue("p_receiveduserid", DBNull.Value);
                        cmd.Parameters.AddWithValue("p_activitiesid", Convert.ToInt32(JobApplicationActivities.ApplicationSubmit));
                        cmd.Parameters.AddWithValue("p_jobhistorycomments", "Job Application is submitted");
                        cmd.Parameters.AddWithValue("p_eventdate_time", DBNull.Value);
                        cmd.Parameters.AddWithValue("p_interviewer_code", DBNull.Value);

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Dispose();
                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_CREATE_AND_JOB_APPLY.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        #endregion

                        string applicationInfo = "Applicant Name : " + jobDetailsDS.Job_Application[0].APPLICANTNAME + "<br>" +
                                                 "Applicant Login ID : " + ApplicantLogInID + "<br>";

                        if (!jobDetailsDS.Job_Application[0].IsPasswordNull())
                        { applicationInfo += "Password : " + jobDetailsDS.Job_Application[0].Password + "<br>"; }

                        //applicationInfo += "Application Tracking ID : " + JobApplicationID.ToString() + "<br>";
                        applicationInfo += "Application Tracking ID : " + ApplicationTrackingID + "<br>";
                        applicationInfo += "<br><br> Please check your email.";

                        #region Send Mail [Mandatory]
                        string returnMessage = "", messageBody = "";
                        //string companyName = ConfigurationManager.AppSettings["CompanyName"].Trim();
                        //string displayMsg = companyName + " : " + "Carrier account information";
                        string displayMsg = "Career account information";
                        string toEmailAddress = jobDetailsDS.Job_Application[0].EMAIL;

                        messageBody = "Congratulations! Your <b>Job Application</b> has been created successfully." + "<br><br>";
                        messageBody += "<b>Job Title : </b>" + jobDetailsDS.Job_Application[0].JobTitle + "<br><br>";
                        messageBody += "<b>Applicant Name : </b>" + jobDetailsDS.Job_Application[0].APPLICANTNAME + "<br>";
                        messageBody += "<b>Applicant Login ID : </b>" + ApplicantLogInID + "<br>";
                        if (!jobDetailsDS.Job_Application[0].IsPasswordNull()) messageBody += "<b>Password : </b>" + jobDetailsDS.Job_Application[0].Password + "<br><br>";
                        messageBody += "<b>Application Tracking ID : </b>" + ApplicationTrackingID + "<br><br>";
                        messageBody += "<br><br>";

                        Mail.Mail mail = new Mail.Mail();
                        if (IsEMailSendWithCredential)
                        {
                            returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, credentialEmailAddress, toEmailAddress, "", displayMsg, messageBody, displayMsg);
                        }
                        else
                        {
                            returnMessage = mail.SendEmail(host, port, credentialEmailAddress, toEmailAddress, "", displayMsg, messageBody, displayMsg);
                        }

                        if (returnMessage != "Email successfully sent.")  // Mail sending failed.
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_JOB_APPLICANT_UPDATE_AND_JOB_APPLY.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();

                            messageDS.ErrorMsg.AddErrorMsgRow("Mail sending failed.");
                            messageDS.AcceptChanges();

                            returnDS.Merge(errDS);
                            returnDS.Merge(messageDS);
                            returnDS.AcceptChanges();
                            return returnDS;
                        }
                        else
                        {
                            errDS.Clear();
                            errDS.AcceptChanges();

                            messageDS.Clear();
                            messageDS.SuccessMsg.AddSuccessMsgRow(applicationInfo);  // To Show in 'Confirmation Page'
                            messageDS.AcceptChanges();

                            jobDetailsDS = new JobDetailsDS();
                            JobDetailsDS.Job_ApplicationRow newRow = jobDetailsDS.Job_Application.NewJob_ApplicationRow();
                            newRow.ApplicantUserID = (Int32)ApplicantUserID;
                            newRow.JOBAPPLICATIONID = (Int32)JobApplicationID;
                            jobDetailsDS.Job_Application.AddJob_ApplicationRow(newRow);
                            jobDetailsDS.AcceptChanges();
                        }
                        #endregion
                    }
                }
                #endregion
            }
            #endregion

            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        private DataSet _getJobApplicantDetails(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            JobDetailsDS infoDS = new JobDetailsDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            string _jobTitle = stringDS.DataStrings[1].StringValue;
            if (_jobTitle == "-1")
            {
                cmd = DBCommandProvider.GetDBCommand("GetJobApplicantDetailsNoApply");
                cmd.Parameters["ApplicantUserID1"].Value = cmd.Parameters["ApplicantUserID2"].Value = stringDS.DataStrings[0].StringValue;
               
            }
            else 
            {
                cmd = DBCommandProvider.GetDBCommand("GetJobApplicantDetails");
                cmd.Parameters["ApplicantUserID1"].Value = cmd.Parameters["ApplicantUserID2"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["TITLEID"].Value = stringDS.DataStrings[1].StringValue;
            }

            if (cmd == null) return UtilDL.GetCommandNotFound();

            adapter.SelectCommand = cmd;
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.ApplicantDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_DETAILS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            jobDetailsDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getJobApplicantHistory_By_ApplicationTrackingID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            EmployeeHistDS jobDetailsDS = new EmployeeHistDS();
            EmployeeHistDS infoDS = new EmployeeHistDS();

            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            string _applicationTrackingID = "";
            string _empCode = "-1";
            _applicationTrackingID = stringDS.DataStrings[0].StringValue;
            _empCode = stringDS.DataStrings[1].StringValue;

            #region Get Applicant Education
            cmd = DBCommandProvider.GetDBCommand("GetJobApplicantEducation_By_TrackingID");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["ApplicationTrackingID"].Value = _applicationTrackingID;
            cmd.Parameters["EmployeeCode"].Value = _empCode;

            adapter.SelectCommand = cmd;
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Education.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_USER_HISTORY_BY_TRACKINGID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            infoDS.AcceptChanges();

            jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Education.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            infoDS.Clear();
            infoDS.AcceptChanges();
            #endregion

            #region Get Applicant Proffessional Trainings
            cmd = DBCommandProvider.GetDBCommand("GetJobApplicantTraining_By_TrackingID");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["ApplicationTrackingID"].Value = _applicationTrackingID;
            cmd.Parameters["EmployeeCode"].Value = _empCode;

            adapter.SelectCommand = cmd;
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Training.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_USER_HISTORY_BY_TRACKINGID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            infoDS.AcceptChanges();

            jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Training.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            infoDS.Clear();
            infoDS.AcceptChanges();
            #endregion

            #region Get Applicant Experience
            cmd = DBCommandProvider.GetDBCommand("GetJobApplicantExperience_By_TrackingID");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["ApplicationTrackingID"].Value = _applicationTrackingID;
            cmd.Parameters["EmployeeCode"].Value = _empCode;

            adapter.SelectCommand = cmd;
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Employment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_USER_HISTORY_BY_TRACKINGID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            infoDS.AcceptChanges();

            jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Employment.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            infoDS.Clear();
            infoDS.AcceptChanges();
            #endregion

            #region Get Applicant Spouse
            cmd = DBCommandProvider.GetDBCommand("GetJobApplicantSpouse_By_TrackingID");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["ApplicationTrackingID"].Value = _applicationTrackingID;
            cmd.Parameters["EmployeeCode"].Value = _empCode;

            adapter.SelectCommand = cmd;
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, infoDS, infoDS.Spouse.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_USER_HISTORY_BY_TRACKINGID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            infoDS.AcceptChanges();

            jobDetailsDS.Merge(infoDS.Tables[jobDetailsDS.Spouse.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();

            infoDS.Clear();
            infoDS.AcceptChanges();
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getJobApplicant_ShortListing(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            JobDetailsDS infoDS = new JobDetailsDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            bool bError;
            int nRowAffected;
            
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            bError = false;
            nRowAffected = -1;
            if (stringDS.DataStrings[0].StringValue == "ShortListingSearchItems")
            {
                cmd = DBCommandProvider.GetDBCommand("GetShortListingSearchItems");
                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;
                nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.ShortListingSearchItems.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            else if (stringDS.DataStrings[0].StringValue == "ShortListingSearchItems_Values")
            {
                DataSet dSet = new DataSet();
                cmd = DBCommandProvider.GetDBCommand("GetShortListingSearchItems_Values");
                cmd.CommandText = cmd.CommandText.Replace("COLUMN_NAME", stringDS.DataStrings[1].StringValue.Trim());
                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;
                nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.ShortListingSearchItems.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            else if (stringDS.DataStrings[0].StringValue == "ShortListingApplicantList")
            {
                DataSet dSet = new DataSet();
                cmd = DBCommandProvider.GetDBCommand("GetShortListingApplicantList");
                cmd.CommandText = cmd.CommandText + stringDS.DataStrings[1].StringValue.Trim();
                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;
                nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.Job_History.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            else if (stringDS.DataStrings[0].StringValue == "TotalApplicantNotApplyForJob")
            {
                DataSet dSet = new DataSet();
                cmd = DBCommandProvider.GetDBCommand("GetTotalApplicantNotApplayForJob");
                cmd.Parameters["jobTitleId"].Value = cmd.Parameters["jobTitleId1"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue.Trim());
                cmd.CommandText = cmd.CommandText + stringDS.DataStrings[1].StringValue.Trim();
                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;
                nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.Job_History.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_DETAILS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            jobDetailsDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getJobApplicant_ShortListingAdvanced(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataIntegerDS _DataIntg = new DataIntegerDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            JobDetailsDS infoDS = new JobDetailsDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            _DataIntg.Merge(inputDS.Tables[_DataIntg.DataIntegers.TableName], false, MissingSchemaAction.Error);
            _DataIntg.AcceptChanges();

            bError = false;
            nRowAffected = -1;
            if (stringDS.DataStrings[0].StringValue == "GetShortListingApplicantListByPaging")
            
            {
                DataSet dSet = new DataSet();

                cmd = DBCommandProvider.GetDBCommand("GetShortListingApplicantListByPaging");
                cmd.Parameters["jobTitle"].Value = _DataIntg.DataIntegers[2].IntegerValue;
                cmd.Parameters["pageindex"].Value = _DataIntg.DataIntegers[0].IntegerValue;
                cmd.Parameters["pagesize"].Value = _DataIntg.DataIntegers[1].IntegerValue;
                cmd.Parameters["Ora_query"].Value = stringDS.DataStrings[1].StringValue.Trim();
                
                if (cmd == null) return UtilDL.GetCommandNotFound();
                adapter.SelectCommand = cmd;
                nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.Job_History.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_JOB_APPLICANT_SHORTLISTING_AD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            jobDetailsDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createJob_ExamOrganizer(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();


            jobDS.Merge(inputDS.Tables[jobDS.ExamOrganizer.TableName], false, MissingSchemaAction.Error);
            jobDS.AcceptChanges();

            foreach (JobDetailsDS.ExamOrganizerRow row in jobDS.ExamOrganizer)
            {
                if (!row.IsIsForUpdateNull() && !row.IsForUpdate)
                {
                    cmd.CommandText = "PRO_JOB_EXAM_ORGANIZER_ADD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) return UtilDL.GetDBOperationFailed();
                    cmd.Parameters.AddWithValue("p_Ex_OrganizerID", genPK);
                }
                else
                {
                    cmd.CommandText = "PRO_JOB_EXAM_ORGANIZER_UPD";
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("p_Ex_OrganizerID", row.Ex_OrganizerID);
                }

                cmd.Parameters.AddWithValue("p_Ex_OrganizerName", row.Ex_OrganizerName);
                cmd.Parameters.AddWithValue("p_Ex_CountryID", row.Ex_Org_CountryID);

                if (!row.IsEx_Org_TelephoneNull()) cmd.Parameters.AddWithValue("p_Ex_Telephone", row.Ex_Org_Telephone);
                else cmd.Parameters.AddWithValue("p_Ex_Telephone", DBNull.Value);
                if (!row.IsEx_Org_FaxNull()) cmd.Parameters.AddWithValue("p_Ex_Fax", row.Ex_Org_Fax);
                else cmd.Parameters.AddWithValue("p_Ex_Fax", DBNull.Value);
                if (!row.IsEx_Org_EmailNull()) cmd.Parameters.AddWithValue("p_Ex_Email", row.Ex_Org_Email);
                else cmd.Parameters.AddWithValue("p_Ex_Email", DBNull.Value);
                if (!row.IsEx_Org_UrlNull()) cmd.Parameters.AddWithValue("p_Ex_URL", row.Ex_Org_Url);
                else cmd.Parameters.AddWithValue("p_Ex_URL", DBNull.Value);
                if (!row.IsEx_Org_AddressNull()) cmd.Parameters.AddWithValue("p_Ex_Address", row.Ex_Org_Address);
                else cmd.Parameters.AddWithValue("p_Ex_Address", DBNull.Value);
                if (!row.IsEx_Org_RemarksNull()) cmd.Parameters.AddWithValue("p_Ex_Remarks", row.Ex_Org_Remarks);
                else cmd.Parameters.AddWithValue("p_Ex_Remarks", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TRAINING_ORGANIZER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getJob_ExamOrganizer(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            JobDetailsDS infoDS = new JobDetailsDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bError = false;
            nRowAffected = -1;
            cmd = DBCommandProvider.GetDBCommand("GetJob_ExamOrganizerList");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;
            nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.ExamOrganizer.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_JOB_EXAM_ORGANIZER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            jobDetailsDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createJob_ExamResultUpload(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();


            jobDS.Merge(inputDS.Tables[jobDS.Exam_Result.TableName], false, MissingSchemaAction.Error);
            jobDS.AcceptChanges();

            foreach (JobDetailsDS.Exam_ResultRow row in jobDS.Exam_Result.Rows)
            {
                if (row.DoesApplicantDataExist)
                {
                    cmd.CommandText = "PRO_JOB_EXAM_RESULT_UPLOAD_UPD";
                }
                else 
                {
                    cmd.CommandText = "PRO_JOB_EXAM_RESULT_UPLOAD";
                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) return UtilDL.GetDBOperationFailed();
                    cmd.Parameters.AddWithValue("p_Ex_ResultID", genPK);
                }
                cmd.CommandType = CommandType.StoredProcedure;

                if (!row.IsEx_OrganizerIDNull()) cmd.Parameters.AddWithValue("p_Ex_OrganizerID", row.Ex_OrganizerID);
                else cmd.Parameters.AddWithValue("p_Ex_OrganizerID", DBNull.Value);

                if (!row.IsRollNumberNull()) cmd.Parameters.AddWithValue("p_RollNumber", Convert.ToInt32(row.RollNumber));
                else cmd.Parameters.AddWithValue("p_RollNumber", DBNull.Value);

                if (!row.IsJobTitleIDNull()) cmd.Parameters.AddWithValue("p_JobTitleID", row.JobTitleID);
                else cmd.Parameters.AddWithValue("p_JobApplicationID", DBNull.Value);

                if (!row.IsExam_DateNull()) cmd.Parameters.AddWithValue("p_Exam_Date", Convert.ToDateTime(row.Exam_Date));
                else cmd.Parameters.AddWithValue("p_Exam_Date", DBNull.Value);

                if (!row.IsMCQ_MarkNull()) cmd.Parameters.AddWithValue("p_MCQ_Mark", Convert.ToDecimal(row.MCQ_Mark));
                else cmd.Parameters.AddWithValue("p_MCQ_Mark", DBNull.Value);

                if (!row.IsWritten_MarkNull()) cmd.Parameters.AddWithValue("p_Written_Mark", Convert.ToDecimal(row.Written_Mark));
                else cmd.Parameters.AddWithValue("p_Written_Mark", DBNull.Value);

                if (!row.IsViva_Internal_MarkNull()) cmd.Parameters.AddWithValue("p_Viva_Internal_Mark", Convert.ToDecimal(row.Viva_Internal_Mark));
                else cmd.Parameters.AddWithValue("p_Viva_Internal_Mark", DBNull.Value);

                if (!row.IsViva_External_MarkNull()) cmd.Parameters.AddWithValue("p_Viva_External_Mark", Convert.ToDecimal(row.Viva_External_Mark));
                else cmd.Parameters.AddWithValue("p_Viva_External_Mark", DBNull.Value);

                if (!row.IsAverage_MarkNull()) cmd.Parameters.AddWithValue("p_Average_Mark", Convert.ToDecimal(row.Average_Mark));
                else cmd.Parameters.AddWithValue("p_Average_Mark", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_EXAM_RESULT_UPLOAD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        private DataSet _deleteJobExamOrganizer(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            jobDetailsDS.Merge(inputDS.Tables[jobDetailsDS.ExamOrganizer.TableName], false, MissingSchemaAction.Error);
            jobDetailsDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_EXAM_ORGANIZER_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            foreach (JobDetailsDS.ExamOrganizerRow row in jobDetailsDS.ExamOrganizer.Rows)
            {
                cmd.Parameters.AddWithValue("p_Ex_OrganizerID", row.Ex_OrganizerID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_EXAM_ORGANIZER_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.Ex_OrganizerName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.Ex_OrganizerName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _createExamHallInfoUpload(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            jobDS.Merge(inputDS.Tables[jobDS.ExamHallInfo.TableName], false, MissingSchemaAction.Error);
            jobDS.AcceptChanges();

            foreach (JobDetailsDS.ExamHallInfoRow row in jobDS.ExamHallInfo)
            {
                cmd.CommandText = "PRO_EXAM_HALL_INFO_ADD";
                cmd.CommandType = CommandType.StoredProcedure;

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();
                cmd.Parameters.AddWithValue("p_ExamHallID", genPK);

                cmd.Parameters.AddWithValue("p_JobTrackingID", row.JobTrackingID);
                cmd.Parameters.AddWithValue("p_Venue", row.Venue);
                cmd.Parameters.AddWithValue("p_RoomNo", row.RoomNo);
                cmd.Parameters.AddWithValue("p_StartRollNumber", row.StartRollNumber);
                cmd.Parameters.AddWithValue("p_EndRollNumber", row.EndRollNumber);
                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                cmd.Dispose();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EXAMHALL_INFO_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateJobHistoryByActivity(DataSet inputDS)
        {
            //declaration
            ErrorDS errDS = new ErrorDS();
            JobDetailsDS jobDS = new JobDetailsDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract DbConnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //extract Data
            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataBoolDS boolDS = new DataBoolDS();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            jobDS.Merge(inputDS.Tables[jobDS.Job_History.TableName], false, MissingSchemaAction.Error);
            jobDS.Merge(inputDS.Tables[jobDS.ExamInfo.TableName], false, MissingSchemaAction.Error);
            jobDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            
            #region Exam Info Create or Update
            if (Convert.ToInt32(jobDS.Job_History.Rows[0]["ACTIVITIESID"]) == 106 || Convert.ToInt32(jobDS.Job_History.Rows[0]["ACTIVITIESID"]) == 108)
            {
                #region Check if the Exam Info already exist or not.

                int nRowAffected = -1;
                bool bError = false;
                OleDbDataAdapter adapter = new OleDbDataAdapter();

                cmd = DBCommandProvider.GetDBCommand("ExamInfoExistStatus");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
                cmd.Parameters["JobTrackingID"].Value = jobDS.ExamInfo.Rows[0]["JobTrackingID"];
                if (Convert.ToInt32(jobDS.Job_History.Rows[0]["ACTIVITIESID"]) == 106) cmd.Parameters["Type"].Value = "Interview";
                else cmd.Parameters["Type"].Value = "Written";

                adapter.SelectCommand = cmd;
                JobDetailsDS jobDetailsDS = new JobDetailsDS();

                nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.ExamInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                cmd.Dispose();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_JOB_EXAM_ORGANIZER.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                jobDetailsDS.AcceptChanges();

                #endregion

                bool isUpdate = boolDS.DataBools[0].BoolValue;

                if (jobDetailsDS.ExamInfo.Count > 0 && isUpdate == false)
                {
                    //Skip the exam info insertion part.
                }
                else if (jobDetailsDS.ExamInfo.Count > 0 && isUpdate == true)
                {
                    cmd = new OleDbCommand();
                    cmd.CommandText = "PRO_EXAM_INFO_UPDATE";
                    cmd.CommandType = CommandType.StoredProcedure;

                    foreach (JobDetailsDS.ExamInfoRow row in jobDS.ExamInfo.Rows)
                    {
                        cmd.Parameters.AddWithValue("p_JobRefNo", row.JobRefNo);

                        //if (!row.IsRollNumberNull()) cmd.Parameters.AddWithValue("p_RollNumber", row.RollNumber);
                        //else cmd.Parameters.AddWithValue("p_RollNumber", DBNull.Value);

                        if (!row.IsJobTrackingIDNull()) cmd.Parameters.AddWithValue("p_JobTrackingID", row.JobTrackingID);
                        else cmd.Parameters.AddWithValue("p_JobTrackingID", DBNull.Value);

                        if (!row.IsTestTypeNull()) cmd.Parameters.AddWithValue("p_TestType", row.TestType);
                        else cmd.Parameters.AddWithValue("p_TestType", DBNull.Value);

                        if (!row.IsExamDateNull()) cmd.Parameters.AddWithValue("p_ExamDate", row.ExamDate);
                        else cmd.Parameters.AddWithValue("p_ExamDate", DBNull.Value);

                        if (!row.IsExamDayNull()) cmd.Parameters.AddWithValue("p_ExamDay", row.ExamDay);
                        else cmd.Parameters.AddWithValue("p_ExamDay", DBNull.Value);

                        if (!row.IsStartTimeNull()) cmd.Parameters.AddWithValue("p_StartTime", row.StartTime);
                        else cmd.Parameters.AddWithValue("p_StartTime", DBNull.Value);

                        if (!row.IsEndTimeNull()) cmd.Parameters.AddWithValue("p_EndTime", row.EndTime);
                        else cmd.Parameters.AddWithValue("p_EndTime", DBNull.Value);

                        if (!row.IsReportingTimeNull()) cmd.Parameters.AddWithValue("p_ReportingTime", row.ReportingTime);
                        else cmd.Parameters.AddWithValue("p_ReportingTime", DBNull.Value);

                        if (!row.IsVenueNull()) cmd.Parameters.AddWithValue("p_Venue", row.Venue);
                        else cmd.Parameters.AddWithValue("p_Venue", DBNull.Value);

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Parameters.Clear();
                        if (bError == true)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_JOB_HISTORY_STATUS_UPDATE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        break;
                    }
                }
                else
                {
                    cmd = new OleDbCommand();
                    cmd.CommandText = "PRO_EXAM_INFO_CREATE";
                    cmd.CommandType = CommandType.StoredProcedure;

                    foreach (JobDetailsDS.ExamInfoRow row in jobDS.ExamInfo.Rows)
                    {
                        long genPK = IDGenerator.GetNextGenericPK();
                        if (genPK == -1) return UtilDL.GetDBOperationFailed();

                        cmd.Parameters.AddWithValue("p_ExamID", genPK);

                        cmd.Parameters.AddWithValue("p_JobRefNo", row.JobRefNo);

                        if (!row.IsRollNumberNull()) cmd.Parameters.AddWithValue("p_RollNumber", row.RollNumber);
                        else cmd.Parameters.AddWithValue("p_RollNumber", DBNull.Value);

                        if (!row.IsJobTrackingIDNull()) cmd.Parameters.AddWithValue("p_JobTrackingID", row.JobTrackingID);
                        else cmd.Parameters.AddWithValue("p_JobTrackingID", DBNull.Value);

                        if (!row.IsTestTypeNull()) cmd.Parameters.AddWithValue("p_TestType", row.TestType);
                        else cmd.Parameters.AddWithValue("p_TestType", DBNull.Value);

                        if (!row.IsExamDateNull()) cmd.Parameters.AddWithValue("p_ExamDate", row.ExamDate);
                        else cmd.Parameters.AddWithValue("p_ExamDate", DBNull.Value);

                        if (!row.IsExamDayNull()) cmd.Parameters.AddWithValue("p_ExamDay", row.ExamDay);
                        else cmd.Parameters.AddWithValue("p_ExamDay", DBNull.Value);

                        if (!row.IsStartTimeNull()) cmd.Parameters.AddWithValue("p_StartTime", row.StartTime);
                        else cmd.Parameters.AddWithValue("p_StartTime", DBNull.Value);

                        if (!row.IsEndTimeNull()) cmd.Parameters.AddWithValue("p_EndTime", row.EndTime);
                        else cmd.Parameters.AddWithValue("p_EndTime", DBNull.Value);

                        if (!row.IsReportingTimeNull()) cmd.Parameters.AddWithValue("p_ReportingTime", row.ReportingTime);
                        else cmd.Parameters.AddWithValue("p_ReportingTime", DBNull.Value);

                        if (!row.IsVenueNull()) cmd.Parameters.AddWithValue("p_Venue", row.Venue);
                        else cmd.Parameters.AddWithValue("p_Venue", DBNull.Value);

                        bError = false;
                        nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd.Parameters.Clear();
                        if (bError == true)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_JOB_HISTORY_STATUS_UPDATE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        break;
                    }
                }
            }
            #endregion

            #region Create Job History
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_APPLY_HISTORY_CREATE";   
            cmd.CommandType = CommandType.StoredProcedure;

            for (int i = 0; i < stringDS.DataStrings.Rows.Count; i++)
            {
                cmd.Parameters.AddWithValue("p_applicationid", Convert.ToInt32(stringDS.DataStrings[i].StringValue));

                foreach (JobDetailsDS.Job_HistoryRow row in jobDS.Job_History.Rows)
                {
                    if (row.IsSENDERUSERIDNull() == false)
                    {
                        cmd.Parameters.AddWithValue("p_senderuserid", row.SENDERUSERID); 
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("p_senderuserid", DBNull.Value);
                    }
                    
                    if (row.IsRECEIVEDUSERIDNull() == false)
                    {
                        cmd.Parameters.AddWithValue("p_receiveduserid", row.RECEIVEDUSERID);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("p_receiveduserid", DBNull.Value);
                    }
                    
                    cmd.Parameters.AddWithValue("p_activitiesid", row.ACTIVITIESID);

                    if (row.IsJOBHISTORYCOMMENTSNull() == false)
                    {
                        cmd.Parameters.AddWithValue("p_jobhistorycomments", row.JOBHISTORYCOMMENTS);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("p_jobhistorycomments", DBNull.Value);
                    }

                    if (row.IsEVENTDATE_TIMENull() == false)
                    {
                        cmd.Parameters.AddWithValue("p_eventdate_time", row.EVENTDATE_TIME);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("p_eventdate_time", DBNull.Value);
                    }
                    if (row.IsINTERVIEWERCODENull() == false)
                    {
                        cmd.Parameters.AddWithValue("p_interviewer_code", row.INTERVIEWERCODE);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("p_interviewer_code", DBNull.Value);
                    }

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_HISTORY_STATUS_UPDATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    break; //as only one row is there.
                }
            }
            #endregion

            #region Preliminary Reject.
            if (Convert.ToInt32(jobDS.Job_History.Rows[0]["ACTIVITIESID"]) == 108)
            {
                cmd = new OleDbCommand();
                cmd.CommandText = "PRO_JOB_HISTORY_CREATE_DB";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("p_jobtrackingid", jobDS.ExamInfo.Rows[0]["JobTrackingID"]);

                foreach (JobDetailsDS.Job_HistoryRow row in jobDS.Job_History.Rows)
                {
                    if (row.IsSENDERUSERIDNull() == false)
                    {
                        cmd.Parameters.AddWithValue("p_senderuserid", row.SENDERUSERID);
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("p_senderuserid", DBNull.Value);
                    }
                    
                    cmd.Parameters.AddWithValue("p_receiveduserid", DBNull.Value);
                    
                    cmd.Parameters.AddWithValue("p_activitiesid", 109);

                    cmd.Parameters.AddWithValue("p_jobhistorycomments", "Preliminary Rejected.");
                    
                    cmd.Parameters.AddWithValue("p_eventdate_time", DBNull.Value);
                   
                    cmd.Parameters.AddWithValue("p_interviewer_code", DBNull.Value);
                    
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_JOB_HISTORY_STATUS_UPDATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    break; //as only one row is there.
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getExamInfoAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            JobDetailsDS jobDetailsDS = new JobDetailsDS();
            JobDetailsDS infoDS = new JobDetailsDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Data Integer Send as parameter
            DataIntegerDS intDS = new DataIntegerDS();
            intDS.Merge(inputDS.Tables[intDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            intDS.AcceptChanges();

            bError = false;
            nRowAffected = -1;

            cmd = DBCommandProvider.GetDBCommand("GetExamInfoAll");
            cmd.Parameters["JobTitleID"].Value = intDS.DataIntegers[0].IntegerValue;

            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            nRowAffected = ADOController.Instance.Fill(adapter, jobDetailsDS, jobDetailsDS.ExamInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Dispose();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_JOB_EXAM_ORGANIZER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            jobDetailsDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(jobDetailsDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getEmpolyeeListLatest(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeListLatest");
            Debug.Assert(cmd != null);
            cmd.CommandText += stringDS.DataStrings[0].StringValue;
            cmd.CommandText += " ORDER BY EmployeeCode ASC";

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            #region Data Load (Jarif 10-Sep-11 (Update))

            EmployeeDS empDS = new EmployeeDS();
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST_REC.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empDS.AcceptChanges();
            #endregion
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }

        public static DataSet _doesRollNumberExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            bool doesRollExist = false;
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DoesRollNumberExist");

            cmd.Parameters["JobTitleID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["RollNumber"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;
            DataSet tableData = new DataSet();
            nRowAffected = ADOController.Instance.Fill(adapter, tableData, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST_REC.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            if (tableData.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToInt32(tableData.Tables[0].Rows[0]["Count"]) == 1)
                    doesRollExist = true;
                    
            }
            boolDS.DataBools.AddDataBool(doesRollExist);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        public static DataSet _doesApplicantDataExistInExamResult(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            bool doesRollExist = false;
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DoesApplicantDataExistInExamResult");

            cmd.Parameters["RollNumber"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["JobTitleID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;
            DataSet tableData = new DataSet();
            nRowAffected = ADOController.Instance.Fill(adapter, tableData, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST_REC.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            if (tableData.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToInt32(tableData.Tables[0].Rows[0]["Count"]) == 1)
                    doesRollExist = true;

            }
            boolDS.DataBools.AddDataBool(doesRollExist);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getAttendanceSheet(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            int jobTitleID = 0;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            jobTitleID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetAttendanceSheet");
            cmd.Parameters["JobTitleID"].Value = jobTitleID;
            
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            JobDetailsDS jobDS = new JobDetailsDS();
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.Fill(adapter, jobDS, jobDS.AttendanceSheet.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATTENDANCE_SHEET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            jobDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(jobDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}
