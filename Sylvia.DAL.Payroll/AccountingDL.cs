using System;
using System.Data;
using System.Data.OleDb;
using Sylvia.Common;
using Sylvia.DAL.Database;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for AccountingDL.
    /// </summary>
    public class AccountingDL
    {
        public AccountingDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_COA_CHILD_NODE_CREATE_UPDATE:
                    return _CreateUpdateChildNodeCOA(inputDS);
                //case ActionID.ACTION_COA_SELECTED_NODE_UPDATE:
                //    return _updateSelectedNodeCOA(inputDS);
                case ActionID.ACTION_COA_SELECTED_NODE_DELETE:
                    return this._deleteSelectedNodeCOA(inputDS);
                case ActionID.NA_ACTION_GET_BASIC_ACCOUNTING_INFO:
                    return this._GetBasicAccountingInfo(inputDS);
                case ActionID.NA_ACTION_GET_ACC_SUPPLIER_LIST:
                    return this._getAcc_SupplierList(inputDS);
                case ActionID.NA_ACTION_GET_ACC_CUSTOMER_LIST:
                    return this._getAcc_CustomerList(inputDS);


                case ActionID.ACTION_ACC_SUPPLIER_CREATE:
                    return this._createAcc_Supplier(inputDS);
                case ActionID.ACTION_ACC_SUPPLIER_UPDATE:
                    return this._updateAcc_Supplier(inputDS);
                case ActionID.ACTION_ACC_SUPPLIER_DELETE:
                    return this._deleteAcc_Supplier(inputDS);

                case ActionID.ACTION_ACC_CUSTOMER_CREATE:
                    return this._createAcc_Customer(inputDS);
                case ActionID.ACTION_ACC_CUSTOMER_UPDATE:
                    return this._updateAcc_Customer(inputDS);
                case ActionID.ACTION_ACC_CUSTOMER_DELETE:
                    return this._deleteAcc_Customer(inputDS);

                case ActionID.NA_ACTION_GET_ACC_REFERENCE_LIST:
                    return this._getAcc_ReferenceList(inputDS);
                case ActionID.ACTION_ACC_REFERENCE_CREATE:
                    return this._createAcc_Reference(inputDS);
                case ActionID.ACTION_ACC_REFERENCE_UPDATE:
                    return this._updateAcc_Reference(inputDS);
                case ActionID.ACTION_ACC_REFERENCE_DELETE:
                    return this._deleteAcc_Reference(inputDS);

                case ActionID.NA_ACTION_GET_ACC_SEGMENT_SETUP:
                    return this._getAcc_SegmentSetUp(inputDS);
                case ActionID.NA_ACTION_GET_ACC_CODES:
                    return this.GetAccountCode(inputDS);

                case ActionID.NA_ACTION_GET_ACC_REP_SPECIFICATION:
                    return this._getAcc_ReportSpecification(inputDS);
                case ActionID.ACTION_ACC_REP_SPEC_NODE_CREATE:
                    return this._createAcc_ReportSpecification(inputDS);
                case ActionID.ACTION_ACC_REP_SPEC_NODE_UPDATE:
                    return this._updateAcc_ReportSpecification(inputDS);
                case ActionID.ACTION_ACC_REP_SPEC_NODE_DELETE:
                    return this._deleteAcc_ReportSpecification(inputDS);

                case ActionID.NA_ACTION_GET_COA_DETAILS:
                    return this._getCOA_Details(inputDS);
                case ActionID.NA_ACTION_GET_COA_PARTIES:
                    return this._getCOA_Parties(inputDS);
                case ActionID.NA_ACTION_GET_COA_LIST:
                    return this._getChartOfAccountList(inputDS);

                case ActionID.ACTION_ACC_VOUCHER_CREATE:
                    return this._saveGeneralVoucher(inputDS);
                case ActionID.NA_ACTION_ACC_GET_GENERAL_VOUCHER_BY_FILTER:
                    return this._getAcc_GeneralVoucher_ByFilter(inputDS);
                case ActionID.ACTION_ACC_GENERAL_VOUCHER_APPROVE:
                    return this._approveAcc_GeneralVoucher(inputDS);
                case ActionID.NA_ACTION_ACC_GET_MEMO_VOUCHERS:
                    return this._getAcc_MemoVouchers(inputDS);
                case ActionID.ACTION_ACC_VOUCHER_DELETE:
                    return this._deleteAcc_Voucher(inputDS);

                case ActionID.ACTION_ACC_APPROVAL_ROUTE_ADD:
                    return this._saveAcc_ApprovalRoute(inputDS);
                case ActionID.NA_ACTION_GET_ACC_APPROVAL_ROUTE_LIST:
                    return this._getAcc_ApprovalRoute(inputDS);
                    
                case ActionID.ACTION_ACC_BANK_INFO_CREATE:
                    return this._createAcc_BankInfo(inputDS);
                case ActionID.ACTION_ACC_BANK_INFO_UPDATE:
                    return this._updateAcc_BankInfo(inputDS);
                case ActionID.ACTION_ACC_BANK_INFO_DELETE:
                    return this._deleteAcc_BankInfo(inputDS);
                case ActionID.NA_ACTION_GET_ACC_BANK_INFO:
                    return this._getAcc_BankInfo(inputDS);

                case ActionID.ACTION_ACC_VOUCHER_TEMPLATE_CREATE:
                    return this._createAcc_VoucherTemplate(inputDS);
                case ActionID.ACTION_ACC_VOUCHER_TEMPLATE_UPDATE:
                    return this._updateAcc_VoucherTemplate(inputDS);
                case ActionID.ACTION_ACC_VOUCHER_TEMPLATE_DELETE:
                    return this._deleteAcc_VoucherTemplate(inputDS);
                case ActionID.NA_ACTION_GET_ACC_VOUCHER_TEMPLATES:
                    return this._getAcc_VoucherTemplates(inputDS);
                case ActionID.ACTION_ACC_VOUCHER_TEMPLATE_PROCESS:
                    return this._processAcc_VoucherTemplate(inputDS);

                case ActionID.NA_ACTION_GET_ACC_MONTH_END_PROCESSES:
                    return this._getAcc_MonthEndProcesses(inputDS);
                case ActionID.ACTION_ACC_MONTH_END_PROCESS_SAVE:
                    return this._saveAcc_MonthEndProcess(inputDS);
                case ActionID.ACTION_ACC_YEAR_END_PROCESS_SAVE:
                    return this._saveAcc_YearEndProcess(inputDS);

                case ActionID.NA_ACTION_GET_ACC_VOUCHERS_IN_DATE_RANGE:
                    return this._getAcc_Vouchers_InDateRange(inputDS);
                case ActionID.NA_ACTION_GET_ACC_LAST_YEAR_END_VOUCHER:
                    return this._getAcc_Last_YearEnd_Voucher(inputDS);

                case ActionID.NA_ACTION_GET_ACC_TRIAL_BALANCE_REPORT:
                    return this._getAcc_Trial_Balance_Report(inputDS);
                case ActionID.NA_ACTION_GET_ACC_BALANCE_SHEET_REPORT:
                    return this._getAcc_Balance_Sheet_Report(inputDS);


                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _CreateUpdateChildNodeCOA(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            accDS.Merge(inputDS.Tables[accDS.ChartOfAccounting.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create New Account Group
            long groupID = 0;
            if (!accDS.ChartOfAccounting[0].IsNewGroupNameNull())
            {
                string groupName = accDS.ChartOfAccounting[0].NewGroupName;

                OleDbCommand cmd_Group = new OleDbCommand();
                cmd_Group.CommandText = "PRO_ACC_COA_CODE_CREATE";
                cmd_Group.CommandType = CommandType.StoredProcedure;

                cmd_Group.Parameters.AddWithValue("p_Code_Type", "COA_GROUP");
                cmd_Group.Parameters.AddWithValue("p_Code", groupName.ToUpper().Substring(0, 3));
                cmd_Group.Parameters.AddWithValue("p_Code_Name", groupName);
                cmd_Group.Parameters.AddWithValue("p_Is_Active", true);
                cmd_Group.Parameters.Add("p_CodeID", OleDbType.Numeric, 15).Direction = ParameterDirection.Output;

                bool bError_Group = false;
                int nRowAffected_Group = -1;
                nRowAffected_Group = ADOController.Instance.ExecuteNonQuery(cmd_Group, connDS.DBConnections[0].ConnectionID, ref bError_Group);

                if (bError_Group)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_CODE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                groupID = Convert.ToInt32(cmd_Group.Parameters["p_CodeID"].Value.ToString());
                cmd_Group.Parameters.Clear();
            }
            #endregion

            if (StringDS.DataStrings[0].StringValue == "create")
            {
                #region Create Head
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ACC_COA_NODE_CREATE";
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (AccountingDS.ChartOfAccountingRow row in accDS.ChartOfAccounting.Rows)
                {
                    long genPK = IDGenerator.GetNextGenericPK();

                    if (genPK == -1) UtilDL.GetDBOperationFailed();
                    else cmd.Parameters.AddWithValue("p_Coa_Id", (object)genPK);

                    if (!row.IsCoa_ParentNull()) cmd.Parameters.AddWithValue("p_Coa_Parent", row.Coa_Parent);
                    else cmd.Parameters.AddWithValue("p_Coa_Parent", DBNull.Value);

                    if (!row.IsCoa_LevelNull()) cmd.Parameters.AddWithValue("p_Coa_Level", row.Coa_Level);
                    else cmd.Parameters.AddWithValue("p_Coa_Level", DBNull.Value);

                    if (!row.IsCoa_Category_IdNull()) cmd.Parameters.AddWithValue("p_Coa_Category_Id", row.Coa_Category_Id);
                    else cmd.Parameters.AddWithValue("p_Coa_Category_Id", DBNull.Value);

                    if (!row.IsCoa_HeadNull()) cmd.Parameters.AddWithValue("p_Coa_Head", row.Coa_Head);
                    else cmd.Parameters.AddWithValue("p_Coa_Head", DBNull.Value);

                    if (!row.IsCoa_Group_IDNull())
                    {
                        if (row.Coa_Group_ID == 0) cmd.Parameters.AddWithValue("p_Coa_Group_ID", groupID);
                        else cmd.Parameters.AddWithValue("p_Coa_Group_ID", row.Coa_Group_ID);
                    }
                    else cmd.Parameters.AddWithValue("p_Coa_Group_ID", DBNull.Value);


                    if (!row.IsCostCenterCodeNull()) cmd.Parameters.AddWithValue("p_CostCenterCode", row.CostCenterCode);
                    else cmd.Parameters.AddWithValue("p_CostCenterCode", DBNull.Value);

                    if (!row.IsCurrencyCodeNull()) cmd.Parameters.AddWithValue("p_CurrencyCode", row.CurrencyCode);
                    else cmd.Parameters.AddWithValue("p_CurrencyCode", DBNull.Value);

                    if (!row.IsBs_Is_CodeNull()) cmd.Parameters.AddWithValue("p_Bs_Is_Code", row.Bs_Is_Code);
                    else cmd.Parameters.AddWithValue("p_Bs_Is_Code", DBNull.Value);

                    if (!row.IsCf_Dr_CodeNull()) cmd.Parameters.AddWithValue("p_Cf_Dr_Code", row.Cf_Dr_Code);
                    else cmd.Parameters.AddWithValue("p_Cf_Dr_Code", DBNull.Value);

                    if (!row.IsCf_Cr_CodeNull()) cmd.Parameters.AddWithValue("p_Cf_Cr_Code", row.Cf_Cr_Code);
                    else cmd.Parameters.AddWithValue("p_Cf_Cr_Code", DBNull.Value);


                    if (!row.IsAccountCodeNull()) cmd.Parameters.AddWithValue("p_AccountCode", row.AccountCode);
                    else cmd.Parameters.AddWithValue("p_AccountCode", DBNull.Value);

                    if (!row.IsCompanyCodeNull()) cmd.Parameters.AddWithValue("p_CompanyCode", row.CompanyCode);
                    else cmd.Parameters.AddWithValue("p_CompanyCode", DBNull.Value);

                    if (!row.IsDepartmentCodeNull()) cmd.Parameters.AddWithValue("p_DepartmentCode", row.DepartmentCode);
                    else cmd.Parameters.AddWithValue("p_DepartmentCode", DBNull.Value);

                    if (!row.IsLocationCodeNull()) cmd.Parameters.AddWithValue("p_LocationCode", row.LocationCode);
                    else cmd.Parameters.AddWithValue("p_LocationCode", DBNull.Value);

                    if (!row.IsProjectCodeNull()) cmd.Parameters.AddWithValue("p_ProjectCode", row.ProjectCode);
                    else cmd.Parameters.AddWithValue("p_ProjectCode", DBNull.Value);

                    if (!row.IsCustomerCodeNull()) cmd.Parameters.AddWithValue("p_CustomerCode", row.CustomerCode);
                    else cmd.Parameters.AddWithValue("p_CustomerCode", DBNull.Value);

                    if (!row.IsSupplierNumberNull()) cmd.Parameters.AddWithValue("p_SupplierNumber", row.SupplierNumber);
                    else cmd.Parameters.AddWithValue("p_SupplierNumber", DBNull.Value);

                    if (!row.IsEmployeeCodeNull()) cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                    else cmd.Parameters.AddWithValue("p_EmployeeCode", DBNull.Value);

                    if (!row.IsReferenceCodeNull()) cmd.Parameters.AddWithValue("p_ReferenceCode", row.ReferenceCode);
                    else cmd.Parameters.AddWithValue("p_ReferenceCode", DBNull.Value);

                    if (!row.IsCoa_DetailsNull()) cmd.Parameters.AddWithValue("p_COA_Details", row.Coa_Details);
                    else cmd.Parameters.AddWithValue("p_COA_Details", DBNull.Value);

                    if (!row.IsBankCodeNull()) cmd.Parameters.AddWithValue("p_BankCode", row.BankCode);
                    else cmd.Parameters.AddWithValue("p_BankCode", DBNull.Value);

                    if (!row.IsTr_AllowNull()) cmd.Parameters.AddWithValue("p_IsTrAllowed", row.Tr_Allow);
                    else cmd.Parameters.AddWithValue("p_IsTrAllowed", DBNull.Value);

                    if (!row.IsNegative_Bal_AllowNull()) cmd.Parameters.AddWithValue("p_IsNegBalanceAllowed", row.Negative_Bal_Allow);
                    else cmd.Parameters.AddWithValue("p_IsNegBalanceAllowed", DBNull.Value);

                    if (!row.IsRe_FlagNull()) cmd.Parameters.AddWithValue("p_RE_Flag", row.Re_Flag);
                    else cmd.Parameters.AddWithValue("p_RE_Flag", DBNull.Value);

                    if (!row.IsIs_ActiveNull()) cmd.Parameters.AddWithValue("p_IsActive", row.Is_Active);
                    else cmd.Parameters.AddWithValue("p_IsActive", DBNull.Value);

                    if (!row.IsCreated_ByNull()) cmd.Parameters.AddWithValue("p_CreatedBy", row.Created_By);
                    else cmd.Parameters.AddWithValue("p_CreatedBy", DBNull.Value);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_COA_CHILD_NODE_CREATE_UPDATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion
            }
            else if (StringDS.DataStrings[0].StringValue == "update")
            {
                #region Update Head
                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ACC_COA_NODE_UPDATE";
                cmd.CommandType = CommandType.StoredProcedure;

                foreach (AccountingDS.ChartOfAccountingRow row in accDS.ChartOfAccounting.Rows)
                {
                    cmd.Parameters.AddWithValue("p_Coa_Id", row.Coa_Id);

                    if (!row.IsCoa_ParentNull()) cmd.Parameters.AddWithValue("p_Coa_Parent", row.Coa_Parent);
                    else cmd.Parameters.AddWithValue("p_Coa_Parent", DBNull.Value);

                    if (!row.IsCoa_LevelNull()) cmd.Parameters.AddWithValue("p_Coa_Level", row.Coa_Level);
                    else cmd.Parameters.AddWithValue("p_Coa_Level", DBNull.Value);

                    if (!row.IsCoa_Category_IdNull()) cmd.Parameters.AddWithValue("p_Coa_Category_Id", row.Coa_Category_Id);
                    else cmd.Parameters.AddWithValue("p_Coa_Category_Id", DBNull.Value);

                    if (!row.IsCoa_HeadNull()) cmd.Parameters.AddWithValue("p_Coa_Head", row.Coa_Head);
                    else cmd.Parameters.AddWithValue("p_Coa_Head", DBNull.Value);

                    if (!row.IsCoa_Group_IDNull())
                    {
                        if (row.Coa_Group_ID == 0) cmd.Parameters.AddWithValue("p_Coa_Group_ID", groupID);
                        else cmd.Parameters.AddWithValue("p_Coa_Group_ID", row.Coa_Group_ID);
                    }
                    else cmd.Parameters.AddWithValue("p_Coa_Group_ID", DBNull.Value);


                    if (!row.IsCostCenterCodeNull()) cmd.Parameters.AddWithValue("p_CostCenterCode", row.CostCenterCode);
                    else cmd.Parameters.AddWithValue("p_CostCenterCode", DBNull.Value);

                    if (!row.IsCurrencyCodeNull()) cmd.Parameters.AddWithValue("p_CurrencyCode", row.CurrencyCode);
                    else cmd.Parameters.AddWithValue("p_CurrencyCode", DBNull.Value);

                    if (!row.IsBs_Is_CodeNull()) cmd.Parameters.AddWithValue("p_Bs_Is_Code", row.Bs_Is_Code);
                    else cmd.Parameters.AddWithValue("p_Bs_Is_Code", DBNull.Value);

                    if (!row.IsCf_Dr_CodeNull()) cmd.Parameters.AddWithValue("p_Cf_Dr_Code", row.Cf_Dr_Code);
                    else cmd.Parameters.AddWithValue("p_Cf_Dr_Code", DBNull.Value);

                    if (!row.IsCf_Cr_CodeNull()) cmd.Parameters.AddWithValue("p_Cf_Cr_Code", row.Cf_Cr_Code);
                    else cmd.Parameters.AddWithValue("p_Cf_Cr_Code", DBNull.Value);


                    if (!row.IsAccountCodeNull()) cmd.Parameters.AddWithValue("p_AccountCode", row.AccountCode);
                    else cmd.Parameters.AddWithValue("p_AccountCode", DBNull.Value);

                    if (!row.IsCompanyCodeNull()) cmd.Parameters.AddWithValue("p_CompanyCode", row.CompanyCode);
                    else cmd.Parameters.AddWithValue("p_CompanyCode", DBNull.Value);

                    if (!row.IsDepartmentCodeNull()) cmd.Parameters.AddWithValue("p_DepartmentCode", row.DepartmentCode);
                    else cmd.Parameters.AddWithValue("p_DepartmentCode", DBNull.Value);

                    if (!row.IsLocationCodeNull()) cmd.Parameters.AddWithValue("p_LocationCode", row.LocationCode);
                    else cmd.Parameters.AddWithValue("p_LocationCode", DBNull.Value);

                    if (!row.IsProjectCodeNull()) cmd.Parameters.AddWithValue("p_ProjectCode", row.ProjectCode);
                    else cmd.Parameters.AddWithValue("p_ProjectCode", DBNull.Value);

                    if (!row.IsCustomerCodeNull()) cmd.Parameters.AddWithValue("p_CustomerCode", row.CustomerCode);
                    else cmd.Parameters.AddWithValue("p_CustomerCode", DBNull.Value);

                    if (!row.IsSupplierNumberNull()) cmd.Parameters.AddWithValue("p_SupplierNumber", row.SupplierNumber);
                    else cmd.Parameters.AddWithValue("p_SupplierNumber", DBNull.Value);

                    if (!row.IsEmployeeCodeNull()) cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                    else cmd.Parameters.AddWithValue("p_EmployeeCode", DBNull.Value);

                    if (!row.IsReferenceCodeNull()) cmd.Parameters.AddWithValue("p_ReferenceCode", row.ReferenceCode);
                    else cmd.Parameters.AddWithValue("p_ReferenceCode", DBNull.Value);

                    if (!row.IsCoa_DetailsNull()) cmd.Parameters.AddWithValue("p_COA_Details", row.Coa_Details);
                    else cmd.Parameters.AddWithValue("p_COA_Details", DBNull.Value);

                    if (!row.IsBankCodeNull()) cmd.Parameters.AddWithValue("p_BankCode", row.BankCode);
                    else cmd.Parameters.AddWithValue("p_BankCode", DBNull.Value);

                    if (!row.IsTr_AllowNull()) cmd.Parameters.AddWithValue("p_IsTrAllowed", row.Tr_Allow);
                    else cmd.Parameters.AddWithValue("p_IsTrAllowed", DBNull.Value);

                    if (!row.IsNegative_Bal_AllowNull()) cmd.Parameters.AddWithValue("p_IsNegBalanceAllowed", row.Negative_Bal_Allow);
                    else cmd.Parameters.AddWithValue("p_IsNegBalanceAllowed", DBNull.Value);

                    if (!row.IsRe_FlagNull()) cmd.Parameters.AddWithValue("p_RE_Flag", row.Re_Flag);
                    else cmd.Parameters.AddWithValue("p_RE_Flag", DBNull.Value);

                    if (!row.IsIs_ActiveNull()) cmd.Parameters.AddWithValue("p_IsActive", row.Is_Active);
                    else cmd.Parameters.AddWithValue("p_IsActive", DBNull.Value);

                    if (!row.IsUpdated_ByNull()) cmd.Parameters.AddWithValue("p_UpdatedBy", row.Updated_By);
                    else cmd.Parameters.AddWithValue("p_UpdatedBy", DBNull.Value);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_COA_CHILD_NODE_CREATE_UPDATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteSelectedNodeCOA(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_COA_NODE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("p_Coa_Id", Convert.ToInt32(stringDS.DataStrings[0].StringValue));

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_COA_SELECTED_NODE_DELETE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetBasicAccountingInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS StringDS = new DataStringDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            
            if (StringDS.DataStrings[0].StringValue == "TreeValueOfChartOfAcc")
            {
                cmd = DBCommandProvider.GetDBCommand("GetTreeValueOfChartOfAcc");
                cmd.Parameters["p_COA_Parent"].Value = Convert.ToInt32(StringDS.DataStrings[1].StringValue);
            }
            else if (StringDS.DataStrings[0].StringValue == "TreeValueAll")
            {
                cmd = DBCommandProvider.GetDBCommand("GetTreeValueAll");
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AccountingDS AccDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, AccDS, AccDS.ChartOfAccounting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BASIC_ACCOUNTING_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            AccDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();            

            returnDS.Merge(AccDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }


        private DataSet _getAcc_SupplierList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS StringDS = new DataStringDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAccSupplierList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AccountingDS AccDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, AccDS, AccDS.Acc_Supplier.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_SUPPLIER_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            AccDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(AccDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createAcc_Supplier(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_SUPPLIER_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Supplier.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AccountingDS.Acc_SupplierRow row in accDS.Acc_Supplier.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("p_SupplierId", (object)genPK);

                if (!row.IsSupplierNumberNull()) cmd.Parameters.AddWithValue("p_SupplierNumber", row.SupplierNumber);
                else cmd.Parameters.AddWithValue("p_SupplierNumber", DBNull.Value);

                if (!row.IsSupplierNameNull()) cmd.Parameters.AddWithValue("p_SupplierName", row.SupplierName);
                else cmd.Parameters.AddWithValue("p_SupplierName", DBNull.Value);

                if (!row.IsBusinessUnitNull()) cmd.Parameters.AddWithValue("p_BusinessUnit", row.BusinessUnit);
                else cmd.Parameters.AddWithValue("p_BusinessUnit", DBNull.Value);

                if (!row.IsPaymentTermsNull()) cmd.Parameters.AddWithValue("p_PaymentTerms", row.PaymentTerms);
                else cmd.Parameters.AddWithValue("p_PaymentTerms", DBNull.Value);

                if (!row.IsPhoneNumberNull()) cmd.Parameters.AddWithValue("p_PhoneNumber", row.PhoneNumber);
                else cmd.Parameters.AddWithValue("p_PhoneNumber", DBNull.Value);

                if (!row.IsEmailNull()) cmd.Parameters.AddWithValue("p_Email", row.Email);
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                if (!row.IsCountryIdNull()) cmd.Parameters.AddWithValue("p_CountryId", row.CountryId);
                else cmd.Parameters.AddWithValue("p_CountryId", DBNull.Value);

                if (!row.IsDivisionIdNull()) cmd.Parameters.AddWithValue("p_DivisionId", row.DivisionId);
                else cmd.Parameters.AddWithValue("p_DivisionId", DBNull.Value);

                if (!row.IsMailingAddressNull()) cmd.Parameters.AddWithValue("p_MailingAddress", row.MailingAddress);
                else cmd.Parameters.AddWithValue("p_MailingAddress", DBNull.Value);

                if (!row.IsIs_ActiveNull()) cmd.Parameters.AddWithValue("p_Is_Active", row.Is_Active);
                else cmd.Parameters.AddWithValue("p_Is_Active", DBNull.Value);

                if (!row.IsCreated_ByNull()) cmd.Parameters.AddWithValue("p_Created_By", row.Created_By);
                else cmd.Parameters.AddWithValue("p_Created_By", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_SUPPLIER_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateAcc_Supplier(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_SUPPLIER_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Supplier.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (AccountingDS.Acc_SupplierRow row in accDS.Acc_Supplier.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (!row.IsSupplierIDNull()) cmd.Parameters.AddWithValue("p_SupplierID", row.SupplierID);
                else cmd.Parameters.AddWithValue("p_SupplierID", DBNull.Value);

                if (!row.IsSupplierNameNull()) cmd.Parameters.AddWithValue("p_SupplierName", row.SupplierName);
                else cmd.Parameters.AddWithValue("p_SupplierName", DBNull.Value);

                if (!row.IsBusinessUnitNull()) cmd.Parameters.AddWithValue("p_BusinessUnit", row.BusinessUnit);
                else cmd.Parameters.AddWithValue("p_BusinessUnit", DBNull.Value);

                if (!row.IsPaymentTermsNull()) cmd.Parameters.AddWithValue("p_PaymentTerms", row.PaymentTerms);
                else cmd.Parameters.AddWithValue("p_PaymentTerms", DBNull.Value);

                if (!row.IsPhoneNumberNull()) cmd.Parameters.AddWithValue("p_PhoneNumber", row.PhoneNumber);
                else cmd.Parameters.AddWithValue("p_PhoneNumber", DBNull.Value);

                if (!row.IsEmailNull()) cmd.Parameters.AddWithValue("p_Email", row.Email);
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                if (!row.IsCountryIdNull()) cmd.Parameters.AddWithValue("p_CountryId", row.CountryId);
                else cmd.Parameters.AddWithValue("p_CountryId", DBNull.Value);

                if (!row.IsDivisionIdNull()) cmd.Parameters.AddWithValue("p_DivisionId", row.DivisionId);
                else cmd.Parameters.AddWithValue("p_DivisionId", DBNull.Value);

                if (!row.IsMailingAddressNull()) cmd.Parameters.AddWithValue("p_MailingAddress", row.MailingAddress);
                else cmd.Parameters.AddWithValue("p_MailingAddress", DBNull.Value);

                if (!row.IsIs_ActiveNull()) cmd.Parameters.AddWithValue("p_Is_Active", row.Is_Active);
                else cmd.Parameters.AddWithValue("p_Is_Active", DBNull.Value);

                if (!row.IsUpdated_ByNull()) cmd.Parameters.AddWithValue("p_Updated_By", row.Updated_By);
                else cmd.Parameters.AddWithValue("p_Updated_By", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_SUPPLIER_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteAcc_Supplier(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_SUPPLIER_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Supplier.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (AccountingDS.Acc_SupplierRow row in accDS.Acc_Supplier.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (!row.IsSupplierIDNull()) cmd.Parameters.AddWithValue("p_SupplierID", row.SupplierID);
                else cmd.Parameters.AddWithValue("p_SupplierID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_SUPPLIER_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(String.Format(@"{0} - [{1}]", row.SupplierName, row.SupplierNumber));
                else messageDS.ErrorMsg.AddErrorMsgRow(String.Format(@"{0} - [{1}]", row.SupplierName, row.SupplierNumber));
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAcc_CustomerList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS StringDS = new DataStringDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAccCustomerList");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AccountingDS AccDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, AccDS, AccDS.Acc_Customer.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_CUSTOMER_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            AccDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(AccDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createAcc_Customer(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_CUSTOMER_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Customer.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AccountingDS.Acc_CustomerRow row in accDS.Acc_Customer.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("p_CustomerID", (object)genPK);

                if (!row.IsCustomerCodeNull()) cmd.Parameters.AddWithValue("p_CustomerCode", row.CustomerCode);
                else cmd.Parameters.AddWithValue("p_CustomerCode", DBNull.Value);

                if (!row.IsCustomerDescNull()) cmd.Parameters.AddWithValue("p_CustomerDesc", row.CustomerDesc);
                else cmd.Parameters.AddWithValue("p_CustomerDesc", DBNull.Value);

                if (!row.IsPhoneNumberNull()) cmd.Parameters.AddWithValue("p_PhoneNumber", row.PhoneNumber);
                else cmd.Parameters.AddWithValue("p_PhoneNumber", DBNull.Value);

                if (!row.IsEmailNull()) cmd.Parameters.AddWithValue("p_Email", row.Email);
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                if (!row.IsBusinessAddressNull()) cmd.Parameters.AddWithValue("p_BusinessAddress", row.BusinessAddress);
                else cmd.Parameters.AddWithValue("p_BusinessAddress", DBNull.Value);

                if (!row.IsIs_ActiveNull()) cmd.Parameters.AddWithValue("p_Is_Active", row.Is_Active);
                else cmd.Parameters.AddWithValue("p_Is_Active", DBNull.Value);

                if (!row.IsCreated_ByNull()) cmd.Parameters.AddWithValue("p_Created_By", row.Created_By);
                else cmd.Parameters.AddWithValue("p_Created_By", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_CUSTOMER_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateAcc_Customer(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_CUSTOMER_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Customer.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (AccountingDS.Acc_CustomerRow row in accDS.Acc_Customer.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (!row.IsCustomerIdNull()) cmd.Parameters.AddWithValue("p_CustomerID", row.CustomerId);
                else cmd.Parameters.AddWithValue("p_CustomerID", DBNull.Value);

                if (!row.IsCustomerDescNull()) cmd.Parameters.AddWithValue("p_CustomerDesc", row.CustomerDesc);
                else cmd.Parameters.AddWithValue("p_CustomerDesc", DBNull.Value);

                if (!row.IsPhoneNumberNull()) cmd.Parameters.AddWithValue("p_PhoneNumber", row.PhoneNumber);
                else cmd.Parameters.AddWithValue("p_PhoneNumber", DBNull.Value);

                if (!row.IsEmailNull()) cmd.Parameters.AddWithValue("p_Email", row.Email);
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                if (!row.IsBusinessAddressNull()) cmd.Parameters.AddWithValue("p_BusinessAddress", row.BusinessAddress);
                else cmd.Parameters.AddWithValue("p_BusinessAddress", DBNull.Value);

                if (!row.IsIs_ActiveNull()) cmd.Parameters.AddWithValue("p_Is_Active", row.Is_Active);
                else cmd.Parameters.AddWithValue("p_Is_Active", DBNull.Value);

                if (!row.IsUpdated_ByNull()) cmd.Parameters.AddWithValue("p_Updated_By", row.Updated_By);
                else cmd.Parameters.AddWithValue("p_Updated_By", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_CUSTOMER_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteAcc_Customer(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_CUSTOMER_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Customer.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (AccountingDS.Acc_CustomerRow row in accDS.Acc_Customer.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (!row.IsCustomerIdNull()) cmd.Parameters.AddWithValue("p_CustomerID", row.CustomerId);
                else cmd.Parameters.AddWithValue("p_CustomerID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_CUSTOMER_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(String.Format(@"{0} - [{1}]", row.CustomerDesc, row.CustomerCode));
                else messageDS.ErrorMsg.AddErrorMsgRow(String.Format(@"{0} - [{1}]", row.CustomerDesc, row.CustomerCode));
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAcc_ReferenceList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS StringDS = new DataStringDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAccReferenceList");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AccountingDS AccDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, AccDS, AccDS.Acc_Reference.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_REFERENCE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            AccDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(AccDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createAcc_Reference(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_REFERENCE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            accDS.Merge(inputDS.Tables[accDS.Acc_Reference.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AccountingDS.Acc_ReferenceRow row in accDS.Acc_Reference.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("p_ReferenceID", (object)genPK);

                if (!row.IsReferenceCodeNull()) cmd.Parameters.AddWithValue("p_ReferenceCode", row.ReferenceCode);
                else cmd.Parameters.AddWithValue("p_ReferenceCode", DBNull.Value);

                if (!row.IsCountryIDNull()) cmd.Parameters.AddWithValue("p_CountryID", row.CountryID);
                else cmd.Parameters.AddWithValue("p_CountryID", DBNull.Value);

                if (!row.IsIs_ActiveNull()) cmd.Parameters.AddWithValue("p_Is_Active", row.Is_Active);
                else cmd.Parameters.AddWithValue("p_Is_Active", DBNull.Value);

                if (!row.IsCreated_ByNull()) cmd.Parameters.AddWithValue("p_Created_By", row.Created_By);
                else cmd.Parameters.AddWithValue("p_Created_By", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_REFERENCE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateAcc_Reference(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_REFERENCE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Reference.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (AccountingDS.Acc_ReferenceRow row in accDS.Acc_Reference.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (!row.IsReferenceIDNull()) cmd.Parameters.AddWithValue("p_ReferenceID", row.ReferenceID);
                else cmd.Parameters.AddWithValue("p_ReferenceID", DBNull.Value);

                if (!row.IsReferenceCodeNull()) cmd.Parameters.AddWithValue("p_ReferenceCode", row.ReferenceCode);
                else cmd.Parameters.AddWithValue("p_ReferenceCode", DBNull.Value);

                if (!row.IsCountryIDNull()) cmd.Parameters.AddWithValue("p_CountryID", row.CountryID);
                else cmd.Parameters.AddWithValue("p_CountryID", DBNull.Value);

                if (!row.IsIs_ActiveNull()) cmd.Parameters.AddWithValue("p_Is_Active", row.Is_Active);
                else cmd.Parameters.AddWithValue("p_Is_Active", DBNull.Value);

                if (!row.IsUpdated_ByNull()) cmd.Parameters.AddWithValue("p_Updated_By", row.Updated_By);
                else cmd.Parameters.AddWithValue("p_Updated_By", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_REFERENCE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteAcc_Reference(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_REFERENCE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Reference.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (AccountingDS.Acc_ReferenceRow row in accDS.Acc_Reference.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (!row.IsReferenceIDNull()) cmd.Parameters.AddWithValue("p_ReferenceID", row.ReferenceID);
                else cmd.Parameters.AddWithValue("p_ReferenceID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_REFERENCE_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.ReferenceCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.ReferenceCode);
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAcc_SegmentSetUp(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS StringDS = new DataStringDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAccSegmentSetup");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AccountingDS AccDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, AccDS, AccDS.Acc_SegmentSetup.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_SEGMENT_SETUP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            AccDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(AccDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet GetAccountCode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAccCodesByCodeType");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["p_CodeType"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AccountingDS AccDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, AccDS, AccDS.Acc_Code.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_CODES.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            AccDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(AccDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getAcc_ReportSpecification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS StringDS = new DataStringDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAccReportSpecificationList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AccountingDS AccDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, AccDS, AccDS.ReportSpecification.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_REP_SPECIFICATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            AccDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(AccDS.ReportSpecification);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createAcc_ReportSpecification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_REP_SPEC_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            accDS.Merge(inputDS.Tables[accDS.ReportSpecification.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AccountingDS.ReportSpecificationRow row in accDS.ReportSpecification.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("p_Spec_ID", (object)genPK);

                if (!row.IsRep_CodeNull()) cmd.Parameters.AddWithValue("p_Rep_Code", row.Rep_Code);
                else cmd.Parameters.AddWithValue("p_Rep_Code", DBNull.Value);

                if (!row.IsRep_DescNull()) cmd.Parameters.AddWithValue("p_Rep_Desc", row.Rep_Desc);
                else cmd.Parameters.AddWithValue("p_Rep_Desc", DBNull.Value);

                if (!row.IsRep_TypeNull()) cmd.Parameters.AddWithValue("p_Rep_Type", row.Rep_Type);
                else cmd.Parameters.AddWithValue("p_Rep_Type", DBNull.Value);

                if (!row.IsRep_ParentNull()) cmd.Parameters.AddWithValue("p_Rep_Parent", row.Rep_Parent);
                else cmd.Parameters.AddWithValue("p_Rep_Parent", DBNull.Value);

                if (!row.IsCode_LevelNull()) cmd.Parameters.AddWithValue("p_Code_Level", row.Code_Level);
                else cmd.Parameters.AddWithValue("p_Code_Level", DBNull.Value);

                if (!row.IsRE_FlagNull()) cmd.Parameters.AddWithValue("p_RE_Flag", row.RE_Flag);
                else cmd.Parameters.AddWithValue("p_RE_Flag", DBNull.Value);

                if (!row.IsSign_FlagNull()) cmd.Parameters.AddWithValue("p_Sign_Flag", row.Sign_Flag);
                else cmd.Parameters.AddWithValue("p_Sign_Flag", DBNull.Value);

                if (!row.IsSum_LabelNull()) cmd.Parameters.AddWithValue("p_Sum_Label", row.Sum_Label);
                else cmd.Parameters.AddWithValue("p_Sum_Label", DBNull.Value);

                if (!row.IsCreated_ByNull()) cmd.Parameters.AddWithValue("p_Created_By", row.Created_By);
                else cmd.Parameters.AddWithValue("p_Created_By", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_REP_SPEC_NODE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateAcc_ReportSpecification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_REP_SPEC_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            accDS.Merge(inputDS.Tables[accDS.ReportSpecification.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AccountingDS.ReportSpecificationRow row in accDS.ReportSpecification.Rows)
            {
                if (!row.IsSpec_IDNull()) cmd.Parameters.AddWithValue("p_Spec_ID", row.Spec_ID);
                else cmd.Parameters.AddWithValue("p_Spec_ID", DBNull.Value);

                if (!row.IsRep_CodeNull()) cmd.Parameters.AddWithValue("p_Rep_Code", row.Rep_Code);
                else cmd.Parameters.AddWithValue("p_Rep_Code", DBNull.Value);

                if (!row.IsRep_DescNull()) cmd.Parameters.AddWithValue("p_Rep_Desc", row.Rep_Desc);
                else cmd.Parameters.AddWithValue("p_Rep_Desc", DBNull.Value);

                if (!row.IsRep_TypeNull()) cmd.Parameters.AddWithValue("p_Rep_Type", row.Rep_Type);
                else cmd.Parameters.AddWithValue("p_Rep_Type", DBNull.Value);

                if (!row.IsRep_ParentNull()) cmd.Parameters.AddWithValue("p_Rep_Parent", row.Rep_Parent);
                else cmd.Parameters.AddWithValue("p_Rep_Parent", DBNull.Value);

                if (!row.IsCode_LevelNull()) cmd.Parameters.AddWithValue("p_Code_Level", row.Code_Level);
                else cmd.Parameters.AddWithValue("p_Code_Level", DBNull.Value);

                if (!row.IsRE_FlagNull()) cmd.Parameters.AddWithValue("p_RE_Flag", row.RE_Flag);
                else cmd.Parameters.AddWithValue("p_RE_Flag", DBNull.Value);

                if (!row.IsSign_FlagNull()) cmd.Parameters.AddWithValue("p_Sign_Flag", row.Sign_Flag);
                else cmd.Parameters.AddWithValue("p_Sign_Flag", DBNull.Value);

                if (!row.IsSum_LabelNull()) cmd.Parameters.AddWithValue("p_Sum_Label", row.Sum_Label);
                else cmd.Parameters.AddWithValue("p_Sum_Label", DBNull.Value);

                if (!row.IsUpdated_ByNull()) cmd.Parameters.AddWithValue("p_Updated_By", row.Updated_By);
                else cmd.Parameters.AddWithValue("p_Updated_By", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_REP_SPEC_NODE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteAcc_ReportSpecification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_REP_SPEC_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            accDS.Merge(inputDS.Tables[accDS.ReportSpecification.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AccountingDS.ReportSpecificationRow row in accDS.ReportSpecification.Rows)
            {
                if (!row.IsSpec_IDNull()) cmd.Parameters.AddWithValue("p_Spec_ID", row.Spec_ID);
                else cmd.Parameters.AddWithValue("p_Spec_ID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_REP_SPEC_NODE_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getCOA_Details(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS StringDS = new DataStringDS();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            accDS.Merge(inputDS.Tables[accDS.ChartOfAccounting.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAcc_COADetails");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            AccountingDS.ChartOfAccountingRow row = accDS.ChartOfAccounting[0];
            cmd.Parameters["p_AccountCode"].Value = row.AccountCode;
            cmd.Parameters["p_CompanyCode"].Value = row.CompanyCode;
            cmd.Parameters["p_DepartmentCode"].Value = row.DepartmentCode;
            cmd.Parameters["p_LocationCode"].Value = row.LocationCode;
            cmd.Parameters["p_ProjectCode"].Value = row.ProjectCode;
            cmd.Parameters["p_CustomerCode"].Value = row.CustomerCode;
            cmd.Parameters["p_SupplierNumber"].Value = row.SupplierNumber;
            cmd.Parameters["p_EmployeeCode"].Value = row.EmployeeCode;
            cmd.Parameters["p_ReferenceCode"].Value = row.ReferenceCode;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.ChartOfAccounting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COA_DETAILS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(accDS.ChartOfAccounting);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getCOA_Parties(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd;
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "CUS") cmd = DBCommandProvider.GetDBCommand("GetCOAParties_Customer");
            else if (stringDS.DataStrings[0].StringValue == "SUP") cmd = DBCommandProvider.GetDBCommand("GetCOAParties_Supplier");
            else if (stringDS.DataStrings[0].StringValue == "STA") cmd = DBCommandProvider.GetDBCommand("GetCOAParties_Staff");
            else if (stringDS.DataStrings[0].StringValue == "CA") cmd = DBCommandProvider.GetDBCommand("GetCOAParties_Cash");
            else if (stringDS.DataStrings[0].StringValue == "BA") cmd = DBCommandProvider.GetDBCommand("GetCOAParties_Bank");
            else cmd = DBCommandProvider.GetDBCommand("GetCOAParties_General");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["p_PartyName"].Value = "%" + stringDS.DataStrings[1].StringValue.ToLower() + "%";
            cmd.Parameters["p_PartyCode1"].Value = (stringDS.DataStrings[2].StringValue != "") ? stringDS.DataStrings[2].StringValue : "0";
            cmd.Parameters["p_PartyCode2"].Value = (stringDS.DataStrings[2].StringValue != "") ? stringDS.DataStrings[2].StringValue : "0";

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.Acc_Party.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COA_PARTIES.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(accDS.Acc_Party);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getChartOfAccountList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();

            accDS.Merge(inputDS.Tables[accDS.ChartOfAccounting.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            AccountingDS.ChartOfAccountingRow row = accDS.ChartOfAccounting[0];
            cmd = DBCommandProvider.GetDBCommand("GetChartOfAccountList");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["Coa_Head"].Value = row.Coa_Head.ToLower();
            cmd.Parameters["Coa_Code"].Value = row.Coa_Code.ToLower();
            cmd.Parameters["Coa_Category_Id1"].Value = cmd.Parameters["Coa_Category_Id2"].Value = row.Coa_Category_Id;
            cmd.Parameters["CostCenterCode1"].Value = cmd.Parameters["CostCenterCode2"].Value = row.CostCenterCode;
            cmd.Parameters["CompanyCode1"].Value = cmd.Parameters["CompanyCode2"].Value = row.CompanyCode;
            cmd.Parameters["DepartmentCode1"].Value = cmd.Parameters["DepartmentCode2"].Value = row.DepartmentCode;
            cmd.Parameters["LocationCode1"].Value = cmd.Parameters["LocationCode2"].Value = row.LocationCode;
            cmd.Parameters["ProjectCode1"].Value = cmd.Parameters["ProjectCode2"].Value = row.ProjectCode;
            cmd.Parameters["CustomerCode1"].Value = cmd.Parameters["CustomerCode2"].Value = row.CustomerCode;
            cmd.Parameters["SupplierNumber1"].Value = cmd.Parameters["SupplierNumber2"].Value = row.SupplierNumber;
            cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = row.EmployeeCode;
            cmd.Parameters["Coa_GroupCode1"].Value = cmd.Parameters["Coa_GroupCode2"].Value = row.Coa_Group;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.ChartOfAccounting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COA_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            returnDS.Merge(accDS.ChartOfAccounting);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        #region Voucher
        private DataSet _getRecieverInfo(DBConnectionDS connDS, int priority, int pathID)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            OleDbCommand cmd_RcvInfo = new OleDbCommand();
            bool bError_Info = false;
            int nRowAffected_Info = -1;
            OleDbDataAdapter adapter_Info = new OleDbDataAdapter();

            cmd_RcvInfo = DBCommandProvider.GetDBCommand("GetAcc_NextApproverList");
            if (cmd_RcvInfo == null) return UtilDL.GetCommandNotFound();
            adapter_Info.SelectCommand = cmd_RcvInfo;
            cmd_RcvInfo.Parameters["Priority"].Value = priority;
            cmd_RcvInfo.Parameters["PathID"].Value = pathID;

            nRowAffected_Info = ADOController.Instance.Fill(adapter_Info, accDS, accDS.Acc_ApprovalRoute.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Info);
            accDS.AcceptChanges();
            if (bError_Info)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                //return errDS;
            }
            accDS.AcceptChanges();

            return accDS;
        }
        private bool _IsApprover(int pathID, decimal voucherTotal, decimal userApprovalValue, int priority, DBConnectionDS connDS)
        {
            try
            {
                AccountingDS pathDS_All = new AccountingDS();
                AccountingDS pathDS_filtered = new AccountingDS();

                //DataSet route_All = _getApprovalRouteList(connDS);
                DataSet route_All = _getRecieverInfo(connDS, priority, pathID);
                pathDS_All.Merge(route_All.Tables[pathDS_All.Acc_ApprovalRoute.TableName], false, MissingSchemaAction.Error);
                pathDS_All.AcceptChanges();
                string sExpr = "PathID =" + pathID;

                DataRow[] foundRows = pathDS_All.Acc_ApprovalRoute.Select(sExpr);

                foreach (DataRow row in foundRows)
                {
                    pathDS_filtered.Acc_ApprovalRoute.ImportRow(row);
                }
                pathDS_filtered.AcceptChanges();

                sExpr = "ApprovalValue < " + voucherTotal;    // Get the Approvers below this range
                foundRows = pathDS_filtered.Acc_ApprovalRoute.Select(sExpr);
                int totalFoundRows = foundRows.Length;

                if (pathDS_filtered.Acc_ApprovalRoute[totalFoundRows - 1].ApprovalValue == userApprovalValue) return true;
                else return false;
            }
            catch (Exception exp)
            {
                return false;
            }
        }
        private bool _IsFinalApprover(string operand, decimal approvalLimit, decimal voucherTotal, DBConnectionDS connDS, int pathID, int priority)
        {
            if (operand == "<")
            {
                if (voucherTotal < approvalLimit) return true;
                else return false;
            }
            else if (operand == "<=")
            {
                if (voucherTotal <= approvalLimit) return true;
                else return false;
            }
            else if (operand == ">=")
            {
                if (voucherTotal >= approvalLimit) return true;
                else return false;
            }
            else if (operand == ">")
            {
                bool isApprover = _IsApprover(pathID, voucherTotal, approvalLimit, priority, connDS);

                if (isApprover) return true;
                else return false;
            }

            return false;
        }
        private DataSet _approveVoucherAutomatically(DBConnectionDS connDS,
                                                     AccountingDS.Acc_TransactionRow transactionRow, 
                                                     AccountingDS.Acc_ApprovalRouteDataTable route)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            AccountingDS.Acc_ApprovalRouteRow appRow;
            bool bError = false;
            int nRowAffected = -1;
            //string returnMessage = "", Url = "", messageBody = "";
            #endregion

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            string messageBody = "";
            string returnMessage = "Email successfully sent."; // Default value
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_TRANSACTION_HIST_ADD";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1) UtilDL.GetDBOperationFailed();

            #region Check if this approver is the Final Approver [Increase current priority]
            bool isFinalApprover = _IsFinalApprover(route[0].Operand,
                                                    route[0].ApprovalValue,
                                                    transactionRow.Txn_Total_Amt,
                                                    connDS,
                                                    transactionRow.PathID,
                                                    (transactionRow.Txnh_Priority));

            if (!isFinalApprover) // Get Next user of Requisitioner
            {
                transactionRow.Txnh_Priority = transactionRow.Txnh_Priority + 1;
                transactionRow.SubjectForEmail = transactionRow.SubjectForEmail.Replace("Created", "Verified");
                appRow = route[1];

                if (!appRow.IsApprovalUserDelegateIDNull())
                {
                    transactionRow.Txnh_Received_User_ID = appRow.ApprovalUserDelegateID;
                    transactionRow.Txnh_Received_UserName = appRow.ApprovalUserDelegateName;
                    transactionRow.ToEmailAddress = (!appRow.IsApprovalUserDelegateMailNull()) ? appRow.ApprovalUserDelegateMail : "";
                }
                else
                {
                    transactionRow.Txnh_Received_User_ID = appRow.ApprovalUserID;
                    transactionRow.Txnh_Received_UserName = appRow.ApprovalUserName;
                    transactionRow.ToEmailAddress = (!appRow.IsApprovalUserMailNull()) ? appRow.ApprovalUserMail : "";
                }
            }
            else        // Get the Voucher Creator
            {
                transactionRow.SubjectForEmail = transactionRow.SubjectForEmail.Replace("Created", "Approved");
                transactionRow.Txnh_Received_User_ID = transactionRow.Create_User;
                transactionRow.Txnh_Received_UserName = transactionRow.Create_UserName;
                transactionRow.ToEmailAddress = (!transactionRow.IsCreate_UserMailNull()) ? transactionRow.Create_UserMail : "";
                transactionRow.Txn_Approval_Status = "A";  // This time voucher will be Approved.
                transactionRow.Txn_VoucherStatus = "Approved";
            }
            #endregion

            #region Bind param Values
            cmd.Parameters.AddWithValue("p_Txnh_Id", (object)genPK);
            cmd.Parameters.AddWithValue("p_Txnh_Txn_Id", transactionRow.Txn_ID);

            if (!transactionRow.IsTxnh_Sender_User_IDNull()) cmd.Parameters.AddWithValue("p_Txnh_Sender_User_Id", transactionRow.Txnh_Sender_User_ID);
            else cmd.Parameters.AddWithValue("p_Txnh_Sender_User_Id", DBNull.Value);

            // This value is set above.
            if (!transactionRow.IsTxnh_Received_User_IDNull()) cmd.Parameters.AddWithValue("p_Txnh_Received_User_Id", transactionRow.Txnh_Received_User_ID);
            else cmd.Parameters.AddWithValue("p_Txnh_Received_User_Id", DBNull.Value);

            if (!transactionRow.IsTxnh_ActivityCodeNull()) cmd.Parameters.AddWithValue("p_Txnh_ActivityCode", transactionRow.Txnh_ActivityCode);
            else cmd.Parameters.AddWithValue("p_Txnh_ActivityCode", DBNull.Value);

            if (!transactionRow.IsTxnh_History_CommentsNull()) cmd.Parameters.AddWithValue("p_Txnh_History_Comments", transactionRow.Txnh_History_Comments);
            else cmd.Parameters.AddWithValue("p_Txnh_History_Comments", DBNull.Value);

            if (!transactionRow.IsTxnh_PriorityNull()) cmd.Parameters.AddWithValue("p_Txnh_Priority", transactionRow.Txnh_Priority);
            else cmd.Parameters.AddWithValue("p_Txnh_Priority", DBNull.Value);

            if (!transactionRow.IsTxn_Approval_StatusNull()) cmd.Parameters.AddWithValue("p_Txn_Approval_Status", transactionRow.Txn_Approval_Status);
            else cmd.Parameters.AddWithValue("p_Txn_Approval_Status", DBNull.Value);
            #endregion

            if (transactionRow.IsNotificationByMail)
            {
                #region Send MAIL
                returnMessage = "";

                messageBody = "<b>Voucher Number: </b>" + transactionRow.Txn_No + "<br><br>";
                messageBody += "<b>Applicant: </b>" + transactionRow.Create_UserName + "<br>";
                messageBody += "<b>Dispatcher: </b>" + transactionRow.Txnh_Sender_UserName;
                messageBody += "<br><br><br>";
                messageBody += "For details, click the following link: ";
                messageBody += "<br>";
                messageBody += transactionRow.Approval_URL;
                messageBody += "<br>";

                string FromEmailAddress = credentialEmailAddress;
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, FromEmailAddress,
                                                   transactionRow.ToEmailAddress, transactionRow.CcEmailAddress, transactionRow.SubjectForEmail,
                                                   messageBody, "COA - " + transactionRow.Txnh_Sender_UserName);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, FromEmailAddress, transactionRow.ToEmailAddress, transactionRow.CcEmailAddress,
                                                   transactionRow.SubjectForEmail, messageBody, "COA - " + transactionRow.Txnh_Sender_UserName);
                }
                #endregion
            }

            if (returnMessage == "Email successfully sent.")
            {
                #region Save Transaction
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion
            }
            else
            {
                messageDS.ErrorMsg.AddErrorMsgRow("Mail sending failed to : " + transactionRow.Txnh_Received_UserName);
                messageDS.AcceptChanges();
                returnDS.Merge(messageDS);
                return returnDS;
            }

            if (nRowAffected > 0)
            {
                messageDS.SuccessMsg.AddSuccessMsgRow(String.Format("{0} [{1}] - [{2}]", transactionRow.Txn_No, transactionRow.Txn_VoucherStatus,
                                                                                       transactionRow.Txnh_Received_UserName));
            }
            else
            {
                messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} [{1}] - [{2}]", transactionRow.Txn_No, transactionRow.Txn_VoucherStatus,
                                                                                   transactionRow.Txnh_Received_UserName));
            }
            messageDS.AcceptChanges();
            
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _doesCoaReferenceExists(DBConnectionDS connDS, AccountingDS.Acc_Transaction_DetailDataTable dt)
        {
            DataSet returnDS = new DataSet();
            DataBoolDS boolDS = new DataBoolDS();
            MessageDS messageDS = new MessageDS();
            ErrorDS errDS = new ErrorDS();
            bool doesRefExists = false, bError = false;
            int nRowAffected = -1;

            OleDbCommand cmd_Ref = new OleDbCommand();
            string errorMessage = "";
            foreach (AccountingDS.Acc_Transaction_DetailRow refRow in dt.Rows)
            {
                if (!refRow.IsTxnd_Ref_NoNull())
                {
                    cmd_Ref = DBCommandProvider.GetDBCommand("DoesExists_Coa_Ref");
                    cmd_Ref.Parameters["Coa_ID"].Value = refRow.Txnd_COA_ID;
                    cmd_Ref.Parameters["Ref_No"].Value = refRow.Txnd_Ref_No;

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd_Ref, connDS.DBConnections[0].ConnectionID, ref bError));

                    if (bError == true)
                    {
                        errDS.Clear();
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                    if(nRowAffected == 0) doesRefExists = false;
                    else
                    {
                        doesRefExists = true;
                        errorMessage = String.Format("Reference Exists : [{0}]", refRow.Txnd_Account_Name);
                        if (messageDS.ErrorMsg.Rows.Count == 0)
                        { messageDS.ErrorMsg.AddErrorMsgRow(errorMessage); }
                        else
                        { messageDS.ErrorMsg[0].StringValue += "</br> " + errorMessage; }
                    }
                }
            }
            messageDS.AcceptChanges();

            boolDS.DataBools.AddDataBool(doesRefExists);

            returnDS.Merge(boolDS.DataBools);
            returnDS.Merge(messageDS.ErrorMsg);
            returnDS.Merge(errDS.Errors);
            return returnDS;
        }
        private DataSet _saveGeneralVoucher(DataSet inputDS)
        {
            #region Local Declaration
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            AccountingDS accDS = new AccountingDS();
            AccountingDS accDS_Rcv = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            OleDbCommand cmd;
            OleDbCommand cmd_RcvInfo;
            OleDbDataAdapter adapter_Info = new OleDbDataAdapter();
            bool bError;
            int nRowAffected;
            string transactionNumber;
            #endregion

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            string messageBody = "", approval_URL = "";
            string returnMessage = "Email successfully sent."; // Default value
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            string FromEmailAddress = credentialEmailAddress;
            #endregion

            accDS.Merge(inputDS.Tables[accDS.Acc_Transaction.TableName], false, MissingSchemaAction.Error);
            accDS.Merge(inputDS.Tables[accDS.Acc_Transaction_Detail.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            AccountingDS.Acc_TransactionRow transactionRow = accDS.Acc_Transaction[0];
            long transactionID = IDGenerator.GetNextGenericPK();
            transactionRow.Txn_ID = (Int32)transactionID;

            #region Check UNIQUENESS of Reference No
            DataSet refExistDS = _doesCoaReferenceExists(connDS, accDS.Acc_Transaction_Detail);
            DataBoolDS boolDS = new DataBoolDS();
            boolDS.Merge(refExistDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            messageDS.Merge(refExistDS.Tables[messageDS.ErrorMsg.TableName], false, MissingSchemaAction.Error);
            errDS.Merge(refExistDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);

            bool doesRefExists = boolDS.DataBools[0].BoolValue;
            if (doesRefExists)
            {
                returnDS.Merge(messageDS);
                returnDS.Merge(errDS);
                return returnDS;
            }
            #endregion

            #region Get ReceiverInfo : For automatic Approval [Not for Memo vouchers]
            if (transactionRow.Txn_CategoryCode != "MMV" && transactionRow.Txn_CategoryCode != "PMMV") // Not for Memo vouchers
            {
                DataSet recieverDS = _getRecieverInfo(connDS, transactionRow.Txnh_Priority, transactionRow.PathID);
                accDS_Rcv.Merge(recieverDS.Tables[accDS_Rcv.Acc_ApprovalRoute.TableName], false, MissingSchemaAction.Error);
                accDS_Rcv.AcceptChanges();

                if (accDS_Rcv.Acc_ApprovalRoute.Rows.Count == 0)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = "Next approver not found !";
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                #region Rejected : Now receiver is set from UI
                //if (!accDS_Rcv.Acc_ApprovalRoute[0].IsApprovalUserDelegateIDNull())
                //{
                //    transactionRow.Txnh_Received_User_ID = accDS_Rcv.Acc_ApprovalRoute[0].ApprovalUserDelegateID;
                //    transactionRow.Txnh_Received_UserName = accDS_Rcv.Acc_ApprovalRoute[0].ApprovalUserDelegateName;
                //}
                //else
                //{
                //    transactionRow.Txnh_Received_User_ID = accDS_Rcv.Acc_ApprovalRoute[0].ApprovalUserID;
                //    transactionRow.Txnh_Received_UserName = accDS_Rcv.Acc_ApprovalRoute[0].ApprovalUserName;
                //}
                //if (!accDS_Rcv.Acc_ApprovalRoute[0].IsApprovalUserDelegateMailNull()) transactionRow.ToEmailAddress = accDS_Rcv.Acc_ApprovalRoute[0].ApprovalUserDelegateMail;
                //else transactionRow.ToEmailAddress = accDS_Rcv.Acc_ApprovalRoute[0].ApprovalUserMail;
                #endregion
            }
            #endregion

            #region Parameter Binding for Transaction
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_TRANSACTION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (transactionID == -1) UtilDL.GetDBOperationFailed();
            else cmd.Parameters.AddWithValue("p_Txn_ID", (object)transactionID);

            if (!transactionRow.IsTxn_DateNull()) cmd.Parameters.AddWithValue("p_Txn_Date", transactionRow.Txn_Date);
            else cmd.Parameters.AddWithValue("p_Txn_Date", DBNull.Value);

            if (!transactionRow.IsTxn_CategoryCodeNull()) cmd.Parameters.AddWithValue("p_Txn_CategoryCode", transactionRow.Txn_CategoryCode);
            else cmd.Parameters.AddWithValue("p_Txn_CategoryCode", DBNull.Value);

            if (!transactionRow.IsTxn_Total_AmtNull()) cmd.Parameters.AddWithValue("p_Txn_Total_Amt", transactionRow.Txn_Total_Amt);
            else cmd.Parameters.AddWithValue("p_Txn_Total_Amt", DBNull.Value);

            if (!transactionRow.IsTxn_Posted_FlagNull()) cmd.Parameters.AddWithValue("p_Txn_Posted_Flag", transactionRow.Txn_Posted_Flag);
            else cmd.Parameters.AddWithValue("p_Txn_Posted_Flag", DBNull.Value);

            if (!transactionRow.IsCreate_UserNull()) cmd.Parameters.AddWithValue("p_Create_User", transactionRow.Create_User);
            else cmd.Parameters.AddWithValue("p_Create_User", DBNull.Value);

            if (!transactionRow.IsTxn_DeliveryNoteNoNull()) cmd.Parameters.AddWithValue("p_Txn_DeliveryNoteNo", transactionRow.Txn_DeliveryNoteNo);
            else cmd.Parameters.AddWithValue("p_Txn_DeliveryNoteNo", DBNull.Value);

            if (!transactionRow.IsTxn_DispatchDocNoNull()) cmd.Parameters.AddWithValue("p_Txn_DispatchDocNo", transactionRow.Txn_DispatchDocNo);
            else cmd.Parameters.AddWithValue("p_Txn_DispatchDocNo", DBNull.Value);

            if (!transactionRow.IsTxn_DispatchedThroughNull()) cmd.Parameters.AddWithValue("p_Txn_DispatchedThrough", transactionRow.Txn_DispatchedThrough);
            else cmd.Parameters.AddWithValue("p_Txn_DispatchedThrough", DBNull.Value);

            if (!transactionRow.IsTxn_DestinationNull()) cmd.Parameters.AddWithValue("p_Txn_Destination", transactionRow.Txn_Destination);
            else cmd.Parameters.AddWithValue("p_Txn_Destination", DBNull.Value);

            if (!transactionRow.IsTxn_OrderNoNull()) cmd.Parameters.AddWithValue("p_Txn_OrderNo", transactionRow.Txn_OrderNo);
            else cmd.Parameters.AddWithValue("p_Txn_OrderNo", DBNull.Value);

            if (!transactionRow.IsTxn_PaymentTermNull()) cmd.Parameters.AddWithValue("p_Txn_PaymentTerm", transactionRow.Txn_PaymentTerm);
            else cmd.Parameters.AddWithValue("p_Txn_PaymentTerm", DBNull.Value);

            if (!transactionRow.IsTxn_OtherRefNull()) cmd.Parameters.AddWithValue("p_Txn_OtherRef", transactionRow.Txn_OtherRef);
            else cmd.Parameters.AddWithValue("p_Txn_OtherRef", DBNull.Value);

            if (!transactionRow.IsTxn_DeliveryTermNull()) cmd.Parameters.AddWithValue("p_Txn_DeliveryTerm", transactionRow.Txn_DeliveryTerm);
            else cmd.Parameters.AddWithValue("p_Txn_DeliveryTerm", DBNull.Value);

            if (!transactionRow.IsTxn_Ref_IDNull()) cmd.Parameters.AddWithValue("p_Txn_Ref_ID", transactionRow.Txn_Ref_ID);
            else cmd.Parameters.AddWithValue("p_Txn_Ref_ID", DBNull.Value);

            cmd.Parameters.Add("p_Txn_NO", OleDbType.VarChar, 1000).Direction = ParameterDirection.Output;
            #endregion

            if (transactionRow.IsNotificationByMail)
            {
                #region Send MAIL
                returnMessage = "";

                messageBody = "<b>Voucher Creator: </b>" + transactionRow.Create_UserName + "<br><br>";
                messageBody += "<br><br><br>";
                messageBody += "For details, click the following link: ";
                messageBody += "<br>";
                messageBody += transactionRow.Approval_URL;
                messageBody += "<br>";

                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, FromEmailAddress, 
                                                   transactionRow.ToEmailAddress, transactionRow.CcEmailAddress, transactionRow.SubjectForEmail, 
                                                   messageBody, "COA - " + transactionRow.Create_UserName);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, FromEmailAddress, transactionRow.ToEmailAddress, transactionRow.CcEmailAddress,
                                                   transactionRow.SubjectForEmail, messageBody, "COA - " + transactionRow.Create_UserName);
                }
                #endregion
            }

            if (returnMessage == "Email successfully sent.")
            {
                #region Save Transaction
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                transactionNumber = cmd.Parameters["p_Txn_NO"].Value.ToString();
                transactionRow.Txn_No = transactionNumber;
                accDS.AcceptChanges();
                messageDS.SuccessMsg.AddSuccessMsgRow(String.Format("{0} [{1}] - [{2}]", transactionNumber, transactionRow.Txn_VoucherStatus,
                                                                                       transactionRow.Txnh_Received_UserName));
                cmd.Parameters.Clear();
                #endregion
            }
            else
            {
                messageDS.ErrorMsg.AddErrorMsgRow("Mail sending failed to : " + transactionRow.Txnh_Received_UserName);
                messageDS.AcceptChanges();
                returnDS.Merge(messageDS);
                returnDS.Merge(errDS);
                return returnDS;
            }

            #region Save Txn Details
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_TRANSACTION_DTL_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (AccountingDS.Acc_Transaction_DetailRow row in accDS.Acc_Transaction_Detail.Rows)
            {
                cmd.Parameters.AddWithValue("p_Txnd_Txn_Id", transactionID);

                if (!row.IsTxnd_COA_IDNull()) cmd.Parameters.AddWithValue("p_Txnd_Coa_Id", row.Txnd_COA_ID);
                else cmd.Parameters.AddWithValue("p_Txnd_Coa_Id", DBNull.Value);

                if (!row.IsTxnd_Account_NameNull()) cmd.Parameters.AddWithValue("p_Txnd_Account_Name", row.Txnd_Account_Name);
                else cmd.Parameters.AddWithValue("p_Txnd_Account_Name", DBNull.Value);

                if (!row.IsTxnd_DescriptionNull()) cmd.Parameters.AddWithValue("p_Txnd_Description", row.Txnd_Description);
                else cmd.Parameters.AddWithValue("p_Txnd_Description", DBNull.Value);

                if (!row.IsTxnd_AmtNull()) cmd.Parameters.AddWithValue("p_Txnd_Amt", row.Txnd_Amt);
                else cmd.Parameters.AddWithValue("p_Txnd_Amt", DBNull.Value);

                if (!row.IsTxnd_NatureNull()) cmd.Parameters.AddWithValue("p_Txnd_Nature", row.Txnd_Nature);
                else cmd.Parameters.AddWithValue("p_Txnd_Nature", DBNull.Value);

                if (!row.IsTxnd_Ref_NoNull()) cmd.Parameters.AddWithValue("p_Txnd_Ref_No", row.Txnd_Ref_No);
                else cmd.Parameters.AddWithValue("p_Txnd_Ref_No", DBNull.Value);

                if (!row.IsTxnd_Ref_DateNull()) cmd.Parameters.AddWithValue("p_Txnd_Ref_Date", row.Txnd_Ref_Date);
                else cmd.Parameters.AddWithValue("p_Txnd_Ref_Date", DBNull.Value);

                if (!row.IsTxnd_Ref_Type_IDNull()) cmd.Parameters.AddWithValue("p_Txnd_Ref_Type_Id", row.Txnd_Ref_Type_ID);
                else cmd.Parameters.AddWithValue("p_Txnd_Ref_Type_Id", DBNull.Value);

                if (!row.IsTxnd_TracerNull()) cmd.Parameters.AddWithValue("p_Tracer", row.Txnd_Tracer);
                else cmd.Parameters.AddWithValue("p_Txnd_Tracer", DBNull.Value);

                if (!row.IsTxnd_HeaderNull()) cmd.Parameters.AddWithValue("p_Txnd_Header", row.Txnd_Header);
                else cmd.Parameters.AddWithValue("p_Txnd_Header", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Save Txn History
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_TRANSACTION_HIST_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            long genPK = IDGenerator.GetNextGenericPK();

            if (genPK == -1) UtilDL.GetDBOperationFailed();
            else cmd.Parameters.AddWithValue("p_Txnh_Id", (object)genPK);

            cmd.Parameters.AddWithValue("p_Txnh_Txn_Id", transactionID);

            if (!transactionRow.IsTxnh_Sender_User_IDNull()) cmd.Parameters.AddWithValue("p_Txnh_Sender_User_Id", transactionRow.Txnh_Sender_User_ID);
            else cmd.Parameters.AddWithValue("p_Txnh_Sender_User_Id", DBNull.Value);

            // This value is set above.
            if (!transactionRow.IsTxnh_Received_User_IDNull()) cmd.Parameters.AddWithValue("p_Txnh_Received_User_Id", transactionRow.Txnh_Received_User_ID);
            else cmd.Parameters.AddWithValue("p_Txnh_Received_User_Id", DBNull.Value);

            if (!transactionRow.IsTxnh_ActivityCodeNull()) cmd.Parameters.AddWithValue("p_Txnh_ActivityCode", transactionRow.Txnh_ActivityCode);
            else cmd.Parameters.AddWithValue("p_Txnh_ActivityCode", DBNull.Value);

            if (!transactionRow.IsTxnh_History_CommentsNull()) cmd.Parameters.AddWithValue("p_Txnh_History_Comments", transactionRow.Txnh_History_Comments);
            else cmd.Parameters.AddWithValue("p_Txnh_History_Comments", DBNull.Value);

            if (!transactionRow.IsTxnh_PriorityNull()) cmd.Parameters.AddWithValue("p_Txnh_Priority", transactionRow.Txnh_Priority);
            else cmd.Parameters.AddWithValue("p_Txnh_Priority", DBNull.Value);

            if (!transactionRow.IsTxn_Approval_StatusNull()) cmd.Parameters.AddWithValue("p_Txn_Approval_Status", transactionRow.Txn_Approval_Status);
            else cmd.Parameters.AddWithValue("p_Txn_Approval_Status", DBNull.Value);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Check if SENDER and RECEIVER are same user
            if (transactionRow.Txn_CategoryCode != "MMV" && transactionRow.Txn_CategoryCode != "PMMV") // Not for Memo vouchers
            {
                if (transactionRow.Txnh_Sender_User_ID == transactionRow.Txnh_Received_User_ID)
                {
                    // Approve for this user automatically
                    DataSet responseDS = _approveVoucherAutomatically(connDS, transactionRow, accDS_Rcv.Acc_ApprovalRoute);

                    messageDS = new MessageDS();
                    try
                    {
                        messageDS.Merge(responseDS.Tables[messageDS.SuccessMsg.TableName], false, MissingSchemaAction.Error);
                        messageDS.Merge(responseDS.Tables[messageDS.ErrorMsg.TableName], false, MissingSchemaAction.Error);
                    }
                    catch (Exception exp) { }
                    try
                    { errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error); }
                    catch (Exception exp) { }

                    messageDS.AcceptChanges();
                    errDS.AcceptChanges();

                    if (messageDS.ErrorMsg.Rows.Count > 0)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = "Automatic approval failed ! " + messageDS.ErrorMsg[0].StringValue;
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                }
            }
            #endregion

            #region [For Template Txn] Update Template NextDueDate
            if (!transactionRow.IsTemplate_IDNull()       // This means this txn is submitted from Template
                && messageDS.ErrorMsg.Rows.Count == 0)    // No error occured till now
            {
                cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ACC_TEMPLATE_UPD_DATE";
                cmd.CommandType = CommandType.StoredProcedure;

                if (!transactionRow.IsTemplate_IDNull()) cmd.Parameters.AddWithValue("p_Template_ID", transactionRow.Template_ID);
                else cmd.Parameters.AddWithValue("p_Template_ID", DBNull.Value);
                if (!transactionRow.IsTmp_Last_Process_DateNull()) cmd.Parameters.AddWithValue("p_Last_Process_Date", transactionRow.Tmp_Last_Process_Date);
                else cmd.Parameters.AddWithValue("p_Last_Process_Date", DBNull.Value);
                if (!transactionRow.IsTmp_Next_Due_DateNull()) cmd.Parameters.AddWithValue("p_Next_Due_Date", transactionRow.Tmp_Next_Due_Date);
                else cmd.Parameters.AddWithValue("p_Next_Due_Date", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            //errDS.Clear();
            //errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAcc_GeneralVoucher_ByFilter(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[5].StringValue != "0")
            {
                cmd = DBCommandProvider.GetDBCommand("GetAcc_GeneralVoucher_ByHeader");
                cmd.Parameters["Txnd_Coa_Id"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            }
            else
            { cmd = DBCommandProvider.GetDBCommand("GetAcc_GeneralVoucher_ByFilter"); }
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            cmd.Parameters["VoucherNumber"].Value = ("%" + stringDS.DataStrings[2].StringValue + "%");
            cmd.Parameters["ReceivedUserID"].Value = cmd.Parameters["SenderUserID"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
            cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["Txn_Category1"].Value = cmd.Parameters["Txn_Category2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["Txn_App_Status1"].Value = cmd.Parameters["Txn_App_Status2"].Value = stringDS.DataStrings[6].StringValue;

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.Acc_Transaction_Detail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_ACC_GET_GENERAL_VOUCHER_BY_FILTER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            returnDS.Merge(accDS.Acc_Transaction_Detail);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _approveAcc_GeneralVoucher(DataSet inputDS)
        {
            #region General Declaration
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            AccountingDS accDS_Rcv;
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();
            OleDbCommand cmd, cmd_RcvInfo;
            OleDbDataAdapter adapter_Info = new OleDbDataAdapter();
            bool bError = true;
            int nRowAffected = -1;
            #endregion

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            string messageBody = "", approval_URL = "";
            string returnMessage = "Email successfully sent."; // Default value
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            string FromEmailAddress = credentialEmailAddress;
            #endregion

            accDS.Merge(inputDS.Tables[accDS.Acc_Transaction.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AccountingDS.Acc_TransactionRow transactionRow in accDS.Acc_Transaction.Rows)
            {
                #region Parameter Binding
                cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ACC_TRANSACTION_HIST_ADD";
                cmd.CommandType = CommandType.StoredProcedure;

                long genPK = IDGenerator.GetNextGenericPK();

                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("p_Txnh_Id", (object)genPK);

                cmd.Parameters.AddWithValue("p_Txnh_Txn_Id", transactionRow.Txn_ID);

                if (!transactionRow.IsTxnh_Sender_User_IDNull()) cmd.Parameters.AddWithValue("p_Txnh_Sender_User_Id", transactionRow.Txnh_Sender_User_ID);
                else cmd.Parameters.AddWithValue("p_Txnh_Sender_User_Id", DBNull.Value);

                if (!transactionRow.IsTxnh_Received_User_IDNull()) cmd.Parameters.AddWithValue("p_Txnh_Received_User_Id", transactionRow.Txnh_Received_User_ID);
                else cmd.Parameters.AddWithValue("p_Txnh_Received_User_Id", DBNull.Value);

                if (!transactionRow.IsTxnh_ActivityCodeNull()) cmd.Parameters.AddWithValue("p_Txnh_ActivityCode", transactionRow.Txnh_ActivityCode);
                else cmd.Parameters.AddWithValue("p_Txnh_ActivityCode", DBNull.Value);

                if (!transactionRow.IsTxnh_History_CommentsNull()) cmd.Parameters.AddWithValue("p_Txnh_History_Comments", transactionRow.Txnh_History_Comments);
                else cmd.Parameters.AddWithValue("p_Txnh_History_Comments", DBNull.Value);

                if (!transactionRow.IsTxnh_PriorityNull()) cmd.Parameters.AddWithValue("p_Txnh_Priority", transactionRow.Txnh_Priority);
                else cmd.Parameters.AddWithValue("p_Txnh_Priority", DBNull.Value);

                if (!transactionRow.IsTxn_Approval_StatusNull()) cmd.Parameters.AddWithValue("p_Txn_Approval_Status", transactionRow.Txn_Approval_Status);
                else cmd.Parameters.AddWithValue("p_Txn_Approval_Status", DBNull.Value);
                #endregion

                if (transactionRow.IsNotificationByMail)
                {
                    #region Send MAIL
                    returnMessage = "";

                    messageBody = "<b>Voucher Number: </b>" + transactionRow.Txn_No + "<br><br>";
                    messageBody += "<b>Applicant: </b>" + transactionRow.Create_UserName + "<br>";
                    messageBody += "<b>Dispatcher: </b>" + transactionRow.Txnh_Sender_UserName;
                    messageBody += "<br><br><br>";
                    messageBody += "For details, click the following link: ";
                    messageBody += "<br>";
                    messageBody += transactionRow.Approval_URL;
                    messageBody += "<br>";

                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, FromEmailAddress,
                                                       transactionRow.ToEmailAddress, transactionRow.CcEmailAddress, transactionRow.SubjectForEmail,
                                                       messageBody, "COA - " + transactionRow.Txnh_Sender_UserName);
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, FromEmailAddress, transactionRow.ToEmailAddress, transactionRow.CcEmailAddress,
                                                       transactionRow.SubjectForEmail, messageBody, "COA - " + transactionRow.Txnh_Sender_UserName);
                    }
                    #endregion
                }

                if (returnMessage == "Email successfully sent.")
                {
                    #region Save History
                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ACC_GENERAL_VOUCHER_APPROVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }

                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(String.Format("{0} - [{1}]",
                                                                                transactionRow.Txn_No.ToString(),
                                                                                transactionRow.Txnh_ActivityName));
                    else messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} - [{1}]",
                                                            transactionRow.Txn_No.ToString(),
                                                            transactionRow.Txnh_ActivityName));
                    #endregion
                }
                else
                {
                    string msg = String.Format("[{0}] - Mail sending failed to : {1}", transactionRow.Txn_No, transactionRow.Txnh_Received_UserName);
                    messageDS.ErrorMsg.AddErrorMsgRow(msg);
                    messageDS.AcceptChanges();
                    returnDS.Merge(messageDS);
                    returnDS.Merge(errDS);
                    return returnDS;
                }

                #region Check if SENDER and RECEIVER are same user [for DELEGATE scenario] && Not finally Approved or Rejected
                if (transactionRow.Txnh_Sender_User_ID == transactionRow.Txnh_Received_User_ID
                    && transactionRow.Txn_Approval_Status == "P")
                {
                    transactionRow.Txnh_Sender_User_ID = transactionRow.Txnh_Received_User_ID;

                    #region Get ReceiverInfo
                    DataSet recieverDS = _getRecieverInfo(connDS, transactionRow.Txnh_Priority, transactionRow.PathID);
                    accDS_Rcv = new AccountingDS();
                    accDS_Rcv.Merge(recieverDS.Tables[accDS_Rcv.Acc_ApprovalRoute.TableName], false, MissingSchemaAction.Error);
                    accDS_Rcv.AcceptChanges();

                    if (accDS_Rcv.Acc_ApprovalRoute.Rows.Count == 0)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = "Next approver not found !";
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ACC_GENERAL_VOUCHER_APPROVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                    if (!accDS_Rcv.Acc_ApprovalRoute[0].IsApprovalUserDelegateIDNull()) transactionRow.Txnh_Received_User_ID = accDS_Rcv.Acc_ApprovalRoute[0].ApprovalUserDelegateID;
                    else transactionRow.Txnh_Received_User_ID = accDS_Rcv.Acc_ApprovalRoute[0].ApprovalUserID;
                    #endregion

                    #region Approve for this user automatically
                    DataSet responseDS = _approveVoucherAutomatically(connDS, transactionRow, accDS_Rcv.Acc_ApprovalRoute);
                    MessageDS messageDS_App = new MessageDS();

                    errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                    errDS.AcceptChanges();

                    try
                    {
                        messageDS_App.Merge(responseDS.Tables[messageDS_App.SuccessMsg.TableName], false, MissingSchemaAction.Error);
                        messageDS_App.Merge(responseDS.Tables[messageDS_App.ErrorMsg.TableName], false, MissingSchemaAction.Error);
                        messageDS_App.AcceptChanges();
                    }
                    catch (Exception exp) 
                    { }

                    if (errDS.Errors.Rows.Count > 0)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = "Automatic approval failed !";
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ACC_GENERAL_VOUCHER_APPROVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    else
                    {
                        // Replace Confirmation Msg with this new one.
                        messageDS.SuccessMsg[messageDS.SuccessMsg.Rows.Count - 1].StringValue = messageDS_App.SuccessMsg[0].StringValue;
                        if (messageDS_App.ErrorMsg.Rows.Count > 0)
                        { messageDS.ErrorMsg[messageDS.ErrorMsg.Rows.Count - 1].StringValue = messageDS_App.ErrorMsg[0].StringValue; }
                        messageDS.AcceptChanges();
                    }
                    #endregion
                }
                #endregion
            }

            errDS.AcceptChanges();
            messageDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getAcc_MemoVouchers(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAcc_MemoVouchers");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.Acc_Transaction_Detail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_ACC_GET_MEMO_VOUCHERS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            returnDS.Merge(accDS.Acc_Transaction_Detail);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteAcc_Voucher(DataSet inputDS)
        {
            #region General Declaration
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();
            bool bError = true;
            int nRowAffected = -1;
            #endregion

            accDS.Merge(inputDS.Tables[accDS.Acc_Transaction.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_VOUCHER_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (AccountingDS.Acc_TransactionRow transactionRow in accDS.Acc_Transaction.Rows)
            {
                cmd.Parameters.AddWithValue("p_Txn_ID", transactionRow.Txn_ID);
                
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(transactionRow.Txn_No);
                else messageDS.ErrorMsg.AddErrorMsgRow(transactionRow.Txn_No);
            }

            errDS.AcceptChanges();
            messageDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        private DataSet _saveAcc_ApprovalRoute(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            AccountingDS accDS = new AccountingDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            bool bError = false;
            int nRowAffected = -1;
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            accDS.Merge(inputDS.Tables[accDS.Acc_ApprovalRoute.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();

            OleDbCommand cmd_Del = new OleDbCommand();
            cmd_Del.CommandText = "PRO_ACC_APPROVAL_ROUTE_DELETE";
            cmd_Del.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Add = new OleDbCommand();
            cmd_Add.CommandText = "PRO_ACC_APPROVAL_ROUTE_CREATE";
            cmd_Add.CommandType = CommandType.StoredProcedure;

            #region Delete Old Records
            cmd_Del.Parameters.AddWithValue("p_PathCode", accDS.Acc_ApprovalRoute[0].PathCode);
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_APPROVAL_ROUTE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(accDS.Acc_ApprovalRoute[0].PathName);
            else messageDS.ErrorMsg.AddErrorMsgRow(accDS.Acc_ApprovalRoute[0].PathName);

            returnDS.Merge(messageDS);
            if (bError)
            {
                returnDS.Merge(errDS);
                return returnDS;
            }
            #endregion

            #region Add New Rows
            if (accDS.Acc_ApprovalRoute[0].IsRemoveOnly == false)
            {
                foreach (AccountingDS.Acc_ApprovalRouteRow row in accDS.Acc_ApprovalRoute.Rows)
                {
                    if (row.ApprovalRouteID == 0)
                    {
                        long genPK = IDGenerator.GetNextGenericPK();
                        if (genPK == -1) return UtilDL.GetDBOperationFailed();
                        cmd_Add.Parameters.AddWithValue("p_ApprovalRouteID", genPK);
                    }
                    else cmd_Add.Parameters.AddWithValue("p_ApprovalRouteID", row.ApprovalRouteID);

                    cmd_Add.Parameters.AddWithValue("p_PathCode", row.PathCode);
                    cmd_Add.Parameters.AddWithValue("p_Priority", row.Priority);
                    cmd_Add.Parameters.AddWithValue("p_SU_DepartmentCode", row.SuperUserDepartmentCode);
                    cmd_Add.Parameters.AddWithValue("p_SU_UserID", row.SuperUserID);

                    cmd_Add.Parameters.AddWithValue("p_Operand", row.Operand);
                    cmd_Add.Parameters.AddWithValue("p_ApprovalValue", row.ApprovalValue);
                    cmd_Add.Parameters.AddWithValue("p_App_DepartmentCode", row.ApprovalDepartmentCode);
                    cmd_Add.Parameters.AddWithValue("p_App_DesignationCode", row.ApprovalDesignationCode);
                    cmd_Add.Parameters.AddWithValue("p_App_UserID", row.ApprovalUserID);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Add, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Add.Parameters.Clear();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ACC_APPROVAL_ROUTE_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion

            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _getAcc_ApprovalRoute(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAcc_ApprovalRouteList");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.Acc_ApprovalRoute.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_APPROVAL_ROUTE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            returnDS.Merge(accDS.Acc_ApprovalRoute);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createAcc_BankInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_BANK_INFO_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Bank_Info.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (AccountingDS.Acc_Bank_InfoRow row in accDS.Acc_Bank_Info.Rows)
            {

                if (!row.IsCompanyCodeNull()) cmd.Parameters.AddWithValue("p_CompanyCode", row.CompanyCode);
                else cmd.Parameters.AddWithValue("p_CompanyCode", DBNull.Value);

                if (!row.IsBranchCodeNull()) cmd.Parameters.AddWithValue("p_BranchCode", row.BranchCode);
                else cmd.Parameters.AddWithValue("p_BranchCode", DBNull.Value);

                if (!row.IsAcc_NumberNull()) cmd.Parameters.AddWithValue("p_Acc_Number", row.Acc_Number);
                else cmd.Parameters.AddWithValue("p_Acc_Number", DBNull.Value);

                if (!row.IsAcc_Op_DateNull()) cmd.Parameters.AddWithValue("p_Acc_Op_Date", row.Acc_Op_Date);
                else cmd.Parameters.AddWithValue("p_Acc_Op_Date", DBNull.Value);

                if (!row.IsAccount_TypeNull()) cmd.Parameters.AddWithValue("p_Account_Type", row.Account_Type);
                else cmd.Parameters.AddWithValue("p_Account_Type", DBNull.Value);

                if (!row.IsSignatoryNull()) cmd.Parameters.AddWithValue("p_Signatory", row.Signatory);
                else cmd.Parameters.AddWithValue("p_Signatory", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_BANK_INFO_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateAcc_BankInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_BANK_INFO_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Bank_Info.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (AccountingDS.Acc_Bank_InfoRow row in accDS.Acc_Bank_Info.Rows)
            {
                if (!row.IsCompanyCodeNull()) cmd.Parameters.AddWithValue("p_CompanyCode", row.CompanyCode);
                else cmd.Parameters.AddWithValue("p_CompanyCode", DBNull.Value);

                if (!row.IsBranchCodeNull()) cmd.Parameters.AddWithValue("p_BranchCode", row.BranchCode);
                else cmd.Parameters.AddWithValue("p_BranchCode", DBNull.Value);

                if (!row.IsAcc_NumberNull()) cmd.Parameters.AddWithValue("p_Acc_Number", row.Acc_Number);
                else cmd.Parameters.AddWithValue("p_Acc_Number", DBNull.Value);

                if (!row.IsAcc_Op_DateNull()) cmd.Parameters.AddWithValue("p_Acc_Op_Date", row.Acc_Op_Date);
                else cmd.Parameters.AddWithValue("p_Acc_Op_Date", DBNull.Value);

                if (!row.IsAccount_TypeNull()) cmd.Parameters.AddWithValue("p_Account_Type", row.Account_Type);
                else cmd.Parameters.AddWithValue("p_Account_Type", DBNull.Value);

                if (!row.IsSignatoryNull()) cmd.Parameters.AddWithValue("p_Signatory", row.Signatory);
                else cmd.Parameters.AddWithValue("p_Signatory", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_BANK_INFO_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(accDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteAcc_BankInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_BANK_INFO_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_Bank_Info.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (AccountingDS.Acc_Bank_InfoRow row in accDS.Acc_Bank_Info.Rows)
            {
                if (!row.IsBranchCodeNull()) cmd.Parameters.AddWithValue("p_BranchCode", row.BranchCode);
                else cmd.Parameters.AddWithValue("p_BranchCode", DBNull.Value);

                if (!row.IsAcc_NumberNull()) cmd.Parameters.AddWithValue("p_Acc_Number", row.Acc_Number);
                else cmd.Parameters.AddWithValue("p_Acc_Number", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_BANK_INFO_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(String.Format(@"{0} - [{1}]", row.BranchName, row.Acc_Number));
                else messageDS.ErrorMsg.AddErrorMsgRow(String.Format(@"{0} - [{1}]", row.BranchName, row.Acc_Number));
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getAcc_BankInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS StringDS = new DataStringDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAccBankInfo");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AccountingDS AccDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, AccDS, AccDS.Acc_Bank_Info.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_BANK_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            AccDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(AccDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createAcc_VoucherTemplate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS_All = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Dtl = new OleDbCommand(); 
            bool bError = false;
            int nRowAffected = -1;

            accDS_All.Merge(inputDS.Tables[accDS_All.Acc_VoucherTemplate.TableName], false, MissingSchemaAction.Error);
            accDS_All.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_ACC_TEMPLATE_ADD";
            cmd_Dtl.CommandText = "PRO_ACC_TEMPLATE_DTL_ADD";
            cmd.CommandType = cmd_Dtl.CommandType = CommandType.StoredProcedure;

            #region Add Template Row
            AccountingDS.Acc_VoucherTemplateRow headerRow = accDS_All.Acc_VoucherTemplate[accDS_All.Acc_VoucherTemplate.Rows.Count - 1];  // HeaderRow is added at last
            long templateID = IDGenerator.GetNextGenericPK();
            if (templateID == -1) UtilDL.GetDBOperationFailed();
            headerRow.Template_ID = (Int32)templateID;

            cmd.Parameters.AddWithValue("p_Template_ID", (object)templateID);

            if (!headerRow.IsTmp_CategoryCodeNull()) cmd.Parameters.AddWithValue("p_Template_CategoryCode", headerRow.Tmp_CategoryCode);
            else cmd.Parameters.AddWithValue("p_Template_CategoryCode", DBNull.Value);
            if (!headerRow.IsTemplate_Total_AmtNull()) cmd.Parameters.AddWithValue("p_Template_Total_Amt", headerRow.Template_Total_Amt);
            else cmd.Parameters.AddWithValue("p_Template_Total_Amt", DBNull.Value);
            if (!headerRow.IsTemplate_NameNull()) cmd.Parameters.AddWithValue("p_Template_Name", headerRow.Template_Name);
            else cmd.Parameters.AddWithValue("p_Template_Name", DBNull.Value);
            if (!headerRow.IsTmp_RepeatDateNull()) cmd.Parameters.AddWithValue("p_Tmp_RepeatDate", headerRow.Tmp_RepeatDate);
            else cmd.Parameters.AddWithValue("p_Tmp_RepeatDate", DBNull.Value);
            if (!headerRow.IsTmp_RepeatMonthNull()) cmd.Parameters.AddWithValue("p_Tmp_RepeatMonth", headerRow.Tmp_RepeatMonth);
            else cmd.Parameters.AddWithValue("p_Tmp_RepeatMonth", DBNull.Value);

            if (!headerRow.IsDeliveryNoteNoNull()) cmd.Parameters.AddWithValue("p_DeliveryNoteNo", headerRow.DeliveryNoteNo);
            else cmd.Parameters.AddWithValue("p_DeliveryNoteNo", DBNull.Value);
            if (!headerRow.IsDispatchDocNoNull()) cmd.Parameters.AddWithValue("p_DispatchDocNo", headerRow.DispatchDocNo);
            else cmd.Parameters.AddWithValue("p_DispatchDocNo", DBNull.Value);
            if (!headerRow.IsDispatchedThroughNull()) cmd.Parameters.AddWithValue("p_DispatchedThrough", headerRow.DispatchedThrough);
            else cmd.Parameters.AddWithValue("p_DispatchedThrough", DBNull.Value);
            if (!headerRow.IsDestinationNull()) cmd.Parameters.AddWithValue("p_Destination", headerRow.Destination);
            else cmd.Parameters.AddWithValue("p_Destination", DBNull.Value);
            if (!headerRow.IsOrderNoNull()) cmd.Parameters.AddWithValue("p_OrderNo", headerRow.OrderNo);
            else cmd.Parameters.AddWithValue("p_OrderNo", DBNull.Value);
            if (!headerRow.IsPaymentTermNull()) cmd.Parameters.AddWithValue("p_PaymentTerm", headerRow.PaymentTerm);
            else cmd.Parameters.AddWithValue("p_PaymentTerm", DBNull.Value);
            if (!headerRow.IsOtherRefNull()) cmd.Parameters.AddWithValue("p_OtherRef", headerRow.OtherRef);
            else cmd.Parameters.AddWithValue("p_OtherRef", DBNull.Value);
            if (!headerRow.IsDeliveryTermNull()) cmd.Parameters.AddWithValue("p_DeliveryTerm", headerRow.DeliveryTerm);
            else cmd.Parameters.AddWithValue("p_DeliveryTerm", DBNull.Value);

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_TEMPLATE_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            cmd.Parameters.Clear();
            #endregion

            #region Add Template Details
            foreach (AccountingDS.Acc_VoucherTemplateRow row in accDS_All.Acc_VoucherTemplate.Rows)
            {
                cmd_Dtl.Parameters.AddWithValue("p_Template_ID", templateID);

                if (!row.IsTmpd_COA_IDNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_COA_ID", row.Tmpd_COA_ID);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_COA_ID", DBNull.Value);

                if (!row.IsTmpd_AmtNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Amt", row.Tmpd_Amt);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Amt", DBNull.Value);

                if (!row.IsTmpd_NatureNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Nature", row.Tmpd_Nature);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Nature", DBNull.Value);

                if (!row.IsTmpd_HeaderNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Header", row.Tmpd_Header);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Header", DBNull.Value);

                if (!row.IsTmpd_RemarksNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Remarks", row.Tmpd_Remarks);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Remarks", DBNull.Value);

                if (!row.IsTmpd_Account_NameNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Acc_Name", row.Tmpd_Account_Name);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Acc_Name", DBNull.Value);

                if (!row.IsTmpd_TracerNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Tracer", row.Tmpd_Tracer);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Tracer", DBNull.Value);

                bError = false; 
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Dtl, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Dtl.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_TEMPLATE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateAcc_VoucherTemplate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS_All = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd_Upd = new OleDbCommand();
            OleDbCommand cmd_Dtl_Del = new OleDbCommand();
            OleDbCommand cmd_Dtl = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;

            accDS_All.Merge(inputDS.Tables[accDS_All.Acc_VoucherTemplate.TableName], false, MissingSchemaAction.Error);
            accDS_All.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd_Upd.CommandText = "PRO_ACC_TEMPLATE_UPD";
            cmd_Dtl_Del.CommandText = "PRO_ACC_TEMPLATE_DTL_DEL";
            cmd_Dtl.CommandText = "PRO_ACC_TEMPLATE_DTL_ADD";
            cmd_Upd.CommandType = cmd_Dtl_Del.CommandType = cmd_Dtl.CommandType = CommandType.StoredProcedure;

            #region Delete Old Detail Row
            AccountingDS.Acc_VoucherTemplateRow headerRow = accDS_All.Acc_VoucherTemplate[accDS_All.Acc_VoucherTemplate.Rows.Count - 1];  // HeaderRow is added at last

            if (!headerRow.IsTemplate_IDNull()) cmd_Dtl_Del.Parameters.AddWithValue("p_Template_ID", headerRow.Template_ID);
            else cmd_Dtl_Del.Parameters.AddWithValue("p_Template_ID", DBNull.Value);

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Dtl_Del, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_TEMPLATE_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            cmd_Dtl_Del.Parameters.Clear();
            #endregion

            #region Add New Template Details
            foreach (AccountingDS.Acc_VoucherTemplateRow row in accDS_All.Acc_VoucherTemplate.Rows)
            {
                if (!row.IsTemplate_IDNull()) cmd_Dtl.Parameters.AddWithValue("p_Template_ID", row.Template_ID);
                else cmd_Dtl.Parameters.AddWithValue("p_Template_ID", DBNull.Value);

                if (!row.IsTmpd_COA_IDNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_COA_ID", row.Tmpd_COA_ID);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_COA_ID", DBNull.Value);

                if (!row.IsTmpd_AmtNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Amt", row.Tmpd_Amt);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Amt", DBNull.Value);

                if (!row.IsTmpd_NatureNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Nature", row.Tmpd_Nature);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Nature", DBNull.Value);

                if (!row.IsTmpd_HeaderNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Header", row.Tmpd_Header);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Header", DBNull.Value);

                if (!row.IsTmpd_RemarksNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Remarks", row.Tmpd_Remarks);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Remarks", DBNull.Value);

                if (!row.IsTmpd_Account_NameNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Acc_Name", row.Tmpd_Account_Name);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Acc_Name", DBNull.Value);

                if (!row.IsTmpd_TracerNull()) cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Tracer", row.Tmpd_Tracer);
                else cmd_Dtl.Parameters.AddWithValue("p_Tmpd_Tracer", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Dtl, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Dtl.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_TEMPLATE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Update Template Row
            if (!headerRow.IsTemplate_IDNull()) cmd_Upd.Parameters.AddWithValue("p_Template_ID", headerRow.Template_ID);
            else cmd_Upd.Parameters.AddWithValue("p_Template_ID", DBNull.Value);
            if (!headerRow.IsTemplate_Total_AmtNull()) cmd_Upd.Parameters.AddWithValue("p_Template_Total_Amt", headerRow.Template_Total_Amt);
            else cmd_Upd.Parameters.AddWithValue("p_Template_Total_Amt", DBNull.Value);
            if (!headerRow.IsTemplate_NameNull()) cmd_Upd.Parameters.AddWithValue("p_Template_Name", headerRow.Template_Name);
            else cmd_Upd.Parameters.AddWithValue("p_Template_Name", DBNull.Value);
            if (!headerRow.IsTmp_RepeatDateNull()) cmd_Upd.Parameters.AddWithValue("p_Tmp_RepeatDate", headerRow.Tmp_RepeatDate);
            else cmd_Upd.Parameters.AddWithValue("p_Tmp_RepeatDate", DBNull.Value);
            if (!headerRow.IsTmp_RepeatMonthNull()) cmd_Upd.Parameters.AddWithValue("p_Tmp_RepeatMonth", headerRow.Tmp_RepeatMonth);
            else cmd_Upd.Parameters.AddWithValue("p_Tmp_RepeatMonth", DBNull.Value);
            if (!headerRow.IsNext_Due_DateNull()) cmd_Upd.Parameters.AddWithValue("p_Next_Due_Date", headerRow.Next_Due_Date);
            else cmd_Upd.Parameters.AddWithValue("p_Next_Due_Date", DBNull.Value);

            if (!headerRow.IsDeliveryNoteNoNull()) cmd_Upd.Parameters.AddWithValue("p_DeliveryNoteNo", headerRow.DeliveryNoteNo);
            else cmd_Upd.Parameters.AddWithValue("p_DeliveryNoteNo", DBNull.Value);
            if (!headerRow.IsDispatchDocNoNull()) cmd_Upd.Parameters.AddWithValue("p_DispatchDocNo", headerRow.DispatchDocNo);
            else cmd_Upd.Parameters.AddWithValue("p_DispatchDocNo", DBNull.Value);
            if (!headerRow.IsDispatchedThroughNull()) cmd_Upd.Parameters.AddWithValue("p_DispatchedThrough", headerRow.DispatchedThrough);
            else cmd_Upd.Parameters.AddWithValue("p_DispatchedThrough", DBNull.Value);
            if (!headerRow.IsDestinationNull()) cmd_Upd.Parameters.AddWithValue("p_Destination", headerRow.Destination);
            else cmd_Upd.Parameters.AddWithValue("p_Destination", DBNull.Value);
            if (!headerRow.IsOrderNoNull()) cmd_Upd.Parameters.AddWithValue("p_OrderNo", headerRow.OrderNo);
            else cmd_Upd.Parameters.AddWithValue("p_OrderNo", DBNull.Value);
            if (!headerRow.IsPaymentTermNull()) cmd_Upd.Parameters.AddWithValue("p_PaymentTerm", headerRow.PaymentTerm);
            else cmd_Upd.Parameters.AddWithValue("p_PaymentTerm", DBNull.Value);
            if (!headerRow.IsOtherRefNull()) cmd_Upd.Parameters.AddWithValue("p_OtherRef", headerRow.OtherRef);
            else cmd_Upd.Parameters.AddWithValue("p_OtherRef", DBNull.Value);
            if (!headerRow.IsDeliveryTermNull()) cmd_Upd.Parameters.AddWithValue("p_DeliveryTerm", headerRow.DeliveryTerm);
            else cmd_Upd.Parameters.AddWithValue("p_DeliveryTerm", DBNull.Value);

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Upd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_TEMPLATE_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            cmd_Upd.Parameters.Clear();
            #endregion

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteAcc_VoucherTemplate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS = new AccountingDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_TEMPLATE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            accDS.Merge(inputDS.Tables[accDS.Acc_VoucherTemplate.TableName], false, MissingSchemaAction.Error);
            accDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (AccountingDS.Acc_VoucherTemplateRow row in accDS.Acc_VoucherTemplate.Rows)
            {
                if (!row.IsTemplate_IDNull()) cmd.Parameters.AddWithValue("p_Template_ID", row.Template_ID);
                else cmd.Parameters.AddWithValue("p_Template_ID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_TEMPLATE_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.Template_Name);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.Template_Name);
            }

            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getAcc_VoucherTemplates(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAcc_VoucherTemplates");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            cmd.Parameters["Due_Date1"].Value = cmd.Parameters["Due_Date2"].Value = Convert.ToDateTime(stringDS.DataStrings[3].StringValue);
            cmd.Parameters["Template_Name"].Value = ("%" + stringDS.DataStrings[0].StringValue + "%");
            cmd.Parameters["Temp_CategoryCode1"].Value = cmd.Parameters["Temp_CategoryCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["Template_ID1"].Value = cmd.Parameters["Template_ID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.Acc_VoucherTemplate.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_VOUCHER_TEMPLATES.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            returnDS.Merge(accDS.Acc_VoucherTemplate);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _processAcc_VoucherTemplate(DataSet inputDS)
        {
            #region General Declaration
            ErrorDS errDS = new ErrorDS();
            AccountingDS accDS_All = new AccountingDS();
            AccountingDS accDS_Submit;
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet requestDS = new DataSet();
            DataSet responseDS = new DataSet();
            DataSet returnDS = new DataSet();
            MessageDS messageDS_Rcv = new MessageDS();
            MessageDS messageDS_Reply = new MessageDS();
            DataRow[] foundRows;

            OleDbCommand cmd_Upd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;
            #endregion

            accDS_All.Merge(inputDS.Tables[accDS_All.Acc_Transaction.TableName], false, MissingSchemaAction.Error);
            accDS_All.Merge(inputDS.Tables[accDS_All.Acc_Transaction_Detail.TableName], false, MissingSchemaAction.Error);
            accDS_All.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd_Upd.CommandText = "PRO_ACC_TEMPLATE_UPD_DATE";
            cmd_Upd.CommandType = CommandType.StoredProcedure;

            foreach (AccountingDS.Acc_TransactionRow transactionRow in accDS_All.Acc_Transaction.Rows)
            {
                accDS_Submit = new AccountingDS();
                accDS_Submit.Acc_Transaction.ImportRow(transactionRow);
                foundRows = accDS_All.Acc_Transaction_Detail.Select("Template_ID = " + transactionRow.Template_ID);
                foreach (DataRow row in foundRows)
                { accDS_Submit.Acc_Transaction_Detail.ImportRow(row); }

                accDS_Submit.AcceptChanges();

                requestDS = new DataSet();
                requestDS.Merge(accDS_Submit);
                requestDS.Merge(connDS);
                requestDS.AcceptChanges();

                responseDS = _saveGeneralVoucher(requestDS); 
                errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                errDS.AcceptChanges();

                if (errDS.Errors.Count > 0) return UtilDL.GetDBOperationFailed();

                try
                {
                    messageDS_Rcv = new MessageDS();
                    messageDS_Rcv.Merge(responseDS.Tables[messageDS_Rcv.SuccessMsg.TableName], false, MissingSchemaAction.Error);
                    messageDS_Rcv.Merge(responseDS.Tables[messageDS_Rcv.ErrorMsg.TableName], false, MissingSchemaAction.Error);

                    if (messageDS_Rcv.SuccessMsg.Rows.Count > 0)
                    { messageDS_Reply.SuccessMsg.AddSuccessMsgRow(messageDS_Rcv.SuccessMsg[0].StringValue); }
                    if (messageDS_Rcv.ErrorMsg.Rows.Count > 0)
                    { messageDS_Reply.ErrorMsg.AddErrorMsgRow(messageDS_Rcv.ErrorMsg[0].StringValue + " [" + transactionRow.Txn_CategoryName + "]"); }
                }
                catch (Exception exp)
                { }
                messageDS_Reply.AcceptChanges();

                #region Update Template Process Dates
                if (!transactionRow.IsTemplate_IDNull()) cmd_Upd.Parameters.AddWithValue("p_Template_ID", transactionRow.Template_ID);
                else cmd_Upd.Parameters.AddWithValue("p_Template_ID", DBNull.Value);
                if (!transactionRow.IsTmp_Last_Process_DateNull()) cmd_Upd.Parameters.AddWithValue("p_Last_Process_Date", transactionRow.Tmp_Last_Process_Date);
                else cmd_Upd.Parameters.AddWithValue("p_Last_Process_Date", DBNull.Value);
                if (!transactionRow.IsTmp_Next_Due_DateNull()) cmd_Upd.Parameters.AddWithValue("p_Next_Due_Date", transactionRow.Tmp_Next_Due_Date);
                else cmd_Upd.Parameters.AddWithValue("p_Next_Due_Date", DBNull.Value);

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Upd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Upd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_VOUCHER_TEMPLATE_PROCESS.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;

                    messageDS_Reply.ErrorMsg.AddErrorMsgRow("Process Date Update failed - [" + transactionRow.Txn_CategoryName + "]");
                }
                #endregion
            }

            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS_Reply);
            returnDS.AcceptChanges();
            return returnDS;
        }

        #region MonthEnd YearEnd Process
        private DataSet _getAcc_MonthEndProcesses(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAcc_MonthEndProcesses");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.Acc_MonthEndProcess.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_MONTH_END_PROCESSES.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            returnDS.Merge(accDS.Acc_MonthEndProcess);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _saveAcc_MonthEndProcess(DataSet inputDS)
        {
            #region General Declaration
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet requestDS = new DataSet();
            DataSet responseDS = new DataSet();
            DataSet returnDS = new DataSet();
            DataRow[] foundRows;

            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;
            #endregion

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Execute MonthEnd Process
            cmd.CommandText = "PRO_ACC_PROCESS_MONTH_END";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_ProcessMonth", Convert.ToDateTime(stringDS.DataStrings[1].StringValue));

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_MONTH_END_PROCESS_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Insert one row into 'Income Over Expenditure' table
            cmd.CommandText = "PRO_ACC_SIOX_ENTRY";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("p_ProcessMonth", Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_YearStartMonth", Convert.ToDateTime(stringDS.DataStrings[2].StringValue));

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_MONTH_END_PROCESS_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _saveAcc_YearEndProcess(DataSet inputDS)
        {
            #region General Declaration
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet requestDS = new DataSet();
            DataSet responseDS = new DataSet();
            DataSet returnDS = new DataSet();
            DataRow[] foundRows;

            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Balance = new OleDbCommand();
            AccountingDS accDS_Balance = new AccountingDS();
            OleDbCommand cmd_ReFlag = new OleDbCommand();
            AccountingDS accDS_ReFlag = new AccountingDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            bool bError = false;
            int nRowAffected = -1;
            string coaHeadWithGL = "";
            string transactionNumber = "";
            decimal coaBalance = 0;
            decimal voucherTotalAmount = 0;
            #endregion

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get SUMMARY of all transactions of this Fiscal Year
            cmd_Balance = DBCommandProvider.GetDBCommand("GetAcc_Inc_Exp_Bal_Summary");
            adapter = new OleDbDataAdapter();
            if (cmd_Balance == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd_Balance;

            cmd_Balance.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue.Trim());
            cmd_Balance.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue.Trim());

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS_Balance, accDS_Balance.Acc_MonthEndProcess.TableName, 
                                                       connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_YEAR_END_PROCESS_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            // Generate AccountName [Super GL]
            coaHeadWithGL = "";
            foreach (AccountingDS.Acc_MonthEndProcessRow row in accDS_Balance.Acc_MonthEndProcess.Rows)
            {
                row.Txnd_Header = false;  // These are all contra rows
                coaHeadWithGL = row.Coa_Head + " [";
                string[] accCode = row.Coa_Code.Split('-');
                for (int i = 0; i < 3; i++)  // To get first 3 parts of acc code
                {
                    coaHeadWithGL += accCode[i].Trim();
                    if (i < 2) coaHeadWithGL += " - ";
                }
                coaHeadWithGL += "]";
                row.Coa_Head = coaHeadWithGL;
            }
            accDS_Balance.AcceptChanges();
            #endregion

            #region Get Last MonthEnd summary [For RE Flag COA]
            cmd_ReFlag = DBCommandProvider.GetDBCommand("GetAcc_RE_Flag_Voucher_Data");
            adapter = new OleDbDataAdapter();
            if (cmd_ReFlag == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd_ReFlag;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS_ReFlag, accDS_ReFlag.Acc_MonthEndProcess.TableName, 
                                                       connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_YEAR_END_PROCESS_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            // Generate AccountName [Super GL]
            coaHeadWithGL = "";
            foreach (AccountingDS.Acc_MonthEndProcessRow row in accDS_ReFlag.Acc_MonthEndProcess.Rows)
            {
                coaHeadWithGL = row.Coa_Head + " [";
                string[] accCode = row.Coa_Code.Split('-');
                for (int i = 0; i < 3; i++)  // To get first 3 parts of acc code
                {
                    coaHeadWithGL += accCode[i].Trim();
                    if (i < 2) coaHeadWithGL += " - ";
                }
                coaHeadWithGL += "]";
                row.Coa_Head = coaHeadWithGL;
            }
            accDS_ReFlag.AcceptChanges();
            #endregion

            #region Insert ONE Transaction Row
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_TRANSACTION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            decimal totalDebit = accDS_ReFlag.Acc_MonthEndProcess[0].SIOX_Yearly_Cum_Dr;
            decimal totalCredit = accDS_ReFlag.Acc_MonthEndProcess[0].SIOX_Yearly_Cum_Cr;
            voucherTotalAmount = totalDebit - totalCredit;
            long transactionID = IDGenerator.GetNextGenericPK();

            if (transactionID == -1) UtilDL.GetDBOperationFailed();
            else cmd.Parameters.AddWithValue("p_Txn_ID", (object)transactionID);

            cmd.Parameters.AddWithValue("p_Txn_Date", Convert.ToDateTime(stringDS.DataStrings[2].StringValue.Trim())); // Last Date of FiscalYear
            cmd.Parameters.AddWithValue("p_Txn_CategoryCode", "YEARV"); // Year End Voucher
            cmd.Parameters.AddWithValue("p_Txn_Total_Amt", Math.Abs(voucherTotalAmount));  // Always insert positive
            cmd.Parameters.AddWithValue("p_Txn_Posted_Flag", false);
            cmd.Parameters.AddWithValue("p_Create_User", Convert.ToInt32(stringDS.DataStrings[3].StringValue.Trim()));
            
            cmd.Parameters.AddWithValue("p_Txn_DeliveryNoteNo", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Txn_DispatchDocNo", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Txn_DispatchedThrough", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Txn_Destination", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Txn_OrderNo", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Txn_PaymentTerm", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Txn_OtherRef", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Txn_DeliveryTerm", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Txn_Ref_ID", DBNull.Value);

            cmd.Parameters.Add("p_Txn_NO", OleDbType.VarChar, 1000).Direction = ParameterDirection.Output;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_YEAR_END_PROCESS_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            transactionNumber = cmd.Parameters["p_Txn_NO"].Value.ToString();
            cmd.Parameters.Clear();
            #endregion

            #region INSERT Transaction Detail Rows
            #region Add RE Flag row in this series
            // Set Debit Credit amounts
            accDS_ReFlag.Acc_MonthEndProcess[0].Mbal_Balance_Dr = accDS_ReFlag.Acc_MonthEndProcess[0].SIOX_Yearly_Cum_Dr;
            accDS_ReFlag.Acc_MonthEndProcess[0].Mbal_Balance_Cr = accDS_ReFlag.Acc_MonthEndProcess[0].SIOX_Yearly_Cum_Cr;
            accDS_ReFlag.Acc_MonthEndProcess[0].Txnd_Header = true;  // Dami :: To recognize RE_Flag row
            accDS_ReFlag.AcceptChanges();

            accDS_Balance.Acc_MonthEndProcess.ImportRow(accDS_ReFlag.Acc_MonthEndProcess[0]);
            accDS_Balance.AcceptChanges();
            #endregion

            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_TRANSACTION_DTL_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (AccountingDS.Acc_MonthEndProcessRow accRow in accDS_Balance.Acc_MonthEndProcess.Rows)
            {
                coaBalance = accRow.Mbal_Balance_Dr - accRow.Mbal_Balance_Cr;

                cmd.Parameters.AddWithValue("p_Txnd_Txn_Id", transactionID);

                if (!accRow.IsMbal_Coa_IdNull()) cmd.Parameters.AddWithValue("p_Txnd_Coa_Id", accRow.Mbal_Coa_Id);
                else cmd.Parameters.AddWithValue("p_Txnd_Coa_Id", DBNull.Value);

                if (!accRow.IsCoa_HeadNull()) cmd.Parameters.AddWithValue("p_Txnd_Account_Name", accRow.Coa_Head);
                else cmd.Parameters.AddWithValue("p_Txnd_Account_Name", DBNull.Value);

                cmd.Parameters.AddWithValue("p_Txnd_Description", DBNull.Value);

                if (!accRow.Txnd_Header)  // For all other accounts
                {
                    cmd.Parameters.AddWithValue("p_Txnd_Amt", Math.Abs(coaBalance));  // Always insert positive

                    if (coaBalance < 0) cmd.Parameters.AddWithValue("p_Txnd_Nature", "D");  // Because this is reverse voucher
                    else cmd.Parameters.AddWithValue("p_Txnd_Nature", "C");
                }
                else  // For RE_Flag account
                {
                    coaBalance = accRow.Mbal_Balance_Dr - accRow.Mbal_Balance_Cr;
                    cmd.Parameters.AddWithValue("p_Txnd_Amt", Math.Abs(coaBalance));  // Always insert positive

                    if (coaBalance < 0) cmd.Parameters.AddWithValue("p_Txnd_Nature", "C");  // No change for 'RE_Flag' account
                    else cmd.Parameters.AddWithValue("p_Txnd_Nature", "D");
                }

                cmd.Parameters.AddWithValue("p_Txnd_Ref_No", DBNull.Value);
                cmd.Parameters.AddWithValue("p_Txnd_Ref_Date", DBNull.Value);
                cmd.Parameters.AddWithValue("p_Txnd_Ref_Type_Id", DBNull.Value);
                cmd.Parameters.AddWithValue("p_Txnd_Tracer", 1);
                cmd.Parameters.AddWithValue("p_Txnd_Header", false);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ACC_YEAR_END_PROCESS_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Insert ONE History Row
            cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ACC_TRANSACTION_HIST_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            long genPK = IDGenerator.GetNextGenericPK();

            if (genPK == -1) UtilDL.GetDBOperationFailed();
            else cmd.Parameters.AddWithValue("p_Txnh_Id", (object)genPK);

            cmd.Parameters.AddWithValue("p_Txnh_Txn_Id", transactionID);
            cmd.Parameters.AddWithValue("p_Txnh_Sender_User_Id", Convert.ToInt32(stringDS.DataStrings[3].StringValue.Trim()));
            cmd.Parameters.AddWithValue("p_Txnh_Received_User_Id", Convert.ToInt32(stringDS.DataStrings[3].StringValue.Trim()));
            cmd.Parameters.AddWithValue("p_Txnh_ActivityCode", "V");  // Verified
            cmd.Parameters.AddWithValue("p_Txnh_History_Comments", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Txnh_Priority", 0);
            cmd.Parameters.AddWithValue("p_Txn_Approval_Status", "A"); // Approved

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ACC_YEAR_END_PROCESS_SAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        #endregion

        private DataSet _getAcc_Vouchers_InDateRange(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAcc_Vouchers_InDateRange");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["Txn_App_Status1"].Value = cmd.Parameters["Txn_App_Status2"].Value = stringDS.DataStrings[2].StringValue;

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.Acc_Transaction_Detail.TableName, 
                                                       connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_VOUCHERS_IN_DATE_RANGE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            returnDS.Merge(accDS.Acc_Transaction_Detail);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAcc_Last_YearEnd_Voucher(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAcc_Last_YearEnd_Voucher");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.Acc_Transaction_Detail.TableName,
                                                       connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_LAST_YEAR_END_VOUCHER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            returnDS.Merge(accDS.Acc_Transaction_Detail);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getAcc_Trial_Balance_Report(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAcc_TrialBalance_OnDate");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            cmd.Parameters["PreviousMonth1"].Value = cmd.Parameters["PreviousMonth2"].Value = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["PeriodStart1"].Value = cmd.Parameters["PeriodStart2"].Value = 
                cmd.Parameters["PeriodStart3"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["PeriodEnd1"].Value = cmd.Parameters["PeriodEnd2"].Value = 
                cmd.Parameters["PeriodEnd3"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.Acc_BalanceReport.TableName,
                                                       connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_TRIAL_BALANCE_REPORT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            returnDS.Merge(accDS.Acc_BalanceReport);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAcc_Balance_Sheet_Report(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            AccountingDS accDS = new AccountingDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAcc_BalanceSheet_OnDate");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            cmd.Parameters["PreviousMonth1"].Value = cmd.Parameters["PreviousMonth2"].Value = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["PeriodStart1"].Value = cmd.Parameters["PeriodStart2"].Value =
                cmd.Parameters["PeriodStart3"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            cmd.Parameters["PeriodEnd1"].Value = cmd.Parameters["PeriodEnd2"].Value =
                cmd.Parameters["PeriodEnd3"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);

            accDS = new AccountingDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, accDS, accDS.Acc_BalanceReport.TableName,
                                                       connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACC_TRIAL_BALANCE_REPORT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            accDS.AcceptChanges();

            returnDS.Merge(accDS.Acc_BalanceReport);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

    }
}