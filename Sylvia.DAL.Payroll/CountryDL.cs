using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for CountryDL.
    /// </summary>
    public class CountryDL
    {
        public CountryDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_ACTION_GET_COUNTRY_LIST:
                    return _getCountryList(inputDS);
                case ActionID.ACTION_DOES_COUNTRY_EXIST:
                    return _doesCountryExist(inputDS);
                case ActionID.NA_ACTION_GET_CITY_LIST:
                    return _getCityList(inputDS);
                case ActionID.NA_ACTION_GET_DIVISION_LIST:
                    return _getDivisionList(inputDS);
                case ActionID.NA_ACTION_GET_DISTRICT_BY_COUNTRYID:
                    return _getDistrictListByCountryId(inputDS);
                case ActionID.NA_ACTION_GET_DISTRICT_LIST:
                    return _getDistrictList(inputDS);
                case ActionID.NA_ACTION_GET_THANA_LIST:
                    return _getThanaList(inputDS);

                #region WALI :: 30-Aug-2014
                case ActionID.ACTION_COUNTRY_DIVISION_CREATE:
                    return this._createCountryDivision(inputDS);
                case ActionID.ACTION_COUNTRY_DIVISION_UPDATE:
                    return this._updateCountryDivision(inputDS);
                case ActionID.ACTION_COUNTRY_DIVISION_DELETE:
                    return this._deleteCountryDivision(inputDS);

                case ActionID.ACTION_COUNTRY_DISTRICT_CREATE:
                    return this._createCountryDistrict(inputDS);
                case ActionID.ACTION_COUNTRY_DISTRICT_UPDATE:
                    return this._updateCountryDistrict(inputDS);
                case ActionID.ACTION_COUNTRY_DISTRICT_DELETE:
                    return this._deleteCountryDistrict(inputDS);

                case ActionID.ACTION_COUNTRY_THANA_CREATE:
                    return this._createCountryThana(inputDS);
                case ActionID.ACTION_COUNTRY_THANA_UPDATE:
                    return this._updateCountryThana(inputDS);
                case ActionID.ACTION_COUNTRY_THANA_DELETE:
                    return this._deleteCountryThana(inputDS);
                #endregion

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getCountryList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();



            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCountryList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            CountryPO countryPO = new CountryPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              countryPO, countryPO.Countries.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COUNTRY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            countryPO.AcceptChanges();

            CountryDS countryDS = new CountryDS();
            if (countryPO.Countries.Count > 0)
            {


                foreach (CountryPO.Country poCom in countryPO.Countries)
                {
                    CountryDS.Country country = countryDS.Countries.NewCountry();
                    if (poCom.IsCodeNull() == false)
                    {
                        country.Code = poCom.Code;
                    }
                    if (poCom.IsNameNull() == false)
                    {
                        country.Name = poCom.Name;
                    }
                    if (poCom.IsDescriptionNull() == false)
                    {
                        country.Description = poCom.Description;
                    }
                    if (poCom.IsNationalityNull() == false)
                    {
                        country.Nationality = poCom.Nationality;
                    }
                    if (poCom.IsCountryidNull() == false)
                    {
                        country.Countryid = poCom.Countryid;
                    }
                    countryDS.Countries.AddCountry(country);
                    countryDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(countryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _doesCountryExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesCountryExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getCityList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCityList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            CountryPO countryPO = new CountryPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              countryPO, countryPO.City.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_CITY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            countryPO.AcceptChanges();

            CountryDS countryDS = new CountryDS();

            if (countryPO.City.Count > 0)
            {
                foreach (CountryPO.CityRow poRow in countryPO.City.Rows)
                {
                    CountryDS.CityRow row = countryDS.City.NewCityRow();

                    if (poRow.IsCITYIDNull() == false)
                    {
                        row.CITYID = poRow.CITYID;
                    }
                    if (poRow.IsCITYNAMENull() == false)
                    {
                        row.CITYNAME = poRow.CITYNAME;
                    }
                    if (poRow.IsCITYREMARKSNull() == false)
                    {
                        row.CITYREMARKS = poRow.CITYREMARKS;
                    }
                    if (poRow.IsCOUNTRYCODENull() == false)
                    {
                        row.COUNTRYCODE = poRow.COUNTRYCODE;
                    }

                    countryDS.City.AddCityRow(row);
                    countryDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(countryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getDivisionList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            CountryDS countryDS = new CountryDS();
            CountryPO countryPO = new CountryPO();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDivisionList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, countryPO, countryPO.Division.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            nRowAffected = ADOController.Instance.Fill(adapter, countryDS, countryDS.Division.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DIVISION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            //countryPO.AcceptChanges();
            countryDS.AcceptChanges();

            #region Commented :: WALI :: 27-Aug-2015
            //if (countryPO.Division.Count > 0)
            //{
            //    foreach (CountryPO.DivisionRow poCom in countryPO.Division)
            //    {
            //        CountryDS.DivisionRow Division = countryDS.Division.NewDivisionRow();
            //        if (poCom.IsDivisionidNull() == false)
            //        {
            //            Division.Divisionid = poCom.Divisionid;
            //        }
            //        if (poCom.IsDivisioncodeNull() == false)
            //        {
            //            Division.Divisioncode = poCom.Divisioncode;
            //        }
            //        if (poCom.IsDivisionnameNull() == false)
            //        {
            //            Division.Divisionname = poCom.Divisionname;
            //        }

            //        if (poCom.IsCountryidNull() == false)
            //        {
            //            Division.Countryid = poCom.Countryid;
            //        }

            //        countryDS.Division.AddDivisionRow(Division);
            //        countryDS.AcceptChanges();
            //    }
            //}
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(countryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getDistrictList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS StringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            CountryPO countryPO = new CountryPO();
            CountryDS countryDS = new CountryDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDistrictList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            // Update :: WALI :: Added new Param 'DIVISIONID1' :: 29-Aug-2015
            cmd.Parameters["DIVISIONID"].Value = cmd.Parameters["DIVISIONID1"].Value = Convert.ToInt32(StringDS.DataStrings[0].StringValue);
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, countryPO, countryPO.District.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            nRowAffected = ADOController.Instance.Fill(adapter, countryDS, countryDS.District.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DISTRICT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            //countryPO.AcceptChanges();
            countryDS.AcceptChanges();

            #region Commented :: WALI :: 27-Aug-2015
            //if (countryPO.District.Count > 0)
            //{
            //    foreach (CountryPO.DistrictRow poCom in countryPO.District)
            //    {
            //        CountryDS.DistrictRow District = countryDS.District.NewDistrictRow();
            //        if (poCom.IsDistrictidNull() == false)
            //        {
            //            District.Districtid = poCom.Districtid;
            //        }
            //        if (poCom.IsDistrictcodeNull() == false)
            //        {
            //            District.Districtcode = poCom.Districtcode;
            //        }
            //        if (poCom.IsDistrictnameNull() == false)
            //        {
            //            District.Districtname = poCom.Districtname;
            //        }

            //        if (poCom.IsDivisionidNull() == false)
            //        {
            //            District.Divisionid = poCom.Divisionid;
            //        }

            //        countryDS.District.AddDistrictRow(District);
            //        countryDS.AcceptChanges();
            //    }
            //}
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(countryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getDistrictListByCountryId(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS StringDS = new DataStringDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            CountryPO countryPO = new CountryPO();
            CountryDS countryDS = new CountryDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDistrictListByCountryId");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["countryId"].Value = Convert.ToInt32(StringDS.DataStrings[0].StringValue);
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, countryDS, countryDS.District.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DISTRICT_BY_COUNTRYID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            //countryPO.AcceptChanges();
            countryDS.AcceptChanges();

            #region Commented :: WALI :: 27-Aug-2015

            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(countryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getThanaList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            DataStringDS StringDS = new DataStringDS();
            CountryPO countryPO = new CountryPO();
            CountryDS countryDS = new CountryDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetThanaList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            //cmd.Parameters["DISTRICTID"].Value = (object)StringDS.DataStrings[0].StringValue;      // WALI :: 01-Sep-2015
            cmd.Parameters["DISTRICTID"].Value = Convert.ToInt32(StringDS.DataStrings[0].StringValue);
            cmd.Parameters["DISTRICTID1"].Value = Convert.ToInt32(StringDS.DataStrings[0].StringValue);
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, countryPO, countryPO.Thana.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            nRowAffected = ADOController.Instance.Fill(adapter, countryDS, countryDS.Thana.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DISTRICT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            countryPO.AcceptChanges();
            countryDS.AcceptChanges();

            #region Commented :: WALI :: 27-Aug-2015
            //if (countryPO.Thana.Count > 0)
            //{
            //    foreach (CountryPO.ThanaRow poCom in countryPO.Thana)
            //    {
            //        CountryDS.ThanaRow Thana = countryDS.Thana.NewThanaRow();
            //        if (poCom.IsThanaidNull() == false)
            //        {
            //            Thana.Thanaid = poCom.Thanaid;
            //        }
            //        if (poCom.IsThanacodeNull() == false)
            //        {
            //            Thana.Thanacode = poCom.Thanacode;
            //        }
            //        if (poCom.IsThananameNull() == false)
            //        {
            //            Thana.Thananame = poCom.Thananame;
            //        }

            //        if (poCom.IsDistrictidNull() == false)
            //        {
            //            Thana.Districtid = poCom.Districtid;
            //        }

            //        countryDS.Thana.AddThanaRow(Thana);
            //        countryDS.AcceptChanges();
            //    }
            //}
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(countryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        #region Country Division :: WALI :: 26-Aug-2014
        private DataSet _createCountryDivision(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CountryDS countryDS = new CountryDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DBConnectionDS connRecDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COUNTRY_DIVISION_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            countryDS.Merge(inputDS.Tables[countryDS.Division.TableName], false, MissingSchemaAction.Error);
            countryDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            connRecDS.Merge(inputDS.Tables[connRecDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connRecDS.AcceptChanges();

            foreach (CountryDS.DivisionRow row in countryDS.Division.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("@p_DivisionID", (object)genPK);

                if (!row.IsDivisioncodeNull()) cmd.Parameters.AddWithValue("@p_DivisionCode", row.Divisioncode);
                else cmd.Parameters.AddWithValue("@p_DivisionCode", DBNull.Value);
                if (!row.IsDivisionnameNull()) cmd.Parameters.AddWithValue("@p_DivisionName", row.Divisionname);
                else cmd.Parameters.AddWithValue("@p_DivisionName", DBNull.Value);
                if (!row.IsCountryidNull()) cmd.Parameters.AddWithValue("@p_CountryID", row.Countryid);
                else cmd.Parameters.AddWithValue("@p_CountryID", DBNull.Value);
                if (!row.IsCountryidNull()) cmd.Parameters.AddWithValue("@p_DivisionName_Nativelang", row.DivisionName_NativeLang);
                else cmd.Parameters.AddWithValue("@p_DivisionName_Nativelang", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connRecDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connRecDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COUNTRY_DIVISION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateCountryDivision(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CountryDS countryDS = new CountryDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DBConnectionDS connRecDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            countryDS.Merge(inputDS.Tables[countryDS.Division.TableName], false, MissingSchemaAction.Error);
            countryDS.AcceptChanges();
            connRecDS.Merge(inputDS.Tables[connRecDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connRecDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COUNTRY_DIVISION_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (CountryDS.DivisionRow row in countryDS.Division.Rows)
            {
                if (!row.IsDivisionidNull()) cmd.Parameters.AddWithValue("@p_DivisionID", row.Divisionid);
                else cmd.Parameters.AddWithValue("@p_DivisionID", DBNull.Value);
                if (!row.IsDivisionnameNull()) cmd.Parameters.AddWithValue("@p_DivisionName", row.Divisionname);
                else cmd.Parameters.AddWithValue("@p_DivisionName", DBNull.Value);
                if (!row.IsCountryidNull()) cmd.Parameters.AddWithValue("@p_CountryID", row.Countryid);
                else cmd.Parameters.AddWithValue("@p_CountryID", DBNull.Value);
                if (!row.IsCountryidNull()) cmd.Parameters.AddWithValue("@p_DivisionName_Nativelang", row.DivisionName_NativeLang);
                else cmd.Parameters.AddWithValue("@p_DivisionName_Nativelang", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connRecDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connRecDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COUNTRY_DIVISION_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteCountryDivision(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DBConnectionDS connRecDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            CountryDS countryDS = new CountryDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            countryDS.Merge(inputDS.Tables[countryDS.Division.TableName], false, MissingSchemaAction.Error);
            countryDS.AcceptChanges();
            connRecDS.Merge(inputDS.Tables[connRecDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connRecDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COUNTRY_DIVISION_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (CountryDS.DivisionRow row in countryDS.Division.Rows)
            {
                cmd.Parameters.AddWithValue("@p_DivisionID", row.Divisionid);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connRecDS.DBConnections.Rows.Count == 2)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connRecDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COUNTRY_DIVISION_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.Divisioncode + " - " + row.Divisionname);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.Divisioncode + " - " + row.Divisionname);
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Country District :: WALI :: 31-Aug-2014
        private DataSet _createCountryDistrict(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CountryDS countryDS = new CountryDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COUNTRY_DISTRICT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            countryDS.Merge(inputDS.Tables[countryDS.District.TableName], false, MissingSchemaAction.Error);
            countryDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (CountryDS.DistrictRow row in countryDS.District.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("@p_DistrictID", (object)genPK);

                if (!row.IsDistrictnameNull()) cmd.Parameters.AddWithValue("@DistrictName", row.Districtname);
                else cmd.Parameters.AddWithValue("@DistrictName", DBNull.Value);
                if (!row.IsDivisionidNull()) cmd.Parameters.AddWithValue("@p_DivisionID", row.Divisionid);
                else cmd.Parameters.AddWithValue("@p_DivisionID", DBNull.Value);
                if (!row.IsDistrictName_NativeLangNull()) cmd.Parameters.AddWithValue("@p_DistrictName_Nativelang", row.DistrictName_NativeLang);
                else cmd.Parameters.AddWithValue("@p_DistrictName_Nativelang", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COUNTRY_DISTRICT_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateCountryDistrict(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CountryDS countryDS = new CountryDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            countryDS.Merge(inputDS.Tables[countryDS.District.TableName], false, MissingSchemaAction.Error);
            countryDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COUNTRY_DISTRICT_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (CountryDS.DistrictRow row in countryDS.District.Rows)
            {
                if (!row.IsDistrictidNull()) cmd.Parameters.AddWithValue("@p_DistrictID", row.Districtid);
                else cmd.Parameters.AddWithValue("@p_DistrictID", DBNull.Value);
                if (!row.IsDistrictnameNull()) cmd.Parameters.AddWithValue("@DistrictName", row.Districtname);
                else cmd.Parameters.AddWithValue("@DistrictName", DBNull.Value);
                if (!row.IsDivisionidNull()) cmd.Parameters.AddWithValue("@p_DivisionID", row.Divisionid);
                else cmd.Parameters.AddWithValue("@p_DivisionID", DBNull.Value);
                if (!row.IsDistrictName_NativeLangNull()) cmd.Parameters.AddWithValue("@p_DistrictName_Nativelang", row.DistrictName_NativeLang);
                else cmd.Parameters.AddWithValue("@p_DistrictName_Nativelang", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COUNTRY_DISTRICT_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteCountryDistrict(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            CountryDS countryDS = new CountryDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            countryDS.Merge(inputDS.Tables[countryDS.District.TableName], false, MissingSchemaAction.Error);
            countryDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COUNTRY_DISTRICT_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (CountryDS.DistrictRow row in countryDS.District.Rows)
            {
                cmd.Parameters.AddWithValue("@p_DistrictID", row.Districtid);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COUNTRY_DISTRICT_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.Districtname + " - " + row.Divisionname + ", " + row.CountryName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.Districtname + " - " + row.Divisionname + ", " + row.CountryName);
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Country Thana :: WALI :: 31-Aug-2014
        private DataSet _createCountryThana(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CountryDS countryDS = new CountryDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DBConnectionDS connRecDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COUNTRY_THANA_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            countryDS.Merge(inputDS.Tables[countryDS.Thana.TableName], false, MissingSchemaAction.Error);
            countryDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (CountryDS.ThanaRow row in countryDS.Thana.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("@p_ThanaID", (object)genPK);

                if (!row.IsThanacodeNull()) cmd.Parameters.AddWithValue("@p_ThanaCode", row.Thanacode);
                else cmd.Parameters.AddWithValue("@p_ThanaCode", DBNull.Value);
                if (!row.IsThananameNull()) cmd.Parameters.AddWithValue("@p_ThanaName", row.Thananame);
                else cmd.Parameters.AddWithValue("@p_ThanaName", DBNull.Value);
                if (!row.IsDistrictidNull()) cmd.Parameters.AddWithValue("@p_DistrictID", row.Districtid);
                else cmd.Parameters.AddWithValue("@p_DistrictID", DBNull.Value);
                if (!row.IsThanaName_NativeLangNull()) cmd.Parameters.AddWithValue("@p_ThanaName_Nativelang", row.ThanaName_NativeLang);
                else cmd.Parameters.AddWithValue("@p_ThanaName_Nativelang", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COUNTRY_THANA_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateCountryThana(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            CountryDS countryDS = new CountryDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            countryDS.Merge(inputDS.Tables[countryDS.Thana.TableName], false, MissingSchemaAction.Error);
            countryDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COUNTRY_THANA_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (CountryDS.ThanaRow row in countryDS.Thana.Rows)
            {
                if (!row.IsThanaidNull()) cmd.Parameters.AddWithValue("@p_ThanaID", row.Thanaid);
                else cmd.Parameters.AddWithValue("@p_ThanaID", DBNull.Value);
                if (!row.IsThanacodeNull()) cmd.Parameters.AddWithValue("@p_ThanaCode", row.Thanacode);
                else cmd.Parameters.AddWithValue("@p_ThanaCode", DBNull.Value);
                if (!row.IsThananameNull()) cmd.Parameters.AddWithValue("@p_ThanaName", row.Thananame);
                else cmd.Parameters.AddWithValue("@p_ThanaName", DBNull.Value);
                if (!row.IsDistrictidNull()) cmd.Parameters.AddWithValue("@p_DistrictID", row.Districtid);
                else cmd.Parameters.AddWithValue("@p_DistrictID", DBNull.Value);
                if (!row.IsThanaName_NativeLangNull()) cmd.Parameters.AddWithValue("@p_ThanaName_Nativelang", row.ThanaName_NativeLang);
                else cmd.Parameters.AddWithValue("@p_ThanaName_Nativelang", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COUNTRY_THANA_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteCountryThana(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            CountryDS countryDS = new CountryDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            countryDS.Merge(inputDS.Tables[countryDS.Thana.TableName], false, MissingSchemaAction.Error);
            countryDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COUNTRY_THANA_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (CountryDS.ThanaRow row in countryDS.Thana.Rows)
            {
                cmd.Parameters.AddWithValue("@p_ThanaID", row.Thanaid);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COUNTRY_THANA_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.Thananame + " - " + row.Districtname + ", " + row.Divisionname + ", " + row.CountryName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.Thananame + " - " + row.Districtname + ", " + row.Divisionname + ", " + row.CountryName);
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

    }
}
