using System;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using System.Text;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
  /// <summary>
  /// Summary description for DepartmentDL.
  /// </summary>
  public class SalaryHeadConfigDL
  {
    public SalaryHeadConfigDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }

    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
      try
      {
        actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();

        returnDS.Merge(errDS);
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch (actionID)
      {
        case ActionID.ACTION_SALARYHEADCONFIG_ADD:
          return _createSalaryHeadConfig(inputDS);
      case ActionID.NA_ACTION_Q_ALL_SALARYHEADCONFIG:
          return this._getSalaryHeadConfigList(inputDS);
        default:
          Util.LogInfo("Action ID is not supported in this class.");
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
          errDS.Errors.AddError(err);
          returnDS.Merge(errDS);
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
 
    private DataSet _createSalaryHeadConfig(DataSet inputDS)
    {
      DataSet responseDS = new DataSet();
      ErrorDS errDS = new ErrorDS();
      SalaryHeadConfigDS configDS = new SalaryHeadConfigDS();
      DBConnectionDS connDS = new DBConnectionDS();
      configDS.Merge(inputDS.Tables[configDS.SalaryHeadConfig.TableName], false, MissingSchemaAction.Error);
      configDS.AcceptChanges();

      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      // Delete items from table

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteSalaryHeadConfig");
      if (cmd == null)
        return UtilDL.GetCommandNotFound();

      cmd.Parameters["ConfigId"].Value = configDS.SalaryHeadConfig[0].ConfigId;
      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.ACTION_SALARYHEADCONFIG_ADD.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;
      }
  // Insert data in table
     cmd = DBCommandProvider.GetDBCommand("CreateSalaryHeadConfig");
      if (cmd == null)
      {
        return UtilDL.GetCommandNotFound();
      }
      
      foreach (SalaryHeadConfigDS.SalaryHeadConfigRow config in configDS.SalaryHeadConfig)
      {
        
        if (config.IsConfigIdNull() == false)
        {
          cmd.Parameters["SalaryHeadConfigId"].Value = config.ConfigId;
        }
        else
        {
          cmd.Parameters["SalaryHeadConfigId"].Value = null;
        }
        if (config.IsAlloeDeductCodeNull() == false)
        {
          cmd.Parameters["AlloeDeductId"].Value = UtilDL.GetAllowDeductId(connDS, config.AlloeDeductCode);
        }
        else
        {
          cmd.Parameters["AlloeDeductId"].Value = null;
        }

        bError = false;
        nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        if (bError == true)
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
          err.ErrorInfo1 = ActionID.ACTION_SALARYHEADCONFIG_ADD.ToString();
          errDS.Errors.AddError(err);
          errDS.AcceptChanges();
          return errDS;
        }
      }
      errDS.Clear();
      errDS.AcceptChanges();
      return errDS;
    }
    private DataSet _getSalaryHeadConfigList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      DataLongDS longDS = new DataLongDS();
      DBConnectionDS connDS = new DBConnectionDS();
      longDS.Merge(inputDS.Tables[longDS.DataLongs.TableName], false, MissingSchemaAction.Error);
      longDS.AcceptChanges();


      connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
      connDS.AcceptChanges();

      OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSalaryHeadConfigList");
      if (cmd == null)
      {

        Util.LogInfo("Command is null.");
        return UtilDL.GetCommandNotFound();
      }
      if (longDS.DataLongs[0].IsLongValueNull() == false)
      {
        long  nConfigId = longDS.DataLongs[0].LongValue;
        cmd.Parameters["ConfigId"].Value = nConfigId; ;

      }
      OleDbDataAdapter adapter = new OleDbDataAdapter();
      adapter.SelectCommand = cmd;

      SalaryHeadConfigPO configPO = new SalaryHeadConfigPO();

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = ADOController.Instance.Fill(adapter,
        configPO,
        configPO.SalaryHeadConfig.TableName,
        connDS.DBConnections[0].ConnectionID,
        ref bError);
      if (bError == true)
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_SALARYHEADCONFIG.ToString();
        errDS.Errors.AddError(err);
        errDS.AcceptChanges();
        return errDS;

      }

      configPO.AcceptChanges();
      SalaryHeadConfigDS configDS = new SalaryHeadConfigDS();
      if (configPO.SalaryHeadConfig.Count > 0)
      {
        foreach (SalaryHeadConfigPO.SalaryHeadConfigRow poConfig in configPO.SalaryHeadConfig)
        {
          SalaryHeadConfigDS.SalaryHeadConfigRow config = configDS.SalaryHeadConfig.NewSalaryHeadConfigRow();
          if (poConfig.IsconfigidNull() == false)
          {
            config.ConfigId = poConfig.configid;
          }
          if (poConfig.IsalloededucidNull() == false)
          {
            config.AlloeDeductCode = UtilDL.GetAllowDeductCode(connDS , poConfig.alloededucid);
          }
          configDS.SalaryHeadConfig.AddSalaryHeadConfigRow(config);
          configDS.AcceptChanges();
        }
      }


      // now create the packet
      errDS.Clear();
      errDS.AcceptChanges();

      DataSet returnDS = new DataSet();

      returnDS.Merge(configDS);
      returnDS.Merge(errDS);
      returnDS.AcceptChanges();
      return returnDS;


    }

   



  }
}

