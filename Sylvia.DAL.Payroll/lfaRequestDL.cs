using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    public class lfaRequestDL
    {
        public lfaRequestDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                #region LFA Request...
                case ActionID.ACTION_LFA_REQUEST_CREATE:
                    return this._createLFARequest(inputDS);
                case ActionID.NA_ACTION_GET_LEAVE_INFO_BY_EMPLOYEEID:
                    return this._getLeaveInfoByEmployeeID(inputDS);
                case ActionID.NA_ACTION_GET_LFA_SUMMARY:
                    return this._getLFASummary(inputDS);
                case ActionID.NA_ACTION_GET_LFA_SUMMARY_REPORT:
                    return this._GetLFASummaryReport(inputDS);

                #endregion

                #region LFA Request Approval
                case ActionID.ACTION_LFA_REQUEST_APPROVAL_CREATE:
                    return this._createLFARequestApproval(inputDS);
                #endregion

                case ActionID.NA_ACTION_GET_EMPLOYEE_LFA:
                    return this._getEmployeeLFA(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_LFA_BY_EMPTYPESITE:
                    return this._getEmployeeLFAByEmpTypeSite(inputDS);
                case ActionID.NA_ACTION_GET_LFA_REQUEST_LIST:
                    return this._getLFARequest(inputDS);
                case ActionID.NA_ACTION_GET_LFA_REQUEST_LIST_BY_YEAR:
                    return this._getLFARequestListBySalaryYear(inputDS);

                case ActionID.NA_ACTION_GET_LFA_SPECIFIC_USER:
                    return this._getLFA(inputDS);

                case ActionID.NA_ACTION_GET_EMPLOYEE_DETAILS_FOR_LFA:
                    return this._getEmployeeDetailsForLFA(inputDS);

                case ActionID.ACTION_LFA_ASSIGN_CREATE:
                    return this._createLFAAssign(inputDS);
                case ActionID.NA_ACTION_GET_LFA_ANALYSIS_RPT:
                    return this._getLFAAnalysisReport(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getLFA(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //DataIntegerDS integerDS = new DataIntegerDS();
            //integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            //integerDS.AcceptChanges();
            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLFAForSEmployee");//lmsLeave 

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["employeeid"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            LFARequestDS LFA = new LFARequestDS();
            //LFA.Merge(responseDS.Tables[LFA.LFASummaryRpt.TableName], false, MissingSchemaAction.Error);
            //LFA.AcceptChanges();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, LFA, LFA.LFASummaryRpt.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LFA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            LFA.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(LFA);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        #region LFA Request...
        private DataSet _createLFARequest(DataSet inputDS)
        {
            string messageBody = "", returnMessage = "";
            ErrorDS errDS = new ErrorDS();
            LFARequestDS lfaRequestDS = new LFARequestDS();
            DBConnectionDS connDS = new DBConnectionDS();

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LFA_REQUEST_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            lfaRequestDS.Merge(inputDS.Tables[lfaRequestDS.LFARequest.TableName], false, MissingSchemaAction.Error);
            lfaRequestDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (LFARequestDS.LFARequestRow row in lfaRequestDS.LFARequest.Rows)
            {
                long lfaRequestID = IDGenerator.GetNextGenericPK();
                if (lfaRequestID == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_LFAREQUESTID", lfaRequestID);

                if (row.IsEMPLOYEEIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EMPLOYEEID", row.EMPLOYEEID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EMPLOYEEID", DBNull.Value);
                }

                if (row.IsLFAAMOUNTNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LFAAMOUNT", row.LFAAMOUNT);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LFAAMOUNT", DBNull.Value);
                }

                if (row.IsLEAVEREQUESTIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LEAVEREQUESTID", row.LEAVEREQUESTID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LEAVEREQUESTID", DBNull.Value);
                }

                if (row.IsLFAYEARNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LFAYEAR", row.LFAYEAR);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LFAYEAR", DBNull.Value);
                }

                if (row.IsPAYMONTHNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_PAYMONTH", row.PAYMONTH);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_PAYMONTH", DBNull.Value);
                }

                if (row.IsLFAREQUESTREMARKSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LFAREQUESTREMARKS", row.LFAREQUESTREMARKS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LFAREQUESTREMARKS", DBNull.Value);
                }

                long lfaHistoryID = IDGenerator.GetNextGenericPK();
                if (lfaHistoryID == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_LFAHISTORYID", lfaHistoryID);

                if (row.IsReceivedEmpIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_RECEIVEDEMPID", row.ReceivedEmpID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_RECEIVEDEMPID", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_ACTIVITIESID", Convert.ToInt32(LFARequestActivities.PendingApproval));
                cmd.Parameters.AddWithValue("p_LFAHISTORYREMARKS", "");

                bool bError = false;
                int nRowAffected = -1;

                if (row.IsNotificationByMail)
                {
                    returnMessage = "";

                    //Kaysar, 03-Nov-2011
                    row.Url += "?pVal=lfa";

                    messageBody = "<b>Dispatcher: </b>" + row.SenderEmpName + " [" + row.SenderEmpCode + "]" + "<br><br>";
                    messageBody += "<b>Applicant: </b>" + row.SenderEmpName + " [" + row.SenderEmpCode + "]<br>";
                    messageBody += "<b>Leave Date: </b>" + row.LeaveRange;
                    messageBody += "<br><br><br>";
                    messageBody += "Click the following link: ";
                    messageBody += "<br>";
                    messageBody += "<a href='" + row.Url + "'>" + row.Url + "</a>";

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, row.FromEmailAddress, row.EmailAddress, row.CcEmailAddress, row.MailingSubject, messageBody, row.SenderEmpName);
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, row.FromEmailAddress, row.EmailAddress, row.CcEmailAddress, row.MailingSubject, messageBody, row.SenderEmpName);
                    }

                    if (returnMessage == "Email successfully sent.")
                    {
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    }
                    else
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LFA_REQUEST_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                else
                {
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                }

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LFA_REQUEST_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getLeaveInfoByEmployeeID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLeaveInfoByEmployeeID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeavePO leavePO = new lmsLeavePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leavePO, leavePO.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LEAVE_INFO_BY_EMPLOYEEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leavePO.AcceptChanges();

            lmsLeaveDS leaveDS = new lmsLeaveDS();
            if (leavePO.Leave.Count > 0)
            {
                foreach (lmsLeavePO.LeaveRow poRow in leavePO.Leave.Rows)
                {
                    lmsLeaveDS.LeaveRow row = leaveDS.Leave.NewLeaveRow();

                    //row.LeaveID = poRow.LeaveID;

                    if (poRow.IsLeaveRequestIDNull() == false)
                    {
                        row.LeaveRequestID = poRow.LeaveRequestID;
                    }
                    if (poRow.IsDateRangeNull() == false)
                    {
                        row.DateRange = poRow.DateRange;
                    }
                    if (poRow.IsNoOfDaysNull() == false)
                    {
                        row.NoOfDays = poRow.NoOfDays;
                    }
                    if (poRow.IsLeaveDateNull() == false)
                    {
                        row.LeaveDate = poRow.LeaveDate;
                    }
                    if (poRow.IsActivitiesIDNull() == false)
                    {
                        row.ActivitiesID = poRow.ActivitiesID;
                    }

                    leaveDS.Leave.AddLeaveRow(row);
                    leaveDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLFASummary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLFASummary");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            LFARequestDS lfaRequestDS = new LFARequestDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, lfaRequestDS, lfaRequestDS.LFASummary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LFA_SUMMARY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            lfaRequestDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(lfaRequestDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        #endregion

        #region LFA Request Approve...
        private DataSet _createLFARequestApproval(DataSet inputDS)
        {
            #region Local Variable Declaration
            long lfaHistoryID = 0;
            ErrorDS errDS = new ErrorDS();
            DataBoolDS boolDS = new DataBoolDS();
            LFARequestDS lfaRequestDS = new LFARequestDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            string messageBody = "", returnMessage = "", URL = "";
            #endregion

            #region Get Email Information from UtilDL
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            OleDbCommand cmdLFARequestHistory = new OleDbCommand();
            cmdLFARequestHistory.CommandType = CommandType.StoredProcedure;

            cmdLFARequestHistory.CommandText = "PRO_LFA_REQ_APPROVAL_CREATE";

            if (cmdLFARequestHistory == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            lfaRequestDS.Merge(inputDS.Tables[lfaRequestDS.LFAHistory.TableName], false, MissingSchemaAction.Error);
            lfaRequestDS.Merge(inputDS.Tables[lfaRequestDS.EmailPacket.TableName], false, MissingSchemaAction.Error);
            lfaRequestDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            foreach (LFARequestDS.LFAHistoryRow row in lfaRequestDS.LFAHistory.Rows)
            {
                #region Save LFA Request Approval...

                lfaHistoryID = IDGenerator.GetNextGenericPK();
                if (lfaHistoryID == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmdLFARequestHistory.Parameters.AddWithValue("p_LFAHISTORYID", lfaHistoryID);

                if (row.IsLFAREQUESTIDNull() == false)
                {
                    cmdLFARequestHistory.Parameters.AddWithValue("p_LFAREQUESTID", row.LFAREQUESTID);
                }
                else
                {
                    cmdLFARequestHistory.Parameters.AddWithValue("p_LFAREQUESTID", DBNull.Value);
                }

                if (row.IsSENDERUSERIDNull() == false)
                {
                    cmdLFARequestHistory.Parameters.AddWithValue("p_SENDERUSERID", row.SENDERUSERID);
                }
                else
                {
                    cmdLFARequestHistory.Parameters.AddWithValue("p_SENDERUSERID", DBNull.Value);
                }

                if (row.IsReceivedEmpIDNull() == false)
                {
                    cmdLFARequestHistory.Parameters.AddWithValue("p_RECEIVEDEMPID", row.ReceivedEmpID);
                }
                else
                {
                    cmdLFARequestHistory.Parameters.AddWithValue("p_RECEIVEDEMPID", DBNull.Value);
                }

                if (row.IsACTIVITIESIDNull() == false)
                {
                    cmdLFARequestHistory.Parameters.AddWithValue("p_ACTIVITIESID", row.ACTIVITIESID);
                }
                else
                {
                    cmdLFARequestHistory.Parameters.AddWithValue("p_ACTIVITIESID", DBNull.Value);
                }

                if (row.IsLFAHISTORYREMARKSNull() == false)
                {
                    cmdLFARequestHistory.Parameters.AddWithValue("p_LFAHISTORYREMARKS", row.LFAHISTORYREMARKS);
                }
                else
                {
                    cmdLFARequestHistory.Parameters.AddWithValue("p_LFAHISTORYREMARKS", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdLFARequestHistory, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LFA_REQUEST_APPROVAL_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                #endregion

                cmdLFARequestHistory.Parameters.Clear();
            }

            #region Send Email...
            if (boolDS.DataBools[0].BoolValue) //If NotifyByMail is Checked
            {
                URL = stringDS.DataStrings[0].StringValue;
                foreach (LFARequestDS.EmailPacketRow emailRow in lfaRequestDS.EmailPacket.Rows)
                {
                    returnMessage = "";

                    //Kaysar, 03-Nov-2011
                    URL += "?pVal=lfa";

                    messageBody = "<b>Dispatcher: </b>" + emailRow.SenderEmpName + " [" + emailRow.SenderEmpCode + "]" + "<br><br>";
                    messageBody += "<b>Applicant: </b>" + emailRow.ApplicantEmpName + " [" + emailRow.ApplicantEmpCode + "]<br>";
                    messageBody += "<b>Leave Applied: </b>" + emailRow.LeaveRange;
                    messageBody += "<br><br><br>";
                    messageBody += "Click the following link: ";
                    messageBody += "<br>";
                    messageBody += "<a href='" + URL + "'>" + URL + "</a>";

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, emailRow.FromEmailAddress, emailRow.EmailAddress, emailRow.CcEmailAddress, emailRow.MailingSubject, messageBody, emailRow.SenderEmpName);
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, emailRow.FromEmailAddress, emailRow.EmailAddress, emailRow.CcEmailAddress, emailRow.MailingSubject, messageBody, emailRow.SenderEmpName);
                    }
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        #endregion

        private DataSet _getEmployeeLFA(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetEmployeeLFA");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["IsGet"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["ActivitiesID"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["Year"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["Month"].Value = integerDS.DataIntegers[3].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            LFARequestDS requestDS = new LFARequestDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requestDS, requestDS.LFASummary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LFA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            requestDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(requestDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getEmployeeLFAByEmpTypeSite(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeLFAByEmpTypeSite");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["IsGet"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["ActivitiesID"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["Year"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["Month"].Value = integerDS.DataIntegers[3].IntegerValue;
            cmd.Parameters["EmpType"].Value = integerDS.DataIntegers[4].IntegerValue;
            cmd.Parameters["PayrollSite"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            LFARequestDS requestDS = new LFARequestDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requestDS, requestDS.LFASummary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LFA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            requestDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(requestDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLFARequest(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLFARequestList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["ActivitiesID"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["Year"].Value = integerDS.DataIntegers[2].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            LFARequestDS requestDS = new LFARequestDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requestDS, requestDS.LFASummary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LFA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            requestDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(requestDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLFARequestListBySalaryYear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLFARequestListBySalaryYear");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["Year"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            LFARequestDS requestDS = new LFARequestDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requestDS, requestDS.LFASummary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LFA_REQUEST_LIST_BY_YEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            requestDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(requestDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetLFASummaryReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLFASummaryReport");
            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            bool bError = false;
            int nRowAffected = -1;
            #region Data Load (Jarif 10-Sep-11 (Update))

            LFARequestDS LFASummaryDS = new LFARequestDS();
            nRowAffected = ADOController.Instance.Fill(adapter, LFASummaryDS, LFASummaryDS.LFASummaryRpt.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LFA_SUMMARY_REPORT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            LFASummaryDS.AcceptChanges();
            #endregion
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(LFASummaryDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;


        }

        private DataSet _getEmployeeDetailsForLFA(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeDetailsForLFA");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            LFARequestDS LFA = new LFARequestDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, LFA, LFA.EmplyeeDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_DETAILS_FOR_LFA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            LFA.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(LFA);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createLFAAssign(DataSet inputDS)
        {
            string messageBody = "", returnMessage = "";
            ErrorDS errDS = new ErrorDS();
            LFARequestDS lfaRequestDS = new LFARequestDS();
            DBConnectionDS connDS = new DBConnectionDS();

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LFA_ASSIGN_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            lfaRequestDS.Merge(inputDS.Tables[lfaRequestDS.LFARequest.TableName], false, MissingSchemaAction.Error);
            lfaRequestDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (LFARequestDS.LFARequestRow row in lfaRequestDS.LFARequest.Rows)
            {
                long lfaRequestID = IDGenerator.GetNextGenericPK();
                if (lfaRequestID == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_LFARequestID", lfaRequestID);
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EMPLOYEEID);
                cmd.Parameters.AddWithValue("p_LFAAmount", row.LFAAMOUNT);
                cmd.Parameters.AddWithValue("p_LFAYear", row.LFAYEAR);
                cmd.Parameters.AddWithValue("p_PayMonth", row.PAYMONTH);
                cmd.Parameters.AddWithValue("p_LFARequestRemarks", "");

                long lfaHistoryID = IDGenerator.GetNextGenericPK();
                if (lfaHistoryID == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_LFAHistoryID", lfaHistoryID);
                cmd.Parameters.AddWithValue("p_SenderUserID", row.SenderUserID);
                cmd.Parameters.AddWithValue("p_ReceivedUserID", row.ReceivedUserID);
                cmd.Parameters.AddWithValue("p_ActivitiesID", Convert.ToInt32(LFARequestActivities.Approve));
                cmd.Parameters.AddWithValue("p_LFAHistoryRemarks", "");
                if (!row.IsLEAVEREQUESTIDNull())
                {
                    cmd.Parameters.AddWithValue("p_LEAVEREQUESTID", row.LEAVEREQUESTID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LEAVEREQUESTID", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;

                if (row.IsNotificationByMail)
                {
                    returnMessage = "";

                    //Kaysar, 03-Nov-2011
                    row.Url += "?pVal=lfa";

                    messageBody = "<b>Dispatcher: </b>" + row.SenderEmpName + " [" + row.SenderEmpCode + "]" + "<br><br>";
                    messageBody += "<b>User: </b>" + row.ReceivedEmpName + "<br>";
                    messageBody += "<br><br><br>";
                    messageBody += "Click the following link: ";
                    messageBody += "<br>";
                    messageBody += "<a href='" + row.Url + "'>" + row.Url + "</a>";

                    Mail.Mail mail = new Mail.Mail();
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, row.FromEmailAddress, row.EmailAddress, row.CcEmailAddress, row.MailingSubject, messageBody, row.SenderEmpName);
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, row.FromEmailAddress, row.EmailAddress, row.CcEmailAddress, row.MailingSubject, messageBody, row.SenderEmpName);
                    }

                    if (returnMessage == "Email successfully sent.")
                    {
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    }
                    else
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_LFA_ASSIGN_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                else
                {
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                }

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LFA_ASSIGN_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getLFAAnalysisReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            LFARequestDS lfaDS = new LFARequestDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLFAAnalysisData");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            //take the dates into the parameters of the command
            if (!stringDS.DataStrings[0].IsStringValueNull())
            {
                cmd.Parameters["MonthYearDate"].Value = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            //------------------------------get employee detail------------------------
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, lfaDS, lfaDS.LFAAnalysisDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LFA_ANALYSIS_RPT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            lfaDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(lfaDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

    }
}
