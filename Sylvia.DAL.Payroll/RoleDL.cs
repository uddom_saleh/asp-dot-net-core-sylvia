/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: April 23, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using Sylvia.Common;
using Sylvia.DAL.Database;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class RoleDL
    {
        public RoleDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_SEC_ROLE_ADD:
                    return _createRole(inputDS);
                case ActionID.ACTION_SEC_ROLE_UPD:
                    return this._updateRole(inputDS);
                case ActionID.NA_ACTION_Q_ALL_ROLE:
                    return _getRoleList(inputDS);
                case ActionID.ACTION_DOES_ROLE_EXIST:
                    return this._doesRoleExist(inputDS);
                case ActionID.NA_ACTION_Q_ALL_PREVILAGE:
                    return this._getPrevilageList(inputDS);
                case ActionID.NA_ACTION_Q_ALL_PREVILAGE_BY_ROLE:
                    return this._getPrevilageListByRole(inputDS);
                case ActionID.NA_ACTION_GET_USERROLE:
                    return this._getUserRole(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }
        private DataSet _doesRoleExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesRoleExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Name"].Value = stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createRolePrevilage(DataSet inputDS, long nLoginId)
        {
            DataSet responseDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            PrevilageDS prevDS = new PrevilageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateRolePrevilage");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();
            prevDS.Merge(inputDS.Tables[prevDS.Previlages.TableName], false, MissingSchemaAction.Error);
            prevDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (PrevilageDS.Previlage prev in prevDS.Previlages)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                    return UtilDL.GetDBOperationFailed();

                cmd.Parameters["RolePrevilageId"].Value = genPK;
                cmd.Parameters["RoleId"].Value = nLoginId;

                if (prev.IsActionIdNull() == false)
                {
                    cmd.Parameters["ActionId"].Value = prev.ActionId;
                }
                else
                {
                    cmd.Parameters["ActionId"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEC_USER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _createRole(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            RoleDS roleDS = new RoleDS();
            PrevilageDS prevDS = new PrevilageDS();
            DataSet responseDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateRole");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();

            roleDS.Merge(inputDS.Tables[roleDS.Roles.TableName], false, MissingSchemaAction.Error);
            roleDS.AcceptChanges();
            prevDS.Merge(inputDS.Tables[prevDS.Previlages.TableName], false, MissingSchemaAction.Error);
            prevDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            foreach (RoleDS.Role role in roleDS.Roles)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                    return UtilDL.GetDBOperationFailed();

                cmd.Parameters["RoleId"].Value = genPK;


                if (role.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = role.Name;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }
                if (role.IsDescriptionNull() == false)
                {
                    cmd.Parameters["Description"].Value = role.Description;
                }
                else
                {
                    cmd.Parameters["Description"].Value = null;
                }
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                responseDS = _createRolePrevilage(inputDS, genPK);
                errDS.Clear();
                errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                errDS.AcceptChanges();
                if (errDS.Errors.Count > 0)
                {
                    bError = true;

                }

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEC_ROLE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateRole(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet responseDS = new DataSet();
            RoleDS roleDS = new RoleDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateRole");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();

            roleDS.Merge(inputDS.Tables[roleDS.Roles.TableName], false, MissingSchemaAction.Error);
            roleDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (RoleDS.Role role in roleDS.Roles)
            {
                if (role.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = role.Name;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }

                if (role.IsDescriptionNull() == false)
                {
                    cmd.Parameters["Description"].Value = role.Description;
                }
                else
                {
                    cmd.Parameters["Description"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == false)
                {
                    responseDS = _deletePrevilage(inputDS, role.Name);
                    errDS.Clear();
                    errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                    errDS.AcceptChanges();
                    if (errDS.Errors.Count > 0)
                    {
                        bError = true;

                    }
                }

                if (bError == false)
                {

                    responseDS = _createRolePrevilage(inputDS, UtilDL.GetRoleId(connDS, role.Name));
                    errDS.Clear();
                    errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                    errDS.AcceptChanges();
                    if (errDS.Errors.Count > 0)
                    {
                        bError = true;

                    }
                }
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEC_ROLE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deletePrevilage(DataSet inputDS, string sName)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeletePrevilageFromRolePrevilage");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();

            cmd.Parameters["Name"].Value = sName;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //err.ErrorInfo1 = ActionID.ACTION_USER_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getRoleList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRoleList");//Jarif(01 Oct 11)
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRoleList_New");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(01 Oct 11)...
            #region Old...
            //RolePO rolePO = new RolePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, rolePO, rolePO.Roles.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_ROLE.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //rolePO.AcceptChanges();
            //RoleDS roleDS = new RoleDS();
            //if (rolePO.Roles.Count > 0)
            //{
            //    foreach (RolePO.Role poRole in rolePO.Roles)
            //    {
            //        RoleDS.Role role = roleDS.Roles.NewRole();
            //        if (poRole.IsNameNull() == false)
            //        {
            //            role.Name = poRole.Name;
            //        }
            //        if (poRole.IsDescriptionNull() == false)
            //        {
            //            role.Description = poRole.Description;
            //        }
            //        roleDS.Roles.AddRole(role);
            //        roleDS.AcceptChanges();
            //    }
            //}
            #endregion

            RoleDS roleDS = new RoleDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, roleDS, roleDS.Roles.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_ROLE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            roleDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(roleDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _getPrevilageListByRole(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPrevilageListByRole");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                string st = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["Name"].Value = stringDS.DataStrings[0].StringValue;

            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(01 Oct 11)...
            #region Old...
            //PrevilagePO prevPO = new PrevilagePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, prevPO, prevPO.Previlages.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_ROLE_BY_USER.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //prevPO.AcceptChanges();
            //PrevilageDS prevDS = new PrevilageDS();
            //if (prevPO.Previlages.Count > 0)
            //{
            //    foreach (PrevilagePO.Previlage poPrevilage in prevPO.Previlages)
            //    {
            //        PrevilageDS.Previlage prev = prevDS.Previlages.NewPrevilage();
            //        if (poPrevilage.IsActionIdNull() == false)
            //        {
            //            prev.ActionId = poPrevilage.ActionId;
            //        }
            //        if (poPrevilage.IsFriendlyNameNull() == false)
            //        {
            //            prev.FriendlyName = poPrevilage.FriendlyName;
            //        }
            //        prevDS.Previlages.AddPrevilage(prev);
            //        prevDS.AcceptChanges();
            //    }
            //}
            #endregion

            PrevilageDS prevDS = new PrevilageDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prevDS, prevDS.Previlages.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_ROLE_BY_USER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            prevDS.AcceptChanges();
            #endregion


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prevDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _getPrevilageList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPrevilageList");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(01 Oct 11)...
            #region Old...
            //PrevilagePO prevPO = new PrevilagePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, prevPO, prevPO.Previlages.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_PREVILAGE.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //prevPO.AcceptChanges();
            //PrevilageDS prevDS = new PrevilageDS();
            //if (prevPO.Previlages.Count > 0)
            //{
            //    foreach (PrevilagePO.Previlage poPrevilage in prevPO.Previlages)
            //    {
            //        PrevilageDS.Previlage prev = prevDS.Previlages.NewPrevilage();
            //        if (poPrevilage.IsActionIdNull() == false)
            //        {
            //            prev.ActionId = poPrevilage.ActionId;
            //        }
            //        if (poPrevilage.IsFriendlyNameNull() == false)
            //        {
            //            prev.FriendlyName = poPrevilage.FriendlyName;
            //        }
            //        prevDS.Previlages.AddPrevilage(prev);
            //        prevDS.AcceptChanges();
            //    }
            //}
            #endregion

            PrevilageDS prevDS = new PrevilageDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, prevDS, prevDS.Previlages.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_PREVILAGE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            prevDS.AcceptChanges();
            #endregion


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prevDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _getUserRole(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetUserRole");  //for current fiscal year's data   

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            if (StringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LoginID"].Value = StringDS.DataStrings[0].StringValue;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            RoleDS roleDS = new RoleDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, roleDS, roleDS.Roles.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_USERROLE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            roleDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(roleDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

    }
}
