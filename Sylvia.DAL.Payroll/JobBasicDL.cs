using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    public class JobBasicDL
    {
        public JobBasicDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                #region Job Specification
                case ActionID.ACTION_JOB_SPECIFICATION_CREATE:
                    return _createJobSpecification(inputDS);
                case ActionID.ACTION_JOB_SPECIFICATION_UPD:
                    return this._updateJobSpecification(inputDS);
                case ActionID.ACTION_JOB_SPECIFICATION_DEL:
                    return this._deleteJobSpecification(inputDS);
                case ActionID.NA_ACTION_JOB_SPECIFICATION_LIST:
                    return _getJobSpecificationList(inputDS);
                case ActionID.NA_ACTION_JOB_SPECIFICATION_LIST_ALL:
                    return _getJobSpecificationAll(inputDS);
                case ActionID.ACTION_JOB_SPECIFICATION_EXIST:
                    return this._doesJobSpecificationExist(inputDS);
                #endregion

                #region Job Title
                case ActionID.ACTION_JOB_TITLE_CREATE:
                    return _createJobTitle(inputDS);
                case ActionID.ACTION_JOB_TITLE_UPD:
                    return this._updateJobTitle(inputDS);
                case ActionID.ACTION_JOB_TITLE_DEL:
                    return this._deleteJobTitle(inputDS);
                case ActionID.NA_ACTION_JOB_TITLE_LIST:
                    return _getJobTitleList(inputDS);
                case ActionID.NA_ACTION_JOB_TITLE_LIST_ALL:
                    return _getJobTitleAll(inputDS);
                case ActionID.ACTION_JOB_TITLE_EXIST:
                    return this._doesJobTitleExist(inputDS);
                #endregion

                #region Job Vacancy
                case ActionID.ACTION_JOB_VACANCY_CREATE:
                    return _createJobVacancy(inputDS);
                case ActionID.ACTION_JOB_VACANCY_UPD:
                    return this._updateJobVacancy(inputDS);
                case ActionID.ACTION_JOB_VACANCY_DEL:
                    return this._deleteJobVacancy(inputDS);
                case ActionID.NA_ACTION_JOB_VACANCY_LIST:
                    return _getJobVacancyList(inputDS);
                case ActionID.NA_ACTION_JOB_VACANCY_LIST_ALL:
                    return _getJobVacancyAll(inputDS);
                case ActionID.ACTION_JOB_VACANCY_EXIST:
                    return this._doesJobVacancyExist(inputDS);
                #endregion

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }

        #region Job Specification
        private DataSet _createJobSpecification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobBasicDS jobBasicDS = new JobBasicDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_SPECIFICATION_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            jobBasicDS.Merge(inputDS.Tables[jobBasicDS.JobSpecification.TableName], false, MissingSchemaAction.Error);
            jobBasicDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (JobBasicDS.JobSpecificationRow row in jobBasicDS.JobSpecification.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_SPECIFICATIONID", genPK);

                if (row.IsSPECIFICATIONCODENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONCODE", row.SPECIFICATIONCODE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONCODE", DBNull.Value);
                }

                if (row.IsSPECIFICATIONNAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONNAME", row.SPECIFICATIONNAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONNAME", DBNull.Value);
                }

                if (row.IsSPECIFICATIONDESCRIPTTIONNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONDESCRIPTTION", row.SPECIFICATIONDESCRIPTTION);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONDESCRIPTTION", "");
                }

                if (row.IsSPECIFICATIONDUTIESNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONDUTIES", row.SPECIFICATIONDUTIES);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONDUTIES", "");
                }

                if (!row.IsIsEquivalentDegreeNull())
                {
                    cmd.Parameters.AddWithValue("p_IsEquivalentDegree", row.IsEquivalentDegree);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_IsEquivalentDegree", false);
                }

                #region WALI :: 12-Jun-2014
                if (row.IsDegreeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DegreeID", row.DegreeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DegreeID", DBNull.Value);
                }


                if (row.IsExperienceYearNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ExperienceYear", row.ExperienceYear);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ExperienceYear", DBNull.Value);
                }
                #endregion

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_SPECIFICATION_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateJobSpecification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobBasicDS jobBasicDS = new JobBasicDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_SPECIFICATION_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            jobBasicDS.Merge(inputDS.Tables[jobBasicDS.JobSpecification.TableName], false, MissingSchemaAction.Error);
            jobBasicDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (JobBasicDS.JobSpecificationRow row in jobBasicDS.JobSpecification.Rows)
            {
                cmd.Parameters.AddWithValue("p_SPECIFICATIONCODE", row.SPECIFICATIONCODE);
                                
                if (row.IsSPECIFICATIONNAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONNAME", row.SPECIFICATIONNAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONNAME", DBNull.Value);
                }

                if (row.IsSPECIFICATIONDESCRIPTTIONNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONDESCRIPTTION", row.SPECIFICATIONDESCRIPTTION);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONDESCRIPTTION", DBNull.Value);
                }

                if (row.IsSPECIFICATIONDUTIESNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONDUTIES", row.SPECIFICATIONDUTIES);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SPECIFICATIONDUTIES", DBNull.Value);
                }
                if (!row.IsIsEquivalentDegreeNull())
                {
                    cmd.Parameters.AddWithValue("p_IsEquivalentDegree", row.IsEquivalentDegree);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_IsEquivalentDegree", false);
                }

                #region WALI :: 12-Jun-2014
                if (row.IsDegreeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DegreeID", row.DegreeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DegreeID", DBNull.Value);
                }

                if (row.IsExperienceYearNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ExperienceYear", row.ExperienceYear);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ExperienceYear", DBNull.Value);
                }
                #endregion

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_SPECIFICATION_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteJobSpecification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_SPECIFICATION_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_SPECIFICATIONCODE", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_SPECIFICATION_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getJobSpecificationList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetJobSpecificationList");
            cmd.Parameters["Code"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            JobBasicPO jobBasicPO = new JobBasicPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobBasicPO, jobBasicPO.JobSpecification.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_JOB_SPECIFICATION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            jobBasicPO.AcceptChanges();

            JobBasicDS jobBasicDS = new JobBasicDS();
            if (jobBasicPO.JobSpecification.Count > 0)
            {
                foreach (JobBasicPO.JobSpecificationRow poRow in jobBasicPO.JobSpecification.Rows)
                {
                    JobBasicDS.JobSpecificationRow row = jobBasicDS.JobSpecification.NewJobSpecificationRow();

                    if (poRow.IsSPECIFICATIONIDNull() == false)
                    {
                        row.SPECIFICATIONID = poRow.SPECIFICATIONID;
                    }
                    if (poRow.IsSPECIFICATIONCODENull() == false)
                    {
                        row.SPECIFICATIONCODE = poRow.SPECIFICATIONCODE;
                    }
                    if (poRow.IsSPECIFICATIONNAMENull() == false)
                    {
                        row.SPECIFICATIONNAME = poRow.SPECIFICATIONNAME;
                    }
                    if (poRow.IsSPECIFICATIONDESCRIPTTIONNull() == false)
                    {
                        row.SPECIFICATIONDESCRIPTTION = poRow.SPECIFICATIONDESCRIPTTION;
                    }
                    if (poRow.IsSPECIFICATIONDUTIESNull() == false)
                    {
                        row.SPECIFICATIONDUTIES = poRow.SPECIFICATIONDUTIES;
                    }
                   
                    jobBasicDS.JobSpecification.AddJobSpecificationRow(row);
                    jobBasicDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(jobBasicDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getJobSpecificationAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetJobSpecificationAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            JobBasicDS jobBasicDS = new JobBasicDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobBasicDS, jobBasicDS.JobSpecification.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_JOB_SPECIFICATION_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            jobBasicDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(jobBasicDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _doesJobSpecificationExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesJobSpecificationExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_SPECIFICATION_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Job Title
        private DataSet _createJobTitle(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobBasicDS jobBasicDS = new JobBasicDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_TITLE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            jobBasicDS.Merge(inputDS.Tables[jobBasicDS.JobTitle.TableName], false, MissingSchemaAction.Error);
            jobBasicDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (JobBasicDS.JobTitleRow row in jobBasicDS.JobTitle.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_TITLEID", genPK);

                if (row.IsTITLECODENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_TITLECODE", row.TITLECODE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_TITLECODE", DBNull.Value);
                }

                if (row.IsTITLENAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_TITLENAME", row.TITLENAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_TITLENAME", DBNull.Value);
                }

                if (row.IsTITLEDESCRIPTIONNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_TITLEDESCRIPTION", row.TITLEDESCRIPTION);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_TITLEDESCRIPTION", DBNull.Value);
                }

                if (row.IsTITLECOMMENTSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_TITLECOMMENTS", row.TITLECOMMENTS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_TITLECOMMENTS", DBNull.Value);
                }

                if (row.IsJOBSPECIFICATIONCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_JOBSPECIFICATIONCode", row.JOBSPECIFICATIONCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_JOBSPECIFICATIONCode", DBNull.Value);
                }

                if (row.IsGRADECodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_GRADECode", row.GRADECode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_GRADECode", DBNull.Value);
                }

                if (row.IsDEPARTMENTCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DEPARTMENTCode", row.DEPARTMENTCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DEPARTMENTCode", DBNull.Value);
                }

                if (row.IsDESIGNATIONCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DESIGNATIONCode", row.DESIGNATIONCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DESIGNATIONCode", DBNull.Value);
                }

                if (row.IsEMPLOYEETYPEPIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EMPLOYEETYPEPID", row.EMPLOYEETYPEPID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EMPLOYEETYPEPID", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_TITLE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateJobTitle(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobBasicDS jobBasicDS = new JobBasicDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_TITLE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            jobBasicDS.Merge(inputDS.Tables[jobBasicDS.JobTitle.TableName], false, MissingSchemaAction.Error);
            jobBasicDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (JobBasicDS.JobTitleRow row in jobBasicDS.JobTitle.Rows)
            {
                cmd.Parameters.AddWithValue("p_TITLECODE", row.TITLECODE);
                
                if (row.IsTITLENAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_TITLENAME", row.TITLENAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_TITLENAME", DBNull.Value);
                }

                if (row.IsTITLEDESCRIPTIONNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_TITLEDESCRIPTION", row.TITLEDESCRIPTION);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_TITLEDESCRIPTION", DBNull.Value);
                }

                if (row.IsTITLECOMMENTSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_TITLECOMMENTS", row.TITLECOMMENTS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_TITLECOMMENTS", DBNull.Value);
                }

                if (row.IsJOBSPECIFICATIONCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_JOBSPECIFICATIONCode", row.JOBSPECIFICATIONCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_JOBSPECIFICATIONCode", DBNull.Value);
                }

                if (row.IsGRADECodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_GRADECode", row.GRADECode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_GRADECode", DBNull.Value);
                }

                if (row.IsDEPARTMENTCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DEPARTMENTCode", row.DEPARTMENTCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DEPARTMENTCode", DBNull.Value);
                }

                if (row.IsDESIGNATIONCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DESIGNATIONCode", row.DESIGNATIONCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DESIGNATIONCode", DBNull.Value);
                }

                if (row.IsEMPLOYEETYPEPIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EMPLOYEETYPEPID", row.EMPLOYEETYPEPID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EMPLOYEETYPEPID", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_TITLE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteJobTitle(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_TITLE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_TITLECODE", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_TITLE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getJobTitleList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetJobTitleList");
            cmd.Parameters["Code"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            JobBasicPO jobBasicPO = new JobBasicPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobBasicPO, jobBasicPO.JobTitle.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_JOB_TITLE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            jobBasicPO.AcceptChanges();

            JobBasicDS jobBasicDS = new JobBasicDS();
            if (jobBasicPO.JobTitle.Count > 0)
            {
                foreach (JobBasicPO.JobTitleRow poRow in jobBasicPO.JobTitle.Rows)
                {
                    JobBasicDS.JobTitleRow row = jobBasicDS.JobTitle.NewJobTitleRow();

                    if (poRow.IsTITLEIDNull() == false)
                    {
                        row.TITLEID = poRow.TITLEID;
                    }
                    if (poRow.IsTITLECODENull() == false)
                    {
                        row.TITLECODE = poRow.TITLECODE;
                    }
                    if (poRow.IsTITLENAMENull() == false)
                    {
                        row.TITLENAME = poRow.TITLENAME;
                    }
                    if (poRow.IsTITLEDESCRIPTIONNull() == false)
                    {
                        row.TITLEDESCRIPTION = poRow.TITLEDESCRIPTION;
                    }
                    if (poRow.IsTITLECOMMENTSNull() == false)
                    {
                        row.TITLECOMMENTS = poRow.TITLECOMMENTS;
                    }
                    if (poRow.IsJOBSPECIFICATIONIDNull() == false)
                    {
                        row.JOBSPECIFICATIONID = poRow.JOBSPECIFICATIONID;
                    }
                    if (poRow.IsGRADEIDNull() == false)
                    {
                        row.GRADEID = poRow.GRADEID;
                    }
                    if (poRow.IsDEPARTMENTIDNull() == false)
                    {
                        row.DEPARTMENTID = poRow.DEPARTMENTID;
                    }
                    if (poRow.IsDESIGNATIONIDNull() == false)
                    {
                        row.DESIGNATIONID = poRow.DESIGNATIONID;
                    }
                    if (poRow.IsEMPLOYEETYPEPIDNull() == false)
                    {
                        row.EMPLOYEETYPEPID = poRow.EMPLOYEETYPEPID;
                    }
                    if (poRow.IsJOBSPECIFICATIONCodeNull() == false)
                    {
                        row.JOBSPECIFICATIONCode = poRow.JOBSPECIFICATIONCode;
                    }
                    if (poRow.IsGRADECodeNull() == false)
                    {
                        row.GRADECode = poRow.GRADECode;
                    }
                    if (poRow.IsDEPARTMENTCodeNull() == false)
                    {
                        row.DEPARTMENTCode = poRow.DEPARTMENTCode;
                    }
                    if (poRow.IsDESIGNATIONCodeNull() == false)
                    {
                        row.DESIGNATIONCode = poRow.DESIGNATIONCode;
                    }

                    jobBasicDS.JobTitle.AddJobTitleRow(row);
                    jobBasicDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(jobBasicDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getJobTitleAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            //DataStringDS StringDS = new DataStringDS();
            //StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //StringDS.AcceptChanges();

            //string activeStatus = StringDS.DataStrings[0].StringValue;
            
            cmd = DBCommandProvider.GetDBCommand("GetJobTitleAll");

            //if (activeStatus.Equals("All"))
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetJobSpecificationAll");
            //}
            //else
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetLmsActiveLeaveTypeList");
            //    if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
            //    else cmd.Parameters["IsActive"].Value = (object)false;
            //}

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            JobBasicPO jobBasicPO = new JobBasicPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobBasicPO, jobBasicPO.JobTitle.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_JOB_TITLE_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            jobBasicPO.AcceptChanges();

            JobBasicDS jobBasicDS = new JobBasicDS();
            if (jobBasicPO.JobTitle.Count > 0)
            {
                foreach (JobBasicPO.JobTitleRow poRow in jobBasicPO.JobTitle.Rows)
                {
                    JobBasicDS.JobTitleRow row = jobBasicDS.JobTitle.NewJobTitleRow();

                    if (poRow.IsTITLEIDNull() == false)
                    {
                        row.TITLEID = poRow.TITLEID;
                    }
                    if (poRow.IsTITLECODENull() == false)
                    {
                        row.TITLECODE = poRow.TITLECODE;
                    }
                    if (poRow.IsTITLENAMENull() == false)
                    {
                        row.TITLENAME = poRow.TITLENAME;
                    }
                    if (poRow.IsTITLEDESCRIPTIONNull() == false)
                    {
                        row.TITLEDESCRIPTION = poRow.TITLEDESCRIPTION;
                    }
                    if (poRow.IsTITLECOMMENTSNull() == false)
                    {
                        row.TITLECOMMENTS = poRow.TITLECOMMENTS;
                    }
                    if (poRow.IsJOBSPECIFICATIONIDNull() == false)
                    {
                        row.JOBSPECIFICATIONID = poRow.JOBSPECIFICATIONID;
                    }
                    if (poRow.IsGRADEIDNull() == false)
                    {
                        row.GRADEID = poRow.GRADEID;
                    }
                    if (poRow.IsDEPARTMENTIDNull() == false)
                    {
                        row.DEPARTMENTID = poRow.DEPARTMENTID;
                    }
                    if (poRow.IsDESIGNATIONIDNull() == false)
                    {
                        row.DESIGNATIONID = poRow.DESIGNATIONID;
                    }
                    if (poRow.IsEMPLOYEETYPEPIDNull() == false)
                    {
                        row.EMPLOYEETYPEPID = poRow.EMPLOYEETYPEPID;
                    }
                    if (poRow.IsJOBSPECIFICATIONCodeNull() == false)
                    {
                        row.JOBSPECIFICATIONCode = poRow.JOBSPECIFICATIONCode;
                    }
                    if (poRow.IsGRADECodeNull() == false)
                    {
                        row.GRADECode = poRow.GRADECode;
                    }
                    if (poRow.IsDEPARTMENTCodeNull() == false)
                    {
                        row.DEPARTMENTCode = poRow.DEPARTMENTCode;
                    }
                    if (poRow.IsDESIGNATIONCodeNull() == false)
                    {
                        row.DESIGNATIONCode = poRow.DESIGNATIONCode;
                    }

                    jobBasicDS.JobTitle.AddJobTitleRow(row);
                    jobBasicDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(jobBasicDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _doesJobTitleExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesJobTitleExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_TITLE_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Job Vacancy
        private DataSet _createJobVacancy(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobBasicDS jobBasicDS = new JobBasicDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_VACANCY_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            jobBasicDS.Merge(inputDS.Tables[jobBasicDS.JobVacancy.TableName], false, MissingSchemaAction.Error);
            jobBasicDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (JobBasicDS.JobVacancyRow row in jobBasicDS.JobVacancy.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_VACANCYID", genPK);

                if (row.IsJOBTITLEIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_JOBTITLEID", row.JOBTITLEID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_JOBTITLEID", DBNull.Value);
                }

                if (row.IsEmployeeCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EmpCode", row.EmployeeCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EmpCode", DBNull.Value);
                }

                if (row.IsVACANCYDESCRIPTIONNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_VACANCYDESCRIPTION", row.VACANCYDESCRIPTION);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_VACANCYDESCRIPTION", DBNull.Value);
                }

                if (row.IsVACANCYISACTIVENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_VACANCYISACTIVE", row.VACANCYISACTIVE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_VACANCYISACTIVE", DBNull.Value);
                }
                
                if (row.IsJOBTRACKINGIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_JOBTRACKINGID", row.JOBTRACKINGID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_JOBTRACKINGID", DBNull.Value);
                }

                if (row.IsNOOFVACANCYNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_NOOFVACANCY", row.NOOFVACANCY);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_NOOFVACANCY", DBNull.Value);
                }

                if (row.IsLASTDATEOFAPPLICATIONNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LASTDATEOFAPPLICATION", row.LASTDATEOFAPPLICATION);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LASTDATEOFAPPLICATION", DBNull.Value);
                }

                #region WALI :: 08-Jun-2015
                if (!row.IsAgeMarginDateNull()) cmd.Parameters.AddWithValue("p_AgeMarginDate", row.AgeMarginDate);
                else cmd.Parameters.AddWithValue("p_AgeMarginDate", DBNull.Value);

                if (!row.IsAgeMargin_MinNull()) cmd.Parameters.AddWithValue("p_AgeMargin_Min", row.AgeMargin_Min);
                else cmd.Parameters.AddWithValue("p_AgeMargin_Min", DBNull.Value);

                if (!row.IsAgeMargin_MaxNull()) cmd.Parameters.AddWithValue("p_AgeMargin_Max", row.AgeMargin_Max);
                else cmd.Parameters.AddWithValue("p_AgeMargin_Max", DBNull.Value);

                if (!row.IsEvaluationMarkNull()) cmd.Parameters.AddWithValue("p_EvaluationMark", row.EvaluationMark);
                else cmd.Parameters.AddWithValue("p_EvaluationMark", DBNull.Value);
                if (!row.IsEvaluationMarkNull()) cmd.Parameters.AddWithValue("p_IsInternalPosting", row.IsInternalPosting);
                else cmd.Parameters.AddWithValue("p_IsInternalPosting", DBNull.Value);
                #endregion

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_VACANCY_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateJobVacancy(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            JobBasicDS jobBasicDS = new JobBasicDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_VACANCY_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            jobBasicDS.Merge(inputDS.Tables[jobBasicDS.JobVacancy.TableName], false, MissingSchemaAction.Error);
            jobBasicDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (JobBasicDS.JobVacancyRow row in jobBasicDS.JobVacancy.Rows)
            {
                if (row.IsJOBTITLEIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_JOBTITLEID", row.JOBTITLEID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_JOBTITLEID", DBNull.Value);
                }

                if (row.IsEmployeeCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EmpCode", row.EmployeeCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EmpCode", DBNull.Value);
                }

                if (row.IsVACANCYDESCRIPTIONNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_VACANCYDESCRIPTION", row.VACANCYDESCRIPTION);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_VACANCYDESCRIPTION", DBNull.Value);
                }

                if (row.IsVACANCYISACTIVENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_VACANCYISACTIVE", row.VACANCYISACTIVE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_VACANCYISACTIVE", DBNull.Value);
                }

                if (row.IsJOBTRACKINGIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_JOBTRACKINGID", row.JOBTRACKINGID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_JOBTRACKINGID", DBNull.Value);
                }

                if (row.IsNOOFVACANCYNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_NOOFVACANCY", row.NOOFVACANCY);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_NOOFVACANCY", DBNull.Value);
                }

                if (row.IsLASTDATEOFAPPLICATIONNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LASTDATEOFAPPLICATION", row.LASTDATEOFAPPLICATION);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LASTDATEOFAPPLICATION", DBNull.Value);
                }

                #region WALI :: 08-Jun-2015
                if (!row.IsAgeMarginDateNull()) cmd.Parameters.AddWithValue("p_AgeMarginDate", row.AgeMarginDate);
                else cmd.Parameters.AddWithValue("p_AgeMarginDate", DBNull.Value);

                if (!row.IsAgeMargin_MinNull()) cmd.Parameters.AddWithValue("p_AgeMargin_Min", row.AgeMargin_Min);
                else cmd.Parameters.AddWithValue("p_AgeMargin_Min", DBNull.Value);

                if (!row.IsAgeMargin_MaxNull()) cmd.Parameters.AddWithValue("p_AgeMargin_Max", row.AgeMargin_Max);
                else cmd.Parameters.AddWithValue("p_AgeMargin_Max", DBNull.Value);

                if (!row.IsEvaluationMarkNull()) cmd.Parameters.AddWithValue("p_EvaluationMark", row.EvaluationMark);
                else cmd.Parameters.AddWithValue("p_EvaluationMark", DBNull.Value);

                if (!row.IsISADMITAVAILABLENull()) cmd.Parameters.AddWithValue("p_IsAdmitAvailable", row.ISADMITAVAILABLE);
                else cmd.Parameters.AddWithValue("p_IsAdmitAvailable", DBNull.Value);
                if (!row.IsEvaluationMarkNull()) cmd.Parameters.AddWithValue("p_IsInternalPosting", row.IsInternalPosting);
                else cmd.Parameters.AddWithValue("p_IsInternalPosting", DBNull.Value);
                #endregion

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_JOB_VACANCY_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteJobVacancy(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_JOB_VACANCY_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_JOBTRACKINGID", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_VACANCY_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getJobVacancyList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetJobVacancyList");
            cmd.Parameters["Code"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            JobBasicPO jobBasicPO = new JobBasicPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobBasicPO, jobBasicPO.JobVacancy.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_JOB_VACANCY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            jobBasicPO.AcceptChanges();

            JobBasicDS jobBasicDS = new JobBasicDS();
            if (jobBasicPO.JobVacancy.Count > 0)
            {
                foreach (JobBasicPO.JobVacancyRow poRow in jobBasicPO.JobVacancy.Rows)
                {
                    JobBasicDS.JobVacancyRow row = jobBasicDS.JobVacancy.NewJobVacancyRow();

                    if (poRow.IsVACANCYIDNull() == false)
                    {
                        row.VACANCYID = poRow.VACANCYID;
                    }
                    if (poRow.IsJOBTITLEIDNull() == false)
                    {
                        row.JOBTITLEID = poRow.JOBTITLEID;
                    }
                    if (poRow.IsVACANCYHIRINGMANAGERIDNull() == false)
                    {
                        row.VACANCYHIRINGMANAGERID = poRow.VACANCYHIRINGMANAGERID;
                    }
                    if (poRow.IsVACANCYDESCRIPTIONNull() == false)
                    {
                        row.VACANCYDESCRIPTION = poRow.VACANCYDESCRIPTION;
                    }
                    if (poRow.IsVACANCYISACTIVENull() == false)
                    {
                        row.VACANCYISACTIVE = poRow.VACANCYISACTIVE;
                    }
                    if (poRow.IsTitleCodeNull() == false)
                    {
                        row.TitleCode = poRow.TitleCode;
                    }
                    if (poRow.IsTitleNameNull() == false)
                    {
                        row.TitleName = poRow.TitleName;
                    }
                    if (poRow.IsEmployeeCodeNull() == false)
                    {
                        row.EmployeeCode = poRow.EmployeeCode;
                    }
                    if (poRow.IsEmployeeNameNull() == false)
                    {
                        row.EmployeeName = poRow.EmployeeName;
                    }              
                    if (poRow.IsJOBTRACKINGIDNull() == false)
                    {
                        row.JOBTRACKINGID = poRow.JOBTRACKINGID;
                    }
                    if (poRow.IsNOOFVACANCYNull() == false)
                    {
                        row.NOOFVACANCY = poRow.NOOFVACANCY;
                    }
                    if (poRow.IsVACANCYCREATEDDATENull() == false)
                    {
                        row.VACANCYCREATEDDATE = poRow.VACANCYCREATEDDATE;
                    }
                    if (poRow.IsLASTDATEOFAPPLICATIONNull() == false)
                    {
                        row.LASTDATEOFAPPLICATION = poRow.LASTDATEOFAPPLICATION;
                    }

                    jobBasicDS.JobVacancy.AddJobVacancyRow(row);
                    jobBasicDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(jobBasicDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getJobVacancyAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            if (activeStatus.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetJobVacancyAll");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetActiveJobVacancyAll");
                if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
                else cmd.Parameters["IsActive"].Value = (object)false;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            //JobBasicPO jobBasicPO = new JobBasicPO();
            JobBasicDS jobBasicDS = new JobBasicDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, jobBasicDS, jobBasicDS.JobVacancy.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_JOB_VACANCY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            jobBasicDS.AcceptChanges();

            //JobBasicDS jobBasicDS = new JobBasicDS();
            //if (jobBasicPO.JobVacancy.Count > 0)
            //{
            //    foreach (JobBasicPO.JobVacancyRow poRow in jobBasicPO.JobVacancy.Rows)
            //    {
            //        JobBasicDS.JobVacancyRow row = jobBasicDS.JobVacancy.NewJobVacancyRow();

            //        if (poRow.IsVACANCYIDNull() == false)
            //        {
            //            row.VACANCYID = poRow.VACANCYID;
            //        }
            //        if (poRow.IsJOBTITLEIDNull() == false)
            //        {
            //            row.JOBTITLEID = poRow.JOBTITLEID;
            //        }
            //        if (poRow.IsVACANCYHIRINGMANAGERIDNull() == false)
            //        {
            //            row.VACANCYHIRINGMANAGERID = poRow.VACANCYHIRINGMANAGERID;
            //        }
            //        if (poRow.IsVACANCYDESCRIPTIONNull() == false)
            //        {
            //            row.VACANCYDESCRIPTION = poRow.VACANCYDESCRIPTION;
            //        }
            //        if (poRow.IsVACANCYISACTIVENull() == false)
            //        {
            //            row.VACANCYISACTIVE = poRow.VACANCYISACTIVE;
            //        }
            //        if (poRow.IsTitleCodeNull() == false)
            //        {
            //            row.TitleCode = poRow.TitleCode;
            //        }
            //        if (poRow.IsTitleNameNull() == false)
            //        {
            //            row.TitleName = poRow.TitleName;
            //        }
            //        if (poRow.IsEmployeeCodeNull() == false)
            //        {
            //            row.EmployeeCode = poRow.EmployeeCode;
            //        }
            //        if (poRow.IsEmployeeNameNull() == false)
            //        {
            //            row.EmployeeName = poRow.EmployeeName;
            //        }
            //        if (poRow.IsJOBTRACKINGIDNull() == false)
            //        {
            //            row.JOBTRACKINGID = poRow.JOBTRACKINGID;
            //        }
            //        if (poRow.IsNOOFVACANCYNull() == false)
            //        {
            //            row.NOOFVACANCY = poRow.NOOFVACANCY;
            //        }
            //        if (poRow.IsVACANCYCREATEDDATENull() == false)
            //        {
            //            row.VACANCYCREATEDDATE = poRow.VACANCYCREATEDDATE;
            //        }
            //        if (poRow.IsLASTDATEOFAPPLICATIONNull() == false)
            //        {
            //            row.LASTDATEOFAPPLICATION = poRow.LASTDATEOFAPPLICATION;
            //        }

            //        jobBasicDS.JobVacancy.AddJobVacancyRow(row);
            //        jobBasicDS.AcceptChanges();
            //    }
            //}

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(jobBasicDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _doesJobVacancyExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesJobVacancyExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["JobTrackingID"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_JOB_VACANCY_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion
    }
}
