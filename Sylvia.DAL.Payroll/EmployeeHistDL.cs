/* Compamy: Milllennium Information Solution Limited
 * Author: Km Jarif
 * Comment Date: March, 07, 2009
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for EmployeeHistDL.
    /// </summary>
    class EmployeeHistDL
    {
        public EmployeeHistDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                //For Employee History
                case ActionID.NA_ACTION_GET_EMPHIST_LIST_BYID:
                    return this._getEmployeeHist(inputDS);
                case ActionID.NA_ACTION_GET_EMPHIST_LIST_ALL:
                    return this._getEmployeeHistAll(inputDS);

                //For Spouse
                case ActionID.ACTION_SPOUSE_ADD:
                    return this._createSpouse(inputDS);
                case ActionID.NA_ACTION_GET_SPOUSE_LIST:
                    return this._getSpouseList(inputDS);
                case ActionID.ACTION_SPOUSE_DEL:
                    return this._deleteSpouse(inputDS);
                case ActionID.ACTION_SPOUSE_UPD:
                    return this._updateSpouse(inputDS);
                case ActionID.ACTION_DOES_SPOUSE_EXIST:
                    return this._doesSpouseExist(inputDS);

                //For Children
                case ActionID.ACTION_CHILDREN_ADD:
                    return this._createChildren(inputDS);
                case ActionID.NA_ACTION_GET_CHILDREN_LIST:
                    return this._getChildrenList(inputDS);
                case ActionID.ACTION_CHILDREN_DEL:
                    return this._deleteChildren(inputDS);
                case ActionID.ACTION_CHILDREN_UPD:
                    return this._updateChildren(inputDS);
                case ActionID.ACTION_DOES_CHILDREN_EXIST:
                    return this._doesChildrenExist(inputDS);

                // For Education
                case ActionID.ACTION_EDUCATION_ADD:
                    return this._createEducation(inputDS);
                case ActionID.NA_ACTION_GET_EDUCATION_LIST:
                    return this._getEducationList(inputDS);
                case ActionID.ACTION_EDUCATION_DEL:
                    return this._deleteEducation(inputDS);
                case ActionID.ACTION_EDUCATION_UPD:
                    return this._updateEducation(inputDS);
                case ActionID.ACTION_DOES_EDUCATION_EXIST:
                    return this._doesEducationExist(inputDS);

                //For Training
                case ActionID.ACTION_TRAINING_ADD:
                    return this._createTraining(inputDS);
                case ActionID.NA_ACTION_GET_TRAINING_LIST:
                    return this._getTrainingList(inputDS);
                case ActionID.ACTION_TRAINING_DEL:
                    return this._deleteTraining(inputDS);
                case ActionID.ACTION_TRAINING_UPD:
                    return this._updateTraining(inputDS);
                case ActionID.ACTION_DOES_TRAINING_EXIST:
                    return this._doesTrainingExist(inputDS);

                //For Employment
                case ActionID.ACTION_EMPLOYMENT_ADD:
                    return this._createEmployment(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYMENT_LIST:
                    return this._getEmploymentList(inputDS);
                case ActionID.ACTION_EMPLOYMENT_DEL:
                    return this._deleteEmployment(inputDS);
                case ActionID.ACTION_EMPLOYMENT_UPD:
                    return this._updateEmployment(inputDS);
                case ActionID.ACTION_DOES_EMPLOYMENT_EXIST:
                    return this._doesEmploymentExist(inputDS);

                //Fro Reference                
                case ActionID.ACTION_REFERENCE_ADD:
                    return this._createReference(inputDS);
                case ActionID.ACTION_DOES_REFERENCE_EXIST:
                    return this._doesReferenceExist(inputDS);
                case ActionID.NA_ACTION_GET_REFERENCE_LIST:
                    return this._getReferenceList(inputDS);
                case ActionID.ACTION_REFERENCE_UPD:
                    return this._updateReference(inputDS);
                case ActionID.ACTION_REFERENCE_DEL:
                    return this._deleteReference(inputDS);


                //Attachment Create
                case ActionID.ACTION_ATTACHMENT_ADD:
                    return this._createAttachment(inputDS);
                case ActionID.NA_ACTION_GET_ATTACHMENT_LIST:
                    return this._getAttachmentList(inputDS);


                //For Disciplinary  raqib 13 dec  
                case ActionID.NA_ACTION_GET_DISCIPLINARY_RECORDS:
                    return this._getDisciplinary(inputDS);
                case ActionID.ACTION_DISCIPLINARYTYPE_ADD:
                    return this._createDisciplinaryType(inputDS);
                case ActionID.ACTION_DISCIPLINARYTYPE_UPD:
                    return this._updateDisciplinaryType(inputDS);
                case ActionID.ACTION_DISCIPLINARY_ADD:
                    return this._createDisciplinary(inputDS);
                case ActionID.ACTION_DISCIPLINARY_UPD:
                    return this._updateDisciplinary(inputDS);
                case ActionID.ACTION_DISCIPLINARY_DEL:
                    return this._deleteDisciplinary(inputDS);

                //For Guarantor
                case ActionID.ACTION_GUARANTOR_ADD:
                    return this._createGuarantor(inputDS);
                case ActionID.ACTION_GUARANTOR_UPD:
                    return this.updateGuarantor(inputDS);
                case ActionID.NA_ACTION_GET_GUARANTOR_LIST:
                    return this._getGuarantor(inputDS);
                case ActionID.ACTION_GUARANTOR_DEL:
                    return this._deleteguarantor(inputDS);

                //For Degree
                case ActionID.NA_ACTION_GET_DEGREE_LIST:
                    return this._getDegreeList(inputDS);

                //For Education update
                case ActionID.NA_ACTION_GET_EDUCATION_LIST_TO_UPDATE:
                    return this._getEduListToUpdate(inputDS);


                //Competency Create
                case ActionID.ACTION_COMPETENCY_ADD:
                    return this._createCompetency(inputDS);
                case ActionID.NA_ACTION_GET_COMPETENCY_LIST:
                    return this._getCompetencyList(inputDS);
                case ActionID.NA_ACTION_GET_COMPETENCY_TYPE:
                    return this._getCompetencyTypeList(inputDS);
                case ActionID.ACTION_COMPETENCY_PROFILE_DEL:
                    return this._deleteCompetencyProfile(inputDS);
                case ActionID.NA_ACTION_GET_COMPETENCY_DETAILS:
                    return this._getCompetencyDetails(inputDS);
                case ActionID.ACTION_COMPETENCY_PROFILE_UPD:
                    return this._updateCompetencyProfile(inputDS);
                case ActionID.ACTION_COMPETENCY_TYPE_ADD:
                    return this._createCompetencyType(inputDS);
                case ActionID.ACTION_COMPETENCY_TYPE_UPDATE:
                    return this._updateCompetencyType(inputDS);

                case ActionID.ACTION_EDUCATIONBOARD_CREATE:
                    return this._createEducationBoard(inputDS);
                case ActionID.ACTION_EDUCATIONBOARD_UPDATE:
                    return this._updateEducationBoard(inputDS);
                case ActionID.ACTION_GROUP_CREATE:
                    return this._createGroup(inputDS);
                case ActionID.ACTION_GROUP_UPDATE:
                    return this._updateGroup(inputDS);
                case ActionID.ACTION_MAJOR_SUBJECT_CREATE:
                    return this._createMajorSubject(inputDS);
                case ActionID.NA_ACTION_GET_MAJOR_SUBJECT_LIST:
                    return this._getMajorSubjectList(inputDS);
                case ActionID.ACTION_MAJOR_SUBJECT_UPDATE:
                    return this._updateMajorSubject(inputDS);
                case ActionID.NA_ACTION_GET_UNIVERSITY_LIST:
                    return this._getUniversityList(inputDS);

                case ActionID.ACTION_ASINFO_ADD:
                    return this._createAuthorisedSignatory(inputDS);
                case ActionID.NA_ACTION_ASINFO_GET:
                    return this._getAuthorisedSignatoryList(inputDS);
                case ActionID.ACTION_ASINFO_UPDATE:
                    return this._updateAuthorisedSignatory(inputDS);
                case ActionID.ACTION_ASINFO_DELETE:
                    return this._deleteAuthorisedSignatory(inputDS);

                case ActionID.ACTION_UNIVERSITY_CREATE:
                    return this._createUniversity(inputDS);
                case ActionID.ACTION_UNIVERSITY_UPDATE:
                    return this._updateUniversity(inputDS);
                case ActionID.ACTION_UNIVERSITY_DEL:
                    return this._deleteUniversity(inputDS);

                // Nominee Information
                case ActionID.ACTION_NOMINEE_SAVE:
                    return this._saveNomineeInformation(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_NOMINEE_LIST:
                    return this._getEmployeeNomineeList(inputDS);

                // Authority
                case ActionID.ACTION_AUTHORITY_CREATE:
                    return this._createAuthority(inputDS);
                case ActionID.NA_ACTION_GET_AUTHORITY_LIST:
                    return this._getAuthorityList(inputDS);
                case ActionID.ACTION_AUTHORITY_UPDATE:
                    return this._updateAuthority(inputDS);
                case ActionID.ACTION_AUTHORITY_DELETE:
                    return this._deleteAuthority(inputDS);

                // Nature of Punishment
                case ActionID.NA_ACTION_GET_PUNISHMENT_NATURE_LIST:
                    return this._getPunishmentNature(inputDS);
                case ActionID.ACTION_PUNISHMENT_NATURE_CREATE:
                    return this._createPunishmentNature(inputDS);
                case ActionID.ACTION_PUNISHMENT_NATURE_UPDATE:
                    return this._updatePunishmentNature(inputDS);
                case ActionID.ACTION_PUNISHMENT_NATURE_DEL:
                    return this._deletePunishmentNature(inputDS);

                case ActionID.NA_ACTION_GET_DISCIPLINARY_TYPE_LIST:
                    return this._getDisciplinaryTypeList(inputDS);
                case ActionID.ACTION_DELETE_DISCIPLINARY_TYPE:
                    return this._deleteDisciplinaryType(inputDS);

                // Proffesional Qualification
                case ActionID.ACTION_QUALIFICATION_ADD:
                    return this._createQualification(inputDS);
                case ActionID.NA_ACTION_GET_QUALIFICATION_LIST:
                    return this._getQualificationList(inputDS);
                case ActionID.ACTION_QUALIFICATION_DEL:
                    return this._deleteQualification(inputDS);
                case ActionID.ACTION_QUALIFICATION_UPD:
                    return this._updateQualification(inputDS);

                // Service Agreement
                case ActionID.ACTION_SERVICE_AGREEMENT_ADD:
                    return this._createServiceAgreement(inputDS);
                case ActionID.NA_ACTION_GET_SERVICE_AGREEMENT_LIST:
                    return this._getServiceAgreement(inputDS);
                case ActionID.ACTION_SERVICE_AGREEMENT_DEL:
                    return this._deleteServiceAgreement(inputDS);
                case ActionID.ACTION_SERVICE_AGREEMENT_UPD:
                    return this._updateServiceAgreement(inputDS);

                // Reward and Recognition
                case ActionID.NA_ACTION_GET_REWARD_TYPE_LIST:
                    return this._getRewardType(inputDS);
                case ActionID.ACTION_REWARD_TYPE_CREATE:
                    return this._createRewardType(inputDS);
                case ActionID.ACTION_REWARD_TYPE_UPDATE:
                    return this._updateRewardType(inputDS);
                case ActionID.ACTION_REWARD_TYPE_DEL:
                    return this._deleteRewardType(inputDS);
                case ActionID.ACTION_REWARD_RECOGNITION_ADD:
                    return this._createRewardRecognition(inputDS);
                case ActionID.ACTION_REWARD_RECOGNITION_UPD:
                    return this._updateRewardRecognition(inputDS);
                case ActionID.ACTION_REWARD_RECOGNITION_DEL:
                    return this._deleteRewardRecognition(inputDS);
                case ActionID.NA_ACTION_GET_REWARD_RECOGNITION_LIST:
                    return this._getRewardRecognition(inputDS);

                // Society Membership
                case ActionID.ACTION_SOCIETY_MEMBERSHIP_ADD:
                    return this._createSocietyMember(inputDS);
                case ActionID.NA_ACTION_GET_SOCIETY_MEMBERSHIP_LIST:
                    return this._getSocietyMember(inputDS);
                case ActionID.ACTION_SOCIETY_MEMBERSHIP_DEL:
                    return this._deleteSocietyMember(inputDS);
                case ActionID.ACTION_SOCIETY_MEMBERSHIP_UPD:
                    return this._updateSocietyMember(inputDS);
                    //Qualification
                case ActionID.ACTION_QUALIFICATION_ENTRY:
                    return this._createQualifications(inputDS);
                case ActionID.ACTION_QUALIFICATION_UPDATE:
                    return this._updateQualifications(inputDS);
                case ActionID.NA_GET_QUALIFICATION_LIST:
                    return this._getQualifications(inputDS);
                case ActionID.ACTION_QUALIFICATION_DELETE:
                    return this._deleteQualifications(inputDS);

                // Educational Degree
                case ActionID.ACTION_SET_EDU_DEGREE_POSITION:
                    return this._updateDegreePosition(inputDS);
                case ActionID.ACTION_EDU_DEGREE_CREATE:
                    return this._createDegree(inputDS);
                case ActionID.ACTION_EDU_DEGREE_UPDATE:
                    return this._updateDegree(inputDS);
                //case ActionID.ACTION_EDU_DEGREE_DELETE:
                //    return this._deleteDegree(inputDS);

                //Employee Relation
                case ActionID.ACTION_RELATION_SAVE:
                    return this._createRelation(inputDS);
                case ActionID.NA_ACTION_GET_RELATION_LIST:
                    return this._getRelationList(inputDS);
                case ActionID.ACTION_RELATION_UPDATE:
                    return this._updateRelation(inputDS);
                case ActionID.ACTION_RELATION_DELETE:
                    return this._deleteRelation(inputDS);

                //Organization Type
                case ActionID.ACTION_ORGANIZATION_TYPE_SAVE:
                    return this._createOrganizationType(inputDS);
                case ActionID.NA_ACTION_GET_ORGANIZATION_TYPE_LIST:
                    return this._getOrganizationTypeList(inputDS);
                case ActionID.ACTION_ORGANIZATION_TYPE_UPDATE:
                    return this._updateOrganizationType(inputDS);
                case ActionID.ACTION_ORGANIZATION_TYPE_DELETE:
                    return this._deleteOrganizationType(inputDS);
                case ActionID.ACTION_COMMON_ANNOUNCEMENT_SUCC_ADD:
                    return this._commonAnnounceByMailSuccRecordAdd(inputDS);
                case ActionID.ACTION_ATTACHMENT_UPDATE:
                    return this.updateAttachment(inputDS);
                case ActionID.ACTION_ATTACHMENT_DELETE:
                    return this._deleteAttachment(inputDS);
                case ActionID.ACTION_RELATIVE_INFORMATION_SAVE:
                    return this._createRelativeInformation(inputDS);
                case ActionID.NA_ACTION_GET_EMP_RELATIVE_INFO:
                    return this._getRelativeInformation(inputDS);
                case ActionID.ACTION_RELATIVE_INFORMATION_DELETE:
                    return this._deleteRelativeInformation(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_PROFILE_INFO:
                    return this._getEmployeeProfileInfoForReport(inputDS);

                //Occupation                
                case ActionID.ACTION_OCCUPATION_ADD:
                    return _createOccupation(inputDS);
                case ActionID.NA_ACTION_GET_OCCUPATION_LIST:
                    return _getOccupationList(inputDS);
                case ActionID.ACTION_OCCUPATION_DELETE:
                    return _deleteOccupation(inputDS);
                case ActionID.ACTION_OCCUPATION_UPDATE:
                    return _updateOccupation(inputDS);

                //Major Topic
                case ActionID.ACTION_MAJOR_TOPICS_ADD:
                    return _createMajorTopic(inputDS);
                case ActionID.NA_ACTION_GET_MAJOR_TOPICS_LIST:
                    return _getMajorTopic(inputDS);
                case ActionID.ACTION_MAJOR_TOPICS_DELETE:
                    return _deleteMajorTopic(inputDS);
                case ActionID.ACTION_MAJOR_TOPICS_UPDATE:
                    return _updateMajorTopic(inputDS);

                // Training Organizer
                case ActionID.ACTION_TRAINING_ORGANIZER_ADD:
                    return _createTrainingOrganizer(inputDS);
                case ActionID.NA_ACTION_GET_TRAINING_ORGANIZER_LIST:
                    return _getTrainingOrganizer(inputDS);
                case ActionID.ACTION_TRAINING_ORGANIZER_DELETE:
                    return _deleteTrainingOrganizer(inputDS);
                case ActionID.ACTION_TRAINING_ORGANIZER_UPDATE:
                    return _updateTrainingOrganizer(inputDS);
                 
                    //Employee Reference
                case ActionID.ACTION_REFRENCE_EMPLOYEE_ADD:
                    return _createEmployeeReference(inputDS);
                case ActionID.NA_ACTION_GET_REFRENCE_EMPLOYEE_LIST:
                    return _getEmployeeReferenceList(inputDS);
                case ActionID.ACTION_REFRENCE_EMPLOYEE_DELETE:
                    return _deleteEmployeeReference(inputDS);
                case ActionID.ACTION_REFRENCE_EMPLOYEE_UPDATE:
                    return _updateEmployeeReference(inputDS);

                case ActionID.NA_ACTION_GET_EMP_HISTORY_FOR_REPORT:
                    return GetEmployeeHistory_For_Report(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYMENT_FUNCTION:
                    return _GetEmployment_FunctionByID(inputDS);
                case ActionID.ACTION_EMPLOYEE_HISTORY_INFO_SEND:
                    return _createEmployeeHistory_Parking(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_HISTORY_PARKING:
                    return GetEmployeeHistory_ParkingInfo(inputDS);
                case ActionID.ACTION_EMP_RECRUITMENT_HIST_ADD:
                    return _createEmployeeHistory_FromRecruitment(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getGuarantor(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == true.ToString())
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServiceGuarantorList");
                cmd.Parameters["EmployeeCode"].Value = EmployeeCode;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetGuarantorList");

                long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
                cmd.Parameters["EmployeeId"].Value = empId;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            //EmployeeHistPO hisPO = new EmployeeHistPO();
            EmployeeHistDS hisDS = new EmployeeHistDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Guarantor.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GUARANTOR_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            hisDS.AcceptChanges();
            
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getEmployeeHist(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeHistById");

            long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
            cmd.Parameters["EmployeeId"].Value = empId;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeHistPO hisPO = new EmployeeHistPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              hisPO, hisPO.Employees.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPHIST_LIST_BYID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            hisPO.AcceptChanges();

            EmployeeHistDS hisDS = new EmployeeHistDS();

            if (hisPO.Employees.Rows.Count > 0)
            {
                foreach (EmployeeHistPO.Employee poEmp in hisPO.Employees.Rows)
                {
                    EmployeeHistDS.Employee emp = hisDS.Employees.NewEmployee();

                    emp.EmployeeId = poEmp.EmployeeId;
                    emp.EmployeeCode = poEmp.EmployeeCode;
                    emp.EmployeeName = poEmp.EmployeeName;
                    emp.DesignationName = poEmp.DesignationName;
                    emp.DepartmentName = poEmp.DepartmentName;
                    emp.JoiningDate = poEmp.JoiningDate;
                    if (poEmp.IsPFMembDateNull() == false)
                    {
                        emp.PFMembDate = poEmp.PFMembDate;
                    }
                    if (poEmp.IsLastIncrementDateNull() == false)
                    {
                        emp.LastIncrementDate = poEmp.LastIncrementDate;
                    }
                    if (poEmp.IsNoOfIncrementNull() == false)
                    {
                        emp.NoOfIncrement = poEmp.NoOfIncrement;
                    }
                    if (poEmp.IsFatherNameNull() == false)
                    {
                        emp.FatherName = poEmp.FatherName;
                    }
                    if (poEmp.IsMotherNameNull() == false)
                    {
                        emp.MotherName = poEmp.MotherName;
                    }
                    if (poEmp.IsDateOfBirthNull() == false)
                    {
                        emp.DateOfBirth = poEmp.DateOfBirth;
                    }
                    if (poEmp.IsNationalityNull() == false)
                    {
                        emp.Nationality = poEmp.Nationality;
                    }
                    if (poEmp.IsReligionNull() == false)
                    {
                        emp.Religion = poEmp.Religion;
                    }
                    if (poEmp.IsMaritalStatusNull() == false)
                    {
                        emp.MaritalStatus = poEmp.MaritalStatus;
                    }
                    emp.Gender = poEmp.Gender;
                    if (poEmp.IsBloodGroupNull() == false)
                    {
                        emp.BloodGroup = poEmp.BloodGroup;
                    }
                    if (poEmp.IsPresentAddressNull() == false)
                    {
                        emp.PresentAddress = poEmp.PresentAddress;
                    }
                    if (poEmp.IsPermanentAddressNull() == false)
                    {
                        emp.PermanentAddress = poEmp.PermanentAddress;
                    }
                    if (poEmp.IsTelephoneNull() == false)
                    {
                        emp.Telephone = poEmp.Telephone;
                    }
                    if (poEmp.IsMobileNull() == false)
                    {
                        emp.Mobile = poEmp.Mobile;
                    }
                    if (poEmp.IsEmailNull() == false)
                    {
                        emp.Email = poEmp.Email;
                    }

                    hisDS.Employees.AddEmployee(emp);
                    hisDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        private DataSet _getEmployeeHistAll(DataSet inputDS)
        {
            //ErrorDS errDS = new ErrorDS();

            //DBConnectionDS connDS = new DBConnectionDS();
            //DataLongDS longDS = new DataLongDS();

            //DataStringDS stringDS = new DataStringDS();
            //string EmployeeCode = "";

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();
            //EmployeeCode = stringDS.DataStrings[0].StringValue;

            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSpouseList");

            //long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
            //cmd.Parameters["EmployeeId"].Value = empId;

            //if (cmd == null)
            //{
            //    return UtilDL.GetCommandNotFound();
            //}

            //OleDbDataAdapter adapter = new OleDbDataAdapter();
            //adapter.SelectCommand = cmd;

            //EmployeeHistPO hisPO = new EmployeeHistPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter,
            //  hisPO, hisPO.Spouse.TableName,
            //  connDS.DBConnections[0].ConnectionID,
            //  ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.ACTION_GET_SPOUSE_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //hisPO.AcceptChanges();

            //EmployeeHistDS hisDS = new EmployeeHistDS();

            //if (hisPO.Spouse.Rows.Count > 0)
            //{
            //    foreach (EmployeeHistPO.SpouseRow poSpo in hisPO.Spouse.Rows)
            //    {
            //        EmployeeHistDS.SpouseRow spo = hisDS.Spouse.NewSpouseRow();
            //        if (poSpo.IsSpouseIdNull() == false)
            //        {
            //            spo.SpouseId = poSpo.SpouseId;
            //        }
            //        if (poSpo.IsSpouseCodeNull() == false)
            //        {
            //            spo.Code = poSpo.SpouseCode;
            //        }
            //        if (poSpo.IsEmployeeIdNull() == false)
            //        {
            //            spo.EmployeeId = poSpo.EmployeeId;
            //        }
            //        if (poSpo.IsEmployeeCodeNull() == false)
            //        {
            //            spo.EmployeeCode = poSpo.EmployeeCode;
            //        }
            //        if (poSpo.IsSpouseNameNull() == false)
            //        {
            //            spo.SpouseName = poSpo.SpouseName;
            //        }
            //        if (poSpo.IsDateOfBirthNull() == false)
            //        {
            //            spo.DateOfBirth = poSpo.DateOfBirth;
            //        }
            //        if (poSpo.IsOccupationNull() == false)
            //        {
            //            spo.Occupation = poSpo.Occupation;
            //        }
            //        if (poSpo.IsNationalityNull() == false)
            //        {
            //            spo.Nationality = poSpo.Nationality;
            //        }
            //        if (poSpo.IsRemarksNull() == false)
            //        {
            //            spo.Remarks = poSpo.Remarks;
            //        }

            //        hisDS.Spouse.AddSpouseRow(spo);
            //        hisDS.AcceptChanges();
            //    }
            //}

            //// now create the packet
            //errDS.Clear();
            //errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            //returnDS.Merge(hisDS);
            //returnDS.Merge(errDS);
            //returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _createSpouse(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Spouse.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();

            if (!histDS.Spouse[0].IsIsDataRenewalNull() && histDS.Spouse[0].IsDataRenewal) cmd = DBCommandProvider.GetDBCommand("CreateDataRenewalSpouse");
            else if (!histDS.Spouse[0].IsIsSelfServiseNull() && histDS.Spouse[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("CreateSelfServiceSpouse");
            else cmd = DBCommandProvider.GetDBCommand("CreateSpouse");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }



            foreach (EmployeeHistDS.SpouseRow spo in histDS.Spouse.Rows)
            {
                //long genPK = IDGenerator.GetNextGenericPK();
                //if (genPK == -1)
                //{
                //    UtilDL.GetDBOperationFailed();
                //}

                //cmd.Parameters["Id"].Value = (object)genPK;

                #region  Update :: WALI :: 05-Jun-2014
                //code
                //if (spo.IsCodeNull() == false)
                //{
                //    cmd.Parameters["Code"].Value = (object)spo.Code;
                //}
                //else
                //{
                //    cmd.Parameters["Code"].Value = null;
                //}
                #endregion

                //Employee Id
                if (spo.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, spo.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
                    else cmd.Parameters["EmpId"].Value = null;
                }
                else
                {
                    cmd.Parameters["EmpId"].Value = null;
                }
                //Spouse Name
                if (spo.IsSpouseNameNull() == false)
                {
                    cmd.Parameters["SpouseName"].Value = (object)spo.SpouseName;
                }
                else
                {
                    cmd.Parameters["SpouseName"].Value = null;
                }
                //Date of Birth
                if (spo.IsDateOfBirthNull() == false)
                {
                    cmd.Parameters["DateOfBirth"].Value = (object)spo.DateOfBirth;
                }
                else
                {
                    cmd.Parameters["DateOfBirth"].Value = null;
                }
                //Occupation
                if (spo.IsOccupationNull() == false)
                {
                    cmd.Parameters["Occupation"].Value = (object)spo.Occupation;
                }
                else
                {
                    cmd.Parameters["Occupation"].Value = null;
                }
                //Nationality
                if (spo.IsNationalityNull() == false)
                {
                    cmd.Parameters["Nationality"].Value = (object)spo.Nationality;
                }
                else
                {
                    cmd.Parameters["Nationality"].Value = null;
                }
                //Remarks
                if (spo.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)spo.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = null;
                }

                // WALI :: 17-Feb-2015
                if (!spo.IsSpouseContactNull()) cmd.Parameters["SpouseContact"].Value = (object)spo.SpouseContact;
                else cmd.Parameters["SpouseContact"].Value = DBNull.Value;
                if (!spo.IsSpouseAddressNull()) cmd.Parameters["SpouseAddress"].Value = (object)spo.SpouseAddress;
                else cmd.Parameters["SpouseAddress"].Value = DBNull.Value; 
                if (!spo.IsDegreeIDNull()) cmd.Parameters["DegreeID"].Value = (object)spo.DegreeID;
                else cmd.Parameters["DegreeID"].Value = DBNull.Value;



                //New--start------------------------------------------------------------------------------------------------------------                
                if (spo.IsInstituteNull() == false) cmd.Parameters["Institute"].Value = (object)spo.Institute;
                else cmd.Parameters["Institute"].Value = DBNull.Value;

                if (spo.IsBoardIDNull() == false) cmd.Parameters["BoardID"].Value = (object)spo.BoardID;
                else cmd.Parameters["BoardID"].Value = DBNull.Value;

                if (spo.IsYearNull() == false) cmd.Parameters["Year"].Value = (object)spo.Year;
                else cmd.Parameters["Year"].Value = DBNull.Value;

                if (spo.IsGroupIDNull() == false) cmd.Parameters["GroupID"].Value = (object)spo.GroupID;
                else cmd.Parameters["GroupID"].Value = DBNull.Value;

                if (spo.IsMajorsubjectIDNull() == false) cmd.Parameters["MajorSubjectID"].Value = (object)spo.MajorsubjectID;
                else cmd.Parameters["MajorSubjectID"].Value = DBNull.Value;

                if (spo.IsUniversityNull() == false) cmd.Parameters["University"].Value = (object)spo.University;
                else cmd.Parameters["University"].Value = DBNull.Value;

                if (spo.IsDivisionResultNull() == false) cmd.Parameters["DivisionResult"].Value = (object)spo.DivisionResult;
                else cmd.Parameters["DivisionResult"].Value = DBNull.Value;

                if (spo.IsCGPAresultNull() == false) cmd.Parameters["CGPAresult"].Value = (object)spo.CGPAresult;
                else cmd.Parameters["CGPAresult"].Value = DBNull.Value;

                if (spo.IsOutOfMarksNull() == false) cmd.Parameters["OutOfMarks"].Value = (object)spo.OutOfMarks;
                else cmd.Parameters["OutOfMarks"].Value = DBNull.Value;

                if (spo.IsMarksPercentageNull() == false) cmd.Parameters["MarksPercentage"].Value = (object)spo.MarksPercentage;
                else cmd.Parameters["MarksPercentage"].Value = DBNull.Value;

                if (spo.IsOptionalMarksNull() == false) cmd.Parameters["OptionalMarks"].Value = (object)spo.OptionalMarks;
                else cmd.Parameters["OptionalMarks"].Value = DBNull.Value;

                if (spo.IsResultTypeIDNull() == false) cmd.Parameters["ResultTypeID"].Value = (object)spo.ResultTypeID;
                else cmd.Parameters["ResultTypeID"].Value = DBNull.Value;
             

                if (spo.IsUniversityIDNull() == false) cmd.Parameters["UniversityID"].Value = spo.UniversityID;
                else cmd.Parameters["UniversityID"].Value = DBNull.Value;


                if (spo.IsQualificationIDNull() == false) cmd.Parameters["QualificationID"].Value = (object)spo.QualificationID;
                else cmd.Parameters["QualificationID"].Value = DBNull.Value;

                if (spo.IsRollNoNull() == false) cmd.Parameters["RollNo"].Value = (object)spo.RollNo;
                else cmd.Parameters["RollNo"].Value = DBNull.Value;
               
                if (spo.IsRegistrationNoNull() == false)cmd.Parameters["RegistrationNo"].Value = (object)spo.RegistrationNo;
                else cmd.Parameters["RegistrationNo"].Value = DBNull.Value;
                //New--end--------------------------------------------------------------------------------------------------------------

                //Rony :: 08 Sep 2016
                if (!spo.IsSpouse_NationalIDNull()) cmd.Parameters["Spouse_NationalID"].Value = (object)spo.Spouse_NationalID;
                else cmd.Parameters["Spouse_NationalID"].Value = DBNull.Value;

                if (!spo.IsIsSelfServiseNull() && spo.IsSelfServise)
                {
                    if (!spo.IsIsSubmittedNull()) cmd.Parameters["IsSubmitted"].Value = (object)spo.IsSubmitted;
                    else cmd.Parameters["IsSubmitted"].Value = false;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SPOUSE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteSpouse(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            //DataStringDS stringDS = new DataStringDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (integerDS.DataIntegers[1].IntegerValue == 1) cmd = DBCommandProvider.GetDBCommand("DeleteDataRenewalSpouse");
            else if (integerDS.DataIntegers[1].IntegerValue == 2) cmd = DBCommandProvider.GetDBCommand("DeleteSelfServSpouse");
            else cmd = DBCommandProvider.GetDBCommand("DeleteSpouse");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataIntegerDS.DataInteger spouseRow in integerDS.DataIntegers)
            {
                cmd.Parameters["SpouseID"].Value = (object)spouseRow.IntegerValue;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SPOUSE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                break;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _getSpouseList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == "IsDataRenewal")
            {
                cmd = DBCommandProvider.GetDBCommand("GetDataRenewalSpouseList");
                cmd.Parameters["EmployeeCode"].Value = EmployeeCode;
            }
            else if (stringDS.DataStrings[1].StringValue == "IsSelfService")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServiceSpouseList");
                cmd.Parameters["EmployeeCode"].Value = EmployeeCode;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetSpouseList");
                long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
                cmd.Parameters["EmployeeId"].Value = empId;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region ...OLD... COmmented :: WALI :: 08-Jun-2014
            //EmployeeHistPO hisPO = new EmployeeHistPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter,
            //  hisPO, hisPO.Spouse.TableName,
            //  connDS.DBConnections[0].ConnectionID,
            //  ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_SPOUSE_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //hisPO.AcceptChanges();

            //EmployeeHistDS hisDS = new EmployeeHistDS();

            //if (hisPO.Spouse.Rows.Count > 0)
            //{
            //    foreach (EmployeeHistPO.SpouseRow poSpo in hisPO.Spouse.Rows)
            //    {
            //        EmployeeHistDS.SpouseRow spo = hisDS.Spouse.NewSpouseRow();
            //        if (poSpo.IsSpouseIdNull() == false)
            //        {
            //            spo.SpouseId = poSpo.SpouseId;
            //        }
            //        if (poSpo.IsSpouseCodeNull() == false)
            //        {
            //            spo.Code = poSpo.SpouseCode;
            //        }
            //        if (poSpo.IsEmployeeIdNull() == false)
            //        {
            //            spo.EmployeeId = poSpo.EmployeeId;
            //        }
            //        if (poSpo.IsEmployeeCodeNull() == false)
            //        {
            //            spo.EmployeeCode = poSpo.EmployeeCode;
            //        }
            //        if (poSpo.IsSpouseNameNull() == false)
            //        {
            //            spo.SpouseName = poSpo.SpouseName;
            //        }
            //        if (poSpo.IsDateOfBirthNull() == false)
            //        {
            //            spo.DateOfBirth = poSpo.DateOfBirth;
            //        }
            //        if (poSpo.IsOccupationNull() == false)
            //        {
            //            spo.Occupation = poSpo.Occupation;
            //        }
            //        if (poSpo.IsNationalityNull() == false)
            //        {
            //            spo.Nationality = poSpo.Nationality;
            //        }
            //        if (poSpo.IsRemarksNull() == false)
            //        {
            //            spo.Remarks = poSpo.Remarks;
            //        }

            //        hisDS.Spouse.AddSpouseRow(spo);
            //        hisDS.AcceptChanges();
            //    }
            //}

            //// now create the packet
            //errDS.Clear();
            //errDS.AcceptChanges();

            //DataSet returnDS = new DataSet();

            //returnDS.Merge(hisDS);
            //returnDS.Merge(errDS);
            //returnDS.AcceptChanges();

            //return returnDS;
            #endregion

            #region Update :: WALI :: 08-Jun-2014
            EmployeeHistDS histDS = new EmployeeHistDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, histDS, histDS.Spouse.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SPOUSE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            histDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(histDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

            #endregion
        }
        private DataSet _updateSpouse(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Spouse.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            if (!histDS.Spouse[0].IsIsDataRenewalNull() && histDS.Spouse[0].IsDataRenewal) cmd = DBCommandProvider.GetDBCommand("UpdateDataRenewalSpouse");
            else if (histDS.Spouse[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("UpdateSelfServiceSpouse");
            else cmd = DBCommandProvider.GetDBCommand("UpdateSpouse");


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }



            foreach (EmployeeHistDS.SpouseRow spo in histDS.Spouse.Rows)
            {
                //code
                //if (spo.IsCodeNull() == false)
                //{
                //    cmd.Parameters["Code"].Value = (object)spo.Code;
                //}
                //else
                //{
                //    cmd.Parameters["Code"].Value = null;
                //}

                cmd.Parameters["SpouseID"].Value = spo.SpouseId;

                //Employee Id
                if (spo.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, spo.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
                    else cmd.Parameters["EmpId"].Value = null;
                }
                else
                {
                    cmd.Parameters["EmpId"].Value = null;
                }
                //Spouse Name
                if (spo.IsSpouseNameNull() == false)
                {
                    cmd.Parameters["SpouseName"].Value = (object)spo.SpouseName;
                }
                else
                {
                    cmd.Parameters["SpouseName"].Value = null;
                }
                //Date of Birth
                if (spo.IsDateOfBirthNull() == false)
                {
                    cmd.Parameters["DateOfBirth"].Value = (object)spo.DateOfBirth;
                }
                else
                {
                    cmd.Parameters["DateOfBirth"].Value = null;
                }
                //Occupation
                if (spo.IsOccupationNull() == false)
                {
                    cmd.Parameters["Occupation"].Value = (object)spo.Occupation;
                }
                else
                {
                    cmd.Parameters["Occupation"].Value = null;
                }
                //Nationality
                if (spo.IsNationalityNull() == false)
                {
                    cmd.Parameters["Nationality"].Value = (object)spo.Nationality;
                }
                else
                {
                    cmd.Parameters["Nationality"].Value = null;
                }
                //Remarks
                if (spo.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)spo.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = null;
                }

                // WALI :: 17-Feb-2015
                if (!spo.IsSpouseContactNull()) cmd.Parameters["SpouseContact"].Value = (object)spo.SpouseContact;
                else cmd.Parameters["SpouseContact"].Value = DBNull.Value;
                if (!spo.IsSpouseAddressNull()) cmd.Parameters["SpouseAddress"].Value = (object)spo.SpouseAddress;
                else cmd.Parameters["SpouseAddress"].Value = DBNull.Value;
                //

                if (!spo.IsDegreeIDNull()) cmd.Parameters["DegreeID"].Value = (object)spo.DegreeID;
                else cmd.Parameters["DegreeID"].Value = DBNull.Value;


                //New--start------------------------------------------------------------------------------------------------------------                
                if (spo.IsInstituteNull() == false) cmd.Parameters["Institute"].Value = (object)spo.Institute;
                else cmd.Parameters["Institute"].Value = DBNull.Value;

                if (spo.IsBoardIDNull() == false) cmd.Parameters["BoardID"].Value = (object)spo.BoardID;
                else cmd.Parameters["BoardID"].Value = DBNull.Value;

                if (spo.IsYearNull() == false) cmd.Parameters["Year"].Value = (object)spo.Year;
                else cmd.Parameters["Year"].Value = DBNull.Value;

                if (spo.IsGroupIDNull() == false) cmd.Parameters["GroupID"].Value = (object)spo.GroupID;
                else cmd.Parameters["GroupID"].Value = DBNull.Value;

                if (spo.IsMajorsubjectIDNull() == false) cmd.Parameters["MajorSubjectID"].Value = (object)spo.MajorsubjectID;
                else cmd.Parameters["MajorSubjectID"].Value = DBNull.Value;

                if (spo.IsUniversityNull() == false) cmd.Parameters["University"].Value = (object)spo.University;
                else cmd.Parameters["University"].Value = DBNull.Value;

                if (spo.IsDivisionResultNull() == false) cmd.Parameters["DivisionResult"].Value = (object)spo.DivisionResult;
                else cmd.Parameters["DivisionResult"].Value = DBNull.Value;

                if (spo.IsCGPAresultNull() == false) cmd.Parameters["CGPAresult"].Value = (object)spo.CGPAresult;
                else cmd.Parameters["CGPAresult"].Value = DBNull.Value;

                if (spo.IsOutOfMarksNull() == false) cmd.Parameters["OutOfMarks"].Value = (object)spo.OutOfMarks;
                else cmd.Parameters["OutOfMarks"].Value = DBNull.Value;

                if (spo.IsMarksPercentageNull() == false) cmd.Parameters["MarksPercentage"].Value = (object)spo.MarksPercentage;
                else cmd.Parameters["MarksPercentage"].Value = DBNull.Value;

                if (spo.IsOptionalMarksNull() == false) cmd.Parameters["OptionalMarks"].Value = (object)spo.OptionalMarks;
                else cmd.Parameters["OptionalMarks"].Value = DBNull.Value;

                if (spo.IsResultTypeIDNull() == false) cmd.Parameters["ResultTypeID"].Value = (object)spo.ResultTypeID;
                else cmd.Parameters["ResultTypeID"].Value = DBNull.Value;

           
                if (spo.IsUniversityIDNull() == false) cmd.Parameters["UniversityID"].Value = spo.UniversityID;
                else cmd.Parameters["UniversityID"].Value = DBNull.Value;


                if (spo.IsQualificationIDNull() == false) cmd.Parameters["QualificationID"].Value = (object)spo.QualificationID;
                else cmd.Parameters["QualificationID"].Value = DBNull.Value;

                if (spo.IsRollNoNull() == false) cmd.Parameters["RollNo"].Value = (object)spo.RollNo;
                else cmd.Parameters["RollNo"].Value = DBNull.Value;

                if (spo.IsRegistrationNoNull() == false) cmd.Parameters["RegistrationNo"].Value = (object)spo.RegistrationNo;
                else cmd.Parameters["RegistrationNo"].Value = DBNull.Value;
                //New--end--------------------------------------------------------------------------------------------------------------

                //Rony :: 08 Sep 2016
                if (!spo.IsSpouse_NationalIDNull()) cmd.Parameters["Spouse_NationalID"].Value = (object)spo.Spouse_NationalID;
                else cmd.Parameters["Spouse_NationalID"].Value = DBNull.Value;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SPOUSE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _doesSpouseExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesSpouseExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_SPOUSE_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createChildren(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Children.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            if (!histDS.Children[0].IsIsDataRenewalNull() && histDS.Children[0].IsDataRenewal) cmd = DBCommandProvider.GetDBCommand("CreateDataRenewalChildren");
            else if (histDS.Children[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("CreateSelfServChildren");
            else cmd = DBCommandProvider.GetDBCommand("CreateChildren");


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistDS.ChildrenRow chi in histDS.Children.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["Id"].Value = (object)genPK;
                //code
                //if (chi.IsCodeNull() == false)
                //{
                //    cmd.Parameters["Code"].Value = (object)chi.Code;
                //}
                //else
                //{
                //    cmd.Parameters["Code"].Value = null;
                //}
                //Employee Id
                if (chi.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, chi.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
                    else cmd.Parameters["EmpId"].Value = null;
                }
                else
                {
                    cmd.Parameters["EmpId"].Value = DBNull.Value;
                }
                //Children Name
                if (chi.IsChildNameNull() == false)
                {
                    cmd.Parameters["ChildName"].Value = (object)chi.ChildName;
                }
                else
                {
                    cmd.Parameters["ChildName"].Value = DBNull.Value;
                }
                //Date of Birth
                if (chi.IsDateOfBirthNull() == false)
                {
                    cmd.Parameters["DateOfBirth"].Value = (object)chi.DateOfBirth;
                }
                else
                {
                    cmd.Parameters["DateOfBirth"].Value = DBNull.Value;
                }
                //Gender
                if (chi.IsGenderNull() == false)
                {
                    cmd.Parameters["Gender"].Value = (object)chi.Gender;
                }
                else
                {
                    cmd.Parameters["Gender"].Value = DBNull.Value;
                }
                //Blood Group
                if (chi.IsBloodGroupNull() == false)
                {
                    cmd.Parameters["BloodGroup"].Value = (object)chi.BloodGroup;
                }
                else
                {
                    cmd.Parameters["BloodGroup"].Value = DBNull.Value;
                }
                //Nationality
                if (chi.IsNationalityNull() == false)
                {
                    cmd.Parameters["Nationality"].Value = (object)chi.Nationality;
                }
                else
                {
                    cmd.Parameters["Nationality"].Value = DBNull.Value;
                }
                //Remarks
                if (chi.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)chi.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = DBNull.Value;
                }

                if (chi.IsDegreeIDNull() == false)
                {
                    cmd.Parameters["DegreeID"].Value = (object)chi.DegreeID;
                }
                else
                {
                    cmd.Parameters["DegreeID"].Value = DBNull.Value;
                }



                //New--start------------------------------------------------------------------------------------------------------------                
                if (chi.IsInstituteNull() == false) cmd.Parameters["Institute"].Value = (object)chi.Institute;
                else cmd.Parameters["Institute"].Value = DBNull.Value;

                if (chi.IsBoardIDNull() == false) cmd.Parameters["BoardID"].Value = (object)chi.BoardID;
                else cmd.Parameters["BoardID"].Value = DBNull.Value;

                if (chi.IsYearNull() == false) cmd.Parameters["Year"].Value = (object)chi.Year;
                else cmd.Parameters["Year"].Value = DBNull.Value;

                if (chi.IsGroupIDNull() == false) cmd.Parameters["GroupID"].Value = (object)chi.GroupID;
                else cmd.Parameters["GroupID"].Value = DBNull.Value;

                if (chi.IsMajorsubjectIDNull() == false) cmd.Parameters["MajorSubjectID"].Value = (object)chi.MajorsubjectID;
                else cmd.Parameters["MajorSubjectID"].Value = DBNull.Value;

                if (chi.IsUniversityNull() == false) cmd.Parameters["University"].Value = (object)chi.University;
                else cmd.Parameters["University"].Value = DBNull.Value;

                if (chi.IsDivisionResultNull() == false) cmd.Parameters["DivisionResult"].Value = (object)chi.DivisionResult;
                else cmd.Parameters["DivisionResult"].Value = DBNull.Value;

                if (chi.IsCGPAresultNull() == false) cmd.Parameters["CGPAresult"].Value = (object)chi.CGPAresult;
                else cmd.Parameters["CGPAresult"].Value = DBNull.Value;

                if (chi.IsOutOfMarksNull() == false) cmd.Parameters["OutOfMarks"].Value = (object)chi.OutOfMarks;
                else cmd.Parameters["OutOfMarks"].Value = DBNull.Value;

                if (chi.IsMarksPercentageNull() == false) cmd.Parameters["MarksPercentage"].Value = (object)chi.MarksPercentage;
                else cmd.Parameters["MarksPercentage"].Value = DBNull.Value;

                if (chi.IsOptionalMarksNull() == false) cmd.Parameters["OptionalMarks"].Value = (object)chi.OptionalMarks;
                else cmd.Parameters["OptionalMarks"].Value = DBNull.Value;

                if (chi.IsResultTypeIDNull() == false) cmd.Parameters["ResultTypeID"].Value = (object)chi.ResultTypeID;
                else cmd.Parameters["ResultTypeID"].Value = DBNull.Value;


                if (chi.IsUniversityIDNull() == false) cmd.Parameters["UniversityID"].Value = chi.UniversityID;
                else cmd.Parameters["UniversityID"].Value = DBNull.Value;


                if (chi.IsQualificationIDNull() == false) cmd.Parameters["QualificationID"].Value = (object)chi.QualificationID;
                else cmd.Parameters["QualificationID"].Value = DBNull.Value;

                if (chi.IsRollNoNull() == false) cmd.Parameters["RollNo"].Value = (object)chi.RollNo;
                else cmd.Parameters["RollNo"].Value = DBNull.Value;

                if (chi.IsRegistrationNoNull() == false) cmd.Parameters["RegistrationNo"].Value = (object)chi.RegistrationNo;
                else cmd.Parameters["RegistrationNo"].Value = DBNull.Value;
                //New--end--------------------------------------------------------------------------------------------------------------

                if (chi.IsChild_NIDNull() == false) cmd.Parameters["Child_NID"].Value = (object)chi.Child_NID;
                else cmd.Parameters["Child_NID"].Value = DBNull.Value;

                if (!chi.IsIsSelfServiseNull() && chi.IsSelfServise)
                {
                    if (!chi.IsIsSubmittedNull()) cmd.Parameters["IsSubmitted"].Value = (object)chi.IsSubmitted;
                    else cmd.Parameters["IsSubmitted"].Value = false;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHILDREN_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteChildren(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //DataStringDS stringDS = new DataStringDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteChildren");

            OleDbCommand cmd = new OleDbCommand();
            if (integerDS.DataIntegers[1].IntegerValue == 1) cmd = DBCommandProvider.GetDBCommand("DeleteDataRenewalChildren");
            else if (integerDS.DataIntegers[1].IntegerValue == 2) cmd = DBCommandProvider.GetDBCommand("DeleteSelfServChildren");
            else cmd = DBCommandProvider.GetDBCommand("DeleteChildren");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataIntegerDS.DataInteger childID in integerDS.DataIntegers)
            {
                cmd.Parameters["ChildrenID"].Value = (object)childID.IntegerValue;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHILDREN_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                break;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _getChildrenList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == "IsDataRenewal")
            {
                cmd = DBCommandProvider.GetDBCommand("GetDataRenewalChildList");
                cmd.Parameters["EmployeeCode"].Value = EmployeeCode;
            }
            else if (stringDS.DataStrings[1].StringValue == "IsSelfService")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServChildList");
                cmd.Parameters["EmployeeCode"].Value = EmployeeCode;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetChildrenList");

                long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
                cmd.Parameters["EmployeeId"].Value = empId;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeHistDS histDS = new EmployeeHistDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, histDS, histDS.Children.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_CHILDREN_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            histDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(histDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _updateChildren(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            histDS.Merge(inputDS.Tables[histDS.Children.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateChildren");

            OleDbCommand cmd = new OleDbCommand();
            if (!histDS.Children[0].IsIsDataRenewalNull() && histDS.Children[0].IsDataRenewal) cmd = DBCommandProvider.GetDBCommand("UpdateDataRenewalChildren");
            else if (histDS.Children[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("UpdateSelfServChildren");
            else cmd = DBCommandProvider.GetDBCommand("UpdateChildren");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }



            foreach (EmployeeHistDS.ChildrenRow chi in histDS.Children.Rows)
            {
                //code
                //if (chi.IsCodeNull() == false)
                //{
                //    cmd.Parameters["Code"].Value = (object)chi.Code;
                //}
                //else
                //{
                //    cmd.Parameters["Code"].Value = null;
                //}
                //Employee Id
                if (chi.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, chi.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
                    else cmd.Parameters["EmpId"].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters["EmpId"].Value = DBNull.Value;
                }
                //Children Name
                if (chi.IsChildNameNull() == false)
                {
                    cmd.Parameters["ChildName"].Value = (object)chi.ChildName;
                }
                else
                {
                    cmd.Parameters["ChildName"].Value = DBNull.Value;
                }
                //Date of Birth
                if (chi.IsDateOfBirthNull() == false)
                {
                    cmd.Parameters["DateOfBirth"].Value = (object)chi.DateOfBirth;
                }
                else
                {
                    cmd.Parameters["DateOfBirth"].Value = DBNull.Value;
                }
                //Gender
                if (chi.IsGenderNull() == false)
                {
                    cmd.Parameters["Gender"].Value = (object)chi.Gender;
                }
                else
                {
                    cmd.Parameters["Gender"].Value = DBNull.Value;
                }
                //Blood Group
                if (chi.IsBloodGroupNull() == false)
                {
                    cmd.Parameters["BloodGroup"].Value = (object)chi.BloodGroup;
                }
                else
                {
                    cmd.Parameters["BloodGroup"].Value = DBNull.Value;
                }
                //Nationality
                if (chi.IsNationalityNull() == false)
                {
                    cmd.Parameters["Nationality"].Value = (object)chi.Nationality;
                }
                else
                {
                    cmd.Parameters["Nationality"].Value = DBNull.Value;
                }
                //Remarks
                if (chi.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)chi.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = DBNull.Value;
                }


                if (chi.IsDegreeIDNull() == false)
                {
                    cmd.Parameters["DegreeID"].Value = (object)chi.DegreeID;
                }
                else
                {
                    cmd.Parameters["DegreeID"].Value = DBNull.Value;
                }



                //New--start------------------------------------------------------------------------------------------------------------                
                if (chi.IsInstituteNull() == false) cmd.Parameters["Institute"].Value = (object)chi.Institute;
                else cmd.Parameters["Institute"].Value = DBNull.Value;

                if (chi.IsBoardIDNull() == false) cmd.Parameters["BoardID"].Value = (object)chi.BoardID;
                else cmd.Parameters["BoardID"].Value = DBNull.Value;

                if (chi.IsYearNull() == false) cmd.Parameters["Year"].Value = (object)chi.Year;
                else cmd.Parameters["Year"].Value = DBNull.Value;

                if (chi.IsGroupIDNull() == false) cmd.Parameters["GroupID"].Value = (object)chi.GroupID;
                else cmd.Parameters["GroupID"].Value = DBNull.Value;

                if (chi.IsMajorsubjectIDNull() == false) cmd.Parameters["MajorSubjectID"].Value = (object)chi.MajorsubjectID;
                else cmd.Parameters["MajorSubjectID"].Value = DBNull.Value;

                if (chi.IsUniversityNull() == false) cmd.Parameters["University"].Value = (object)chi.University;
                else cmd.Parameters["University"].Value = DBNull.Value;

                if (chi.IsDivisionResultNull() == false) cmd.Parameters["DivisionResult"].Value = (object)chi.DivisionResult;
                else cmd.Parameters["DivisionResult"].Value = DBNull.Value;

                if (chi.IsCGPAresultNull() == false) cmd.Parameters["CGPAresult"].Value = (object)chi.CGPAresult;
                else cmd.Parameters["CGPAresult"].Value = DBNull.Value;

                if (chi.IsOutOfMarksNull() == false) cmd.Parameters["OutOfMarks"].Value = (object)chi.OutOfMarks;
                else cmd.Parameters["OutOfMarks"].Value = DBNull.Value;

                if (chi.IsMarksPercentageNull() == false) cmd.Parameters["MarksPercentage"].Value = (object)chi.MarksPercentage;
                else cmd.Parameters["MarksPercentage"].Value = DBNull.Value;

                if (chi.IsOptionalMarksNull() == false) cmd.Parameters["OptionalMarks"].Value = (object)chi.OptionalMarks;
                else cmd.Parameters["OptionalMarks"].Value = DBNull.Value;

                if (chi.IsResultTypeIDNull() == false) cmd.Parameters["ResultTypeID"].Value = (object)chi.ResultTypeID;
                else cmd.Parameters["ResultTypeID"].Value = DBNull.Value;


                if (chi.IsUniversityIDNull() == false) cmd.Parameters["UniversityID"].Value = chi.UniversityID;
                else cmd.Parameters["UniversityID"].Value = DBNull.Value;


                if (chi.IsQualificationIDNull() == false) cmd.Parameters["QualificationID"].Value = (object)chi.QualificationID;
                else cmd.Parameters["QualificationID"].Value = DBNull.Value;

                if (chi.IsRollNoNull() == false) cmd.Parameters["RollNo"].Value = (object)chi.RollNo;
                else cmd.Parameters["RollNo"].Value = DBNull.Value;

                if (chi.IsRegistrationNoNull() == false) cmd.Parameters["RegistrationNo"].Value = (object)chi.RegistrationNo;
                else cmd.Parameters["RegistrationNo"].Value = DBNull.Value;
                //New--end--------------------------------------------------------------------------------------------------------------

                if (chi.IsChild_NIDNull() == false) cmd.Parameters["Child_NID"].Value = (object)chi.Child_NID;
                else cmd.Parameters["Child_NID"].Value = DBNull.Value;

                cmd.Parameters["ChildrenID"].Value = (object)chi.ChildrenId;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHILDREN_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _doesChildrenExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesChildrenExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_CHILDREN_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createEducation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.EducationDetails.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            
            //create command
            OleDbCommand cmd = new OleDbCommand();

            if (!histDS.EducationDetails[0].IsIsDataRenewalNull() && histDS.EducationDetails[0].IsDataRenewal) cmd = DBCommandProvider.GetDBCommand("CreateDataRenewalEduDetails");
            else if (!histDS.EducationDetails[0].IsIsSelfServiceNull() && histDS.EducationDetails[0].IsSelfService) cmd = DBCommandProvider.GetDBCommand("CreateSelfServEduDetails");
            else cmd = DBCommandProvider.GetDBCommand("CreateEducationDetails");
                
                
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistDS.EducationDetailsRow edu in histDS.EducationDetails.Rows)
            {
                //long genPK = IDGenerator.GetNextGenericPK();
                //if (genPK == -1)
                //{
                //    UtilDL.GetDBOperationFailed();
                //}

                //cmd.Parameters["EDUCATIONID"].Value = (object)genPK;

                //Employee Id
                if (edu.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, edu.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
                    else cmd.Parameters["EmpId"].Value = DBNull.Value;
                }
                else
                {
                    cmd.Parameters["EmpId"].Value = DBNull.Value;
                }
                //Degree
                if (edu.IsDegreeIDNull() == false)
                {
                    cmd.Parameters["DegreeID"].Value = (object)edu.DegreeID;
                }
                else
                {
                    cmd.Parameters["DegreeID"].Value = DBNull.Value;
                }
                //Institute
                if (edu.IsInstituteNull() == false)
                {
                    cmd.Parameters["Institute"].Value = (object)edu.Institute;
                }
                else
                {
                    cmd.Parameters["Institute"].Value = DBNull.Value;
                }
                //Board
                if (edu.IsBoardIDNull() == false)
                {
                    cmd.Parameters["BoardID"].Value = (object)edu.BoardID;
                }
                else
                {
                    cmd.Parameters["BoardID"].Value = DBNull.Value;
                }
                //Year
                if (edu.IsYearNull() == false)
                {
                    cmd.Parameters["Year"].Value = (object)edu.Year;
                }
                else
                {
                    cmd.Parameters["Year"].Value = DBNull.Value;
                }

                //Remarks
                if (edu.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)edu.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = DBNull.Value;
                }
                //GroupID
                if (edu.IsGroupIDNull() == false)
                {
                    cmd.Parameters["GroupID"].Value = (object)edu.GroupID;
                }
                else
                {
                    cmd.Parameters["GroupID"].Value = DBNull.Value;
                }

                //Major Subject ID           
                if (edu.IsMajorSubjectIDNull() == false)
                {
                    cmd.Parameters["MajorSubjectID"].Value = (object)edu.MajorSubjectID;
                }
                else
                {
                    cmd.Parameters["MajorSubjectID"].Value = DBNull.Value;
                }

                //University
                if (edu.IsUniversityNull() == false)
                {
                    cmd.Parameters["University"].Value = (object)edu.University;
                }
                else
                {
                    cmd.Parameters["University"].Value = DBNull.Value;
                }

                //Division Result
                if (edu.IsDivisionResultNull() == false)
                {
                    cmd.Parameters["DivisionResult"].Value = (object)edu.DivisionResult;
                }
                else
                {
                    cmd.Parameters["DivisionResult"].Value = DBNull.Value;
                }

                //CGPA Result
                if (edu.IsCGPAresultNull() == false)
                {
                    cmd.Parameters["CGPAresult"].Value = (object)edu.CGPAresult;
                }
                else
                {
                    cmd.Parameters["CGPAresult"].Value = DBNull.Value;
                }

                //Out of GPA
                if (edu.IsOutOfMarksNull() == false)
                {
                    cmd.Parameters["OutOfMarks"].Value = (object)edu.OutOfMarks;
                }
                else
                {
                    cmd.Parameters["OutOfMarks"].Value = DBNull.Value;
                }

                //Marks Percentage
                if (edu.IsMarksPercentageNull() == false)
                {
                    cmd.Parameters["MarksPercentage"].Value = (object)edu.MarksPercentage;
                }
                else
                {
                    cmd.Parameters["MarksPercentage"].Value = DBNull.Value;
                }

                //Optional Marks
                if (edu.IsOptionalMarksNull() == false)
                {
                    cmd.Parameters["OptionalMarks"].Value = (object)edu.OptionalMarks;
                }
                else
                {
                    cmd.Parameters["OptionalMarks"].Value = DBNull.Value;
                }
                //Result Type
                if (edu.IsResulttypeidNull() == false)
                {
                    cmd.Parameters["Resulttypeid"].Value = (object)edu.Resulttypeid;
                }
                else
                {
                    cmd.Parameters["Resulttypeid"].Value = DBNull.Value;
                }

                #region WALI :: 11-Jun-2014
                if (edu.IsOASYearNull() == false)
                {
                    cmd.Parameters["OASYear"].Value = (object)edu.OASYear;
                }
                else
                {
                    cmd.Parameters["OASYear"].Value = DBNull.Value;
                }
                if (edu.IsOASResultNull() == false)
                {
                    cmd.Parameters["OASResult"].Value = (object)edu.OASResult;
                }
                else
                {
                    cmd.Parameters["OASResult"].Value = DBNull.Value;
                }
                #endregion

                #region WALI :: 30-Jun-2014
                if (edu.IsUniversityIDNull() == false)
                {
                    cmd.Parameters["UniversityID"].Value = edu.UniversityID;
                }
                else
                {
                    cmd.Parameters["UniversityID"].Value = DBNull.Value;
                }
                #endregion

                #region Qualification Shakir 16.02.15
                if (edu.IsQualificationIDNull() == false)
                {
                    cmd.Parameters["QualificationID"].Value = (object)edu.QualificationID;
                }
                else
                {
                    cmd.Parameters["QualificationID"].Value = DBNull.Value;
                }
                #endregion

                //Shakir 23.02.2015
                if (edu.IsRollNoNull() == false)
                {
                    cmd.Parameters["RollNo"].Value = (object)edu.RollNo;
                }
                else
                {
                    cmd.Parameters["RollNo"].Value = DBNull.Value;
                }

                if (edu.IsRegistrationNoNull() == false)
                {
                    cmd.Parameters["RegistrationNo"].Value = (object)edu.RegistrationNo;
                }
                else
                {
                    cmd.Parameters["RegistrationNo"].Value = DBNull.Value;
                }

                #region WALI :: 31-Aug-2016
                if (!edu.IsCountryCodeNull()) cmd.Parameters["CountryCode"].Value = edu.CountryCode;
                else cmd.Parameters["CountryCode"].Value = DBNull.Value;

                if (!edu.IsVerificationStatusNull()) cmd.Parameters["VerificationStatus"].Value = edu.VerificationStatus;
                else cmd.Parameters["VerificationStatus"].Value = DBNull.Value;
                if (!edu.IsVerifierEmployeeNull()) cmd.Parameters["VerifierEmployee"].Value = edu.VerifierEmployee;
                else cmd.Parameters["VerifierEmployee"].Value = DBNull.Value;
                if (!edu.IsVerificationDateNull()) cmd.Parameters["VerificationDate"].Value = edu.VerificationDate;
                else cmd.Parameters["VerificationDate"].Value = DBNull.Value;
                if (!edu.IsVerificationNoteNull()) cmd.Parameters["VerificationNote"].Value = edu.VerificationNote;
                else cmd.Parameters["VerificationNote"].Value = DBNull.Value;
                #endregion

                #region Rony :: 06 Sep 2016
                if (!edu.IsCertificateFileNameNull()) cmd.Parameters["CertificateFileName"].Value = edu.CertificateFileName;
                else cmd.Parameters["CertificateFileName"].Value = DBNull.Value;
                #endregion

                if (!edu.IsIsSelfServiceNull() && edu.IsSelfService)
                {
                    if (!edu.IsIsSubmittedNull()) cmd.Parameters["IsSubmitted"].Value = (object)edu.IsSubmitted;
                    else cmd.Parameters["IsSubmitted"].Value = false;
                }
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EDUCATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteEducation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == "IsDataRenewal") cmd = DBCommandProvider.GetDBCommand("DeleteDataRenewalEdu");
            else if (stringDS.DataStrings[1].StringValue == "IsSelfService") cmd = DBCommandProvider.GetDBCommand("DeleteSelfServEdu");
            else cmd = DBCommandProvider.GetDBCommand("DeleteEducation");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EDUCATION_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                break;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _getEducationList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == "IsDataRenewal")
            {
                cmd = DBCommandProvider.GetDBCommand("GetDataRenewalEduList");
                cmd.Parameters["EmployeeCode"].Value = EmployeeCode;
            }
            else if (stringDS.DataStrings[1].StringValue == "IsSelfService")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServiceEduList");
                cmd.Parameters["EmployeeCode"].Value = EmployeeCode;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetEducationList");
                long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
                cmd.Parameters["EmployeeId"].Value = empId;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            //EmployeeHistPO hisPO = new EmployeeHistPO();
            EmployeeHistDS hisDS = new EmployeeHistDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              hisDS, hisDS.Education.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EDUCATION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            hisDS.AcceptChanges();
            //hisPO.AcceptChanges();

            //EmployeeHistDS hisDS = new EmployeeHistDS();

            //if (hisPO.Education.Rows.Count > 0)
            //{
            //    foreach (EmployeeHistPO.EducationRow poEdu in hisPO.Education.Rows)
            //    {
            //        EmployeeHistDS.EducationRow Edu = hisDS.Education.NewEducationRow();
            //        if (poEdu.IsEducationIdNull() == false)
            //        {
            //            Edu.EducationId = poEdu.EducationId;
            //        }
            //        if (poEdu.IsEducationCodeNull() == false)
            //        {
            //            Edu.Code = poEdu.EducationCode;
            //        }
            //        if (poEdu.IsEmployeeIdNull() == false)
            //        {
            //            Edu.EmployeeId = poEdu.EmployeeId;
            //        }
            //        if (poEdu.IsEmployeeCodeNull() == false)
            //        {
            //            Edu.EmployeeCode = poEdu.EmployeeCode;
            //        }
            //        if (poEdu.IsDegreeNull() == false)
            //        {
            //            Edu.Degree = poEdu.Degree;
            //        }
            //        if (poEdu.IsInstituteNull() == false)
            //        {
            //            Edu.Institute = poEdu.Institute;
            //        }
            //        if (poEdu.IsBoardNull() == false)
            //        {
            //            Edu.Board = poEdu.Board;
            //        }
            //        if (poEdu.IsYearNull() == false)
            //        {
            //            Edu.Year = poEdu.Year;
            //        }
            //        if (poEdu.IsResultNull() == false)
            //        {
            //            Edu.Result = poEdu.Result;
            //        }
            //        if (poEdu.IsRemarksNull() == false)
            //        {
            //            Edu.Remarks = poEdu.Remarks;
            //        }

            //        hisDS.Education.AddEducationRow(Edu);
            //        hisDS.AcceptChanges();
            //    }
            //}

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        private DataSet _updateEducation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.EducationDetails.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            if (!histDS.EducationDetails[0].IsIsDataRenewalNull() && histDS.EducationDetails[0].IsDataRenewal) cmd = DBCommandProvider.GetDBCommand("UpdateDataRenewalEducation");
            else if (!histDS.EducationDetails[0].IsIsSelfServiceNull() && histDS.EducationDetails[0].IsSelfService) cmd = DBCommandProvider.GetDBCommand("UpdateSelfServEducation");
            else cmd = DBCommandProvider.GetDBCommand("UpdateEducation");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }



            foreach (EmployeeHistDS.EducationDetailsRow edu in histDS.EducationDetails.Rows)
            {
                ////code
                //if (edu.IsCodeNull() == false)
                //{
                //    cmd.Parameters["Code"].Value = (object)edu.Code;
                //}
                //else
                //{
                //    cmd.Parameters["Code"].Value = null;
                //}

                //Employee Id
                if (edu.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, edu.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
                    else cmd.Parameters["EmpId"].Value = null;
                }
                else
                {
                    cmd.Parameters["EmpId"].Value = null;
                }

                //Degree ID
                if (edu.IsEducationIdNull() == false)
                {
                    cmd.Parameters["EducationId"].Value = (object)edu.EducationId;
                }
                else
                {
                    cmd.Parameters["EducationId"].Value = null;
                }

                //Degree
                if (edu.IsDegreeIDNull() == false)
                {
                    cmd.Parameters["DegreeID"].Value = (object)edu.DegreeID;
                }
                else
                {
                    cmd.Parameters["DegreeID"].Value = System.DBNull.Value;
                }
                //Institute
                if (edu.IsInstituteNull() == false)
                {
                    cmd.Parameters["Institute"].Value = (object)edu.Institute;
                }
                else
                {
                    cmd.Parameters["Institute"].Value = System.DBNull.Value;
                }
                //Board
                if (edu.IsBoardIDNull() == false)
                {
                    cmd.Parameters["BoardID"].Value = (object)edu.BoardID;
                }
                else
                {
                    cmd.Parameters["BoardID"].Value = System.DBNull.Value;
                }
                //Year
                if (edu.IsYearNull() == false)
                {
                    cmd.Parameters["Year"].Value = (object)edu.Year;
                }
                else
                {
                    cmd.Parameters["Year"].Value = System.DBNull.Value;
                }

                //Remarks
                if (edu.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)edu.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = System.DBNull.Value;
                }
                //GroupID
                if (edu.IsGroupIDNull() == false)
                {
                    cmd.Parameters["GroupID"].Value = (object)edu.GroupID;
                }
                else
                {
                    cmd.Parameters["GroupID"].Value = System.DBNull.Value;
                }

                //MajorGroupID
                if (edu.IsMajorSubjectIDNull() == false)
                {
                    cmd.Parameters["MajorSubjectID"].Value = (object)edu.MajorSubjectID;
                }
                else
                {
                    cmd.Parameters["MajorSubjectID"].Value = System.DBNull.Value;
                }

                //University
                if (edu.IsUniversityNull() == false)
                {
                    cmd.Parameters["University"].Value = (object)edu.University;
                }
                else
                {
                    cmd.Parameters["University"].Value = System.DBNull.Value;
                }
                //Divission Result
                if (edu.IsDivisionResultNull() == false)
                {
                    cmd.Parameters["DivisionResult"].Value = (object)edu.DivisionResult;
                }
                else
                {
                    cmd.Parameters["DivisionResult"].Value = System.DBNull.Value;
                }

                //CGPAResult
                if (edu.IsCGPAresultNull() == false)
                {
                    cmd.Parameters["CGPAresult"].Value = (object)edu.CGPAresult;
                }
                else
                {
                    cmd.Parameters["CGPAresult"].Value = System.DBNull.Value;
                }
                //OutofMarks
                if (edu.IsOutOfMarksNull() == false)
                {
                    cmd.Parameters["OutOfMarks"].Value = (object)edu.OutOfMarks;
                }
                else
                {
                    cmd.Parameters["OutOfMarks"].Value = System.DBNull.Value;
                }
                //MarksParcentage
                if (edu.IsMarksPercentageNull() == false)
                {
                    cmd.Parameters["MarksPercentage"].Value = (object)edu.MarksPercentage;
                }
                else
                {
                    cmd.Parameters["MarksPercentage"].Value = System.DBNull.Value;
                }

                //OptionalMarks
                if (edu.IsOptionalMarksNull() == false)
                {
                    cmd.Parameters["OptionalMarks"].Value = (object)edu.OptionalMarks;
                }
                else
                {
                    cmd.Parameters["OptionalMarks"].Value = System.DBNull.Value;
                }
                //ResulttypeID
                if (edu.IsResulttypeidNull() == false)
                {
                    cmd.Parameters["Resulttypeid"].Value = (object)edu.Resulttypeid;
                }
                else
                {
                    cmd.Parameters["Resulttypeid"].Value = System.DBNull.Value;
                }

                #region WALI :: 11-Jun-2014
                if (edu.IsOASYearNull() == false)
                {
                    cmd.Parameters["OASYear"].Value = (object)edu.OASYear;
                }
                else
                {
                    cmd.Parameters["OASYear"].Value = DBNull.Value;
                }
                if (edu.IsOASResultNull() == false)
                {
                    cmd.Parameters["OASResult"].Value = (object)edu.OASResult;
                }
                else
                {
                    cmd.Parameters["OASResult"].Value = DBNull.Value;
                }
                #endregion

                #region WALI :: 30-Jun-2014
                if (edu.IsUniversityIDNull() == false)
                {
                    cmd.Parameters["UniversityID"].Value = edu.UniversityID;
                }
                else
                {
                    cmd.Parameters["UniversityID"].Value = DBNull.Value;
                }
                #endregion


                #region Qualification Shakir 16.02.15
                if (edu.IsQualificationIDNull() == false)
                {
                    cmd.Parameters["QualificationID"].Value = (object)edu.QualificationID;
                }
                else
                {
                    cmd.Parameters["QualificationID"].Value = DBNull.Value;
                }
                #endregion

                //Shakir 23.02.2015
                if (edu.IsRollNoNull() == false)
                {
                    cmd.Parameters["RollNo"].Value = (object)edu.RollNo;
                }
                else
                {
                    cmd.Parameters["RollNo"].Value = DBNull.Value;
                }

                if (edu.IsRegistrationNoNull() == false)
                {
                    cmd.Parameters["RegistrationNo"].Value = (object)edu.RegistrationNo;
                }
                else
                {
                    cmd.Parameters["RegistrationNo"].Value = DBNull.Value;
                }

                #region WALI :: 31-Aug-2016
                if (!edu.IsCountryCodeNull()) cmd.Parameters["CountryCode"].Value = edu.CountryCode;
                else cmd.Parameters["CountryCode"].Value = DBNull.Value;

                if (!edu.IsVerificationStatusNull()) cmd.Parameters["VerificationStatus"].Value = edu.VerificationStatus;
                else cmd.Parameters["VerificationStatus"].Value = DBNull.Value;
                if (!edu.IsVerifierEmployeeNull()) cmd.Parameters["VerifierEmployee"].Value = edu.VerifierEmployee;
                else cmd.Parameters["VerifierEmployee"].Value = DBNull.Value;
                if (!edu.IsVerificationDateNull()) cmd.Parameters["VerificationDate"].Value = edu.VerificationDate;
                else cmd.Parameters["VerificationDate"].Value = DBNull.Value;
                if (!edu.IsVerificationNoteNull()) cmd.Parameters["VerificationNote"].Value = edu.VerificationNote;
                else cmd.Parameters["VerificationNote"].Value = DBNull.Value;
                #endregion

                #region Rony :: 06 Sep 2016
                if (!edu.IsCertificateFileNameNull()) cmd.Parameters["CertificateFileName"].Value = edu.CertificateFileName;
                else cmd.Parameters["CertificateFileName"].Value = DBNull.Value;
                #endregion

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EDUCATION_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _doesEducationExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesEducationExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_EDUCATION_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createTraining(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            histDS.Merge(inputDS.Tables[histDS.Training.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            if (!histDS.Training[0].IsIsDataRenewalNull() && histDS.Training[0].IsDataRenewal) cmd = DBCommandProvider.GetDBCommand("CreateDataRenewalTraining");
            else if (histDS.Training[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("CreateSelfServTraining");
            else cmd = DBCommandProvider.GetDBCommand("CreateTraining");


            foreach (EmployeeHistDS.TrainingRow tra in histDS.Training.Rows)
            {
                //long genPK = IDGenerator.GetNextGenericPK();
                //if (genPK == -1)
                //{
                //    UtilDL.GetDBOperationFailed();
                //}

                //cmd.Parameters["Id"].Value = (object)genPK;
                //Employee Id
                if (tra.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, tra.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
                    else cmd.Parameters["EmpId"].Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters["EmpId"].Value = System.DBNull.Value;
                }
                //Course Name
                if (tra.IsCourseNameNull() == false)
                {
                    cmd.Parameters["CoursName"].Value = (object)tra.CourseName;
                }
                else
                {
                    cmd.Parameters["CoursName"].Value = System.DBNull.Value;
                }
                //Institute
                if (tra.IsInstituteNull() == false)
                {
                    cmd.Parameters["Institute"].Value = (object)tra.Institute;
                }
                else
                {
                    cmd.Parameters["Institute"].Value = System.DBNull.Value;
                }
                //Country
                if (tra.IsCountryidNull() == false)
                {
                    cmd.Parameters["Country"].Value = (object)tra.Countryid;
                }
                else
                {
                    cmd.Parameters["Country"].Value = System.DBNull.Value;
                }
                //Date From
                if (tra.IsDateFromNull() == false)
                {
                    cmd.Parameters["DateFrom"].Value = (object)tra.DateFrom;
                }
                else
                {
                    cmd.Parameters["DateFrom"].Value = System.DBNull.Value;
                }
                //Date To
                if (tra.IsDateToNull() == false)
                {
                    cmd.Parameters["DateTo"].Value = (object)tra.DateTo;
                }
                else
                {
                    cmd.Parameters["DateTo"].Value = System.DBNull.Value;
                }
                //Result
                if (tra.IsResultNull() == false)
                {
                    cmd.Parameters["Result"].Value = (object)tra.Result;
                }
                else
                {
                    cmd.Parameters["Result"].Value = System.DBNull.Value;
                }
                //Remarks
                if (tra.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)tra.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = System.DBNull.Value;
                }
                //Shakir, 13-3-2013
                //Major Topics
                if (tra.IsMajortopicIDNull() == false)
                {
                    cmd.Parameters["MajortopicID"].Value = (object)tra.MajortopicID;
                }
                else
                {
                    cmd.Parameters["MajortopicID"].Value = System.DBNull.Value;
                }
                //Duration
                if (tra.IsDurationNull() == false)
                {
                    cmd.Parameters["Duration"].Value = (object)tra.Duration;
                }
                else
                {
                    cmd.Parameters["Duration"].Value = System.DBNull.Value;
                }
                //Organized by
                if (tra.IsOrganizerIDNull() == false)
                {
                    cmd.Parameters["OrganizerID"].Value = (object)tra.OrganizerID;
                }
                else
                {
                    cmd.Parameters["OrganizerID"].Value = System.DBNull.Value;
                }
                //Vanue
                if (tra.IsVanueNull() == false)
                {
                    cmd.Parameters["Vanue"].Value = (object)tra.Vanue;
                }
                else
                {
                    cmd.Parameters["Vanue"].Value = System.DBNull.Value;
                }
                //Training Type
                if (tra.IsTrainingtypeNull() == false)
                {
                    cmd.Parameters["Trainingtype"].Value = (object)tra.Trainingtype;
                }
                else
                {
                    cmd.Parameters["Trainingtype"].Value = System.DBNull.Value;
                }
                //Release Type
                if (tra.IsReleasetypeNull() == false)
                {
                    cmd.Parameters["Releasetype"].Value = (object)tra.Releasetype;
                }
                else
                {
                    cmd.Parameters["Releasetype"].Value = System.DBNull.Value;
                }
                //Realease Date
                if (tra.IsReleasedateNull() == false)
                {
                    cmd.Parameters["Releasedate"].Value = (object)tra.Releasedate;
                }
                else
                {
                    cmd.Parameters["Releasedate"].Value = System.DBNull.Value;
                }
                //Final Payment
                if (tra.IsFinalpaymentNull() == false)
                {
                    cmd.Parameters["Finalpayment"].Value = (object)tra.Finalpayment;
                }
                else
                {
                    cmd.Parameters["Finalpayment"].Value = System.DBNull.Value;
                }
                //Payment Received
                if (tra.IsPaymentreceivedNull() == false)
                {
                    cmd.Parameters["Paymentreceived"].Value = (object)tra.Paymentreceived;
                }
                else
                {
                    cmd.Parameters["Paymentreceived"].Value = System.DBNull.Value;
                }
                //Deu Amount
                if (tra.IsDueamountNull() == false)
                {
                    cmd.Parameters["Dueamount"].Value = (object)tra.Dueamount;
                }
                else
                {
                    cmd.Parameters["Dueamount"].Value = System.DBNull.Value;
                }
                if (tra.IsZoneIdNull() == false)
                {
                    cmd.Parameters["ZoneID"].Value = (object)tra.ZoneId;
                }
                else
                {
                    cmd.Parameters["ZoneID"].Value = System.DBNull.Value;
                }
                if (tra.IsSiteIdNull() == false)
                {
                    cmd.Parameters["SiteID"].Value = (object)tra.SiteId;
                }
                else
                {
                    cmd.Parameters["SiteID"].Value = System.DBNull.Value;
                }
                if (tra.IsDepartmentIdNull() == false)
                {
                    cmd.Parameters["DepartmentID"].Value = (object)tra.DepartmentId;
                }
                else
                {
                    cmd.Parameters["DepartmentID"].Value = System.DBNull.Value;
                }

                if (!tra.IsFinancedByNull()) cmd.Parameters["FinancedBy"].Value = (object)tra.FinancedBy;
                else cmd.Parameters["FinancedBy"].Value = System.DBNull.Value;

                if (!tra.IsCertificateNameNull()) cmd.Parameters["CertificateName"].Value = (object)tra.CertificateName;
                else cmd.Parameters["CertificateName"].Value = System.DBNull.Value;

                if (!tra.IsIsSelfServiseNull() && tra.IsSelfServise)
                {
                    if (!tra.IsIsSubmittedNull()) cmd.Parameters["IsSubmitted"].Value = (object)tra.IsSubmitted;
                    else cmd.Parameters["IsSubmitted"].Value = false;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TRAINING_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteTraining(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            //DataStringDS stringDS = new DataStringDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (integerDS.DataIntegers[integerDS.DataIntegers.Count - 1].IntegerValue == 1)
            { cmd = DBCommandProvider.GetDBCommand("DeleteDataRenewalTraining"); }
            else if (integerDS.DataIntegers[integerDS.DataIntegers.Count - 1].IntegerValue == 2)
                { cmd = DBCommandProvider.GetDBCommand("DeleteSelfServTraining"); }
            else
            { cmd = DBCommandProvider.GetDBCommand("DeleteTraining"); }
            if (cmd == null) return UtilDL.GetCommandNotFound();

            // Delete FLAG_ROW
            integerDS.DataIntegers[integerDS.DataIntegers.Count - 1].Delete();
            integerDS.AcceptChanges();


            foreach (DataIntegerDS.DataInteger intVal in integerDS.DataIntegers)
            {
                cmd.Parameters["TrainingID"].Value = (object)intVal.IntegerValue;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TRAINING_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _getTrainingList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == "IsDataRenewal")
            {
                cmd = DBCommandProvider.GetDBCommand("GetDataRenewalTrainingList");
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            }
            else if (stringDS.DataStrings[1].StringValue == "IsSelfService")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServTrainingList");
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetTrainingList");
                cmd.Parameters["EmployeeCode"].Value = cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue.Trim();
                cmd.Parameters["DepartmentID1"].Value = cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue.Trim());
            }

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeHistDS hisDS = new EmployeeHistDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Training.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TRAINING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            hisDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateTraining(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            histDS.Merge(inputDS.Tables[histDS.Training.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            if (!histDS.Training[0].IsIsDataRenewalNull() && histDS.Training[0].IsDataRenewal) cmd = DBCommandProvider.GetDBCommand("UpdateDataRenewalTraining");
            else if (histDS.Training[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("UpdateSelfServTraining");
            else cmd = DBCommandProvider.GetDBCommand("UpdateTraining");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.TrainingRow tra in histDS.Training.Rows)
            {
                //Employee Id
                if (tra.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, tra.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
                    else cmd.Parameters["EmpId"].Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters["EmpId"].Value = System.DBNull.Value;
                }
                //Course Name
                if (tra.IsCourseNameNull() == false)
                {
                    cmd.Parameters["CoursName"].Value = (object)tra.CourseName;
                }
                else
                {
                    cmd.Parameters["CoursName"].Value = System.DBNull.Value;
                }
                //Institute
                if (tra.IsInstituteNull() == false)
                {
                    cmd.Parameters["Institute"].Value = (object)tra.Institute;
                }
                else
                {
                    cmd.Parameters["Institute"].Value = System.DBNull.Value;
                }
                //Country
                if (tra.IsCountryidNull() == false)
                {
                    cmd.Parameters["Country"].Value = (object)tra.Countryid;
                }
                else
                {
                    cmd.Parameters["Country"].Value = System.DBNull.Value;
                }
                //Date From
                if (tra.IsDateFromNull() == false)
                {
                    cmd.Parameters["DateFrom"].Value = (object)tra.DateFrom;
                }
                else
                {
                    cmd.Parameters["DateFrom"].Value = System.DBNull.Value;
                }
                //Date To
                if (tra.IsDateToNull() == false)
                {
                    cmd.Parameters["DateTo"].Value = (object)tra.DateTo;
                }
                else
                {
                    cmd.Parameters["DateTo"].Value = System.DBNull.Value;
                }
                //Result
                if (tra.IsResultNull() == false)
                {
                    cmd.Parameters["Result"].Value = (object)tra.Result;
                }
                else
                {
                    cmd.Parameters["Result"].Value = System.DBNull.Value;
                }
                //Remarks
                if (tra.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)tra.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = System.DBNull.Value;
                }
                //Shakir, 13-3-2013
                //Major Topics
                if (tra.IsMajortopicIDNull() == false)
                {
                    cmd.Parameters["MajortopicID"].Value = (object)tra.MajortopicID;
                }
                else
                {
                    cmd.Parameters["MajortopicID"].Value = System.DBNull.Value;
                }
                //Duration
                if (tra.IsDurationNull() == false)
                {
                    cmd.Parameters["Duration"].Value = (object)tra.Duration;
                }
                else
                {
                    cmd.Parameters["Duration"].Value = System.DBNull.Value;
                }
                //Organized by
                if (tra.IsOrganizerIDNull() == false)
                {
                    cmd.Parameters["OrganizerID"].Value = (object)tra.OrganizerID;
                }
                else
                {
                    cmd.Parameters["OrganizerID"].Value = System.DBNull.Value;
                }
                //Vanue
                if (tra.IsVanueNull() == false)
                {
                    cmd.Parameters["Vanue"].Value = (object)tra.Vanue;
                }
                else
                {
                    cmd.Parameters["Vanue"].Value = System.DBNull.Value;
                }

                //Training Type
                if (tra.IsTrainingtypeNull() == false)
                {
                    cmd.Parameters["Trainingtype"].Value = (object)tra.Trainingtype;
                }
                else
                {
                    cmd.Parameters["Trainingtype"].Value = System.DBNull.Value;
                }
                //Release Type
                if (tra.IsReleasetypeNull() == false)
                {
                    cmd.Parameters["Releasetype"].Value = (object)tra.Releasetype;
                }
                else
                {
                    cmd.Parameters["Releasetype"].Value = System.DBNull.Value;
                }
                //Realease Date
                if (tra.IsReleasedateNull() == false)
                {
                    cmd.Parameters["Releasedate"].Value = (object)tra.Releasedate;
                }
                else
                {
                    cmd.Parameters["Releasedate"].Value = System.DBNull.Value;
                }
                //Final Payment
                if (tra.IsFinalpaymentNull() == false)
                {
                    cmd.Parameters["Finalpayment"].Value = (object)tra.Finalpayment;
                }
                else
                {
                    cmd.Parameters["Finalpayment"].Value = System.DBNull.Value;
                }
                //Payment Received
                if (tra.IsPaymentreceivedNull() == false)
                {
                    cmd.Parameters["Paymentreceived"].Value = (object)tra.Paymentreceived;
                }
                else
                {
                    cmd.Parameters["Paymentreceived"].Value = System.DBNull.Value;
                }
                //Deu Amount
                if (tra.IsDueamountNull() == false)
                {
                    cmd.Parameters["Dueamount"].Value = (object)tra.Dueamount;
                }
                else
                {
                    cmd.Parameters["Dueamount"].Value = System.DBNull.Value;
                }
                if (tra.IsZoneIdNull() == false)
                {
                    cmd.Parameters["ZoneID"].Value = (object)tra.ZoneId;
                }
                else
                {
                    cmd.Parameters["ZoneID"].Value = System.DBNull.Value;
                }
                if (tra.IsSiteIdNull() == false)
                {
                    cmd.Parameters["SiteID"].Value = (object)tra.SiteId;
                }
                else
                {
                    cmd.Parameters["SiteID"].Value = System.DBNull.Value;
                }
                if (tra.IsDepartmentIdNull() == false)
                {
                    cmd.Parameters["DepartmentID"].Value = (object)tra.DepartmentId;
                }
                else
                {
                    cmd.Parameters["DepartmentID"].Value = System.DBNull.Value;
                }


                if (!tra.IsFinancedByNull()) cmd.Parameters["FinancedBy"].Value = (object)tra.FinancedBy;
                else cmd.Parameters["FinancedBy"].Value = System.DBNull.Value;

                if (!tra.IsCertificateNameNull()) cmd.Parameters["CertificateName"].Value = (object)tra.CertificateName;
                else cmd.Parameters["CertificateName"].Value = System.DBNull.Value;

                cmd.Parameters["TrainingID"].Value = (object)tra.TrainingId;



                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TRAINING_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _doesTrainingExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesTrainingExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_TRAINING_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }


        private DataSet _createEmployment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Employment.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Employment_Function.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //create command
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateEmployment");
            OleDbCommand cmd = new OleDbCommand();
            //if (histDS.Employment[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("CreateSelfServEmployment");
            //else cmd = DBCommandProvider.GetDBCommand("CreateEmployment");
            if (histDS.Employment[0].IsSelfServise) cmd.CommandText = "PRO_EMPLOYMENT_CREATE_PARK";
            else cmd.CommandText = "PRO_EMPLOYMENT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();
           
            #region Set Parameter Value
            foreach (EmployeeHistDS.EmploymentRow emp in histDS.Employment.Rows)
            {
                //long genPK = IDGenerator.GetNextGenericPK();
                //if (genPK == -1)
                //{
                //    UtilDL.GetDBOperationFailed();
                //}

                //cmd.Parameters["Id"].Value = (object)genPK;
                //Responsibility
                if (emp.IsResponsibilityNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Responsibility",emp.Responsibility);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Responsibility", System.DBNull.Value);
                }

                //code
                //if (emp.IsCodeNull() == false)
                //{
                //    cmd.Parameters["Code"].Value = (object)emp.Code;
                //}
                //else
                //{
                //    cmd.Parameters["Code"].Value = System.DBNull.Value;
                //}
                //Employee Id
                if (emp.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, emp.EmployeeCode);
                    if (empId > 0) cmd.Parameters.AddWithValue("p_EmpId", empId);
                    else cmd.Parameters.AddWithValue("p_EmpId", System.DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EmpId", System.DBNull.Value);
                }
                //Organization Name
                if (emp.IsOrganizationNameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Organization",emp.OrganizationName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Organization", System.DBNull.Value);
                }
                //Designation
                if (emp.IsDesignationNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Designation", emp.Designation);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Designation", System.DBNull.Value);
                }
                //Address
                if (emp.IsAddressNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Address", emp.Address);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Address", System.DBNull.Value);
                }
                //Date From
                if (emp.IsDateFromNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DateFrom", emp.DateFrom);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DateFrom", System.DBNull.Value);
                }
                //Date To
                if (emp.IsDateToNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DateTo",emp.DateTo);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DateTo", System.DBNull.Value);
                }
                //Remarks
                if (emp.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", emp.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", System.DBNull.Value);
                }

                //Shakir 16-10-2012
                if (emp.IsPreSalaryNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_PreSalary", emp.PreSalary);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_PreSalary", System.DBNull.Value);
                }

                if (emp.IsSUPERVISORYEXPERIENCENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SupervisoryExperience", emp.SUPERVISORYEXPERIENCE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SupervisoryExperience", System.DBNull.Value);
                }

                if (emp.IsSUPERVISORYRESPOSIBILTYNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SuperVisoryResponsivility",emp.SUPERVISORYRESPOSIBILTY);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SuperVisoryResponsivility", System.DBNull.Value);
                }

                if (emp.IsUNDERSUPERVISORYEXPNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_UnderSupervisoryExp",emp.UNDERSUPERVISORYEXP);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_UnderSupervisoryExp", System.DBNull.Value);
                }

                if (emp.IsUNDERSUPRESPONSIVILITYNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_UnderSupResponsibilty", emp.UNDERSUPRESPONSIVILITY);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_UnderSupResponsibilty", System.DBNull.Value);
                }

                if (emp.IsLASTSUPERVISORNAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LastSuperVisorName", emp.LASTSUPERVISORNAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LastSuperVisorName", System.DBNull.Value);
                }

                if (emp.IsLASTSUPERVISORCELLNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LastSupervisorCell", emp.LASTSUPERVISORCELL);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LastSupervisorCell", System.DBNull.Value);
                }


                if (emp.IsLASTSUPERVISORADDRESSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LastSupervisorAddress", emp.LASTSUPERVISORADDRESS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LastSupervisorAddress", System.DBNull.Value);
                }
                //End

                if (!emp.IsOrganizationTypeIDNull()) cmd.Parameters.AddWithValue("p_ORGANIZATIONTYPEID",emp.OrganizationTypeID);
                else cmd.Parameters.AddWithValue("p_ORGANIZATIONTYPEID", System.DBNull.Value);
                //End OrganizationTypeID

                cmd.Parameters.Add("p_EmploymentID", OleDbType.Numeric, 22).Direction = ParameterDirection.Output;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                long EmploymentID = Convert.ToInt64(cmd.Parameters["p_EmploymentID"].Value.ToString());

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYMENT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (histDS.Employment[0].IsSelfServise) errDS.Merge(_createEmployment_Function(inputDS, connDS, EmploymentID));
            }
            #endregion

            #region 
            //OleDbCommand cmd_Func_del = new OleDbCommand();
            //cmd_Func_del.CommandText = "PRO_ADDITINAL_JOB_HISTORY_DEL";
            //cmd_Func_del.CommandType = CommandType.StoredProcedure;

            //OleDbCommand cmd_Func = new OleDbCommand();
            //cmd_Func.CommandText = "PRO_ADDITINAL_JOB_HISTORY_ADD";
            //cmd_Func.CommandType = CommandType.StoredProcedure;

            //if (cmd_Func == null || cmd_Func_del == null) return UtilDL.GetCommandNotFound();

            //#region Delete
            //foreach (EmployeeHistDS.EmploymentRow row in histDS.Employment.Rows)
            //{
            //    cmd_Func_del.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);

            //    bool bErrorDel = false;
            //    int nRowAffectedDel = -1;
            //    nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmd_Func_del, connDS.DBConnections[0].ConnectionID, ref bErrorDel);
            //    cmd_Func_del.Parameters.Clear();
            //    if (bErrorDel)
            //    {
            //        ErrorDS.Error err = errDS.Errors.NewError();
            //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //        err.ErrorInfo1 = ActionID.ACTION_EMPLOYMENT_ADD.ToString();
            //        errDS.Errors.AddError(err);
            //        errDS.AcceptChanges();
            //        return errDS;
            //    }
            //}
            //#endregion

            //#region Create

            //foreach (EmployeeHistDS.EmploymentRow row in histDS.Employment.Rows)
            //{
            //    long genPK = IDGenerator.GetNextGenericPK();
            //    if (genPK == -1) return UtilDL.GetDBOperationFailed();

            //    //cmd.Parameters.AddWithValue("p_AdditionaljobhistID", genPK);
            //    //cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
            //    //cmd.Parameters.AddWithValue("p_SiteCode", row.Sitecode);
            //    //cmd.Parameters.AddWithValue("p_CompanyDivisionID", row.CompanyDivisionID);
            //    //cmd.Parameters.AddWithValue("p_DepartmentCode", row.DepartmentCode);
            //    //cmd.Parameters.AddWithValue("p_FunctionCode", row.Functioncode);
            //    //cmd.Parameters.AddWithValue("p_DateFrom", row.DateFrom);
            //    //cmd.Parameters.AddWithValue("p_DateTo", row.DateTo);

            //    //if (!row.IsLiabilityIDNull()) cmd.Parameters.AddWithValue("p_LiabilityID", row.LiabilityID);
            //    //else cmd.Parameters.AddWithValue("p_LiabilityID", DBNull.Value);

            //    //if (!row.IsReporting_Func_Desig_IDNull()) cmd.Parameters.AddWithValue("p_Reporting_Func_Desig_ID", row.Reporting_Func_Desig_ID);
            //    //else cmd.Parameters.AddWithValue("p_Reporting_Func_Desig_ID", DBNull.Value);

            //    //if (!row.IsSupervisorcodeNull()) cmd.Parameters.AddWithValue("p_SupervisorCode", row.Supervisorcode);
            //    //else cmd.Parameters.AddWithValue("p_SupervisorCode", DBNull.Value);

            //    //if (!row.IsCommentsNull()) cmd.Parameters.AddWithValue("p_Comments", row.Comments);
            //    //else cmd.Parameters.AddWithValue("p_Comments", DBNull.Value);

            //    bool bError = false;
            //    int nRowAffected = -1;
            //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Func, connDS.DBConnections[0].ConnectionID,
            //       ref bError);
            //    cmd_Func.Parameters.Clear();
            //    if (bError)
            //    {
            //        ErrorDS.Error err = errDS.Errors.NewError();
            //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //        err.ErrorInfo1 = ActionID.ACTION_EMPLOYMENT_ADD.ToString();
            //        errDS.Errors.AddError(err);
            //        errDS.AcceptChanges();
            //        return errDS;
            //    }
            //    //if (nRowAffected > 0) isSucceeded = true;
            //    //else isNotSucceeded = true;
            //}
            ////if (isSucceeded && !isNotSucceeded) messageDS.SuccessMsg.AddSuccessMsgRow(historyDS.EmployeeHistories[0].EmployeeCode + " - " + historyDS.EmployeeHistories[0].EmployeeName);
            ////else messageDS.ErrorMsg.AddErrorMsgRow(historyDS.EmployeeHistories[0].EmployeeCode + " - " + historyDS.EmployeeHistories[0].EmployeeName);
            ////messageDS.AcceptChanges();
            //#endregion
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteEmployment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            //DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            #region Delete
            OleDbCommand cmd_del = new OleDbCommand();
            cmd_del.CommandText = "PRO_EMPLOYMENT_FUNC_DELETE";
            cmd_del.CommandType = CommandType.StoredProcedure;
           
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            if (integerDS.DataIntegers[integerDS.DataIntegers.Count - 1].IntegerValue == 1) // 1 for SelfService, 0 for General
            { cmd = DBCommandProvider.GetDBCommand("DeleteSelfServEmployment"); }
            else
            { cmd = DBCommandProvider.GetDBCommand("DeleteEmployment"); }
            if (cmd == null) return UtilDL.GetCommandNotFound();

            // Delete FLAG_ROW
            integerDS.DataIntegers[integerDS.DataIntegers.Count - 1].Delete();
            integerDS.AcceptChanges();
            foreach (DataIntegerDS.DataInteger intVal in integerDS.DataIntegers)
            {
                cmd_del.Parameters.AddWithValue("p_EmploymentID", intVal.IntegerValue);

                bool bErrorDel = false;
                int nRowAffectedDel = -1;
                nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bErrorDel);
                cmd_del.Parameters.Clear();
                if (bErrorDel)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYMENT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            foreach (DataIntegerDS.DataInteger intVal in integerDS.DataIntegers)
            {
                cmd.Parameters["EmploymentID"].Value = (object)intVal.IntegerValue;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYMENT_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _getEmploymentList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmploymentList");
            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == true.ToString())
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServEmploymentList");
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmploymentList");

                long empId = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
                cmd.Parameters["EmployeeId"].Value = empId;
            }

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeHistDS hisDS = new EmployeeHistDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Employment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYMENT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            hisDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateEmployment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            histDS.Merge(inputDS.Tables[histDS.Employment.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Employment_Function.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //create command
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateEmployment");
            OleDbCommand cmd = new OleDbCommand();
            if (histDS.Employment[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("UpdateSelfServEmployment");
            else cmd = DBCommandProvider.GetDBCommand("UpdateEmployment");
            if (cmd == null) return UtilDL.GetCommandNotFound();


            foreach (EmployeeHistDS.EmploymentRow emp in histDS.Employment.Rows)
            {
                ////code
                //if (emp.IsCodeNull() == false)
                //{
                //    cmd.Parameters["Code"].Value = (object)emp.Code;
                //}
                //else
                //{
                //    cmd.Parameters["Code"].Value = System.DBNull.Value;
                //}

                //Employment ID
                if (emp.IsEmploymentIdNull() == false)
                {
                    cmd.Parameters["EmploymentId"].Value = (object)emp.EmploymentId;
                }
                else
                {
                    cmd.Parameters["EmploymentId"].Value = System.DBNull.Value;
                }

                //Responsibility
                if (emp.IsResponsibilityNull() == false)
                {
                    cmd.Parameters["Responsibility"].Value = (object)emp.Responsibility;
                }
                else
                {
                    cmd.Parameters["Responsibility"].Value = System.DBNull.Value;
                }


                ////Employee Id
                if (emp.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, emp.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
                    else cmd.Parameters["EmpId"].Value = System.DBNull.Value;
                }
                else
                {
                    cmd.Parameters["EmpId"].Value = System.DBNull.Value;
                }
                //Organization Name
                if (emp.IsOrganizationNameNull() == false)
                {
                    cmd.Parameters["Organization"].Value = (object)emp.OrganizationName;
                }
                else
                {
                    cmd.Parameters["Organization"].Value = System.DBNull.Value;
                }
                //Designation
                if (emp.IsDesignationNull() == false)
                {
                    cmd.Parameters["Designation"].Value = (object)emp.Designation;
                }
                else
                {
                    cmd.Parameters["Designation"].Value = System.DBNull.Value;
                }
                //Address
                if (emp.IsAddressNull() == false)
                {
                    cmd.Parameters["Address"].Value = (object)emp.Address;
                }
                else
                {
                    cmd.Parameters["Address"].Value = System.DBNull.Value;
                }
                //Date From
                if (emp.IsDateFromNull() == false)
                {
                    cmd.Parameters["DateFrom"].Value = (object)emp.DateFrom;
                }
                else
                {
                    cmd.Parameters["DateFrom"].Value = System.DBNull.Value;
                }
                //Date To
                if (emp.IsDateToNull() == false)
                {
                    cmd.Parameters["DateTo"].Value = (object)emp.DateTo;
                }
                else
                {
                    cmd.Parameters["DateTo"].Value = System.DBNull.Value;
                }
                //Remarks
                if (emp.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)emp.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = System.DBNull.Value;
                }

                //Shakir 16-10-2012 **************************

                if (emp.IsPreSalaryNull() == false)
                {
                    cmd.Parameters["PreSalary"].Value = (object)emp.PreSalary;
                }
                else
                {
                    cmd.Parameters["PreSalary"].Value = System.DBNull.Value;
                }

                if (emp.IsSUPERVISORYEXPERIENCENull() == false)
                {
                    cmd.Parameters["SuperVaisoryExperience"].Value = (object)emp.SUPERVISORYEXPERIENCE;
                }
                else
                {
                    cmd.Parameters["SuperVaisoryExperience"].Value = System.DBNull.Value;
                }

                if (emp.IsSUPERVISORYRESPOSIBILTYNull() == false)
                {
                    cmd.Parameters["SupResponsivility"].Value = (object)emp.SUPERVISORYRESPOSIBILTY;
                }
                else
                {
                    cmd.Parameters["SupResponsivility"].Value = System.DBNull.Value;
                }

                if (emp.IsUNDERSUPERVISORYEXPNull() == false)
                {
                    cmd.Parameters["UnderSupExp"].Value = (object)emp.UNDERSUPERVISORYEXP;
                }
                else
                {
                    cmd.Parameters["UnderSupExp"].Value = System.DBNull.Value;
                }


                if (emp.IsUNDERSUPRESPONSIVILITYNull() == false)
                {
                    cmd.Parameters["UnderSupResponsibility"].Value = (object)emp.UNDERSUPRESPONSIVILITY;
                }
                else
                {
                    cmd.Parameters["UnderSupResponsibility"].Value = System.DBNull.Value;
                }

                if (emp.IsLASTSUPERVISORNAMENull() == false)
                {
                    cmd.Parameters["LastSupName"].Value = (object)emp.LASTSUPERVISORNAME;
                }
                else
                {
                    cmd.Parameters["LastSupName"].Value = System.DBNull.Value;
                }

                if (emp.IsLASTSUPERVISORCELLNull() == false)
                {
                    cmd.Parameters["LastSupCell"].Value = (object)emp.LASTSUPERVISORCELL;
                }
                else
                {
                    cmd.Parameters["LastSupCell"].Value = System.DBNull.Value;
                }

                if (emp.IsLASTSUPERVISORADDRESSNull() == false)
                {
                    cmd.Parameters["LastSupAddress"].Value = (object)emp.LASTSUPERVISORADDRESS;
                }
                else
                {
                    cmd.Parameters["LastSupAddress"].Value = System.DBNull.Value;
                }

                if (!emp.IsOrganizationTypeIDNull()) cmd.Parameters["ORGANIZATIONTYPEID"].Value = (object)emp.OrganizationTypeID;
                else cmd.Parameters["ORGANIZATIONTYPEID"].Value = System.DBNull.Value;
                //End OrganizationTypeID


                //End
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYMENT_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (histDS.Employment[0].IsSelfServise) errDS.Merge(_createEmployment_Function(inputDS, connDS, emp.EmploymentId));
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _doesEmploymentExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesEmploymentExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_EMPLOYMENT_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }


        private DataSet _createReference(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            histDS.Merge(inputDS.Tables[histDS.Reference.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            if (histDS.Reference[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("CreateSelfServiceReference");
            else cmd = DBCommandProvider.GetDBCommand("CreaterReference");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistDS.ReferenceRow Ref in histDS.Reference.Rows)
            {
                //long genPK = IDGenerator.GetNextGenericPK();
                //if (genPK == -1)
                //{
                //    UtilDL.GetDBOperationFailed();
                //}

                //cmd.Parameters["REFERENCEID"].Value = (object)genPK;

                if (Ref.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, Ref.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EMPID"].Value = (object)empId;
                    else cmd.Parameters["EMPID"].Value = null;
                }
                else
                {
                    cmd.Parameters["EMPID"].Value = null;
                }


                #region New
                if (Ref.IsReqruitmentChannelNull() == false) cmd.Parameters["ReqruitmentChannel"].Value = (object)Ref.ReqruitmentChannel;
                else cmd.Parameters["ReqruitmentChannel"].Value = DBNull.Value;

                if (Ref.IsEmpReferenceIdNull() == false) cmd.Parameters["EmpReferenceID"].Value = (object)Ref.EmpReferenceId;
                else cmd.Parameters["EmpReferenceID"].Value = DBNull.Value;

                if (Ref.IsRelationNull() == false) cmd.Parameters["Relation"].Value = (object)Ref.Relation;
                else cmd.Parameters["Relation"].Value = DBNull.Value;

                if (Ref.IsReqChennalDateNull() == false) cmd.Parameters["ReqChennalDate"].Value = (object)Ref.ReqChennalDate;
                else cmd.Parameters["ReqChennalDate"].Value = DBNull.Value;

                if (Ref.IsRemarksNull() == false) cmd.Parameters["Remarks"].Value = (object)Ref.Remarks;
                else cmd.Parameters["Remarks"].Value = DBNull.Value;

                #endregion



                ////Reference Name

                //if (Ref.IsReferenceNameNull() == false)
                //{
                //    cmd.Parameters["REFERENCENAME"].Value = (object)Ref.ReferenceName;
                //}
                //else
                //{
                //    cmd.Parameters["REFERENCENAME"].Value = null;
                //}

                ////Organization Name
                //if (Ref.IsOrganizationNameNull() == false)
                //{
                //    cmd.Parameters["ORGANIZATIONNAME"].Value = (object)Ref.OrganizationName;
                //}
                //else
                //{
                //    cmd.Parameters["ORGANIZATIONNAME"].Value = null;
                //}

                ////Designation
                //if (Ref.IsDesignationNull() == false)
                //{
                //    cmd.Parameters["DESIGNATION"].Value = (object)Ref.Designation;
                //}
                //else
                //{
                //    cmd.Parameters["DESIGNATION"].Value = null;
                //}


                ////Gender
                //if (Ref.IsGenderNull() == false)
                //{
                //    cmd.Parameters["GENDER"].Value = (object)Ref.Gender;
                //}
                //else
                //{
                //    cmd.Parameters["GENDER"].Value = null;
                //}

                ////Nationality
                //if (Ref.IsNationalityNull() == false)
                //{
                //    cmd.Parameters["NATIONALITY"].Value = (object)Ref.Nationality;
                //}
                //else
                //{
                //    cmd.Parameters["NATIONALITY"].Value = null;
                //}

                ////Mobile
                //if (Ref.IsMobileNull() == false)
                //{
                //    cmd.Parameters["MOBILE"].Value = (object)Ref.Mobile;
                //}
                //else
                //{
                //    cmd.Parameters["MOBILE"].Value = null;
                //}

                ////Office Phone
                //if (Ref.IsOfficePhoneNull() == false)
                //{
                //    cmd.Parameters["OFFICEPHONE"].Value = (object)Ref.OfficePhone;
                //}
                //else
                //{
                //    cmd.Parameters["OFFICEPHONE"].Value = null;
                //}
                ////Email Address
                //if (Ref.IsEmailNull() == false)
                //{
                //    cmd.Parameters["EMAIL"].Value = (object)Ref.Email;
                //}
                //else
                //{
                //    cmd.Parameters["EMAIL"].Value = null;
                //}


                ////Remarks
                //if (Ref.IsRemarksNull() == false)
                //{
                //    cmd.Parameters["REMARKS"].Value = (object)Ref.Remarks;
                //}
                //else
                //{
                //    cmd.Parameters["REMARKS"].Value = null;
                //}

                ////Address
                //if (Ref.IsAddressNull() == false)
                //{
                //    cmd.Parameters["Address"].Value = (object)Ref.Address;
                //}
                //else
                //{
                //    cmd.Parameters["Address"].Value = null;
                //}
                ////Shakir, 13-3-13
                ////Relation
                //if (Ref.IsRelationNull() == false)
                //{
                //    cmd.Parameters["Relation"].Value = (object)Ref.Relation;
                //}
                //else
                //{
                //    cmd.Parameters["Relation"].Value = null;
                //}

                ////Rony :: 08 Sep 2016
                //if (!Ref.IsRef_NationalIDNull()) cmd.Parameters["Ref_NationalID"].Value = (object)Ref.Ref_NationalID;
                //else cmd.Parameters["Ref_NationalID"].Value = DBNull.Value;

                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHILDREN_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _doesReferenceExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesReferenceExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["ReferenceCode"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_CHILDREN_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getReferenceList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == true.ToString())
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServiceReferenceList");
                cmd.Parameters["EmployeeCode"].Value = EmployeeCode;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetReferenceList");

                long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
                cmd.Parameters["EmployeeId"].Value = empId;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            EmployeeHistDS hisDS = new EmployeeHistDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Reference.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REFERENCE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            hisDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _updateReference(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Reference.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //create command
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateReference");

            OleDbCommand cmd = new OleDbCommand();
            if (histDS.Reference[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("UpdateSelfServiceReference");
            else cmd = DBCommandProvider.GetDBCommand("UpdateReference");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            foreach (EmployeeHistDS.ReferenceRow Ref in histDS.Reference.Rows)
            {
          
                //if (Ref.IsEmployeeCodeNull() == false)
                //{
                //    long empId = UtilDL.GetEmployeeId(connDS, Ref.EmployeeCode);
                //    if (empId > 0) cmd.Parameters["EMPID"].Value = (object)empId;
                //    else cmd.Parameters["EMPID"].Value = null;
                //}
                //else
                //{
                //    cmd.Parameters["EMPID"].Value = null;
                //}
                ////Reference Name

                //if (Ref.IsReferenceNameNull() == false)
                //{
                //    cmd.Parameters["REFERENCENAME"].Value = (object)Ref.ReferenceName;
                //}
                //else
                //{
                //    cmd.Parameters["REFERENCENAME"].Value = null;
                //}

                ////Organization Name
                //if (Ref.IsOrganizationNameNull() == false)
                //{
                //    cmd.Parameters["ORGANIZATIONNAME"].Value = (object)Ref.OrganizationName;
                //}
                //else
                //{
                //    cmd.Parameters["ORGANIZATIONNAME"].Value = null;
                //}

                ////Designation
                //if (Ref.IsDesignationNull() == false)
                //{
                //    cmd.Parameters["DESIGNATION"].Value = (object)Ref.Designation;
                //}
                //else
                //{
                //    cmd.Parameters["DESIGNATION"].Value = null;
                //}

                ////Gender
                //if (Ref.IsGenderNull() == false)
                //{
                //    cmd.Parameters["GENDER"].Value = (object)Ref.Gender;
                //}
                //else
                //{
                //    cmd.Parameters["GENDER"].Value = null;
                //}

                ////Nationality
                //if (Ref.IsNationalityNull() == false)
                //{
                //    cmd.Parameters["NATIONALITY"].Value = (object)Ref.Nationality;
                //}
                //else
                //{
                //    cmd.Parameters["NATIONALITY"].Value = null;
                //}

                ////Mobile
                //if (Ref.IsMobileNull() == false)
                //{
                //    cmd.Parameters["MOBILE"].Value = (object)Ref.Mobile;
                //}
                //else
                //{
                //    cmd.Parameters["MOBILE"].Value = null;
                //}

                ////Office Phone
                //if (Ref.IsOfficePhoneNull() == false)
                //{
                //    cmd.Parameters["OFFICEPHONE"].Value = (object)Ref.OfficePhone;
                //}
                //else
                //{
                //    cmd.Parameters["OFFICEPHONE"].Value = null;
                //}
                ////Email Address
                //if (Ref.IsEmailNull() == false)
                //{
                //    cmd.Parameters["EMAIL"].Value = (object)Ref.Email;
                //}
                //else
                //{
                //    cmd.Parameters["EMAIL"].Value = null;
                //}

                ////Remarks
                //if (Ref.IsRemarksNull() == false)
                //{
                //    cmd.Parameters["REMARKS"].Value = (object)Ref.Remarks;
                //}
                //else
                //{
                //    cmd.Parameters["REMARKS"].Value = null;
                //}

                ////Address
                //if (Ref.IsAddressNull() == false)
                //{
                //    cmd.Parameters["ADDRESS"].Value = (object)Ref.Address;
                //}
                //else
                //{
                //    cmd.Parameters["ADDRESS"].Value = null;
                //}
                ////Shakir, 13-3-2013
                ////Relation
                //if (Ref.IsRelationNull() == false)
                //{
                //    cmd.Parameters["Relation"].Value = (object)Ref.Relation;
                //}
                //else
                //{
                //    cmd.Parameters["Relation"].Value = null;
                //}

                ////Rony :: 08 Sep 2016
                //if (!Ref.IsRef_NationalIDNull()) cmd.Parameters["Ref_NationalID"].Value = (object)Ref.Ref_NationalID;
                //else cmd.Parameters["Ref_NationalID"].Value = DBNull.Value;


                if (Ref.IsReqruitmentChannelNull() == false) cmd.Parameters["ReqruitmentChannel"].Value = (object)Ref.ReqruitmentChannel;
                else cmd.Parameters["ReqruitmentChannel"].Value = DBNull.Value;

                if (Ref.IsEmpReferenceIdNull() == false) cmd.Parameters["EmpReferenceID"].Value = (object)Ref.EmpReferenceId;
                else cmd.Parameters["EmpReferenceID"].Value = DBNull.Value;

                if (Ref.IsRelationNull() == false) cmd.Parameters["Relation"].Value = (object)Ref.Relation;
                else cmd.Parameters["Relation"].Value = DBNull.Value;

                if (Ref.IsReqChennalDateNull() == false) cmd.Parameters["ReqChennalDate"].Value = (object)Ref.ReqChennalDate;
                else cmd.Parameters["ReqChennalDate"].Value = DBNull.Value;

                if (Ref.IsRemarksNull() == false) cmd.Parameters["Remarks"].Value = (object)Ref.Remarks;
                else cmd.Parameters["Remarks"].Value = DBNull.Value;

                cmd.Parameters["ReferenceID"].Value = (object)Ref.ReferenceID;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REFERENCE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteReference(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //DataStringDS stringDS = new DataStringDS();
            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteReferees");
            OleDbCommand cmd = new OleDbCommand();
            if (integerDS.DataIntegers[integerDS.DataIntegers.Count - 1].IntegerValue == 1)
            {
                cmd = DBCommandProvider.GetDBCommand("DeleteSelfServiceReferees");
            }
            else cmd = DBCommandProvider.GetDBCommand("DeleteReferees");

            integerDS.DataIntegers[integerDS.DataIntegers.Count - 1].Delete();
            integerDS.AcceptChanges();
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataIntegerDS.DataInteger intVal in integerDS.DataIntegers)
            {
                cmd.Parameters["ReferenceID"].Value = (object)intVal.IntegerValue;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REFERENCE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            return errDS;  // return empty ErrorDS
        }

        /*private DataSet _deleteEducation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteEducation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EDUCATION_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _updateEducation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateEducation");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            histDS.Merge(inputDS.Tables[histDS.Education.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();


            foreach (EmployeeHistDS.EducationRow edu in histDS.Education.Rows)
            {
                //code
                if (edu.IsCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)edu.Code;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }
                //Employee Id
                if (edu.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, edu.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
                    else cmd.Parameters["EmpId"].Value = null;
                }
                else
                {
                    cmd.Parameters["EmpId"].Value = null;
                }
                //Degree
                if (edu.IsDegreeNull() == false)
                {
                    cmd.Parameters["Degree"].Value = (object)edu.Degree;
                }
                else
                {
                    cmd.Parameters["Degree"].Value = null;
                }
                //Institute
                if (edu.IsInstituteNull() == false)
                {
                    cmd.Parameters["Institute"].Value = (object)edu.Institute;
                }
                else
                {
                    cmd.Parameters["Institute"].Value = null;
                }
                //Board
                if (edu.IsBoardNull() == false)
                {
                    cmd.Parameters["Board"].Value = (object)edu.Board;
                }
                else
                {
                    cmd.Parameters["Board"].Value = null;
                }
                //Year
                if (edu.IsYearNull() == false)
                {
                    cmd.Parameters["Year"].Value = (object)edu.Year;
                }
                else
                {
                    cmd.Parameters["Year"].Value = null;
                }
                //Result
                if (edu.IsResultNull() == false)
                {
                    cmd.Parameters["Result"].Value = (object)edu.Result;
                }
                else
                {
                    cmd.Parameters["Result"].Value = null;
                }
                //Remarks
                if (edu.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)edu.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EDUCATION_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _doesEducationExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesEducationExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_EDUCATION_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }*/

        private DataSet _createAttachment(DataSet inputDS)
        {
            #region OLD :: WALI :: 24-Feb-2015
            //ErrorDS errDS = new ErrorDS();
            //EmployeeHistDS histDS = new EmployeeHistDS();
            //DBConnectionDS connDS = new DBConnectionDS();

            ////extract dbconnection 
            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();

            ////create command
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateAttachment");
            //if (cmd == null)
            //{
            //    return UtilDL.GetCommandNotFound();
            //}

            //histDS.Merge(inputDS.Tables[histDS.Attachment.TableName], false, MissingSchemaAction.Error);
            //histDS.AcceptChanges();

            //foreach (EmployeeHistDS.AttachmentRow Compt in histDS.Attachment.Rows)
            //{
            //    long genPK = IDGenerator.GetNextGenericPK();
            //    if (genPK == -1)
            //    {
            //        UtilDL.GetDBOperationFailed();
            //    }

            //    cmd.Parameters["attachmentId"].Value = (object)genPK;
            //    //CompetencyCode
            //    if (Compt.IsFileNameNull() == false)
            //    {
            //        cmd.Parameters["FileName"].Value = (object)Compt.FileName;
            //    }
            //    else
            //    {
            //        cmd.Parameters["FileName"].Value = null;
            //    }
            //    //Employee Id
            //    if (Compt.IsEmployeeCodeNull() == false)
            //    {
            //        long empId = UtilDL.GetEmployeeId(connDS, Compt.EmployeeCode);
            //        if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
            //        else cmd.Parameters["EmpId"].Value = null;
            //    }
            //    else
            //    {
            //        cmd.Parameters["EmpId"].Value = null;
            //    }

            //    //Purpose
            //    if (Compt.IsPurposeNull() == false)
            //    {
            //        cmd.Parameters["Purpose"].Value = (object)Compt.Purpose;
            //    }
            //    else
            //    {
            //        cmd.Parameters["Purpose"].Value = null;
            //    }

            //    //Remarks
            //    if (Compt.IsRemarksNull() == false)
            //    {
            //        cmd.Parameters["Remarks"].Value = (object)Compt.Remarks;
            //    }
            //    else
            //    {
            //        cmd.Parameters["Remarks"].Value = null;
            //    }


            //    bool bError = false;
            //    int nRowAffected = -1;
            //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            //    if (bError == true)
            //    {
            //        ErrorDS.Error err = errDS.Errors.NewError();
            //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //        err.ErrorInfo1 = ActionID.ACTION_ATTACHMENT_ADD.ToString();
            //        errDS.Errors.AddError(err);
            //        errDS.AcceptChanges();
            //        return errDS;
            //    }
            //}
            //errDS.Clear();
            //errDS.AcceptChanges();
            //return errDS;
            #endregion

            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            histDS.Merge(inputDS.Tables[histDS.Attachment.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            long empId = UtilDL.GetEmployeeId(connDS, histDS.Attachment[0].EmployeeCode);
            long senderEmpId = UtilDL.GetEmployeeId(connDS, histDS.Attachment[0].SenderEmployeeCode);

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_FILE_ATTACHMENT_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.AttachmentRow row in histDS.Attachment.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_AttachmentID", genPK);
                cmd.Parameters.AddWithValue("p_FileName", row.FileName);
                cmd.Parameters.AddWithValue("p_EmployeeID", empId);

                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                cmd.Parameters.AddWithValue("p_Purpose", row.Purpose);

                if (!row.IsReferenceIDNull()) cmd.Parameters.AddWithValue("p_ReferenceID", histDS.Attachment[0].ReferenceID);
                else cmd.Parameters.AddWithValue("p_ReferenceID", DBNull.Value);

                cmd.Parameters.AddWithValue("p_ReferenceName", row.ReferenceName);
                cmd.Parameters.AddWithValue("p_ActualFileName", row.ActualFileName);
                cmd.Parameters.AddWithValue("p_SenderEmployeeID", senderEmpId);

                if (!row.IsIssueDateNull()) cmd.Parameters.AddWithValue("p_IssueDate", row.IssueDate);
                else cmd.Parameters.AddWithValue("p_IssueDate", DBNull.Value);

                if (!row.IsExpiryDateNull()) cmd.Parameters.AddWithValue("p_ExpiryDate", row.ExpiryDate);
                else cmd.Parameters.AddWithValue("p_ExpiryDate", DBNull.Value);

                if (!row.IsReceiverEmployeeIDNull()) cmd.Parameters.AddWithValue("p_ReceiverEmployeeID", row.ReceiverEmployeeID);
                else cmd.Parameters.AddWithValue("p_ReceiverEmployeeID", DBNull.Value);

                if (!row.IsReceiverDepartmentIDNull()) cmd.Parameters.AddWithValue("p_ReceiverDepartmentID", row.ReceiverDepartmentID);
                else cmd.Parameters.AddWithValue("p_ReceiverDepartmentID", DBNull.Value);

                bool bError2 = false;
                int nRowAffected2 = -1;
                nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError2);
                cmd.Parameters.Clear();

                if (bError2)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ATTACHMENT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getAttachmentList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string EmployeeCode = "0";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();            

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            
            
            
            long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
            OleDbCommand cmd;
            if (stringDS.DataStrings[0].StringValue != "") EmployeeCode = stringDS.DataStrings[0].StringValue;
            if (stringDS.DataStrings[2].StringValue == "0")
            {
                cmd = DBCommandProvider.GetDBCommand("GetAttachmentList_NotUploaded");

                cmd.Parameters["CertificateID1"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["CertificateID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["EmployeeCode1"].Value = EmployeeCode;
                cmd.Parameters["EmployeeCode2"].Value = EmployeeCode;
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["CompanyCode1"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["CompanyCode2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["LocationCode1"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["LocationCode2"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["GradeCode1"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["GradeCode2"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["EmployeeType1"].Value = Convert.ToInt32(stringDS.DataStrings[9].StringValue);
                cmd.Parameters["EmployeeType2"].Value = Convert.ToInt32(stringDS.DataStrings[9].StringValue);
                cmd.Parameters["EmployeeStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[10].StringValue);
                cmd.Parameters["EmployeeStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[10].StringValue);

            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetAttachmentList_Uploaded");
                cmd.Parameters["CertificateID1"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["CertificateID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["EmployeeCode1"].Value = EmployeeCode;
                cmd.Parameters["EmployeeCode2"].Value = EmployeeCode;
                cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["CompanyCode1"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["CompanyCode2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[6].StringValue;
                cmd.Parameters["LocationCode1"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["LocationCode2"].Value = stringDS.DataStrings[7].StringValue;
                cmd.Parameters["GradeCode1"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["GradeCode2"].Value = stringDS.DataStrings[8].StringValue;
                cmd.Parameters["EmployeeType1"].Value = Convert.ToInt32(stringDS.DataStrings[9].StringValue);
                cmd.Parameters["EmployeeType2"].Value = Convert.ToInt32(stringDS.DataStrings[9].StringValue);
                cmd.Parameters["EmployeeStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[10].StringValue);
                cmd.Parameters["EmployeeStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[10].StringValue);


                cmd.Parameters["IssueDateFrom1"].Value = cmd.Parameters["IssueDateFrom2"].Value = Convert.ToDateTime(stringDS.DataStrings[11].StringValue);
                cmd.Parameters["IssueDateTo1"].Value = cmd.Parameters["IssueDateTo2"].Value = Convert.ToDateTime(stringDS.DataStrings[12].StringValue);
                cmd.Parameters["ExpiryDateFrom1"].Value = cmd.Parameters["ExpiryDateFrom2"].Value = Convert.ToDateTime(stringDS.DataStrings[13].StringValue);
                cmd.Parameters["ExpiryDateTo1"].Value = cmd.Parameters["ExpiryDateTo2"].Value = Convert.ToDateTime(stringDS.DataStrings[14].StringValue);
     
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            EmployeeHistDS hisDS = new EmployeeHistDS();

            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Attachment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATTACHMENT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            hisDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _createDisciplinaryType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateDisciplinaryType");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            histDS.Merge(inputDS.Tables[histDS.Disciplinary.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            foreach (EmployeeHistDS.DisciplinaryRow dis in histDS.Disciplinary.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["TYPEID"].Value = (object)genPK;

                if (dis.IsDisciplinaryTypeCodeNull() == false) cmd.Parameters["TYPECODE"].Value = (object)dis.DisciplinaryTypeCode;
                else cmd.Parameters["TYPECODE"].Value = null;

                if (dis.IsDisciplinaryTypeNull() == false) cmd.Parameters["TYPENAME"].Value = (object)dis.DisciplinaryType;
                else cmd.Parameters["TYPENAME"].Value = null;

                if (dis.IsRemarksNull() == false) cmd.Parameters["REMARKS"].Value = (object)dis.Remarks;
                else cmd.Parameters["REMARKS"].Value = null;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DISCIPLINARYTYPE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;

        }


        private DataSet _updateDisciplinaryType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateDisciplinaryType");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            histDS.Merge(inputDS.Tables[histDS.Disciplinary.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();


            foreach (EmployeeHistDS.DisciplinaryRow dis in histDS.Disciplinary.Rows)
            {
                if (dis.IsDisciplinaryTypeCodeNull() == false) cmd.Parameters["TYPECODE"].Value = (object)dis.DisciplinaryTypeCode;
                else cmd.Parameters["TYPECODE"].Value = null;

                if (dis.IsDisciplinaryTypeNull() == false) cmd.Parameters["TYPENAME"].Value = (object)dis.DisciplinaryType;
                else cmd.Parameters["TYPENAME"].Value = null;

                if (dis.IsRemarksNull() == false) cmd.Parameters["REMARKS"].Value = (object)dis.Remarks;
                else cmd.Parameters["REMARKS"].Value = null;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DISCIPLINARYTYPE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateDisciplinary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("updateDisciplinary");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            histDS.Merge(inputDS.Tables[histDS.Disciplinary.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            foreach (EmployeeHistDS.DisciplinaryRow row in histDS.Disciplinary.Rows)
            {
                long employeeID = UtilDL.GetEmployeeId(connDS, row.EmployeeCode);
                long officerID1 = UtilDL.GetEmployeeId(connDS, row.InqOfficerCode1);
                long officerID2 = UtilDL.GetEmployeeId(connDS, row.InqOfficerCode2);

                cmd.Parameters["HearingReferenceNo"].Value = row.HearingReferenceNo;
                cmd.Parameters["HearingDate"].Value = row.HearingDate;
                cmd.Parameters["MajorIrregularity"].Value = row.MajorIrregularity;

                if (officerID1 != 0) cmd.Parameters["InqOfficerID1"].Value = officerID1;
                else cmd.Parameters["InqOfficerID1"].Value = DBNull.Value;
                if (officerID2 != 0) cmd.Parameters["InqOfficerID2"].Value = officerID2;
                else cmd.Parameters["InqOfficerID2"].Value = DBNull.Value;

                if (!row.IsInqFindingsNull()) cmd.Parameters["InqFindings"].Value = row.InqFindings;
                else cmd.Parameters["InqFindings"].Value = DBNull.Value;

                if (!row.IsAuthorityIDNull()) cmd.Parameters["AuthorityID"].Value = row.AuthorityID;
                else cmd.Parameters["AuthorityID"].Value = DBNull.Value;
                if (!row.IsAuthorityDecisionNull()) cmd.Parameters["AuthorityDecision"].Value = row.AuthorityDecision;
                else cmd.Parameters["AuthorityDecision"].Value = DBNull.Value;

                if (!row.IsPunishmentIDNull()) cmd.Parameters["PunishmentID"].Value = row.PunishmentID;
                else cmd.Parameters["PunishmentID"].Value = DBNull.Value;
                if (!row.IsPunishmentReferenceNoNull()) cmd.Parameters["PunishmentReferenceNo"].Value = row.PunishmentReferenceNo;
                else cmd.Parameters["PunishmentReferenceNo"].Value = DBNull.Value;
                if (!row.IsPunishmentDateNull()) cmd.Parameters["PunishmentDate"].Value = row.PunishmentDate;
                else cmd.Parameters["PunishmentDate"].Value = DBNull.Value;

                if (!row.IsAppealsNull()) cmd.Parameters["Appeals"].Value = row.Appeals;
                else cmd.Parameters["Appeals"].Value = DBNull.Value;
                if (!row.IsAppealsOutComeNull()) cmd.Parameters["AppealsOutcome"].Value = row.AppealsOutCome;
                else cmd.Parameters["AppealsOutcome"].Value = DBNull.Value;

                if (!row.IsRemarksNull()) cmd.Parameters["Remarks"].Value = row.Remarks;
                else cmd.Parameters["Remarks"].Value = DBNull.Value;

                //New Add Field :: Rony :: 16-Feb-2015
                if (!row.IsTransgressionDateNull()) cmd.Parameters["TransgressionDate"].Value = row.TransgressionDate;
                else cmd.Parameters["TransgressionDate"].Value = DBNull.Value;
                if (!row.IsNatureOfTransgressionNull()) cmd.Parameters["NatureOfTransgression"].Value = row.NatureOfTransgression;
                else cmd.Parameters["NatureOfTransgression"].Value = DBNull.Value;
                if (!row.IsHearingDate_NewNull()) cmd.Parameters["HearingDate_New"].Value = row.HearingDate_New;
                else cmd.Parameters["HearingDate_New"].Value = DBNull.Value;
                if (!row.IsOutcomeOfHearingNull()) cmd.Parameters["OutcomeOfHearing"].Value = row.OutcomeOfHearing;
                else cmd.Parameters["OutcomeOfHearing"].Value = DBNull.Value;
                //End
                //RONY
                if (!row.IsPunishment_DateFromNull()) cmd.Parameters["Punishment_DateFrom"].Value = row.Punishment_DateFrom;
                else cmd.Parameters["Punishment_DateFrom"].Value = DBNull.Value;
                if (!row.IsPunishment_DateToNull()) cmd.Parameters["Punishment_DateTo"].Value = row.Punishment_DateTo;
                else cmd.Parameters["Punishment_DateTo"].Value = DBNull.Value;
                if (!row.IsDISCIPLINARYSTATUSIDNull()) cmd.Parameters["DisciplinaryStsId"].Value = row.DISCIPLINARYSTATUSID;
                else cmd.Parameters["DisciplinaryStsId"].Value = DBNull.Value;

                cmd.Parameters["DisciplinaryID"].Value = row.DisciplinaryID;

               

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DISCIPLINARY_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();


            #region Employee History Create
            cmd.Parameters.Clear();
            cmd.Dispose();

            foreach (EmployeeHistDS.DisciplinaryRow row in histDS.Disciplinary.Rows)
            {
                if (!row.IsDisciplinaryActionNull())
                {
                    if (row.DisciplinaryAction == 5)
                    {
                        long employeeID = UtilDL.GetEmployeeId(connDS, row.EmployeeCode);
                        cmd.CommandText = "PRO_DISCIPLINARY_SUSPEND_HIST";
                        cmd.CommandType = CommandType.StoredProcedure;

                        long genPK = IDGenerator.GetNextGenericPK();
                        if (genPK == -1)
                        {
                            UtilDL.GetDBOperationFailed();
                        }

                        cmd.Parameters.AddWithValue("EmployeejobHistID", (Int32)genPK);
                        cmd.Parameters.AddWithValue("EmployeeID", employeeID);

                        if (!row.IsEffectDateNull()) cmd.Parameters.AddWithValue("EffectDate", row.EffectDate);
                        else cmd.Parameters.AddWithValue("EffectDate", System.DBNull.Value);

                        if (!row.IsCreate_UserNull()) cmd.Parameters.AddWithValue("Create_User", row.Create_User);
                        else cmd.Parameters.AddWithValue("Create_User", System.DBNull.Value);

                        if (!row.IsLockedNull()) cmd.Parameters.AddWithValue("Locked", row.Locked);
                        else cmd.Parameters.AddWithValue("Locked", System.DBNull.Value);

                        bool bError = false;
                        int nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                        if (bError == true)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_DISCIPLINARY_UPD.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                }
                break;
            }
            #endregion



            //return errDS;

            // WALI :: 23-Feb-2015 
            DataSet returnDS = new DataSet();
            returnDS.Merge(histDS.Disciplinary);
            returnDS.Merge(errDS);
            return returnDS;
            //
        }
        private DataSet _createDisciplinary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Disciplinary.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            #region Employee History Create
                   

            foreach (EmployeeHistDS.DisciplinaryRow row in histDS.Disciplinary.Rows)
            {
                if (!row.IsDisciplinaryActionNull())
                {
                    if (row.DisciplinaryAction == 5)
                    {
                        long employeeID = UtilDL.GetEmployeeId(connDS, row.EmployeeCode);
                        cmd.CommandText = "PRO_DISCIPLINARY_SUSPEND_HIST";
                        cmd.CommandType = CommandType.StoredProcedure;

                        long genPK = IDGenerator.GetNextGenericPK();
                        if (genPK == -1)
                        {
                            UtilDL.GetDBOperationFailed();
                        }

                        cmd.Parameters.AddWithValue("p_EmployeejobHistID", (Int32)genPK);
                        cmd.Parameters.AddWithValue("p_EmployeeID", employeeID);

                        if (!row.IsEffectDateNull()) cmd.Parameters.AddWithValue("p_EffectDate", row.EffectDate);
                        else cmd.Parameters.AddWithValue("p_EffectDate", System.DBNull.Value);

                        if (!row.IsCreate_UserNull()) cmd.Parameters.AddWithValue("p_Create_User", row.Create_User);
                        else cmd.Parameters.AddWithValue("p_Create_User", System.DBNull.Value);

                        if (!row.IsLockedNull()) cmd.Parameters.AddWithValue("p_Locked", row.Locked);
                        else cmd.Parameters.AddWithValue("p_Locked", System.DBNull.Value);


                        bool bError = false;
                        int nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                        if (bError == true)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_DISCIPLINARY_ADD.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                    }
                }
                break;
            }
            #endregion


            cmd.Parameters.Clear();
            cmd.Dispose();

            //create command
            cmd = DBCommandProvider.GetDBCommand("CreateDisciplinary");
            if (cmd == null) return UtilDL.GetCommandNotFound();



            int disciplinaryAction = 0;

            foreach (EmployeeHistDS.DisciplinaryRow row in histDS.Disciplinary.Rows)
            {
                long employeeID = UtilDL.GetEmployeeId(connDS, row.EmployeeCode);
                long officerID1 = UtilDL.GetEmployeeId(connDS, row.InqOfficerCode1);
                long officerID2 = UtilDL.GetEmployeeId(connDS, row.InqOfficerCode2);

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                row.DisciplinaryID = (Int32)genPK;      // WALI :: 24-Feb-2015 :: Sent back for file attachment.

                cmd.Parameters["DisciplinaryID"].Value = genPK;
                cmd.Parameters["EmployeeID"].Value = employeeID;
                cmd.Parameters["EmployeeID2"].Value = employeeID;
                cmd.Parameters["TypeID"].Value = row.DisciplinaryTypeID;
                cmd.Parameters["HearingReferenceNo"].Value = row.HearingReferenceNo;
                cmd.Parameters["HearingDate"].Value = row.HearingDate;
                cmd.Parameters["SiteID_DIS"].Value = row.SiteID_Dis;
                cmd.Parameters["DepartmentID_DIS"].Value = row.DepartmentID_Dis;
                cmd.Parameters["MajorIrregularity"].Value = row.MajorIrregularity;

                if (officerID1 != 0) cmd.Parameters["InqOfficerID1"].Value = officerID1;
                else cmd.Parameters["InqOfficerID1"].Value = DBNull.Value;
                if (officerID2 != 0) cmd.Parameters["InqOfficerID2"].Value = officerID2;
                else cmd.Parameters["InqOfficerID2"].Value = DBNull.Value;

                if (!row.IsInqFindingsNull()) cmd.Parameters["InqFindings"].Value = row.InqFindings;
                else cmd.Parameters["InqFindings"].Value = DBNull.Value;

                if (!row.IsAuthorityIDNull()) cmd.Parameters["AuthorityID"].Value = row.AuthorityID;
                else cmd.Parameters["AuthorityID"].Value = DBNull.Value;

                if (!row.IsAuthorityDecisionNull()) cmd.Parameters["AuthorityDecision"].Value = row.AuthorityDecision;
                else cmd.Parameters["AuthorityDecision"].Value = DBNull.Value;

                if (!row.IsPunishmentIDNull()) cmd.Parameters["PunishmentID"].Value = row.PunishmentID;
                else cmd.Parameters["PunishmentID"].Value = DBNull.Value;
                if (!row.IsPunishmentReferenceNoNull()) cmd.Parameters["PunishmentReferenceNo"].Value = row.PunishmentReferenceNo;
                else cmd.Parameters["PunishmentReferenceNo"].Value = DBNull.Value;
                if (!row.IsPunishmentDateNull()) cmd.Parameters["PunishmentDate"].Value = row.PunishmentDate;
                else cmd.Parameters["PunishmentDate"].Value = DBNull.Value;

                if (!row.IsAppealsNull()) cmd.Parameters["Appeals"].Value = row.Appeals;
                else cmd.Parameters["Appeals"].Value = DBNull.Value;
                if (!row.IsAppealsOutComeNull()) cmd.Parameters["AppealsOutcome"].Value = row.AppealsOutCome;
                else cmd.Parameters["AppealsOutcome"].Value = DBNull.Value;

                if (!row.IsRemarksNull()) cmd.Parameters["Remarks"].Value = row.Remarks;
                else cmd.Parameters["Remarks"].Value = DBNull.Value;

                if (!row.IsTransgressionDateNull()) cmd.Parameters["TransgressionDate"].Value = row.TransgressionDate;
                else cmd.Parameters["TransgressionDate"].Value = DBNull.Value;
                if (!row.IsNatureOfTransgressionNull()) cmd.Parameters["NatureOfTransgression"].Value = row.NatureOfTransgression;
                else cmd.Parameters["NatureOfTransgression"].Value = DBNull.Value;
                if (!row.IsHearingDate_NewNull()) cmd.Parameters["HearingDate_New"].Value = row.HearingDate_New;
                else cmd.Parameters["HearingDate_New"].Value = DBNull.Value;
                if (!row.IsOutcomeOfHearingNull()) cmd.Parameters["OutcomeOfHearing"].Value = row.OutcomeOfHearing;
                else cmd.Parameters["OutcomeOfHearing"].Value = DBNull.Value;

                if (!row.IsPunishment_DateFromNull()) cmd.Parameters["Punishment_DateFrom"].Value = row.Punishment_DateFrom;
                else cmd.Parameters["Punishment_DateFrom"].Value = DBNull.Value;

                if (!row.IsPunishment_DateToNull()) cmd.Parameters["Punishment_DateTo"].Value = row.Punishment_DateTo;
                else cmd.Parameters["Punishment_DateTo"].Value = DBNull.Value;

                if (!row.IsDISCIPLINARYSTATUSIDNull()) cmd.Parameters["DisciplinaryStsId"].Value = row.DISCIPLINARYSTATUSID;
                else cmd.Parameters["DisciplinaryStsId"].Value = DBNull.Value;

                //if (!row.IsDisciplinaryActionNull()) cmd.Parameters["DisciplinaryAction"].Value = row.DisciplinaryAction;
                //else cmd.Parameters["DisciplinaryAction"].Value = DBNull.Value;
                //if (!row.IsEffectDateNull()) cmd.Parameters["EffectDate"].Value = row.EffectDate;
                //else cmd.Parameters["EffectDate"].Value = DBNull.Value;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DISCIPLINARY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            


            errDS.Clear();
            errDS.AcceptChanges();
            //return errDS;

            // WALI :: 23-Feb-2015 
            DataSet returnDS = new DataSet();
            returnDS.Merge(histDS.Disciplinary);
            returnDS.Merge(errDS);
            return returnDS;
            //
        }


        private DataSet _deleteDisciplinary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //DataStringDS stringDS = new DataStringDS();
            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteDisciplinary");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataIntegerDS.DataInteger intValue in integerDS.DataIntegers)
            {
                cmd.Parameters["DisciplinaryID"].Value = (object)intValue.IntegerValue;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DISCIPLINARY_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS

        }
        private DataSet _getDisciplinary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDisciplinary");

            long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
            cmd.Parameters["EMPLOYEEID"].Value = empId;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            //EmployeeHistPO hisPO = new EmployeeHistPO();
            EmployeeHistDS hisDS = new EmployeeHistDS();
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Disciplinary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DISCIPLINARY_RECORDS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            hisDS.AcceptChanges();

            // EmployeeHistDS hisDS = new EmployeeHistDS();

            //if (hisPO.DisciplinaryType.Rows.Count > 0)
            //{
            //    try
            //    {
            //        foreach (EmployeeHistPO.DisciplinaryTypeRow poChi in hisPO.DisciplinaryType.Rows)
            //        {
            //            //EmployeeHistDS.DisciplinaryRow Chi = hisDS.Disciplinary.NewReferenceRow();
            //            EmployeeHistDS.DisciplinaryTypeRow Chi = hisDS.DisciplinaryType.NewDisciplinaryTypeRow();

            //            if (poChi.IsDISCIPLINARYIDNull() == false)
            //            {
            //                Chi.DISCIPLINARYID = poChi.DISCIPLINARYID;
            //            }
            //            if (poChi.IsDISCIPLINARYCODENull() == false)
            //            {
            //                Chi.DISCIPLINARYCODE = poChi.DISCIPLINARYCODE;
            //            }
            //            if (poChi.IsDISCIPLINARYTYPENull() == false)
            //            {
            //                Chi.DISCIPLINARYTYPE = poChi.DISCIPLINARYTYPE;
            //            }
            //            //if(poChi.IsTRANSGRESSIONDATENull()=false)
            //            //{
            //            Chi.TRANSGRESSIONDATE = poChi.TRANSGRESSIONDATE;
            //            //}
            //            if (poChi.IsNATUREOFTHETRANSGRESSIONNull() == false)
            //            {
            //                Chi.NATUREOFTHETRANSGRESSION = poChi.NATUREOFTHETRANSGRESSION;
            //            }
            //            if (poChi.IsHEARINGDATENull() == false)
            //            {
            //                Chi.HEARINGDATE = poChi.HEARINGDATE;
            //            }

            //            if (poChi.IsOUTCOMEOFHEARINGNull() == false)
            //            {
            //                Chi.OUTCOMEOFHEARING = poChi.OUTCOMEOFHEARING;
            //            }
            //            if (poChi.IsACTIONTAKENNull() == false)
            //            {
            //                Chi.ACTIONTAKEN = poChi.ACTIONTAKEN;
            //            }
            //            if (poChi.IsREMARKSNull() == false)
            //            {
            //                Chi.REMARKS = poChi.REMARKS;
            //            }
            //            if (poChi.IsEMPLOYEEIDNull() == false)
            //            {
            //                Chi.EMPLOYEEID = poChi.EMPLOYEEID;
            //            }
            //            //hisDS.Reference.AddReferenceRow(Chi);
            //            hisDS.DisciplinaryType.AddDisciplinaryTypeRow(Chi);
            //            hisDS.AcceptChanges();
            //        }
            //    }
            //    catch(Exception ex)
            //    {
            //    }
            //}

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getDegreeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            #region Get degree
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getDegreeList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,employeeHistDS, employeeHistDS.Degree.TableName,connDS.DBConnections[0].ConnectionID,ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEGREE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            #endregion

            #region Get Group
            OleDbCommand cmd2 = DBCommandProvider.GetDBCommand("getGroupList");

            if (cmd2 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter2 = new OleDbDataAdapter();
            adapter2.SelectCommand = cmd2;


            //EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            bool bError2 = false;
            int nRowAffected2 = -1;
            nRowAffected2 = ADOController.Instance.Fill(adapter2,
              employeeHistDS, employeeHistDS.Group.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError2);
            if (bError2 == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEGREE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            #endregion

            #region Get Mejor Subject
            OleDbCommand cmd3 = DBCommandProvider.GetDBCommand("getMajorSubjectList");

            if (cmd3 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter3 = new OleDbDataAdapter();
            adapter3.SelectCommand = cmd3;


            //EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            bool bError3 = false;
            int nRowAffected3 = -1;
            nRowAffected3 = ADOController.Instance.Fill(adapter3,
              employeeHistDS, employeeHistDS.Majorsubject.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError3);
            if (bError3 == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEGREE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            #endregion

            #region Get Education Board List
            OleDbCommand cmd4 = DBCommandProvider.GetDBCommand("getEduBoardList");

            if (cmd4 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter4 = new OleDbDataAdapter();
            adapter4.SelectCommand = cmd4;


            //EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            bool bError4 = false;
            int nRowAffected4 = -1;
            nRowAffected4 = ADOController.Instance.Fill(adapter4,
              employeeHistDS, employeeHistDS.Educationboard.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError4);
            if (bError4 == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEGREE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            #endregion

            employeeHistDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _createGuarantor(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Guarantor.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();
            //create command
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateGuarantor");

            OleDbCommand cmd = new OleDbCommand();
            if (histDS.Guarantor[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("CreateSelfServiceRGuarantor");
            else cmd = DBCommandProvider.GetDBCommand("CreateGuarantor");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistDS.GuarantorRow Gua in histDS.Guarantor.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["GUARANTORID"].Value = (object)genPK;
                //code
                //if (Gua.IsCodeNull() == false)
                //{
                //    cmd.Parameters["GUARANTORCODE"].Value = (object)Gua.Code;
                //}
                //else
                //{
                //    cmd.Parameters["GUARANTORCODE"].Value = null;
                //}
                //Employee Id
                if (Gua.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, Gua.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EMPID"].Value = (object)empId;
                    else cmd.Parameters["EMPID"].Value = null;
                }
                else
                {
                    cmd.Parameters["EMPID"].Value = null;
                }
                //Reference Name

                if (Gua.IsGuarantorNameNull() == false)
                {
                    cmd.Parameters["GUARANTORNAME"].Value = (object)Gua.GuarantorName;
                }
                else
                {
                    cmd.Parameters["GUARANTORNAME"].Value = null;
                }

                //Organization Name
                if (Gua.IsOrganizationNameNull() == false)
                {
                    cmd.Parameters["ORGANIZATIONNAME"].Value = (object)Gua.OrganizationName;
                }
                else
                {
                    cmd.Parameters["ORGANIZATIONNAME"].Value = null;
                }

                //Designation
                if (Gua.IsDesignationNull() == false)
                {
                    cmd.Parameters["DESIGNATION"].Value = (object)Gua.Designation;
                }
                else
                {
                    cmd.Parameters["DESIGNATION"].Value = null;
                }


                //Gender
                if (Gua.IsGenderNull() == false)
                {
                    cmd.Parameters["GENDER"].Value = (object)Gua.Gender;
                }
                else
                {
                    cmd.Parameters["GENDER"].Value = null;
                }

                //Nationality
                if (Gua.IsNationalityNull() == false)
                {
                    cmd.Parameters["NATIONALITY"].Value = (object)Gua.Nationality;
                }
                else
                {
                    cmd.Parameters["NATIONALITY"].Value = null;
                }

                //Mobile
                if (Gua.IsMobileNull() == false)
                {
                    cmd.Parameters["MOBILE"].Value = (object)Gua.Mobile;
                }
                else
                {
                    cmd.Parameters["MOBILE"].Value = null;
                }

                //Office Phone
                if (Gua.IsOfficePhoneNull() == false)
                {
                    cmd.Parameters["OFFICEPHONE"].Value = (object)Gua.OfficePhone;
                }
                else
                {
                    cmd.Parameters["OFFICEPHONE"].Value = null;
                }
                //Email Address
                if (Gua.IsEmailNull() == false)
                {
                    cmd.Parameters["EMAIL"].Value = (object)Gua.Email;
                }
                else
                {
                    cmd.Parameters["EMAIL"].Value = null;
                }


                //Remarks
                if (Gua.IsRemarksNull() == false)
                {
                    cmd.Parameters["REMARKS"].Value = (object)Gua.Remarks;
                }
                else
                {
                    cmd.Parameters["REMARKS"].Value = null;
                }
                //Guarantor
                if (Gua.IsAddressNull() == false)
                {
                    cmd.Parameters["ADDRESS"].Value = (object)Gua.Address;
                }
                else
                {
                    cmd.Parameters["ADDRESS"].Value = null;
                }

                //Rony :: 08 Sep 2016
                if (!Gua.IsGuarantor_NationalIDNull()) cmd.Parameters["Guarantor_NationalID"].Value = (object)Gua.Guarantor_NationalID;
                else cmd.Parameters["Guarantor_NationalID"].Value = DBNull.Value;
                //Hasinul :: 28-05-2017
                if (!Gua.IsRelationIDNull()) cmd.Parameters["RelationID"].Value = (object)Gua.RelationID;
                else cmd.Parameters["RelationID"].Value = DBNull.Value;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GUARANTOR_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet updateGuarantor(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Guarantor.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            //create command
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateGuarantor");
            OleDbCommand cmd = new OleDbCommand();
            if (histDS.Guarantor[0].IsSelfServise) cmd = DBCommandProvider.GetDBCommand("UpdateSelfServiceRGuarantor");
            else cmd = DBCommandProvider.GetDBCommand("UpdateGuarantor");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistDS.GuarantorRow Ref in histDS.Guarantor.Rows)
            {
                //code
                //if (Ref.IsCodeNull() == false)
                //{
                //    cmd.Parameters["GUARANTORCODE"].Value = (object)Ref.Code;
                //}
                //else
                //{
                //    cmd.Parameters["GUARANTORCODE"].Value = null;
                //}
                //Employee Id
                if (Ref.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, Ref.EmployeeCode);
                    if (empId > 0) cmd.Parameters["EMPID"].Value = (object)empId;
                    else cmd.Parameters["EMPID"].Value = null;
                }
                else
                {
                    cmd.Parameters["EMPID"].Value = null;
                }
                //Reference Name

                if (Ref.IsGuarantorNameNull() == false)
                {
                    cmd.Parameters["GUARANTORNAME"].Value = (object)Ref.GuarantorName;
                }
                else
                {
                    cmd.Parameters["GUARANTORNAME"].Value = null;
                }

                //Organization Name
                if (Ref.IsOrganizationNameNull() == false)
                {
                    cmd.Parameters["ORGANIZATIONNAME"].Value = (object)Ref.OrganizationName;
                }
                else
                {
                    cmd.Parameters["ORGANIZATIONNAME"].Value = null;
                }

                //Designation
                if (Ref.IsDesignationNull() == false)
                {
                    cmd.Parameters["DESIGNATION"].Value = (object)Ref.Designation;
                }
                else
                {
                    cmd.Parameters["DESIGNATION"].Value = null;
                }


                //Gender
                if (Ref.IsGenderNull() == false)
                {
                    cmd.Parameters["GENDER"].Value = (object)Ref.Gender;
                }
                else
                {
                    cmd.Parameters["GENDER"].Value = null;
                }

                //Nationality
                if (Ref.IsNationalityNull() == false)
                {
                    cmd.Parameters["NATIONALITY"].Value = (object)Ref.Nationality;
                }
                else
                {
                    cmd.Parameters["NATIONALITY"].Value = null;
                }

                //Mobile
                if (Ref.IsMobileNull() == false)
                {
                    cmd.Parameters["MOBILE"].Value = (object)Ref.Mobile;
                }
                else
                {
                    cmd.Parameters["MOBILE"].Value = null;
                }

                //Office Phone
                if (Ref.IsOfficePhoneNull() == false)
                {
                    cmd.Parameters["OFFICEPHONE"].Value = (object)Ref.OfficePhone;
                }
                else
                {
                    cmd.Parameters["OFFICEPHONE"].Value = null;
                }
                //Email Address
                if (Ref.IsEmailNull() == false)
                {
                    cmd.Parameters["EMAIL"].Value = (object)Ref.Email;
                }
                else
                {
                    cmd.Parameters["EMAIL"].Value = null;
                }


                //Remarks
                if (Ref.IsRemarksNull() == false)
                {
                    cmd.Parameters["REMARKS"].Value = (object)Ref.Remarks;
                }
                else
                {
                    cmd.Parameters["REMARKS"].Value = null;
                }

                //Address
                if (Ref.IsAddressNull() == false)
                {
                    cmd.Parameters["ADDRESS"].Value = (object)Ref.Address;
                }
                else
                {
                    cmd.Parameters["ADDRESS"].Value = null;
                }
                if (Ref.IsGuarantorIDNull() == false)
                {
                    cmd.Parameters["GUARANTORID"].Value = (object)Ref.GuarantorID;
                }
                else
                {
                    cmd.Parameters["GUARANTORID"].Value = null;
                }
                //Rony :: 08 Sep 2016
                if (!Ref.IsGuarantor_NationalIDNull()) cmd.Parameters["Guarantor_NationalID"].Value = (object)Ref.Guarantor_NationalID;
                else cmd.Parameters["Guarantor_NationalID"].Value = DBNull.Value;

                //Hasinul :: 28-5-2017
                if (!Ref.IsRelationIDNull()) cmd.Parameters["RelationID"].Value = (object)Ref.RelationID;
                else cmd.Parameters["RelationID"].Value = DBNull.Value;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GUARANTOR_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;


        }
        private DataSet _deleteguarantor(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteGuarantees");

            OleDbCommand cmd = new OleDbCommand();
            if (integerDS.DataIntegers[integerDS.DataIntegers.Count - 1].IntegerValue == 1)
            {
                cmd = DBCommandProvider.GetDBCommand("DeleteSelfServiceGuarantees");
            }
            else cmd = DBCommandProvider.GetDBCommand("DeleteGuarantees");

            integerDS.DataIntegers[integerDS.DataIntegers.Count - 1].Delete();
            integerDS.AcceptChanges();
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataIntegerDS.DataInteger intVal in integerDS.DataIntegers)
            {
                cmd.Parameters["GuarantorID"].Value = (object)intVal.IntegerValue;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GUARANTOR_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getEduListToUpdate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            //string EducationID = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            // EducationID = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            if (stringDS.DataStrings[1].StringValue == true.ToString()) cmd = DBCommandProvider.GetDBCommand("GetSelfServEduListToUpdate");
            else cmd = DBCommandProvider.GetDBCommand("GetEduListToUpdate");


            //long EduId = UtilDL.GetEmployeeId(connDS, EducationID);
            cmd.Parameters["EducationID"].Value = (object)stringDS.DataStrings[0].StringValue; ;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            //EmployeeHistPO hisPO = new EmployeeHistPO();
            EmployeeHistDS hisDS = new EmployeeHistDS();


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              hisDS, hisDS.EducationDetails.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EDUCATION_LIST_TO_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            hisDS.AcceptChanges();

            #region ==============
            /*
            hisPO.AcceptChanges();

            EmployeeHistDS hisDS = new EmployeeHistDS();

            if (hisPO.Education.Rows.Count > 0)
            {
                foreach (EmployeeHistPO.EducationRow poEdu in hisPO.Education.Rows)
                {
                    EmployeeHistDS.EducationRow Edu = hisDS.Education.NewEducationRow();
                    if (poEdu.IsEducationIdNull() == false)
                    {
                        Edu.EducationId = poEdu.EducationId;
                    }
                    if (poEdu.IsEducationCodeNull() == false)
                    {
                        Edu.Code = poEdu.EducationCode;
                    }
                    if (poEdu.IsEmployeeIdNull() == false)
                    {
                        Edu.EmployeeId = poEdu.EmployeeId;
                    }
                    if (poEdu.IsEmployeeCodeNull() == false)
                    {
                        Edu.EmployeeCode = poEdu.EmployeeCode;
                    }
                    if (poEdu.IsDegreeNull() == false)
                    {
                        Edu.Degree = poEdu.Degree;
                    }
                    if (poEdu.IsInstituteNull() == false)
                    {
                        Edu.Institute = poEdu.Institute;
                    }
                    if (poEdu.IsBoardNull() == false)
                    {
                        Edu.Board = poEdu.Board;
                    }
                    if (poEdu.IsYearNull() == false)
                    {
                        Edu.Year = poEdu.Year;
                    }
                    if (poEdu.IsResultNull() == false)
                    {
                        Edu.Result = poEdu.Result;
                    }
                    if (poEdu.IsRemarksNull() == false)
                    {
                        Edu.Remarks = poEdu.Remarks;
                    }

                    hisDS.Education.AddEducationRow(Edu);
                    hisDS.AcceptChanges();
                }
            }
            */
            #endregion
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        #region Competency :: Wali :: 21-Apr-2014

        private DataSet _getCompetencyTypeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAllCompetencyTypeList");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeHistDS, employeeHistDS.CompetencyProfile.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COMPETENCY_TYPE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            employeeHistDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createCompetency(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.CompetencyProfile.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (employeeHistDS.CompetencyProfile[0].IsSelfServise) cmd.CommandText = "PRO_SELF_COMPETENCY_PRO_CREATE";
            else cmd.CommandText = "PRO_COMPETENCY_PROFILE_CREATE";

            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistDS.CompetencyProfileRow row in employeeHistDS.CompetencyProfile.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_CompetencyID", (object)genPK);
                }

                if (row.IsCompetencyCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_CompetencyCode", row.CompetencyCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_CompetencyCode", DBNull.Value);
                }

                if (row.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, row.EmployeeCode);
                    if (empId > 0) cmd.Parameters.AddWithValue("@p_EmployeeID", empId);
                    else cmd.Parameters.AddWithValue("@p_EmployeeID", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_EmployeeID", DBNull.Value);
                }

                if (row.IsStatusNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Status", row.Status);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Status", DBNull.Value);
                }

                if (row.IsProficiencyLevelNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_ProficiencyLevel", row.ProficiencyLevel);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_ProficiencyLevel", DBNull.Value);
                }

                if (row.IsDateFromNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_DateFrom", row.DateFrom);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_DateFrom", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }

                if (row.IsCmptypeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_CompetencyTypeID", row.CmptypeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_CompetencyTypeID", DBNull.Value);
                }

                if (row.IsDateToNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_DateTo", row.DateTo);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_DateTo", DBNull.Value);
                }


                bool bError = false;
                string actualErrMsg = "";
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    messageDS.ErrorMsg.AddErrorMsgRow(row.Status);

                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMPETENCY_ADD.ToString();
                    err.ActualMsg = actualErrMsg;
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //returnDS.Merge(errDS);
                    //returnDS.Merge(failDS);
                    //return returnDS;
                }
                else messageDS.SuccessMsg.AddSuccessMsgRow(row.Status);
                messageDS.AcceptChanges();
            }

            //errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getCompetencyList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS hisDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCompetencyList");

            //long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
            //cmd.Parameters["EmployeeId"].Value = empId;

            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == true.ToString())
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServiceCompetencyList");
                cmd.Parameters["EmployeeCode"].Value = EmployeeCode;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetCompetencyList");

                long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
                cmd.Parameters["EmployeeId"].Value = empId;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.CompetencyProfile.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COMPETENCY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            hisDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteCompetencyProfile(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.CompetencyProfile.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            employeeHistDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (employeeHistDS.CompetencyProfile[0].IsSelfServise == true)
            {
                cmd.CommandText = "PRO_SELF_COMPETENCY_PRO_DELETE";
            }
            else cmd.CommandText = "PRO_COMPETENCY_PROFILE_DELETE";

            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistDS.CompetencyProfileRow compRow in employeeHistDS.CompetencyProfile.Rows)
            {
                if (compRow.IsCompetencyIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_CompetencyID", compRow.CompetencyID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_CompetencyID", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMPETENCY_PROFILE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(compRow.EmployeeCode + " => " + compRow.CmpTypeCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(compRow.EmployeeCode + " => " + compRow.CmpTypeCode);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;

        }
        private DataSet _getCompetencyDetails(DataSet inputDS)
        {
            EmployeeHistDS hisDS = new EmployeeHistDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            DataSet returnDS = new DataSet();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (Convert.ToBoolean(integerDS.DataIntegers[1].IntegerValue).ToString() == true.ToString())
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServiceCompetencyDetails");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetCompetencyDetails");
            }

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCompetencyDetails");
            cmd.Parameters["CompetencyId"].Value = integerDS.DataIntegers[0].IntegerValue;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.CompetencyProfile.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COMPETENCY_DETAILS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            hisDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(hisDS.CompetencyProfile);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateCompetencyProfile(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.CompetencyProfile.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (employeeHistDS.CompetencyProfile[0].IsSelfServise) cmd.CommandText = "PRO_SELF_COMPETENCY_PRO_UPD";
            else cmd.CommandText = "PRO_COMPETENCY_PROFILE_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistDS.CompetencyProfileRow row in employeeHistDS.CompetencyProfile.Rows)
            {
                if (row.IsStatusNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_STATUS", row.Status);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_STATUS", DBNull.Value);
                }

                if (row.IsProficiencyLevelNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_PROFICIENCYLEVEL", row.ProficiencyLevel);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_PROFICIENCYLEVEL", DBNull.Value);
                }

                if (row.IsDateFromNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DATEFROM", row.DateFrom);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DATEFROM", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_REMARKS", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_REMARKS", DBNull.Value);
                }

                if (row.IsCmptypeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_CMPTYPEID", row.CmptypeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_CMPTYPEID", DBNull.Value);
                }

                if (row.IsDateToNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DATETO", row.DateTo);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DATETO", DBNull.Value);
                }

                if (row.IsCompetencyIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_COMPETENCYID", row.CompetencyID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_COMPETENCYID", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMPETENCY_PROFILE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createCompetencyType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COMPETENCY_TYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.CompetencyProfile.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (EmployeeHistDS.CompetencyProfileRow row in employeeHistDS.CompetencyProfile.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_CmpTypeId", (object)genPK);
                }

                cmd.Parameters.AddWithValue("@p_CmpTypeCode", row.CmpTypeCode);
                cmd.Parameters.AddWithValue("@p_Description", row.Description);

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("@p_IsActive", row.IsActive);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GROUP_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateCompetencyType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.CompetencyProfile.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COMPETENCY_TYPE_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistDS.CompetencyProfileRow row in employeeHistDS.CompetencyProfile.Rows)
            {
                cmd.Parameters.AddWithValue("@p_Description", row.Description);
                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("@p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("@p_CmpTypeCode", row.CmpTypeCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMPETENCY_PROFILE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        //private DataSet _createCompetency(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    EmployeeHistDS histDS = new EmployeeHistDS();
        //    DBConnectionDS connDS = new DBConnectionDS();

        //    //extract dbconnection 
        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //    connDS.AcceptChanges();

        //    //create command
        //    OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateCompetency");
        //    if (cmd == null)
        //    {
        //        return UtilDL.GetCommandNotFound();
        //    }

        //    histDS.Merge(inputDS.Tables[histDS.Competency.TableName], false, MissingSchemaAction.Error);
        //    histDS.AcceptChanges();

        //    foreach (EmployeeHistDS.CompetencyRow Compt in histDS.Competency.Rows)
        //    {
        //        long genPK = IDGenerator.GetNextGenericPK();
        //        if (genPK == -1)
        //        {
        //            UtilDL.GetDBOperationFailed();
        //        }

        //        cmd.Parameters["CompetencyId"].Value = (object)genPK;
        //        //CompetencyCode
        //        if (Compt.IsCompetencyCodeNull() == false)
        //        {
        //            cmd.Parameters["CompetencyCode"].Value = (object)Compt.CompetencyCode;
        //        }
        //        else
        //        {
        //            cmd.Parameters["CompetencyCode"].Value = null;
        //        }
        //        //Employee Id
        //        if (Compt.IsEmployeeCodeNull() == false)
        //        {
        //            long empId = UtilDL.GetEmployeeId(connDS, Compt.EmployeeCode);
        //            if (empId > 0) cmd.Parameters["EmpId"].Value = (object)empId;
        //            else cmd.Parameters["EmpId"].Value = null;
        //        }
        //        else
        //        {
        //            cmd.Parameters["EmpId"].Value = null;
        //        }
        //        //CompetencyType
        //        if (Compt.IsCompetencyTypeNull() == false)
        //        {
        //            cmd.Parameters["CompetencyType"].Value = (object)Compt.CompetencyType;
        //        }
        //        else
        //        {
        //            cmd.Parameters["CompetencyType"].Value = null;
        //        }

        //        //ProficiencyLevel
        //        if (Compt.IsProficiencyLevelNull() == false)
        //        {
        //            cmd.Parameters["ProficiencyLevel"].Value = (object)Compt.ProficiencyLevel;
        //        }
        //        else
        //        {
        //            cmd.Parameters["ProficiencyLevel"].Value = null;
        //        }


        //        //Institute
        //        if (Compt.IsInstituteNull() == false)
        //        {
        //            cmd.Parameters["Institute"].Value = (object)Compt.Institute;
        //        }
        //        else
        //        {
        //            cmd.Parameters["Institute"].Value = null;
        //        }

        //        //Year
        //        if (Compt.IsYearNull() == false)
        //        {
        //            cmd.Parameters["Year"].Value = (object)Compt.Year;
        //        }
        //        else
        //        {
        //            cmd.Parameters["Year"].Value = null;
        //        }
        //        //Status
        //        if (Compt.IsStatusNull() == false)
        //        {
        //            cmd.Parameters["Status"].Value = (object)Compt.Status;
        //        }
        //        else
        //        {
        //            cmd.Parameters["Status"].Value = null;
        //        }
        //        //DatForm
        //        if (Compt.IsDateFromNull() == false)
        //        {
        //            cmd.Parameters["DateFrom"].Value = (object)Compt.DateFrom;
        //        }
        //        else
        //        {
        //            cmd.Parameters["DateFrom"].Value = null;
        //        }

        //        //Remarks
        //        if (Compt.IsRemarksNull() == false)
        //        {
        //            cmd.Parameters["Remarks"].Value = (object)Compt.Remarks;
        //        }
        //        else
        //        {
        //            cmd.Parameters["Remarks"].Value = null;
        //        }


        //        bool bError = false;
        //        int nRowAffected = -1;
        //        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        //        if (bError == true)
        //        {
        //            ErrorDS.Error err = errDS.Errors.NewError();
        //            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        //            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //            err.ErrorInfo1 = ActionID.ACTION_EDUCATION_ADD.ToString();
        //            errDS.Errors.AddError(err);
        //            errDS.AcceptChanges();
        //            return errDS;
        //        }
        //    }
        //    errDS.Clear();
        //    errDS.AcceptChanges();
        //    return errDS;
        //}

        //private DataSet _getCompetencyList(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();

        //    DBConnectionDS connDS = new DBConnectionDS();
        //    DataLongDS longDS = new DataLongDS();

        //    DataStringDS stringDS = new DataStringDS();
        //    string EmployeeCode = "";

        //    stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        //    stringDS.AcceptChanges();
        //    EmployeeCode = stringDS.DataStrings[0].StringValue;

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //    connDS.AcceptChanges();

        //    OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetCompetencyList");

        //    long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
        //    cmd.Parameters["EmployeeId"].Value = empId;

        //    if (cmd == null)
        //    {
        //        return UtilDL.GetCommandNotFound();
        //    }

        //    OleDbDataAdapter adapter = new OleDbDataAdapter();
        //    adapter.SelectCommand = cmd;

        //    // EmployeeHistPO hisPO = new EmployeeHistPO();
        //    EmployeeHistDS hisDS = new EmployeeHistDS();

        //    bool bError = false;
        //    int nRowAffected = -1;
        //    //nRowAffected = ADOController.Instance.Fill(adapter,hisPO, hisPO.Education.TableName,connDS.DBConnections[0].ConnectionID,ref bError);
        //    nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Competency.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        //    if (bError == true)
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorInfo1 = ActionID.NA_ACTION_GET_EDUCATION_LIST.ToString();
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;

        //    }

        //    hisDS.AcceptChanges();

        //    //EmployeeHistDS hisDS = new EmployeeHistDS();

        //    //if (hisPO.Education.Rows.Count > 0)
        //    //{
        //    //    foreach (EmployeeHistPO.EducationRow poEdu in hisPO.Education.Rows)
        //    //    {
        //    //        EmployeeHistDS.EducationRow Edu = hisDS.Education.NewEducationRow();
        //    //        if (poEdu.IsEducationIdNull() == false)
        //    //        {
        //    //            Edu.EducationId = poEdu.EducationId;
        //    //        }
        //    //        if (poEdu.IsEducationCodeNull() == false)
        //    //        {
        //    //            Edu.Code = poEdu.EducationCode;
        //    //        }
        //    //        if (poEdu.IsEmployeeIdNull() == false)
        //    //        {
        //    //            Edu.EmployeeId = poEdu.EmployeeId;
        //    //        }
        //    //        if (poEdu.IsEmployeeCodeNull() == false)
        //    //        {
        //    //            Edu.EmployeeCode = poEdu.EmployeeCode;
        //    //        }
        //    //        if (poEdu.IsDegreeNull() == false)
        //    //        {
        //    //            Edu.Degree = poEdu.Degree;
        //    //        }
        //    //        if (poEdu.IsInstituteNull() == false)
        //    //        {
        //    //            Edu.Institute = poEdu.Institute;
        //    //        }
        //    //        if (poEdu.IsBoardNull() == false)
        //    //        {
        //    //            Edu.Board = poEdu.Board;
        //    //        }
        //    //        if (poEdu.IsYearNull() == false)
        //    //        {
        //    //            Edu.Year = poEdu.Year;
        //    //        }
        //    //        if (poEdu.IsResultNull() == false)
        //    //        {
        //    //            Edu.Result = poEdu.Result;
        //    //        }
        //    //        if (poEdu.IsRemarksNull() == false)
        //    //        {
        //    //            Edu.Remarks = poEdu.Remarks;
        //    //        }

        //    //        hisDS.Education.AddEducationRow(Edu);
        //    //        hisDS.AcceptChanges();
        //    //    }
        //    //}

        //    // now create the packet
        //    errDS.Clear();
        //    errDS.AcceptChanges();

        //    DataSet returnDS = new DataSet();

        //    returnDS.Merge(hisDS);
        //    returnDS.Merge(errDS);
        //    returnDS.AcceptChanges();

        //    return returnDS;

        //}
        #endregion

        #region Educational History Related :: WALI :: 27-Apr-2014

        private DataSet _createEducationBoard(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EDU_BOARD_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Educationboard.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (EmployeeHistDS.EducationboardRow row in employeeHistDS.Educationboard.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_BoardId", (object)genPK);
                }

                if (row.IsBoardcodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_BoardCode", row.Boardcode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_BoardCode", DBNull.Value);
                }

                if (row.IsBoardnameNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_BoardName", row.Boardname);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_BoardName", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EDUCATIONBOARD_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateEducationBoard(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EDU_BOARD_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Educationboard.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.EducationboardRow row in employeeHistDS.Educationboard.Rows)
            {
                if (row.IsBoardnameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_BoardName", row.Boardname);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_BoardName", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                }

                if (row.IsBoardcodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_BoardCode", row.Boardcode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_BoardCode", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErcruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EDUCATIONBOARD_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createGroup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EDU_GROUP_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Group.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (EmployeeHistDS.GroupRow row in employeeHistDS.Group.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_GroupId", (object)genPK);
                }

                if (row.IsGroupcodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_GroupCode", row.Groupcode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_GroupCode", DBNull.Value);
                }

                if (row.IsGroupnameNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_GroupName", row.Groupname);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_GroupName", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GROUP_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateGroup(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EDU_GROUP_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Group.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.GroupRow row in employeeHistDS.Group.Rows)
            {
                if (row.IsGroupnameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_GroupName", row.Groupname);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_GroupName", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                }

                if (row.IsGroupcodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_GroupCode", row.Groupcode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_GroupCode", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                   nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GROUP_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createMajorSubject(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EDU_MAJORSUBJECT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Majorsubject.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (EmployeeHistDS.MajorsubjectRow row in employeeHistDS.Majorsubject.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_SubjectId", (object)genPK);
                }

                if (row.IsSubjectcodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_SubjectCode", row.Subjectcode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_SubjectCode", DBNull.Value);
                }

                if (row.IsSubjectnameNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_SubjectName", row.Subjectname);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_SubjectName", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAJOR_SUBJECT_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getMajorSubjectList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetAllMajorSubjectList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeHistDS, employeeHistDS.Majorsubject.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_MAJOR_SUBJECT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            employeeHistDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateMajorSubject(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EDU_MAJORSUBJECT_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Majorsubject.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.MajorsubjectRow row in employeeHistDS.Majorsubject.Rows)
            {
                if (row.IsSubjectnameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SubjectName", row.Subjectname);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SubjectName", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                }

                if (row.IsSubjectcodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SubjectCode", row.Subjectcode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SubjectCode", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAJOR_SUBJECT_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getUniversityList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getUniversityList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeHistDS, employeeHistDS.Edu_University.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_UNIVERSITY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            employeeHistDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS.Edu_University);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        #endregion

        #region Shakir Hossain :: 28-May-2014 : AuthorisedSignatory
        private DataSet _createAuthorisedSignatory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_AS_INFO_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.ASInfo.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (EmployeeHistDS.ASInfoRow row in employeeHistDS.ASInfo.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();

                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("p_Asid", (object)genPK);

                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);

                if (row.IsEffectDateNull() == false) cmd.Parameters.AddWithValue("p_EffectDate", row.EffectDate);
                else cmd.Parameters.AddWithValue("p_EffectDate", DBNull.Value);

                if (row.IsExpireDateNull() == false) cmd.Parameters.AddWithValue("p_ExpireDate", row.ExpireDate);
                else cmd.Parameters.AddWithValue("p_ExpireDate", DBNull.Value);

                if (row.IsAuthorisationTypeNull() == false) cmd.Parameters.AddWithValue("p_AuthorisationType", row.AuthorisationType);
                else cmd.Parameters.AddWithValue("p_AuthorisationType", DBNull.Value);

                if (row.IsIdentificationNoNull() == false) cmd.Parameters.AddWithValue("p_IdentificationNo", row.IdentificationNo);
                else cmd.Parameters.AddWithValue("p_IdentificationNo", DBNull.Value);


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GROUP_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getAuthorisedSignatoryList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS hisDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();
            DataIntegerDS intgerDS = new DataIntegerDS();

            intgerDS.Merge(inputDS.Tables[intgerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            intgerDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAuthorisedSignByType");

            cmd.Parameters["pAuthorisationType"].Value = intgerDS.DataIntegers[0].IntegerValue; ;
            cmd.Parameters["pAsID1"].Value = intgerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["pAsID2"].Value = intgerDS.DataIntegers[1].IntegerValue;

            if (intgerDS.DataIntegers[2].IntegerValue == 1 && intgerDS.DataIntegers[1].IntegerValue == 0)
            {
                cmd.Dispose();
                cmd = DBCommandProvider.GetDBCommand("GetAuthorisedSignByTypeAll");

                cmd.Parameters["pAuthorisationType"].Value = intgerDS.DataIntegers[0].IntegerValue; ;

            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.ASInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COMPETENCY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            hisDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateAuthorisedSignatory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_AS_INFO_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.ASInfo.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (EmployeeHistDS.ASInfoRow row in employeeHistDS.ASInfo.Rows)
            {
                cmd.Parameters.AddWithValue("p_Asid", row.ASID);

                if (row.IsEffectDateNull() == false) cmd.Parameters.AddWithValue("p_EffectDate", row.EffectDate);
                else cmd.Parameters.AddWithValue("p_EffectDate", DBNull.Value);

                if (row.IsExpireDateNull() == false) cmd.Parameters.AddWithValue("p_ExpireDate", row.ExpireDate);
                else cmd.Parameters.AddWithValue("p_ExpireDate", DBNull.Value);

                if (row.IsIdentificationNoNull() == false) cmd.Parameters.AddWithValue("p_IdentificationNo", row.IdentificationNo);
                else cmd.Parameters.AddWithValue("p_IdentificationNo", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GROUP_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteAuthorisedSignatory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.ASInfo.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            employeeHistDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_AS_INFO_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeHistDS.ASInfoRow ASInfoRow in employeeHistDS.ASInfo.Rows)
            {
                if (ASInfoRow.IsASIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("pASID", ASInfoRow.ASID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("pASID", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMPETENCY_PROFILE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(ASInfoRow.EmployeeCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(ASInfoRow.EmployeeCode);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;

        }
        #endregion

        #region University :: WALI
        private DataSet _createUniversity(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_UNIVERSITY_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Edu_University.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (EmployeeHistDS.Edu_UniversityRow row in employeeHistDS.Edu_University.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_UniversityId", (object)genPK);
                }

                if (row.IsUniversityCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_UniversityCode", row.UniversityCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_UniversityCode", DBNull.Value);
                }

                if (row.IsUniversityNameNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_UniversityName", row.UniversityName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_UniversityName", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_UNIVERSITY_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            //DataSet returnDS = new DataSet();

            //returnDS.Merge(employeeHistDS);
            //returnDS.Merge(stringDS);
            //returnDS.Merge(errDS);
            //returnDS.AcceptChanges();

            return errDS;
        }
        private DataSet _updateUniversity(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_UNIVERSITY_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Edu_University.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.Edu_UniversityRow row in employeeHistDS.Edu_University.Rows)
            {
                if (row.IsUniversityNameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_UniversityName", row.UniversityName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_UniversityName", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                }

                if (row.IsUniversityCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_UniversityCode", row.UniversityCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_UniversityCode", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_UNIVERSITY_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteUniversity(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_UNIVERSITY_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_UniversityCode", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_UNIVERSITY_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;  // return empty ErrorDS
        }
        #endregion

        #region Nominee :: WALI :: 14-Aug-2014
        private DataSet _saveNomineeInformation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();
            DataStringDS stringDS = new DataStringDS();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Nominee.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmdDel = new OleDbCommand();
            OleDbCommand cmd = new OleDbCommand();

            if (stringDS.DataStrings[1].StringValue == "DataRenewal")
            {
                cmdDel.CommandText = "PRO_DATA_RENEW_NOMINEE_DELETE";
                cmd.CommandText = "PRO_DATA_RENEW_NOMINEE_SAVE";
            }
            else if (stringDS.DataStrings[1].StringValue == "SelfService")
            {
                cmdDel.CommandText = "PRO_SELF_NOMINEE_DELETE";
                cmd.CommandText = "PRO_SELF_NOMINEE_SAVE";
            }
            else
            {
                cmdDel.CommandText = "PRO_NOMINEE_DELETE";
                cmd.CommandText = "PRO_NOMINEE_INFORMATION_SAVE";
            }
            cmdDel.CommandType = cmd.CommandType = CommandType.StoredProcedure;

            #region Delete Old Nominee Records
            cmdDel.Parameters.AddWithValue("@p_EmployeeCode", stringDS.DataStrings[0].StringValue.Trim());

            bool bErrorDel = false;
            string actualErrMsgDel = "";
            int nRowAffectedDel = -1;
            nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);

            if (bErrorDel)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_NOMINEE_SAVE.ToString();
                err.ActualMsg = actualErrMsgDel;
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
            }
            #endregion

            #region Save Nominee Information
            foreach (EmployeeHistDS.NomineeRow row in employeeHistDS.Nominee.Rows)
            {
                string nomineeType = row.NomineeFor;
                if (row.NomineeID != 0) cmd.Parameters.AddWithValue("@p_NomineeID", row.NomineeID); // Save data with Previous ID
                else
                {
                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) UtilDL.GetDBOperationFailed();
                    else cmd.Parameters.AddWithValue("@p_NomineeID", (object)genPK);
                }

                cmd.Parameters.AddWithValue("@p_NomineeName", row.NomineeName);
                cmd.Parameters.AddWithValue("@p_NomineeRelation", row.NomineeRelation);
                cmd.Parameters.AddWithValue("@p_NomineeShare", row.NomineeShare);

                if (row.IsNomineeTelephoneNull() == false) cmd.Parameters.AddWithValue("@p_NomineeTelephone", row.NomineeTelephone);
                else cmd.Parameters.AddWithValue("@p_NomineeTelephone", DBNull.Value);

                if (row.IsNomineeMobileNull() == false) cmd.Parameters.AddWithValue("@p_NomineeMobile", row.NomineeMobile);
                else cmd.Parameters.AddWithValue("@p_NomineeMobile", DBNull.Value);

                cmd.Parameters.AddWithValue("@p_EmployeeCode", row.EmployeeCode);
                cmd.Parameters.AddWithValue("@p_Serial", row.Serial);

                // WALI :: 03-Sep-2016
                if (!row.IsNominee_NIDNull()) cmd.Parameters.AddWithValue("@p_Nominee_NID", row.Nominee_NID);
                else cmd.Parameters.AddWithValue("@p_Nominee_NID", DBNull.Value);

                if (!row.IsNominee_BirthDateNull()) cmd.Parameters.AddWithValue("@p_Nominee_BirthDate", row.Nominee_BirthDate);
                else cmd.Parameters.AddWithValue("@p_Nominee_BirthDate", DBNull.Value);

                if (!row.IsNomineeForNull()) cmd.Parameters.AddWithValue("@p_NomineeFor", row.NomineeFor);
                else cmd.Parameters.AddWithValue("@p_NomineeFor", DBNull.Value);

                if (!row.IsNominee_GuardianNameNull()) cmd.Parameters.AddWithValue("@p_GuardianName", row.Nominee_GuardianName);
                else cmd.Parameters.AddWithValue("@p_GuardianName", DBNull.Value);

                if (!row.IsNominee_Guardian_NIDNull()) cmd.Parameters.AddWithValue("@p_Guardian_NID", row.Nominee_Guardian_NID);
                else cmd.Parameters.AddWithValue("@p_Guardian_NID", DBNull.Value);

                if (!row.IsNominee_Guardian_PhoneNull()) cmd.Parameters.AddWithValue("@p_Guardian_Phone", row.Nominee_Guardian_Phone);
                else cmd.Parameters.AddWithValue("@p_Guardian_Phone", DBNull.Value);

                if (!row.IsBirthCertificateNoNull()) cmd.Parameters.AddWithValue("@p_BirthCertificate", row.BirthCertificateNo);
                else cmd.Parameters.AddWithValue("@p_BirthCertificate", DBNull.Value);

                if (!row.IsPassportNoNull()) cmd.Parameters.AddWithValue("@p_PassportNo", row.PassportNo);
                else cmd.Parameters.AddWithValue("@p_PassportNo", DBNull.Value);

                bool bError = false;
                string actualErrMsg = "";
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_NOMINEE_SAVE.ToString();
                    err.ActualMsg = actualErrMsg;
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (bError) messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} - {1} [{2}]", nomineeType, row.NomineeName, row.NomineeShare.ToString("0.00")));
                else messageDS.SuccessMsg.AddSuccessMsgRow(String.Format("{0} - {1} [{2}]", nomineeType, row.NomineeName, row.NomineeShare.ToString("0.00")));
            }
            messageDS.AcceptChanges();
            #endregion

            //errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getEmployeeNomineeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS hisDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetNomineeListByEmpCode");
            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[2].StringValue == "Nominee_List")
            {
                if (stringDS.DataStrings[1].StringValue == "IsDataRenewal")
                { cmd = DBCommandProvider.GetDBCommand("GetDataRenewalNomineeListByEmpCode"); }
                else if (stringDS.DataStrings[1].StringValue == "IsSelfService")
                { cmd = DBCommandProvider.GetDBCommand("GetSelfServNomineeListByEmpCode"); }
                else
                { cmd = DBCommandProvider.GetDBCommand("GetNomineeListByEmpCode"); }
                if (cmd == null) return UtilDL.GetCommandNotFound();
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Nominee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_NOMINEE_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                hisDS.AcceptChanges();
            }
            else if (stringDS.DataStrings[2].StringValue == "Nominee_FundTypeList")
            {
                cmd = DBCommandProvider.GetDBCommand("GetNominee_FundType"); 
                
                if (cmd == null) return UtilDL.GetCommandNotFound();

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Nominee_FundType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_NOMINEE_LIST.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                hisDS.AcceptChanges();
            }
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Authority
        private DataSet _createAuthority(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Authority.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_AUTHORITY_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            #region Save Authority
            foreach (EmployeeHistDS.AuthorityRow row in employeeHistDS.Authority.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("@p_AuthorityID", genPK);
                cmd.Parameters.AddWithValue("@p_AuthorityName", row.AuthorityName);
                cmd.Parameters.AddWithValue("@p_AuthorityDescription", row.AuthorityDescription);
                cmd.Parameters.AddWithValue("@p_IsActive", row.IsActive);

                bool bError = false;
                string actualErrMsg = "";
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_AUTHORITY_CREATE.ToString();
                    err.ActualMsg = actualErrMsg;
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //returnDS.Merge(errDS);
                    //returnDS.Merge(failDS);
                    //return returnDS;
                }
                if (nRowAffected < 0) messageDS.ErrorMsg.AddErrorMsgRow(row.AuthorityName);
                else messageDS.SuccessMsg.AddSuccessMsgRow(row.AuthorityName);
                messageDS.AcceptChanges();
            }
            #endregion

            //errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAuthorityList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS hisDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAuthorityList");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Authority.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_AUTHORITY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            hisDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateAuthority(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Authority.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_AUTHORITY_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            #region Update Authority
            foreach (EmployeeHistDS.AuthorityRow row in employeeHistDS.Authority.Rows)
            {
                cmd.Parameters.AddWithValue("@p_AuthorityDescription", row.AuthorityDescription);
                cmd.Parameters.AddWithValue("@p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("@p_AuthorityID", row.AuthorityID);

                bool bError = false;
                string actualErrMsg = "";
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_AUTHORITY_UPDATE.ToString();
                    err.ActualMsg = actualErrMsg;
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //returnDS.Merge(errDS);
                    //returnDS.Merge(failDS);
                    //return returnDS;
                }
                if (nRowAffected < 0) messageDS.ErrorMsg.AddErrorMsgRow(row.AuthorityName);
                else messageDS.SuccessMsg.AddSuccessMsgRow(row.AuthorityName);
                messageDS.AcceptChanges();
            }
            #endregion

            //errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteAuthority(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Authority.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_AUTHORITY_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            #region Delete Authorities
            foreach (EmployeeHistDS.AuthorityRow row in employeeHistDS.Authority.Rows)
            {
                cmd.Parameters.AddWithValue("@p_AuthorityID", row.AuthorityID);

                bool bError = false;
                string actualErrMsg = "";
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_AUTHORITY_DELETE.ToString();
                    err.ActualMsg = actualErrMsg;
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //returnDS.Merge(errDS);
                    //returnDS.Merge(failDS);
                    //return returnDS;
                }
                if (nRowAffected < 0) messageDS.ErrorMsg.AddErrorMsgRow(row.AuthorityName);
                else messageDS.SuccessMsg.AddSuccessMsgRow(row.AuthorityName);
                messageDS.AcceptChanges();
            }
            #endregion

            //errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Nature of Punishment :: Rony :: 25-Aug-2014
        private DataSet _getPunishmentNature(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getPunishmentNature");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeHistDS, employeeHistDS.PunishmentNature.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PUNISHMENT_NATURE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            employeeHistDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS.PunishmentNature);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createPunishmentNature(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_PUNISHMENT_NATURE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.PunishmentNature.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (EmployeeHistDS.PunishmentNatureRow row in employeeHistDS.PunishmentNature.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_PunishmentId", (object)genPK);
                }
                cmd.Parameters.AddWithValue("@p_NatureOfPunishment", row.NatureOfPunishment);

                if (row.IsDetailsNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_Details", row.Details);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_Details", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PUNISHMENT_NATURE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updatePunishmentNature(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_PUNISHMENT_NATURE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.PunishmentNature.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.PunishmentNatureRow row in employeeHistDS.PunishmentNature.Rows)
            {
                cmd.Parameters.AddWithValue("p_NatureOfPunishment", row.NatureOfPunishment);

                if (row.IsDetailsNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Details", row.Details);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Details", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_PunishmentId", row.PunishmentId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_PUNISHMENT_NATURE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deletePunishmentNature(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_PUNISHMENT_NATURE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_NatureOfPunishment", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_PUNISHMENT_NATURE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;
        }
        #endregion

        private DataSet _getDisciplinaryTypeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS hisDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getDisciplinaryListGrid");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Disciplinary.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DISCIPLINARY_TYPE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            hisDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteDisciplinaryType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            histDS.Merge(inputDS.Tables[histDS.Disciplinary.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteDisciplinaryTypeGrid");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.DisciplinaryRow row in histDS.Disciplinary.Rows)
            {
                cmd.Parameters["TYPECODE"].Value = row.DisciplinaryTypeCode;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DELETE_DISCIPLINARY_TYPE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.DisciplinaryTypeCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.DisciplinaryTypeCode);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        #region Proffesional Qualification :: Rony :: Uploaded > WALI :: 26-Aug-2014
        private DataSet _createQualification(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS empDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            empDS.Merge(inputDS.Tables[empDS.Qualification.TableName], false, MissingSchemaAction.Error);
            empDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (!empDS.Qualification[0].IsIsDataRenewalNull() && empDS.Qualification[0].IsDataRenewal) cmd.CommandText = "PRO_QUALIFICATION_CREATE_PARK";
            else if (empDS.Qualification[0].IsSelfServise) cmd.CommandText = "PRO_QUALIFICATION_CREATE_SELF";
            else cmd.CommandText = "PRO_QUALIFICATION_CREATE";
            
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (EmployeeHistDS.QualificationRow row in empDS.Qualification.Rows)
            {
                //long genPK = IDGenerator.GetNextGenericPK();
                //if (genPK == -1) UtilDL.GetDBOperationFailed();

                //cmd.Parameters.AddWithValue("p_Qualificationid", genPK);
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                cmd.Parameters.AddWithValue("p_PQID", row.PQID);
                cmd.Parameters.AddWithValue("p_Institution", row.Institution);
                cmd.Parameters.AddWithValue("p_Year", row.Year);
                cmd.Parameters.AddWithValue("p_ResultTypeID", row.Resulttypeid);

                if (row.IsDivisionResultNull() == false) cmd.Parameters.AddWithValue("p_Divisionresult", row.DivisionResult);
                else cmd.Parameters.AddWithValue("p_Divisionresult", DBNull.Value);

                if (row.IsMarksPercentageNull() == false) cmd.Parameters.AddWithValue("p_MarksPercentage", row.MarksPercentage);
                else cmd.Parameters.AddWithValue("p_MarksPercentage", DBNull.Value);

                if (row.IsCGPAresultNull() == false) cmd.Parameters.AddWithValue("p_CGPAResult", row.CGPAresult);
                else cmd.Parameters.AddWithValue("p_CGPAResult", DBNull.Value);

                if (row.IsOutOfMarksNull() == false) cmd.Parameters.AddWithValue("p_OutOfMarks", row.OutOfMarks);
                else cmd.Parameters.AddWithValue("p_OutOfMarks", DBNull.Value);

                if (row.IsOptionalMarksNull() == false) cmd.Parameters.AddWithValue("p_Optionalmarks", row.OptionalMarks);
                else cmd.Parameters.AddWithValue("p_Optionalmarks", DBNull.Value);

                if (row.IsCGPAresultNameNull() == false) cmd.Parameters.AddWithValue("p_Cgparesultname", row.CGPAresultName);
                else cmd.Parameters.AddWithValue("p_Cgparesultname", DBNull.Value);

                if (row.IsRemarksNull() == false) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                cmd.Parameters.AddWithValue("p_CountryID", row.CountryID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_QUALIFICATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.QualificationName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.QualificationName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateQualification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS empDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            empDS.Merge(inputDS.Tables[empDS.Qualification.TableName], false, MissingSchemaAction.Error);
            empDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (!empDS.Qualification[0].IsIsDataRenewalNull() && empDS.Qualification[0].IsDataRenewal) cmd.CommandText = "PRO_QUALIFICATION_UPD_PARK";
            else if (empDS.Qualification[0].IsSelfServise) cmd.CommandText = "PRO_QUALIFICATION_UPD_SELF";
            else cmd.CommandText = "PRO_QUALIFICATION_UPD";

            cmd.CommandType = CommandType.StoredProcedure;

            foreach (EmployeeHistDS.QualificationRow row in empDS.Qualification.Rows)
            {
                cmd.Parameters.AddWithValue("p_Qualificationid", row.QualificationId);
                cmd.Parameters.AddWithValue("p_PQID", row.PQID);
                cmd.Parameters.AddWithValue("p_Institution", row.Institution);
                cmd.Parameters.AddWithValue("p_Year", row.Year);
                cmd.Parameters.AddWithValue("p_ResultTypeID", row.Resulttypeid);

                if (row.IsDivisionResultNull() == false) cmd.Parameters.AddWithValue("p_DivisionResult", row.DivisionResult);
                else cmd.Parameters.AddWithValue("p_DivisionResult", DBNull.Value);

                if (row.IsMarksPercentageNull() == false) cmd.Parameters.AddWithValue("p_MarksPercentage", row.MarksPercentage);
                else cmd.Parameters.AddWithValue("p_MarksPercentage", DBNull.Value);

                if (row.IsCGPAresultNull() == false) cmd.Parameters.AddWithValue("p_CGPAResult", row.CGPAresult);
                else cmd.Parameters.AddWithValue("p_CGPAResult", DBNull.Value);

                if (row.IsOutOfMarksNull() == false) cmd.Parameters.AddWithValue("p_OutOfMarks", row.OutOfMarks);
                else cmd.Parameters.AddWithValue("p_OutOfMarks", DBNull.Value);

                if (row.IsOptionalMarksNull() == false) cmd.Parameters.AddWithValue("p_OptionalMarks", row.OptionalMarks);
                else cmd.Parameters.AddWithValue("p_OptionalMarks", DBNull.Value);

                if (row.IsOutOfMarksNull() == false) cmd.Parameters.AddWithValue("p_CGPAResultName", row.OutOfMarks);
                else cmd.Parameters.AddWithValue("p_CGPAResultName", DBNull.Value);

                if (row.IsRemarksNull() == false) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                cmd.Parameters.AddWithValue("p_CountryID", row.CountryID);
                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_QUALIFICATION_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + "-" + row.QualificationName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + "-" + row.QualificationName);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(empDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteQualification(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS empDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            empDS.Merge(inputDS.Tables[empDS.Qualification.TableName], false, MissingSchemaAction.Error);
            empDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (!empDS.Qualification[0].IsIsDataRenewalNull() && empDS.Qualification[0].IsDataRenewal) cmd.CommandText = "PRO_QUALIFICATION_DEL_PARK";
            else if (empDS.Qualification[0].IsSelfServise) cmd.CommandText = "PRO_QUALIFICATION_DEL_SELF";
            else cmd.CommandText = "PRO_QUALIFICATION_DEL";

            cmd.CommandType = CommandType.StoredProcedure;

            foreach (EmployeeHistDS.QualificationRow row in empDS.Qualification.Rows)
            {
                cmd.Parameters.AddWithValue("p_QualificationID", row.QualificationId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_QUALIFICATION_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.DegreeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.DegreeName);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getQualificationList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool selfService = Convert.ToBoolean(stringDS.DataStrings[1].StringValue);

            if (!selfService)
            {
                cmd = DBCommandProvider.GetDBCommand("GetQualificationList");
                long empId = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
                cmd.Parameters["EmployeeId"].Value = empId;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("getQualificationListByMultiEmpCode_Park");
                cmd.Parameters["EmpCode1"].Value = cmd.Parameters["EmpCode2"].Value = stringDS.DataStrings[0].StringValue;
            }

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            //EmployeeHistPO hisPO = new EmployeeHistPO();
            EmployeeHistDS hisDS = new EmployeeHistDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              hisDS, hisDS.Qualification.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_QUALIFICATION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            hisDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        #endregion

        #region Service Agreement :: Rony :: Uploaded WALI :: 27-Aug-2014
        private DataSet _createServiceAgreement(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.ServiceAgreement.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            if (employeeHistDS.ServiceAgreement[0].IsSelfServise) cmd.CommandText = "PRO_SELF_SERVICE_AGREE_CREATE";
            else cmd.CommandText = "PRO_SERVICE_AGREEMENT_CREATE";

            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.ServiceAgreementRow row in employeeHistDS.ServiceAgreement.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("@p_ServiceagreementID", (object)genPK);

                if (row.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, row.EmployeeCode);
                    if (empId > 0) cmd.Parameters.AddWithValue("@p_EmployeeID", empId);
                    else cmd.Parameters.AddWithValue("@p_EmployeeID", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_EmployeeID", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("@p_DateOfAgreement", row.DateOfAgreement);
                cmd.Parameters.AddWithValue("@p_AgreementFrom", row.AgreementFrom);
                cmd.Parameters.AddWithValue("@p_AgreementTo", row.AgreementTo);
                cmd.Parameters.AddWithValue("@p_CashSecurity", row.CashSecurity);
                cmd.Parameters.AddWithValue("@p_PayOrder", row.PayOrder);
                cmd.Parameters.AddWithValue("@p_BankCode", row.BankCode);
                cmd.Parameters.AddWithValue("@p_PaymentDate", row.PaymentDate);
                cmd.Parameters.AddWithValue("@p_IsCashReturn", row.IsCashReturn);

                if (row.IsCashreturnDateNull() == false) cmd.Parameters.AddWithValue("@p_CashReturnDate", row.CashreturnDate);
                else cmd.Parameters.AddWithValue("@p_CashReturnDate", DBNull.Value);

                bool bError = false;
                string actualErrMsg = "";
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SERVICE_AGREEMENT_ADD.ToString();
                    err.ActualMsg = actualErrMsg;
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();

                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.AgreementFrom.ToString("dd-MMM-yyyy") + " -> " + row.AgreementTo.ToString("dd-MMM-yyyy"));
                else messageDS.ErrorMsg.AddErrorMsgRow(row.AgreementFrom.ToString("dd-MMM-yyyy") + " -> " + row.AgreementTo.ToString("dd-MMM-yyyy"));

                messageDS.AcceptChanges();
            }


            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getServiceAgreement(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS hisDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            if (stringDS.DataStrings[stringDS.DataStrings.Count - 1].StringValue == true.ToString())
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServiceServiceAgreement");
                cmd.Parameters["EmployeeCode"].Value = EmployeeCode;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetServiceAgreement");

                long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
                cmd.Parameters["EmployeeId"].Value = empId;
            }

            //long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
            //cmd.Parameters["EmployeeId"].Value = empId;

            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.ServiceAgreement.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SERVICE_AGREEMENT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            hisDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteServiceAgreement(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.ServiceAgreement.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            if (employeeHistDS.ServiceAgreement[0].IsSelfServise == true)
            {
                cmd.CommandText = "PRO_SELF_SERVICE_AGREE_DELETE";
            }
            else cmd.CommandText = "PRO_SERVICE_AGREEMENT_DELETE";

            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.ServiceAgreementRow serRow in employeeHistDS.ServiceAgreement.Rows)
            {
                if (serRow.IsServiceAgreementIdNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_ServiceagreementID", serRow.ServiceAgreementId);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_ServiceagreementID", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SERVICE_AGREEMENT_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(serRow.AgreementFrom.ToString("dd-MMM-yyyy") + " -> " + serRow.AgreementTo.ToString("dd-MMM-yyyy"));
                else messageDS.ErrorMsg.AddErrorMsgRow(serRow.AgreementFrom.ToString("dd-MMM-yyyy") + " -> " + serRow.AgreementTo.ToString("dd-MMM-yyyy"));
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateServiceAgreement(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.ServiceAgreement.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            if (employeeHistDS.ServiceAgreement[0].IsSelfServise) cmd.CommandText = "PRO_SELF_SERVICE_AGREEMENT_UPD";
            else cmd.CommandText = "PRO_SERVICE_AGREEMENT_UPD";

            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.ServiceAgreementRow row in employeeHistDS.ServiceAgreement.Rows)
            {
                cmd.Parameters.AddWithValue("p_DateOfAgreement", row.DateOfAgreement);
                cmd.Parameters.AddWithValue("p_AgreementFrom", row.AgreementFrom);
                cmd.Parameters.AddWithValue("p_AgreementTo", row.AgreementTo);
                cmd.Parameters.AddWithValue("p_CashSecurity", row.CashSecurity);
                cmd.Parameters.AddWithValue("p_PayOrder", row.PayOrder);
                cmd.Parameters.AddWithValue("p_BankCode", row.BankCode);
                cmd.Parameters.AddWithValue("p_PaymentDate", row.PaymentDate);
                cmd.Parameters.AddWithValue("p_IsCashReturn", row.IsCashReturn);
                if (row.IsCashreturnDateNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_CashReturnDate", row.CashreturnDate);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_CashReturnDate", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_ServiceagreementID", row.ServiceAgreementId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SERVICE_AGREEMENT_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        #endregion

        #region Reward and Recognition :: WALI :: 28-Aug-2014
        private DataSet _getRewardType(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getRewardType");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeHistDS, employeeHistDS.RewardRecognition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REWARD_TYPE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            employeeHistDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS.RewardRecognition);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createRewardType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REWARD_TYPE_CREATE ";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.RewardRecognition.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (EmployeeHistDS.RewardRecognitionRow row in employeeHistDS.RewardRecognition.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("@p_RewardTypeId", (object)genPK);

                cmd.Parameters.AddWithValue("@p_RewardTypeName", row.RewardTypeName);

                if (row.IsRewardTypeDetailsNull() == false) cmd.Parameters.AddWithValue("@p_RewardTypeDetails", row.RewardTypeDetails);
                else cmd.Parameters.AddWithValue("@p_RewardTypeDetails", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REWARD_TYPE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateRewardType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REWARD_TYPE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.RewardRecognition.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.RewardRecognitionRow row in employeeHistDS.RewardRecognition.Rows)
            {
                cmd.Parameters.AddWithValue("p_RewardTypeName", row.RewardTypeName);

                if (row.IsRewardTypeDetailsNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_RewardTypeDetails", row.RewardTypeDetails);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_RewardTypeDetails", DBNull.Value);
                }

                cmd.Parameters.AddWithValue("p_RewardTypeId", row.RewardTypeId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REWARD_TYPE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteRewardType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.RewardRecognition.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_REWARD_TYPE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.RewardRecognitionRow row in employeeHistDS.RewardRecognition.Rows)
            {
                cmd.Parameters.AddWithValue("p_RewardTypeName", row.RewardTypeName);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REWARD_TYPE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.RewardTypeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.RewardTypeName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _createRewardRecognition(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.RewardRecognition.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            if (employeeHistDS.RewardRecognition[0].IsSelfServise) cmd.CommandText = "PRO_SELF_REWARD_RECOG_ADD";
            else cmd.CommandText = "PRO_REWARD_RECOGNITION_ADD";

            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.RewardRecognitionRow row in employeeHistDS.RewardRecognition.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("@p_RewardID", (object)genPK);

                cmd.Parameters.AddWithValue("@p_RewardTypeID", row.RewardTypeId);
                cmd.Parameters.AddWithValue("@p_ReferenceNo", row.ReferenceNo);
                cmd.Parameters.AddWithValue("@p_RewardEffectDate", row.RewardEffectDate);
                cmd.Parameters.AddWithValue("@p_AuthorityID", row.AuthorityID);
                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                cmd.Parameters.AddWithValue("@p_EmployeeCode", row.EmployeeCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REWARD_RECOGNITION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateRewardRecognition(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.RewardRecognition.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (employeeHistDS.RewardRecognition[0].IsSelfServise) cmd.CommandText = "PRO_SELF_REWARD_RECOG_UPD";
            else cmd.CommandText = "PRO_REWARD_RECOGNITION_UPD";

            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.RewardRecognitionRow row in employeeHistDS.RewardRecognition.Rows)
            {
                cmd.Parameters.AddWithValue("@p_RewardTypeID", row.RewardTypeId);
                cmd.Parameters.AddWithValue("@p_ReferenceNo", row.ReferenceNo);
                cmd.Parameters.AddWithValue("@p_RewardEffectDate", row.RewardEffectDate);
                cmd.Parameters.AddWithValue("@p_AuthorityID", row.AuthorityID);
                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("@p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("@p_Remarks", DBNull.Value);
                cmd.Parameters.AddWithValue("@p_RewardID", row.RewardID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REWARD_RECOGNITION_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteRewardRecognition(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.RewardRecognition.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (employeeHistDS.RewardRecognition[0].IsSelfServise == true)
            {
                cmd.CommandText = "PRO_SELF_REWARD_RECOG_DEL";
            }
            else cmd.CommandText = "PRO_REWARD_RECOGNITION_DEL";

            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.RewardRecognitionRow row in employeeHistDS.RewardRecognition.Rows)
            {
                cmd.Parameters.AddWithValue("p_RewardID", row.RewardID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REWARD_RECOGNITION_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.RewardTypeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.RewardTypeName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _getRewardRecognition(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[stringDS.DataStrings.Count - 1].StringValue == true.ToString())
            {
                cmd = DBCommandProvider.GetDBCommand("getSelfServiceRewardRecognition");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("getRewardRecognition");
            }


            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("getRewardRecognition");
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeHistDS, employeeHistDS.RewardRecognition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REWARD_RECOGNITION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            employeeHistDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS.RewardRecognition);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Society Membership :: Rony :: Uploaded WALI :: 28-Aug-2014
        private DataSet _createSocietyMember(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.SocietyMembership.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            if (employeeHistDS.SocietyMembership[0].IsDataRenewal) cmd.CommandText = "PRO_DATA_RENEW_SOCIETY_MEMBER";
            else if (employeeHistDS.SocietyMembership[0].IsSelfServise) cmd.CommandText = "PRO_SELF_SOCIETY_MEMBER_ADD";
            else cmd.CommandText = "PRO_SOCIETY_MEMBERSHIP_CREATE";

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (EmployeeHistDS.SocietyMembershipRow row in employeeHistDS.SocietyMembership.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_SocietyMembershipId", (object)genPK);
                }
                if (row.IsEmployeeCodeNull() == false)
                {
                    long empId = UtilDL.GetEmployeeId(connDS, row.EmployeeCode);
                    if (empId > 0) cmd.Parameters.AddWithValue("@p_EmployeeID", empId);
                    else cmd.Parameters.AddWithValue("@p_EmployeeID", DBNull.Value);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_EmployeeID", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("@p_MembershipCategory", row.MembershipCategory);
                cmd.Parameters.AddWithValue("@p_MembershipNo", row.MembershipNo);
                cmd.Parameters.AddWithValue("@p_OrganizationName", row.OrganizationName);
                cmd.Parameters.AddWithValue("@p_CountryId", row.CountryId);
                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                bool bError = false;
                string actualErrMsg = "";
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    messageDS.ErrorMsg.AddErrorMsgRow(row.MembershipCategory);

                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SERVICE_AGREEMENT_ADD.ToString();
                    err.ActualMsg = actualErrMsg;
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();

                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.MembershipNo);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.MembershipNo);
                messageDS.AcceptChanges();
            }


            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS);
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getSocietyMember(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS hisDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetSocietyMemberships");
            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == "IsDataRenewal")
            {
                cmd = DBCommandProvider.GetDBCommand("GetDataRenewalSocietyMemberships");
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            }
            else if (stringDS.DataStrings[1].StringValue == "IsSelfService")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSelfServSocietyMemberships");
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetSocietyMemberships");

                long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
                cmd.Parameters["EmployeeId"].Value = empId;
            }

            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.SocietyMembership.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SOCIETY_MEMBERSHIP_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            hisDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteSocietyMember(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.SocietyMembership.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            employeeHistDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            if (employeeHistDS.SocietyMembership[0].IsSelfServise) cmd.CommandText = "PRO_SELF_SOCIETY_MEMBER_DEL";
            else cmd.CommandText = "PRO_SOCIETY_MEMBERSHIP_DELETE";

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.SocietyMembershipRow memRow in employeeHistDS.SocietyMembership.Rows)
            {
                if (memRow.IsSocietyMembershipIdNull() == false)
                {
                    cmd.Parameters.AddWithValue("@p_SocietyMembershipId", memRow.SocietyMembershipId);
                }
                else
                {
                    cmd.Parameters.AddWithValue("@p_SocietyMembershipId", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SOCIETY_MEMBERSHIP_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(memRow.MembershipNo);
                else messageDS.ErrorMsg.AddErrorMsgRow(memRow.MembershipNo);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateSocietyMember(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.SocietyMembership.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            if (employeeHistDS.SocietyMembership[0].IsSelfServise) cmd.CommandText = "PRO_SELF_SOCIETY_MEMBER_UPD";
            else cmd.CommandText = "PRO_SOCIETY_MEMBERSHIP_UPD";

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.SocietyMembershipRow row in employeeHistDS.SocietyMembership.Rows)
            {
                cmd.Parameters.AddWithValue("p_MembershipCategory", row.MembershipCategory);
                cmd.Parameters.AddWithValue("p_MembershipNo", row.MembershipNo);
                cmd.Parameters.AddWithValue("p_OrganizationName", row.OrganizationName);
                cmd.Parameters.AddWithValue("p_CountryId", row.CountryId);
                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                cmd.Parameters.AddWithValue("p_SocietyMembershipId", row.SocietyMembershipId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SOCIETY_MEMBERSHIP_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        #endregion

        #region Qualification Shakir 16.02.15
        private DataSet _createQualifications(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateQualification");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            histDS.Merge(inputDS.Tables[histDS.Qualifications.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            foreach (EmployeeHistDS.QualificationsRow Qtion in histDS.Qualifications.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["QualificationID"].Value = (object)genPK;

                if (Qtion.IsQualificationNameNull() == false)
                {
                    cmd.Parameters["QualificationName"].Value = Qtion.QualificationName;
                }
                else
                {
                    cmd.Parameters["QualificationName"].Value = DBNull.Value;
                }

                if (Qtion.IsDetailsNull() == false)
                {
                    cmd.Parameters["Details"].Value = Qtion.Details;
                }
                else
                {
                    cmd.Parameters["Details"].Value = DBNull.Value;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && Qtion.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHILDREN_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateQualifications(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateQualification");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            histDS.Merge(inputDS.Tables[histDS.Qualifications.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            foreach (EmployeeHistDS.QualificationsRow Qtion in histDS.Qualifications.Rows)
            {
                if (Qtion.IsDetailsNull() == false)
                {
                    cmd.Parameters["Details"].Value = Qtion.Details;
                }
                else
                {
                    cmd.Parameters["Details"].Value = DBNull.Value;
                }

                if (Qtion.IsQualificationNameNull() == false)
                {
                    cmd.Parameters["QualificationName"].Value = Qtion.QualificationName;
                }
                else
                {
                    cmd.Parameters["QualificationName"].Value = DBNull.Value;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && Qtion.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CHILDREN_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getQualifications(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            //string EmployeeCode = "";

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();
            //EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetQualificationsList");

            //long empId = UtilDL.GetEmployeeId(connDS, EmployeeCode);
            //cmd.Parameters["EmployeeId"].Value = empId;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            EmployeeHistDS histDS = new EmployeeHistDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, histDS, histDS.Qualifications.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_QUALIFICATION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            histDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(histDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _deleteQualifications(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            //DataIntegerDS integerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            //integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            //integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteQualification");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataStringDS.DataString row in stringDS.DataStrings)
            {
                cmd.Parameters["QualificationName"].Value = (object)row.StringValue;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SPOUSE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }
        #endregion

        #region Education Degree :: WALI :: 14-Jun-2015 [For other clients except 'Beximco']
        private DataSet _updateDegreePosition(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SET_DEGREE_POSITION";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            histDS.Merge(inputDS.Tables[histDS.Degree.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            foreach (EmployeeHistDS.DegreeRow row in histDS.Degree)
            {
                cmd.Parameters.AddWithValue("p_DegreeName", row.DegreeName);
                cmd.Parameters.AddWithValue("p_Position", row.DegreePosition);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SET_EDU_DEGREE_POSITION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.DegreeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.DegreeName);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _createDegree(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EDU_DEGREE_CREATE_NEW";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            histDS.Merge(inputDS.Tables[histDS.Degree.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();

            foreach (EmployeeHistDS.DegreeRow row in histDS.Degree.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("p_DegreeId", (object)genPK);

                if (!row.IsDegreeNameNull()) cmd.Parameters.AddWithValue("p_DegreeName", row.DegreeName);
                else cmd.Parameters.AddWithValue("p_DegreeName", DBNull.Value);

                if (!row.IsDegreeFriendlyNameNull()) cmd.Parameters.AddWithValue("p_DegreeFriendlyName", row.DegreeFriendlyName);
                else cmd.Parameters.AddWithValue("p_DegreeFriendlyName", DBNull.Value);

                if (!row.IsIsActiveNull()) cmd.Parameters.AddWithValue("p_IsActive", Convert.ToInt32(row.IsActive));
                else cmd.Parameters.AddWithValue("p_IsActive", DBNull.Value);

                if (row.IsAssessmentYearNull() == false) cmd.Parameters.AddWithValue("p_AssessmentYear", row.AssessmentYear);
                else cmd.Parameters.AddWithValue("p_AssessmentYear", DBNull.Value);
                if (row.IsAPRMarksNull() == false) cmd.Parameters.AddWithValue("p_APRMarks", row.APRMarks);
                else cmd.Parameters.AddWithValue("p_APRMarks", DBNull.Value);

                if (row.IsAuthorityNull() == false) cmd.Parameters.AddWithValue("p_Authority", row.Authority);
                else cmd.Parameters.AddWithValue("p_Authority", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EDU_DEGREE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;

            //DataSet returnDS = new DataSet();

            //returnDS.Merge(histDS);
            //returnDS.Merge(stringDS);
            //returnDS.Merge(errDS);
            //returnDS.AcceptChanges();

            //return returnDS;
        }
        private DataSet _updateDegree(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EDU_DEGREE_UPDATE_NEW";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            histDS.Merge(inputDS.Tables[histDS.Degree.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            foreach (EmployeeHistDS.DegreeRow row in histDS.Degree.Rows)
            {
                if (!row.IsDegreeFriendlyNameNull()) cmd.Parameters.AddWithValue("p_DegreeFriendlyName", row.DegreeFriendlyName);
                else cmd.Parameters.AddWithValue("p_DegreeFriendlyName", DBNull.Value);

                if (!row.IsIsActiveNull()) cmd.Parameters.AddWithValue("p_IsActive", Convert.ToInt32(row.IsActive));
                else cmd.Parameters.AddWithValue("p_IsActive", DBNull.Value);

                if (!row.IsAssessmentYearNull()) cmd.Parameters.AddWithValue("p_AssessmentYear", row.AssessmentYear);
                else cmd.Parameters.AddWithValue("p_AssessmentYear", DBNull.Value);

                if (!row.IsDegreeNameNull()) cmd.Parameters.AddWithValue("p_Degreename", row.DegreeName);
                else cmd.Parameters.AddWithValue("p_Degreename", DBNull.Value);

                if (!row.IsAPRMarksNull()) cmd.Parameters.AddWithValue("p_APRMarks", row.APRMarks);
                else cmd.Parameters.AddWithValue("p_APRMarks", DBNull.Value);

                if (row.IsAuthorityNull() == false) cmd.Parameters.AddWithValue("p_Authority", row.Authority);
                else cmd.Parameters.AddWithValue("p_Authority", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TR_DEGREE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        #endregion

        #region Employee Relation :: Rony :: 12-May-2016
        private DataSet _createRelation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            histDS.Merge(inputDS.Tables[histDS.Relation.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_RELATION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.RelationRow row in histDS.Relation.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_RelationID", genPK);
                cmd.Parameters.AddWithValue("p_RelationName", row.RelationName);
                cmd.Parameters.AddWithValue("p_Gender", row.Gender);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RELATION_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();
            }

            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _getRelationList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetRelationList");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, histDS, histDS.Relation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_RELATION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                //return errDS;
            }

            histDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(histDS.Relation);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateRelation(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Relation.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_RELATION_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.RelationRow row in histDS.Relation.Rows)
            {
                cmd.Parameters.AddWithValue("p_RelationName", row.RelationName);
                cmd.Parameters.AddWithValue("p_Gender", row.Gender);
                cmd.Parameters.AddWithValue("p_RelationID", row.RelationID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RELATION_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.RelationName );
                else messageDS.ErrorMsg.AddErrorMsgRow(row.RelationName);
            }
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _deleteRelation(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Relation.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_RELATION_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.RelationRow row in histDS.Relation.Rows)
            {
                cmd.Parameters.AddWithValue("p_RelationID", row.RelationID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RELATION_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.RelationName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.RelationName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        #endregion

        #region Organization Type :: Rony :: 16-May-2016
        private DataSet _createOrganizationType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            histDS.Merge(inputDS.Tables[histDS.OrganizationType.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ORGANIZATION_TYPE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.OrganizationTypeRow row in histDS.OrganizationType.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_OrganizationTypeID", genPK);
                cmd.Parameters.AddWithValue("p_OrganizationType", row.OrganizationType);
                cmd.Parameters.AddWithValue("p_Description", row.Description);
                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ORGANIZATION_TYPE_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();
            }

            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _getOrganizationTypeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetOrganizationTypeList");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, histDS, histDS.OrganizationType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ORGANIZATION_TYPE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                //return errDS;
            }

            histDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(histDS.OrganizationType);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateOrganizationType(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.OrganizationType.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ORGANIZATION_TYPE_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.OrganizationTypeRow row in histDS.OrganizationType.Rows)
            {
                cmd.Parameters.AddWithValue("p_OrganizationType", row.OrganizationType);
                cmd.Parameters.AddWithValue("p_Description", row.Description);
                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("p_OrganizationTypeID", row.OrganizationTypeID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ORGANIZATION_TYPE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.OrganizationType);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.OrganizationType);
            }
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _deleteOrganizationType(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.OrganizationType.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ORGANIZATION_TYPE_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.OrganizationTypeRow row in histDS.OrganizationType.Rows)
            {
                cmd.Parameters.AddWithValue("p_OrganizationTypeID", row.OrganizationTypeID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ORGANIZATION_TYPE_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.OrganizationType);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.OrganizationType);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        #endregion

        private DataSet _commonAnnounceByMailSuccRecordAdd(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            DataStringDS EmailInfoDS = new DataStringDS();
            EmailInfoDS.Merge(inputDS.Tables[EmailInfoDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            EmailInfoDS.AcceptChanges();

            DataIntegerDS EmployeeIdDS = new DataIntegerDS();
            EmployeeIdDS.Merge(inputDS.Tables[EmployeeIdDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            EmployeeIdDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            long genPK = IDGenerator.GetNextGenericPK();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COMMON_ANNOUNCEMENT_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("p_ID", genPK);
            cmd.Parameters.AddWithValue("p_EmailSub", EmailInfoDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_EmailBody", EmailInfoDS.DataStrings[1].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            cmd.Dispose();


            foreach (DataRow row in EmployeeIdDS.DataIntegers.Rows)
            {
                cmd.CommandText = "PRO_COMMON_ANNOUNC_HIST_ADD";
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("p_ID", genPK);
                cmd.Parameters.AddWithValue("p_EmployeeID", row[0].ToString());

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMMON_ANNOUNCEMENT_SUCC_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        #region Rony :: 30 August 2016 :: Attachment update & delete
        private DataSet updateAttachment(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Attachment.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            long empId = UtilDL.GetEmployeeId(connDS, histDS.Attachment[0].EmployeeCode);
            long senderEmpId = UtilDL.GetEmployeeId(connDS, histDS.Attachment[0].SenderEmployeeCode);

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ATTACHMENT_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.AttachmentRow row in histDS.Attachment.Rows)
            {
                cmd.Parameters.AddWithValue("p_FileName", row.FileName);
                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                cmd.Parameters.AddWithValue("p_Purpose", row.Purpose);
                cmd.Parameters.AddWithValue("p_ActualFileName", row.ActualFileName);
                cmd.Parameters.AddWithValue("p_SenderEmployeeID", senderEmpId);
                cmd.Parameters.AddWithValue("p_AttachmentID", row.AttachmentID);
                cmd.Parameters.AddWithValue("p_EmployeeID", empId);

                if (!row.IsIssueDateNull()) cmd.Parameters.AddWithValue("p_IssueDate", row.IssueDate);
                else cmd.Parameters.AddWithValue("p_IssueDate", DBNull.Value);
                if (!row.IsExpiryDateNull()) cmd.Parameters.AddWithValue("p_ExpiryDate", row.ExpiryDate);
                else cmd.Parameters.AddWithValue("p_ExpiryDate", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ATTACHMENT_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();
            }
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _deleteAttachment(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            histDS.Merge(inputDS.Tables[histDS.Attachment.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ATTACHMENT_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.AttachmentRow row in histDS.Attachment.Rows)
            {
                cmd.Parameters.AddWithValue("p_AttachmentID", row.AttachmentID);
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ATTACHMENT_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeName + "-->" + row.FileName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeName + "-->" + row.FileName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        #endregion

        #region Rony :: 04 Sep 2016 :: Relative Information
        private DataSet _createRelativeInformation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            histDS.Merge(inputDS.Tables[histDS.RelativeInformation.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_RELATIVE_INFORMATION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.RelativeInformationRow row in histDS.RelativeInformation.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                cmd.Parameters.AddWithValue("p_Relative_EmpCode", row.Relative_EmployeeCode);
                cmd.Parameters.AddWithValue("p_RelationID", row.RelationID);
                if (!row.IsIsSelfServiseNull()) cmd.Parameters.AddWithValue("p_IsSelfServise", row.IsSelfServise.ToString().ToLower());
                else cmd.Parameters.AddWithValue("p_IsSelfServise", System.DBNull.Value);

                if (!row.IsIsDataRenewalNull()) cmd.Parameters.AddWithValue("p_IsDataRenewal", row.IsDataRenewal.ToString().ToLower());
                else cmd.Parameters.AddWithValue("p_IsDataRenewal", System.DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RELATIVE_INFORMATION_SAVE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();
            }

            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _getRelativeInformation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[1].StringValue == "IsDataRenewal") cmd = DBCommandProvider.GetDBCommand("GetDataRenewalRelativeInfo");
            else if (stringDS.DataStrings[1].StringValue == "IsSelfService") cmd = DBCommandProvider.GetDBCommand("GetSelfServRelativeInfo");
            else cmd = DBCommandProvider.GetDBCommand("GetRelativeInformation");
            
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, histDS, histDS.RelativeInformation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMP_RELATIVE_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                //return errDS;
            }

            histDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(histDS.RelativeInformation);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteRelativeInformation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.RelativeInformation.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_RELATIVE_INFORMATION_DEL";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.RelativeInformationRow row in employeeHistDS.RelativeInformation.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                cmd.Parameters.AddWithValue("p_Relative_EmployeeCode", row.Relative_EmployeeCode);
                if (!row.IsIsSelfServiseNull()) cmd.Parameters.AddWithValue("p_IsSelfServise", row.IsSelfServise.ToString().ToLower());
                else cmd.Parameters.AddWithValue("p_IsSelfServise", System.DBNull.Value );

                if (!row.IsIsDataRenewalNull()) cmd.Parameters.AddWithValue("p_IsDataRenewal", row.IsDataRenewal.ToString().ToLower());
                else cmd.Parameters.AddWithValue("p_IsDataRenewal", System.DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_RELATIVE_INFORMATION_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.Relative_EmployeeName + " --> " + row.RelationName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.Relative_EmployeeName + " --> " + row.RelationName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        #endregion

        #region RONY :: 21 Sep 2016 :: Employee Personal Data sheet Report
        private DataSet _getEmployeeProfileInfoForReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();
            DataBoolDS boolDS = new DataBoolDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Employee Information
            string sCode = "-1";
            
            bool isSelfService = Convert.ToBoolean(stringDS.DataStrings[stringDS.DataStrings.Count - 1].StringValue);
            stringDS.DataStrings[stringDS.DataStrings.Count - 1].Delete();
            stringDS.AcceptChanges();

            foreach (DataStringDS.DataString str in stringDS.DataStrings.Rows)
            {
                sCode += "," + str["StringValue"].ToString();
            }
            sCode += ",-1";

            if (isSelfService) cmd = DBCommandProvider.GetDBCommand("GetEmployeeByMultiEmpCode_Park");
            else cmd = DBCommandProvider.GetDBCommand("GetEmployeeByMultiEmpCode");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["EmpCode1"].Value = sCode;
            cmd.Parameters["EmpCode2"].Value = sCode;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, histDS, histDS.EmpPersonalData.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            histDS.AcceptChanges();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_PROFILE_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
            }
            #endregion

            #region Get Employee History
            cmd.Dispose();
            cmd.Parameters.Clear();
            adapter.Dispose();

            bool bError_Hist = false;
            int nRowAffected_Hist = -1;

            #region Get Children
            if (Convert.ToBoolean(boolDS.DataBools[0].BoolValue))
            {
                if (isSelfService)
                {
                    cmd = DBCommandProvider.GetDBCommand("GetChildrenListByMultiEmpCode_Park");
                }
                else cmd = DBCommandProvider.GetDBCommand("GetChildrenListByMultiEmpCode");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Children.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Relative Information
            if (Convert.ToBoolean(boolDS.DataBools[1].BoolValue))
            {
                if (isSelfService)
                {
                    cmd = DBCommandProvider.GetDBCommand("GetRelativeInformationByMultiEmpCode_Park");
                }
                else cmd = DBCommandProvider.GetDBCommand("GetRelativeInformationByMultiEmpCode");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.RelativeInformation.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Education
            if (Convert.ToBoolean(boolDS.DataBools[2].BoolValue))
            {
                if (isSelfService)
                {
                    cmd = DBCommandProvider.GetDBCommand("GetEducationListByMultiEmpCode_Park");
                }
                else cmd = DBCommandProvider.GetDBCommand("GetEducationListByMultiEmpCode");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Education.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Training
            if (Convert.ToBoolean(boolDS.DataBools[3].BoolValue))
            {
                if (isSelfService)
                {
                    cmd = DBCommandProvider.GetDBCommand("GetTrainingListByMultiEmpCode_Park");
                }
                else cmd = DBCommandProvider.GetDBCommand("GetTrainingListByMultiEmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Training.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Disciplinary
            if (Convert.ToBoolean(boolDS.DataBools[4].BoolValue))
            {
                if (isSelfService)
                {
                    cmd = DBCommandProvider.GetDBCommand("GetDisciplinaryByMultiEmpCode_Park");
                }
                else cmd = DBCommandProvider.GetDBCommand("GetDisciplinaryByMultiEmpCode");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Disciplinary.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Past Employment
            if (Convert.ToBoolean(boolDS.DataBools[5].BoolValue))
            {
                if (isSelfService) cmd = DBCommandProvider.GetDBCommand("GetEmploymentListByMultiEmpCode_Park");
                else cmd = DBCommandProvider.GetDBCommand("GetEmploymentListByMultiEmpCode");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Employment.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            if (Convert.ToBoolean(boolDS.DataBools[5].BoolValue) && isSelfService) //Past employment Function information
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmploymentFunctionByMultiEmpCode_Park");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Employment_Function.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Reference
            if (Convert.ToBoolean(boolDS.DataBools[6].BoolValue))
            {
                if (isSelfService) cmd = DBCommandProvider.GetDBCommand("GetReferenceListByMultiEmpCode_Park");
                else cmd = DBCommandProvider.GetDBCommand("GetReferenceListByMultiEmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Reference.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Guarantor
            if (Convert.ToBoolean(boolDS.DataBools[7].BoolValue))
            {
                if (isSelfService) cmd = DBCommandProvider.GetDBCommand("GetGuarantorListByMultiEmpCode_Park");
                else cmd = DBCommandProvider.GetDBCommand("GetGuarantorListByMultiEmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Guarantor.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Competency / Skill Profile
            if (Convert.ToBoolean(boolDS.DataBools[8].BoolValue))
            {
                if (isSelfService) cmd = DBCommandProvider.GetDBCommand("GetCompetencyListByMultiEmpCode_Park");
                else cmd = DBCommandProvider.GetDBCommand("GetCompetencyListByMultiEmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.CompetencyProfile.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Nomminee - Provident Fund, Gratuity and superannuation
            if (Convert.ToBoolean(boolDS.DataBools[9].BoolValue))
            {
                if (isSelfService) cmd = DBCommandProvider.GetDBCommand("GetNomineeListByMultiEmpCode_Park");
                else cmd = DBCommandProvider.GetDBCommand("GetNomineeListByMultiEmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Nominee.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Professional Qualification
            if (Convert.ToBoolean(boolDS.DataBools[10].BoolValue))
            {
                if (isSelfService) cmd = DBCommandProvider.GetDBCommand("getQualificationListByMultiEmpCode_Park");
                else cmd = DBCommandProvider.GetDBCommand("getQualificationListByMultiEmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Qualification.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Service Agreement
            if (Convert.ToBoolean(boolDS.DataBools[11].BoolValue))
            {
                if (isSelfService) cmd = DBCommandProvider.GetDBCommand("GetServiceAgreementByMultiEmpCode_Park");
                else cmd = DBCommandProvider.GetDBCommand("GetServiceAgreementByMultiEmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.ServiceAgreement.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Reward And Recognition
            if (Convert.ToBoolean(boolDS.DataBools[12].BoolValue))
            {
                if (isSelfService) cmd = DBCommandProvider.GetDBCommand("getRewardRecognitionByMultiEmpCode_Park");
                else cmd = DBCommandProvider.GetDBCommand("getRewardRecognitionByMultiEmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.RewardRecognition.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #region Get Society Membership
            if (Convert.ToBoolean(boolDS.DataBools[13].BoolValue))
            {
                if (isSelfService) cmd = DBCommandProvider.GetDBCommand("GetSocietyMembershipsByMultiEmpCode_Park");
                else cmd = DBCommandProvider.GetDBCommand("GetSocietyMembershipsByMultiEmpCode");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.SocietyMembership.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            
            #endregion

            #region Get Leave Information
            if (Convert.ToBoolean(boolDS.DataBools[14].BoolValue))
            {
                if (isSelfService)
                {
                    cmd = DBCommandProvider.GetDBCommand("GetLeaveInfoByMultiEmpCode_Park");

                    if (cmd == null) return UtilDL.GetCommandNotFound();

                    cmd.Parameters["EmpCode1"].Value = sCode;
                    cmd.Parameters["EmpCode2"].Value = sCode;

                    adapter.SelectCommand = cmd;

                    nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.EmpLeaveInformation.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                    histDS.AcceptChanges();
                }
            }
            #endregion

            #region Get Employee Job History
            if (Convert.ToBoolean(boolDS.DataBools[15].BoolValue))
            {
                if (isSelfService)
                {
                    cmd = DBCommandProvider.GetDBCommand("GetJobHistByMultiEmpCode_Park");

                    if (cmd == null) return UtilDL.GetCommandNotFound();

                    cmd.Parameters["EmpCode1"].Value = sCode;
                    cmd.Parameters["EmpCode2"].Value = sCode;

                    adapter.SelectCommand = cmd;

                    nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.EmployeeJobHist.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                    histDS.AcceptChanges();
                }
            }
            #endregion

            #region Get Spouse
            if (Convert.ToBoolean(boolDS.DataBools[16].BoolValue))
            {
                if (isSelfService) cmd = DBCommandProvider.GetDBCommand("GetSpouseByMultiEmpCode_Park");
                else cmd = DBCommandProvider.GetDBCommand("GetSpouseByMultiEmpCode");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmpCode1"].Value = sCode;
                cmd.Parameters["EmpCode2"].Value = sCode;

                adapter.SelectCommand = cmd;

                nRowAffected_Hist = ADOController.Instance.Fill(adapter, histDS, histDS.Spouse.TableName, connDS.DBConnections[0].ConnectionID, ref bError_Hist);
                histDS.AcceptChanges();
            }
            #endregion

            #endregion
            if (bError_Hist)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_PROFILE_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
            }
            histDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(histDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Occupation:: Hasinul :: 19-September-2016 >> Uploaded By Rony
        private DataSet _createOccupation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_OCCUPATION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Occupation.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.OccupationRow row in employeeHistDS.Occupation)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_OccupationId", genPK);
                cmd.Parameters.AddWithValue("p_OccupationName", row.OccupationName);

                if (!row.IsOccupationDetailsNull()) cmd.Parameters.AddWithValue("p_OccupationDetails", row.OccupationDetails);
                else cmd.Parameters.AddWithValue("p_OccupationDetails", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OCCUPATION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getOccupationList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetOccupationList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeHistDS, employeeHistDS.Occupation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_OCCUPATION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            employeeHistDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS.Occupation);
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _deleteOccupation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Occupation.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_OCCUPATION_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            foreach (EmployeeHistDS.OccupationRow row in employeeHistDS.Occupation.Rows)
            {
                cmd.Parameters.AddWithValue("p_OccupationId", row.OccupationId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OCCUPATION_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.OccupationName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.OccupationName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateOccupation(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_OCCUPATION_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Occupation.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.OccupationRow row in employeeHistDS.Occupation.Rows)
            {
                cmd.Parameters.AddWithValue("p_OccupationName", row.OccupationName);

                if (!row.IsOccupationDetailsNull()) cmd.Parameters.AddWithValue("p_OccupationDetails", row.OccupationDetails);
                else cmd.Parameters.AddWithValue("p_OccupationDetails", DBNull.Value);

                cmd.Parameters.AddWithValue("p_OccupationId", row.OccupationId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && row.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OCCUPATION_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
            }
            returnDS.Merge(errDS);
            return returnDS;
        }
        #endregion

        #region Major Topics:: Hasinul :: 19-September-2016
        private DataSet _createMajorTopic(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_MAJOR_TOPIC_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.MajorTopic.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.MajorTopicRow row in employeeHistDS.MajorTopic)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_MajorTopicId", genPK);
                cmd.Parameters.AddWithValue("p_MajorTopicName", row.MajorTopicName);

                if (!row.IsMajorTopicDetailsNull()) cmd.Parameters.AddWithValue("p_MajorTopicDetails", row.MajorTopicDetails);
                else cmd.Parameters.AddWithValue("p_MajorTopicDetails", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAJOR_TOPICS_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getMajorTopic(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetMajorTopicList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeHistDS, employeeHistDS.MajorTopic.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_MAJOR_TOPICS_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            employeeHistDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS.MajorTopic);
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _deleteMajorTopic(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.MajorTopic.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_MAJOR_TOPIC_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            foreach (EmployeeHistDS.MajorTopicRow row in employeeHistDS.MajorTopic.Rows)
            {
                cmd.Parameters.AddWithValue("p_MajorTopicId", row.MajorTopicId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAJOR_TOPICS_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.MajorTopicName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.MajorTopicName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateMajorTopic(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_MAJOR_TOPIC_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.MajorTopic.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.MajorTopicRow row in employeeHistDS.MajorTopic.Rows)
            {
                cmd.Parameters.AddWithValue("p_MajorTopicName", row.MajorTopicName);

                if (!row.IsMajorTopicDetailsNull()) cmd.Parameters.AddWithValue("p_MajorTopicDetails", row.MajorTopicDetails);
                else cmd.Parameters.AddWithValue("p_MajorTopicDetails", DBNull.Value);

                cmd.Parameters.AddWithValue("p_MajorTopicId", row.MajorTopicId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAJOR_TOPICS_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
            }
            returnDS.Merge(errDS);
            return returnDS;
        }
        #endregion

        #region Training Organizer:: Hasinul :: 19-September-2016
        private DataSet _createTrainingOrganizer(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TRAINING_ORGANIZER_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.TrainingOrganizer.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.TrainingOrganizerRow row in employeeHistDS.TrainingOrganizer)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("P_OrganizerId", genPK);
                cmd.Parameters.AddWithValue("p_OrganizerName", row.OrganizerName);
                cmd.Parameters.AddWithValue("p_CountryId", row.Org_CountryId);

                if (!row.IsOrg_TelephoneNull()) cmd.Parameters.AddWithValue("p_Telephone", row.Org_Telephone);
                else cmd.Parameters.AddWithValue("p_Telephone", DBNull.Value);
                if (!row.IsOrg_FaxNull()) cmd.Parameters.AddWithValue("p_Fax", row.Org_Fax);
                else cmd.Parameters.AddWithValue("p_Fax", DBNull.Value);
                if (!row.IsOrg_EmailNull()) cmd.Parameters.AddWithValue("p_Email", row.Org_Email);
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);
                if (!row.IsOrg_URLNull()) cmd.Parameters.AddWithValue("p_URL", row.Org_URL);
                else cmd.Parameters.AddWithValue("p_URL", DBNull.Value);
                if (!row.IsOrg_AddressNull()) cmd.Parameters.AddWithValue("p_Address", row.Org_Address);
                else cmd.Parameters.AddWithValue("p_Address", DBNull.Value);
                if (!row.IsOrg_RemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Org_Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TRAINING_ORGANIZER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getTrainingOrganizer(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetTrainingOrganizerList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeHistDS, employeeHistDS.TrainingOrganizer.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_TRAINING_ORGANIZER_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            employeeHistDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS.TrainingOrganizer);
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _deleteTrainingOrganizer(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.TrainingOrganizer.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TRAINING_ORGANIZER_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            foreach (EmployeeHistDS.TrainingOrganizerRow row in employeeHistDS.TrainingOrganizer.Rows)
            {
                cmd.Parameters.AddWithValue("P_OrganizerId", row.OrganizerId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TRAINING_ORGANIZER_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.OrganizerName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.OrganizerName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateTrainingOrganizer(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_TRAINING_ORGANIZER_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.TrainingOrganizer.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.TrainingOrganizerRow row in employeeHistDS.TrainingOrganizer.Rows)
            {
                cmd.Parameters.AddWithValue("p_OrganizerName", row.OrganizerName);
                cmd.Parameters.AddWithValue("p_CountryId", row.Org_CountryId);

                if (!row.IsOrg_TelephoneNull()) cmd.Parameters.AddWithValue("p_Telephone", row.Org_Telephone);
                else cmd.Parameters.AddWithValue("p_Telephone", DBNull.Value);
                if (!row.IsOrg_FaxNull()) cmd.Parameters.AddWithValue("p_Fax", row.Org_Fax);
                else cmd.Parameters.AddWithValue("p_Fax", DBNull.Value);
                if (!row.IsOrg_EmailNull()) cmd.Parameters.AddWithValue("p_Email", row.Org_Email);
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);
                if (!row.IsOrg_URLNull()) cmd.Parameters.AddWithValue("p_URL", row.Org_URL);
                else cmd.Parameters.AddWithValue("p_URL", DBNull.Value);
                if (!row.IsOrg_AddressNull()) cmd.Parameters.AddWithValue("p_Address", row.Org_Address);
                else cmd.Parameters.AddWithValue("p_Address", DBNull.Value);
                if (!row.IsOrg_RemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Org_Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                cmd.Parameters.AddWithValue("P_OrganizerId", row.OrganizerId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_TRAINING_ORGANIZER_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
            }
            returnDS.Merge(errDS);
            return returnDS;
        }
        #endregion

        #region Employee Reference:: Hasinul :: 7-September-2016

        private DataSet _createEmployeeReference(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EMPLOYEE_REFRENCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Reference.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.ReferenceRow row in employeeHistDS.Reference)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_EmployeeRefernceId", genPK);
                cmd.Parameters.AddWithValue("p_REFERENCECODE", row.REFERENCECODE);

                if (!row.IsNameNull()) cmd.Parameters.AddWithValue("p_EmpReferenceName", row.Name);
                else cmd.Parameters.AddWithValue("p_EmpReferenceName", DBNull.Value);

                if (!row.IsOrganizationTypeIdNull()) cmd.Parameters.AddWithValue("p_OrganizationTypeId", row.OrganizationTypeId);
                else cmd.Parameters.AddWithValue("p_OrganizationTypeId", DBNull.Value);

                if (!row.IsOrganizationNameNull()) cmd.Parameters.AddWithValue("p_OrganizationName", row.OrganizationName);
                else cmd.Parameters.AddWithValue("p_OrganizationName", DBNull.Value);

                if (!row.IsDesignationNull()) cmd.Parameters.AddWithValue("p_DesignationName", row.Designation);
                else cmd.Parameters.AddWithValue("p_DesignationName", DBNull.Value);

                if (!row.IsGenderNull()) cmd.Parameters.AddWithValue("p_Gender", row.Gender);
                else cmd.Parameters.AddWithValue("p_Gender", DBNull.Value);

                if (!row.IsCountryIdNull()) cmd.Parameters.AddWithValue("p_CountryId", row.CountryId);
                else cmd.Parameters.AddWithValue("p_CountryId", DBNull.Value);

                if (!row.IsNationalIdNull()) cmd.Parameters.AddWithValue("p_NationalId", row.NationalId);
                else cmd.Parameters.AddWithValue("p_NationalId", DBNull.Value);


                if (!row.IsPresentAddressNull()) cmd.Parameters.AddWithValue("p_PresentAddress", row.PresentAddress);
                else cmd.Parameters.AddWithValue("p_PresentAddress", DBNull.Value);

                if (!row.IsPermanentAddressNull()) cmd.Parameters.AddWithValue("p_PermanentAddress", row.PermanentAddress);
                else cmd.Parameters.AddWithValue("p_PermanentAddress", DBNull.Value);

                if (!row.IsCellPhoneNull()) cmd.Parameters.AddWithValue("p_CellPhone", row.CellPhone);
                else cmd.Parameters.AddWithValue("p_CellPhone", DBNull.Value);

                if (!row.IsOfficePhoneNull()) cmd.Parameters.AddWithValue("p_OfficePhone", row.OfficePhone);
                else cmd.Parameters.AddWithValue("p_OfficePhone", DBNull.Value);

                if (!row.IsHomePhoneNull()) cmd.Parameters.AddWithValue("p_HomePhone", row.HomePhone);
                else cmd.Parameters.AddWithValue("p_HomePhone", DBNull.Value);

                if (!row.IsEmailNull()) cmd.Parameters.AddWithValue("p_Email", row.Email);
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                cmd.Parameters.AddWithValue("p_IsLocked", row.IsLocked);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REFRENCE_EMPLOYEE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getEmployeeReferenceList(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeReferenceList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeHistDS, employeeHistDS.Reference.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REFRENCE_EMPLOYEE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            employeeHistDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(employeeHistDS.Reference);
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _deleteEmployeeReference(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Reference.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EMPLOYEE_REFRENCE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            foreach (EmployeeHistDS.ReferenceRow row in employeeHistDS.Reference.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeReferenceId", row.EmpReferenceId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REFRENCE_EMPLOYEE_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(String.Format("{0} - {1}", row.REFERENCECODE, row.Name));
                else messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} - {1}", row.REFERENCECODE, row.Name));
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateEmployeeReference(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS employeeHistDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            //extract dbconnection
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EMPLOYEE_REFRENCE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            employeeHistDS.Merge(inputDS.Tables[employeeHistDS.Reference.TableName], false, MissingSchemaAction.Error);
            employeeHistDS.AcceptChanges();

            foreach (EmployeeHistDS.ReferenceRow row in employeeHistDS.Reference.Rows)
            {
                if (!row.IsNameNull()) cmd.Parameters.AddWithValue("p_EmpReferenceName", row.Name);
                else cmd.Parameters.AddWithValue("p_EmpReferenceName", DBNull.Value);

                if (!row.IsOrganizationTypeIdNull()) cmd.Parameters.AddWithValue("p_OrganizationTypeId", row.OrganizationTypeId);
                else cmd.Parameters.AddWithValue("p_OrganizationTypeId", DBNull.Value);

                if (!row.IsOrganizationNameNull()) cmd.Parameters.AddWithValue("p_OrganizationName", row.OrganizationName);
                else cmd.Parameters.AddWithValue("p_OrganizationName", DBNull.Value);

                if (!row.IsDesignationNull()) cmd.Parameters.AddWithValue("p_DesignationName", row.Designation);
                else cmd.Parameters.AddWithValue("p_DesignationName", DBNull.Value);

                if (!row.IsGenderNull()) cmd.Parameters.AddWithValue("p_Gender", row.Gender);
                else cmd.Parameters.AddWithValue("p_Gender", DBNull.Value);

                if (!row.IsCountryIdNull()) cmd.Parameters.AddWithValue("p_CountryId", row.CountryId);
                else cmd.Parameters.AddWithValue("p_CountryId", DBNull.Value);


                if (!row.IsNationalIdNull()) cmd.Parameters.AddWithValue("p_NationalId", row.NationalId);
                else cmd.Parameters.AddWithValue("p_NationalId", DBNull.Value);
                if (!row.IsPresentAddressNull()) cmd.Parameters.AddWithValue("p_PresentAddress", row.PresentAddress);
                else cmd.Parameters.AddWithValue("p_PresentAddress", DBNull.Value);
                if (!row.IsPermanentAddressNull()) cmd.Parameters.AddWithValue("p_PermanentAddress", row.PermanentAddress);
                else cmd.Parameters.AddWithValue("p_PermanentAddress", DBNull.Value);
                if (!row.IsCellPhoneNull()) cmd.Parameters.AddWithValue("p_CellPhone", row.CellPhone);
                else cmd.Parameters.AddWithValue("p_CellPhone", DBNull.Value);
                if (!row.IsOfficePhoneNull()) cmd.Parameters.AddWithValue("p_OfficePhone", row.OfficePhone);
                else cmd.Parameters.AddWithValue("p_OfficePhone", DBNull.Value);
                if (!row.IsHomePhoneNull()) cmd.Parameters.AddWithValue("p_HomePhone", row.HomePhone);
                else cmd.Parameters.AddWithValue("p_HomePhone", DBNull.Value);
                if (!row.IsEmailNull()) cmd.Parameters.AddWithValue("p_Email", row.Email);
                else cmd.Parameters.AddWithValue("p_Email", DBNull.Value);

                cmd.Parameters.AddWithValue("p_IsLocked", row.IsLocked);
                cmd.Parameters.AddWithValue("p_EmployeeReferenceId", row.EmpReferenceId);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REFRENCE_EMPLOYEE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
            }
            returnDS.Merge(errDS);
            return returnDS;
        }
        #endregion

        private DataSet GetEmployeeHistory_For_Report(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "Past_Employment")
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmpPastEmploymentForReport");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["SiteID1"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["OrganizationName"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["OrganizationTypeID1"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                cmd.Parameters["OrganizationTypeID2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[7].StringValue);
                cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[8].StringValue);
                cmd.Parameters["JobDuration"].Value = Convert.ToDecimal(stringDS.DataStrings[9].StringValue);
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, histDS, histDS.Employment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMP_HISTORY_FOR_REPORT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                //return errDS;
            }

            histDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(histDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetEmployment_FunctionByID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS hisDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            string EmployeeCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            EmployeeCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[1].StringValue == true.ToString())
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmployment_Function_SelfService");
                cmd.Parameters["EmploymentID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

                //else
                //{
                //    cmd = DBCommandProvider.GetDBCommand("GetEmployment_FunctionByID");

                //    long empId = UtilDL.GetEmployeeId(connDS, stringDS.DataStrings[0].StringValue);
                //    cmd.Parameters["EmploymentID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
                //}

                if (cmd == null) return UtilDL.GetCommandNotFound();

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, hisDS, hisDS.Employment_Function.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYMENT_FUNCTION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                hisDS.AcceptChanges();
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(hisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createEmployment_Function(DataSet inputDS, DBConnectionDS connDS, long employmentID)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            //bool isSucceeded = false;
            //bool isNotSucceeded = false;

            OleDbCommand cmd_del = new OleDbCommand();
            cmd_del.CommandText = "PRO_EMPLOYMENT_FUNC_DELETE";
            cmd_del.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EMPLOYMENT_FUNC_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmd_del == null) return UtilDL.GetCommandNotFound();

            EmployeeHistDS histDS = new EmployeeHistDS();

            histDS.Merge(inputDS.Tables[histDS.Employment_Function.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Delete
            foreach (EmployeeHistDS.Employment_FunctionRow row in histDS.Employment_Function.Rows)
            {
                cmd_del.Parameters.AddWithValue("p_EmploymentID", employmentID);

                bool bErrorDel = false;
                int nRowAffectedDel = -1;
                nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bErrorDel);
                cmd_del.Parameters.Clear();
                if (bErrorDel)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYMENT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Create

            foreach (EmployeeHistDS.Employment_FunctionRow row in histDS.Employment_Function.Rows)
            {

                cmd.Parameters.AddWithValue("p_EmploymentID", employmentID);

                if (!row.IsFunctionCodeNull()) cmd.Parameters.AddWithValue("p_FunctionCode", row.FunctionCode);
                else cmd.Parameters.AddWithValue("p_FunctionCode", DBNull.Value);

                if (!row.IsFunctionCodeNull()) cmd.Parameters.AddWithValue("Func_DateFrom", row.Func_DateFrom);
                else cmd.Parameters.AddWithValue("Func_DateFrom", DBNull.Value);

                if (!row.IsFunctionCodeNull()) cmd.Parameters.AddWithValue("Func_DateTo", row.Func_DateTo);
                else cmd.Parameters.AddWithValue("Func_DateTo", DBNull.Value);

                if (!row.IsFunctionCodeNull()) cmd.Parameters.AddWithValue("Func_Duration", row.Func_Duration);
                else cmd.Parameters.AddWithValue("Func_Duration", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID,
                   ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYMENT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        #region RoNy :: 16 Apr 2018 :: Self Service
        private DataSet _createEmployeeHistory_Parking(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeHistDS histDS = new EmployeeHistDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            histDS.Merge(inputDS.Tables[histDS.EmployeeHistory_Parking.TableName], false, MissingSchemaAction.Error);
            histDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            //cmd.CommandText = "PRO_EMPLOYEE_HISTORY_PARKING";PRO_EMPLOYEE_HISTORY_PARK_ADD
            cmd.CommandText = "PRO_EMPLOYEE_HISTORY_PARK_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeHistDS.EmployeeHistory_ParkingRow row in histDS.EmployeeHistory_Parking.Rows)
            {
                string[] splittedHistoryType = row.HistoryType.Trim(',').Split(',');
                for (int index = 0; index < splittedHistoryType.Length; index++)
                {
                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) return UtilDL.GetDBOperationFailed();

                    cmd.Parameters.AddWithValue("p_History_ParkingID", genPK);
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_SenderUserID", row.SenderUserID);
                    cmd.Parameters.AddWithValue("p_ReceiverUserID", row.ReceiverUserID);
                    cmd.Parameters.AddWithValue("p_Priority", row.Priority);
                    cmd.Parameters.AddWithValue("p_Approval_ActivityID", row.Approval_ActivityID);
                    cmd.Parameters.AddWithValue("p_HistoryType", splittedHistoryType[index].ToString());
                    cmd.Parameters.AddWithValue("p_LoginUserID", row.LoginUserID);
                    cmd.Parameters.AddWithValue("p_IsSubmitted", row.IsSubmitted);
                    cmd.Parameters.AddWithValue("p_IsApproved", false);
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_HISTORY_INFO_SEND.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                    }
                    cmd.Parameters.Clear();
                    if (bError) messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} - {1} ==> {2} - {3} [{4}]", row.EmployeeCode, row.EmployeeName, row.ReceiverUserEmpCode, row.ReceiverUserName, splittedHistoryType[index].ToString()));
                    else messageDS.SuccessMsg.AddSuccessMsgRow(String.Format("{0} - {1} ==> {2} - {3} [{4}]", row.EmployeeCode, row.EmployeeName, row.ReceiverUserEmpCode, row.ReceiverUserName, splittedHistoryType[index].ToString()));
                }

                if (!row.IsIsNotifyByMailNull() && row.IsNotifyByMail)
                {
                    #region Get Email Information
                    Mail.Mail mail = new Mail.Mail();
                    string messageBody = "", toAddress = "";
                    string returnMessage = "Email successfully sent.";
                    string host = UtilDL.GetHost();
                    Int32 port = UtilDL.GetPort();
                    bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
                    string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
                    string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
                    bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());

                    string senderEmail = "";

                    #region Set Message
                    messageBody += "Dear Sir/Madam,\n You have pending requests for self service data verification.<br>";
                    messageBody += "Please check the HR Management system for details.<br>";
                    messageBody += "<br><br><br>";
                    messageBody += "For details, click the following link: ";
                    messageBody += "<br>";
                    messageBody += row.URL;
                    messageBody += "<br>";
                    #endregion
                    #endregion

                    #region Send Notification Mails
                    senderEmail = row.SenderUserEmpEmail;
                    toAddress += row.ReceiverUserEmpEmail;

                    senderEmail = credentialEmailAddress;
                    returnMessage = "";
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, senderEmail, toAddress, "",
                                                       "Request for Self Service approval", messageBody, "[" + row.SenderUserEmpCode + "] " + row.SenderUserEmpName);
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, senderEmail, toAddress, "", "Request for Self Service approval", messageBody, "[" + row.SenderUserEmpCode + "] " + row.SenderUserEmpName);
                    }
                    if (returnMessage != "Email successfully sent.")
                    {
                        messageDS.ErrorMsg.AddErrorMsgRow("Email sending failed to " + "[" + row.SenderUserEmpCode + "] " + row.SenderUserEmpName);
                        messageDS.AcceptChanges();
                    }
                    #endregion
                }
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet GetEmployeeHistory_ParkingInfo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeHistDS hisDS = new EmployeeHistDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = new OleDbCommand();
            DataSet returnDS = new DataSet();
            string tableName = "";
            if (stringDS.DataStrings[0].StringValue == "SelfService")
            {
                #region Old
                //if (stringDS.DataStrings[1].StringValue == "Spouse")
                //{ cmd = DBCommandProvider.GetDBCommand("GetSelfServiceSpouseList"); tableName = hisDS.Spouse.TableName; }
                //if (stringDS.DataStrings[1].StringValue == "Children")
                //{ cmd = DBCommandProvider.GetDBCommand("GetSelfServChildList"); tableName = hisDS.Children.TableName; }
                //if (stringDS.DataStrings[1].StringValue == "Education")
                //{ cmd = DBCommandProvider.GetDBCommand("GetSelfServiceEduList"); tableName = hisDS.Education.TableName; }
                //if (stringDS.DataStrings[1].StringValue == "Nominee")
                //{ cmd = DBCommandProvider.GetDBCommand("GetSelfServNomineeListByEmpCode"); tableName = hisDS.Nominee.TableName; }
                //if (stringDS.DataStrings[1].StringValue == "Relative")
                //{ cmd = DBCommandProvider.GetDBCommand("GetSelfServRelativeInfo"); tableName = hisDS.RelativeInformation.TableName; }
                //if (stringDS.DataStrings[1].StringValue == "Qualification")
                //{ cmd = DBCommandProvider.GetDBCommand("GetSelfServiceSpouseList"); tableName = hisDS.Qualifications.TableName; }
                //if (stringDS.DataStrings[1].StringValue == "SocietyMembership")
                //{ cmd = DBCommandProvider.GetDBCommand("GetSelfServSocietyMemberships"); tableName = hisDS.SocietyMembership.TableName; }
                //if (stringDS.DataStrings[1].StringValue == "Training")
                //{ cmd = DBCommandProvider.GetDBCommand("GetSelfServTrainingList"); tableName = hisDS.Training.TableName; }
                //bool bError = false;
                //int nRowAffected = -1;
                //OleDbDataAdapter adapter = new OleDbDataAdapter(); 
                #endregion

                DataSet resultDS = new DataSet();

                #region Spouse Information :: Rony :: 27 Feb 2018
                resultDS = _executeCommandForEmployeeHistory_ParkingInfo(connDS, "GetSelfServiceSpouseList", hisDS.Spouse.TableName, stringDS.DataStrings[1].StringValue);
                if (resultDS != null && resultDS.Tables[hisDS.Spouse.TableName].Rows.Count > 0) returnDS.Merge(resultDS.Tables[hisDS.Spouse.TableName]);
                returnDS.AcceptChanges();
                #endregion
                
                #region Children Information
                resultDS = new DataSet();
                resultDS = _executeCommandForEmployeeHistory_ParkingInfo(connDS, "GetSelfServChildList", hisDS.Children.TableName, stringDS.DataStrings[1].StringValue);
                if (resultDS != null && resultDS.Tables.Count != 0 && resultDS.Tables[hisDS.Children.TableName].Rows.Count > 0) returnDS.Merge(resultDS.Tables[hisDS.Children.TableName]);
                returnDS.AcceptChanges();
                #endregion

                #region Education Information
                resultDS = new DataSet();
                resultDS = _executeCommandForEmployeeHistory_ParkingInfo(connDS, "GetSelfServiceEduList", hisDS.Education.TableName, stringDS.DataStrings[1].StringValue);
                if (resultDS != null && resultDS.Tables.Count != 0 && resultDS.Tables[hisDS.Education.TableName].Rows.Count > 0) returnDS.Merge(resultDS.Tables[hisDS.Education.TableName]);
                returnDS.AcceptChanges();
                #endregion

                #region Nominee Information
                resultDS = new DataSet();
                resultDS = _executeCommandForEmployeeHistory_ParkingInfo(connDS, "GetSelfServNomineeListByEmpCode", hisDS.Nominee.TableName, stringDS.DataStrings[1].StringValue);
                if (resultDS != null && resultDS.Tables.Count != 0 && resultDS.Tables[hisDS.Nominee.TableName].Rows.Count > 0) returnDS.Merge(resultDS.Tables[hisDS.Nominee.TableName]);
                returnDS.AcceptChanges();
                #endregion

                #region Relative Information
                resultDS = new DataSet();
                resultDS = _executeCommandForEmployeeHistory_ParkingInfo(connDS, "GetSelfServRelativeInfo", hisDS.RelativeInformation.TableName, stringDS.DataStrings[1].StringValue);
                if (resultDS != null && resultDS.Tables.Count != 0 && resultDS.Tables[hisDS.RelativeInformation.TableName].Rows.Count > 0) returnDS.Merge(resultDS.Tables[hisDS.RelativeInformation.TableName]);
                returnDS.AcceptChanges();
                #endregion

                #region Qualification Information
                resultDS = new DataSet();
                resultDS = _executeCommandForEmployeeHistory_ParkingInfo(connDS, "GetSelfServiceQualificationList", hisDS.Qualification.TableName, stringDS.DataStrings[1].StringValue);
                if (resultDS != null && resultDS.Tables.Count != 0 && resultDS.Tables[hisDS.Qualification.TableName].Rows.Count > 0) returnDS.Merge(resultDS.Tables[hisDS.Qualification.TableName]);
                returnDS.AcceptChanges();
                #endregion

                #region Society Membership Information
                resultDS = new DataSet();
                resultDS = _executeCommandForEmployeeHistory_ParkingInfo(connDS, "GetSelfServSocietyMemberships", hisDS.SocietyMembership.TableName, stringDS.DataStrings[1].StringValue);
                if (resultDS != null && resultDS.Tables.Count != 0 && resultDS.Tables[hisDS.SocietyMembership.TableName].Rows.Count > 0)returnDS.Merge(resultDS.Tables[hisDS.SocietyMembership.TableName]);
                returnDS.AcceptChanges();
                #endregion

                #region Training Information
                resultDS = new DataSet();
                resultDS = _executeCommandForEmployeeHistory_ParkingInfo(connDS, "GetSelfServTrainingList", hisDS.Training.TableName, stringDS.DataStrings[1].StringValue);
                if (resultDS != null && resultDS.Tables.Count != 0 && resultDS.Tables[hisDS.Training.TableName].Rows.Count > 0)returnDS.Merge(resultDS.Tables[hisDS.Training.TableName]);
                returnDS.AcceptChanges();
                #endregion
            }
            returnDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _executeCommandForEmployeeHistory_ParkingInfo(DBConnectionDS connDS, string CommandName, String tableName, string employeeCode)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            //EmployeeHistDS hisDS = new EmployeeHistDS();
            OleDbCommand cmd = new OleDbCommand();
            DataSet fillDS = new DataSet();
            cmd = DBCommandProvider.GetDBCommand(CommandName);
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["EmployeeCode"].Value = employeeCode;

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, fillDS, tableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_HISTORY_PARKING.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            DataSet returnDS = new DataSet();

            returnDS.Merge(fillDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        private DataSet _createEmployeeHistory_FromRecruitment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            EmployeeDS empDS = new EmployeeDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            empDS.Merge(inputDS.Tables[empDS.Employees.TableName], false, MissingSchemaAction.Error);
            empDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_EMP_RECRUITMENT_HIST_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (EmployeeDS.Employee row in empDS.Employees.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                cmd.Parameters.AddWithValue("p_Application_TrackingID", row.Application_TrackingID);
                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_HISTORY_INFO_SEND.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();
            }

            returnDS.Merge(errDS);
            return returnDS;
        }
    }
}
