using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class lmsHolidayDL
    {
        public lmsHolidayDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                #region Weekend
                case ActionID.NA_ACTION_LMS_GET_WEEKEND_LIST_ALL:
                    return this._getWeekendListAll(inputDS);
                case ActionID.ACTION_LMS_WEEKEND_UPD:
                    return this._updateWeekend(inputDS);
                #endregion

                #region Holiday
                case ActionID.ACTION_LMS_HOLIDAY_ADD:
                    return this._createHoliday(inputDS);
                case ActionID.ACTION_LMS_HOLIDAY_UPD:
                    return this._updateHoliday(inputDS);
                case ActionID.ACTION_LMS_HOLIDAY_DEL:
                    return this._deleteHoliday(inputDS);
                case ActionID.NA_ACTION_LMS_GET_HOLIDAY_LIST:
                    return this._getHolidayList(inputDS);
                case ActionID.NA_ACTION_LMS_GET_HOLIDAY_LIST_ALL:
                    return this._getHolidayListAll(inputDS);
                #endregion
                case ActionID.ACTION_HOLYDAY_TYPE_ADD:
                    return this._createHolidayType(inputDS);
                case ActionID.ACTION_HOLYDAY_TYPE_UPDATE:
                    return this._updateHolidayType(inputDS);
                case ActionID.ACTION_HOLYDAY_TYPE_DELETE:
                    return this._deleteHolidayType(inputDS);
                case ActionID.NA_ACTION_HOLYDAY_TYPE_GET:
                    return this._getHolidayType(inputDS);
                case ActionID.NA_ACTION_GET_HOLYDAY_TYPE_LIST:
                    return this._getHolidayTypeList(inputDS);
                case ActionID.ACTION_BRANCH_WISE_HOLIDAY_ADD:
                    return this._saveBranchWiseHoliday(inputDS);
                case ActionID.NA_ACTION_GET_SITELIST_HOLIDAY:
                    return this._getSiteLIstForHoliday(inputDS);
                case ActionID.NA_ACTION_LMS_GET_HOLIDAY_LIST_FOR_SITE:
                    return this._getHolidayLIstForSite(inputDS);

                #region DayLength
                case ActionID.NA_ACTION_LMS_GET_DAYLENGTH_LIST:
                    return this._getDayLengthList(inputDS);
                #endregion

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        #region Weekend
        private DataSet _getWeekendListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLmsWeekendListAll");
                        
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsHolidayPO holidayPO = new lmsHolidayPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, holidayPO, holidayPO.Weekends.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_WEEKEND_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            holidayPO.AcceptChanges();

            lmsHolidayDS holidayDS = new lmsHolidayDS();
            if (holidayPO.Weekends.Count > 0)
            {
                foreach (lmsHolidayPO.WeekendsRow poRow in holidayPO.Weekends.Rows)
                {
                    lmsHolidayDS.WeekendsRow row = holidayDS.Weekends.NewWeekendsRow();
                    if (poRow.IsWeekendIDNull() == false)
                    {
                        row.WeekendID = poRow.WeekendID;
                    }
                    if (poRow.IsNameOfDayNull() == false)
                    {
                        row.NameOfDay = poRow.NameOfDay;
                    }
                    if (poRow.IsDayLengthIDNull() == false)
                    {
                        row.DayLengthID = poRow.DayLengthID;
                    }
                    //Kaysar, 30-Apr-2011
                    if (poRow.IsSiteCodeNull() == false)
                    {
                        row.SiteCode = poRow.SiteCode;
                    }
                    if (poRow.IsSiteWeekendIDNull() == false)
                    {
                        row.SiteWeekendID = poRow.SiteWeekendID;
                    }
                    //

                    //Kaysar, 03-May-2011
                    if (poRow.IsSiteIDNull() == false)
                    {
                        row.SiteID = poRow.SiteID;
                    }
                    //

                    holidayDS.Weekends.AddWeekendsRow(row);
                    holidayDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(holidayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _updateWeekend(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsHolidayDS holidayDS = new lmsHolidayDS();
            DataBoolDS boolDS = new DataBoolDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            //cmd.CommandText = "PRO_LMS_WEEKENDS_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            holidayDS.Merge(inputDS.Tables[holidayDS.Weekends.TableName], false, MissingSchemaAction.Error);
            holidayDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            foreach (lmsHolidayDS.WeekendsRow row in holidayDS.Weekends.Rows)
            {
                //Kaysar, 03-May-2011
                if (boolDS.DataBools[0].BoolValue) //Indicates for Insert...
                {
                    long siteWeekendID = IDGenerator.GetNextGenericPK();
                    if (siteWeekendID == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }
                    
                    cmd.CommandText = "PRO_LMS_SITEWEEKEND_CREATE";
                    cmd.Parameters.AddWithValue("p_SiteWeekendID", siteWeekendID);
                }
                else //Indicates for Update...
                {
                    cmd.CommandText = "PRO_LMS_SITEWEEKEND_UPDATE";
                    cmd.Parameters.AddWithValue("p_SiteWeekendID", row.SiteWeekendID);
                }

                if (row.IsSiteCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SiteCode", row.SiteCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SiteCode", DBNull.Value);
                }
                //
                
                cmd.Parameters.AddWithValue("p_WEEKENDSID", row.WeekendID);

                try
                {
                    Convert.ToInt32(row.DayLengthID);
                    cmd.Parameters.AddWithValue("p_DAYLENGTHID", row.DayLengthID);
                }
                catch
                {
                    cmd.Parameters.AddWithValue("p_DAYLENGTHID", DBNull.Value);
                }

                if (row.IsSiteWeekendRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SiteWeekendRemarks", row.SiteWeekendRemarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SiteWeekendRemarks", DBNull.Value);
                }


                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_WEEKEND_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        #endregion

        #region Holiday
        private DataSet _createHoliday(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsHolidayDS holidayDS = new lmsHolidayDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_HOLIDAY_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            holidayDS.Merge(inputDS.Tables[holidayDS.Holidays.TableName], false, MissingSchemaAction.Error);
            holidayDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsHolidayDS.HolidaysRow row in holidayDS.Holidays.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_HOLIDAYID", genPK);

                if (row.IsNameOfHolidayNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_NAMEOFHOLIDAY", row.NameOfHoliday);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_NAMEOFHOLIDAY", DBNull.Value);
                }

                if (row.IsHoliDateNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HOLIDATE", row.HoliDate);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HOLIDATE", DBNull.Value);
                }

                if (row.IsRepeatsAnnuallyNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_REPEATSANNUALLY", row.RepeatsAnnually);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_REPEATSANNUALLY", DBNull.Value);
                }

                if (row.IsDayLengthIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DAYLENGTHID", row.DayLengthID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DAYLENGTHID", DBNull.Value);
                }

                if (row.IsDescriptionNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DESCRIPTION", row.Description);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DESCRIPTION", DBNull.Value);
                }

                if (!row.IsHoliDays_TypeIDNull()) cmd.Parameters.AddWithValue("p_Holidays_TypeID", row.HoliDays_TypeID);
                else cmd.Parameters.AddWithValue("p_Holidays_TypeID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_HOLIDAY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateHoliday(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsHolidayDS holidayDS = new lmsHolidayDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_HOLIDAY_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            holidayDS.Merge(inputDS.Tables[holidayDS.Holidays.TableName], false, MissingSchemaAction.Error);
            holidayDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsHolidayDS.HolidaysRow row in holidayDS.Holidays.Rows)
            {
                if (row.IsNameOfHolidayNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_NAMEOFHOLIDAY", row.NameOfHoliday);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_NAMEOFHOLIDAY", DBNull.Value);
                }

                if (row.IsRepeatsAnnuallyNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_REPEATSANNUALLY", row.RepeatsAnnually);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_REPEATSANNUALLY", DBNull.Value);
                }

                if (row.IsDayLengthIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DAYLENGTHID", row.DayLengthID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DAYLENGTHID", DBNull.Value);
                }

                if (row.IsDescriptionNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DESCRIPTION", row.Description);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DESCRIPTION", DBNull.Value);
                }

                if (row.IsHolidayIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HOLIDAYID", row.HolidayID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HOLIDAYID", DBNull.Value);
                }

                if (!row.IsHoliDays_TypeIDNull()) cmd.Parameters.AddWithValue("p_Holidays_TypeID", row.HoliDays_TypeID);
                else cmd.Parameters.AddWithValue("p_Holidays_TypeID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_HOLIDAY_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteHoliday(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_HOLIDAY_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_HOLIDAYID", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_HOLIDAY_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getHolidayList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DateTime date = dateDS.DataDates[0].DateValue;

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLmsHolidayList");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["FromDate"].Value = (object)date.Date;
            cmd.Parameters["ToDate"].Value = (object)date.Date.AddMonths(1).AddDays(-1);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsHolidayPO holidayPO = new lmsHolidayPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, holidayPO, holidayPO.Holidays.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_HOLIDAY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            holidayPO.AcceptChanges();

            lmsHolidayDS holidayDS = new lmsHolidayDS();

            if (holidayPO.Holidays.Rows.Count > 0)
            {
                foreach (lmsHolidayPO.HolidaysRow poRow in holidayPO.Holidays.Rows)
                {
                    lmsHolidayDS.HolidaysRow row = holidayDS.Holidays.NewHolidaysRow();

                    row.HolidayID = poRow.HolidayID;
                    row.NameOfHoliday = poRow.NameOfHoliday;
                    row.HoliDate = poRow.HoliDate;
                    row.sHoliDate = row.HoliDate.ToString("dd-MMM-yyyy");

                    if (poRow.IsRepeatsAnnuallyNull() == false)
                    {
                        row.RepeatsAnnually = poRow.RepeatsAnnually;
                    }

                    if (poRow.IsDayLengthIDNull() == false)
                    {
                        row.DayLengthID = poRow.DayLengthID;
                    }

                    if (poRow.IsDayLengthFriendlyNameNull() == false) //kaysar, 06 Dec, 2010
                    {
                        row.DayLengthFriendlyName = poRow.DayLengthFriendlyName;  //kaysar, 06 Dec, 2010
                    }

                    if (poRow.IsDescriptionNull() == false)
                    {
                        row.Description = poRow.Description;
                    }

                    if (poRow.IsDayLengthIDNull() == false) //kaysar, 06 Dec, 2010
                    {
                        row.DayLength = poRow.DayLength;
                    }

                    if (poRow.IsHoliDateStringNull() == false) //kaysar, 06 Dec, 2010
                    {
                        row.HoliDateString = poRow.HoliDateString;
                    }

                    holidayDS.Holidays.AddHolidaysRow(row);
                    holidayDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(holidayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getHolidayListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLmsHolidayListAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsHolidayPO holidayPO = new lmsHolidayPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, holidayPO, holidayPO.Holidays.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_HOLIDAY_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            holidayPO.AcceptChanges();

            lmsHolidayDS holidayDS = new lmsHolidayDS();

            if (holidayPO.Holidays.Rows.Count > 0)
            {
                foreach (lmsHolidayPO.HolidaysRow poRow in holidayPO.Holidays.Rows)
                {
                    lmsHolidayDS.HolidaysRow row = holidayDS.Holidays.NewHolidaysRow();

                    row.HolidayID = poRow.HolidayID;
                    row.NameOfHoliday = poRow.NameOfHoliday;
                    row.HoliDate = poRow.HoliDate;
                    row.sHoliDate = row.HoliDate.ToString("dd-MMM-yyyy");

                    if (poRow.IsRepeatsAnnuallyNull() == false)
                    {
                        row.RepeatsAnnually = poRow.RepeatsAnnually;
                    }

                    if (poRow.IsDayLengthIDNull() == false)
                    {
                        row.DayLengthID = poRow.DayLengthID;
                    }

                    if (poRow.IsDayLengthFriendlyNameNull() == false)  //kaysar, 06 Dec, 2010
                    {
                        row.DayLengthFriendlyName = poRow.DayLengthFriendlyName;  //kaysar, 06 Dec, 2010
                    }

                    if (poRow.IsDescriptionNull() == false)
                    {
                        row.Description = poRow.Description;
                    }

                    if (poRow.IsDayLengthIDNull() == false) //kaysar, 06 Dec, 2010
                    {
                        row.DayLength = poRow.DayLength;
                    }

                    if (poRow.IsHoliDateStringNull() == false) //kaysar, 06 Dec, 2010
                    {
                        row.HoliDateString = poRow.HoliDateString;
                    }

                    if (!poRow.IsHolidays_TypeIDNull()) row.HoliDays_TypeID = poRow.Holidays_TypeID;

                    holidayDS.Holidays.AddHolidaysRow(row);
                    holidayDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(holidayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        #endregion

        #region DayLength
        private DataSet _getDayLengthList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLmsDayLengthList");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsHolidayPO holidayPO = new lmsHolidayPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, holidayPO, holidayPO.DayLength.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_DAYLENGTH_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            holidayPO.AcceptChanges();

            lmsHolidayDS holidayDS = new lmsHolidayDS();

            if (holidayPO.DayLength.Rows.Count > 0)
            {
                foreach (lmsHolidayPO.DayLengthRow poRow in holidayPO.DayLength.Rows)
                {
                    lmsHolidayDS.DayLengthRow row = holidayDS.DayLength.NewDayLengthRow();

                    row.DayLengthID = poRow.DayLengthID;
                    
                    if (poRow.IsDescriptionNull() == false)
                    {
                        row.Description = poRow.Description;
                    }

                    if (poRow.IsDayLengthNull() == false)
                    {
                        row.DayLength = poRow.DayLength;
                    }

                    if (poRow.IsIsActiveNull() == false)
                    {
                        row.IsActive = poRow.IsActive;
                    }

                    if (poRow.IsFriendlyNameNull() == false)
                    {
                        row.FriendlyName = poRow.FriendlyName;
                    }

                    holidayDS.DayLength.AddDayLengthRow(row);
                    holidayDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(holidayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        #endregion
        private DataSet _createHolidayType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsHolidayDS holidayDS = new lmsHolidayDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_HOLIDAY_TYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            holidayDS.Merge(inputDS.Tables[holidayDS.HolydayType.TableName], false, MissingSchemaAction.Error);
            holidayDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsHolidayDS.HolydayTypeRow row in holidayDS.HolydayType.Rows)
            {

                if (row.IsHolydayTypeCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HolidayTypeCode", row.HolydayTypeCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HolidayTypeCode", DBNull.Value);
                }

                if (row.IsHolydayTypeNameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_TypeName", row.HolydayTypeName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_TypeName", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                }

                if (row.IsCreateUserIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Last_UpdateUser", row.CreateUserID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Last_UpdateUser", DBNull.Value);
                }

                if (row.IsOverTimeCategoryIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_OverTime_CategoryID", row.OverTimeCategoryID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_OverTime_CategoryID", DBNull.Value);
                } 

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_HOLIDAY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateHolidayType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsHolidayDS holidayDS = new lmsHolidayDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_HOLIDAY_TYPE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            holidayDS.Merge(inputDS.Tables[holidayDS.HolydayType.TableName], false, MissingSchemaAction.Error);
            holidayDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsHolidayDS.HolydayTypeRow row in holidayDS.HolydayType.Rows)
            {
                if (row.IsHolydayTypeCodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_HolidayTypeCode", row.HolydayTypeCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_HolidayTypeCode", DBNull.Value);
                }

                if (row.IsHolydayTypeNameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_TypeName", row.HolydayTypeName);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_TypeName", DBNull.Value);
                }

                if (row.IsRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                }
               
                if (row.IsCreateUserIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_Last_UpdateUser", row.CreateUserID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_Last_UpdateUser", DBNull.Value);
                }
                if (row.IsOverTimeCategoryIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_OverTime_CategoryID", row.OverTimeCategoryID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_OverTime_CategoryID", DBNull.Value);
                }

               

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_HOLIDAY_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteHolidayType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_HOLIDAY_TYPE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_HolidayTypeCode", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_HOLIDAY_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _getHolidayType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLmsHolidayTypeList");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsHolidayDS holidayDS = new lmsHolidayDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, holidayDS, holidayDS.HolydayType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_HOLIDAY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            holidayDS.AcceptChanges();

           
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(holidayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getHolidayTypeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetListForAllHolidayType");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsHolidayDS holidayDS = new lmsHolidayDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, holidayDS, holidayDS.HolydayType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_HOLIDAY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            holidayDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(holidayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _saveBranchWiseHoliday(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsHolidayDS holydayDS = new lmsHolidayDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            

            holydayDS.Merge(inputDS.Tables[holydayDS.SiteHoliday.TableName], false, MissingSchemaAction.Error);
            holydayDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmdDel = new OleDbCommand();
            cmdDel.CommandText = "PRO_LMS_SITE_WISE_HOLIDAY_DEL";
            cmdDel.CommandType = CommandType.StoredProcedure;

            if (cmdDel == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmdDel.Parameters.AddWithValue("p_HoliDayID", holydayDS.SiteHoliday[0].HolidayID);

            bool bErrorDel = false;
            int nRowAffectedDel = -1;
            nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);

            if (bErrorDel)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LMS_HOLIDAY_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            foreach (lmsHolidayDS.SiteHolidayRow holiday in holydayDS.SiteHoliday)
            {
                cmd.CommandText = "PRO_LMS_SITE_WISE_HOLIDAY_ADD";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters.AddWithValue("p_SiteID", holiday.SiteID);
                cmd.Parameters.AddWithValue("p_HoliDayID", holiday.HolidayID);


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_HOLIDAY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Dispose();
                cmd.Parameters.Clear();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getSiteLIstForHoliday(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            lmsHolidayDS holidayDS = new lmsHolidayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBranchWiseHoliday");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["HolidayID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);          

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, holidayDS, holidayDS.SiteHoliday.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_HOLIDAY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            holidayDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();            

            returnDS.Merge(holidayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getHolidayLIstForSite(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataIntegerDS integerDS = new DataIntegerDS();


            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLmsHolidayForSite");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsHolidayPO holidayPO = new lmsHolidayPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, holidayPO, holidayPO.Holidays.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_HOLIDAY_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            holidayPO.AcceptChanges();

            lmsHolidayDS holidayDS = new lmsHolidayDS();

            if (holidayPO.Holidays.Rows.Count > 0)
            {
                foreach (lmsHolidayPO.HolidaysRow poRow in holidayPO.Holidays.Rows)
                {
                    lmsHolidayDS.HolidaysRow row = holidayDS.Holidays.NewHolidaysRow();

                    row.HolidayID = poRow.HolidayID;
                    row.NameOfHoliday = poRow.NameOfHoliday;
                    row.HoliDate = poRow.HoliDate;
                    row.sHoliDate = row.HoliDate.ToString("dd-MMM-yyyy");

                    if (poRow.IsRepeatsAnnuallyNull() == false)
                    {
                        row.RepeatsAnnually = poRow.RepeatsAnnually;
                    }

                    if (poRow.IsDayLengthIDNull() == false)
                    {
                        row.DayLengthID = poRow.DayLengthID;
                    }

                    if (poRow.IsDayLengthFriendlyNameNull() == false)  //kaysar, 06 Dec, 2010
                    {
                        row.DayLengthFriendlyName = poRow.DayLengthFriendlyName;  //kaysar, 06 Dec, 2010
                    }

                    if (poRow.IsDescriptionNull() == false)
                    {
                        row.Description = poRow.Description;
                    }

                    if (poRow.IsDayLengthIDNull() == false) //kaysar, 06 Dec, 2010
                    {
                        row.DayLength = poRow.DayLength;
                    }

                    if (poRow.IsHoliDateStringNull() == false) //kaysar, 06 Dec, 2010
                    {
                        row.HoliDateString = poRow.HoliDateString;
                    }

                    if (!poRow.IsHolidays_TypeIDNull()) row.HoliDays_TypeID = poRow.Holidays_TypeID;

                    holidayDS.Holidays.AddHolidaysRow(row);
                    holidayDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(holidayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }


    }
}
