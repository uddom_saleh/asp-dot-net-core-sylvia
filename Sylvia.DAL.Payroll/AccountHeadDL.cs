/* *********************************************************************
 *  Compamy: Milllennium Information Solution Limited		 		           *  
 *  Author: Md. Hasinur Rahman Likhon				 				                   *
 *  Comment Date: March 29, 2006									                     *
 ***********************************************************************/

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
	/// <summary>
	/// Summary description for AccountHeadDL.
	/// </summary>
	public class AccountHeadDL
	{
		public AccountHeadDL()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public DataSet Execute(DataSet inputDS)
		{
			DataSet returnDS = new DataSet();
			ErrorDS errDS = new ErrorDS();

			ActionDS actionDS = new  ActionDS();
			actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
			actionDS.AcceptChanges();

			ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
			try
			{
				actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
			}
			catch
			{
				ErrorDS.Error err = errDS.Errors.NewError();
				err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
				err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
				err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
				err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
				errDS.Errors.AddError(err );
				errDS.AcceptChanges();

				returnDS.Merge( errDS );
				returnDS.AcceptChanges();
				return returnDS;
			}

			switch ( actionID )
			{
				case ActionID.ACTION_ACCOUNTHEAD_ADD:
					return _createAccountHead( inputDS );
                case ActionID.NA_ACTION_GET_ACCOUNTHEAD_LIST:
					return _getAccountHeadList(inputDS);
				case ActionID.ACTION_ACCOUNTHEAD_DEL:
					return this._deleteAccountHead( inputDS );
				case ActionID.ACTION_ACCOUNTHEAD_UPD:
					return this._updateAccountHead( inputDS );
                case ActionID.ACTION_DOES_ACCOUNTHEAD_EXIST:
                    return this._doesAccountHeadExist( inputDS );
                case ActionID.ACTION_DOES_FIXEDACCOUNT_EXIST:
                   return this._doesFixedAccountExist(inputDS);
				default:
					Util.LogInfo("Action ID is not supported in this class."); 
					ErrorDS.Error err = errDS.Errors.NewError();
					err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
					err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
					err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
					err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
					errDS.Errors.AddError( err );
					returnDS.Merge ( errDS );
					returnDS.AcceptChanges();
					return returnDS;
			}  // end of switch statement

		}
		private DataSet _createAccountHead(DataSet inputDS)
		{
			ErrorDS errDS = new ErrorDS();
			AccountHeadDS accountDS = new AccountHeadDS();
			DBConnectionDS connDS = new  DBConnectionDS();
			//extract dbconnection 
			connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
			connDS.AcceptChanges();
			//create command
			OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateAccountHead");
			if ( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }
      accountDS.Merge( inputDS.Tables[ accountDS.AccountHeads.TableName ], false, MissingSchemaAction.Error );
			accountDS.AcceptChanges();
      foreach(AccountHeadDS.AccountHead acc in accountDS.AccountHeads)
      {
        long genPK = IDGenerator.GetNextGenericPK();
        if( genPK == -1)
        {
          return UtilDL.GetDBOperationFailed();
        }
       
        cmd.Parameters["Id"].Value = (object) genPK;
        
        if ( acc.IsCodeNull() == false)
        {
          cmd.Parameters["Code"].Value = (object) acc.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value=null;
        }
        if ( acc.IsNameNull()==false)
        {
          cmd.Parameters["Name"].Value = (object) acc.Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value =null;
        }
        if (acc.IsIsActiveNull()==false)
        {
          cmd.Parameters["IsActive"].Value = (object) acc.IsActive;
        }
        else
        {
          cmd.Parameters["IsActive"].Value = null;
        }
        if (acc.IsFixedAccountNull() == false)
        {
            cmd.Parameters["fixedaccount"].Value = (object)acc.FixedAccount;
        }
          else
        {
            cmd.Parameters["fixedaccount"].Value = null;
        }
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_ACCOUNTHEAD_ADD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }

			errDS.Clear();
			errDS.AcceptChanges();
			return errDS;
		}
		private DataSet _deleteAccountHead(DataSet inputDS )
		{
			ErrorDS errDS = new ErrorDS();
			DBConnectionDS connDS = new  DBConnectionDS();      
			DataStringDS stringDS = new DataStringDS();
			connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
			connDS.AcceptChanges();
			stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
			stringDS.AcceptChanges();
			OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteAccountHead");
      if ( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }
			cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue;
			bool bError = false;
			int nRowAffected = -1;
			nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
			if (bError == true )
			{
				ErrorDS.Error err = errDS.Errors.NewError();
				err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
				err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
				err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
				err.ErrorInfo1 = ActionID.ACTION_ACCOUNTHEAD_DEL.ToString();
				errDS.Errors.AddError( err );
				errDS.AcceptChanges();
				return errDS;
			}
			return errDS;  // return empty ErrorDS
		}
		private DataSet _getAccountHeadList(DataSet inputDS)
		{
			ErrorDS errDS = new ErrorDS();
			DBConnectionDS connDS = new  DBConnectionDS();      
			DataLongDS longDS = new DataLongDS();
			connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
			connDS.AcceptChanges();
			OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAccountHeadList");
      if ( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }
			OleDbDataAdapter adapter = new OleDbDataAdapter();
			adapter.SelectCommand = cmd;
			AccountHeadPO accountPO = new AccountHeadPO();
			bool bError = false;
			int nRowAffected = -1;
			nRowAffected = ADOController.Instance.Fill(adapter, 
				accountPO, accountPO.AccountHeads.TableName, 
				connDS.DBConnections[0].ConnectionID, 
				ref bError);
			if (bError == true )
			{
				ErrorDS.Error err = errDS.Errors.NewError();
				err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
				err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
				err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACCOUNTHEAD_LIST.ToString();
				errDS.Errors.AddError( err );
				errDS.AcceptChanges();
				return errDS;

			}
			accountPO.AcceptChanges();
			AccountHeadDS accountDS = new AccountHeadDS();
			if ( accountPO.AccountHeads.Count > 0 )
			{
				foreach ( AccountHeadPO.AccountHead poAccount in accountPO.AccountHeads)
				{
					AccountHeadDS.AccountHead account = accountDS.AccountHeads.NewAccountHead();
					if (poAccount.IsAccountCodeNull()==false)
					{
						account.Code = poAccount.AccountCode;
					}
					if (poAccount.IsAccountNameNull()==false)
					{
						account.Name = poAccount.AccountName;
					}
                    if (poAccount.IsFixedAccountNull() == false)
                    {
                        account.FixedAccount = poAccount.FixedAccount;
                    }
						account.IsActive = poAccount.IsActive;
					accountDS.AccountHeads.AddAccountHead( account );
					accountDS.AcceptChanges();
				}
			}
			// now create the packet
			errDS.Clear();
			errDS.AcceptChanges();
			DataSet returnDS = new DataSet();
			returnDS.Merge( accountDS );
			returnDS.Merge( errDS );
			returnDS.AcceptChanges();
			return returnDS;
		}
  
		private DataSet _updateAccountHead(DataSet inputDS )
		{
			ErrorDS errDS = new ErrorDS();
			AccountHeadDS accountDS = new AccountHeadDS();
			DBConnectionDS connDS = new  DBConnectionDS();
			//extract dbconnection 
			connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
			connDS.AcceptChanges();
			//create command
			OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateAccountHead");
      if ( cmd == null )
      {
        return UtilDL.GetCommandNotFound();
      }
      accountDS.Merge( inputDS.Tables[ accountDS.AccountHeads.TableName ], false, MissingSchemaAction.Error );
      accountDS.AcceptChanges();
      foreach(AccountHeadDS.AccountHead acc in accountDS.AccountHeads)
      {
        
        if ( acc.IsCodeNull() == false)
        {
          cmd.Parameters["Code"].Value = (object) acc.Code;
        }
        else
        {
          cmd.Parameters["Code"].Value=null;
        }
        if ( acc.IsNameNull()==false)
        {
          cmd.Parameters["Name"].Value = (object) acc.Name ;
        }
        else
        {
          cmd.Parameters["Name"].Value =null;
        }
        if (acc.IsIsActiveNull()==false)
        {
          cmd.Parameters["IsActive"].Value = (object) acc.IsActive;
        }
        else
        {
          cmd.Parameters["IsActive"].Value = null;
        }        
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_ACCOUNTHEAD_UPD.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }

			errDS.Clear();
			errDS.AcceptChanges();
			return errDS;
		}

    private DataSet _doesAccountHeadExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     

      DBConnectionDS connDS = new DBConnectionDS();
      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      DataStringDS stringDS = new DataStringDS();
      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

      OleDbCommand cmd = null;
      cmd = DBCommandProvider.GetDBCommand("DoesAccountHeadExist");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }

      if(stringDS.DataStrings[0].IsStringValueNull()==false)
      {
        cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue ;
      }

      bool bError = false;
      int nRowAffected = -1;
      nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

      if (bError == true )
      {
        errDS.Clear();
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
        err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }      

      boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
      boolDS.AcceptChanges();
      errDS.Clear();
      errDS.AcceptChanges();

      returnDS.Merge( boolDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;
    }
    private DataSet _doesFixedAccountExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();            
            
            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesFixedAccountExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            
            if (integerDS.DataIntegers[0].IsIntegerValueNull() == false)
            {
                cmd.Parameters["fixedaccount"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        } 

	}
}
