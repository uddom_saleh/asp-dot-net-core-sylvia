/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: March 27, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class ReportSignatoryDL
    {
        public ReportSignatoryDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_ACTION_GET_REPORT_LIST_FORSIGNATORY:
                    return _getReport_ForSignatory(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_LIST_FOR_SIGNATORY:
                    return _getEmployeeList_ForSignatory(inputDS);
                case ActionID.ACTION_REPORT_SIGNATORY_CREATE:
                    return _createReportSignatory(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getReport_ForSignatory(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            ReportSignatoryDS reportDS = new ReportSignatoryDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            // WALI :: 05-Apr-2015
            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getReportList_ForSign");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["LoginID"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, reportDS, reportDS.ReportSignatory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REPORT_LIST_FORSIGNATORY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            reportDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(reportDS.ReportSignatory);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getEmployeeList_ForSignatory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            EmployeeDS empDS = new EmployeeDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_ForSignatory");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST_FOR_SIGNATORY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(empDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createReportSignatory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            ReportSignatoryDS repoSignDS = new ReportSignatoryDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            repoSignDS.Merge(inputDS.Tables[repoSignDS.ReportSignatory.TableName], false, MissingSchemaAction.Error);
            repoSignDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd_del = new OleDbCommand();
            OleDbCommand cmd = new OleDbCommand();
            cmd_del.CommandText = "PRO_REPORT_SIGNATORY_DELETE";
            cmd.CommandText = "PRO_REPORT_SIGNATORY_CREATE";
            cmd_del.CommandType = cmd.CommandType = CommandType.StoredProcedure;

            if (cmd_del == null || cmd == null) return UtilDL.GetCommandNotFound();

            // First delete the old information
            foreach (ReportSignatoryDS.ReportSignatoryRow row in repoSignDS.ReportSignatory.Rows)
            {
                cmd_del.Parameters.AddWithValue("p_ReportID", row.ReportID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_del.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REPORT_SIGNATORY_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            // Then insert new Information
            foreach (ReportSignatoryDS.ReportSignatoryRow row in repoSignDS.ReportSignatory.Rows)
            {
                cmd.Parameters.AddWithValue("@p_REPORTID", row.ReportID);
                if (!row.IsSignatory_1Null()) cmd.Parameters.AddWithValue("@p_SIGNATORY_1", row.Signatory_1);
                else cmd.Parameters.AddWithValue("p_SIGNATORY_1", DBNull.Value);
                if (!row.IsSignatory_2Null()) cmd.Parameters.AddWithValue("@p_SIGNATORY_2", row.Signatory_2);
                else cmd.Parameters.AddWithValue("p_SIGNATORY_1", DBNull.Value);
                if (!row.IsSignatory_3Null()) cmd.Parameters.AddWithValue("@p_SIGNATORY_3", row.Signatory_3);
                else cmd.Parameters.AddWithValue("p_SIGNATORY_1", DBNull.Value);
                cmd.Parameters.AddWithValue("@p_ISGENERALMESSAGE", row.IsGeneralMessage);

                // WALI :: 02-Apr-2015
                if (!row.IsAlternativeText_1Null()) cmd.Parameters.AddWithValue("p_AlternativeText_1", row.AlternativeText_1);
                else cmd.Parameters.AddWithValue("p_AlternativeText_1", DBNull.Value);
                if (!row.IsAlternativeText_2Null()) cmd.Parameters.AddWithValue("p_AlternativeText_2", row.AlternativeText_2);
                else cmd.Parameters.AddWithValue("p_AlternativeText_2", DBNull.Value);
                if (!row.IsAlternativeText_3Null()) cmd.Parameters.AddWithValue("p_AlternativeText_3", row.AlternativeText_3);
                else cmd.Parameters.AddWithValue("p_AlternativeText_3", DBNull.Value);
                //

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REPORT_SIGNATORY_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.ReportReference);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.ReportReference);
            }
            messageDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(repoSignDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

    }
}
 