using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for GradeDL.
    /// </summary>
    public class GradeDL
    {
        public GradeDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_GRADE_ADD:
                    return _createGrade(inputDS);
                case ActionID.NA_ACTION_GET_GRADE_LIST:
                    return _getGradeList(inputDS);
                case ActionID.ACTION_GRADE_DEL:
                    return this._deleteGrade(inputDS);
                case ActionID.ACTION_GRADE_UPD:
                    return this._updateGrade(inputDS);
                case ActionID.ACTION_DOES_GRADE_EXIST:
                    return this._doesGradeExist(inputDS);

                case ActionID.ACTION_SET_GRADE_POSITION:
                    return this._updateGradePosition(inputDS);
                case ActionID.ACTION_GRADE_TAX_REBATE_UPDATE:
                    return this._updateGradeTaxRebate(inputDS);

                case ActionID.ACTION_GRADE_CATEGORY_ADD:
                    return this._createGradeCategory(inputDS);
                case ActionID.ACTION_GRADE_CATEGORY_Update:
                    return this._updateGradeCategory(inputDS);
                case ActionID.ACTION_GRADE_CATEGORY_DELETE:
                    return this._deleteGradeCategory(inputDS);
                case ActionID.NA_ACTION_GET_GRADECATEGORY_LIST:
                    return this._getGradeCategoryList(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        

        private DataSet _createGrade(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            GradeDS gradeDS = new GradeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            long designaitonID = 0, hierarchy_GradeID = 0;

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateGrade");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            gradeDS.Merge(inputDS.Tables[gradeDS.Grades.TableName], false, MissingSchemaAction.Error);
            gradeDS.AcceptChanges();

            designaitonID = UtilDL.GetDesignationId(connDS, gradeDS.Grades[0].DesignationCode);
            hierarchy_GradeID = UtilDL.GetGradeId(connDS, gradeDS.Grades[0].Hierarchy_GradeCode);

            foreach (GradeDS.Grade grade in gradeDS.Grades)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["Id"].Value = (object)genPK;
                if (grade.IsCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)grade.Code;
                }
                else
                {
                    cmd.Parameters["Code"].Value = DBNull.Value;
                }
                if (grade.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = (object)grade.Name;
                }
                else
                {
                    cmd.Parameters["Name"].Value = DBNull.Value;
                }
                if (grade.IsMinBasicSalaryNull() == false)
                {
                    cmd.Parameters["MinBasic"].Value = (object)grade.MinBasicSalary;
                }
                else
                {
                    cmd.Parameters["MinBasic"].Value = DBNull.Value;
                }

                if (grade.IsMaxBasicSalaryNull() == false)
                {
                    cmd.Parameters["MaxBasic"].Value = (object)grade.MaxBasicSalary;
                }
                else
                {
                    cmd.Parameters["MaxBasic"].Value = DBNull.Value;
                }
                //if (grade.IsPositionNull() == false)
                //{
                //    cmd.Parameters["Position"].Value = (object)grade.Position;
                //}
                //else
                //{
                //    cmd.Parameters["Position"].Value = DBNull.Value;
                //}
                if (grade.IsActiveNull() == false)
                {
                    cmd.Parameters["Active"].Value = (object)grade.Active;
                }
                else
                {
                    cmd.Parameters["Active"].Value = DBNull.Value;
                }

                //Kaysar, 27-Sep-2011
                if (grade.IsLFAContributionNull() == false)
                {
                    cmd.Parameters["LFAContribution"].Value = (object)grade.LFAContribution;
                }
                else
                {
                    cmd.Parameters["LFAContribution"].Value = DBNull.Value;
                }
                //

                //Jarif, 03-Sep-2011
                if (!grade.IsTaxRebateNull())
                {
                    cmd.Parameters["TaxRebate"].Value = (object)grade.TaxRebate;
                }
                else
                {
                    cmd.Parameters["TaxRebate"].Value = DBNull.Value;
                }
                //

                //Shakir, 05-02-2013
                if (grade.IsPerincrementNull() == false)
                {
                    cmd.Parameters["Perincrement"].Value = (object)grade.Perincrement;
                }
                else
                {
                    cmd.Parameters["Perincrement"].Value = DBNull.Value;
                }
                //

                if (!grade.IsLFA_FlatAmountNull())
                {
                    cmd.Parameters["LFA_FlatAmount"].Value = (object)grade.LFA_FlatAmount;
                }
                else
                {
                    cmd.Parameters["LFA_FlatAmount"].Value = 0;
                }

                #region WALI :: Update :: 23-Feb-14
                if (grade.IsPerIncrement_PctNull() == false) cmd.Parameters["PerIncrement_Pct"].Value = (object)grade.PerIncrement_Pct;
                else cmd.Parameters["PerIncrement_Pct"].Value = DBNull.Value;
                #endregion

                #region WALI :: Update :: 21-Dec-14
                if (designaitonID != 0) cmd.Parameters["DesignationID"].Value = designaitonID;
                else cmd.Parameters["DesignationID"].Value = DBNull.Value;
                #endregion

                #region WALI :: Update :: 31-Dec-14
                if (hierarchy_GradeID != 0) cmd.Parameters["Hierarchy_GradeID"].Value = hierarchy_GradeID;
                else cmd.Parameters["Hierarchy_GradeID"].Value = DBNull.Value;
                #endregion

                cmd.Parameters["DefaultSelection"].Value = (object)grade.DefaultSelection;
                cmd.Parameters["IsExpatriate"].Value = (object)grade.IsExpatriate;

                if (!grade.IsResig_NoticePeriodNull()) cmd.Parameters["Resig_NoticePeriod"].Value = grade.Resig_NoticePeriod;
                else cmd.Parameters["Resig_NoticePeriod"].Value = 0;

                if (!grade.IsNoOfBasicSurrenderNull()) cmd.Parameters["NoOfBasicSurrender"].Value = grade.NoOfBasicSurrender;
                else cmd.Parameters["NoOfBasicSurrender"].Value = 0;

                if (!grade.IsGradeCategoryIDNull() && grade.GradeCategoryID != 0) cmd.Parameters["GradeCategoryID"].Value = grade.GradeCategoryID;
                else cmd.Parameters["GradeCategoryID"].Value = DBNull.Value;

                cmd.Parameters["IsAttachmentMandatory"].Value = (object)grade.IsAttachmentMandatory;
                cmd.Parameters["IsProRataForResignation"].Value = (object)grade.IsProRataForResignation;
                

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && grade.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GRADE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteGrade(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteGrade");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BONUS_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getGradeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            GradeDS gradeDS = new GradeDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGradeList");//Jarif(28 Sep 11)
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGradeList_New");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(28 Sep 11)...
            #region Old...
            //GradePO gradePO = new GradePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, gradePO, gradePO.Grades.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRADE_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //gradePO.AcceptChanges();

            //GradeDS gradeDS = new GradeDS();
            //if (gradePO.Grades.Count > 0)
            //{
            //    foreach (GradePO.Grade poGrade in gradePO.Grades)
            //    {
            //        GradeDS.Grade grade = gradeDS.Grades.NewGrade();
            //        if (poGrade.IsCodeNull() == false)
            //        {
            //            grade.Code = poGrade.Code;
            //        }
            //        if (poGrade.IsNameNull() == false)
            //        {
            //            grade.Name = poGrade.Name;
            //        }
            //        if (poGrade.IsMinBasicSalaryNull() == false)
            //        {
            //            grade.MinBasicSalary = poGrade.MinBasicSalary;
            //        }
            //        if (poGrade.IsMaxBasicSalaryNull() == false)
            //        {
            //            grade.MaxBasicSalary = poGrade.MaxBasicSalary;
            //        }
            //        if (poGrade.IsPositionNull() == false)
            //        {
            //            grade.Position = poGrade.Position;
            //        }
            //        if (poGrade.IsActiveNull() == false)
            //        {
            //            grade.Active = poGrade.Active;
            //        }
            //        //Kaysar, 27-Sep-2011
            //        if (poGrade.IsLFAContributionNull() == false)
            //        {
            //            grade.LFAContribution = poGrade.LFAContribution;
            //        }
            //        //
            //        gradeDS.Grades.AddGrade(grade);
            //        gradeDS.AcceptChanges();
            //    }
            //}
            #endregion

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, gradeDS, gradeDS.Grades.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRADE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            gradeDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(gradeDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _updateGrade(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            GradeDS gradeDS = new GradeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            long designaitonID = 0, hierarchy_GradeID = 0;

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            gradeDS.Merge(inputDS.Tables[gradeDS.Grades.TableName], false, MissingSchemaAction.Error);
            gradeDS.AcceptChanges();


            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_GRD";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (GradeDS.Grade grade in gradeDS.Grades)
            {
                if (grade.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_GRADE_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion



            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateGrade");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            foreach (GradeDS.Grade grade in gradeDS.Grades)
            {
                if (grade.IsCodeNull() == false)
                {
                    cmd.Parameters["Code"].Value = (object)grade.Code;
                }
                else
                {
                    cmd.Parameters["Code"].Value = null;
                }
                if (grade.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = (object)grade.Name;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }
                if (grade.IsMinBasicSalaryNull() == false)
                {
                    cmd.Parameters["MinBasic"].Value = (object)grade.MinBasicSalary;
                }
                else
                {
                    cmd.Parameters["MinBasic"].Value = null;
                }

                if (grade.IsMaxBasicSalaryNull() == false)
                {
                    cmd.Parameters["MaxBasic"].Value = (object)grade.MaxBasicSalary;
                }
                else
                {
                    cmd.Parameters["MaxBasic"].Value = null;
                }
                if (grade.IsPositionNull() == false)
                {
                    cmd.Parameters["Position"].Value = (object)grade.Position;
                }
                else
                {
                    cmd.Parameters["Position"].Value = null;
                }
                if (grade.IsActiveNull() == false)
                {
                    cmd.Parameters["Active"].Value = (object)grade.Active;
                }
                else
                {
                    cmd.Parameters["Active"].Value = null;
                }
                //Kaysar, 27-Sep-2011
                if (grade.IsLFAContributionNull() == false)
                {
                    cmd.Parameters["LFAContribution"].Value = (object)grade.LFAContribution;
                }
                else
                {
                    cmd.Parameters["LFAContribution"].Value = null;
                }
                //

                //Jarif, 03-Sep-2011
                if (!grade.IsTaxRebateNull())
                {
                    cmd.Parameters["TaxRebate"].Value = (object)grade.TaxRebate;
                }
                else
                {
                    cmd.Parameters["TaxRebate"].Value = null;
                }
                //
                //Shakir, 05-02-2013
                if (grade.IsPerincrementNull() == false)
                {
                    cmd.Parameters["Perincrement"].Value = (object)grade.Perincrement;
                }
                else
                {
                    cmd.Parameters["Perincrement"].Value = null;
                }
                //

                if (!grade.IsLFA_FlatAmountNull())
                {
                    cmd.Parameters["LFA_FlatAmount"].Value = (object)grade.LFA_FlatAmount;
                }
                else
                {
                    cmd.Parameters["LFA_FlatAmount"].Value = 0;
                }

                #region WALI :: Update :: 23-Feb-14
                if (grade.IsPerIncrement_PctNull() == false) cmd.Parameters["PerIncrement_Pct"].Value = (object)grade.PerIncrement_Pct;
                else cmd.Parameters["PerIncrement_Pct"].Value = 0;
                #endregion

                #region WALI :: Update :: 21-Dec-14
                designaitonID = UtilDL.GetDesignationId(connDS, grade.DesignationCode);
                if (designaitonID != 0) cmd.Parameters["DesignationID"].Value = (object)designaitonID;
                else cmd.Parameters["DesignationID"].Value = DBNull.Value;
                #endregion

                #region WALI :: Update :: 31-Dec-14
                hierarchy_GradeID = UtilDL.GetGradeId(connDS, gradeDS.Grades[0].Hierarchy_GradeCode);
                if (hierarchy_GradeID != 0) cmd.Parameters["Hierarchy_GradeID"].Value = hierarchy_GradeID;
                else cmd.Parameters["Hierarchy_GradeID"].Value = DBNull.Value;
                #endregion

                cmd.Parameters["DefaultSelection"].Value = (object)grade.DefaultSelection;
                cmd.Parameters["IsExpatriate"].Value = (object)grade.IsExpatriate;

                if (!grade.IsResig_NoticePeriodNull()) cmd.Parameters["Resig_NoticePeriod"].Value = grade.Resig_NoticePeriod;
                else cmd.Parameters["Resig_NoticePeriod"].Value = 0;

                if (!grade.IsNoOfBasicSurrenderNull()) cmd.Parameters["NoOfBasicSurrender"].Value = grade.NoOfBasicSurrender;
                else cmd.Parameters["NoOfBasicSurrender"].Value = 0;

                if (!grade.IsGradeCategoryIDNull() && grade.GradeCategoryID != 0) cmd.Parameters["GradeCategoryID"].Value = grade.GradeCategoryID;
                else cmd.Parameters["GradeCategoryID"].Value = DBNull.Value;

                cmd.Parameters["IsAttachmentMandatory"].Value = (object)grade.IsAttachmentMandatory;
                cmd.Parameters["IsProRataForResignation"].Value = (object)grade.IsProRataForResignation;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && grade.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GRADE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _doesGradeExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesGradeExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _updateGradePosition(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            GradeDS gradeDS = new GradeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdGradePosNull = new OleDbCommand();
            cmdGradePosNull.CommandText = "PRO_SET_GRADE_POSITION_NULL";
            cmdGradePosNull.CommandType = CommandType.StoredProcedure;

            bool aError = false;
            int aRowAffected = -1;
            aRowAffected = ADOController.Instance.ExecuteNonQuery(cmdGradePosNull, connDS.DBConnections[0].ConnectionID, ref aError);
            cmdGradePosNull.Dispose();
            if (aError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SET_GRADE_POSITION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }             
            #endregion


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SET_GRADE_POSITION";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            gradeDS.Merge(inputDS.Tables[gradeDS.Grades.TableName], false, MissingSchemaAction.Error);
            gradeDS.AcceptChanges();

            foreach (GradeDS.Grade grade in gradeDS.Grades)
            {
                cmd.Parameters.AddWithValue("p_GradeCode", grade.Code);
                cmd.Parameters.AddWithValue("p_Position", grade.Position);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SET_GRADE_POSITION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(grade.Code + " - " + grade.Name);
                else messageDS.ErrorMsg.AddErrorMsgRow(grade.Code + " - " + grade.Name);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateGradeTaxRebate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            GradeDS grdDS = new GradeDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            grdDS.Merge(inputDS.Tables[grdDS.Grades.TableName], false, MissingSchemaAction.Error);
            grdDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_GRADE_TAX_REBATE_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (GradeDS.Grade row in grdDS.Grades.Rows)
            {
                cmd.Parameters.AddWithValue("p_TaxRebate", row.TaxRebate);
                cmd.Parameters.AddWithValue("p_GradeId", row.Grade_Id);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GRADE_TAX_REBATE_UPDATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createGradeCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            GradeDS gradeCategoryDS = new GradeDS();
            //lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();
            gradeCategoryDS.Merge(inputDS.Tables[gradeCategoryDS.GradeCategory.TableName], false, MissingSchemaAction.Error);
            gradeCategoryDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool bError = false;
            int nRowAffected = -1;

            #region Delete Grade Category

            //cmd = DBCommandProvider.GetDBCommand("DELETE_GRADE_CATEGORY");
            //if (cmd == null)
            //{
            //    return UtilDL.GetCommandNotFound();
            //}
            //bError = false;
            //nRowAffected = -1;
            //nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.ACTION_GRADE_CATEGORY_ADD.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}                
            //cmd.Dispose();
            #endregion

            #region Create Grade Category

            //cmd.CommandText = "PRO_GRADE_CATEGORY_CREATE";
            //cmd.CommandType = CommandType.StoredProcedure;

            //foreach (lmsLeaveCategoryDS.LCConfigRow row in leaveCatDS.LCConfig.Rows)
            foreach (GradeDS.GradeCategoryRow row in gradeCategoryDS.GradeCategory.Rows)
            {
                cmd.CommandText = "PRO_GRADE_CATEGORY_CREATE";
                cmd.CommandType = CommandType.StoredProcedure;                
                cmd.Parameters.AddWithValue("p_GradeCategoryNAME", row.GradeCategoryName);
                
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GRADE_CATEGORY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
                cmd.Dispose();
            }
            #endregion

            return errDS;

        }

        private DataSet _updateGradeCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            GradeDS gradeCategoryDS = new GradeDS();
            //lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();
            gradeCategoryDS.Merge(inputDS.Tables[gradeCategoryDS.GradeCategory.TableName], false, MissingSchemaAction.Error);
            gradeCategoryDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool bError = false;
            int nRowAffected = -1;

            #region Update Grade Category

            cmd = DBCommandProvider.GetDBCommand("UPDATE_GRADE_CATEGORY");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["GRADECATEGORYNAME"].Value = gradeCategoryDS.GradeCategory[0].GradeCategoryName;
            cmd.Parameters["GRADECATEGORYID"].Value = gradeCategoryDS.GradeCategory[0].GradeCategoryID;                  

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_GRADE_CATEGORY_Update.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            cmd.Dispose();
            #endregion


            return errDS;

        }

        private DataSet _deleteGradeCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            GradeDS gradeCategoryDS = new GradeDS();
            //lmsLeaveCategoryDS leaveCatDS = new lmsLeaveCategoryDS();
            gradeCategoryDS.Merge(inputDS.Tables[gradeCategoryDS.GradeCategory.TableName], false, MissingSchemaAction.Error);
            gradeCategoryDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool bError = false;
            int nRowAffected = -1;

            #region Delete Grade Category
            cmd.CommandText = "PRO_GRADE_CATEGORY_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            //cmd = DBCommandProvider.GetDBCommand("DELETE_GRADE_CATEGORY");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            foreach (GradeDS.GradeCategoryRow row in gradeCategoryDS.GradeCategory.Rows)
            {
                //cmd.Parameters["p_GradeCategoryID"].Value = gradeCategoryDS.GradeCategory[0].GradeCategoryID;
                cmd.Parameters.AddWithValue("p_GradeCategoryID", row.GradeCategoryID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GRADE_CATEGORY_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion


            return errDS;

        }

        private DataSet _getGradeCategoryList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            GradeDS gradeCategoryDS = new GradeDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGradeCategoryList");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;                  

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, gradeCategoryDS, gradeCategoryDS.GradeCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRADECATEGORY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            gradeCategoryDS.AcceptChanges();            

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(gradeCategoryDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
    }
}
