/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: April 04, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class IncomeTaxParamDL
    {
        public IncomeTaxParamDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_ITPARAMETER_ADD:
                    return _createIncomrTaxParam(inputDS);
                case ActionID.ACTION_ITPARAMETER_UPD:
                    return _updateIncomrTaxParam(inputDS);
                case ActionID.NA_ACTION_Q_ITPARAMETER_ALL:
                    return _getParameterList(inputDS);
                case ActionID.NA_ACTION_Q_ITSLAB_ALL:
                    return _getSlabList(inputDS);
                case ActionID.ACTION_DOES_ITPARAMETER_EXIST:
                    return _doesITParameterExist(inputDS);
                case ActionID.NA_ACTION_Q_TAXHISTORY_BY_SALARYYEAR:
                    return _getIncomeTaxHistory(inputDS);
                case ActionID.ACTION_ITPARAMETER_DEL:
                    return _deleteITParameter(inputDS);
                case ActionID.NA_ACTION_Q_TAXHISTORY_BY_SALARYMONTH:
                    return _getIncomeTaxHistoryBySalaryMonth(inputDS);
                case ActionID.ACTION_INCOMETAX_UPD:
                    return _updateIncomrTax(inputDS);

                case ActionID.NA_ACTION_Q_TAXHISTORY_BY_SALARYYEAR_SP:
                    return _getIncomeTaxHistory_SP(inputDS);

                case ActionID.NA_ACTION_GET_ITIRSLAB_SP:
                    return _getIRSlabList_SP(inputDS);
                case ActionID.NA_ACTION_GET_ITINV_ALLOWANCE_SLAB_BY_YEAR:
                    return this._getInvestmentSlabByYear(inputDS);

                case ActionID.NA_ACTION_Q_TAXHISTORY_BY_SALARYYEAR_SM:
                    return _getIncomeTaxHistory_SM(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _doesITParameterExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesITParameterExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["FiscalYear"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _deleteITParameter(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            bool bErrorInv = false;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Delete Slabs
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteITParameterSlab");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["FiscalYear"].Value = stringDS.DataStrings[0].StringValue;
            }

            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            #endregion

            #region Delete Investment Slabs
            cmd = DBCommandProvider.GetDBCommand("DeleteIT_InvSlab");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            if (stringDS.DataStrings[0].IsStringValueNull() == false) cmd.Parameters["FiscalYear"].Value = stringDS.DataStrings[0].StringValue;

            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bErrorInv);
            #endregion

            if (bError == true || bErrorInv == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ITPARAMETER_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("DeleteITParameter");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
                cmd.Parameters["FiscalYear"].Value = stringDS.DataStrings[0].StringValue;
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ITPARAMETER_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
            }
            return errDS;  // return empty ErrorDS
        }

        private DataSet _createIncomrTaxParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            IncomeTaxParamDS paramDS = new IncomeTaxParamDS();
            DBConnectionDS connDS = new DBConnectionDS();

            #region Create Income Tax Parameter
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateITParameter");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            paramDS.Merge(inputDS.Tables[paramDS.IncomeTaxParameters.TableName], false, MissingSchemaAction.Error);
            paramDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            long nParamId = IDGenerator.GetNextGenericPK();

            IncomeTaxParamDS.IncomeTaxParameter param = paramDS.IncomeTaxParameters[0];
            cmd.Parameters["TaxParamId"].Value = nParamId; //;param.TaxParameterId;
            if (param.IsFiscalYearNull() == false)
            {
                cmd.Parameters["FiscalYear"].Value = param.FiscalYear;
            }
            if (param.IsAssessmentYearNull() == false)
            {
                cmd.Parameters["AssessmentYear"].Value = param.AssessmentYear;
            }
            if (param.IsMaximumInvestedAmountNull() == false)
            {
                cmd.Parameters["MaxInvAmount"].Value = param.MaximumInvestedAmount;
            }
            if (param.IsPerOIncThatShownAsInvestmentNull() == false)
            {
                cmd.Parameters["MaxInvestPercent"].Value = param.PerOIncThatShownAsInvestment;
            }
            if (param.IsMaxHRPercentNull() == false)
            {
                cmd.Parameters["MaxHRPercent"].Value = param.MaxHRPercent;
            }
            if (param.IsMaxHRAmountNull() == false)
            {
                cmd.Parameters["MaxHRAmount"].Value = param.MaxHRAmount;
            }
            if (param.IsConveyanceAmountNull() == false)
            {
                cmd.Parameters["MaxConvAmount"].Value = param.ConveyanceAmount;
            }
            if (param.IsMaxCPFPercentNull() == false)
            {
                cmd.Parameters["MaxCPFPercent"].Value = param.MaxCPFPercent;
            }
            if (param.IsMaxMediAmountNull() == false)
            {
                cmd.Parameters["MaxMediAmount"].Value = param.MaxMediAmount;
            }

            if (param.IsPerOInvThatExemFromTaxNull() == false)
            {
                cmd.Parameters["MaxInvExempPercent"].Value = param.PerOInvThatExemFromTax;
            }
            if (param.IsMinAmountOfTaxNull() == false)
            {
                cmd.Parameters["MinTaxAmount"].Value = param.MinAmountOfTax;
            }
            if (param.IsPFInProjectionNull() == false)
            {
                cmd.Parameters["PFIntProjection"].Value = param.PFInProjection;
            }
            //mislbd.Jarif Update Start(27-July-2009)
            if (param.IsCARTAXNull() == false)
            {
                cmd.Parameters["CarTax"].Value = param.CARTAX;
            }
            else cmd.Parameters["CarTax"].Value = 0;
            //mislbd.Jarif Update End(27-July-2009)

            //mislbd.Jarif Update Start(16-July-2012)
            if (!param.IsAdditionalNMMediAmountNull())
            {
                cmd.Parameters["AddMediAmountNMS"].Value = param.AdditionalNMMediAmount;
            }
            //mislbd.Jarif Update End(16-July-2012)

            //mislbd.Jarif Update Start(12-Sep-2012)
            if (!param.IsTaxableIncomeWithPFNull())
            {
                cmd.Parameters["TaxableIncomeWithPF"].Value = param.TaxableIncomeWithPF;
            }
            //mislbd.Jarif Update End(12-Sep-2012)
            if (!param.IsMinTaxAmount_DNull())
            {
                cmd.Parameters["MinTaxAmount_D"].Value = param.MinTaxAmount_D;
            }
            if (!param.IsMinTaxAmount_TNull())
            {
                cmd.Parameters["MinTaxAmount_T"].Value = param.MinTaxAmount_T;
            }
            if (!param.IsMinTaxAmount_OCNull())
            {
                cmd.Parameters["MinTaxAmount_OC"].Value = param.MinTaxAmount_OC;
            }


            if (!param.IsAS_Medical_NMSNull())
            {
                cmd.Parameters["AS_Medical_NMS"].Value = param.AS_Medical_NMS;
            }
            if (!param.IsAS_Medical_MSNull())
            {
                cmd.Parameters["AS_Medical_MS"].Value = param.AS_Medical_MS;
            }
            if (!param.IsPer_MediBasic_NMSNull())
            {
                cmd.Parameters["Per_MediBasic_NMS"].Value = param.Per_MediBasic_NMS;
            }
            if (!param.IsPer_MediBasic_MSNull())
            {
                cmd.Parameters["Per_MediBasic_MS"].Value = param.Per_MediBasic_MS;
            }
            if (!param.IsCarTax_FlatAmountNull())
            {
                cmd.Parameters["CarTax_FlatAmount"].Value = param.CarTax_FlatAmount;
            }
            if (!param.IsAS_CarTaxNull())
            {
                cmd.Parameters["AS_CarTax"].Value = param.AS_CarTax;
            }
            if (!param.IsAnnexureNull())
            {
                cmd.Parameters["Annexure"].Value = param.Annexure;
            }
            if (!param.IsPerquisiteRefferenceNull())
            {
                cmd.Parameters["PerquisiteRefference"].Value = param.PerquisiteRefference;
            }
            if (!param.IsAdmissibleLimitNull())
            {
                cmd.Parameters["AdmissibleLimit"].Value = param.AdmissibleLimit;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ITPARAMETER_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create Permission
            cmd = DBCommandProvider.GetDBCommand("CreateITSlab");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            IncomeTaxSlabDS slabDS = new IncomeTaxSlabDS();
            slabDS.Merge(inputDS.Tables[slabDS.IncomeTaxSlabs.TableName], false, MissingSchemaAction.Error);
            slabDS.Merge(inputDS.Tables[slabDS.IncomeTaxInvSlab.TableName], false, MissingSchemaAction.Error);
            slabDS.AcceptChanges();

            //connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
            //connDS.AcceptChanges();

            foreach (IncomeTaxSlabDS.IncomeTaxSlab slab in slabDS.IncomeTaxSlabs)
            {

                cmd.Parameters["TaxParamId"].Value = nParamId;//(object) slabDS.IncomeTaxSlabs[i].TaxParameterId;
                if (slab.IsSequenceNumberNull() == false)
                {
                    cmd.Parameters["SequenceNo"].Value = slab.SequenceNumber;
                }
                else
                {
                    cmd.Parameters["SequenceNo"].Value = null;
                }
                if (slab.IsIncomeAmountNull() == false)
                {
                    cmd.Parameters["IncomeAmount"].Value = slab.IncomeAmount;
                }
                else
                {
                    cmd.Parameters["IncomeAmount"].Value = null;
                }
                if (slab.IsTaxPercentNull() == false)
                {
                    cmd.Parameters["TaxPercent"].Value = slab.TaxPercent;
                }
                else
                {
                    cmd.Parameters["TaxPercent"].Value = null;
                }
                #region Create By Jarif(05 Aug 11)...
                if (!slab.IsAdditionalFmIncomeamountNull())
                {
                    cmd.Parameters["AdditionalFmIncomeamount"].Value = slab.AdditionalFmIncomeamount;
                }
                else
                {
                    cmd.Parameters["AdditionalFmIncomeamount"].Value = null;
                }
                #endregion
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ITSLAB_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

            }
            #endregion

            #region Create Investment Slabs
            cmd = DBCommandProvider.GetDBCommand("CreateIT_InvSlab");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            IncomeTaxSlabDS invSlabDS = new IncomeTaxSlabDS();
            invSlabDS.Merge(inputDS.Tables[invSlabDS.IncomeTaxInvSlab.TableName], false, MissingSchemaAction.Error);
            invSlabDS.AcceptChanges();

            foreach (IncomeTaxSlabDS.IncomeTaxInvSlabRow slab in invSlabDS.IncomeTaxInvSlab.Rows)
            {
                cmd.Parameters["TaxParamID"].Value = nParamId;

                if (!slab.IsSequenceNoNull()) cmd.Parameters["SequenceNo"].Value = slab.SequenceNo;
                else cmd.Parameters["SequenceNo"].Value = DBNull.Value;

                if (!slab.IsInvEligibleAmountNull()) cmd.Parameters["InvEligibleAmount"].Value = slab.InvEligibleAmount;
                else cmd.Parameters["InvEligibleAmount"].Value = DBNull.Value;

                if (!slab.IsInvRebatePerNull()) cmd.Parameters["InvRebatePer"].Value = slab.InvRebatePer;
                else cmd.Parameters["InvRebatePer"].Value = DBNull.Value;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ITSLAB_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateIncomrTaxParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            IncomeTaxParamDS paramDS = new IncomeTaxParamDS();
            DBConnectionDS connDS = new DBConnectionDS();
            string sFiscalYear = "";
            long nParamId = -1;

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateITParameter");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            paramDS.Merge(inputDS.Tables[paramDS.IncomeTaxParameters.TableName], false, MissingSchemaAction.Error);
            paramDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Update Param
            IncomeTaxParamDS.IncomeTaxParameter param = paramDS.IncomeTaxParameters[0];
            if (param.IsFiscalYearNull() == false)
            {
                cmd.Parameters["FiscalYear"].Value = param.FiscalYear;
                sFiscalYear = param.FiscalYear;
            }
            //if (param.IsAssessmentYearNull() == false)
            //{
            //  cmd.Parameters["AssessmentYear"].Value = param.AssessmentYear;
            //}
            if (param.IsMaximumInvestedAmountNull() == false)
            {
                cmd.Parameters["MaxInvAmount"].Value = param.MaximumInvestedAmount;
            }
            if (param.IsPerOIncThatShownAsInvestmentNull() == false)
            {
                cmd.Parameters["MaxInvestPercent"].Value = param.PerOIncThatShownAsInvestment;
            }
            if (param.IsMaxHRPercentNull() == false)
            {
                cmd.Parameters["MaxHRPercent"].Value = param.MaxHRPercent;
            }
            if (param.IsMaxHRAmountNull() == false)
            {
                cmd.Parameters["MaxHRAmount"].Value = param.MaxHRAmount;
            }
            if (param.IsConveyanceAmountNull() == false)
            {
                cmd.Parameters["MaxConvAmount"].Value = param.ConveyanceAmount;
            }
            if (param.IsMaxCPFPercentNull() == false)
            {
                cmd.Parameters["MaxCPFPercent"].Value = param.MaxCPFPercent;
            }
            if (param.IsMaxMediAmountNull() == false)
            {
                cmd.Parameters["MaxMediAmount"].Value = param.MaxMediAmount;
            }

            if (param.IsPerOInvThatExemFromTaxNull() == false)
            {
                cmd.Parameters["MaxInvExempPercent"].Value = param.PerOInvThatExemFromTax;
            }
            if (param.IsMinAmountOfTaxNull() == false)
            {
                cmd.Parameters["MinTaxAmount"].Value = param.MinAmountOfTax;
            }
            if (param.IsPFInProjectionNull() == false)
            {
                cmd.Parameters["PFIntProjection"].Value = param.PFInProjection;
            }
            //mislbd.Jarif Update Start(27-July-2009)
            if (param.IsCARTAXNull() == false)
            {
                cmd.Parameters["CarTax"].Value = param.CARTAX;
            }
            else cmd.Parameters["CarTax"].Value = 0;
            //mislbd.Jarif Update End(27-July-2009)

            //mislbd.Jarif Update Start(16-July-2012)
            if (!param.IsAdditionalNMMediAmountNull())
            {
                cmd.Parameters["AddMediAmountNMS"].Value = param.AdditionalNMMediAmount;
            }
            //mislbd.Jarif Update End(16-July-2012)

            //mislbd.Jarif Update Start(12-Sep-2012)
            if (!param.IsTaxableIncomeWithPFNull())
            {
                cmd.Parameters["TaxableIncomeWithPF"].Value = param.TaxableIncomeWithPF;
            }
            //mislbd.Jarif Update End(12-Sep-2012)

            if (!param.IsMinTaxAmount_DNull())
            {
                cmd.Parameters["MinTaxAmount_D"].Value = param.MinTaxAmount_D;
            }
            if (!param.IsMinTaxAmount_TNull())
            {
                cmd.Parameters["MinTaxAmount_T"].Value = param.MinTaxAmount_T;
            }
            if (!param.IsMinTaxAmount_OCNull())
            {
                cmd.Parameters["MinTaxAmount_OC"].Value = param.MinTaxAmount_OC;
            }


            if (!param.IsAS_Medical_NMSNull())
            {
                cmd.Parameters["AS_Medical_NMS"].Value = param.AS_Medical_NMS;
            }
            if (!param.IsAS_Medical_MSNull())
            {
                cmd.Parameters["AS_Medical_MS"].Value = param.AS_Medical_MS;
            }
            if (!param.IsPer_MediBasic_NMSNull())
            {
                cmd.Parameters["Per_MediBasic_NMS"].Value = param.Per_MediBasic_NMS;
            }
            if (!param.IsPer_MediBasic_MSNull())
            {
                cmd.Parameters["Per_MediBasic_MS"].Value = param.Per_MediBasic_MS;
            }

            if (!param.IsCarTax_FlatAmountNull())
            {
                cmd.Parameters["CarTax_FlatAmount"].Value = param.CarTax_FlatAmount;
            }
            if (!param.IsAS_CarTaxNull())
            {
                cmd.Parameters["AS_CarTax"].Value = param.AS_CarTax;
            }
            if (!param.IsAnnexureNull())
            {
                cmd.Parameters["Annexure"].Value = param.Annexure;
            }
            if (!param.IsPerquisiteRefferenceNull())
            {
                cmd.Parameters["PerquisiteRefference"].Value = param.PerquisiteRefference;
            }
            if (!param.IsAdmissibleLimitNull())
            {
                cmd.Parameters["AdmissibleLimit"].Value = param.AdmissibleLimit;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ITPARAMETER_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Update Slab
            // First Delete
            cmd = DBCommandProvider.GetDBCommand("DeleteITParameterSlab");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["FiscalYear"].Value = sFiscalYear;
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ITPARAMETER_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            else
            {
                #region Create Slab
                cmd = DBCommandProvider.GetDBCommand("CreateITSlab");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
                IncomeTaxSlabDS slabDS = new IncomeTaxSlabDS();
                slabDS.Merge(inputDS.Tables[slabDS.IncomeTaxSlabs.TableName], false, MissingSchemaAction.Error);
                slabDS.AcceptChanges();

                //connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
                //connDS.AcceptChanges();
                nParamId = UtilDL.GetIncomeTaxId(connDS, sFiscalYear);
                foreach (IncomeTaxSlabDS.IncomeTaxSlab slab in slabDS.IncomeTaxSlabs)
                {
                    cmd.Parameters["TaxParamId"].Value = nParamId;//(object) slabDS.IncomeTaxSlabs[i].TaxParameterId;
                    if (slab.IsSequenceNumberNull() == false)
                    {
                        cmd.Parameters["SequenceNo"].Value = slab.SequenceNumber;
                    }
                    else
                    {
                        cmd.Parameters["SequenceNo"].Value = null;
                    }
                    if (slab.IsIncomeAmountNull() == false)
                    {
                        cmd.Parameters["IncomeAmount"].Value = slab.IncomeAmount;
                    }
                    else
                    {
                        cmd.Parameters["IncomeAmount"].Value = null;
                    }
                    if (slab.IsTaxPercentNull() == false)
                    {
                        cmd.Parameters["TaxPercent"].Value = slab.TaxPercent;
                    }
                    else
                    {
                        cmd.Parameters["TaxPercent"].Value = null;
                    }
                    #region Create By Jarif(05 Aug 11)...
                    if (!slab.IsAdditionalFmIncomeamountNull())
                    {
                        cmd.Parameters["AdditionalFmIncomeamount"].Value = slab.AdditionalFmIncomeamount;
                    }
                    else
                    {
                        cmd.Parameters["AdditionalFmIncomeamount"].Value = null;
                    }
                    #endregion
                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ITSLAB_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion
            }
            #endregion

            #region Update Investment Slab
            IncomeTaxSlabDS invSlabDS = new IncomeTaxSlabDS();
            invSlabDS.Merge(inputDS.Tables[invSlabDS.IncomeTaxInvSlab.TableName], false, MissingSchemaAction.Error);
            invSlabDS.AcceptChanges();

            // First Delete
            cmd = DBCommandProvider.GetDBCommand("DeleteIT_InvSlab");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["FiscalYear"].Value = sFiscalYear;
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ITPARAMETER_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            else
            {
                #region Create Investment Slab
                cmd = DBCommandProvider.GetDBCommand("CreateIT_InvSlab");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (IncomeTaxSlabDS.IncomeTaxInvSlabRow slab in invSlabDS.IncomeTaxInvSlab.Rows)
                {
                    cmd.Parameters["TaxParamID"].Value = nParamId;

                    if (!slab.IsSequenceNoNull()) cmd.Parameters["SequenceNo"].Value = slab.SequenceNo;
                    else cmd.Parameters["SequenceNo"].Value = DBNull.Value;

                    if (!slab.IsInvEligibleAmountNull()) cmd.Parameters["InvEligibleAmount"].Value = slab.InvEligibleAmount;
                    else cmd.Parameters["InvEligibleAmount"].Value = DBNull.Value;

                    if (!slab.IsInvRebatePerNull()) cmd.Parameters["InvRebatePer"].Value = slab.InvRebatePer;
                    else cmd.Parameters["InvRebatePer"].Value = DBNull.Value;

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ITSLAB_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getSlabList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            string sSalaryYear = stringDS.DataStrings[0].StringValue;
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetITSlabList");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["FiscalYear"].Value = sSalaryYear;//(object) longDS.DataStrings[0].StringValue;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //IncomeTaxSlabPO slabPO = new IncomeTaxSlabPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, slabPO, slabPO.IncomeTaxSlabs.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ITSLAB_ALL.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //slabPO.AcceptChanges();
            //IncomeTaxSlabDS slabDS = new IncomeTaxSlabDS();
            //if (slabPO.IncomeTaxSlabs.Count > 0)
            //{
            //    foreach (IncomeTaxSlabPO.IncomeTaxSlab poSlab in slabPO.IncomeTaxSlabs)
            //    {
            //        IncomeTaxSlabDS.IncomeTaxSlab slab = slabDS.IncomeTaxSlabs.NewIncomeTaxSlab();
            //        if (poSlab.IsTaxParameterIdNull() == false)
            //        {
            //            slab.TaxParameterId = poSlab.TaxParameterId;
            //        }
            //        if (poSlab.IsSequenceNumberNull() == false)
            //        {
            //            slab.SequenceNumber = poSlab.SequenceNumber;
            //        }
            //        if (poSlab.IsIncomeAmountNull() == false)
            //        {
            //            slab.IncomeAmount = poSlab.IncomeAmount;
            //        }
            //        if (poSlab.IsTaxPercentNull() == false)
            //        {
            //            slab.TaxPercent = poSlab.TaxPercent;
            //        }
            //        slabDS.IncomeTaxSlabs.AddIncomeTaxSlab(slab);
            //        slabDS.AcceptChanges();
            //    }


            //}
            #endregion

            IncomeTaxSlabDS slabDS = new IncomeTaxSlabDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, slabDS, slabDS.IncomeTaxSlabs.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ITSLAB_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            slabDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(slabDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getParameterList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetITParamList");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //IncomeTaxParamPO paramPO = new IncomeTaxParamPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, paramPO, paramPO.IncomeTaxParameters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ITPARAMETER_ALL.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //paramPO.AcceptChanges();
            //IncomeTaxParamDS paramDS = new IncomeTaxParamDS();

            //if (paramPO.IncomeTaxParameters.Count > 0)
            //{
            //    foreach (IncomeTaxParamPO.IncomeTaxParameter poParam in paramPO.IncomeTaxParameters)
            //    {
            //        IncomeTaxParamDS.IncomeTaxParameter param = paramDS.IncomeTaxParameters.NewIncomeTaxParameter();
            //        if (poParam.IsTAXPARAMIDNull() == false)
            //        {
            //            param.TAXPARAMID = poParam.TAXPARAMID;
            //        }
            //        if (poParam.IsFiscalYearNull() == false)
            //        {
            //            param.FiscalYear = poParam.FiscalYear;
            //        }
            //        if (poParam.IsAssessmentYearNull() == false)
            //        {
            //            param.AssessmentYear = poParam.AssessmentYear;
            //        }
            //        if (poParam.IsMaximumInvestedAmountNull() == false)
            //        {
            //            param.MaximumInvestedAmount = poParam.MaximumInvestedAmount;
            //        }
            //        if (poParam.IsPerOIncThatShownAsInvestmentNull() == false)
            //        {
            //            param.PerOIncThatShownAsInvestment = poParam.PerOIncThatShownAsInvestment;
            //        }
            //        if (poParam.IsMaxHRPercentNull() == false)
            //        {
            //            param.MaxHRPercent = poParam.MaxHRPercent;
            //        }
            //        if (poParam.IsMaxHRAmountNull() == false)
            //        {
            //            param.MaxHRAmount = poParam.MaxHRAmount;
            //        }
            //        if (poParam.IsConveyanceAmountNull() == false)
            //        {
            //            param.ConveyanceAmount = poParam.ConveyanceAmount;
            //        }
            //        if (poParam.IsmaxmediamountNull() == false)
            //        {
            //            param.MaxMediAmount = poParam.maxmediamount;
            //        }
            //        if (poParam.IsMaxCPFPercentNull() == false)
            //        {
            //            param.MaxCPFPercent = poParam.MaxCPFPercent;
            //        }
            //        if (poParam.IsPerOInvThatExemFromTaxNull() == false)
            //        {
            //            param.PerOInvThatExemFromTax = poParam.PerOInvThatExemFromTax;
            //        }
            //        if (poParam.IsMinAmountOfTaxNull() == false)
            //        {
            //            param.MinAmountOfTax = poParam.MinAmountOfTax;
            //        }

            //        //mislbd.Jarif Update Start(27-July-2009)
            //        if (poParam.IsCARTAXNull() == false)
            //        {
            //            param.CARTAX = poParam.CARTAX;
            //        }
            //        //mislbd.Jarif Update End(27-July-2009)

            //        paramDS.IncomeTaxParameters.AddIncomeTaxParameter(param);
            //        paramDS.AcceptChanges();
            //    }
            //}
            #endregion

            IncomeTaxParamDS paramDS = new IncomeTaxParamDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, paramDS, paramDS.IncomeTaxParameters.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ITPARAMETER_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            paramDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(paramDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _getIncomeTaxHistory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetIncomeTaxHistory");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (dateDS.DataDates[0].DateValue.Month <= 6)
            {
                cmd.Parameters["StartMonthYear"].Value = new DateTime(dateDS.DataDates[0].DateValue.Year - 1, 7, 1);
                cmd.Parameters["EndMonthYear"].Value = dateDS.DataDates[0].DateValue;

            }
            if (dateDS.DataDates[0].DateValue.Month > 6)
            {
                cmd.Parameters["StartMonthYear"].Value = new DateTime(dateDS.DataDates[0].DateValue.Year, 7, 1);
                cmd.Parameters["EndMonthYear"].Value = dateDS.DataDates[0].DateValue;

            }



            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...
            #region Old...
            //TaxHistoryPO historyPO = new TaxHistoryPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, historyPO, historyPO.TaxHistories.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_TAXHISTORY_BY_SALARYYEAR.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //historyPO.AcceptChanges();
            //TaxHistoryDS historyDS = new TaxHistoryDS();
            //if (historyPO.TaxHistories.Count > 0)
            //{
            //    foreach (TaxHistoryPO.TaxHistory poHistory in historyPO.TaxHistories)
            //    {
            //        TaxHistoryDS.TaxHistory history = historyDS.TaxHistories.NewTaxHistory();
            //        if (poHistory.IsEmployeeCodeNull() == false)
            //        {
            //            history.EmployeeCode = poHistory.EmployeeCode;
            //        }
            //        if (poHistory.IsMonthYearNull() == false)
            //        {
            //            history.MonthYear = poHistory.MonthYear;
            //        }
            //        if (poHistory.IsRegularTaxNull() == false)
            //        {
            //            history.RegularTax = poHistory.RegularTax;
            //        }
            //        if (poHistory.IsExtraTaxNull() == false)
            //        {
            //            history.ExtraTax = poHistory.ExtraTax;
            //        }
            //        if (poHistory.IsExtraTaxbleIncomeNull() == false)
            //        {
            //            history.ExtraTaxbleIncome = poHistory.ExtraTaxbleIncome;
            //        }
            //        if (poHistory.IsTaxableIncomeNull() == false)
            //        {
            //            history.TaxableIncome = poHistory.TaxableIncome;
            //        }
            //        if (poHistory.IsCompanyPfNull() == false)
            //        {
            //            history.CompanyPf = poHistory.CompanyPf;
            //        }
            //        if (poHistory.IsConveyanceNull() == false)
            //        {
            //            history.Conveyance = poHistory.Conveyance;
            //        }
            //        if (poHistory.IsBasicSalaryNull() == false)
            //        {
            //            history.BasicSalary = poHistory.BasicSalary;
            //        }
            //        if (poHistory.IsHouseRentNull() == false)
            //        {
            //            history.HouseRent = poHistory.HouseRent;
            //        }
            //        if (poHistory.IsMedicalNull() == false)
            //        {
            //            history.Medical = poHistory.Medical;
            //        }

            //        //mislbd.Jarif Update Start(04-August-2009)
            //        if (poHistory.IsConveyanceFacilityNull() == false)
            //        {
            //            history.ConveyanceFacility = poHistory.ConveyanceFacility;
            //        }
            //        //mislbd.Jarif Update End(04-August-2009)


            //        historyDS.TaxHistories.AddTaxHistory(history);
            //        historyDS.AcceptChanges();
            //    }


            //}
            #endregion

            TaxHistoryDS historyDS = new TaxHistoryDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, historyDS, historyDS.TaxHistories.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_TAXHISTORY_BY_SALARYYEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            historyDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(historyDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getIncomeTaxHistoryBySalaryMonth(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetIncomeTaxHistoryBySMonth");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["SalaryMonthYear"].Value = dateDS.DataDates[0].DateValue;


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Load Data...

            TaxHistoryDS historyDS = new TaxHistoryDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, historyDS, historyDS.TaxHistories.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_TAXHISTORY_BY_SALARYMONTH.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            historyDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(historyDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _updateIncomrTax(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_INCOMETAX_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            TaxHistoryDS historyDS = new TaxHistoryDS();
            historyDS.Merge(inputDS.Tables[historyDS.TaxHistories.TableName], false, MissingSchemaAction.Error);
            historyDS.AcceptChanges();

            foreach (TaxHistoryDS.TaxHistory row in historyDS.TaxHistories.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                cmd.Parameters.AddWithValue("p_MonthYear", row.MonthYear);
                cmd.Parameters.AddWithValue("p_RegularTax", row.RegularTax);
                cmd.Parameters.AddWithValue("p_ExtraTax", row.ExtraTax);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_INCOMETAX_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getIncomeTaxHistory_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetIncomeTaxHistory_SP");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (dateDS.DataDates[0].DateValue.Month <= 6)
            {
                cmd.Parameters["StartMonthYear"].Value = new DateTime(dateDS.DataDates[0].DateValue.Year - 1, 7, 1);
                cmd.Parameters["EndMonthYear"].Value = dateDS.DataDates[0].DateValue;
            }
            if (dateDS.DataDates[0].DateValue.Month > 6)
            {
                cmd.Parameters["StartMonthYear"].Value = new DateTime(dateDS.DataDates[0].DateValue.Year, 7, 1);
                cmd.Parameters["EndMonthYear"].Value = dateDS.DataDates[0].DateValue;
            }
            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["StartMonthYear1"].Value = cmd.Parameters["StartMonthYear"].Value;
            cmd.Parameters["EndMonthYear1"].Value = cmd.Parameters["EndMonthYear"].Value;
            cmd.Parameters["EmployeeTypeID1"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID1"].Value = (object)integerDS.DataIntegers[1].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            TaxHistoryDS historyDS = new TaxHistoryDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, historyDS, historyDS.TaxHistories.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_TAXHISTORY_BY_SALARYYEAR_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            historyDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(historyDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getIRSlabList_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            string sSalaryYear = stringDS.DataStrings[0].StringValue;

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetITIRSlabList_SP");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["FiscalYear"].Value = sSalaryYear;//(object) longDS.DataStrings[0].StringValue;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            IncomeTaxSlabDS slabDS = new IncomeTaxSlabDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, slabDS, slabDS.IncomeTaxInvSlab.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ITIRSLAB_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            slabDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(slabDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getInvestmentSlabByYear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            IncomeTaxSlabDS slabDS = new IncomeTaxSlabDS();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetIT_InvSlab_ByYear");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["FiscalYear"].Value = stringDS.DataStrings[0].StringValue;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, slabDS, slabDS.IncomeTaxInvSlab.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ITINV_ALLOWANCE_SLAB_BY_YEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            slabDS.AcceptChanges();

            returnDS.Merge(slabDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getIncomeTaxHistory_SM(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetIncomeTaxHistory_SM");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (dateDS.DataDates[0].DateValue.Month <= 6)
            {
                cmd.Parameters["StartMonthYear"].Value = new DateTime(dateDS.DataDates[0].DateValue.Year - 1, 7, 1);
                cmd.Parameters["EndMonthYear"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            }
            else
            {
                cmd.Parameters["StartMonthYear"].Value = new DateTime(dateDS.DataDates[0].DateValue.Year, 7, 1);
                cmd.Parameters["EndMonthYear"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            TaxHistoryDS historyDS = new TaxHistoryDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, historyDS, historyDS.TaxHistories.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_TAXHISTORY_BY_SALARYYEAR_SM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            historyDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(historyDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        
    }
}
