using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class lmsLeaveQuotaDL
    {
        public lmsLeaveQuotaDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                #region Leave Quota

                case ActionID.ACTION_LMS_ELQ_ADD:
                    return this._createELQ(inputDS);
                case ActionID.ACTION_LMS_ELQDETAILS_ADD:
                    return this._createELQDetails(inputDS);
                case ActionID.ACTION_LMS_ELQDETAILS_UPD:
                    return this._updateELQDetails(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST:
                    return this._getLeaveQuotaList(inputDS);
                case ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HISTORY:
                    return this._getLmsEmployeeHistory(inputDS);
                case ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HIST_BY_EMPID:
                    return this._getLmsEmployeeHistoryByEmpID(inputDS);
                case ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HIST_BY_EMPCODE:
                    return this._getLmsEmployeeHistoryByCode(inputDS);
                //Kaysar, 28-Apr-2011
                case ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST_BY_LOGINID:
                    return this._getLeaveQuotaListByLoginID(inputDS);
                //
                case ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_HISTORY:
                    return this._getLeaveQuotaHistory(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST_NAME:
                    return this._getLeaveQuotaListName(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVE_DATE_LIST:
                    return this._getLeaveDateList(inputDS);
                case ActionID.ACTION_DEALLOCATE:
                    return this._deleteELQDetails(inputDS);
                case ActionID.ACTION_DEALLOCATE_EMPLOYEE:
                    return this._deleteELQdetailsEmployee(inputDS);


                case ActionID.NA_ACTION_GET_MAXELQID:
                    return this._getMaxELQID(inputDS);
                case ActionID.NA_ACTION_GET_LEAVEHISTORY:
                    return this._getLeaveHistory(inputDS);

                case ActionID.ACTION_LMS_ELQDETAILS_UPD_DAYS:
                    return this._updateELQDetailsDays(inputDS);
                #endregion

                case ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_BY_EMPID_CFYEAR:
                    return this._getLeaveQuotaByEmpID_CFYear(inputDS);

                case ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_TO_MODIFY:
                    return this._getLeaveQuotaHistoryToModify(inputDS);
                case ActionID.NA_ACTION_GET_LMS_FISCAL_YEAR:
                    return this._getLmsFiscalYear(inputDS);

                case ActionID.NA_ACTION_GET_EMPLOYEE_LIST_TO_ASSIGN_LEAVE:
                    return this._getEmployeeList_ToAssignLeave(inputDS);

                case ActionID.NA_ACTION_GET_EMPLOYEE_LIST_FOR_LEAVE_ALLOCATED_WITHOUTPOLICY:
                    return this._getEmployeeList_ForLeaveAllocatedWithoutPolicy(inputDS);
                case ActionID.ACTION_LEAVE_ALLOCATED_WITHOUTPOLICY:
                    return this._EmpLeaveAllocatedWithoutPolicy(inputDS);
                case ActionID.NA_ACTION_LMS_GET_ALL_LEAVE_QUOTA_HISTORY:
                    return this._getAllLeaveQuotaHistory(inputDS);
                case ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_BY_EMPID_CFYEAR_PREVYEAR:
                    return this._getLeaveQuotaByEmpID_CFYear_PrevYear(inputDS);

                case ActionID.NA_ACTION_LMS_GET_LEAVE_REQ_STS_FOR_EMP_TRANSFER:
                    return this._getPendingLeaveReqByEmpIDForTransProcess(inputDS);

                case ActionID.NA_ACTION_GET_EMPLOYEE_TO_BULK_LEAVEASSIGN:
                    return this._getEmployee_ToAssignLeaveBulk(inputDS);
                
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }
                
        #region Leave Quota
        private DataSet _createELQ(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            long elqID = 0;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_EMP_LEAVEQUOTA_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
            leaveQuotaDS.Merge(inputDS.Tables[leaveQuotaDS.EmployeeLeaveQuota.TableName], false, MissingSchemaAction.Error);
            leaveQuotaDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveQuotaDS.EmployeeLeaveQuotaRow row in leaveQuotaDS.EmployeeLeaveQuota.Rows)
            {
                elqID = IDGenerator.GetNextGenericPK();
                if (elqID == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_ELQID", elqID);

                if (row.IsEffectFromMonthYearNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EFFECTFROMMONTHYEAR", row.EffectFromMonthYear);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EFFECTFROMMONTHYEAR", DBNull.Value);
                }
                if (row.IsEffectToMonthYearNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EFFECTTOMONTHYEAR", row.EffectToMonthYear);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EFFECTTOMONTHYEAR", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_ELQ_ADD.ToString();//
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataLongDS longDS = new DataLongDS();
            longDS.DataLongs.AddDataLong(elqID);
            longDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(longDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        #region Update by jarif, 12.Jan.14
        #region Old...
        //private DataSet _createELQDetails(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    DBConnectionDS connDS = new DBConnectionDS();

        //    OleDbCommand cmd = new OleDbCommand();


        //    #region raqib 23 oct 2012
        //    //cmd.CommandText = "PRO_LMS_ELQDETAILS_CREATE";
        //    cmd.CommandText = "PRO_LMS_ELQDETAILS_CREATE_NEW";
        //    #endregion
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    if (cmd == null)
        //    {
        //        return UtilDL.GetCommandNotFound();
        //    }

        //    lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
        //    leaveQuotaDS.Merge(inputDS.Tables[leaveQuotaDS.ELQDetails.TableName], false, MissingSchemaAction.Error);
        //    leaveQuotaDS.AcceptChanges();

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //    connDS.AcceptChanges();

        //    foreach (lmsLeaveQuotaDS.ELQDetailsRow row in leaveQuotaDS.ELQDetails.Rows)
        //    {
        //        if (row.IsELQIDNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_ELQID", row.ELQID);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_ELQID", DBNull.Value);
        //        }

        //        if (row.IsLCDetailsIDNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_LCDETAILSID", row.LCDetailsID);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_LCDETAILSID", DBNull.Value);
        //        }

        //        if (row.IsLeaveTypeIDNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_LEAVETYPEID", row.LeaveTypeID);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_LEAVETYPEID", DBNull.Value);
        //        }

        //        if (row.IsEmployeeIDNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_EMPLOYEEID", DBNull.Value);
        //        }

        //        if (row.IsCalculatedDaysAllottedNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_DAYALLOTTED", row.CalculatedDaysAllotted);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_DAYALLOTTED", DBNull.Value);
        //        }

        //        if (row.IsMaxCarryOverNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_MaxDaysCarry", row.MaxCarryOver);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_MaxDaysCarry", DBNull.Value);
        //        }

        //        if (row.IsMaxEncashmentNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_MaxEncashmentCarry", row.MaxEncashment);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_MaxEncashmentCarry", DBNull.Value);
        //        }

        //        if (row.IsCalculatedEffecteDDateNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_CalculatedEffectedDate", row.CalculatedEffecteDDate);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_CalculatedEffectedDate", DBNull.Value);
        //        }

        //        if (row.IsCalculatedDaysImmatureNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_CalculatedDaysImmature", row.CalculatedDaysImmature);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_CalculatedDaysImmature", DBNull.Value);
        //        }

        //        if (row.IsCurrentUserIDNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_CurrentUserId", DBNull.Value);
        //        }

        //        bool bError = false;
        //        int nRowAffected = -1;
        //        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        //        if (bError)
        //        {
        //            ErrorDS.Error err = errDS.Errors.NewError();
        //            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        //            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //            err.ErrorInfo1 = ActionID.ACTION_LMS_ELQDETAILS_ADD.ToString();//
        //            errDS.Errors.AddError(err);
        //            errDS.AcceptChanges();
        //            return errDS;
        //        }
        //    }
        //    errDS.Clear();
        //    errDS.AcceptChanges();
        //    return errDS;
        //}
        #endregion

        private DataSet _createELQDetails(DataSet inputDS)
        {           
            #region Local Variables Declaration...
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_GetEmployee = new OleDbCommand();

            MessageDS messageDS = new MessageDS();
            DataStringDS StringDS = new DataStringDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            DataDateDS dateDS = new DataDateDS();
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            bool bError = false;
            int nRowAffected = -1;

            string employeeCode = "", branchCode = "";
            Int32 leaveTypeID = 0, employeeType = 0, elqID = 0, prev_elqID = 0, activitiesID = 0;
            DateTime processDate = new DateTime();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Data Extracting...
            activitiesID = Convert.ToInt32(LeaveActivities.Approve);
                        
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();
                        
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
                        
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            
            try
            {
                employeeCode = StringDS.DataStrings[0].StringValue;
                branchCode = StringDS.DataStrings[1].StringValue;

                leaveTypeID = integerDS.DataIntegers[0].IntegerValue;
                employeeType = integerDS.DataIntegers[1].IntegerValue;
                elqID = integerDS.DataIntegers[2].IntegerValue;
                prev_elqID = integerDS.DataIntegers[3].IntegerValue;

                processDate = dateDS.DataDates[0].DateValue;
            }
            catch
            {
            }

            #endregion

            #region Get employee information from DB...
            if (employeeCode == "All Employees")
            {
                #region Get Employee Information From DB...
                #region Selection for XML...               
                if (employeeType != -1 && branchCode != "0")
                {
                    cmd_GetEmployee = DBCommandProvider.GetDBCommand("GetEmpInfo_LA_BySiteWithEmpType");
                    cmd_GetEmployee.Parameters["SiteCode"].Value = (object)branchCode;
                    cmd_GetEmployee.Parameters["EmployeeType"].Value = (object)employeeType;
                }
                else if (employeeType != -1)
                {
                    cmd_GetEmployee = DBCommandProvider.GetDBCommand("GetEmpInfo_LeaveAllocation_ByEmpType");
                    cmd_GetEmployee.Parameters["EmployeeType"].Value = (object)employeeType;
                }
                else if (branchCode != "0")
                {
                    cmd_GetEmployee = DBCommandProvider.GetDBCommand("GetEmpInfo_LeaveAllocation_BySite");
                    cmd_GetEmployee.Parameters["SiteCode"].Value = (object)branchCode;
                }
                else // All Employee
                {
                    cmd_GetEmployee = DBCommandProvider.GetDBCommand("GetEmpInfo_LeaveAllocation_All");
                 
                }
                
                #endregion

                adapter.SelectCommand = cmd_GetEmployee;
                bError = false;
                nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_ELQDETAILS_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();

                    returnDS.Merge(errDS);
                    returnDS.Merge(messageDS);
                    return returnDS;
                }
                leaveQuotaDS.AcceptChanges();
                #endregion
            }
            else
            {
                #region Get Employee Information From DB...
                cmd_GetEmployee = DBCommandProvider.GetDBCommand("GetEmpInfo_LeaveAllocation_ByEmp");
                cmd_GetEmployee.Parameters["EmployeeCode"].Value = (object)employeeCode;
                cmd_GetEmployee.Parameters["EmployeeTypeId"].Value = (object)employeeType;

                adapter.SelectCommand = cmd_GetEmployee;
                bError = false;
                nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_ELQDETAILS_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();

                    returnDS.Merge(errDS);
                    returnDS.Merge(messageDS);
                    return returnDS;
                }
                leaveQuotaDS.AcceptChanges();
                #endregion               
            }
            #endregion

            #region Saving ELQ Details Data...
            if (leaveTypeID == 0) //0=All
            {
                if (Convert.ToBoolean(StringDS.DataStrings[2].StringValue)) cmd.CommandText = "PRO_LMS_LEAVE_ALLOCATION_ROUND";
                else cmd.CommandText = "PRO_LMS_LEAVE_ALLOCATION";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                foreach (lmsLeaveQuotaDS.ELQDetailsRow row in leaveQuotaDS.ELQDetails.Rows)
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_JoiningDate", row.JoiningDate);
                    cmd.Parameters.AddWithValue("p_ProcessDate", processDate);
                    cmd.Parameters.AddWithValue("p_Gender", row.Gender);
                    cmd.Parameters.AddWithValue("p_Religion", row.Religion);
                    cmd.Parameters.AddWithValue("p_ActivitiesID", activitiesID);
                    cmd.Parameters.AddWithValue("p_LeaveCategoryID", row.LeaveCategoryID);
                    cmd.Parameters.AddWithValue("p_Prev_ELQID", prev_elqID);
                    cmd.Parameters.AddWithValue("p_ELQID", elqID);
                    cmd.Parameters.AddWithValue("p_CurrentUserId", row.UserID);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError) messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode);
                    cmd.Parameters.Clear();
                }
            }
            else
            {
                if (Convert.ToBoolean(StringDS.DataStrings[2].StringValue)) cmd.CommandText = "PRO_LMS_LEAVE_ALLOCATION_LTRND";
                else cmd.CommandText = "PRO_LMS_LEAVE_ALLOCATION_BY_LT";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                foreach (lmsLeaveQuotaDS.ELQDetailsRow row in leaveQuotaDS.ELQDetails.Rows)
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_JoiningDate", row.JoiningDate);
                    cmd.Parameters.AddWithValue("p_ProcessDate", processDate);
                    cmd.Parameters.AddWithValue("p_LeaveTypeID", leaveTypeID);
                    cmd.Parameters.AddWithValue("p_ActivitiesID", activitiesID);
                    cmd.Parameters.AddWithValue("p_LeaveCategoryID", row.LeaveCategoryID);
                    cmd.Parameters.AddWithValue("p_Prev_ELQID", prev_elqID);
                    cmd.Parameters.AddWithValue("p_ELQID", elqID);
                    cmd.Parameters.AddWithValue("p_CurrentUserId", row.UserID);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError) messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode);
                    cmd.Parameters.Clear();
                }
            }                        

            #endregion


            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;

        }
        #endregion

        private DataSet _updateELQDetails(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
            DataBoolDS boolDS = new DataBoolDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            leaveQuotaDS.Merge(inputDS.Tables[leaveQuotaDS.ELQDetails.TableName], false, MissingSchemaAction.Error);
            leaveQuotaDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            foreach (lmsLeaveQuotaDS.ELQDetailsRow row in leaveQuotaDS.ELQDetails.Rows)
            {
                cmd.Parameters.AddWithValue("p_ELQDetailsID", row.ELQDetailsID);
                cmd.Parameters.AddWithValue("p_ChangedDaysAllotted", row.ChangedDaysAllotted);

                if (boolDS.DataBools[0].BoolValue) //ModifyLeaveQuota...
                {
                    cmd.Parameters.AddWithValue("p_ChangedDaysImmature", row.ChangedDaysImmature);
                    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                    cmd.Parameters.AddWithValue("p_dayscarry", row.DaysCarry);
                    cmd.Parameters.AddWithValue("p_daysencashmentcarry", row.DaysEncashmentCarry);
                    cmd.Parameters.AddWithValue("p_daysconslapses", row.DAYSCONSLAPSES);
                    cmd.CommandText = "PRO_LMS_LEAVEQUOTA_UPDATE";
                }
                else //LeaveCounter...
                {
                    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                    cmd.CommandText = "PRO_LMS_LQUOTA_UPDATE_LCOUNTER";
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_ELQDETAILS_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getLeaveQuotaList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string status = StringDS.DataStrings[0].StringValue;
            if (status.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveQuotaList");
            }
            else //Current Fiscal Year's Data
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsCurrYearLeaveQuotaList");
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 24-Dec-11)...
            #region Old...
            //lmsLeaveQuotaPO leaveQuotaPO = new lmsLeaveQuotaPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaPO, leaveQuotaPO.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}
            //leaveQuotaPO.AcceptChanges();

            //lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            //#region Populate ELQDetails Table
            //if (leaveQuotaPO.ELQDetails.Count > 0)
            //{
            //    foreach (lmsLeaveQuotaPO.ELQDetailsRow poRow in leaveQuotaPO.ELQDetails.Rows)
            //    {
            //        lmsLeaveQuotaDS.ELQDetailsRow row = leaveQuotaDS.ELQDetails.NewELQDetailsRow();

            //        if (poRow.IsELQDetailsIDNull() == false)
            //        {
            //            row.ELQDetailsID = poRow.ELQDetailsID;
            //        }
            //        if (poRow.IsELQIDNull() == false)
            //        {
            //            row.ELQID = poRow.ELQID;
            //        }
            //        if (poRow.IsLCDetailsIDNull() == false)
            //        {
            //            row.LCDetailsID = poRow.LCDetailsID;
            //        }
            //        if (poRow.IsLeaveTypeIDNull() == false)
            //        {
            //            row.LeaveTypeID = poRow.LeaveTypeID;
            //        }                   
            //        if (poRow.IsEmployeeIDNull() == false)
            //        {
            //            row.EmployeeID = poRow.EmployeeID;
            //        }                    
            //        if (poRow.IsCalculatedDaysAllottedNull() == false)
            //        {
            //            row.CalculatedDaysAllotted = poRow.CalculatedDaysAllotted;
            //        }
            //        if (poRow.IsLeaveTakenNull() == false)
            //        {
            //            row.LeaveTaken = poRow.LeaveTaken;
            //        }
            //        if (poRow.IsDaysCarryNull() == false)
            //        {
            //            row.DaysCarry = poRow.DaysCarry;
            //        }
            //        if (poRow.IsDaysEncashmentCarryNull() == false)
            //        {
            //            row.DaysEncashmentCarry = poRow.DaysEncashmentCarry;
            //        }
            //        if (poRow.IsChangedDaysAllottedNull() == false)
            //        {
            //            row.ChangedDaysAllotted = poRow.ChangedDaysAllotted;
            //        }
            //        if (poRow.IsEffectFromMonthYearNull() == false)
            //        {
            //            row.EffectFromMonthYear = poRow.EffectFromMonthYear;
            //        }
            //        if (poRow.IsEffectToMonthYearNull() == false)
            //        {
            //            row.EffectToMonthYear = poRow.EffectToMonthYear;
            //        }
            //        if (poRow.IsLeaveTypeNameNull() == false)
            //        {
            //            row.LeaveTypeName = poRow.LeaveTypeName;
            //        }
            //        if (poRow.IsEmployeeNameNull() == false)
            //        {
            //            row.EmployeeName = poRow.EmployeeName;
            //        }
            //        if (poRow.IsLeaveCategoryNameNull() == false)
            //        {
            //            row.LeaveCategoryName = poRow.LeaveCategoryName;
            //        }
            //        if (poRow.IsEmployeeCodeNull() == false)
            //        {
            //            row.EmployeeCode = poRow.EmployeeCode;
            //        }
            //        if (poRow.IsFiscalYearNull() == false)
            //        {
            //            row.FiscalYear = poRow.FiscalYear;
            //        }
            //        if (poRow.IsLeaveScheduledNull() == false)
            //        {
            //            row.LeaveScheduled = poRow.LeaveScheduled;
            //        }
            //        if (poRow.IsLeaveRemainNull() == false)
            //        {
            //            row.LeaveRemain = poRow.LeaveRemain;
            //        }

            //        //Kaysar, 09-Feb-2011
            //        if (poRow.IsGradeCodeNull() == false)
            //        {
            //            row.GradeCode = poRow.GradeCode;
            //        }
            //        //

            //        //Kaysar, 24-Apr-2011
            //        if (poRow.IsCalculatedDaysImmatureNull() == false)
            //        {
            //            row.CalculatedDaysImmature = poRow.CalculatedDaysImmature;
            //        }
            //        if (poRow.IsChangedDaysImmatureNull() == false)
            //        {
            //            row.ChangedDaysImmature = poRow.ChangedDaysImmature;
            //        }
            //        if (poRow.IsCalculatedEffecteDDateNull() == false)
            //        {
            //            row.CalculatedEffecteDDate = poRow.CalculatedEffecteDDate;
            //            row.CalculatedEffecteDDateString = poRow.CalculatedEffecteDDate.ToString();
            //        }
            //        else
            //        {
            //            row.CalculatedEffecteDDateString = "";
            //        }

            //        if (poRow.IsChangedEffectedDateNull() == false)
            //        {
            //            row.ChangedEffectedDate = poRow.ChangedEffectedDate;
            //            row.ChangedEffectedDateString = poRow.ChangedEffectedDate.ToString();
            //        }
            //        else
            //        {
            //            row.ChangedEffectedDateString = "";
            //        }
            //        //

            //        //Kaysar, 21-Nov-2011
            //        if (poRow.IsCalculatedDaysCarryNull() == false)
            //        {
            //            row.CalculatedDaysCarry = poRow.CalculatedDaysCarry;
            //        }
            //        if (poRow.IsCalculatedEncashmentCarryNull() == false)
            //        {
            //            row.CalculatedEncashmentCarry = poRow.CalculatedEncashmentCarry;
            //        }
            //        //

            //        leaveQuotaDS.ELQDetails.AddELQDetailsRow(row);
            //        leaveQuotaDS.AcceptChanges();
            //    }
            //}
            //#endregion

            #endregion

            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveQuotaDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            try
            {
                returnDS.Merge(leaveQuotaDS);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();

                return returnDS;
            }
            catch (Exception EX)
            {
                return returnDS;

            }
        }

        private DataSet _getLmsEmployeeHistory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            //raqib 03 nov
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLmsEmployeeHistory");
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLmsEmployeeHistoryApplyFor");
            ////////////


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            //lmsEmployeeHistoryPO lmsEmpHistPO = new lmsEmployeeHistoryPO();
            lmsEmployeeHistoryDS lmsEmpHistDS = new lmsEmployeeHistoryDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, lmsEmpHistDS, lmsEmpHistDS.Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HISTORY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            //lmsEmpHistPO.AcceptChanges();
            lmsEmpHistDS.AcceptChanges();

            //lmsEmployeeHistoryDS lmsEmpHistDS = new lmsEmployeeHistoryDS();

            #region Populate LmsEmployeeHistory Table
            //if (lmsEmpHistPO.Employee.Count > 0)
            //{
            //    foreach (lmsEmployeeHistoryPO.EmployeeRow poRow in lmsEmpHistPO.Employee.Rows)
            //    {
            //        lmsEmployeeHistoryDS.EmployeeRow row = lmsEmpHistDS.Employee.NewEmployeeRow();

            //        if (poRow.IsEmployeeIDNull() == false)
            //        {
            //            row.EmployeeID = poRow.EmployeeID;
            //        }
            //        if (poRow.IsEmployeeCodeNull() == false)
            //        {
            //            row.EmployeeCode = poRow.EmployeeCode;
            //        }
            //        if (poRow.IsEmployeeNameNull() == false)
            //        {
            //            row.EmployeeName = poRow.EmployeeName;
            //        }
            //        if (poRow.IsJoiningDateNull() == false)
            //        {
            //            row.JoiningDate = poRow.JoiningDate;
            //        }
            //        if (poRow.IsConfirmDateNull() == false)
            //        {
            //            row.ConfirmDate = poRow.ConfirmDate;
            //        }
            //        if (poRow.IsGradeIDNull() == false)
            //        {
            //            row.GradeID = poRow.GradeID;
            //        }
            //        if (poRow.IsLeaveCategoryIDNull() == false)
            //        {
            //            row.LeaveCategoryID = poRow.LeaveCategoryID;
            //        }
            //        if (poRow.IsLeaveApprovalAuthorityNull() == false)
            //        {
            //            row.LeaveApprovalAuthority = poRow.LeaveApprovalAuthority;
            //        }
            //        if (poRow.IsLoginIDNull() == false)
            //        {
            //            row.LoginID = poRow.LoginID;
            //        }
            //        if (poRow.IsDelegatedUserIDNull() == false)
            //        {
            //            row.DelegatedUserID = poRow.DelegatedUserID;
            //        }
            //        if (poRow.IsUserIDNull() == false)
            //        {
            //            row.UserID = poRow.UserID;
            //        }
            //        if (poRow.IsEmailNull() == false)
            //        {
            //            row.Email = poRow.Email;
            //        }
            //        if (poRow.IsSupervisorIDNull() == false)
            //        {
            //            row.SupervisorID = poRow.SupervisorID;
            //        }
            //        if (poRow.IsSupervisorEmailNull() == false)
            //        {
            //            row.SupervisorEmail = poRow.SupervisorEmail;
            //        }
            //        if (poRow.IsTotalDeptEmpNull() == false)
            //        {
            //            row.TotalDeptEmp = poRow.TotalDeptEmp;
            //        }
            //        if (poRow.IsDelegatedEmployeeIDNull() == false)
            //        {
            //            row.DelegatedEmployeeID = poRow.DelegatedEmployeeID;
            //        }
            //        if (poRow.IsDelegatedUserEmailNull() == false)
            //        {
            //            row.DelegatedUserEmail = poRow.DelegatedUserEmail;
            //        }
            //        if (poRow.IsEmailPasswordNull() == false)
            //        {
            //            row.EmailPassword = poRow.EmailPassword;
            //        }

            //        lmsEmpHistDS.Employee.AddEmployeeRow(row);
            //        lmsEmpHistDS.AcceptChanges();
            //    }
            //}
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(lmsEmpHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveQuotaListByLoginID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string status = StringDS.DataStrings[0].StringValue;
            if (status.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveQuotaListByLoginID");
            }
            else //Current Fiscal Year's Data
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsCurrYearLeaveQuotaListByLoginID");
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["LoginID"].Value = StringDS.DataStrings[1].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 24-Dec-11)...
            #region Old...
            //lmsLeaveQuotaPO leaveQuotaPO = new lmsLeaveQuotaPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaPO, leaveQuotaPO.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST_BY_LOGINID.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}
            //leaveQuotaPO.AcceptChanges();

            //lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            //#region Populate ELQDetails Table
            //if (leaveQuotaPO.ELQDetails.Count > 0)
            //{
            //    foreach (lmsLeaveQuotaPO.ELQDetailsRow poRow in leaveQuotaPO.ELQDetails.Rows)
            //    {
            //        lmsLeaveQuotaDS.ELQDetailsRow row = leaveQuotaDS.ELQDetails.NewELQDetailsRow();

            //        if (poRow.IsELQDetailsIDNull() == false)
            //        {
            //            row.ELQDetailsID = poRow.ELQDetailsID;
            //        }
            //        if (poRow.IsELQIDNull() == false)
            //        {
            //            row.ELQID = poRow.ELQID;
            //        }
            //        if (poRow.IsLCDetailsIDNull() == false)
            //        {
            //            row.LCDetailsID = poRow.LCDetailsID;
            //        }
            //        if (poRow.IsLeaveTypeIDNull() == false)
            //        {
            //            row.LeaveTypeID = poRow.LeaveTypeID;
            //        }
            //        if (poRow.IsEmployeeIDNull() == false)
            //        {
            //            row.EmployeeID = poRow.EmployeeID;
            //        }
            //        if (poRow.IsCalculatedDaysAllottedNull() == false)
            //        {
            //            row.CalculatedDaysAllotted = poRow.CalculatedDaysAllotted;
            //        }
            //        if (poRow.IsLeaveTakenNull() == false)
            //        {
            //            row.LeaveTaken = poRow.LeaveTaken;
            //        }
            //        if (poRow.IsDaysCarryNull() == false)
            //        {
            //            row.DaysCarry = poRow.DaysCarry;
            //        }
            //        if (poRow.IsDaysEncashmentCarryNull() == false)
            //        {
            //            row.DaysEncashmentCarry = poRow.DaysEncashmentCarry;
            //        }
            //        if (poRow.IsChangedDaysAllottedNull() == false)
            //        {
            //            row.ChangedDaysAllotted = poRow.ChangedDaysAllotted;
            //        }
            //        if (poRow.IsEffectFromMonthYearNull() == false)
            //        {
            //            row.EffectFromMonthYear = poRow.EffectFromMonthYear;
            //        }
            //        if (poRow.IsEffectToMonthYearNull() == false)
            //        {
            //            row.EffectToMonthYear = poRow.EffectToMonthYear;
            //        }
            //        if (poRow.IsLeaveTypeNameNull() == false)
            //        {
            //            row.LeaveTypeName = poRow.LeaveTypeName;
            //        }
            //        if (poRow.IsEmployeeNameNull() == false)
            //        {
            //            row.EmployeeName = poRow.EmployeeName;
            //        }
            //        if (poRow.IsLeaveCategoryNameNull() == false)
            //        {
            //            row.LeaveCategoryName = poRow.LeaveCategoryName;
            //        }
            //        if (poRow.IsEmployeeCodeNull() == false)
            //        {
            //            row.EmployeeCode = poRow.EmployeeCode;
            //        }
            //        if (poRow.IsFiscalYearNull() == false)
            //        {
            //            row.FiscalYear = poRow.FiscalYear;
            //        }
            //        if (poRow.IsLeaveScheduledNull() == false)
            //        {
            //            row.LeaveScheduled = poRow.LeaveScheduled;
            //        }
            //        if (poRow.IsLeaveRemainNull() == false)
            //        {
            //            row.LeaveRemain = poRow.LeaveRemain;
            //        }

            //        //Kaysar, 09-Feb-2011
            //        if (poRow.IsGradeCodeNull() == false)
            //        {
            //            row.GradeCode = poRow.GradeCode;
            //        }
            //        //

            //        //Kaysar, 24-Apr-2011
            //        if (poRow.IsCalculatedDaysImmatureNull() == false)
            //        {
            //            row.CalculatedDaysImmature = poRow.CalculatedDaysImmature;
            //        }
            //        if (poRow.IsChangedDaysImmatureNull() == false)
            //        {
            //            row.ChangedDaysImmature = poRow.ChangedDaysImmature;
            //        }
            //        if (poRow.IsCalculatedEffecteDDateNull() == false)
            //        {
            //            row.CalculatedEffecteDDate = poRow.CalculatedEffecteDDate;
            //            row.CalculatedEffecteDDateString = poRow.CalculatedEffecteDDate.ToString();
            //        }
            //        else
            //        {
            //            row.CalculatedEffecteDDateString = "";
            //        }

            //        if (poRow.IsChangedEffectedDateNull() == false)
            //        {
            //            row.ChangedEffectedDate = poRow.ChangedEffectedDate;
            //            row.ChangedEffectedDateString = poRow.ChangedEffectedDate.ToString();
            //        }
            //        else
            //        {
            //            row.ChangedEffectedDateString = "";
            //        }
            //        //

            //        leaveQuotaDS.ELQDetails.AddELQDetailsRow(row);
            //        leaveQuotaDS.AcceptChanges();
            //    }
            //}
            //#endregion
            #endregion

            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST_BY_LOGINID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveQuotaDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveQuotaDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveQuotaHistory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveQuotaHistory");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["ActivitiesID_A"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ActivitiesID_R"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
            cmd.Parameters["ELQID"].Value = Convert.ToInt32(StringDS.DataStrings[0].StringValue);
            cmd.Parameters["LoginID"].Value = StringDS.DataStrings[1].StringValue;
            cmd.Parameters["LoginID1"].Value = StringDS.DataStrings[1].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 22-April-12)...
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_HISTORY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveQuotaDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveQuotaDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveQuotaListName(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveQuotaListName");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.EmployeeLeaveQuota.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_LIST_NAME.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveQuotaDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveQuotaDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveDateList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            Int32 employeeID = 0, leaveTypeID = 0;
            DateTime dateFrom, dateTo;

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            employeeID = Convert.ToInt32(StringDS.DataStrings[0].StringValue);
            leaveTypeID = Convert.ToInt32(StringDS.DataStrings[1].StringValue);
            dateFrom = Convert.ToDateTime(StringDS.DataStrings[2].StringValue);
            dateTo = Convert.ToDateTime(StringDS.DataStrings[3].StringValue);

            OleDbCommand cmd = new OleDbCommand();
            if (leaveTypeID == -1)
                cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveDateListAll");
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveDateList");
                cmd.Parameters["LeaveTypeID"].Value = leaveTypeID;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeID"].Value = employeeID;
            cmd.Parameters["DateFrom"].Value = dateFrom.Date;
            cmd.Parameters["DateTo"].Value = dateTo.Date;
            cmd.Parameters["ActivitiesID"].Value = Convert.ToInt32(LeaveActivities.Approve);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 22-April-12)...
            lmsLeaveDS leaveDS = new lmsLeaveDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_DATE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        #region Update by Jarif, 16.Jan.14...
        #region Old...
        //private DataSet _deleteELQDetails(DataSet inputDS)
        //{

        //    DataStringDS stringDS = new DataStringDS();
        //    stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        //    stringDS.AcceptChanges();


        //    string strSpecificEmployee = stringDS.DataStrings[0].StringValue;
        //    int a = 0;



        //    ErrorDS errDS = new ErrorDS();
        //    DBConnectionDS connDS = new DBConnectionDS();

        //    OleDbCommand cmd = new OleDbCommand();

        //    //if (strSpecificEmployee == "one Employee")
        //    //{
        //    cmd.CommandText = "PRO_LMS_ELQDETAILS_DEALLOCATE";


        //    //}
        //    //else if (strSpecificEmployee == "All")
        //    //{
        //    //cmd.CommandText = "PRO_LMS_ELQDETAILS_DEALLO_ALL";
        //    //}
        //    cmd.CommandType = CommandType.StoredProcedure;

        //    if (cmd == null)
        //    {
        //        return UtilDL.GetCommandNotFound();
        //    }

        //    lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
        //    leaveQuotaDS.Merge(inputDS.Tables[leaveQuotaDS.ELQDetails.TableName], false, MissingSchemaAction.Error);
        //    leaveQuotaDS.AcceptChanges();

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //    connDS.AcceptChanges();

        //    foreach (lmsLeaveQuotaDS.ELQDetailsRow row in leaveQuotaDS.ELQDetails.Rows)
        //    {
        //        if (row.IsELQIDNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_ELQID", row.ELQID);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_ELQID", DBNull.Value);
        //        }
        //        a = row.ELQID;
        //        if (row.IsEmployeeIDNull() == false)
        //        {
        //            cmd.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);
        //        }
        //        else
        //        {
        //            cmd.Parameters.AddWithValue("p_EMPLOYEEID", DBNull.Value);
        //        }


        //        bool bError = false;
        //        int nRowAffected = -1;
        //        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

        //        if (bError)
        //        {
        //            ErrorDS.Error err = errDS.Errors.NewError();
        //            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        //            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //            err.ErrorInfo1 = ActionID.ACTION_LMS_ELQDETAILS_ADD.ToString();//
        //            errDS.Errors.AddError(err);
        //            errDS.AcceptChanges();
        //            return errDS;
        //        }






        //    }
        //    //if (strSpecificEmployee == "one Employee")
        //    //{
        //    //int connID = ADOController.Instance.OpenNewConnection();
        //    //if (connID == -1)
        //    //{
        //    //    return null;
        //    //}
        //    //    bool bError = false;
        //    //    OleDbCommand cmd2 = new OleDbCommand();
        //    //    cmd2 = DBCommandProvider.GetDBCommand("GetLeaveQuota");
        //    //    cmd2.Parameters["p_ELQID"].Value = a;
        //    //    OleDbDataAdapter adapter = new OleDbDataAdapter();
        //    //    adapter.SelectCommand = cmd2;
        //    //  int  nRowAffected2 = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        //    //  ADOController.Instance.CommitTransaction();
        //    //  ADOController.Instance.CloseConnection();
        //    //}
        //    errDS.Clear();
        //    errDS.AcceptChanges();
        //    return errDS;
        //}
        #endregion

        private DataSet _deleteELQDetails(DataSet inputDS)
        {
            #region Local Variables Declaration...
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Delete_ELQ = new OleDbCommand();
            OleDbCommand cmd_GetEmployee = new OleDbCommand();
            OleDbCommand cmd_ELQDetails = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            MessageDS messageDS = new MessageDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            bool bError = false;
            int nRowAffected = -1;

            string employeeCode = "", branchCode = "";
            Int32 leaveTypeID = 0, employeeType = 0, elqID = 0;
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Data Extracting...
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();
            
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            try
            {
                employeeCode = StringDS.DataStrings[0].StringValue;
                branchCode = StringDS.DataStrings[1].StringValue;

                leaveTypeID = integerDS.DataIntegers[0].IntegerValue;
                employeeType = integerDS.DataIntegers[1].IntegerValue;
                elqID = integerDS.DataIntegers[2].IntegerValue;
            }
            catch
            {
            }

            #endregion

            #region Get employee information from DB...
            if (employeeCode == "All Employees")
            {
                #region Get Employee Information From DB...
                #region Selection for XML...
                if (employeeType != -1 && branchCode != "0")
                {
                    cmd_GetEmployee = DBCommandProvider.GetDBCommand("GetEmpInfo_LA_BySiteWithEmpType");
                    cmd_GetEmployee.Parameters["SiteCode"].Value = (object)branchCode;
                    cmd_GetEmployee.Parameters["EmployeeType"].Value = (object)employeeType;
                }
                else if (employeeType != -1)
                {
                    cmd_GetEmployee = DBCommandProvider.GetDBCommand("GetEmpInfo_LeaveAllocation_ByEmpType");
                    cmd_GetEmployee.Parameters["EmployeeType"].Value = (object)employeeType;
                }
                else if (branchCode != "0")
                {
                    cmd_GetEmployee = DBCommandProvider.GetDBCommand("GetEmpInfo_LeaveAllocation_BySite");
                    cmd_GetEmployee.Parameters["SiteCode"].Value = (object)branchCode;
                }
                else // All Employee
                {
                    cmd_GetEmployee = DBCommandProvider.GetDBCommand("GetEmpInfo_LeaveAllocation_All");
                }

                #endregion

                adapter.SelectCommand = cmd_GetEmployee;
                bError = false;
                nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DEALLOCATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();

                    returnDS.Merge(errDS);
                    returnDS.Merge(messageDS);
                    return returnDS;
                }
                leaveQuotaDS.AcceptChanges();
                #endregion
            }
            else
            {
                #region Get Employee Information From DB...
                cmd_GetEmployee = DBCommandProvider.GetDBCommand("GetEmpInfo_LeaveAllocation_ByEmp");
                cmd_GetEmployee.Parameters["EmployeeCode"].Value = (object)employeeCode;
                cmd_GetEmployee.Parameters["EmployeeTypeId"].Value = (object)employeeType;


                adapter.SelectCommand = cmd_GetEmployee;
                bError = false;
                nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DEALLOCATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();

                    returnDS.Merge(errDS);
                    returnDS.Merge(messageDS);
                    return returnDS;
                }
                leaveQuotaDS.AcceptChanges();
                #endregion
            }
            #endregion

            #region Deleting ELQ Details Data...
            if (leaveTypeID == 0) //0=All
            {
                cmd.CommandText = "PRO_LMS_ELQDETAILS_DEALLOCATE";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                foreach (lmsLeaveQuotaDS.ELQDetailsRow row in leaveQuotaDS.ELQDetails.Rows)
                {
                    cmd.Parameters.AddWithValue("p_ELQID", elqID);
                    cmd.Parameters.AddWithValue("p_EMPLOYEEID", row.EmployeeID);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError) messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode);
                    cmd.Parameters.Clear();
                }
            }
            else
            {
                cmd.CommandText = "PRO_LMS_ELQDETAILS_DA_LT";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                foreach (lmsLeaveQuotaDS.ELQDetailsRow row in leaveQuotaDS.ELQDetails.Rows)
                {
                    cmd.Parameters.AddWithValue("p_ELQID", elqID);
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_LeaveTypeID", leaveTypeID);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError) messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode);
                    cmd.Parameters.Clear();
                }
            }
            #endregion

            #region Checking ELQ Detalis Data And Deleting Employee Leave Quota...
            adapter.Dispose();
            leaveQuotaDS = new lmsLeaveQuotaDS();

            cmd_ELQDetails = DBCommandProvider.GetDBCommand("GetELQDetailsByELQID");
            cmd_ELQDetails.Parameters["ELQID"].Value = (object)elqID;

            adapter.SelectCommand = cmd_ELQDetails;
            bError = false;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            leaveQuotaDS.AcceptChanges();

            if (leaveQuotaDS.ELQDetails.Rows.Count == 0)
            {
                #region Deleting Employee Leave Quota Data...
                cmd_Delete_ELQ = DBCommandProvider.GetDBCommand("DeleteELQDetails");

                if (cmd_Delete_ELQ == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd_Delete_ELQ.Parameters.AddWithValue("p_ELQID", elqID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Delete_ELQ, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Delete_ELQ.Parameters.Clear();
                #endregion
            }

            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        #endregion

        private DataSet _deleteELQdetailsEmployee(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            // DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();

            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
            leaveQuotaDS.Merge(inputDS.Tables[leaveQuotaDS.ELQDetails.TableName], false, MissingSchemaAction.Error);
            leaveQuotaDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteELQDetails");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (lmsLeaveQuotaDS.ELQDetailsRow row in leaveQuotaDS.ELQDetails.Rows)
            {
                cmd.Parameters["p_ELQID"].Value = row.ELQID;
                break;
            }
            //    }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SHIFTING_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            //}

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getLeaveHistory(DataSet inputDS)
        {
            int connID = ADOController.Instance.OpenNewConnection();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getLeave");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveDS LeaveDS = new lmsLeaveDS();


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, LeaveDS, LeaveDS.Leave.TableName, connID, ref bError);

            //nRowAffected = ADOController.Instance.Fill(adapter, lmsEmpHistDS, lmsEmpHistDS.Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HISTORY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            //lmsEmpHistPO.AcceptChanges();
            LeaveDS.AcceptChanges();

            //lmsEmployeeHistoryDS lmsEmpHistDS = new lmsEmployeeHistoryDS();



            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(LeaveDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getMaxELQID(DataSet inputDS)
        {
            //int connID = ADOController.Instance.OpenNewConnection();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetMaxELQID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveQuotaDS leaveQuotaDS1 = new lmsLeaveQuotaDS();


            try
            {

                bool bError = false;
                int nRowAffected = -1;
                //nRowAffected = ADOController.Instance.Fill(adapter, LeaveDS, LeaveDS.Leave.TableName, connID, ref bError);
                nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS1, leaveQuotaDS1.MaxELQID.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                //nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS1, leaveQuotaDS1.ELQDetails.TableName, connID, ref bError);

                //nRowAffected = ADOController.Instance.Fill(adapter, lmsEmpHistDS, lmsEmpHistDS.Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                //if (bError)
                //{
                //    ErrorDS.Error err = errDS.Errors.NewError();
                //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //    err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HISTORY.ToString();
                //    errDS.Errors.AddError(err);
                //    errDS.AcceptChanges();
                //    return errDS;

                //}

                leaveQuotaDS1.AcceptChanges();

                // now create the packet
                errDS.Clear();
                errDS.AcceptChanges();

                DataSet returnDS = new DataSet();

                returnDS.Merge(leaveQuotaDS1);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();

                return returnDS;
            }
            catch
            {
                leaveQuotaDS1.AcceptChanges();

                // now create the packet
                errDS.Clear();
                errDS.AcceptChanges();

                DataSet returnDS = new DataSet();

                returnDS.Merge(leaveQuotaDS1);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();

                return returnDS;
            }

        }

        private DataSet _getLmsEmployeeHistoryByEmpID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLmsEmpHistByEmpID");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmployeeID1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmployeeID2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmployeeID3"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmployeeID4"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsEmployeeHistoryDS lmsEmpHistDS = new lmsEmployeeHistoryDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, lmsEmpHistDS, lmsEmpHistDS.Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HIST_BY_EMPID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            lmsEmpHistDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(lmsEmpHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLmsEmployeeHistoryByCode(DataSet inputDS)
        {
            bool bError = false;
            int nRowAffected = -1;

            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Get EmployeeID
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            cmd = DBCommandProvider.GetDBCommand("GetEmployeeId");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["Code"].Value = stringDS.DataStrings[0].StringValue;
            adapter.SelectCommand = cmd;

            EmployeeDS employeeDS = new EmployeeDS();

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, employeeDS, employeeDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            employeeDS.AcceptChanges();
            if (employeeDS.Employees.Rows.Count == 0) bError = false;

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HIST_BY_EMPCODE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            cmd.Dispose();
            #endregion

            cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLmsEmpHistByEmpID");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["EmployeeID"].Value = employeeDS.Employees[0].Employeeid;
            cmd.Parameters["EmployeeID1"].Value = employeeDS.Employees[0].Employeeid;
            cmd.Parameters["EmployeeID2"].Value = employeeDS.Employees[0].Employeeid;
            cmd.Parameters["EmployeeID3"].Value = employeeDS.Employees[0].Employeeid;
            cmd.Parameters["EmployeeID4"].Value = employeeDS.Employees[0].Employeeid;

            adapter.SelectCommand = cmd;
            lmsEmployeeHistoryDS lmsEmpHistDS = new lmsEmployeeHistoryDS();

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, lmsEmpHistDS, lmsEmpHistDS.Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HIST_BY_EMPCODE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            lmsEmpHistDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(lmsEmpHistDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        #endregion

        private DataSet _getLeaveQuotaByEmpID_CFYear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataIntegerDS IntegerDS = new DataIntegerDS();
            IntegerDS.Merge(inputDS.Tables[IntegerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            IntegerDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveQuotaByEmpID_CFYear");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["ActivitiesID_A"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ActivitiesID_R"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
            cmd.Parameters["ActivitiesID_A1"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ActivitiesID_R1"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR1"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA1"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
            cmd.Parameters["EmployeeID"].Value = IntegerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_BY_EMPID_CFYEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveQuotaDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            try
            {
                returnDS.Merge(leaveQuotaDS);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();

                return returnDS;
            }
            catch (Exception EX)
            {
                return returnDS;

            }
        }

        private DataSet _getPendingLeaveReqByEmpIDForTransProcess(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS IntegerDS = new DataStringDS();
            IntegerDS.Merge(inputDS.Tables[IntegerDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            IntegerDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveQuotaByEmpID_CFYear_For_Transfer_Process");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["ActivitiesID_A"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ActivitiesID_R"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
            cmd.Parameters["ActivitiesID_A1"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ActivitiesID_R1"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR1"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA1"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
            cmd.Parameters["EmployeeCode"].Value = IntegerDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_BY_EMPID_CFYEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveQuotaDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            try
            {
                returnDS.Merge(leaveQuotaDS);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();

                return returnDS;
            }
            catch (Exception EX)
            {
                return returnDS;

            }
        }

        #region Update ElqDetails Days :: WALI :: 24-Apr-2014
        private DataSet _updateELQDetailsDays(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_LEAVEQUOTA_UPDATE_DAYS";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            leaveQuotaDS.Merge(inputDS.Tables[leaveQuotaDS.ELQDetails.TableName], false, MissingSchemaAction.Error);
            leaveQuotaDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveQuotaDS.ELQDetailsRow row in leaveQuotaDS.ELQDetails.Rows)
            {
                cmd.Parameters.AddWithValue("p_ChangedDaysAllotted", row.ChangedDaysAllotted);
                cmd.Parameters.AddWithValue("p_DaysCarry", row.DaysCarry);
                cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                cmd.Parameters.AddWithValue("p_ELQDetailsID", row.ELQDetailsID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_ELQDETAILS_UPD_DAYS.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeName);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region LMS Employee Leave Quota :: WALI :: 03-Jun-2014
        private DataSet _getLeaveQuotaHistoryToModify(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS StringDS = new DataStringDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveQuotaHistoryToModify");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["LeaveTypeID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["LeaveTypeID2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["EmployeeCode"].Value = StringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = StringDS.DataStrings[0].StringValue;
            cmd.Parameters["FiscalYear"].Value = StringDS.DataStrings[1].StringValue;
            cmd.Parameters["FiscalYear2"].Value = StringDS.DataStrings[1].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_QUOTA_TO_MODIFY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveQuotaDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            try
            {
                returnDS.Merge(leaveQuotaDS);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();

                return returnDS;
            }
            catch (Exception EX)
            {
                return returnDS;

            }
        }
        private DataSet _getLmsFiscalYear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetLmsFiscalYear");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.EmployeeLeaveQuota.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LMS_FISCAL_YEAR.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveQuotaDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            try
            {
                returnDS.Merge(leaveQuotaDS);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();

                return returnDS;
            }
            catch (Exception EX)
            {
                return returnDS;
            }
        }
        #endregion

        private DataSet _getEmployeeList_ToAssignLeave(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_ToAssignLeave");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            string siteCode = stringDS.DataStrings[0].StringValue;
            string departmentCode = stringDS.DataStrings[1].StringValue;
            string designationCode = stringDS.DataStrings[2].StringValue;
            string gradeCode = stringDS.DataStrings[3].StringValue;
            int leaveTypeID = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            decimal noOfDays = Convert.ToDecimal(stringDS.DataStrings[6].StringValue);
            DateTime leaveFrom = Convert.ToDateTime(stringDS.DataStrings[7].StringValue, System.Globalization.CultureInfo.InvariantCulture);
            DateTime leaveTo = Convert.ToDateTime(stringDS.DataStrings[8].StringValue, System.Globalization.CultureInfo.InvariantCulture);
            int employeeTypeID = Convert.ToInt32(stringDS.DataStrings[9].StringValue);

            cmd.Parameters["FromDate1"].Value = cmd.Parameters["FromDate2"].Value = leaveFrom;
            cmd.Parameters["ActivitiesID_A1"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ActivitiesID_R1"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR1"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA1"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);

            cmd.Parameters["FromDate3"].Value = leaveFrom;
            cmd.Parameters["ToDate1"].Value = leaveTo;
            cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ActivitiesID_R2"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR2"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA2"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);

            cmd.Parameters["FromDate4"].Value = leaveFrom;
            cmd.Parameters["ToDate2"].Value = leaveTo;

            cmd.Parameters["LeaveTypeID"].Value = leaveTypeID;
            //cmd.Parameters["NoOfDays"].Value = noOfDays;  // Not filtered by LeaveDays, just checked '!=0'. Because noOfDays may vary from Branch to Branch [Weekend]
            //cmd.Parameters["FromDate4"].Value = leaveFrom; // Removed 0 days filter because HOLIDAY ASSIGN doesn't require any leave balance :: WALI :: 14-Oct-2015
            cmd.Parameters["ToDate3"].Value = leaveTo;

            cmd.Parameters["SiteCode1"].Value = cmd.Parameters["SiteCode2"].Value = siteCode;
            cmd.Parameters["DepartmentCode1"].Value = cmd.Parameters["DepartmentCode2"].Value = departmentCode;
            cmd.Parameters["DesignationCode1"].Value = cmd.Parameters["DesignationCode2"].Value = designationCode;
            cmd.Parameters["GradeCode1"].Value = cmd.Parameters["GradeCode2"].Value = gradeCode;
            cmd.Parameters["EmployeeType1"].Value = cmd.Parameters["EmployeeType2"].Value = employeeTypeID;

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST_TO_ASSIGN_LEAVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            leaveQuotaDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            try
            {
                returnDS.Merge(leaveQuotaDS);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();

                return returnDS;
            }
            catch (Exception EX)
            {
                return returnDS;
            }
        }

        private DataSet _getEmployeeList_ForLeaveAllocatedWithoutPolicy(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[6].StringValue == "GradePos") cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_LeaveAllocatedWithoutPolicy_SortByDes");
            else cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_LeaveAllocatedWithoutPolicy_SortByCode");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            long siteID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[2].StringValue);

            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue.ToUpper() + "%";
            cmd.Parameters["SiteID1"].Value = siteID;
            cmd.Parameters["SiteID2"].Value = siteID;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["EmployeeStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            cmd.Parameters["EmployeeStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);

            cmd.Parameters["FunctionCode1"].Value = cmd.Parameters["FunctionCode2"].Value = stringDS.DataStrings[7].StringValue;
            cmd.Parameters["LiabilityCode1"].Value = cmd.Parameters["LiabilityCode2"].Value = stringDS.DataStrings[8].StringValue;
            cmd.Parameters["CompanyDivisionCode1"].Value = cmd.Parameters["CompanyDivisionCode2"].Value = stringDS.DataStrings[9].StringValue;
            cmd.Parameters["GradeCode1"].Value = cmd.Parameters["GradeCode2"].Value = stringDS.DataStrings[10].StringValue;
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            lmsLeaveQuotaDS lvDS = new lmsLeaveQuotaDS();
            nRowAffected = ADOController.Instance.Fill(adapter, lvDS, lvDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST_FOR_LEAVE_ALLOCATED_WITHOUTPOLICY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            lvDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(lvDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _EmpLeaveAllocatedWithoutPolicy(DataSet inputDS)
        {
            bool bError = false;
            int nRowAffected = -1;
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            lmsLeaveQuotaDS lqDS = new lmsLeaveQuotaDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            lqDS.Merge(inputDS.Tables[lqDS.ELQDetails.TableName], false, MissingSchemaAction.Error);
            lqDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LMS_ALLOCATE_NOPOLICY";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (lmsLeaveQuotaDS.ELQDetailsRow row in lqDS.ELQDetails.Rows)
            {
                long elqDetailsId = IDGenerator.GetNextGenericPK();
                if (elqDetailsId == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                if (row.IsEmployeeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);
                }
                if (row.IsProcessDateNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ProcessDate", row.ProcessDate);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ProcessDate", DBNull.Value);
                }
                if (row.IsLeaveTypeIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeID", row.LeaveTypeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LeaveTypeID", DBNull.Value);
                }
                if (row.IsELQIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ELQID", row.ELQID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ELQID", DBNull.Value);
                }
                cmd.Parameters.AddWithValue("p_ELQDetailsId", elqDetailsId);
                if (row.IsChangedDaysAllottedNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ChangedDaysAllotted", row.ChangedDaysAllotted);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ChangedDaysAllotted", DBNull.Value);
                }                
                
                if (row.IsLCDetailsIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LCDetailsId", row.LCDetailsID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LCDetailsId", DBNull.Value);
                }

                if (row.IsInsertByNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_InsertBy", row.InsertBy);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_InsertBy", DBNull.Value);
                }
                if (row.IsWithoutPolicyNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_WithoutPolicy", row.WithoutPolicy);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_WithoutPolicy", DBNull.Value);
                }

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LEAVE_ALLOCATED_WITHOUTPOLICY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow( row.EmpNameWithCode );
                else messageDS.ErrorMsg.AddErrorMsgRow( row.EmpNameWithCode );
                messageDS.AcceptChanges();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            errDS.Merge(messageDS);
            return errDS;
        }

        private DataSet _getAllLeaveQuotaHistory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAllLmsLeaveQuotaHistory");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["ActivitiesID_A"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ActivitiesID_R"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
            cmd.Parameters["ELQID"].Value = Convert.ToInt32(StringDS.DataStrings[0].StringValue);
            cmd.Parameters["EmployeeCode"].Value = StringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = StringDS.DataStrings[1].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Data Load (Jarif 22-April-12)...
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_ALL_LEAVE_QUOTA_HISTORY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveQuotaDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(leaveQuotaDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getLeaveQuotaByEmpID_CFYear_PrevYear(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataIntegerDS IntegerDS = new DataIntegerDS();
            IntegerDS.Merge(inputDS.Tables[IntegerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            IntegerDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetLmsLeaveQuotaByEmpID_CFYear_PrevYear");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["ActivitiesID_A"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ActivitiesID_R"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
            cmd.Parameters["ActivitiesID_A1"].Value = Convert.ToInt32(LeaveActivities.Approve);
            cmd.Parameters["ActivitiesID_R1"].Value = Convert.ToInt32(LeaveActivities.Recommend);
            cmd.Parameters["ActivitiesID_NR1"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
            cmd.Parameters["ActivitiesID_PA1"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
            cmd.Parameters["EmployeeID"].Value = cmd.Parameters["EmployeeID2"].Value = cmd.Parameters["EmployeeID3"].Value = IntegerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_REQ_STS_FOR_EMP_TRANSFER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            leaveQuotaDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            try
            {
                returnDS.Merge(leaveQuotaDS);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();

                return returnDS;
            }
            catch (Exception EX)
            {
                return returnDS;

            }
        }

        private DataSet _getEmployee_ToAssignLeaveBulk(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            lmsLeaveQuotaDS leaveQuotaDS = new lmsLeaveQuotaDS();
            DataStringDS stringDS = new DataStringDS();
            lmsLeaveDS leaveDS = new lmsLeaveDS();

            leaveDS.Merge(inputDS.Tables[leaveDS.ModifyLeave.TableName], false, MissingSchemaAction.Error);
            leaveDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (lmsLeaveDS.ModifyLeaveRow row in leaveDS.ModifyLeave.Rows)
            {
                cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_ToAssignLeaveBulk");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["FromDate1"].Value = cmd.Parameters["FromDate2"].Value = row.LeaveFrom;
                cmd.Parameters["ActivitiesID_A1"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["ActivitiesID_R1"].Value = Convert.ToInt32(LeaveActivities.Recommend);
                cmd.Parameters["ActivitiesID_NR1"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
                cmd.Parameters["ActivitiesID_PA1"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
                cmd.Parameters["FromDate3"].Value = row.LeaveFrom;
                cmd.Parameters["ToDate1"].Value = row.LeaveTo;
                cmd.Parameters["ActivitiesID_A2"].Value = Convert.ToInt32(LeaveActivities.Approve);
                cmd.Parameters["ActivitiesID_R2"].Value = Convert.ToInt32(LeaveActivities.Recommend);
                cmd.Parameters["ActivitiesID_NR2"].Value = Convert.ToInt32(LeaveActivities.NotRecommend);
                cmd.Parameters["ActivitiesID_PA2"].Value = Convert.ToInt32(LeaveActivities.PendingApproval);
                cmd.Parameters["FromDate4"].Value = row.LeaveFrom;
                cmd.Parameters["ToDate2"].Value = row.LeaveTo;
                cmd.Parameters["LeaveTypeID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
                cmd.Parameters["ToDate3"].Value = row.LeaveTo;
                cmd.Parameters["EmployeeCode1"].Value = cmd.Parameters["EmployeeCode2"].Value = row.EmployeeCode;

                Debug.Assert(cmd != null);
                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, leaveQuotaDS, leaveQuotaDS.ELQDetails.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_TO_BULK_LEAVEASSIGN.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                leaveQuotaDS.AcceptChanges();
            }

            DataSet returnDS = new DataSet();
            try
            {
                returnDS.Merge(leaveQuotaDS);
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();

                return returnDS;
            }
            catch (Exception EX)
            {
                return returnDS;
            }
        }
    }
}
