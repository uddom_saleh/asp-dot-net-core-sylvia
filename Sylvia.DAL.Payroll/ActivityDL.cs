/* *********************************************************************
 *  Compamy: Milllennium Information Solution Limited		 		           *  
 *  Author: Md. Hasinur Rahman Likhon				 				                   *
 *  Comment Date: April 13, 2006									                     *
 ***********************************************************************/

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
	/// <summary>
	/// Summary description for ActivityDL.
	/// </summary>
	public class ActivityDL
	{
		public ActivityDL()
		{
			//
			// TODO: Add constructor logic here
			//
		}
        public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {

          case ActionID.NA_ACTION_GET_ACTIVITY_LIST:
          return _getActivityList(inputDS);
        case ActionID.ACTION_DOES_ACTIVITY_EXIST:
          return _doesActivityExist(inputDS);
        case ActionID.ACTION_ACTIVITY_ADD:
          return _createActivity(inputDS);
        case ActionID.ACTION_ACTIVITY_DEL:
          return _deleteActivity(inputDS);
        case ActionID.ACTION_ACTIVITY_UPD:
          return _updateActivity(inputDS);
       
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
  
        private DataSet _getActivityList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new  DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            ActivityPO actPO = new ActivityPO();
            ActivityDS actDS = new ActivityDS();

            connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetActivityList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, actPO, actPO.Activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            nRowAffected = ADOController.Instance.Fill(adapter, actDS, actDS.Activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true )
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACTIVITY_LIST.ToString();
                errDS.Errors.AddError( err );
                errDS.AcceptChanges();
                return errDS;
            }

            //actPO.AcceptChanges();   // Commented :: WALI :: 29-Jul-2015
            actDS.AcceptChanges();

            #region Commented :: WALI :: 29-Jul-2015
            //if ( actPO.Activities.Count > 0 )
            //{
            //    foreach ( ActivityPO.Activity poAct in actPO.Activities)
            //    {
            //      ActivityDS.Activity act = actDS.Activities.NewActivity();
            //      if(poAct.IsActivityCodeNull()==false)
            //      {
            //        act.Code = poAct.ActivityCode;
            //      }
            //      if(poAct.IsActivityNameNull()==false)
            //      {
            //        act.Name = poAct.ActivityName;
            //      }
            //      actDS.Activities.AddActivity( act );
            //      actDS.AcceptChanges();
            //    }
            //}
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge( actDS );
            returnDS.Merge( errDS );
            returnDS.AcceptChanges();
            return returnDS;
        }
  
        private DataSet _doesActivityExist( DataSet inputDS )
        {
          DataBoolDS boolDS = new DataBoolDS();
          ErrorDS errDS = new ErrorDS();
          DataSet returnDS = new DataSet();     

          DBConnectionDS connDS = new DBConnectionDS();
          connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
          connDS.AcceptChanges();

          DataStringDS stringDS = new DataStringDS();
          stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
          stringDS.AcceptChanges();

          OleDbCommand cmd = null;
          cmd = DBCommandProvider.GetDBCommand("DoesActivityExist");
          if(cmd==null)
          {
            return UtilDL.GetCommandNotFound();
          }

          if(stringDS.DataStrings[0].IsStringValueNull()==false)
          {
            cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue ;
          }

          bool bError = false;
          int nRowAffected = -1;
          nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

          if (bError == true )
          {
            errDS.Clear();
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
            err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
            errDS.Errors.AddError( err );
            errDS.AcceptChanges();
            return errDS;
          }      

          boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
          boolDS.AcceptChanges();
          errDS.Clear();
          errDS.AcceptChanges();

          returnDS.Merge( boolDS );
          returnDS.Merge( errDS );
          returnDS.AcceptChanges();
          return returnDS;
        } 

        private DataSet _createActivity(DataSet inputDS)
        {
          ErrorDS errDS = new ErrorDS();
          ActivityDS actDS = new ActivityDS();
          DBConnectionDS connDS = new  DBConnectionDS();
          //extract dbconnection 
          connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
          connDS.AcceptChanges();
          //create command
          OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateActivity");
          if(cmd==null)
          {
            return UtilDL.GetCommandNotFound();
          }
          actDS.Merge( inputDS.Tables[ actDS.Activities.TableName ], false, MissingSchemaAction.Error );
          actDS.AcceptChanges();
          foreach(ActivityDS.Activity act in actDS.Activities)
          {
            long genPK = IDGenerator.GetNextGenericPK();
            if( genPK == -1)
            {
              UtilDL.GetDBOperationFailed();
            }
            cmd.Parameters["Id"].Value = (object) genPK;
            //code
            if(act.IsCodeNull()==false)
            {
              cmd.Parameters["Code"].Value = (object) act.Code;
            }
            else
            {
              cmd.Parameters["Code"].Value = null;
            }
            //name
            if(act.IsNameNull()==false)
            {
              cmd.Parameters["Name"].Value = (object) act.Name ;
            }
            else
            {
              cmd.Parameters["Name"].Value=null;
            }

            cmd.Parameters["DefaultSelection"].Value = (object)act.DefaultSelection;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
            if (bError == true )
            {
              ErrorDS.Error err = errDS.Errors.NewError();
              err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
              err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
              err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
              err.ErrorInfo1 = ActionID.ACTION_ACTIVITY_ADD.ToString();
              errDS.Errors.AddError( err );
              errDS.AcceptChanges();
              return errDS;
            }
          }
          errDS.Clear();
          errDS.AcceptChanges();
          return errDS;
        }
        private DataSet _deleteActivity(DataSet inputDS )
        {
          ErrorDS errDS = new ErrorDS();
          DBConnectionDS connDS = new  DBConnectionDS();      
          DataStringDS stringDS = new DataStringDS();
          connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
          connDS.AcceptChanges();
          stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
          stringDS.AcceptChanges();
          OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteActivity");
          if( cmd == null )
          {
            return UtilDL.GetCommandNotFound();
          }
          cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue;
          bool bError = false;
          int nRowAffected = -1;
          nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );
          if (bError == true )
          {
            ErrorDS.Error err = errDS.Errors.NewError();
            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
            err.ErrorInfo1 = ActionID.ACTION_ACTIVITY_DEL.ToString();
            errDS.Errors.AddError( err );
            errDS.AcceptChanges();
            return errDS;
          }
          return errDS;  // return empty ErrorDS
        }
        private DataSet _updateActivity(DataSet inputDS )
        {
          ErrorDS errDS = new ErrorDS();
          ActivityDS actDS = new ActivityDS();
          DBConnectionDS connDS = new  DBConnectionDS();
          //extract dbconnection 
          connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
          connDS.AcceptChanges();

          //create command
          OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateActivity");
          if ( cmd == null )
          {
            return UtilDL.GetCommandNotFound();
          }
          actDS.Merge( inputDS.Tables[ actDS.Activities.TableName ], false, MissingSchemaAction.Error );
          actDS.AcceptChanges();




          #region Update Default Selection to Zero if New Selection found
          OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
          cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_ACT";
          cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

          foreach (ActivityDS.Activity act in actDS.Activities)
          {
              if (act.DefaultSelection)
              {
                  bool bError = false;
                  int nRowAffected = -1;
                  nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                  if (bError == true)
                  {
                      ErrorDS.Error err = errDS.Errors.NewError();
                      err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                      err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                      err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                      err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_UPD.ToString();
                      errDS.Errors.AddError(err);
                      errDS.AcceptChanges();
                      return errDS;
                  }
              }
              break;
          }
          #endregion




          foreach (ActivityDS.Activity act in actDS.Activities)
          {
            if (act.IsCodeNull()==false)
            {
              cmd.Parameters["Code"].Value = (object) act.Code;
            }
            else
            {
              cmd.Parameters["Code"].Value =null;
            }
            if (act.IsNameNull()== false)
            {
              cmd.Parameters["Name"].Value = (object) act.Name ;
            }
            else
            {
              cmd.Parameters["Name"].Value = null;
            }

            cmd.Parameters["DefaultSelection"].Value = (object)act.DefaultSelection;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

            if (bError == true )
            {
              ErrorDS.Error err = errDS.Errors.NewError();
              err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
              err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
              err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
              err.ErrorInfo1 = ActionID.ACTION_ACTIVITY_ADD.ToString();
              errDS.Errors.AddError( err );
              errDS.AcceptChanges();
              return errDS;
            }
          }
          errDS.Clear();
          errDS.AcceptChanges();
          return errDS;
        }
	}
}
