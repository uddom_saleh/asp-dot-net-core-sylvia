using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using Sylvia.Common.Reports;

namespace Sylvia.DAL.Payroll
{
    class EmpAttendanceDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {


                //For Children
                case ActionID.ACTION_EMPLOYEE_ATTENDANCE_ADD:
                    return this._createAttendance_Login(inputDS);
                case ActionID.ACTION_EMPLOYEE_ATTENDANCE_UPD:
                    return this._updateAttendance_Logout(inputDS);

                case ActionID.NA_ACTION_GET_HOLIDAY_SPECIFIC_EMPLOYEE:
                    return this._getHoliday_Specific_Employee(inputDS);
                case ActionID.NA_ACTION_GET_ANNUAL_HOLIDAY:
                    return this._getAnnualHoliday(inputDS);
                case ActionID.NA_ACTION_GET_PERSONAL_LEAVE:
                    return this._getPersonalLeave(inputDS);
                case ActionID.NA_ACTION_GET_EMPLIST_SPECIFIC_DATE:
                    return this._getEmpList_Specific_Day(inputDS);
                case ActionID.ACTION_EXCEPTIONAL_LOGIN_UPD:
                    return this._updateExceptionalLogin(inputDS);
                case ActionID.ACTION_EXCEPTIONAL_LOGIN_ADD:
                    return this._createExceptional_Login(inputDS);
                case ActionID.NA_ACTION_GET_SHIFTING_PLAN:
                    return this._getShiftingList(inputDS);

                case ActionID.NA_ACTION_GET_SPECIFIC_EMPLOYEE:
                    return this._getSpecificEmployee(inputDS);
                case ActionID.NA_ACTION_GET_SPECIFIC_EMPLOYEE_FOR_FIRST_DAY:
                    return this._getSpecificEmployeeForFirstday(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEELIST_FOR_DAILY_ATTENDANCE:
                    return this._getEmployeeListForFormDate(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEELIST_FOR_DAILY_ATTENDANCE_ATTREGISTER:
                    return this._getEmployeeListForFormDate_AttRegister(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEELIST_FOR_DAILY_ATTENDANCE_ATTREGISTER_GRID:
                    return this._getEmployeeListForFormDateAttRegisterGrid(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_LIST_EXCEPTIONAL_LOGIN:
                    return this._getEmployeeListForExceptionalLogin(inputDS);
                case ActionID.NA_ACTION_GET_SHIFTING_PLAN_CODE:
                    return this._getShiftingListCode(inputDS);
                case ActionID.NA_ACTION_GET_SPECIFIC_EMPLOYEE_ALL:
                    return this._getSpecificEmployeeAll(inputDS);


                case ActionID.NA_ACTION_GET_DAILY_ATTENDANCE_REPORT_ADMIN:
                    return this._getDailyAttendanceForAdmin(inputDS);


                case ActionID.NA_ACTION_GET_ATT_WEEKEND_LIST_ALL:
                    return this._getAttWeekendListAll(inputDS);
                case ActionID.ACTION_ATT_WEEKEND_UPD:
                    return this._updateAttWeekend(inputDS);

                case ActionID.NA_ACTION_GET_SPECIFIC_EMPLOYEE_ATTENDANCE_NO_CALCULATION:
                    return this._getSpecificEmployeeAttendance_No_Calculation(inputDS);
                case ActionID.ACTION_ADD_ATTENDANCE_NO_CALCULATION:
                    return this._createAttendance_No_Calculation(inputDS);
                case ActionID.ACTION_ADD_ATTENDANCE_SIMPLE_LOGIN:
                    return this._createAttendance_Simple_Login(inputDS);

                case ActionID.ACTION_EMPLOYEE_ATTENDANCE_UPLOAD:
                    return this._create_Attendance_Upload(inputDS);
                case ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE:
                    return this._create_Attendance_Parking(inputDS);
                case ActionID.NA_ACTION_GET_ATTENDANCE_UPLOAD_PARKING:
                    return this._getAttendanceParking(inputDS);
                case ActionID.NA_ACTION_GET_SERIALNO:
                    return this._getSerialNoList(inputDS);
                case ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_DELETE:
                    return _deleteAttendanceParking(inputDS);

                case ActionID.NA_ACTION_GET_DAILY_ATTENDANCE_FOR_EXCEPTION:
                    return _getAttendanceRecordForException(inputDS);
                case ActionID.ACTION_EXCEPTIONAL_ATTENDANCE_PARKING_CREATE:
                    return _create_Attendance_Exception_Parking(inputDS);
                case ActionID.NA_ACTION_GET_EXCEPTIONAL_ATTENDANCE_PARKING:
                    return _getAttendanceExceptionParking(inputDS);
                case ActionID.ACTION_EXCEPTIONAL_ATTENDANCE_UPDATE:
                    return _update_Attendance_Exception(inputDS);
                case ActionID.ACTION_EXCEPTION_ATTENDANCE_PARKING_DELETE:
                    return _deleteAttendanceParkingException(inputDS);
                case ActionID.NA_ACTION_GET_PARKING_SERIALNO:
                    return this._getSerialNo(inputDS);
                case ActionID.NA_ACTION_GET_ATT_DEVICE_DATA:
                    return this._getAttDeviceData(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }

        }
        private DataSet _createAttendance_Login(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("createAttendance_Login");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            empAttendanceDS EmpDS = new empAttendanceDS();
            EmpDS.Merge(inputDS.Tables[EmpDS.AttendanceHistory.TableName], false, MissingSchemaAction.Error);
            EmpDS.AcceptChanges();

            foreach (empAttendanceDS.AttendanceHistoryRow EmpDSRow in EmpDS.AttendanceHistory.Rows)
            {
                //ATT ID
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["AttendanceID"].Value = (object)genPK;


                //EMP ID
                if (EmpDSRow.IsEmployeeIDNull() == false)
                {
                    cmd.Parameters["EmployeeID"].Value = (object)EmpDSRow.EmployeeID;
                }
                else
                {
                    cmd.Parameters["EmployeeID"].Value = null;
                }


                //ATT DATE

                if (EmpDSRow.IsAtt_DateNull() == false)
                {
                    cmd.Parameters["Att_Date"].Value = (object)EmpDSRow.Att_Date;
                }
                else
                {
                    cmd.Parameters["Att_Date"].Value = DBNull.Value; ;
                }


                //ATT LOGIN TIME 

                if (EmpDSRow.IsAtt_LoginTimeNull() == false)
                {
                    cmd.Parameters["Att_LoginTime"].Value = (object)EmpDSRow.Att_LoginTime;
                }
                else
                {
                    cmd.Parameters["Att_LoginTime"].Value = DBNull.Value;
                }


                ////ATT LOGOUT TIME

                //if (EmpDSRow.IsAtt_LogoutTimeNull() == false)
                //{
                //    cmd.Parameters["Att_LogoutTime"].Value = (object)EmpDSRow.Att_LogoutTime;
                //}
                //else
                //{
                //    cmd.Parameters["Att_LogoutTime"].Value = null;
                //}


                //ATT REMARKS
                if (EmpDSRow.IsAtt_RemarksNull() == false)
                {
                    cmd.Parameters["Att_Remarks"].Value = (object)EmpDSRow.Att_Remarks;
                }
                else
                {
                    cmd.Parameters["Att_Remarks"].Value = null;
                }


                //ATT FLAG
                if (EmpDSRow.IsAtt_FlagNull() == false)
                {
                    cmd.Parameters["Att_Flag"].Value = (object)EmpDSRow.Att_Flag;
                }
                else
                {
                    cmd.Parameters["Att_Flag"].Value = null;
                }

                //ACTIVE FLAG
                if (EmpDSRow.IsActiveFlagNull() == false)
                {
                    cmd.Parameters["ActiveFlag"].Value = (object)EmpDSRow.ActiveFlag;
                }
                else
                {
                    cmd.Parameters["ActiveFlag"].Value = null;
                }
                //CREATE USER
                if (EmpDSRow.IsCreateUserNull() == false)
                {
                    cmd.Parameters["CreateUser"].Value = (object)EmpDSRow.CreateUser;
                }
                else
                {
                    cmd.Parameters["CreateUser"].Value = null;
                }
                //CREATE DATE
                if (EmpDSRow.IsCreateDateNull() == false)
                {
                    cmd.Parameters["CreateDate"].Value = (object)EmpDSRow.CreateDate;
                }
                else
                {
                    cmd.Parameters["CreateDate"].Value = null;
                }
                ////UPDATE USER
                //if (EmpDSRow.IsUpdUserNull() == false)
                //{
                //    cmd.Parameters["UpdUser"].Value = (object)EmpDSRow.UpdUser;
                //}
                //else
                //{
                //    cmd.Parameters["UpdUser"].Value = null;
                //}
                ////UPDATE DATE
                //if (EmpDSRow.IsUpdDateNull() == false)
                //{
                //    cmd.Parameters["UpdDate"].Value = (object)EmpDSRow.UpdDate;
                //}
                //else
                //{
                //    cmd.Parameters["UpdDate"].Value = null;
                //}

                //SHIFTING PLAN ID
                if (EmpDSRow.IsShiftingPlanIDNull() == false)
                {
                    cmd.Parameters["ShiftingPlanID"].Value = (object)EmpDSRow.ShiftingPlanID;
                }
                else
                {
                    cmd.Parameters["ShiftingPlanID"].Value = null;
                }
                ////EXTRA HOUR
                //if (EmpDSRow.IsExtraHourNull() == false)
                //{
                //    cmd.Parameters["ExtraHour"].Value = (object)EmpDSRow.ExtraHour;
                //}
                //else
                //{
                //    cmd.Parameters["ExtraHour"].Value = null;
                //}
                if (EmpDSRow.IsAtt_RemarksNull() == false)
                {
                    cmd.Parameters["Att_Remarks"].Value = (object)EmpDSRow.Att_Remarks;
                }
                else
                {
                    cmd.Parameters["Att_Remarks"].Value = null;
                }



                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateAttendance_Logout(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateAttendance_Logout");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            empAttendanceDS EmpDS = new empAttendanceDS();
            EmpDS.Merge(inputDS.Tables[EmpDS.AttendanceHistory.TableName], false, MissingSchemaAction.Error);
            EmpDS.AcceptChanges();

            foreach (empAttendanceDS.AttendanceHistoryRow EmpDSRow in EmpDS.AttendanceHistory.Rows)
            {
                //ATTENDANCE ID

                cmd.Parameters["ATT_DATE"].Value = (object)EmpDSRow.Att_Date;
                //ATT LOGOUT TIME

                if (EmpDSRow.IsAtt_LogoutTimeNull() == false)
                {
                    cmd.Parameters["Att_LogoutTime"].Value = (object)EmpDSRow.Att_LogoutTime;
                }
                else
                {
                    cmd.Parameters["Att_LogoutTime"].Value = DBNull.Value; ;
                }


                //ATT REMARKS
                if (EmpDSRow.IsAtt_RemarksNull() == false)
                {
                    cmd.Parameters["Att_Remarks"].Value = (object)EmpDSRow.Att_Remarks;
                }
                else
                {
                    cmd.Parameters["Att_Remarks"].Value = null;
                }

                //UPDATE USER
                if (EmpDSRow.IsUpdUserNull() == false)
                {
                    cmd.Parameters["UpdUser"].Value = (object)EmpDSRow.UpdUser;
                }
                else
                {
                    cmd.Parameters["UpdUser"].Value = null;
                }
                //UPDATE DATE
                if (EmpDSRow.IsUpdDateNull() == false)
                {
                    cmd.Parameters["UpdDate"].Value = (object)EmpDSRow.UpdDate;
                }
                else
                {
                    cmd.Parameters["UpdDate"].Value = null;
                }

                //EXTRATime_HOUR
                if (EmpDSRow.IsExtraTime_HourNull() == false)
                {
                    cmd.Parameters["ExtraTime_Hour"].Value = (object)EmpDSRow.ExtraTime_Hour;
                }
                else
                {
                    cmd.Parameters["ExtraTime_Hour"].Value = DBNull.Value;
                }
                //EXTRATIME_MIN
                if (EmpDSRow.IsExtraTime_MinNull() == false)
                {
                    cmd.Parameters["ExtraTime_Min"].Value = (object)EmpDSRow.ExtraTime_Min;
                }
                else
                {
                    cmd.Parameters["ExtraTime_Min"].Value = DBNull.Value;
                }
                //EARLY DEPARTURE_HOUR
                if (EmpDSRow.IsEarlyDeparture_HourNull() == false)
                {
                    cmd.Parameters["EarlyDeparture_Hour"].Value = (object)EmpDSRow.EarlyDeparture_Hour;
                }
                else
                {
                    cmd.Parameters["EarlyDeparture_Hour"].Value = DBNull.Value;
                }
                //EARLY DEPARTURE_MIN
                if (EmpDSRow.IsEarlyDeparture_MinNull() == false)
                {
                    cmd.Parameters["EarlyDeparture_Min"].Value = (object)EmpDSRow.EarlyDeparture_Min;
                }
                else
                {
                    cmd.Parameters["EarlyDeparture_Min"].Value = DBNull.Value;
                }

                cmd.Parameters["EmpWorkingTime_Hour"].Value = (object)EmpDSRow.EmpWorkingTime_Hour;
                cmd.Parameters["EmpWorkingTime_Min"].Value = (object)EmpDSRow.EmpWorkingTime_Min;
                cmd.Parameters["EmployeeID"].Value = (object)EmpDSRow.EmployeeID;



                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getHoliday_Specific_Employee(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            empAttendanceDS EmpDS = new empAttendanceDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();

            string SLoginID = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            SLoginID = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Get sql command from commandXML folder
            if (SLoginID != "")
            {
                cmd = DBCommandProvider.GetDBCommand("GetHoliday_Specific_Employee");
                cmd.Parameters["LOGINID"].Value = SLoginID;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
            }
            //else
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetShiftingListAll");
            //}

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, EmpDS, EmpDS.empHoliday.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            //PersonDS.AcceptChanges();
            EmpDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(EmpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;





        }
        private DataSet _getAnnualHoliday(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            empAttendanceDS EmpDS = new empAttendanceDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();

            string eCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            eCode = stringDS.DataStrings[0].StringValue;

            //DateTime dtAttendanceDate2 = Convert.ToDateTime(eCode);

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Get sql command from commandXML folder
            //if (eCode != "")
            //{
            cmd = DBCommandProvider.GetDBCommand("getAnnualHoliday");
            // cmd.Parameters["dtAttendanceDate2"].Value = dtAttendanceDate2;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            //}
            //else
            //{
            //    cmd = DBCommandProvider.GetDBCommand("GetShiftingListAll");
            //}

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, EmpDS, EmpDS.empAnnualHoliday.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            //PersonDS.AcceptChanges();
            EmpDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(EmpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;





        }
        private DataSet _getPersonalLeave(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            empAttendanceDS EmpDS = new empAttendanceDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();

            string LOGINID = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            LOGINID = stringDS.DataStrings[0].StringValue;

            //DateTime dtAttendanceDate2 = Convert.ToDateTime(eCode);

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Get sql command from commandXML folder
            //if (eCode != "")
            //{
            cmd = DBCommandProvider.GetDBCommand("getPersonalLeave");
            cmd.Parameters["LOGINID"].Value = (object)LOGINID;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, EmpDS, EmpDS.empPersonalLeave.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            //PersonDS.AcceptChanges();
            EmpDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(EmpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;





        }
        private DataSet _getEmpList_Specific_Day(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            empAttendanceDS EmpDS = new empAttendanceDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();

            string strAttDate = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            strAttDate = stringDS.DataStrings[0].StringValue;

            //DateTime dtAttendanceDate2 = Convert.ToDateTime(eCode);

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Get sql command from commandXML folder
            //if (eCode != "")
            //{

            DateTime ATT_DATE = Convert.ToDateTime(strAttDate);

            cmd = DBCommandProvider.GetDBCommand("getEmpListForSpecificDate");
            cmd.Parameters["ATT_DATE"].Value = (object)ATT_DATE;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, EmpDS, EmpDS.empAttendanceCheck.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            //PersonDS.AcceptChanges();
            EmpDS.AcceptChanges();
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(EmpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;





        }
        private DataSet _updateExceptionalLogin(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateExceptionalLogin");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            empAttendanceDS EmpDS = new empAttendanceDS();
            EmpDS.Merge(inputDS.Tables[EmpDS.AttendanceHistory.TableName], false, MissingSchemaAction.Error);
            EmpDS.AcceptChanges();

            foreach (empAttendanceDS.AttendanceHistoryRow EmpDSRow in EmpDS.AttendanceHistory.Rows)
            {
                //ATTENDANCE ID

                cmd.Parameters["ATT_DATE"].Value = (object)EmpDSRow.Att_Date;

                //ATT LOGIN TIME

                if (EmpDSRow.IsAtt_LoginTimeNull() == false)
                {
                    cmd.Parameters["ATT_LOGINTIME"].Value = (object)EmpDSRow.Att_LoginTime;
                }
                else
                {
                    cmd.Parameters["ATT_LOGINTIME"].Value = DBNull.Value; ;
                }

                //ATT LOGOUT TIME

                if (EmpDSRow.IsAtt_LogoutTimeNull() == false)
                {
                    cmd.Parameters["Att_LogoutTime"].Value = (object)EmpDSRow.Att_LogoutTime;
                }
                else
                {
                    cmd.Parameters["Att_LogoutTime"].Value = DBNull.Value;
                }


                //ATT REMARKS
                if (EmpDSRow.IsAtt_RemarksNull() == false)
                {
                    cmd.Parameters["Att_Remarks"].Value = (object)EmpDSRow.Att_Remarks;
                }
                else
                {
                    cmd.Parameters["Att_Remarks"].Value = null;
                }

                //UPDATE USER
                if (EmpDSRow.IsUpdUserNull() == false)
                {
                    cmd.Parameters["UpdUser"].Value = (object)EmpDSRow.UpdUser;
                }
                else
                {
                    cmd.Parameters["UpdUser"].Value = DBNull.Value;
                }
                //UPDATE DATE
                if (EmpDSRow.IsUpdDateNull() == false)
                {
                    cmd.Parameters["UpdDate"].Value = (object)EmpDSRow.UpdDate;
                }
                else
                {
                    cmd.Parameters["UpdDate"].Value = null;
                }

                //EXTRATime_HOUR
                if (EmpDSRow.IsExtraTime_HourNull() == false)
                {
                    cmd.Parameters["ExtraTime_Hour"].Value = (object)EmpDSRow.ExtraTime_Hour;
                }
                else
                {
                    cmd.Parameters["ExtraTime_Hour"].Value = DBNull.Value;
                }
                //EXTRATIME_MIN
                if (EmpDSRow.IsExtraTime_MinNull() == false)
                {
                    cmd.Parameters["ExtraTime_Min"].Value = (object)EmpDSRow.ExtraTime_Min;
                }
                else
                {
                    cmd.Parameters["ExtraTime_Min"].Value = DBNull.Value;
                }
                //EARLY DEPARTURE_HOUR
                if (EmpDSRow.IsEarlyDeparture_HourNull() == false)
                {
                    cmd.Parameters["EarlyDeparture_Hour"].Value = (object)EmpDSRow.EarlyDeparture_Hour;
                }
                else
                {
                    cmd.Parameters["EarlyDeparture_Hour"].Value = DBNull.Value;
                }
                //EARLY DEPARTURE_MIN
                if (EmpDSRow.IsEarlyDeparture_MinNull() == false)
                {
                    cmd.Parameters["EarlyDeparture_Min"].Value = (object)EmpDSRow.EarlyDeparture_Min;
                }
                else
                {
                    cmd.Parameters["EarlyDeparture_Min"].Value = DBNull.Value;
                }
                //EMPLOYEEID
                cmd.Parameters["EMPLOYEEID"].Value = (object)EmpDSRow.EmployeeID;
                cmd.Parameters["ATT_FLAG"].Value = (object)EmpDSRow.Att_Flag;


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _createExceptional_Login(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("createExceptional_Login");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }


            empAttendanceDS EmpDS = new empAttendanceDS();
            EmpDS.Merge(inputDS.Tables[EmpDS.AttendanceHistory.TableName], false, MissingSchemaAction.Error);
            EmpDS.AcceptChanges();

            foreach (empAttendanceDS.AttendanceHistoryRow EmpDSRow in EmpDS.AttendanceHistory.Rows)
            {
                //ATT ID
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters["AttendanceID"].Value = (object)genPK;


                //EMP ID
                if (EmpDSRow.IsEmployeeIDNull() == false)
                {
                    cmd.Parameters["EmployeeID"].Value = (object)EmpDSRow.EmployeeID;
                }
                else
                {
                    cmd.Parameters["EmployeeID"].Value = null;
                }


                //ATT DATE

                if (EmpDSRow.IsAtt_DateNull() == false)
                {
                    cmd.Parameters["Att_Date"].Value = (object)EmpDSRow.Att_Date;
                }
                else
                {
                    cmd.Parameters["Att_Date"].Value = DBNull.Value;
                }


                //ATT LOGIN TIME 

                if (EmpDSRow.IsAtt_LoginTimeNull() == false)
                {
                    cmd.Parameters["Att_LoginTime"].Value = (object)EmpDSRow.Att_LoginTime;
                }
                else
                {
                    cmd.Parameters["Att_LoginTime"].Value = DBNull.Value;
                }


                //ATT LOGOUT TIME
                try
                {
                    if (EmpDSRow.IsAtt_LogoutTimeNull() == false)
                    {
                        cmd.Parameters["Att_LogoutTime"].Value = (object)EmpDSRow.Att_LogoutTime;
                    }
                    else
                    {
                        cmd.Parameters["Att_LogoutTime"].Value = DBNull.Value;
                    }
                }
                catch (Exception ex)
                {
                }

                //ATT REMARKS
                if (EmpDSRow.IsAtt_RemarksNull() == false)
                {
                    cmd.Parameters["Att_Remarks"].Value = (object)EmpDSRow.Att_Remarks;
                }
                else
                {
                    cmd.Parameters["Att_Remarks"].Value = null;
                }


                //ATT FLAG
                if (EmpDSRow.IsAtt_FlagNull() == false)
                {
                    cmd.Parameters["Att_Flag"].Value = (object)EmpDSRow.Att_Flag;
                }
                else
                {
                    cmd.Parameters["Att_Flag"].Value = null;
                }

                //ACTIVE FLAG
                if (EmpDSRow.IsActiveFlagNull() == false)
                {
                    cmd.Parameters["ActiveFlag"].Value = (object)EmpDSRow.ActiveFlag;
                }
                else
                {
                    cmd.Parameters["ActiveFlag"].Value = null;
                }
                //CREATE USER
                if (EmpDSRow.IsCreateUserNull() == false)
                {
                    cmd.Parameters["CreateUser"].Value = (object)EmpDSRow.CreateUser;
                }
                else
                {
                    cmd.Parameters["CreateUser"].Value = null;
                }
                //CREATE DATE
                if (EmpDSRow.IsCreateDateNull() == false)
                {
                    cmd.Parameters["CreateDate"].Value = (object)EmpDSRow.CreateDate;
                }
                else
                {
                    cmd.Parameters["CreateDate"].Value = DBNull.Value; ;
                }


                ////UPDATE USER
                //if (EmpDSRow.IsUpdUserNull() == false)
                //{
                //    cmd.Parameters["UpdUser"].Value = (object)EmpDSRow.UpdUser;
                //}
                //else
                //{
                //    cmd.Parameters["UpdUser"].Value = null;
                //}
                ////UPDATE DATE
                //if (EmpDSRow.IsUpdDateNull() == false)
                //{
                //    cmd.Parameters["UpdDate"].Value = (object)EmpDSRow.UpdDate;
                //}
                //else
                //{
                //    cmd.Parameters["UpdDate"].Value = null;
                //}

                //SHIFTING PLAN ID
                if (EmpDSRow.IsShiftingPlanIDNull() == false)
                {
                    cmd.Parameters["ShiftingPlanID"].Value = (object)EmpDSRow.ShiftingPlanID;
                }
                else
                {
                    cmd.Parameters["ShiftingPlanID"].Value = null;
                }
                ////EXTRA HOUR
                //if (EmpDSRow.IsExtraHourNull() == false)
                //{
                //    cmd.Parameters["ExtraHour"].Value = (object)EmpDSRow.ExtraHour;
                //}
                //else
                //{
                //    cmd.Parameters["ExtraHour"].Value = null;
                //}




                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getShiftingList(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            ShiftingPlanDS SpDS = new ShiftingPlanDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();

            string eCode = "";

            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();
            //eCode = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("getShiftingPlanList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, SpDS, SpDS.GetShiftingPlan.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            SpDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(SpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        //private DataSet GetShiftingPlanList_specificEmployee(DataSet inputDS)
        //{
        //    OleDbCommand cmd = new OleDbCommand();
        //    ShiftingPlanDS SpDS = new ShiftingPlanDS();
        //    ErrorDS errDS = new ErrorDS();

        //    DBConnectionDS connDS = new DBConnectionDS();
        //    DataLongDS longDS = new DataLongDS();

        //    DataStringDS stringDS = new DataStringDS();

        //    string SLoginID = "";

        //    stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        //    stringDS.AcceptChanges();
        //    SLoginID = stringDS.DataStrings[0].StringValue;

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //    connDS.AcceptChanges();

        //    cmd = DBCommandProvider.GetDBCommand("getShiftingPlanListForSpecificEmployee");
        //    cmd.Parameters["LOGINID"].Value = SLoginID;

        //    if (cmd == null)
        //    {
        //        return UtilDL.GetCommandNotFound();
        //    }

        //    OleDbDataAdapter adapter = new OleDbDataAdapter();
        //    adapter.SelectCommand = cmd;

        //    bool bError = false;
        //    int nRowAffected = -1;
        //    nRowAffected = ADOController.Instance.Fill(adapter, SpDS, SpDS.GetShiftingPlan.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

        //    if (bError)
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;

        //    }

        //    SpDS.AcceptChanges();
        //    errDS.Clear();
        //    errDS.AcceptChanges();
        //    DataSet returnDS = new DataSet();
        //    returnDS.Merge(SpDS);
        //    returnDS.Merge(errDS);
        //    returnDS.AcceptChanges();
        //    return returnDS;
        //}
        private DataSet _getSpecificEmployee(DataSet inputDS)
        {

            OleDbCommand cmd = new OleDbCommand();
            //EmployeeDS empDS = new EmployeeDS();
            empAttendanceDS empDS = new empAttendanceDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            string strSpecificEmployeeForAtt = "";
            strSpecificEmployeeForAtt = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (strSpecificEmployeeForAtt != "")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSpecificEmployeeForAttendance");
                cmd.Parameters["Loginid"].Value = strSpecificEmployeeForAtt;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.empAttendance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SPECIFIC_EMPLOYEE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }


            empDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _getSpecificEmployeeForFirstday(DataSet inputDS)
        {

            OleDbCommand cmd = new OleDbCommand();
            //EmployeeDS empDS = new EmployeeDS();
            empAttendanceDS empDS = new empAttendanceDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            string strSpecificEmployeeForAtt = stringDS.DataStrings[0].StringValue;
            string strSpecificEmployeeForAtt1 = stringDS.DataStrings[1].StringValue;


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (strSpecificEmployeeForAtt != "")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSpecificEmployeeForAttendanceFirstDay");
                cmd.Parameters["Loginid"].Value = strSpecificEmployeeForAtt;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.empAttendance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SPECIFIC_EMPLOYEE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }


            empDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        //private DataSet _getEmployeeListForFormDate(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();

        //    DBConnectionDS connDS = new DBConnectionDS();
        //    DataLongDS longDS = new DataLongDS();


        //    DataStringDS stringDS = new DataStringDS();
        //    stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        //    stringDS.AcceptChanges();

        //    string strShiftingCode = stringDS.DataStrings[0].StringValue;
        //    //string strSpecificEmployeeForAtt1 = stringDS.DataStrings[1].StringValue;
        //    DateTime dtEffDate = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //    connDS.AcceptChanges();
        //    OleDbCommand cmd = DBCommandProvider.GetDBCommand("getEmployeeListForFormDate");
        //    //cmd.Parameters["SHIFTINGCODE"].Value = strShiftingCode;
        //    cmd.Parameters["ATT_DATE"].Value = dtEffDate;

        //    //Debug.Assert(cmd != null);
        //    OleDbDataAdapter adapter = new OleDbDataAdapter();
        //    adapter.SelectCommand = cmd;

        //    bool bError = false;
        //    int nRowAffected = -1;
        //    //#region Data Load (Jarif 10-Sep-11 (Update))


        //    //EmployeeDS empDS = new EmployeeDS();
        //    //nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


        //    empAttendanceDS empDS = new empAttendanceDS();
        //    nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.empAttendance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        //    if (bError)
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST.ToString();
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }
        //    empDS.AcceptChanges();

        //    // now create the packet
        //    errDS.Clear();
        //    errDS.AcceptChanges();
        //    DataSet returnDS = new DataSet();
        //    returnDS.Merge(empDS);
        //    returnDS.Merge(errDS);
        //    returnDS.AcceptChanges();
        //    //    
        //    return returnDS;
        //    //    
        //}
        private DataSet _getEmployeeListForFormDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();


            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            string strShiftingCode = stringDS.DataStrings[0].StringValue;
            //string strSpecificEmployeeForAtt1 = stringDS.DataStrings[1].StringValue;
            DateTime dtEffDate = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getEmployeeListForFormDate");
            //cmd.Parameters["SHIFTINGCODE"].Value = strShiftingCode;
            cmd.Parameters["ATT_DATE"].Value = dtEffDate;

            //Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //#region Data Load (Jarif 10-Sep-11 (Update))


            //EmployeeDS empDS = new EmployeeDS();
            //nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            empAttendanceDS AempDS = new empAttendanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, AempDS, AempDS.AttReport.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            AempDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(AempDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }
        private DataSet _getEmployeeListForFormDate_AttRegister(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();


            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            //string strShiftingCode = stringDS.DataStrings[0].StringValue;
            //string strSpecificEmployeeForAtt1 = stringDS.DataStrings[1].StringValue;
            DateTime dtFromDate = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            DateTime dtTodate = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getEmployeeListForFormDate_AttRegister");
            //cmd.Parameters["SHIFTINGCODE"].Value = strShiftingCode;
            cmd.Parameters["dtFromDate"].Value = dtFromDate;
            cmd.Parameters["dtTodate"].Value = dtTodate;


            //Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //#region Data Load (Jarif 10-Sep-11 (Update))


            //EmployeeDS empDS = new EmployeeDS();
            //nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            empAttendanceDS AempDS = new empAttendanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, AempDS, AempDS.AttReport.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            AempDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(AempDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }
        private DataSet _getEmployeeListForFormDateAttRegisterGrid(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();


            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            //string strShiftingCode = stringDS.DataStrings[0].StringValue;
            //string strSpecificEmployeeForAtt1 = stringDS.DataStrings[1].StringValue;
            DateTime dtFromDate = Convert.ToDateTime(stringDS.DataStrings[0].StringValue);
            DateTime dtTodate = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
            string strPresent = stringDS.DataStrings[2].StringValue;
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (strPresent == "nothing")
            {
                cmd = DBCommandProvider.GetDBCommand("getEmployeeListForFormDate_AttRegister_grid");



            }
            else if (strPresent == "P")
            {
                cmd = DBCommandProvider.GetDBCommand("getEmployeeListForFormDate_AttRegister_grid_Present");

            }
            else if (strPresent == "L")
            {
                cmd = DBCommandProvider.GetDBCommand("getEmployeeListForFormDate_AttRegister_grid_Late");

            }
            //cmd.Parameters["SHIFTINGCODE"].Value = strShiftingCode;
            //cmd.Parameters["ATT_FLAG"].Value = strPresent;
            cmd.Parameters["dtFromDate"].Value = dtFromDate;
            cmd.Parameters["dtTodate"].Value = dtTodate;


            //Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            //#region Data Load (Jarif 10-Sep-11 (Update))


            //EmployeeDS empDS = new EmployeeDS();
            //nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);


            empAttendanceDS AempDS = new empAttendanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, AempDS, AempDS.AttReportGrid.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            AempDS.AcceptChanges();






            ////////////////////////////////////////////////////////////////////////////// DataTable df = AempDS.Tables["AttReportGrid"].DefaultView.ToTable(true, "");




            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(AempDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }
        private DataSet _getEmployeeListForExceptionalLogin(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeListForExcepLogin");
            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            EmployeePO empPO = new EmployeePO();

            bool bError = false;
            int nRowAffected = -1;
            #region Data Load (Jarif 10-Sep-11 (Update))
            #region Old...
            //nRowAffected = ADOController.Instance.Fill(adapter, empPO, empPO.Employees.TableName,connDS.DBConnections[0].ConnectionID,ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}
            //empPO.AcceptChanges();


            //EmployeeDS empDS = new EmployeeDS();
            //if (empPO.Employees.Count > 0)
            //{
            //    foreach (EmployeePO.Employee poEmp in empPO.Employees)
            //    {
            //        EmployeeDS.Employee emp = empDS.Employees.NewEmployee();
            //        emp.EmployeeCode = poEmp.EmployeeCode;
            //        emp.EmployeeName = poEmp.EmployeeName;
            //        if (poEmp.IsPassportNumberNull() != true)
            //        {
            //            emp.PassportNumber = poEmp.PassportNumber;
            //        }
            //        if (poEmp.IsTinNumberNull() != true)
            //        {
            //            emp.TinNumber = poEmp.TinNumber;
            //        }
            //        if (poEmp.IsGenderNull() != true)
            //        {
            //            emp.Gender = poEmp.Gender;
            //        }
            //        if (poEmp.IsBasicSalaryNull() == false)
            //        {
            //            emp.BasicSalary = poEmp.BasicSalary;
            //        }
            //        if (poEmp.IsPercentOfBasicNull() == false)
            //        {
            //            emp.PercentOfBasicSalary = poEmp.PercentOfBasic; // Miss
            //        }

            //        if (poEmp.IsERPIdNull() != true)
            //        {
            //            emp.ERPId = poEmp.ERPId;
            //        }
            //        if (poEmp.IsReligionNull() != true)
            //        {
            //            emp.Religion = poEmp.Religion;
            //        }
            //        if (poEmp.IsBloodGroupNull() != true)
            //        {
            //            emp.BloodGroup = poEmp.BloodGroup;
            //        }
            //        if (poEmp.IsNationalityNull() != true)
            //        {
            //            emp.Nationality = poEmp.Nationality;
            //        }
            //        if (poEmp.IsMaritalStatusNull() != true)
            //        {
            //            emp.MaritalStatus = poEmp.MaritalStatus;
            //        }
            //        if (poEmp.IsNationalCodeNull() != true)
            //        {
            //            emp.NationalCode = poEmp.NationalCode;
            //        }
            //        if (poEmp.IsDateOfBirthNull() != true)
            //        {
            //            emp.DateOfBirth = poEmp.DateOfBirth;
            //        }
            //        if (poEmp.IsPlaceOfBirthNull() != true)
            //        {
            //            emp.PlaceOfBirth = poEmp.PlaceOfBirth;
            //        }
            //        if (poEmp.IsFatherNameNull() != true)
            //        {
            //            emp.FatherName = poEmp.FatherName;
            //        }
            //        if (poEmp.IsFatherOccupationNull() != true)
            //        {
            //            emp.FatherOccupation = poEmp.FatherOccupation;
            //        }
            //        if (poEmp.IsMotherNameNull() != true)
            //        {
            //            emp.MotherName = poEmp.MotherName;
            //        }
            //        if (poEmp.IsMotherOccupationNull() != true)
            //        {
            //            emp.MotherOccupation = poEmp.MotherOccupation;
            //        }
            //        if (poEmp.IsMarriageDateNull() != true)
            //        {
            //            emp.MarriageDate = poEmp.MarriageDate;
            //        }
            //        if (poEmp.IsSpouseNameNull() != true)
            //        {
            //            emp.SpouseName = poEmp.SpouseName;
            //        }
            //        if (poEmp.IsSpouseOccupationNull() != true)
            //        {
            //            emp.SpouseOccupation = poEmp.SpouseOccupation;
            //        }
            //        if (poEmp.IsPresentAddressNull() != true)
            //        {
            //            emp.PresentAddress = poEmp.PresentAddress;
            //        }
            //        if (poEmp.IsPermanentAddressNull() != true)
            //        {
            //            emp.PermanentAddress = poEmp.PermanentAddress;
            //        }
            //        if (poEmp.IsEmailNull() != true)
            //        {
            //            emp.Email = poEmp.Email;
            //        }
            //        if (poEmp.IsFaxNull() != true)
            //        {
            //            emp.Fax = poEmp.Fax;
            //        }
            //        if (poEmp.IsMobileNull() != true)
            //        {
            //            emp.Mobile = poEmp.Mobile;
            //        }
            //        if (poEmp.IsTelephoneNull() != true)
            //        {
            //            emp.Telephone = poEmp.Telephone;
            //        }
            //        if (poEmp.IsLNNmaeNull() != true)
            //        {
            //            emp.LNName = poEmp.LNNmae;
            //        }
            //        if (poEmp.IsLNAddressNull() != true)
            //        {
            //            emp.LNAddress = poEmp.LNAddress;
            //        }

            //        //emp.EmployeeStatus=poEmp.EmployeeStatus;
            //        if (poEmp.IsConfirmDateNull() != true)
            //        {
            //            emp.ConfirmationDate = poEmp.ConfirmDate;
            //        }
            //        if (poEmp.IsBranchCodeNull() != true)
            //        {
            //            // emp.BankCode=poEmp.BranchCode;
            //            emp.BranchCode = poEmp.BranchCode;
            //        }
            //        if (poEmp.IsBankCodeNull() != true)
            //        {

            //            emp.BankCode = poEmp.BankCode;
            //        }
            //        if (poEmp.IsBankAccountNoNull() != true)
            //        {
            //            emp.BankAccountNo = poEmp.BankAccountNo;
            //        }
            //        if (poEmp.IsDepartmentCodeNull() != true)
            //        {
            //            emp.DepartmentCode = poEmp.DepartmentCode;
            //        }
            //        if (poEmp.IsDepartmentNameNull() != true)
            //        {
            //            emp.DepartmentName = poEmp.DepartmentName;
            //        }
            //        if (poEmp.IsLocationCodeNull() != true)
            //        {
            //            emp.LocationCode = poEmp.LocationCode;
            //        }
            //        if (poEmp.IsDesignationCodeNull() != true)
            //        {
            //            emp.DesignationCode = poEmp.DesignationCode;
            //        }
            //        if (poEmp.IsDesignationNameNull() != true)
            //        {
            //            emp.DesignationName = poEmp.DesignationName;
            //        }
            //        if (poEmp.IsGradeCodeNull() != true)
            //        {
            //            emp.GradeCode = poEmp.GradeCode;
            //        }
            //        if (poEmp.IsGradeNameNull() != true)
            //        {
            //            emp.GradeName = poEmp.GradeName;
            //        }
            //        if (poEmp.IsPaymentModeNull() != true)
            //        {
            //            emp.PaymentMode = poEmp.PaymentMode;
            //        }
            //        if (poEmp.IsIsExpatriateNull() != true)
            //        {
            //            emp.IsExpatriate = poEmp.IsExpatriate;
            //        }
            //        if (poEmp.IsCompanyCodeNull() != true)
            //        {
            //            emp.CompanyCode = poEmp.CompanyCode;
            //        }
            //        if (poEmp.IsCostCenterCodeNull() != true)
            //        {
            //            emp.CostCenterCode = poEmp.CostCenterCode;
            //        }
            //        if (poEmp.IsCostcenterNameNull() != true)
            //        {
            //            emp.CostcenterName = poEmp.CostcenterName;
            //        }
            //        if (poEmp.IsActivityCodeNull() != true)
            //        {
            //            emp.ActivityCode = poEmp.ActivityCode;
            //        }
            //        if (poEmp.IsFunctionCodeNull() != true)
            //        {
            //            emp.FunctionCode = poEmp.FunctionCode;
            //        }
            //        if (poEmp.IsRescenterCodeNull() != true)
            //        {
            //            emp.RescenterCode = poEmp.RescenterCode;
            //        }
            //        if (poEmp.IsSiteCodeNull() != true)
            //        {
            //            emp.SiteCode = poEmp.SiteCode;
            //        }
            //        if (poEmp.IsPayrollSiteCodeNull() != true)
            //        {
            //            emp.PayrollSiteCode = poEmp.PayrollSiteCode;
            //        }
            //        if (poEmp.IsEmployeeTypeNull() == false)
            //        {
            //            emp.EmployeeType = poEmp.EmployeeType;
            //        }
            //        if (poEmp.IsJoiningDateNull() != true)
            //        {
            //            emp.JoiningDate = poEmp.JoiningDate;
            //        }
            //        if (poEmp.IsEmployeeStatusNull() != true)
            //        {
            //            emp.EmployeeStatus = poEmp.EmployeeStatus;
            //        }
            //        if (poEmp.IsEmployeeCategoryNull() != true)
            //        {
            //            emp.EmployeeCategory = poEmp.EmployeeCategory;
            //        }
            //        if (poEmp.IsPFMembDateNull() != true)
            //        {
            //            emp.PFMembDate = poEmp.PFMembDate;
            //        }

            //        //mislbd.Jarif Update Start(28-July-2009)
            //         emp.IsCarAllocated = poEmp.IsCarAllocated;
            //        //mislbd.Jarif Update End(28-July-2009)

            //         //mislbd.Jarif Update Start(01-Nov-2009)
            //         emp.IsEligibleForGratuity = poEmp.IsEligibleForGratuity;
            //         //mislbd.Jarif Update End(01-Nov-2009)

            //         //mislbd.Jarif Update Start(14-Dec-2010)
            //         if (poEmp.IsKPIGroupIDNull() != true)
            //         {
            //             emp.KPIGroupID = poEmp.KPIGroupID;
            //         }
            //         //mislbd.Jarif Update End(14-Dec-2010)

            //         //mislbd.Kaysar Update Start(18-Jan-2011)
            //         emp.LeaveApprovalAuthority = poEmp.LeaveApprovalAuthority;
            //         try
            //         {
            //             emp.SupervisorID = poEmp.SupervisorID;
            //         }
            //         catch
            //         {
            //         }
            //         try
            //         {
            //             emp.SupervisorCode = poEmp.SupervisorCode;
            //         }
            //         catch
            //         {
            //         }
            //         //mislbd.Kaysar Update End(18-Jan-2011)


            //        empDS.Employees.AddEmployee(emp);
            //        empDS.AcceptChanges();
            //    }
            //}
            #endregion

            EmployeeDS empDS = new EmployeeDS();
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empDS.AcceptChanges();
            #endregion
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }
        private DataSet _getShiftingListCode(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            ShiftingPlanDS SpDS = new ShiftingPlanDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();

            string eCode = "";



            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("getShiftingPlanListCode");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, SpDS, SpDS.ShiftingPlan.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SHIFTING_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            SpDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(SpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _getSpecificEmployeeAll(DataSet inputDS)
        {

            OleDbCommand cmd = new OleDbCommand();
            empAttendanceDS empDS = new empAttendanceDS();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            //DataStringDS stringDS = new DataStringDS();
            //stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            //stringDS.AcceptChanges();

            //string strSpecificEmployeeForAtt = "";
            //strSpecificEmployeeForAtt = stringDS.DataStrings[0].StringValue;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //if (strSpecificEmployeeForAtt != "")
            //{
            cmd = DBCommandProvider.GetDBCommand("GetSpecificEmployeeForAttendanceAll");
            //cmd.Parameters["Loginid"].Value = strSpecificEmployeeForAtt;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            //}

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.empAttendance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SPECIFIC_EMPLOYEE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }


            empDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }
        private DataSet _getDailyAttendanceForAdmin(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("getDailyAttendanceReportfORAdmin");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["ATT_DATE"].Value = (object)dateDS.DataDates[0].DateValue; //Jarif (28 Sep 11)

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            rptDailyAttendanceDS DailyAttendanceDS = new rptDailyAttendanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, DailyAttendanceDS, DailyAttendanceDS.DailyAttendanceForAdminReport.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PAYSHEET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            DailyAttendanceDS.AcceptChanges();

            String queryExp = "0";
            string strFullGrid = "";
            foreach (DataStringDS.DataString empCode in stringDS.DataStrings)
            {
                strFullGrid = empCode.StringValue;
                //if (strFullGrid != "Full")
                //{
                queryExp += "," + "'" + empCode.StringValue + "'";
                //}
            }
            //if (strFullGrid != "Full")
            //{
            DataRow[] foundRows = DailyAttendanceDS.DailyAttendanceForAdminReport.Select("EMPLOYEECODE IN(" + queryExp + ")");
            DailyAttendanceDS = new rptDailyAttendanceDS();
            int n = foundRows.Length;
            foreach (DataRow dRow in foundRows)
            {
                DailyAttendanceDS.DailyAttendanceForAdminReport.ImportRow(dRow);
            }

            DailyAttendanceDS.AcceptChanges();
            //}

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(DailyAttendanceDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        #region WALI :: Configure Weekends for Attendance :: 29-Jan-2014
        private DataSet _getAttWeekendListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd = DBCommandProvider.GetDBCommand("GetAttWeekendListAll");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            lmsHolidayPO holidayPO = new lmsHolidayPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, holidayPO, holidayPO.Weekends.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATT_WEEKEND_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            holidayPO.AcceptChanges();

            lmsHolidayDS holidayDS = new lmsHolidayDS();
            if (holidayPO.Weekends.Count > 0)
            {
                foreach (lmsHolidayPO.WeekendsRow poRow in holidayPO.Weekends.Rows)
                {
                    lmsHolidayDS.WeekendsRow row = holidayDS.Weekends.NewWeekendsRow();
                    if (poRow.IsWeekendIDNull() == false)
                    {
                        row.WeekendID = poRow.WeekendID;
                    }
                    if (poRow.IsNameOfDayNull() == false)
                    {
                        row.NameOfDay = poRow.NameOfDay;
                    }
                    if (poRow.IsDayLengthIDNull() == false)
                    {
                        row.DayLengthID = poRow.DayLengthID;
                    }
                    //Kaysar, 30-Apr-2011
                    if (poRow.IsSiteCodeNull() == false)
                    {
                        row.SiteCode = poRow.SiteCode;
                    }
                    if (poRow.IsSiteWeekendIDNull() == false)
                    {
                        row.SiteWeekendID = poRow.SiteWeekendID;
                    }
                    //

                    //Kaysar, 03-May-2011
                    if (poRow.IsSiteIDNull() == false)
                    {
                        row.SiteID = poRow.SiteID;
                    }
                    //

                    holidayDS.Weekends.AddWeekendsRow(row);
                    holidayDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(holidayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _updateAttWeekend(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            lmsHolidayDS holidayDS = new lmsHolidayDS();
            DataBoolDS boolDS = new DataBoolDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            //cmd.CommandText = "PRO_LMS_WEEKENDS_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            holidayDS.Merge(inputDS.Tables[holidayDS.Weekends.TableName], false, MissingSchemaAction.Error);
            holidayDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            foreach (lmsHolidayDS.WeekendsRow row in holidayDS.Weekends.Rows)
            {
                //Kaysar, 03-May-2011
                if (boolDS.DataBools[0].BoolValue) //Indicates for Insert...
                {
                    long siteWeekendID = IDGenerator.GetNextGenericPK();
                    if (siteWeekendID == -1) return UtilDL.GetDBOperationFailed();

                    cmd.CommandText = "PRO_ATT_SITEWEEKEND_CREATE";
                    cmd.Parameters.AddWithValue("p_SiteWeekendID", siteWeekendID);
                }
                else //Indicates for Update...
                {
                    cmd.CommandText = "PRO_ATT_SITEWEEKEND_UPDATE";
                    cmd.Parameters.AddWithValue("p_SiteWeekendID", row.SiteWeekendID);
                }

                if (row.IsSiteCodeNull() == false) cmd.Parameters.AddWithValue("p_SiteCode", row.SiteCode);
                else cmd.Parameters.AddWithValue("p_SiteCode", DBNull.Value);

                cmd.Parameters.AddWithValue("p_WEEKENDSID", row.WeekendID);

                try
                {
                    Convert.ToInt32(row.DayLengthID);
                    cmd.Parameters.AddWithValue("p_DAYLENGTHID", row.DayLengthID);
                }
                catch
                {
                    cmd.Parameters.AddWithValue("p_DAYLENGTHID", DBNull.Value);
                }

                if (row.IsSiteWeekendRemarksNull() == false) cmd.Parameters.AddWithValue("p_SiteWeekendRemarks", row.SiteWeekendRemarks);
                else cmd.Parameters.AddWithValue("p_SiteWeekendRemarks", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_LMS_WEEKEND_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        #endregion
        private DataSet _getSpecificEmployeeAttendance_No_Calculation(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            empAttendanceDS empDS = new empAttendanceDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetSpecificEmpAttendance_NoCalculation");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["LoginID"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.EmpAttendance_NoCalculation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SPECIFIC_EMPLOYEE_ATTENDANCE_NO_CALCULATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            empDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(empDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createAttendance_No_Calculation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            empAttendanceDS attDS = new empAttendanceDS();
            DataSet returnDS = new DataSet();
            string message = "Login from SYLVIA";

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            attDS.Merge(inputDS.Tables[attDS.EmpAttendance_NoCalculation.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateAttendance_NoCalc");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (empAttendanceDS.EmpAttendance_NoCalculationRow row in attDS.EmpAttendance_NoCalculation.Rows)
            {
                cmd.Parameters["UserID"].Value = row.LoginID;                 // EmployeeCode
                cmd.Parameters["EventTrigger"].Value = row.EventTrigger;      // "IN" or "OUT"
                cmd.Parameters["OriginalSN"].Value = message;
                cmd.Parameters["SenderSN"].Value = message;
                cmd.Parameters["p_AccessMethod"].Value = message;
                cmd.Parameters["p_PhotoPath"].Value = message;

                if (!row.IsRemarkNull()) cmd.Parameters["Remark"].Value = row.Remark;
                else cmd.Parameters["Remark"].Value = message;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ADD_ATTENDANCE_NO_CALCULATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
            }

            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _createAttendance_Simple_Login(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            empAttendanceDS attDS = new empAttendanceDS();
            rptDailyAttendanceDS dailyAttendanceDSrpt = new rptDailyAttendanceDS();
            DataSet returnDS = new DataSet();
            DataStringDS stringDS = new DataStringDS();
            string message = "Login from SYLVIA";
            string remarks = "Simple Login from SYLVIA";
            string successMsg = "";
            string loginDateTime = "";
            bool bError = false;
            int nRowAffected = -1;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            attDS.Merge(inputDS.Tables[attDS.EmpAttendance_NoCalculation.TableName], false, MissingSchemaAction.Error);
            attDS.AcceptChanges();

            string clientName = stringDS.DataStrings[0].StringValue;
            stringDS = new DataStringDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ATT_LOGIN_SIMPLE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (empAttendanceDS.EmpAttendance_NoCalculationRow row in attDS.EmpAttendance_NoCalculation.Rows)
            {
                if (clientName == "Uddipan")
                {
                    #region For Uddipan
                    cmd.Parameters.AddWithValue("p_UserID", row.LoginID);
                    cmd.Parameters.AddWithValue("p_OriginalSN", message);
                    cmd.Parameters.AddWithValue("p_SenderSN", message);

                    if (!row.IsRemarkNull()) cmd.Parameters.AddWithValue("p_Remark", row.Remark);
                    else cmd.Parameters.AddWithValue("p_Remark", remarks);

                    cmd.Parameters.AddWithValue("p_AccessMethod", message);
                    cmd.Parameters.AddWithValue("p_PhotoPath", message);
                    cmd.Parameters.Add("p_OutputMessage", OleDbType.VarChar, 1000).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("p_LoginDateTime", OleDbType.Date, 1000).Direction = ParameterDirection.Output;
                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ADD_ATTENDANCE_SIMPLE_LOGIN.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                    }
                    successMsg = cmd.Parameters["p_OutputMessage"].Value.ToString();
                    loginDateTime = cmd.Parameters["p_LoginDateTime"].Value.ToString();
                    stringDS.DataStrings.AddDataString(successMsg);
                    stringDS.DataStrings.AddDataString(loginDateTime);
                    stringDS.AcceptChanges();
                    #endregion
                }
                else if (clientName == "Millennium")
                {
                    #region For MISL
                    cmd.Parameters.AddWithValue("p_UserID", row.LoginID);

                    if (!row.IsRemarkNull()) cmd.Parameters.AddWithValue("p_Remark", row.Remark);
                    else cmd.Parameters.AddWithValue("p_Remark", remarks);

                    cmd.Parameters.Add("p_OutputMessage", OleDbType.VarChar, 1000).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("p_LoginDateTime", OleDbType.Date, 1000).Direction = ParameterDirection.Output;
                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ADD_ATTENDANCE_SIMPLE_LOGIN.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                    }
                    successMsg = cmd.Parameters["p_OutputMessage"].Value.ToString();
                    loginDateTime = cmd.Parameters["p_LoginDateTime"].Value.ToString();
                    stringDS.DataStrings.AddDataString(successMsg);
                    stringDS.DataStrings.AddDataString(loginDateTime);
                    stringDS.AcceptChanges();
                    #endregion
                }
                else if (clientName == "UCBL")
                {
                    #region For UCBL
                    cmd.Parameters.AddWithValue("p_UserID", row.LoginID);

                    if (!row.IsRemarkNull()) cmd.Parameters.AddWithValue("p_Remark", row.Remark);
                    else cmd.Parameters.AddWithValue("p_Remark", remarks);

                    cmd.Parameters.Add("p_OutputMessage", OleDbType.VarChar, 1000).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("p_LoginDateTime", OleDbType.Date, 1000).Direction = ParameterDirection.Output;
                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_ADD_ATTENDANCE_SIMPLE_LOGIN.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                    }
                    successMsg = cmd.Parameters["p_OutputMessage"].Value.ToString();
                    loginDateTime = cmd.Parameters["p_LoginDateTime"].Value.ToString();
                    stringDS.DataStrings.AddDataString(successMsg);
                    stringDS.DataStrings.AddDataString(loginDateTime);
                    stringDS.AcceptChanges();
                    #endregion
                }

                #region Rony :: Get employee work shift schedule
                cmd.Dispose();
                cmd.Parameters.Clear();
                cmd = DBCommandProvider.GetDBCommand("GetWorkShiftsByEmpCode");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EmployeeCode1"].Value = (object)row.LoginID;
                cmd.Parameters["EmployeeCode2"].Value = (object)row.LoginID;

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                nRowAffected = ADOController.Instance.Fill(adapter, dailyAttendanceDSrpt, dailyAttendanceDSrpt.InRangeShiftsList.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ADD_ATTENDANCE_SIMPLE_LOGIN.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                dailyAttendanceDSrpt.AcceptChanges();
                #endregion
            }
            returnDS.Merge(dailyAttendanceDSrpt);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _create_Attendance_Upload(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            empAttendanceDS EmpDS = new empAttendanceDS();
            EmpDS.Merge(inputDS.Tables[EmpDS.AttendanceMaster.TableName], false, MissingSchemaAction.Error);
            EmpDS.AcceptChanges();

            foreach (empAttendanceDS.AttendanceMasterRow EmpDSRow in EmpDS.AttendanceMaster.Rows)
            {
                if (!EmpDS.AttendanceMaster[0].IsAdditionalShift)
                {
                    //create procedure
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.CommandText = "PRO_ATTENDANCE_UPLOAD_MASTER";
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (cmd == null)
                    {
                        return UtilDL.GetCommandNotFound();
                    }

                    //EMP ID
                    if (!EmpDSRow.IsEmployeeIDNull()) cmd.Parameters.AddWithValue("p_EmployeeID", (object)EmpDSRow.EmployeeID);
                    else cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);

                    //ATT DATE
                    if (!EmpDSRow.IsAtt_DateNull()) cmd.Parameters.AddWithValue("p_Att_Date", (object)EmpDSRow.Att_Date);
                    else cmd.Parameters.AddWithValue("p_Att_Date", DBNull.Value);

                    //ACTUAL LOGIN TIME 
                    if (!EmpDSRow.IsActual_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Act_LoginTime", (object)EmpDSRow.Actual_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Act_LoginTime", DBNull.Value);

                    //ACTUAL LOGOUT TIME
                    if (!EmpDSRow.IsActual_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Act_LogoutTime", (object)EmpDSRow.Actual_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Act_LogoutTime", DBNull.Value);

                    //LOGIN STATUS
                    if (!EmpDSRow.IsLoginStatusNull()) cmd.Parameters.AddWithValue("p_LoginStatus", (object)EmpDSRow.LoginStatus);
                    else cmd.Parameters.AddWithValue("p_LoginStatus", DBNull.Value);

                    //CAL LOGIN TIME
                    if (!EmpDSRow.IsCal_LoginDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LoginTime", (object)EmpDSRow.Cal_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LoginTime", DBNull.Value);

                    //CAL LOGOUT TIME
                    if (!EmpDSRow.IsCal_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LogoutTime", (object)EmpDSRow.Cal_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LogoutTime", DBNull.Value);

                    //UPDATE USER
                    if (!EmpDSRow.IsUpdate_UserNull()) cmd.Parameters.AddWithValue("p_UpdateUser", (object)EmpDSRow.Update_User);
                    else cmd.Parameters.AddWithValue("p_UpdateUser", DBNull.Value);

                    //UPDATE DATE
                    if (!EmpDSRow.IsUpdate_DateNull()) cmd.Parameters.AddWithValue("p_UpdateDate", (object)EmpDSRow.Update_Date);
                    else cmd.Parameters.AddWithValue("p_UpdateDate", DBNull.Value);

                    //EARLY DEPARTURE MIN
                    if (!EmpDSRow.IsEarlyDep_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", (object)EmpDSRow.EarlyDep_Min);
                    else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                    //STANDARD WORKING MIN
                    if (!EmpDSRow.IsStd_Working_MinNull()) cmd.Parameters.AddWithValue("p_Std_WorkingTime", (object)EmpDSRow.Std_Working_Min);
                    else cmd.Parameters.AddWithValue("p_Std_WorkingTime", DBNull.Value);

                    //WORKED MIN
                    if (!EmpDSRow.IsWorked_MinNull()) cmd.Parameters.AddWithValue("p_WorkedTime", (object)EmpDSRow.Worked_Min);
                    else cmd.Parameters.AddWithValue("p_WorkedTime", DBNull.Value);

                    //OVERTIME MIN
                    if (!EmpDSRow.IsOvertime_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", (object)EmpDSRow.Overtime_Min);
                    else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                    //CHANGED SHIFTING ID
                    if (!EmpDSRow.IsChanged_ShiftingIDNull()) cmd.Parameters.AddWithValue("p_ShiftingID", (object)EmpDSRow.Changed_ShiftingID);
                    else cmd.Parameters.AddWithValue("p_ShiftingID", DBNull.Value);

                    //NIGHT SHIFT
                    if (!EmpDSRow.IsIsNight_ShiftNull()) cmd.Parameters.AddWithValue("p_IsNight", (object)EmpDSRow.IsNight_Shift);
                    else cmd.Parameters.AddWithValue("p_IsNight", DBNull.Value);

                    //ATT REMARKS
                    if (!EmpDSRow.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", (object)EmpDSRow.Remarks);
                    else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_UPLOAD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();
                }
                else
                {
                    //create procedure
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.CommandText = "PRO_ATTENDANCE_UPLOAD_ADL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (cmd == null)
                    {
                        return UtilDL.GetCommandNotFound();
                    }

                    //EMP ID
                    if (!EmpDSRow.IsEmployeeIDNull()) cmd.Parameters.AddWithValue("p_EmployeeID", (object)EmpDSRow.EmployeeID);
                    else cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);

                    //ATT DATE
                    if (!EmpDSRow.IsAtt_DateNull()) cmd.Parameters.AddWithValue("p_Att_Date", (object)EmpDSRow.Att_Date);
                    else cmd.Parameters.AddWithValue("p_Att_Date", DBNull.Value);

                    //ACTUAL LOGIN TIME 
                    if (!EmpDSRow.IsActual_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Act_LoginTime", (object)EmpDSRow.Actual_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Act_LoginTime", DBNull.Value);

                    //ACTUAL LOGOUT TIME
                    if (!EmpDSRow.IsActual_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Act_LogoutTime", (object)EmpDSRow.Actual_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Act_LogoutTime", DBNull.Value);

                    //LOGIN STATUS
                    if (!EmpDSRow.IsLoginStatusNull()) cmd.Parameters.AddWithValue("p_LoginStatus", (object)EmpDSRow.LoginStatus);
                    else cmd.Parameters.AddWithValue("p_LoginStatus", DBNull.Value);

                    //CAL LOGIN TIME
                    if (!EmpDSRow.IsCal_LoginDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LoginTime", (object)EmpDSRow.Cal_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LoginTime", DBNull.Value);

                    //CAL LOGOUT TIME
                    if (!EmpDSRow.IsCal_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LogoutTime", (object)EmpDSRow.Cal_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LogoutTime", DBNull.Value);

                    //UPDATE USER
                    if (!EmpDSRow.IsUpdate_UserNull()) cmd.Parameters.AddWithValue("p_UpdateUser", (object)EmpDSRow.Update_User);
                    else cmd.Parameters.AddWithValue("p_UpdateUser", DBNull.Value);

                    //UPDATE DATE
                    if (!EmpDSRow.IsUpdate_DateNull()) cmd.Parameters.AddWithValue("p_UpdateDate", (object)EmpDSRow.Update_Date);
                    else cmd.Parameters.AddWithValue("p_UpdateDate", DBNull.Value);

                    //EARLY DEPARTURE MIN
                    if (!EmpDSRow.IsEarlyDep_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", (object)EmpDSRow.EarlyDep_Min);
                    else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                    //STANDARD WORKING MIN
                    if (!EmpDSRow.IsStd_Working_MinNull()) cmd.Parameters.AddWithValue("p_Std_WorkingTime", (object)EmpDSRow.Std_Working_Min);
                    else cmd.Parameters.AddWithValue("p_Std_WorkingTime", DBNull.Value);

                    //WORKED MIN
                    if (!EmpDSRow.IsWorked_MinNull()) cmd.Parameters.AddWithValue("p_WorkedTime", (object)EmpDSRow.Worked_Min);
                    else cmd.Parameters.AddWithValue("p_WorkedTime", DBNull.Value);

                    //OVERTIME MIN
                    if (!EmpDSRow.IsOvertime_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", (object)EmpDSRow.Overtime_Min);
                    else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                    //CHANGED SHIFTING ID
                    if (!EmpDSRow.IsChanged_ShiftingIDNull()) cmd.Parameters.AddWithValue("p_ShiftingID", (object)EmpDSRow.Changed_ShiftingID);
                    else cmd.Parameters.AddWithValue("p_ShiftingID", DBNull.Value);

                    //NIGHT SHIFT
                    if (!EmpDSRow.IsIsNight_ShiftNull()) cmd.Parameters.AddWithValue("p_IsNight", (object)EmpDSRow.IsNight_Shift);
                    else cmd.Parameters.AddWithValue("p_IsNight", DBNull.Value);

                    //ATT REMARKS
                    if (!EmpDSRow.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", (object)EmpDSRow.Remarks);
                    else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_UPLOAD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _create_Attendance_Parking(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            string SerialNo = "";
            string messageBody = "", returnMessage = "";

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create procedure

            EmployeeHistoryDS empHistoryDS = new EmployeeHistoryDS();
            empHistoryDS.Merge(inputDS.Tables[empHistoryDS.EmailPacket.TableName], false, MissingSchemaAction.Error);
            empHistoryDS.AcceptChanges();

            #region Update Serial No
            OleDbCommand cmdSerialNo = new OleDbCommand();
            cmdSerialNo.CommandText = "PRO_SERIAL_NO_UPDATE";
            cmdSerialNo.CommandType = CommandType.StoredProcedure;

            if (cmdSerialNo == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            bool bErrorSerialNo = false;
            int nRowAffectedSerialNo = -1;
            nRowAffectedSerialNo = ADOController.Instance.ExecuteNonQuery(cmdSerialNo, connDS.DBConnections[0].ConnectionID, ref bErrorSerialNo);

            if (bErrorSerialNo)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Get Serial No
            empAttendanceDS EmpAttDS = new empAttendanceDS();
            OleDbCommand cmdNextSerialNo = new OleDbCommand();
            cmdNextSerialNo = DBCommandProvider.GetDBCommand("GetNextSerialNo");

            if (cmdNextSerialNo == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapterNextSerialNo = new OleDbDataAdapter();
            adapterNextSerialNo.SelectCommand = cmdNextSerialNo;

            bool bErrorNextSerialNo = false;
            int nRowAffectedNextSerialNo = -1;
            nRowAffectedNextSerialNo = ADOController.Instance.Fill(adapterNextSerialNo, EmpAttDS, EmpAttDS.AttendanceBulkParking.TableName, connDS.DBConnections[0].ConnectionID, ref bErrorNextSerialNo);
            if (bErrorNextSerialNo)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            empAttendanceDS.AttendanceBulkParkingRow rowSerialNo = (empAttendanceDS.AttendanceBulkParkingRow)EmpAttDS.AttendanceBulkParking.Rows[0];

            if (rowSerialNo.IsSerialNoNull() == false)
            {
                SerialNo = rowSerialNo.SerialNo.ToString();
            }
            #endregion

            #region Sending Email
            EmployeeHistoryDS.EmailPacketRow emailRow = (EmployeeHistoryDS.EmailPacketRow)empHistoryDS.EmailPacket.Rows[0];

            if (emailRow.IsNotificationByMail)
            {
                returnMessage = "";

                messageBody = "<b>Dispatcher: </b>" + emailRow.MakerName + " [" + emailRow.MakerCode + "]" + "<br><br>";
                messageBody += "<b>Serial No: </b>" + SerialNo;
                messageBody += "<br><br><br>";
                messageBody += "Click the following link: ";
                messageBody += "<br>";
                messageBody += "<a href='" + emailRow.URL + "'>" + emailRow.URL + "</a>";

                Mail.Mail mail = new Mail.Mail();
                string FromEmailAddress = credentialEmailAddress;
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, FromEmailAddress, emailRow.ToEmailAddress, emailRow.CcEmailAddress, emailRow.MailingSubject, messageBody, emailRow.MakerName);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, FromEmailAddress, emailRow.ToEmailAddress, emailRow.CcEmailAddress, emailRow.MailingSubject, messageBody, emailRow.MakerName);
                }

                if (returnMessage != "Email successfully sent.")
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            empAttendanceDS EmpDS = new empAttendanceDS();
            EmpDS.Merge(inputDS.Tables[EmpDS.AttendanceBulkParking.TableName], false, MissingSchemaAction.Error);
            EmpDS.AcceptChanges();

            //ErrorDS.AttendanceErrorsRow success = errDS.AttendanceErrors.NewAttendanceErrorsRow();
            //ErrorDS.AttendanceErrorsRow error = errDS.AttendanceErrors.NewAttendanceErrorsRow();

            foreach (empAttendanceDS.AttendanceBulkParkingRow EmpDSRow in EmpDS.AttendanceBulkParking.Rows)
            {
                ErrorDS.AttendanceErrorsRow success = errDS.AttendanceErrors.NewAttendanceErrorsRow();
                ErrorDS.AttendanceErrorsRow error = errDS.AttendanceErrors.NewAttendanceErrorsRow();
                int EmployeeID = Convert.ToInt32(UtilDL.GetEmployeeId(connDS, EmpDSRow.EmployeeCode));
                EmpDSRow.EmployeeID = EmployeeID;

                if (EmpDSRow.IsAdditionalShift)
                {
                    empAttendanceDS EmpAddDS = new empAttendanceDS();
                    OleDbCommand cmdAdditional = new OleDbCommand();
                    cmdAdditional = DBCommandProvider.GetDBCommand("GetAttendanceAdditionalEmployee");
                    cmdAdditional.Parameters["EmployeeID"].Value = EmployeeID;
                    cmdAdditional.Parameters["Att_Date"].Value = EmpDSRow.Att_Date;
                    cmdAdditional.Parameters["ShiftingID"].Value = EmpDSRow.ShiftingID;

                    if (cmdAdditional == null) return UtilDL.GetCommandNotFound();

                    OleDbDataAdapter adapterAdditional = new OleDbDataAdapter();
                    adapterAdditional.SelectCommand = cmdAdditional;

                    bool bErrorAdditional = false;
                    int nRowAffectedAdditional = -1;
                    nRowAffectedAdditional = ADOController.Instance.Fill(adapterAdditional, EmpAddDS, EmpAddDS.AttendanceMaster.TableName, connDS.DBConnections[0].ConnectionID, ref bErrorAdditional);
                    if (bErrorAdditional)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }

                    if (EmpAddDS.AttendanceMaster.Rows.Count > 0)
                    {
                        OleDbCommand cmd = new OleDbCommand();
                        cmd.CommandText = "PRO_ATTENDANCE_PARKING_CREATE";
                        cmd.CommandType = CommandType.StoredProcedure;

                        if (cmd == null)
                        {
                            return UtilDL.GetCommandNotFound();
                        }

                        //ATT ID
                        long genPK = IDGenerator.GetNextGenericPK();
                        if (genPK == -1)
                        {
                            UtilDL.GetDBOperationFailed();
                        }
                        cmd.Parameters.AddWithValue("p_AttendanceParkingID", (object)genPK);

                        //EMP ID
                        if (!EmpDSRow.IsEmployeeIDNull()) cmd.Parameters.AddWithValue("p_EmployeeID", (object)EmpDSRow.EmployeeID);
                        else cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);

                        //ATT DATE
                        if (!EmpDSRow.IsAtt_DateNull()) cmd.Parameters.AddWithValue("p_Att_Date", (object)EmpDSRow.Att_Date);
                        else cmd.Parameters.AddWithValue("p_Att_Date", DBNull.Value);

                        //ACTUAL LOGIN TIME 
                        if (!EmpDSRow.IsActual_LoginTimeNull()) cmd.Parameters.AddWithValue("p_Act_LoginTime", (object)EmpDSRow.Actual_LoginTime);
                        else cmd.Parameters.AddWithValue("p_Act_LoginTime", DBNull.Value);

                        //ACTUAL LOGOUT TIME
                        if (!EmpDSRow.IsActual_LoginTimeNull()) cmd.Parameters.AddWithValue("p_Act_LogoutTime", (object)EmpDSRow.Actual_LogoutTime);
                        else cmd.Parameters.AddWithValue("p_Act_LogoutTime", DBNull.Value);

                        //CAL LOGIN TIME 
                        if (!EmpDSRow.IsCal_LoginTimeNull()) cmd.Parameters.AddWithValue("p_Cal_LoginTime", (object)EmpDSRow.Actual_LoginTime);
                        else cmd.Parameters.AddWithValue("p_Cal_LoginTime", DBNull.Value);

                        //CAL LOGOUT TIME
                        if (!EmpDSRow.IsCal_LogoutTimeNull()) cmd.Parameters.AddWithValue("p_Cal_LogoutTime", (object)EmpDSRow.Actual_LogoutTime);
                        else cmd.Parameters.AddWithValue("p_Cal_LogoutTime", DBNull.Value);

                        //LOGIN STATUS
                        if (!EmpDSRow.IsLoginStatusNull()) cmd.Parameters.AddWithValue("p_Login_Status", (object)EmpDSRow.LoginStatus);
                        else cmd.Parameters.AddWithValue("p_Login_Status", DBNull.Value);

                        //MAKER
                        if (!EmpDSRow.IsMakerLoginIDNull()) cmd.Parameters.AddWithValue("p_Maker", (object)EmpDSRow.MakerLoginID);
                        else cmd.Parameters.AddWithValue("p_Maker", DBNull.Value);

                        //CHECKER
                        if (!EmpDSRow.IsCheckerLoginIDNull()) cmd.Parameters.AddWithValue("p_Checker", (object)EmpDSRow.CheckerLoginID);
                        else cmd.Parameters.AddWithValue("p_Checker", DBNull.Value);

                        //ENTRY DATE
                        if (!EmpDSRow.IsEntryDateNull()) cmd.Parameters.AddWithValue("p_EntryDate", (object)EmpDSRow.EntryDate);
                        else cmd.Parameters.AddWithValue("p_EntryDate", DBNull.Value);

                        //ATT REMARKS
                        if (!EmpDSRow.IsAtt_RemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", (object)EmpDSRow.Att_Remarks);
                        else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                        //SERIAL NO
                        if (!EmpDSRow.IsSerialNoNull()) cmd.Parameters.AddWithValue("p_SerialNo", EmpDSRow.SerialNo);
                        else cmd.Parameters.AddWithValue("p_SerialNo", SerialNo);

                        //Early Departure
                        if (!EmpDSRow.IsEarlyDep_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", EmpDSRow.EarlyDep_Min);
                        else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                        //Additional Shift
                        if (!EmpDSRow.IsIsAdditionalShiftNull()) cmd.Parameters.AddWithValue("p_IsAdditional", EmpDSRow.IsAdditionalShift);
                        else cmd.Parameters.AddWithValue("p_IsAdditional", DBNull.Value);

                        //Night Shift
                        if (!EmpDSRow.IsIsNightShiftNull()) cmd.Parameters.AddWithValue("p_IsNight", EmpDSRow.IsNightShift);
                        else cmd.Parameters.AddWithValue("p_IsNight", DBNull.Value);

                        //Overtime
                        if (!EmpDSRow.IsOvertime_MinNull()) cmd.Parameters.AddWithValue("p_Overtime", EmpDSRow.Overtime_Min);
                        else cmd.Parameters.AddWithValue("p_Overtime", DBNull.Value);

                        //Shifting ID
                        if (!EmpDSRow.IsShiftingIDNull()) cmd.Parameters.AddWithValue("p_ShiftingID", EmpDSRow.ShiftingID);
                        else cmd.Parameters.AddWithValue("p_ShiftingID", DBNull.Value);

                        //Standard Working Time
                        if (!EmpDSRow.IsStd_Working_MinNull()) cmd.Parameters.AddWithValue("p_Std_WorkingTime", EmpDSRow.Std_Working_Min);
                        else cmd.Parameters.AddWithValue("p_Std_WorkingTime", DBNull.Value);

                        //Worked Time
                        if (!EmpDSRow.IsWorked_MinNull()) cmd.Parameters.AddWithValue("p_WorkedTime", EmpDSRow.Worked_Min);
                        else cmd.Parameters.AddWithValue("p_WorkedTime", DBNull.Value);

                        bool bError = false;
                        int nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        success.Code = "Uploaded Successfully!";
                        success.Info1 = "Employee Code: " + EmpDSRow.EmployeeCode + " Attendance Date: " + EmpDSRow.Att_Date.ToShortDateString() + " Uploaded Successfully!";
                        errDS.AttendanceErrors.AddAttendanceErrorsRow(success);
                        errDS.AcceptChanges();

                        cmd.Parameters.Clear();
                    }
                    else
                    {
                        error.Code = "No Record Found!";
                        error.Info1 = "Employee Code: " + EmpDSRow.EmployeeCode + " Attendance Date: " + EmpDSRow.Att_Date.ToShortDateString() + " No Record Found!";
                        errDS.AttendanceErrors.AddAttendanceErrorsRow(error);
                        errDS.AcceptChanges();
                    }
                }

                else
                {
                    empAttendanceDS EmpMasDS = new empAttendanceDS();
                    OleDbCommand cmdMaster = new OleDbCommand();
                    cmdMaster = DBCommandProvider.GetDBCommand("GetAttendanceMasterEmployee");
                    cmdMaster.Parameters["EmployeeID"].Value = EmployeeID;
                    cmdMaster.Parameters["Att_Date"].Value = EmpDSRow.Att_Date;
                    cmdMaster.Parameters["ShiftingID"].Value = EmpDSRow.ShiftingID;

                    if (cmdMaster == null) return UtilDL.GetCommandNotFound();

                    OleDbDataAdapter adapter = new OleDbDataAdapter();
                    adapter.SelectCommand = cmdMaster;

                    bool bErrorMaster = false;
                    int nRowAffectedMaster = -1;
                    nRowAffectedMaster = ADOController.Instance.Fill(adapter, EmpMasDS, EmpMasDS.AttendanceMaster.TableName, connDS.DBConnections[0].ConnectionID, ref bErrorMaster);
                    if (bErrorMaster)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    if (EmpMasDS.AttendanceMaster.Rows.Count > 0)
                    {
                        OleDbCommand cmd = new OleDbCommand();
                        cmd.CommandText = "PRO_ATTENDANCE_PARKING_CREATE";
                        cmd.CommandType = CommandType.StoredProcedure;

                        if (cmd == null)
                        {
                            return UtilDL.GetCommandNotFound();
                        }

                        //ATT ID
                        long genPK = IDGenerator.GetNextGenericPK();
                        if (genPK == -1)
                        {
                            UtilDL.GetDBOperationFailed();
                        }
                        cmd.Parameters.AddWithValue("p_AttendanceParkingID", (object)genPK);

                        //EMP ID
                        if (!EmpDSRow.IsEmployeeIDNull()) cmd.Parameters.AddWithValue("p_EmployeeID", (object)EmpDSRow.EmployeeID);
                        else cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);

                        //ATT DATE
                        if (!EmpDSRow.IsAtt_DateNull()) cmd.Parameters.AddWithValue("p_Att_Date", (object)EmpDSRow.Att_Date);
                        else cmd.Parameters.AddWithValue("p_Att_Date", DBNull.Value);

                        //ACTUAL LOGIN TIME 
                        if (!EmpDSRow.IsActual_LoginTimeNull()) cmd.Parameters.AddWithValue("p_Act_LoginTime", (object)EmpDSRow.Actual_LoginTime);
                        else cmd.Parameters.AddWithValue("p_Act_LoginTime", DBNull.Value);

                        //ACTUAL LOGOUT TIME
                        if (!EmpDSRow.IsActual_LoginTimeNull()) cmd.Parameters.AddWithValue("p_Act_LogoutTime", (object)EmpDSRow.Actual_LogoutTime);
                        else cmd.Parameters.AddWithValue("p_Act_LogoutTime", DBNull.Value);

                        //CAL LOGIN TIME 
                        if (!EmpDSRow.IsCal_LoginTimeNull()) cmd.Parameters.AddWithValue("p_Cal_LoginTime", (object)EmpDSRow.Actual_LoginTime);
                        else cmd.Parameters.AddWithValue("p_Cal_LoginTime", DBNull.Value);

                        //CAL LOGOUT TIME
                        if (!EmpDSRow.IsCal_LogoutTimeNull()) cmd.Parameters.AddWithValue("p_Cal_LogoutTime", (object)EmpDSRow.Actual_LogoutTime);
                        else cmd.Parameters.AddWithValue("p_Cal_LogoutTime", DBNull.Value);

                        //LOGIN STATUS
                        if (!EmpDSRow.IsLoginStatusNull()) cmd.Parameters.AddWithValue("p_Login_Status", (object)EmpDSRow.LoginStatus);
                        else cmd.Parameters.AddWithValue("p_Login_Status", DBNull.Value);

                        //MAKER
                        if (!EmpDSRow.IsMakerLoginIDNull()) cmd.Parameters.AddWithValue("p_Maker", (object)EmpDSRow.MakerLoginID);
                        else cmd.Parameters.AddWithValue("p_Maker", DBNull.Value);

                        //CHECKER
                        if (!EmpDSRow.IsCheckerLoginIDNull()) cmd.Parameters.AddWithValue("p_Checker", (object)EmpDSRow.CheckerLoginID);
                        else cmd.Parameters.AddWithValue("p_Checker", DBNull.Value);

                        //ENTRY DATE
                        if (!EmpDSRow.IsEntryDateNull()) cmd.Parameters.AddWithValue("p_EntryDate", (object)EmpDSRow.EntryDate);
                        else cmd.Parameters.AddWithValue("p_EntryDate", DBNull.Value);

                        //ATT REMARKS
                        if (!EmpDSRow.IsAtt_RemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", (object)EmpDSRow.Att_Remarks);
                        else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                        //SERIAL NO
                        if (!EmpDSRow.IsSerialNoNull()) cmd.Parameters.AddWithValue("p_SerialNo", EmpDSRow.SerialNo);
                        else cmd.Parameters.AddWithValue("p_SerialNo", SerialNo);

                        //Early Departure
                        if (!EmpDSRow.IsEarlyDep_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", EmpDSRow.EarlyDep_Min);
                        else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                        //Additional Shift
                        if (!EmpDSRow.IsIsAdditionalShiftNull()) cmd.Parameters.AddWithValue("p_IsAdditional", EmpDSRow.IsAdditionalShift);
                        else cmd.Parameters.AddWithValue("p_IsAdditional", DBNull.Value);

                        //Night Shift
                        if (!EmpDSRow.IsIsNightShiftNull()) cmd.Parameters.AddWithValue("p_IsNight", EmpDSRow.IsNightShift);
                        else cmd.Parameters.AddWithValue("p_IsNight", DBNull.Value);

                        //Overtime
                        if (!EmpDSRow.IsOvertime_MinNull()) cmd.Parameters.AddWithValue("p_Overtime", EmpDSRow.Overtime_Min);
                        else cmd.Parameters.AddWithValue("p_Overtime", DBNull.Value);

                        //Shifting ID
                        if (!EmpDSRow.IsShiftingIDNull()) cmd.Parameters.AddWithValue("p_ShiftingID", EmpDSRow.ShiftingID);
                        else cmd.Parameters.AddWithValue("p_ShiftingID", DBNull.Value);

                        //Standard Working Time
                        if (!EmpDSRow.IsStd_Working_MinNull()) cmd.Parameters.AddWithValue("p_Std_WorkingTime", EmpDSRow.Std_Working_Min);
                        else cmd.Parameters.AddWithValue("p_Std_WorkingTime", DBNull.Value);

                        //Worked Time
                        if (!EmpDSRow.IsWorked_MinNull()) cmd.Parameters.AddWithValue("p_WorkedTime", EmpDSRow.Worked_Min);
                        else cmd.Parameters.AddWithValue("p_WorkedTime", DBNull.Value);

                        bool bError = false;
                        int nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                        if (bError)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                            return errDS;
                        }
                        success.Code = "Uploaded Successfully!";
                        success.Info1 = "Employee Code: " + EmpDSRow.EmployeeCode + " Attendance Date: " + EmpDSRow.Att_Date.ToShortDateString() + " Uploaded Successfully!";
                        errDS.AttendanceErrors.AddAttendanceErrorsRow(success);
                        errDS.AcceptChanges();

                        cmd.Parameters.Clear();
                    }
                    else
                    {
                        error.Code = "No Record Found!";
                        error.Info1 = "Employee Code: " + EmpDSRow.EmployeeCode + " Attendance Date: " + EmpDSRow.Att_Date.ToShortDateString() + " No Record Found!";
                        errDS.AttendanceErrors.AddAttendanceErrorsRow(error);
                        errDS.AcceptChanges();
                    }
                }
            }

            //errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getAttendanceParking(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd;
            cmd = DBCommandProvider.GetDBCommand("GetAttendanceParking");
            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            empAttendanceDS attendanceDS = new empAttendanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, attendanceDS, attendanceDS.AttendanceBulkParking.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATTENDANCE_UPLOAD_PARKING.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attendanceDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(attendanceDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getSerialNoList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd;
            cmd = DBCommandProvider.GetDBCommand("GetSerialNo");

            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            empAttendanceDS attDS = new empAttendanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, attDS, attDS.AttendanceBulkParking.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SERIALNO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(attDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteAttendanceParking(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ATTENDANCE_PARK_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_AttendanceParkingID", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_DELETE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }

        private DataSet _getAttendanceRecordForException(DataSet inputDS)
        {
            empAttendanceDS empAttDS = new empAttendanceDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            int shiftingID = 0;

            if (stringDS.DataStrings[6].StringValue != "")
            {
                shiftingID = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
            }

            if (stringDS.DataStrings[0].StringValue == "")
            {
                stringDS.DataStrings[0].StringValue = "-1";
            }


            long siteID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[2].StringValue);

            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetAttendanceRecordForException");

            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["AttendanceDate0"].Value = cmd.Parameters["AttendanceDate1"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["CompanyCode"].Value = cmd.Parameters["CompanyCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SiteID1"].Value = siteID;
            cmd.Parameters["SiteID2"].Value = siteID;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["EmployeeStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
            cmd.Parameters["EmployeeStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue);
            cmd.Parameters["GroupID"].Value = cmd.Parameters["GroupID1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            cmd.Parameters["ShiftingID"].Value = cmd.Parameters["ShiftingID1"].Value = shiftingID;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empAttDS, empAttDS.empAttendance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DAILY_ATTENDANCE_FOR_EXCEPTION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empAttDS.AcceptChanges();

            empAttDS.empAttendance.Merge(empAttDS.empAttendance);
            empAttDS.empAttendance.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empAttDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _create_Attendance_Exception_Parking(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            string SerialNo = "";
            string messageBody = "", returnMessage = "";

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            EmployeeHistoryDS empHistoryDS = new EmployeeHistoryDS();
            empHistoryDS.Merge(inputDS.Tables[empHistoryDS.EmailPacket.TableName], false, MissingSchemaAction.Error);
            empHistoryDS.AcceptChanges();

            #region Update Serial No
            OleDbCommand cmdSerialNo = new OleDbCommand();
            cmdSerialNo.CommandText = "PRO_SERIAL_NO_UPDATE";
            cmdSerialNo.CommandType = CommandType.StoredProcedure;

            if (cmdSerialNo == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            bool bErrorSerialNo = false;
            int nRowAffectedSerialNo = -1;
            nRowAffectedSerialNo = ADOController.Instance.ExecuteNonQuery(cmdSerialNo, connDS.DBConnections[0].ConnectionID, ref bErrorSerialNo);

            if (bErrorSerialNo)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Get Serial No
            empAttendanceDS EmpAttDS = new empAttendanceDS();
            OleDbCommand cmdNextSerialNo = new OleDbCommand();
            cmdNextSerialNo = DBCommandProvider.GetDBCommand("GetNextSerialNo");

            if (cmdNextSerialNo == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapterNextSerialNo = new OleDbDataAdapter();
            adapterNextSerialNo.SelectCommand = cmdNextSerialNo;

            bool bErrorNextSerialNo = false;
            int nRowAffectedNextSerialNo = -1;
            nRowAffectedNextSerialNo = ADOController.Instance.Fill(adapterNextSerialNo, EmpAttDS, EmpAttDS.AttendanceBulkParking.TableName, connDS.DBConnections[0].ConnectionID, ref bErrorNextSerialNo);
            if (bErrorNextSerialNo)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            empAttendanceDS.AttendanceBulkParkingRow rowSerialNo = (empAttendanceDS.AttendanceBulkParkingRow)EmpAttDS.AttendanceBulkParking.Rows[0];

            if (rowSerialNo.IsSerialNoNull() == false)
            {
                SerialNo = rowSerialNo.SerialNo.ToString();
            }
            #endregion

            #region Sending Email
            EmployeeHistoryDS.EmailPacketRow emailRow = (EmployeeHistoryDS.EmailPacketRow)empHistoryDS.EmailPacket.Rows[0];

            if (emailRow.IsNotificationByMail)
            {
                returnMessage = "";

                messageBody = "<b>Dispatcher: </b>" + emailRow.MakerName + " [" + emailRow.MakerCode + "]" + "<br><br>";
                messageBody += "<b>Serial No: </b>" + SerialNo;
                messageBody += "<br><br><br>";
                messageBody += "Click the following link: ";
                messageBody += "<br>";
                messageBody += "<a href='" + emailRow.URL + "'>" + emailRow.URL + "</a>";

                Mail.Mail mail = new Mail.Mail();
                string FromEmailAddress = credentialEmailAddress;
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, FromEmailAddress, emailRow.ToEmailAddress, emailRow.CcEmailAddress, emailRow.MailingSubject, messageBody, emailRow.MakerName);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, FromEmailAddress, emailRow.ToEmailAddress, emailRow.CcEmailAddress, emailRow.MailingSubject, messageBody, emailRow.MakerName);
                }

                if (returnMessage != "Email successfully sent.")
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ATTENDANCE_PARKING_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            empAttendanceDS EmpDS = new empAttendanceDS();
            EmpDS.Merge(inputDS.Tables[EmpDS.AttendanceBulkParking.TableName], false, MissingSchemaAction.Error);
            EmpDS.AcceptChanges();

            foreach (empAttendanceDS.AttendanceBulkParkingRow EmpDSRow in EmpDS.AttendanceBulkParking.Rows)
            {
                ErrorDS.AttendanceErrorsRow success = errDS.AttendanceErrors.NewAttendanceErrorsRow();
                ErrorDS.AttendanceErrorsRow error = errDS.AttendanceErrors.NewAttendanceErrorsRow();

                int EmployeeID = Convert.ToInt32(UtilDL.GetEmployeeId(connDS, EmpDSRow.EmployeeCode));
                EmpDSRow.EmployeeID = EmployeeID;

                OleDbCommand cmd = new OleDbCommand();
                cmd.CommandText = "PRO_ATT_EXCEP_PARK_CREATE";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                //ATT ID
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_AttendanceParkingID", (object)genPK);

                //EMP ID
                if (!EmpDSRow.IsEmployeeIDNull()) cmd.Parameters.AddWithValue("p_EmployeeID", (object)EmpDSRow.EmployeeID);
                else cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);

                //ATT DATE
                if (!EmpDSRow.IsAtt_DateNull()) cmd.Parameters.AddWithValue("p_Att_Date", (object)EmpDSRow.Att_Date);
                else cmd.Parameters.AddWithValue("p_Att_Date", DBNull.Value);

                //ACTUAL LOGIN TIME 
                if (!EmpDSRow.IsActual_LoginTimeNull()) cmd.Parameters.AddWithValue("p_Act_LoginTime", (object)EmpDSRow.Actual_LoginTime);
                else cmd.Parameters.AddWithValue("p_Act_LoginTime", DBNull.Value);

                //ACTUAL LOGOUT TIME
                if (!EmpDSRow.IsActual_LoginTimeNull()) cmd.Parameters.AddWithValue("p_Act_LogoutTime", (object)EmpDSRow.Actual_LogoutTime);
                else cmd.Parameters.AddWithValue("p_Act_LogoutTime", DBNull.Value);

                //CAL LOGIN TIME 
                if (!EmpDSRow.IsCal_LoginTimeNull()) cmd.Parameters.AddWithValue("p_Cal_LoginTime", (object)EmpDSRow.Actual_LoginTime);
                else cmd.Parameters.AddWithValue("p_Cal_LoginTime", DBNull.Value);

                //CAL LOGOUT TIME
                if (!EmpDSRow.IsCal_LogoutTimeNull()) cmd.Parameters.AddWithValue("p_Cal_LogoutTime", (object)EmpDSRow.Actual_LogoutTime);
                else cmd.Parameters.AddWithValue("p_Cal_LogoutTime", DBNull.Value);

                //LOGIN STATUS
                if (!EmpDSRow.IsLoginStatusNull()) cmd.Parameters.AddWithValue("p_Login_Status", (object)EmpDSRow.LoginStatus);
                else cmd.Parameters.AddWithValue("p_Login_Status", DBNull.Value);

                //MAKER
                if (!EmpDSRow.IsMakerLoginIDNull()) cmd.Parameters.AddWithValue("p_Maker", (object)EmpDSRow.MakerLoginID);
                else cmd.Parameters.AddWithValue("p_Maker", DBNull.Value);

                //CHECKER
                if (!EmpDSRow.IsCheckerLoginIDNull()) cmd.Parameters.AddWithValue("p_Checker", (object)EmpDSRow.CheckerLoginID);
                else cmd.Parameters.AddWithValue("p_Checker", DBNull.Value);

                //ENTRY DATE
                if (!EmpDSRow.IsEntryDateNull()) cmd.Parameters.AddWithValue("p_EntryDate", (object)EmpDSRow.EntryDate);
                else cmd.Parameters.AddWithValue("p_EntryDate", DBNull.Value);

                //ATT REMARKS
                if (!EmpDSRow.IsAtt_RemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", (object)EmpDSRow.Att_Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                //SERIAL NO
                if (!EmpDSRow.IsSerialNoNull()) cmd.Parameters.AddWithValue("p_SerialNo", EmpDSRow.SerialNo);
                else cmd.Parameters.AddWithValue("p_SerialNo", SerialNo);

                //Early Departure
                if (!EmpDSRow.IsEarlyDep_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", EmpDSRow.EarlyDep_Min);
                else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                //Additional Shift
                if (!EmpDSRow.IsIsAdditionalShiftNull()) cmd.Parameters.AddWithValue("p_IsAdditional", EmpDSRow.IsAdditionalShift);
                else cmd.Parameters.AddWithValue("p_IsAdditional", DBNull.Value);

                //Night Shift
                if (!EmpDSRow.IsIsNightShiftNull()) cmd.Parameters.AddWithValue("p_IsNight", EmpDSRow.IsNightShift);
                else cmd.Parameters.AddWithValue("p_IsNight", DBNull.Value);

                //Overtime
                if (!EmpDSRow.IsOvertime_MinNull()) cmd.Parameters.AddWithValue("p_Overtime", EmpDSRow.Overtime_Min);
                else cmd.Parameters.AddWithValue("p_Overtime", DBNull.Value);

                //Shifting ID
                if (!EmpDSRow.IsShiftingIDNull()) cmd.Parameters.AddWithValue("p_ShiftingID", EmpDSRow.ShiftingID);
                else cmd.Parameters.AddWithValue("p_ShiftingID", DBNull.Value);

                //Standard Working Time
                if (!EmpDSRow.IsStd_Working_MinNull()) cmd.Parameters.AddWithValue("p_Std_WorkingTime", EmpDSRow.Std_Working_Min);
                else cmd.Parameters.AddWithValue("p_Std_WorkingTime", DBNull.Value);

                //Worked Time
                if (!EmpDSRow.IsWorked_MinNull()) cmd.Parameters.AddWithValue("p_WorkedTime", EmpDSRow.Worked_Min);
                else cmd.Parameters.AddWithValue("p_WorkedTime", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EXCEPTIONAL_ATTENDANCE_PARKING_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                success.Code = "Uploaded Successfully!";
                success.Info1 = "Employee Code: " + EmpDSRow.EmployeeCode + " Attendance Date: " + EmpDSRow.Att_Date.ToShortDateString() + " Uploaded Successfully!";
                errDS.AttendanceErrors.AddAttendanceErrorsRow(success);
                errDS.AcceptChanges();
                cmd.Parameters.Clear();
            }
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getAttendanceExceptionParking(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd;
            cmd = DBCommandProvider.GetDBCommand("GetAttendanceExceptionsParking");
            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            empAttendanceDS attendanceDS = new empAttendanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, attendanceDS, attendanceDS.AttendanceBulkParking.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EXCEPTIONAL_ATTENDANCE_PARKING.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attendanceDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(attendanceDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _update_Attendance_Exception(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            empAttendanceDS EmpDS = new empAttendanceDS();
            EmpDS.Merge(inputDS.Tables[EmpDS.AttendanceMaster.TableName], false, MissingSchemaAction.Error);
            EmpDS.AcceptChanges();

            foreach (empAttendanceDS.AttendanceMasterRow EmpDSRow in EmpDS.AttendanceMaster.Rows)
            {
                if (!EmpDS.AttendanceMaster[0].IsAdditionalShift)
                {
                    //create procedure
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.CommandText = "PRO_ATT_EXCEP_UPD_MASTER";
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (cmd == null)
                    {
                        return UtilDL.GetCommandNotFound();
                    }

                    //EMP ID
                    if (!EmpDSRow.IsEmployeeIDNull()) cmd.Parameters.AddWithValue("p_EmployeeID", (object)EmpDSRow.EmployeeID);
                    else cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);

                    //ATT DATE
                    if (!EmpDSRow.IsAtt_DateNull()) cmd.Parameters.AddWithValue("p_Att_Date", (object)EmpDSRow.Att_Date);
                    else cmd.Parameters.AddWithValue("p_Att_Date", DBNull.Value);

                    //ACTUAL LOGIN TIME 
                    if (!EmpDSRow.IsActual_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Act_LoginTime", (object)EmpDSRow.Actual_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Act_LoginTime", DBNull.Value);

                    //ACTUAL LOGOUT TIME
                    if (!EmpDSRow.IsActual_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Act_LogoutTime", (object)EmpDSRow.Actual_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Act_LogoutTime", DBNull.Value);

                    //LOGIN STATUS
                    if (!EmpDSRow.IsLoginStatusNull()) cmd.Parameters.AddWithValue("p_LoginStatus", (object)EmpDSRow.LoginStatus);
                    else cmd.Parameters.AddWithValue("p_LoginStatus", DBNull.Value);

                    //CAL LOGIN TIME
                    if (!EmpDSRow.IsCal_LoginDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LoginTime", (object)EmpDSRow.Cal_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LoginTime", DBNull.Value);

                    //CAL LOGOUT TIME
                    if (!EmpDSRow.IsCal_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LogoutTime", (object)EmpDSRow.Cal_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LogoutTime", DBNull.Value);

                    //UPDATE USER
                    if (!EmpDSRow.IsUpdate_UserNull()) cmd.Parameters.AddWithValue("p_UpdateUser", (object)EmpDSRow.Update_User);
                    else cmd.Parameters.AddWithValue("p_UpdateUser", DBNull.Value);

                    //UPDATE DATE
                    if (!EmpDSRow.IsUpdate_DateNull()) cmd.Parameters.AddWithValue("p_UpdateDate", (object)EmpDSRow.Update_Date);
                    else cmd.Parameters.AddWithValue("p_UpdateDate", DBNull.Value);

                    //EARLY DEPARTURE MIN
                    if (!EmpDSRow.IsEarlyDep_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", (object)EmpDSRow.EarlyDep_Min);
                    else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                    //STANDARD WORKING MIN
                    if (!EmpDSRow.IsStd_Working_MinNull()) cmd.Parameters.AddWithValue("p_Std_WorkingTime", (object)EmpDSRow.Std_Working_Min);
                    else cmd.Parameters.AddWithValue("p_Std_WorkingTime", DBNull.Value);

                    //WORKED MIN
                    if (!EmpDSRow.IsWorked_MinNull()) cmd.Parameters.AddWithValue("p_WorkedTime", (object)EmpDSRow.Worked_Min);
                    else cmd.Parameters.AddWithValue("p_WorkedTime", DBNull.Value);

                    //OVERTIME MIN
                    if (!EmpDSRow.IsOvertime_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", (object)EmpDSRow.Overtime_Min);
                    else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                    //CHANGED SHIFTING ID
                    if (!EmpDSRow.IsChanged_ShiftingIDNull()) cmd.Parameters.AddWithValue("p_ShiftingID", (object)EmpDSRow.Changed_ShiftingID);
                    else cmd.Parameters.AddWithValue("p_ShiftingID", DBNull.Value);

                    //NIGHT SHIFT
                    if (!EmpDSRow.IsIsNight_ShiftNull()) cmd.Parameters.AddWithValue("p_IsNight", (object)EmpDSRow.IsNight_Shift);
                    else cmd.Parameters.AddWithValue("p_IsNight", DBNull.Value);

                    //ATT REMARKS
                    if (!EmpDSRow.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", (object)EmpDSRow.Remarks);
                    else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EXCEPTIONAL_ATTENDANCE_UPDATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();
                }
                else
                {
                    //create procedure
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.CommandText = "PRO_ATT_EXCEP_UPD_ADL";
                    cmd.CommandType = CommandType.StoredProcedure;

                    if (cmd == null)
                    {
                        return UtilDL.GetCommandNotFound();
                    }

                    //EMP ID
                    if (!EmpDSRow.IsEmployeeIDNull()) cmd.Parameters.AddWithValue("p_EmployeeID", (object)EmpDSRow.EmployeeID);
                    else cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);

                    //ATT DATE
                    if (!EmpDSRow.IsAtt_DateNull()) cmd.Parameters.AddWithValue("p_Att_Date", (object)EmpDSRow.Att_Date);
                    else cmd.Parameters.AddWithValue("p_Att_Date", DBNull.Value);

                    //ACTUAL LOGIN TIME 
                    if (!EmpDSRow.IsActual_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Act_LoginTime", (object)EmpDSRow.Actual_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Act_LoginTime", DBNull.Value);

                    //ACTUAL LOGOUT TIME
                    if (!EmpDSRow.IsActual_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Act_LogoutTime", (object)EmpDSRow.Actual_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Act_LogoutTime", DBNull.Value);

                    //LOGIN STATUS
                    if (!EmpDSRow.IsLoginStatusNull()) cmd.Parameters.AddWithValue("p_LoginStatus", (object)EmpDSRow.LoginStatus);
                    else cmd.Parameters.AddWithValue("p_LoginStatus", DBNull.Value);

                    //CAL LOGIN TIME
                    if (!EmpDSRow.IsCal_LoginDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LoginTime", (object)EmpDSRow.Cal_LoginDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LoginTime", DBNull.Value);

                    //CAL LOGOUT TIME
                    if (!EmpDSRow.IsCal_LogoutDatetimeNull()) cmd.Parameters.AddWithValue("p_Cal_LogoutTime", (object)EmpDSRow.Cal_LogoutDatetime);
                    else cmd.Parameters.AddWithValue("p_Cal_LogoutTime", DBNull.Value);

                    //UPDATE USER
                    if (!EmpDSRow.IsUpdate_UserNull()) cmd.Parameters.AddWithValue("p_UpdateUser", (object)EmpDSRow.Update_User);
                    else cmd.Parameters.AddWithValue("p_UpdateUser", DBNull.Value);

                    //UPDATE DATE
                    if (!EmpDSRow.IsUpdate_DateNull()) cmd.Parameters.AddWithValue("p_UpdateDate", (object)EmpDSRow.Update_Date);
                    else cmd.Parameters.AddWithValue("p_UpdateDate", DBNull.Value);

                    //EARLY DEPARTURE MIN
                    if (!EmpDSRow.IsEarlyDep_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", (object)EmpDSRow.EarlyDep_Min);
                    else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                    //STANDARD WORKING MIN
                    if (!EmpDSRow.IsStd_Working_MinNull()) cmd.Parameters.AddWithValue("p_Std_WorkingTime", (object)EmpDSRow.Std_Working_Min);
                    else cmd.Parameters.AddWithValue("p_Std_WorkingTime", DBNull.Value);

                    //WORKED MIN
                    if (!EmpDSRow.IsWorked_MinNull()) cmd.Parameters.AddWithValue("p_WorkedTime", (object)EmpDSRow.Worked_Min);
                    else cmd.Parameters.AddWithValue("p_WorkedTime", DBNull.Value);

                    //OVERTIME MIN
                    if (!EmpDSRow.IsOvertime_MinNull()) cmd.Parameters.AddWithValue("p_EarlyDep_Min", (object)EmpDSRow.Overtime_Min);
                    else cmd.Parameters.AddWithValue("p_EarlyDep_Min", DBNull.Value);

                    //CHANGED SHIFTING ID
                    if (!EmpDSRow.IsChanged_ShiftingIDNull()) cmd.Parameters.AddWithValue("p_ShiftingID", (object)EmpDSRow.Changed_ShiftingID);
                    else cmd.Parameters.AddWithValue("p_ShiftingID", DBNull.Value);

                    //NIGHT SHIFT
                    if (!EmpDSRow.IsIsNight_ShiftNull()) cmd.Parameters.AddWithValue("p_IsNight", (object)EmpDSRow.IsNight_Shift);
                    else cmd.Parameters.AddWithValue("p_IsNight", DBNull.Value);

                    //ATT REMARKS
                    if (!EmpDSRow.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", (object)EmpDSRow.Remarks);
                    else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_EXCEPTIONAL_ATTENDANCE_UPDATE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                    cmd.Parameters.Clear();
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getSerialNo(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd;
            cmd = DBCommandProvider.GetDBCommand("GetAttExcepSerialNo");

            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            empAttendanceDS attDS = new empAttendanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, attDS, attDS.AttendanceBulkParking.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PARKING_SERIALNO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(attDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _deleteAttendanceParkingException(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ATT_EXCEP_PARK_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_AttendanceParkingID", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_EXCEPTION_ATTENDANCE_PARKING_DELETE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }
        private DataSet _getAttDeviceData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd;
            cmd = DBCommandProvider.GetDBCommand("GetAttDeviceDataForSpam");

            if (cmd == null)
            {
                return UtilDL.GetDBOperationFailed();
            }
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["ToDate"].Value = dateDS.DataDates[1].DateValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            empAttendanceDS attDS = new empAttendanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, attDS, attDS.empAttendance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ATT_DEVICE_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            attDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(attDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
    }
}
