﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;


namespace Sylvia.DAL.Payroll
{
    class ScaleOfPayDL
    {

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_SCALE_OF_PAY_ADD:
                    return _createScaleOfPay(inputDS);
                case ActionID.NA_ACTION_GET_GRADEWISE_SLAB_LIST:
                    return _getGradeWisePayScale(inputDS);
                case ActionID.ACTION_GRADE_WISE_SLAB_DEL:
                    return _deleteGradeScale(inputDS);
                case ActionID.NA_ACTION_GET_PAY_FIXATION_INFO:
                    return this._getPendingPayFixationData(inputDS);
                case ActionID.ACTION_APPROVE_PAY_FIXATION:
                    return this._approvePayFixation(inputDS);
                case ActionID.NA_ACTION_GET_PAY_FIXATION:
                    return this._getPayFixation(inputDS);
                case ActionID.ACTION_CREATE_PAY_FIXATION_PARKING:
                    return this._createPayFixationParking(inputDS);
                case ActionID.ACTION_REJECT_PAY_FIXATION:
                    return this._rejectPayFixation(inputDS);

                case ActionID.NA_ACTION_GET_LOCALPOSTING_DATA:
                    return this._getLocalPostingData(inputDS);
                case ActionID.ACTION_CREATE_LOCALPOSTING:
                    return this._createLocalPosting(inputDS);

                case ActionID.ACTION_CREATE_PROFIT_POSTING_PARKING:
                    return this._createProfitPosting(inputDS);
                case ActionID.ACTION_APPROVE_PROFIT_POSTING:
                    return this._approveProfitPosting(inputDS);
                case ActionID.NA_ACTION_GET_PROFIT:
                    return this._getProfit(inputDS);
                case ActionID.NA_ACTION_GET_PENDING_PROFIT_POSTING:
                    return this._getPendingProfitPosting(inputDS);

                case ActionID.NA_ACTION_GET_APPROVED_PAY_FIXATION_INFO:
                    return this._getApprovedPayFixationData(inputDS);
                case ActionID.NA_ACTION_GET_ALLOWANCE_DATA_FOR_PAY_FIXATION:
                    return this._getAllowanceDataForPayFixation(inputDS);

                case ActionID.ACTION_OTHERS_BENEFIT_ADD:
                    return this._createOthersBenefit(inputDS);
                case ActionID.ACTION_OTHERS_BENEFIT_UPD:
                    return this._updateOthersBenefit(inputDS);
                case ActionID.ACTION_OTHERS_BENEFIT_DEL:
                    return this._deleteOthersBenefit(inputDS);
                case ActionID.NA_ACTION_GET_OTHERS_BENEFIT:
                    return this._getOthersBenefit(inputDS);

                case ActionID.NA_ACTION_GET_PAY_FIXATION_ASSESS_EMPLOYEE:
                    return this._getPayFixationForAssessmentEmployee(inputDS);

                case ActionID.NA_ACTION_GET_BONUSPOSTING_DATA:
                    return this._getBonusPostingData(inputDS);
                case ActionID.ACTION_CREATE_BONUSPOSTING:
                    return this._createBonusPosting(inputDS);

                case ActionID.NA_ACTION_GET_DISBUSMENT_DATE:
                    return this.getDisbursmentDateList(inputDS);
                case ActionID.NA_ACTION_GET_BONUS_DATA:
                    return this._getBonusData(inputDS);

                case ActionID.ACTION_UPDATE_BONUSPOSTING:
                    return this._updateBonusPosting(inputDS);
                case ActionID.ACTION_DELETE_BONUSPOSTING:
                    return this._deleteBonusPosting(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }

        private DataSet _createScaleOfPay(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ScaleOfPayDS paramDS = new ScaleOfPayDS();
            DBConnectionDS connDS = new DBConnectionDS();
            bool bError;
            int nRowAffected;

            paramDS.Merge(inputDS.Tables[paramDS.ScaleOfPay.TableName], false, MissingSchemaAction.Error);
            paramDS.Merge(inputDS.Tables[paramDS.AllowDeducts.TableName], false, MissingSchemaAction.Error);
            paramDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region First Delete Old Records....

            foreach (ScaleOfPayDS.ScaleOfPayRow row in paramDS.ScaleOfPay.Rows)
            {
                OleDbCommand cmd_del = DBCommandProvider.GetDBCommand("DeleteGradeWiseSlab");
                if (cmd_del == null) return UtilDL.GetCommandNotFound();
                cmd_del.Parameters["GradeId"].Value = UtilDL.GetGradeId(connDS, row.GradeCode);
                cmd_del.Parameters["EffectiveDate"].Value = row.Effectdate;

                nRowAffected = -1;
                bError = false;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ITPARAMETER_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


                OleDbCommand cmd_Allow = new OleDbCommand();
                cmd_Allow.CommandText = "PRO_DELETE_ALLOW_SCALE_OF_PAY";
                cmd_Allow.CommandType = CommandType.StoredProcedure;

                cmd_Allow.Parameters.AddWithValue("p_GradeCode", row.GradeCode);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Allow, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SCALE_OF_PAY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


                break;          // deletes all previous records from DB then exits.
            }
            #endregion

            #region Insert new Records.........
            foreach (ScaleOfPayDS.ScaleOfPayRow payScaleRow in paramDS.ScaleOfPay.Rows)
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateScaleOfPay");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["GradeId"].Value = UtilDL.GetGradeId(connDS, payScaleRow.GradeCode);
                cmd.Parameters["GradeId2"].Value = UtilDL.GetGradeId(connDS, payScaleRow.GradeCode);
                cmd.Parameters["EffectiveDate"].Value = payScaleRow.Effectdate;
                cmd.Parameters["EffectiveDate2"].Value = payScaleRow.Effectdate;
                cmd.Parameters["Basic"].Value = payScaleRow.Basic;
                cmd.Parameters["IncrementAmount"].Value = payScaleRow.Incrementamount;
                cmd.Parameters["No_Increment"].Value = payScaleRow.No_Increment;
                if (payScaleRow.IsIncreasedBasic_PrctNull() == false)
                {
                    cmd.Parameters["IncreasedBasic_Prct"].Value = payScaleRow.IncreasedBasic_Prct;
                }
                else
                {
                    cmd.Parameters["IncreasedBasic_Prct"].Value = System.DBNull.Value;
                }
                cmd.Parameters["FullScale"].Value = payScaleRow.FullScale;

                #region Shakir 31.08.16
                if (!payScaleRow.IsIncreasedGrossSalary_PrctNull()) cmd.Parameters["IncreasedGrossSalary_Prct"].Value = payScaleRow.IncreasedGrossSalary_Prct;
                else cmd.Parameters["IncreasedGrossSalary_Prct"].Value = System.DBNull.Value;
                #endregion

                nRowAffected = -1;
                bError = false;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SCALE_OF_PAY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                #region Update Grade according to PayScale :: WALI :: 15-Mar-2015
                int rowCount = paramDS.ScaleOfPay.Rows.Count;
                OleDbCommand cmd_Grade = new OleDbCommand();
                cmd_Grade.CommandText = "PRO_UPDATE_GRADE_ON_PAYSCALE";
                cmd_Grade.CommandType = CommandType.StoredProcedure;

                if (cmd_Grade == null) return UtilDL.GetCommandNotFound();

                cmd_Grade.Parameters.AddWithValue("p_MinBasicSalary", paramDS.ScaleOfPay[0].Basic);
                cmd_Grade.Parameters.AddWithValue("p_MaxBasicSalary", paramDS.ScaleOfPay[rowCount - 1].Basic);
                cmd_Grade.Parameters.AddWithValue("p_GradeCode", paramDS.ScaleOfPay[0].GradeCode);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Grade, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SCALE_OF_PAY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion
            }
            #endregion

            #region Shakir



            //Insert Allowance grade wise
            foreach (ScaleOfPayDS.AllowDeduct AllowanceRow in paramDS.AllowDeducts.Rows)
            {
                OleDbCommand cmd_Allow = new OleDbCommand();
                cmd_Allow.CommandText = "PRO_INSERT_ALLOW_SCALE_OF_PAY";
                cmd_Allow.CommandType = CommandType.StoredProcedure;

                cmd_Allow.Parameters.AddWithValue("p_GradeCode", AllowanceRow.GradeCode);
                cmd_Allow.Parameters.AddWithValue("p_ADID", AllowanceRow.ADID);
                cmd_Allow.Parameters.AddWithValue("p_Amount", AllowanceRow.Amount);
                cmd_Allow.Parameters.AddWithValue("p_FlatAmount", AllowanceRow.FlatAmount);
                cmd_Allow.Parameters.AddWithValue("p_PercentageOfBasic", AllowanceRow.PercentageOfBasic);
                cmd_Allow.Parameters.AddWithValue("p_AppliedStat", AllowanceRow.AppliedStat);
                cmd_Allow.Parameters.AddWithValue("p_IsTaxable", AllowanceRow.IsTaxable);
                cmd_Allow.Parameters.AddWithValue("p_IsRegularTax", AllowanceRow.IsRegularTax);
                cmd_Allow.Parameters.AddWithValue("p_IsPoratedPayment", AllowanceRow.IsPoratedPayment);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Allow, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SCALE_OF_PAY_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getSlabList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            ScaleOfPayDS slabDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetScaleOfPayList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["GradeCode"].Value = stringDS.DataStrings[0].StringValue;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, slabDS, slabDS.ScaleOfPay.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ITSLAB_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            slabDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(slabDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getGradeWisePayScale(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS StringDS = new DataStringDS();
            ScaleOfPayDS slabDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            long gradeid = UtilDL.GetGradeId(connDS, StringDS.DataStrings[0].StringValue);

            cmd = DBCommandProvider.GetDBCommand("GetGradeWiseSlabList");
            cmd.Parameters["GradeId"].Value = gradeid;
            cmd.Parameters["GradeId2"].Value = gradeid;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, slabDS, slabDS.ScaleOfPay.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRADEWISE_SLAB_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            slabDS.AcceptChanges();


            #region Shakir 31.08.2016
            cmd.Parameters.Clear();
            cmd.Dispose();

            cmd = DBCommandProvider.GetDBCommand("GetGradeWiseAllowForScaleOfPay");
            cmd.Parameters["GradeId"].Value = gradeid;

            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, slabDS, slabDS.AllowDeducts.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRADEWISE_SLAB_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            slabDS.AcceptChanges();
            #endregion


            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(slabDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteGradeScale(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            long gradeid = UtilDL.GetGradeId(connDS, stringDS.DataStrings[0].StringValue);

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteGradeWiseSlab");
            cmd.Parameters["EffectiveDate"].Value = dateDS.DataDates[0].DateValue;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["GradeId"].Value = gradeid;
                cmd.Parameters["EffectiveDate"].Value = dateDS.DataDates[0].DateValue;
            }
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ITPARAMETER_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            return errDS;
        }
        private DataSet _getPendingPayFixationData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataIntegerDS integerDS = new DataIntegerDS();
            ScaleOfPayDS payFixationDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            int userID = integerDS.DataIntegers[0].IntegerValue;

            cmd = DBCommandProvider.GetDBCommand("GetPendingPayFixationData_By_UserID");
            cmd.Parameters["Checker1"].Value = userID;
            cmd.Parameters["Checker2"].Value = userID;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, payFixationDS, payFixationDS.PayFixation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PAY_FIXATION_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            payFixationDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(payFixationDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _approvePayFixation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ScaleOfPayDS payFixationDS = new ScaleOfPayDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            payFixationDS.Merge(inputDS.Tables[payFixationDS.PayFixation.TableName], false, MissingSchemaAction.Error);
            payFixationDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_PAY_FIXATION_APPROVE";
            cmd.CommandType = CommandType.StoredProcedure;

          

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ScaleOfPayDS.PayFixationRow row in payFixationDS.PayFixation.Rows)
            {
                bool bError = false;
                int nRowAffected = -1;

                #region Approve Pay Fixation
                cmd.Parameters.AddWithValue("p_PayFixationParkingID", row.PayFixationParkingID);
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                cmd.Parameters.AddWithValue("p_EntryDate", row.EntryDate);
                cmd.Parameters.AddWithValue("p_CompanyID", row.CompanyID);
                cmd.Parameters.AddWithValue("p_LocationID", row.LocationID);
                //cmd.Parameters.AddWithValue("p_GradeID", row.GradeID);            // Update :: WALI :: 29-Dec-2014
                cmd.Parameters.AddWithValue("p_GradeID", row.ProposedGradeID);
                cmd.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                //cmd.Parameters.AddWithValue("p_DesignationID", row.DesignationID); // Update :: WALI :: 29-Dec-2014
                cmd.Parameters.AddWithValue("p_DesignationID", row.ProposedDesignationID);
                cmd.Parameters.AddWithValue("p_CostCenterID", row.CostCenterID);
                cmd.Parameters.AddWithValue("p_EventCode", row.EventCode);
                cmd.Parameters.AddWithValue("p_Comments", row.Comments);
                cmd.Parameters.AddWithValue("p_ArrearStatus", row.ArrearStatus);
                cmd.Parameters.AddWithValue("p_IsGetArrear", row.IsGetArrear);
                cmd.Parameters.AddWithValue("p_PerIncrement", row.PerIncrement);
                cmd.Parameters.AddWithValue("p_ProposedBasic", row.ProposedBasic);
                cmd.Parameters.AddWithValue("p_No_Increment", row.NO_Increment);
                cmd.Parameters.AddWithValue("p_EffectDate", row.EffectDate);
                cmd.Parameters.AddWithValue("p_Status", row.Status);
                if (!row.IsNextIncrementDateNull())
                    cmd.Parameters.AddWithValue("p_NextIncrementDate", row.NextIncrementDate);
                else cmd.Parameters.AddWithValue("p_NextIncrementDate", System.DBNull.Value);

                if (row.IsLast_Update_UserNull() == false) cmd.Parameters.AddWithValue("P_Last_Update_User", row.Last_Update_User);
                else cmd.Parameters.AddWithValue("P_Last_Update_User", System.DBNull.Value);

                cmd.Parameters.AddWithValue("p_UpdateNextIncrementDate", row.UpdateNextIncrementDate);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_APPROVE_PAY_FIXATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }

                //if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                //else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                #endregion

                #region Create Additional Job History Row [For Promotion Only]  :: WALI :: 28-Dec-2014

                //OleDbCommand cmd_Upd = new OleDbCommand();
                //cmd_Upd.CommandText = "PRO_INCREMENT_ON_PROMOTION";
                //cmd_Upd.CommandType = CommandType.StoredProcedure;
                //if (cmd_Upd == null) return UtilDL.GetCommandNotFound();

                //if (row.EventCode == 11)
                //{
                //    cmd_Upd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                //    cmd_Upd.Parameters.AddWithValue("p_EffectDate", row.EffectDate);
                //    cmd_Upd.Parameters.AddWithValue("p_EntryDate", row.EntryDate);
                //    cmd_Upd.Parameters.AddWithValue("p_CompanyID", row.CompanyID);
                //    cmd_Upd.Parameters.AddWithValue("p_LocationID", row.LocationID);
                //    cmd_Upd.Parameters.AddWithValue("p_GradeID", row.ProposedGradeID);
                //    cmd_Upd.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                //    cmd_Upd.Parameters.AddWithValue("p_DesignationID", row.ProposedDesignationID);
                //    cmd_Upd.Parameters.AddWithValue("p_CostCenterID", row.CostCenterID);
                //    cmd_Upd.Parameters.AddWithValue("p_ProposedBasic", row.ProposedBasic);
                //    cmd_Upd.Parameters.AddWithValue("p_EventCode", Convert.ToInt32(EventCode.INCREMENT));
                //    cmd_Upd.Parameters.AddWithValue("p_Comments", row.Comments);
                //    cmd_Upd.Parameters.AddWithValue("p_ArrearStatus", row.ArrearStatus);
                //    cmd_Upd.Parameters.AddWithValue("p_IsGetArrear", row.IsGetArrear);
                //    cmd_Upd.Parameters.AddWithValue("p_No_Increment", row.NO_Increment);
                //    cmd_Upd.Parameters.AddWithValue("p_PerIncrement", row.PerIncrement);

                //    if (row.IsLast_Update_UserNull() == false) cmd_Upd.Parameters.AddWithValue("P_Last_Update_User", row.Last_Update_User);
                //    else cmd_Upd.Parameters.AddWithValue("P_Last_Update_User", System.DBNull.Value); 

                //    bError = false;
                //    nRowAffected = -1;
                //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Upd, connDS.DBConnections[0].ConnectionID, ref bError);
                //    cmd_Upd.Parameters.Clear();
                //    if (bError == true)
                //    {
                //        ErrorDS.Error err = errDS.Errors.NewError();
                //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //        err.ErrorInfo1 = ActionID.ACTION_APPROVE_PAY_FIXATION.ToString();
                //        errDS.Errors.AddError(err);
                //        errDS.AcceptChanges();
                //    }
                //}
                #endregion

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _getPayFixation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            DataSet returnDS = new DataSet();
            DateTime EffectDate_PS = new DateTime(1754, 1, 1);
            Int32 GradeID = 0, DesignationID = 0, PayrollSiteID = 0;
            bool bError = false;
            int nRowAffected = -1;

            #region Merge Input DS...
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            #endregion

            #region Get ID's from DataBase
            GradeID = Convert.ToInt32(UtilDL.GetGradeId(connDS, stringDS.DataStrings[1].StringValue));
            DesignationID = Convert.ToInt32(UtilDL.GetDesignationId(connDS, stringDS.DataStrings[2].StringValue));
            PayrollSiteID = Convert.ToInt32(UtilDL.GetPayrollSiteId(connDS, stringDS.DataStrings[3].StringValue));
            #endregion

            #region Select DB Command...
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();


            if (integerDS.DataIntegers[3].IntegerValue == 3)
            {
                #region Change Pay Scale...
                #region Get Current Pay Scale Date...
                cmd = DBCommandProvider.GetDBCommand("GetCurrentPayScale");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["ProcessDate"].Value = dateDS.DataDates[0].DateValue;
                adapter.SelectCommand = cmd;

                ScaleOfPayDS payScaleDS = new ScaleOfPayDS();

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, payScaleDS, payScaleDS.ScaleOfPay.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_GET_PAY_FIXATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;

                }
                payScaleDS.AcceptChanges();

                foreach (ScaleOfPayDS.ScaleOfPayRow row in payScaleDS.ScaleOfPay.Rows)
                {
                    EffectDate_PS = row.Effectdate;
                    break;
                }

                cmd.Parameters.Clear();
                #endregion

                cmd = DBCommandProvider.GetDBCommand("GetPayFixation_ChangePS");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["ProcessDate"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["EffectDate"].Value = EffectDate_PS;
                cmd.Parameters["ProcessDate1"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["EffectDate1"].Value = EffectDate_PS;

                cmd.Parameters["EventCode"].Value = Convert.ToInt32(EventCode.INCREMENT);
                cmd.Parameters["EventCode1"].Value = Convert.ToInt32(EventCode.INCREMENT);

                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;

                cmd.Parameters["LiabilityID"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["LiabilityID1"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["DepartmentID1"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["SiteID1"].Value = integerDS.DataIntegers[2].IntegerValue;

                cmd.Parameters["GradeID"].Value = GradeID;
                cmd.Parameters["GradeID1"].Value = GradeID;
                cmd.Parameters["DesignationID"].Value = DesignationID;
                cmd.Parameters["DesignationID1"].Value = DesignationID;
                cmd.Parameters["PayrollSiteID"].Value = PayrollSiteID;
                cmd.Parameters["PayrollSiteID1"].Value = PayrollSiteID;

                cmd.Parameters["LocationID1"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["LocationID2"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["EmployeeType1"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["EmployeeType2"].Value = integerDS.DataIntegers[5].IntegerValue;

                #endregion
            }
            else if (integerDS.DataIntegers[3].IntegerValue == 2)
            {
                #region Increment...
                cmd = DBCommandProvider.GetDBCommand("GetPayFixation_Increment");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["ToDate"].Value = dateDS.DataDates[1].DateValue;
                cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["ToDate1"].Value = dateDS.DataDates[1].DateValue;

                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;

                cmd.Parameters["LiabilityID"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["LiabilityID1"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["DepartmentID1"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["SiteID1"].Value = integerDS.DataIntegers[2].IntegerValue;

                cmd.Parameters["GradeID"].Value = GradeID;
                cmd.Parameters["GradeID1"].Value = GradeID;
                cmd.Parameters["DesignationID"].Value = DesignationID;
                cmd.Parameters["DesignationID1"].Value = DesignationID;
                cmd.Parameters["PayrollSiteID"].Value = PayrollSiteID;
                cmd.Parameters["PayrollSiteID1"].Value = PayrollSiteID;


                cmd.Parameters["LocationID1"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["LocationID2"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["EmployeeType1"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["EmployeeType2"].Value = integerDS.DataIntegers[5].IntegerValue;

                cmd.Parameters["EventCode"].Value = 2;
                cmd.Parameters["JoinEC"].Value = Convert.ToInt32(EventCode.JOIN);
                cmd.Parameters["JoinEC1"].Value = Convert.ToInt32(EventCode.JOIN);
                cmd.Parameters["PromotionEC"].Value = Convert.ToInt32(EventCode.PROMOTION);
                cmd.Parameters["FromDate1"].Value = dateDS.DataDates[0].DateValue;
                
                #endregion
            }
            else if (integerDS.DataIntegers[3].IntegerValue == 1)
            {
                #region Promotion...
                cmd = DBCommandProvider.GetDBCommand("GetPayFixation_Promotion");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["EffectDate"].Value = dateDS.DataDates[1].DateValue.ToString("MM/dd/yyyy");
                cmd.Parameters["EventCode"].Value = Convert.ToInt32(EventCode.PROMOTION);
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["LiabilityID"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["GradeID"].Value = GradeID;
                cmd.Parameters["DesignationID"].Value = DesignationID;
                cmd.Parameters["PayrollSiteID"].Value = PayrollSiteID;
                cmd.Parameters["TenableYear"].Value = Convert.ToDecimal(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["LocationID1"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["LocationID2"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["EmployeeType1"].Value = integerDS.DataIntegers[5].IntegerValue;
                cmd.Parameters["EmployeeType2"].Value = integerDS.DataIntegers[5].IntegerValue;
                
              
                #endregion
            }
            #endregion

            adapter.SelectCommand = cmd;

            ScaleOfPayDS payFixationDS = new ScaleOfPayDS();

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, payFixationDS, payFixationDS.PayFixation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PAY_FIXATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            payFixationDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(payFixationDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createPayFixationParking(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ScaleOfPayDS payFixationDS = new ScaleOfPayDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            bool sendMail = false;
            string messageBody = "", returnMessage = "";
            int senderUserID = 0, recipientUserID = 0;
            string senderEmpName = "", senderEmpCode = "";
            string fromEmailAddress = "", fromEmailPassword = "", emailAddress = "";
            string mailingSubject = "", ccEmailAddress = "", mailFor = "";
            #endregion

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            payFixationDS.Merge(inputDS.Tables[payFixationDS.PayFixation.TableName], false, MissingSchemaAction.Error);
            payFixationDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_PAY_FIXATION_SUBMIT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            #region Verifier Mail Informations ....

            foreach (ScaleOfPayDS.PayFixationRow rowMail in payFixationDS.PayFixation.Rows)
            {
                if (rowMail.IsNotificationByMail == true)
                {
                    //activityID = rowMail.ActivitiesID;  // Get activityID to check whether Leave is APPROVED or RECOMENDED.

                    sendMail = true;
                    senderUserID = rowMail.Maker;
                    senderEmpName = rowMail.SenderEmpName;
                    senderEmpCode = rowMail.SenderEmpCode;
                    fromEmailAddress = rowMail.SenderEmailAddress;        // Email address of Maker user
                    fromEmailPassword = rowMail.EmailPassword;
                    recipientUserID = rowMail.Checker;
                    emailAddress = rowMail.VerifierEmailAddress;        // Email address of Verifier user
                    mailFor = rowMail.MailingSubject;
                    mailingSubject = "Notification to check " + rowMail.MailingSubject;
                    break;
                }
                else break;
            }
            #endregion

            if (sendMail)
            {
                #region Save data after sending MAIL

                #region Send Email to Verifier
                Mail.Mail mail = new Mail.Mail();

                messageBody = "<b>You are requested to process " + mailFor + "." + "<br>";
                messageBody += "Please check the system for details." + "<br>";

                messageBody += "<b>Notified by : </b>" + senderEmpName + " [" + senderEmpCode + "]" + "<br><br>";
                messageBody += "<br><br><br>";
                returnMessage = "";

                fromEmailAddress = credentialEmailAddress;
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, senderEmpName);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, fromEmailAddress, emailAddress, ccEmailAddress, mailingSubject, messageBody, senderEmpName);
                }
                #endregion

                if (returnMessage == "Email successfully sent.")
                {
                    #region Save Data to DB
                    foreach (ScaleOfPayDS.PayFixationRow row in payFixationDS.PayFixation.Rows)
                    {
                        cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                        cmd.Parameters.AddWithValue("p_EffectDate", row.EffectDate);
                        cmd.Parameters.AddWithValue("p_EntryDate", row.EntryDate);
                        cmd.Parameters.AddWithValue("p_CompanyID", row.CompanyID);
                        cmd.Parameters.AddWithValue("p_LocationID", row.LocationID);
                        cmd.Parameters.AddWithValue("p_GradeID", row.GradeID);
                        cmd.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                        cmd.Parameters.AddWithValue("p_DesignationID", row.DesignationID);
                        cmd.Parameters.AddWithValue("p_CostCenterID", row.CostCenterID);
                        cmd.Parameters.AddWithValue("p_BasicSalary", row.BasicSalary);
                        cmd.Parameters.AddWithValue("p_EventCode", row.EventCode);
                        cmd.Parameters.AddWithValue("p_Comments", row.Comments);
                        cmd.Parameters.AddWithValue("p_ArrearStatus", row.ArrearStatus);
                        cmd.Parameters.AddWithValue("p_IsGetArrear", row.IsGetArrear);
                        cmd.Parameters.AddWithValue("p_Status", row.Status);
                        cmd.Parameters.AddWithValue("p_Maker", row.Maker);
                        cmd.Parameters.AddWithValue("p_Checker", row.Checker);
                        cmd.Parameters.AddWithValue("p_PerIncrement", row.PerIncrement);
                        cmd.Parameters.AddWithValue("p_No_Increment", row.NO_Increment);
                        cmd.Parameters.AddWithValue("p_FunctionID", row.FunctionID);
                        cmd.Parameters.AddWithValue("p_SiteID", row.SiteID);
                        cmd.Parameters.AddWithValue("p_ScaleOfPay", row.ScaleOfPay);
                        cmd.Parameters.AddWithValue("p_ProposedScaleOfPay", row.ProposedScaleOfPay);
                        cmd.Parameters.AddWithValue("p_ProposedBasic", row.ProposedBasic);
                        cmd.Parameters.AddWithValue("p_IncreasedBasic_Prct", row.IncreasedBasic_Prct);

                        // SerialNo Inserted by a trigger

                        #region Update :: WALI :: 16-Mar-2014
                        cmd.Parameters.AddWithValue("p_SenderEmailAddress", row.SenderEmailAddress);
                        #endregion

                        if (!row.IsNextIncrementDateNull())
                            cmd.Parameters.AddWithValue("p_NextIncrementDate", row.NextIncrementDate);
                        else cmd.Parameters.AddWithValue("p_NextIncrementDate", System.DBNull.Value);

                        #region Update :: WALI :: 24-Dec-2014
                        cmd.Parameters.AddWithValue("p_ProposedGradeID", row.ProposedGradeID);
                        cmd.Parameters.AddWithValue("p_ProposedDesignationID", row.ProposedDesignationID);

                        if (!row.IsPromotedAgeNull()) cmd.Parameters.AddWithValue("p_PromotedAge", row.PromotedAge);
                        else cmd.Parameters.AddWithValue("p_PromotedAge", DBNull.Value);

                        if (!row.IsLast_PromotedDateNull()) cmd.Parameters.AddWithValue("p_Last_PromotedDate", row.Last_PromotedDate);
                        else cmd.Parameters.AddWithValue("p_Last_PromotedDate", DBNull.Value);

                        if (!row.IsLastBasicNull()) cmd.Parameters.AddWithValue("p_LastBasic", row.LastBasic);
                        else cmd.Parameters.AddWithValue("p_LastBasic", DBNull.Value);
                        #endregion

                        // Update :: WALI :: 08-Jan-2015
                        if (!row.IsPromotionDueDateNull()) cmd.Parameters.AddWithValue("p_PromotionDueDate", row.PromotionDueDate);
                        else cmd.Parameters.AddWithValue("p_PromotionDueDate", DBNull.Value);

                        #region Update :: WALI :: 14-Jan-2015
                        if (!row.IsMin_ScaleBasicNull()) cmd.Parameters.AddWithValue("p_Min_ScaleBasic", row.Min_ScaleBasic);
                        else cmd.Parameters.AddWithValue("p_Min_ScaleBasic", DBNull.Value);

                        if (!row.IsMax_ScaleBasicNull()) cmd.Parameters.AddWithValue("p_Max_ScaleBasic", row.Max_ScaleBasic);
                        else cmd.Parameters.AddWithValue("p_Max_ScaleBasic", DBNull.Value);

                        if (!row.IsES_EffectDateNull()) cmd.Parameters.AddWithValue("p_ES_EffectDate", row.ES_EffectDate);
                        else cmd.Parameters.AddWithValue("p_ES_EffectDate", DBNull.Value);
                        #endregion

                        cmd.Parameters.AddWithValue("p_UpdateNextIncrementDate", row.UpdateNextIncrementDate);

                        bool bError = false;
                        int nRowAffected = -1;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                        if (bError == true)
                        {
                            ErrorDS.Error err = errDS.Errors.NewError();
                            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                            err.ErrorInfo1 = ActionID.ACTION_CREATE_PAY_FIXATION_PARKING.ToString();
                            errDS.Errors.AddError(err);
                            errDS.AcceptChanges();
                        }
                        cmd.Parameters.Clear();

                        if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                        else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                    }
                    #endregion
                }
                else    // Email sending FAILED !
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CREATE_PAY_FIXATION_PARKING.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }
                #endregion
            }
            else
            {
                #region Save data without sending MAIL
                foreach (ScaleOfPayDS.PayFixationRow row in payFixationDS.PayFixation.Rows)
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_EffectDate", row.EffectDate);
                    cmd.Parameters.AddWithValue("p_EntryDate", row.EntryDate);
                    cmd.Parameters.AddWithValue("p_CompanyID", row.CompanyID);
                    cmd.Parameters.AddWithValue("p_LocationID", row.LocationID);
                    cmd.Parameters.AddWithValue("p_GradeID", row.GradeID);
                    cmd.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                    cmd.Parameters.AddWithValue("p_DesignationID", row.DesignationID);
                    cmd.Parameters.AddWithValue("p_CostCenterID", row.CostCenterID);
                    cmd.Parameters.AddWithValue("p_BasicSalary", row.BasicSalary);
                    cmd.Parameters.AddWithValue("p_EventCode", row.EventCode);
                    cmd.Parameters.AddWithValue("p_Comments", row.Comments);
                    cmd.Parameters.AddWithValue("p_ArrearStatus", row.ArrearStatus);
                    cmd.Parameters.AddWithValue("p_IsGetArrear", row.IsGetArrear);
                    cmd.Parameters.AddWithValue("p_Status", row.Status);
                    cmd.Parameters.AddWithValue("p_Maker", row.Maker);
                    cmd.Parameters.AddWithValue("p_Checker", row.Checker);
                    cmd.Parameters.AddWithValue("p_PerIncrement", row.PerIncrement);
                    cmd.Parameters.AddWithValue("p_No_Increment", row.NO_Increment);
                    cmd.Parameters.AddWithValue("p_FunctionID", row.FunctionID);
                    cmd.Parameters.AddWithValue("p_SiteID", row.SiteID);
                    cmd.Parameters.AddWithValue("p_ScaleOfPay", row.ScaleOfPay);
                    cmd.Parameters.AddWithValue("p_ProposedScaleOfPay", row.ProposedScaleOfPay);
                    cmd.Parameters.AddWithValue("p_ProposedBasic", row.ProposedBasic);
                    cmd.Parameters.AddWithValue("p_IncreasedBasic_Prct", row.IncreasedBasic_Prct);

                    // SerialNo Inserted by a trigger

                    #region Update :: WALI :: 16-Mar-2014
                    cmd.Parameters.AddWithValue("p_SenderEmailAddress", row.SenderEmailAddress);
                    #endregion

                    if (!row.IsNextIncrementDateNull())
                        cmd.Parameters.AddWithValue("p_NextIncrementDate", row.NextIncrementDate);
                    else cmd.Parameters.AddWithValue("p_NextIncrementDate", System.DBNull.Value);

                    #region Update :: WALI :: 24-Dec-2014
                    cmd.Parameters.AddWithValue("p_ProposedGradeID", row.ProposedGradeID);
                    cmd.Parameters.AddWithValue("p_ProposedDesignationID", row.ProposedDesignationID);

                    if (!row.IsPromotedAgeNull()) cmd.Parameters.AddWithValue("p_PromotedAge", row.PromotedAge);
                    else cmd.Parameters.AddWithValue("p_PromotedAge", DBNull.Value);

                    if (!row.IsLast_PromotedDateNull()) cmd.Parameters.AddWithValue("p_Last_PromotedDate", row.Last_PromotedDate);
                    else cmd.Parameters.AddWithValue("p_Last_PromotedDate", DBNull.Value);

                    if (!row.IsLastBasicNull()) cmd.Parameters.AddWithValue("p_LastBasic", row.LastBasic);
                    else cmd.Parameters.AddWithValue("p_LastBasic", DBNull.Value);
                    #endregion

                    // Update :: WALI :: 08-Jan-2015
                    if (!row.IsPromotionDueDateNull()) cmd.Parameters.AddWithValue("p_PromotionDueDate", row.PromotionDueDate);
                    else cmd.Parameters.AddWithValue("p_PromotionDueDate", DBNull.Value);

                    #region Update :: WALI :: 14-Jan-2015
                    if (!row.IsMin_ScaleBasicNull()) cmd.Parameters.AddWithValue("p_Min_ScaleBasic", row.Min_ScaleBasic);
                    else cmd.Parameters.AddWithValue("p_Min_ScaleBasic", DBNull.Value);

                    if (!row.IsMax_ScaleBasicNull()) cmd.Parameters.AddWithValue("p_Max_ScaleBasic", row.Max_ScaleBasic);
                    else cmd.Parameters.AddWithValue("p_Max_ScaleBasic", DBNull.Value);

                    if (!row.IsES_EffectDateNull()) cmd.Parameters.AddWithValue("p_ES_EffectDate", row.ES_EffectDate);
                    else cmd.Parameters.AddWithValue("p_ES_EffectDate", DBNull.Value);
                    #endregion

                    cmd.Parameters.AddWithValue("p_UpdateNextIncrementDate", row.UpdateNextIncrementDate);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_CREATE_PAY_FIXATION_PARKING.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                    }
                    cmd.Parameters.Clear();

                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                }
                #endregion
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _rejectPayFixation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ScaleOfPayDS payFixationDS = new ScaleOfPayDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            payFixationDS.Merge(inputDS.Tables[payFixationDS.PayFixation.TableName], false, MissingSchemaAction.Error);
            payFixationDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_PAY_FIXATION_REJECT";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ScaleOfPayDS.PayFixationRow row in payFixationDS.PayFixation.Rows)
            {
                cmd.Parameters.AddWithValue("p_PayFixationParkingID", row.PayFixationParkingID);
                bool bError = false;

                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REJECT_PAY_FIXATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _getLocalPostingData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            #region Get ID's from DataBase
            Int32 PayrollSiteID = 0;
            PayrollSiteID = Convert.ToInt32(UtilDL.GetPayrollSiteId(connDS, stringDS.DataStrings[1].StringValue));
            #endregion

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            if (Convert.ToBoolean(stringDS.DataStrings[6].StringValue)) cmd = DBCommandProvider.GetDBCommand("GetLocalPostingDataWithoutSeparatedEmployee");
            else cmd = DBCommandProvider.GetDBCommand("GetLocalPostingData");
            cmd.Parameters["HeadCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["HeadCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["MonthYearDate"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["MonthYearDate_1"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["MonthYearDate_2"].Value = dateDS.DataDates[0].DateValue;

            cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["SiteID1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = PayrollSiteID;
            cmd.Parameters["PayrollSiteID1"].Value = PayrollSiteID;

            cmd.Parameters["Gender"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["Gender1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["Religion"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["Religion1"].Value = stringDS.DataStrings[3].StringValue;

            cmd.Parameters["GradeCode"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["GradeCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["EmployeeType"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            cmd.Parameters["EmployeeType1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);


            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, scaleOfPayDS, scaleOfPayDS.LocalPosting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LOCALPOSTING_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                //return errDS;
            }

            scaleOfPayDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(scaleOfPayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createLocalPosting(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            scaleOfPayDS.Merge(inputDS.Tables[scaleOfPayDS.LocalPosting.TableName], false, MissingSchemaAction.Error);
            scaleOfPayDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_LOCAL_POSTING_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ScaleOfPayDS.LocalPostingRow row in scaleOfPayDS.LocalPosting.Rows)
            {
                cmd.Parameters.AddWithValue("p_SalaryID", row.SalaryId);
                cmd.Parameters.AddWithValue("p_HeadID", row.HeadId);
                cmd.Parameters.AddWithValue("p_HeadCode", row.HeadCode);
                cmd.Parameters.AddWithValue("p_HeadType", row.HeadType);
                cmd.Parameters.AddWithValue("p_Description", row.Description);
                cmd.Parameters.AddWithValue("p_Position", row.Position);
                cmd.Parameters.AddWithValue("p_CalculatedAmount", row.CalculatedAmount);
                cmd.Parameters.AddWithValue("p_ChangedAmount", row.ChangedAmount);
                cmd.Parameters.AddWithValue("p_IdealAmount", row.IdealAmount);
                cmd.Parameters.AddWithValue("p_LoginID", row.LoginID);
                cmd.Parameters.AddWithValue("p_EmployeeId", row.EmployeeId);
                cmd.Parameters.AddWithValue("p_MonthYearDate", row.MonthYearDate);
                cmd.Parameters.AddWithValue("p_IsSalaryExcluded", row.IsSalaryExcludedLoan);
                //cmd.Parameters.AddWithValue("p_IsSuspended", row.IsSuspended);
                //cmd.Parameters.AddWithValue("p_SuspendedAmount", row.SuspendedAmount);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CREATE_PAY_FIXATION_PARKING.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }

        private DataSet _createProfitPosting(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            scaleOfPayDS.Merge(inputDS.Tables[scaleOfPayDS.ProfitPosting.TableName], false, MissingSchemaAction.Error);
            scaleOfPayDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_PROFIT_POSTING_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ScaleOfPayDS.ProfitPostingRow row in scaleOfPayDS.ProfitPosting.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeId);
                cmd.Parameters.AddWithValue("p_SalaryMonth", row.SalaryMonth);
                cmd.Parameters.AddWithValue("p_Status", row.Applied);   //  0 for APPLIED
                cmd.Parameters.AddWithValue("p_Maker", row.Maker);
                cmd.Parameters.AddWithValue("p_Checker", row.Checker);
                //cmd.Parameters.AddWithValue("p_SenderEmailAddress", row.SenderEmailAddress);
                cmd.Parameters.AddWithValue("p_ProfitType", row.ProfitType);
                cmd.Parameters.AddWithValue("p_ProfitRate_Prct", row.ProfitRate_Prct);
                cmd.Parameters.AddWithValue("p_CumulativeAmount", row.CumulativeAmount);
                cmd.Parameters.AddWithValue("p_ProposedProfit", row.ProposedProfit);
                cmd.Parameters.AddWithValue("p_Outstanding", row.OutStanding);

                cmd.Parameters.AddWithValue("p_SalaryID", row.SalaryId);
                cmd.Parameters.AddWithValue("p_HeadCode", row.HeadCode);
                cmd.Parameters.AddWithValue("p_HeadType", row.HeadType);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CREATE_PROFIT_POSTING_PARKING.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _approveProfitPosting(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            string Description = "";

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            scaleOfPayDS.Merge(inputDS.Tables[scaleOfPayDS.ProfitPosting.TableName], false, MissingSchemaAction.Error);
            scaleOfPayDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_PROFIT_POSTING_APPROVE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            #region Get ID's from DataBase...
            foreach (ScaleOfPayDS.ProfitPostingRow AllowRow in scaleOfPayDS.ProfitPosting.Rows)
            {
                Description = UtilDL.GetAllowDeductName(connDS, AllowRow.HeadCode);
                break;
            }
            #endregion

            foreach (ScaleOfPayDS.ProfitPostingRow row in scaleOfPayDS.ProfitPosting.Rows)
            {
                cmd.Parameters.AddWithValue("p_ProfitPostingParkingID", row.ProfitPostingParkingID);
                cmd.Parameters.AddWithValue("p_Status", true);
                cmd.Parameters.AddWithValue("p_SalaryID", row.SalaryId);
                cmd.Parameters.AddWithValue("p_HeadID", row.HeadId);
                cmd.Parameters.AddWithValue("p_HeadCode", row.HeadCode);
                cmd.Parameters.AddWithValue("p_HeadType", row.HeadType);
                cmd.Parameters.AddWithValue("p_Description", Description);
                cmd.Parameters.AddWithValue("p_Position", row.Position);
                cmd.Parameters.AddWithValue("p_CalculatedAmount", row.ProposedProfit);
                cmd.Parameters.AddWithValue("p_ChangedAmount", row.ProposedProfit);
                cmd.Parameters.AddWithValue("p_IdealAmount", row.ProposedProfit);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_APPROVE_PROFIT_POSTING.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _getProfit(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            Int32 GradeID = 0, DesignationID = 0, PayrollSiteID = 0;
            bool bError = false;
            int nRowAffected = -1;

            #region Merge Input DS...
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            #endregion

            #region Get ID's from DataBase
            GradeID = Convert.ToInt32(UtilDL.GetGradeId(connDS, stringDS.DataStrings[1].StringValue));
            DesignationID = Convert.ToInt32(UtilDL.GetDesignationId(connDS, stringDS.DataStrings[2].StringValue));
            PayrollSiteID = Convert.ToInt32(UtilDL.GetPayrollSiteId(connDS, stringDS.DataStrings[3].StringValue));
            #endregion

            #region Select DB Command...
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();


            if (integerDS.DataIntegers[4].IntegerValue == 1)
            {
                #region PF Profit...
                cmd = DBCommandProvider.GetDBCommand("GetPFProfit");
                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }

                cmd.Parameters["ExcludeOutStanding"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["ProfitRate"].Value = Convert.ToDecimal(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["ProfitRate_Flat"].Value = Convert.ToDecimal(stringDS.DataStrings[5].StringValue);
                cmd.Parameters["ProfitRate1"].Value = Convert.ToDecimal(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["ProfitRate_Flat1"].Value = Convert.ToDecimal(stringDS.DataStrings[5].StringValue);

                cmd.Parameters["SalaryDate"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["SalaryDate1"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["SalaryDate2"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["SalaryDate3"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["SalaryDate4"].Value = dateDS.DataDates[0].DateValue;

                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;

                cmd.Parameters["LiabilityID"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["LiabilityID1"].Value = integerDS.DataIntegers[0].IntegerValue;
                cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["DepartmentID1"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["SiteID1"].Value = integerDS.DataIntegers[2].IntegerValue;

                cmd.Parameters["GradeID"].Value = GradeID;
                cmd.Parameters["GradeID1"].Value = GradeID;
                cmd.Parameters["DesignationID"].Value = DesignationID;
                cmd.Parameters["DesignationID1"].Value = DesignationID;
                cmd.Parameters["PayrollSiteID"].Value = PayrollSiteID;
                cmd.Parameters["PayrollSiteID1"].Value = PayrollSiteID;
                #endregion
            }
            else if (integerDS.DataIntegers[4].IntegerValue == 2)
            {
                #region Gratuity...
                // Code need to write here 
                #endregion
            }
            #endregion

            adapter.SelectCommand = cmd;

            ScaleOfPayDS payFixationDS = new ScaleOfPayDS();

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, payFixationDS, payFixationDS.ProfitPosting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PROFIT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            payFixationDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(payFixationDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _getPendingProfitPosting(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataIntegerDS integerDS = new DataIntegerDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetPendingProfitPosting");
            cmd.Parameters["Checker"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["Checker1"].Value = integerDS.DataIntegers[0].IntegerValue;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, scaleOfPayDS, scaleOfPayDS.ProfitPosting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PENDING_PROFIT_POSTING.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                //return errDS;
            }

            scaleOfPayDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(scaleOfPayDS.ProfitPosting);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getApprovedPayFixationData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataIntegerDS integerDS = new DataIntegerDS();
            ScaleOfPayDS payFixationDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();
            DataBoolDS boolDS = new DataBoolDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            // if boolDS.DataBools[0].BoolValue, means only Employees who got Fixation while Promotion.
            if (boolDS.DataBools[0].BoolValue) cmd = DBCommandProvider.GetDBCommand("GetApprovedPayFixationData_WithFixationNeeded");
            else cmd = DBCommandProvider.GetDBCommand("GetApprovedPayFixationData");

            cmd.Parameters["JoinEC"].Value = Convert.ToInt32(EventCode.JOIN);
            cmd.Parameters["PromotionEC"].Value = Convert.ToInt32(EventCode.PROMOTION);
            cmd.Parameters["EventCode"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["Status"].Value = true;
            cmd.Parameters["Year"].Value = integerDS.DataIntegers[1].IntegerValue;
            cmd.Parameters["Month"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["Month2"].Value = integerDS.DataIntegers[2].IntegerValue;
            cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[3].IntegerValue;
            cmd.Parameters["SiteID2"].Value = integerDS.DataIntegers[3].IntegerValue;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, payFixationDS, payFixationDS.PayFixation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PAY_FIXATION_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            payFixationDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(payFixationDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getAllowanceDataForPayFixation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS stringDS = new DataStringDS();
            ScaleOfPayDS payFixationDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAllowanceDataForPayFixation");
            cmd.Parameters["EventCode"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["Year"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["Month"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["Month2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, payFixationDS, payFixationDS.ProfitPosting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PAY_FIXATION_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            payFixationDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(payFixationDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        #region WALI :: 12-Jun-2014
        private DataSet _createOthersBenefit(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            scaleOfPayDS.Merge(inputDS.Tables[scaleOfPayDS.OthersBenefit.TableName], false, MissingSchemaAction.Error);
            scaleOfPayDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_OTHERS_BENEFIT_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ScaleOfPayDS.OthersBenefitRow row in scaleOfPayDS.OthersBenefit.Rows)
            {
                // OthersBenefitID is Inserted by a trigger
                cmd.Parameters.AddWithValue("p_OthersBenefitCode", row.OthersBenefitCode);
                cmd.Parameters.AddWithValue("p_OthersBenefitName", row.OthersBenefitName);
                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OTHERS_BENEFIT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.OthersBenefitCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.OthersBenefitCode);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _getOthersBenefit(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetAllOthersBenefit");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, scaleOfPayDS, scaleOfPayDS.OthersBenefit.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_OTHERS_BENEFIT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            scaleOfPayDS.AcceptChanges();

            returnDS.Merge(scaleOfPayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateOthersBenefit(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_OTHERS_BENEFIT_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            scaleOfPayDS.Merge(inputDS.Tables[scaleOfPayDS.OthersBenefit.TableName], false, MissingSchemaAction.Error);
            scaleOfPayDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (ScaleOfPayDS.OthersBenefitRow row in scaleOfPayDS.OthersBenefit.Rows)
            {
                cmd.Parameters.AddWithValue("p_OthersBenefitName", row.OthersBenefitName);
                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("p_OthersBenefitCode", row.OthersBenefitCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OTHERS_BENEFIT_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.OthersBenefitCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.OthersBenefitCode);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(scaleOfPayDS);
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteOthersBenefit(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_OTHERS_BENEFIT_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            scaleOfPayDS.Merge(inputDS.Tables[scaleOfPayDS.OthersBenefit.TableName], false, MissingSchemaAction.Error);
            scaleOfPayDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (ScaleOfPayDS.OthersBenefitRow row in scaleOfPayDS.OthersBenefit.Rows)
            {
                cmd.Parameters.AddWithValue("p_OthersBenefitCode", row.OthersBenefitCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OTHERS_BENEFIT_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.OthersBenefitCode);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.OthersBenefitCode);
            }
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        // WALI :: 15-Mar-2015
        private DataSet _getPayFixationForAssessmentEmployee(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataLongDS longDS = new DataLongDS();
            DataSet returnDS = new DataSet();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            DateTime EffectDate_PS = new DateTime(1754, 1, 1);
            bool bError = false;
            int nRowAffected = -1;

            #region Merge Input DS...
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            #endregion

            #region Get Bulk Promotion Data...
            cmd = DBCommandProvider.GetDBCommand("GetBulkPromotionProcess");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["EffectDate"].Value = dateDS.DataDates[1].DateValue.ToString("MM/dd/yyyy");
            cmd.Parameters["EventCode"].Value = Convert.ToInt32(EventCode.PROMOTION);
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

            adapter.SelectCommand = cmd;
            ScaleOfPayDS payFixationDS = new ScaleOfPayDS();

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, payFixationDS, payFixationDS.PayFixation.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PAY_FIXATION_ASSESS_EMPLOYEE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            payFixationDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(payFixationDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getBonusPostingData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS stringDS = new DataStringDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            if (!Convert.ToBoolean(stringDS.DataStrings[20].StringValue.Trim()))
            {
                cmd = DBCommandProvider.GetDBCommand("GetBonusPostingData");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["ADCode"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["DisburseDate"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue.Trim());
                cmd.Parameters["PaymentMonth"].Value = Convert.ToDateTime(stringDS.DataStrings[17].StringValue.Trim());
                cmd.Parameters["EmployeeType"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue.Trim());
                cmd.Parameters["SiteID"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue.Trim());
                cmd.Parameters["CompDivisionID"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue.Trim());
                cmd.Parameters["DepartmentID"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue.Trim());
                cmd.Parameters["LiabilityID"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue.Trim());
                cmd.Parameters["GradeID"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue.Trim());
                cmd.Parameters["Religion"].Value = stringDS.DataStrings[8].StringValue.Trim();
                cmd.Parameters["Gender"].Value = stringDS.DataStrings[9].StringValue.Trim();
                cmd.Parameters["Considerable_LWP"].Value = Convert.ToInt32(stringDS.DataStrings[10].StringValue.Trim());
                cmd.Parameters["ElqID"].Value = Convert.ToInt32(stringDS.DataStrings[14].StringValue.Trim());
                cmd.Parameters["ServiceLength_Day"].Value = Convert.ToInt32(stringDS.DataStrings[11].StringValue.Trim());
                cmd.Parameters["LateralExperience"].Value = Convert.ToDecimal(stringDS.DataStrings[12].StringValue.Trim());
                cmd.Parameters["EmpStatus"].Value = stringDS.DataStrings[13].StringValue;
                cmd.Parameters["PayExclude_From"].Value = Convert.ToDateTime(stringDS.DataStrings[15].StringValue.Trim());
                cmd.Parameters["PayExclude_To"].Value = Convert.ToDateTime(stringDS.DataStrings[16].StringValue.Trim());
                cmd.Parameters["SalaryMonth"].Value = Convert.ToDateTime(stringDS.DataStrings[18].StringValue.Trim());
                cmd.Parameters["Eligible_Only"].Value = Convert.ToBoolean(stringDS.DataStrings[19].StringValue.Trim());
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetBonusPostingDataRoundOff");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["ADCode"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["DisburseDate"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue.Trim());
                cmd.Parameters["PaymentMonth"].Value = Convert.ToDateTime(stringDS.DataStrings[17].StringValue.Trim());
                cmd.Parameters["EmployeeType"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue.Trim());
                cmd.Parameters["SiteID"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue.Trim());
                cmd.Parameters["CompDivisionID"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue.Trim());
                cmd.Parameters["DepartmentID"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue.Trim());
                cmd.Parameters["LiabilityID"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue.Trim());
                cmd.Parameters["GradeID"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue.Trim());
                cmd.Parameters["Religion"].Value = stringDS.DataStrings[8].StringValue.Trim();
                cmd.Parameters["Gender"].Value = stringDS.DataStrings[9].StringValue.Trim();
                cmd.Parameters["Considerable_LWP"].Value = Convert.ToInt32(stringDS.DataStrings[10].StringValue.Trim());
                cmd.Parameters["ElqID"].Value = Convert.ToInt32(stringDS.DataStrings[14].StringValue.Trim());
                cmd.Parameters["ServiceLength_Day"].Value = Convert.ToInt32(stringDS.DataStrings[11].StringValue.Trim());
                cmd.Parameters["LateralExperience"].Value = Convert.ToDecimal(stringDS.DataStrings[12].StringValue.Trim());
                cmd.Parameters["EmpStatus"].Value = stringDS.DataStrings[13].StringValue;
                cmd.Parameters["PayExclude_From"].Value = Convert.ToDateTime(stringDS.DataStrings[15].StringValue.Trim());
                cmd.Parameters["PayExclude_To"].Value = Convert.ToDateTime(stringDS.DataStrings[16].StringValue.Trim());
                cmd.Parameters["SalaryMonth"].Value = Convert.ToDateTime(stringDS.DataStrings[18].StringValue.Trim());
                cmd.Parameters["Eligible_Only"].Value = Convert.ToBoolean(stringDS.DataStrings[19].StringValue.Trim());
            

            }
            
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, scaleOfPayDS, scaleOfPayDS.LocalPosting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BONUSPOSTING_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            scaleOfPayDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(scaleOfPayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createBonusPosting(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            scaleOfPayDS.Merge(inputDS.Tables[scaleOfPayDS.LocalPosting.TableName], false, MissingSchemaAction.Error);
            scaleOfPayDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BONUS_POSTING_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ScaleOfPayDS.LocalPostingRow row in scaleOfPayDS.LocalPosting.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeId);
                cmd.Parameters.AddWithValue("p_HeadId", row.HeadId);
                cmd.Parameters.AddWithValue("p_HeadCode", row.HeadCode);
                cmd.Parameters.AddWithValue("p_HeadType", row.HeadType);
                cmd.Parameters.AddWithValue("p_Description", row.Description);
                cmd.Parameters.AddWithValue("p_Position", row.Position);
                cmd.Parameters.AddWithValue("p_CalculatedAmount", row.CalculatedAmount);
                cmd.Parameters.AddWithValue("p_ChangedAmount", row.ChangedAmount);
                cmd.Parameters.AddWithValue("p_IdealAmount", row.IdealAmount);
                cmd.Parameters.AddWithValue("p_Disburse_Date", row.Disburse_Date);
                cmd.Parameters.AddWithValue("p_Payment_Month", row.Payment_Month);
                cmd.Parameters.AddWithValue("p_Suspended_Amount", row.SuspendedAmount);
                cmd.Parameters.AddWithValue("p_BonusCode", row.BonusCode);
                cmd.Parameters.AddWithValue("p_CreateBy", row.CreateBy);
                cmd.Parameters.AddWithValue("p_HandsOnPayment", row.HandsOnPayment);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CREATE_BONUSPOSTING.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }

            returnDS.Merge(errDS);
            return returnDS;
        }

        private DataSet getDisbursmentDateList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDisbursmentDateList");
            cmd.Parameters["HEADCODE"].Value = stringDS.DataStrings[0].StringValue;
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(25 Sep 11)...

            ScaleOfPayDS processDS = new ScaleOfPayDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, processDS, processDS.LocalPosting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DISBUSMENT_DATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            processDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(processDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getBonusData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS stringDS = new DataStringDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            long companyID = 0;
            if (stringDS.DataStrings[14].StringValue != "0") companyID = UtilDL.GetCompanyId(connDS, stringDS.DataStrings[14].StringValue);

            if (stringDS.DataStrings[11].StringValue == "GetBonusDataForModification")
            {
                cmd = DBCommandProvider.GetDBCommand("GetBonusDataForModification");
                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["HEADCODE1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["HEADCODE2"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["HeadCode_RS1"].Value = stringDS.DataStrings[10].StringValue.Trim();
                cmd.Parameters["HeadCode_RS2"].Value = stringDS.DataStrings[10].StringValue.Trim();
                cmd.Parameters["HEADCODE3"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["HEADCODE4"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["HeadCode_RS3"].Value = stringDS.DataStrings[10].StringValue.Trim();
                cmd.Parameters["HEADCODE5"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeName"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["HEADCODE6"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["HEADCODE7"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["DISBURSE_DATE1"].Value = Convert.ToDateTime(stringDS.DataStrings[3].StringValue.Trim());
                cmd.Parameters["DISBURSE_DATE2"].Value = Convert.ToDateTime(stringDS.DataStrings[3].StringValue.Trim());
                cmd.Parameters["SiteID1"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue.Trim());
                cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue.Trim());
                cmd.Parameters["CompDivisionID1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue.Trim());
                cmd.Parameters["CompDivisionID2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue.Trim());
                cmd.Parameters["DepartmentID1"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue.Trim());
                cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue.Trim());
                cmd.Parameters["LiabilityID1"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue.Trim());
                cmd.Parameters["LiabilityID2"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue.Trim());
                cmd.Parameters["GradeID1"].Value = Convert.ToInt32(stringDS.DataStrings[8].StringValue.Trim());
                cmd.Parameters["GradeID2"].Value = Convert.ToInt32(stringDS.DataStrings[8].StringValue.Trim());
                cmd.Parameters["EmployeeType1"].Value = Convert.ToInt32(stringDS.DataStrings[9].StringValue.Trim());
                cmd.Parameters["EmployeeType2"].Value = Convert.ToInt32(stringDS.DataStrings[9].StringValue.Trim());
                cmd.Parameters["CompanyID1"].Value = companyID;
                cmd.Parameters["CompanyID2"].Value = companyID;
            }
            else
            {
                if (stringDS.DataStrings[13].StringValue == "Designation")
                {
                    cmd = DBCommandProvider.GetDBCommand("GetBonusData");
                }
                else cmd = DBCommandProvider.GetDBCommand("GetBonusDataOrderByEmpCode");

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters["HEADCODE3"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["HeadCode_RS1"].Value = stringDS.DataStrings[12].StringValue.Trim();
                cmd.Parameters["HEADCODE4"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["HeadCode_RS2"].Value = stringDS.DataStrings[12].StringValue.Trim();
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["EmployeeName"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["HEADCODE1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["HEADCODE2"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["DISBURSE_DATE1"].Value = Convert.ToDateTime(stringDS.DataStrings[3].StringValue.Trim());
                cmd.Parameters["DISBURSE_DATE2"].Value = Convert.ToDateTime(stringDS.DataStrings[3].StringValue.Trim());
                cmd.Parameters["SiteID1"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue.Trim());
                cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[4].StringValue.Trim());
                cmd.Parameters["CompDivisionID1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue.Trim());
                cmd.Parameters["CompDivisionID2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue.Trim());
                cmd.Parameters["DepartmentID1"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue.Trim());
                cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue.Trim());
                cmd.Parameters["LiabilityID1"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue.Trim());
                cmd.Parameters["LiabilityID2"].Value = Convert.ToInt32(stringDS.DataStrings[7].StringValue.Trim());
                cmd.Parameters["GradeID1"].Value = Convert.ToInt32(stringDS.DataStrings[8].StringValue.Trim());
                cmd.Parameters["GradeID2"].Value = Convert.ToInt32(stringDS.DataStrings[8].StringValue.Trim());
                cmd.Parameters["Religion1"].Value = stringDS.DataStrings[9].StringValue.Trim();
                cmd.Parameters["Religion2"].Value = stringDS.DataStrings[9].StringValue.Trim();
                cmd.Parameters["Gender1"].Value = stringDS.DataStrings[10].StringValue.Trim();
                cmd.Parameters["Gender2"].Value = stringDS.DataStrings[10].StringValue.Trim();
                cmd.Parameters["EmployeeType1"].Value = Convert.ToInt32(stringDS.DataStrings[11].StringValue.Trim());
                cmd.Parameters["EmployeeType2"].Value = Convert.ToInt32(stringDS.DataStrings[11].StringValue.Trim());
                cmd.Parameters["CompanyID1"].Value = companyID;
                cmd.Parameters["CompanyID2"].Value = companyID;

                cmd.Parameters["DISBURSE_DATE3"].Value = cmd.Parameters["DISBURSE_DATE4"].Value = Convert.ToDateTime(stringDS.DataStrings[16].StringValue.Trim());
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, scaleOfPayDS, scaleOfPayDS.LocalPosting.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BONUS_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            scaleOfPayDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(scaleOfPayDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _updateBonusPosting(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            scaleOfPayDS.Merge(inputDS.Tables[scaleOfPayDS.LocalPosting.TableName], false, MissingSchemaAction.Error);
            scaleOfPayDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BONUS_POSTING_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ScaleOfPayDS.LocalPostingRow row in scaleOfPayDS.LocalPosting.Rows)
            {
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeId);
                cmd.Parameters.AddWithValue("p_BonusAmount_SbID", row.BonusAmount_SbID);
                cmd.Parameters.AddWithValue("p_IncomeTax_SbID", row.IncomeTax_SbID);
                cmd.Parameters.AddWithValue("p_Deduction_SbID", row.Deduction_SbID);
                cmd.Parameters.AddWithValue("p_BonusAmount", row.BonusAmount);
                cmd.Parameters.AddWithValue("p_SuspendedAmount", row.SuspendedAmount);
                cmd.Parameters.AddWithValue("p_IncomeTax", row.IncomeTax);
                cmd.Parameters.AddWithValue("p_Deduction", row.Deduction);
                cmd.Parameters.AddWithValue("p_LastUpdateUser", row.CreateBy);

                
                cmd.Parameters.AddWithValue("p_HeadType_Tax", row.HeadType_Tax);
                cmd.Parameters.AddWithValue("p_Position_Tax", row.Position_Tax);
                cmd.Parameters.AddWithValue("p_ADID_Tax", row.ADID_Tax);
                cmd.Parameters.AddWithValue("p_Description_Tax", row.Description_Tax);
                cmd.Parameters.AddWithValue("p_HeadCode_Tax", row.HeadCode_Tax);
                cmd.Parameters.AddWithValue("p_CalculatedAmount_Tax", row.CalculateAmount_Tax);
                cmd.Parameters.AddWithValue("p_IdealAmount_Tax", row.IdealAmount_Tax);
                cmd.Parameters.AddWithValue("p_SuspendedAmount_Tax", row.SuspendedAmount_Tax);

                cmd.Parameters.AddWithValue("p_HeadType", row.HeadType);
                cmd.Parameters.AddWithValue("p_Position", row.Position);
                cmd.Parameters.AddWithValue("p_ADID", row.ADID);
                cmd.Parameters.AddWithValue("p_CalculatedAmount_Deduction", row.CalculatedAmount_Deduction);
                cmd.Parameters.AddWithValue("p_SuspendedAmount_Deduction", row.SuspendedAmount_Deduction);


                if (row.IsDescriptionNull() == false) cmd.Parameters.AddWithValue("p_Description", row.Description);
                else cmd.Parameters.AddWithValue("p_Description", System.DBNull.Value);
                //cmd.Parameters.AddWithValue("p_Description", row.Description);
                cmd.Parameters.AddWithValue("p_HeadCode", row.HeadCode);
                cmd.Parameters.AddWithValue("p_Payment_Month", row.Payment_Month);
                cmd.Parameters.AddWithValue("p_Disburse_Date", row.Disburse_Date);
                cmd.Parameters.AddWithValue("p_BonusCode", row.BonusCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_UPDATE_BONUSPOSTING.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }

            returnDS.Merge(errDS);
            return returnDS;
        }

        private DataSet _deleteBonusPosting(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            ScaleOfPayDS scaleOfPayDS = new ScaleOfPayDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            scaleOfPayDS.Merge(inputDS.Tables[scaleOfPayDS.LocalPosting.TableName], false, MissingSchemaAction.Error);
            scaleOfPayDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BONUS_POSTING_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (ScaleOfPayDS.LocalPostingRow row in scaleOfPayDS.LocalPosting.Rows)
            {
                cmd.Parameters.AddWithValue("p_BonusAmount_SbID", row.BonusAmount_SbID);
                cmd.Parameters.AddWithValue("p_IncomeTax_SbID", row.IncomeTax_SbID);
                cmd.Parameters.AddWithValue("p_Deduction_SbID", row.Deduction_SbID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DELETE_BONUSPOSTING.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }

            returnDS.Merge(errDS);
            return returnDS;
        }

    }
}
