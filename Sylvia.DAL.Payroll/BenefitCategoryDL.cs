﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class BenefitCategoryDL
    {

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_BENEFIT_CATEGORY_CREATE:
                    return this._BenefitCategoryEntry(inputDS);
                case ActionID.ACTION_BENEFIT_CATEGORY_UPD:
                    return this._updateBenefitCategories(inputDS);
                case ActionID.NA_ACTION_GET_BENEFIT_CATEGORY_LIST:
                    return this._getBenefitCategoryList(inputDS);
                case ActionID.ACTION_BENEFIT_CATEGORY_DELETE:
                    return this._deleteBenefitCategories(inputDS);
                case ActionID.NA_ACTION_GET_BENEFIT_CATEGORY_LIST_ALL:
                    return this._GetBenefitCategoryListAll(inputDS);
                case ActionID.NA_ACTION_GET_BENEFIT_LIST_BY_CATEGORYEID:
                    return this._GetBenefitByBenefitCategoryID(inputDS);
                case ActionID.ACTION_BENEFIT_CATEGORY_CONFIG_CREATE:
                    return this._BenefitCategoryConfigEntry(inputDS);
                case ActionID.ACTION_BENEFIT_CATEGORY_CONFIG_DELETE:
                    return this._deleteBenefitCategoryConfig(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        #region WALI :: 13-Jun-2014
        private DataSet _BenefitCategoryEntry(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("BenefitCategoryEntry");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            LoanDS benefitDS = new LoanDS();
            benefitDS.Merge(inputDS.Tables[benefitDS.BenefitCategory.TableName], false, MissingSchemaAction.Error);
            benefitDS.AcceptChanges();

            foreach (LoanDS.BenefitCategoryRow categoryRow in benefitDS.BenefitCategory.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters["BenCatID"].Value = (object)genPK;


                if (categoryRow.IsBenefitCATEGORYCODENull() == false)
                {
                    cmd.Parameters["BenCatCode"].Value = (object)categoryRow.BenefitCATEGORYCODE;
                }
                else
                {
                    cmd.Parameters["BenCatCode"].Value = null;
                }

                if (categoryRow.IsBenefitCATEGORYNAMENull() == false)
                {
                    cmd.Parameters["BenCatName"].Value = (object)categoryRow.BenefitCATEGORYNAME;
                }
                else
                {
                    cmd.Parameters["BenCatName"].Value = null;
                }

                if (categoryRow.IsBenefitCATEGORYREMARKSNull() == false)
                {
                    cmd.Parameters["BenCatRemarks"].Value = (object)categoryRow.BenefitCATEGORYREMARKS;
                }
                else
                {
                    cmd.Parameters["BenCatRemarks"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSET_CATEGORY_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateBenefitCategories(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            LoanDS bebefitDS = new LoanDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateBenefitCategories");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            bebefitDS.Merge(inputDS.Tables[bebefitDS.BenefitCategory.TableName], false, MissingSchemaAction.Error);
            bebefitDS.AcceptChanges();

            foreach (LoanDS.BenefitCategoryRow benCatRow in bebefitDS.BenefitCategory.Rows)
            {
                if (benCatRow.IsBenefitCATEGORYNAMENull() == false)
                {
                    cmd.Parameters["BenCatNAME"].Value = (object)benCatRow.BenefitCATEGORYNAME;
                }
                else
                {
                    cmd.Parameters["BenCatNAME"].Value = null;
                }


                if (benCatRow.IsBenefitCATEGORYREMARKSNull() == false)
                {
                    cmd.Parameters["BenCatREMARKS"].Value = (object)benCatRow.BenefitCATEGORYREMARKS;
                }
                else
                {
                    cmd.Parameters["BenCatREMARKS"].Value = null;
                }


                if (benCatRow.IsBenefitCATEGORYCODENull() == false)
                {
                    cmd.Parameters["BenCatCODE"].Value = (object)benCatRow.BenefitCATEGORYCODE;
                }
                else
                {
                    cmd.Parameters["BenCatCODE"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BENEFIT_CATEGORY_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getBenefitCategoryList(DataSet inputDS)
        {
            OleDbCommand cmd = new OleDbCommand();
            LoanDS loanDS = new LoanDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Get sql command from commandXML folder
            if (stringDS.DataStrings[0].StringValue != "")
            {
                cmd = DBCommandProvider.GetDBCommand("GetBenefitCategoryList");
                cmd.Parameters["BenCatCode"].Value = stringDS.DataStrings[0].StringValue;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetBenefitCategoryAll");
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.BenefitCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BENEFIT_CATEGORY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            loanDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(loanDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _deleteBenefitCategories(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteBenefitCategories");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["BenCatCode"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BENEFIT_CATEGORY_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;
        }
        private DataSet _GetBenefitCategoryListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();
            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetBenefitCategoryAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            LoanDS loanDS = new LoanDS();
            nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.BenefitCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            loanDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(loanDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetBenefitByBenefitCategoryID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAssTypeByAssCateID");
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBenefitByBenCateID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["BenefitCategoryID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            LoanDS loanDS = new LoanDS();
            nRowAffected = ADOController.Instance.Fill(adapter, loanDS, loanDS.BenefitCateConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            loanDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(loanDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _BenefitCategoryConfigEntry(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Data Delete from "BenefitCateConfig" Table by gridCode wise

            OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("BenefitCategoryConfigDelete");
            if (cmd1 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            LoanDS loanDS = new LoanDS();
            loanDS.Merge(inputDS.Tables[loanDS.BenefitCateConfig.TableName], false, MissingSchemaAction.Error);
            loanDS.AcceptChanges();

            foreach (LoanDS.BenefitCateConfigRow row1 in loanDS.BenefitCateConfig.Rows)
            {
                if (row1.IsBenefitCategoryIDNull() == false)  cmd1.Parameters["BenefitCategoryID"].Value = (object)row1.BenefitCategoryID;
                else cmd1.Parameters["BenefitCategoryID"].Value = null;
                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd1, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BENEFIT_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Data Insert to "AssetCateConfig" from grdTypeList

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("BenefitCategoryConfigEntry");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            LoanDS loanDS2 = new LoanDS();
            loanDS2.Merge(inputDS.Tables[loanDS2.BenefitCateConfig.TableName], false, MissingSchemaAction.Error);
            loanDS2.AcceptChanges();

            foreach (LoanDS.BenefitCateConfigRow row in loanDS2.BenefitCateConfig.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters["BCCID"].Value = (object)genPK;

                if (row.IsBenefitCategoryIDNull() == false) cmd.Parameters["BenefitCategoryID"].Value = (object)row.BenefitCategoryID;
                else cmd.Parameters["BenefitCategoryID"].Value = null;

                if (row.IsOthersBenefitCodeNull() == false) cmd.Parameters["OthersBenefitCode"].Value = (object)row.OthersBenefitCode;
                else cmd.Parameters["OthersBenefitCode"].Value = null;
                
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BENEFIT_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion



            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        #endregion

        private DataSet _deleteBenefitCategoryConfig(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            LoanDS loanDS = new LoanDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            loanDS.Merge(inputDS.Tables[loanDS.BenefitCateConfig.TableName], false, MissingSchemaAction.Error);
            loanDS.AcceptChanges();

            OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("BenefitCategoryConfigDelete");
            if (cmd1 == null) return UtilDL.GetCommandNotFound();
            
            foreach (LoanDS.BenefitCateConfigRow row in loanDS.BenefitCateConfig.Rows)
            {
                if (row.IsBenefitCategoryIDNull() == false) cmd1.Parameters["BenefitCategoryID"].Value = (object)row.BenefitCategoryID;
                else cmd1.Parameters["BenefitCategoryID"].Value = null;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd1, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BENEFIT_CATEGORY_CONFIG_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            
            return errDS;
        }
    }
}
