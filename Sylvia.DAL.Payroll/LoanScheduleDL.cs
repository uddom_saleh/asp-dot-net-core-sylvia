using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
  /// <summary>
  /// Summary description for LoanScheduleDL.
  /// </summary>
  public class LoanScheduleDL
  {
    public LoanScheduleDL()
    {
      //
      // TODO: Add constructor logic here
      //
    }
    
    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();

        returnDS.Merge( errDS );
        returnDS.AcceptChanges();
        return returnDS;
      }

      switch ( actionID )
      {
        case ActionID.ACTION_LOANSCHEDULE_ADD:
          return _createLoanSchedule( inputDS );
      case ActionID.NA_ACTION_Q_LOANSCHEDULE_ALL:
          return _getLoanScheduleList(inputDS);
        case ActionID.ACTION_LOANSCHEDULE_DEL:
          return this._deleteLoanSchedule( inputDS );
        case ActionID.ACTION_LOANSCHEDULE_UPD:
          return this._updateLoanSchedule( inputDS );
          //        case ActionID.ACTION_DOES_LOANISSUE_EXIST:
          //          return this._doesLoanScheduleExist( inputDS );
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }  // end of switch statement

    }
    private DataSet _createLoanSchedule(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        LoanIssueDS loanIssueDS = new LoanIssueDS();
        DBConnectionDS connDS = new DBConnectionDS();
        long nId = -1;



        //extract dbconnection 
        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();


        loanIssueDS.Merge(inputDS.Tables[loanIssueDS.LoanIssues.TableName], false, MissingSchemaAction.Error);
        loanIssueDS.Merge(inputDS.Tables[loanIssueDS.LoanSchedules.TableName], false, MissingSchemaAction.Error);
        loanIssueDS.AcceptChanges();

        DataStringDS stringDS = new DataStringDS();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();

        //create command
        OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateLoanSchedule");
        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        if (stringDS.DataStrings[0].IsStringValueNull() == false)
        {
            nId = UtilDL.GetLoanIssueId(connDS, stringDS.DataStrings[0].StringValue);
            if (nId == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }
            cmd.Parameters["Id"].Value = nId;
        }
        foreach (LoanIssueDS.LoanSchedule loanSchedule in loanIssueDS.LoanSchedules)
        {

            //        long genPK = IDGenerator.GetNextGenericPK();
            //        if(genPK == -1)
            //        {
            //          return UtilDL.GetDBOperationFailed();
            //        }
            //         
            //        cmd.Parameters["Id"].Value = (object) genPK;
            if (loanSchedule.IsScheduleNoNull() == false)
            {
                cmd.Parameters["ScheduleNo"].Value = (object)loanSchedule.ScheduleNo;
            }
            else
            {
                cmd.Parameters["ScheduleNo"].Value = null;
            }

            if (loanSchedule.IsInstallmentPrincipalNull() == false)
            {

                cmd.Parameters["InstallmentPrincipal"].Value = (object)loanSchedule.InstallmentPrincipal;
            }
            else
            {
                cmd.Parameters["InstallmentPrincipal"].Value = null;
            }

            if (loanSchedule.IsInstallmentInterestNull() == false)
            {

                cmd.Parameters["InstallmentInterest"].Value = (object)loanSchedule.InstallmentInterest;
            }
            else
            {
                cmd.Parameters["InstallmentInterest"].Value = null;
            }

            if (loanSchedule.IsDueInstallmentDateNull() == false)
            {

                cmd.Parameters["DueInstallmentDate"].Value = (object)loanSchedule.DueInstallmentDate;
            }
            else
            {
                cmd.Parameters["DueInstallmentDate"].Value = System.DBNull.Value;
            }
            if (loanSchedule.IsPaymentDateNull() == false)
            {

                cmd.Parameters["PaymentDate"].Value = (object)loanSchedule.PaymentDate;
            }
            else
            {
                cmd.Parameters["PaymentDate"].Value = System.DBNull.Value;
            }
            if (loanSchedule.IsVoucherNoNull() == false)
            {

                cmd.Parameters["VoucherNo"].Value = (object)loanSchedule.VoucherNo;
            }
            else
            {
                cmd.Parameters["VoucherNo"].Value = System.DBNull.Value;
            }
            if (loanSchedule.IsCashAmountNull() == false)
            {

                cmd.Parameters["CashAmount"].Value = (object)loanSchedule.CashAmount;
            }
            else
            {
                cmd.Parameters["CashAmount"].Value = System.DBNull.Value;
            }


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LOANISSUE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
        }
        errDS.Clear();
        errDS.AcceptChanges();
        return errDS;
    }
    private DataSet _deleteLoanSchedule(DataSet inputDS )
    {
      ErrorDS errDS = new ErrorDS();

      DBConnectionDS connDS = new  DBConnectionDS();      
      DataStringDS stringDS = new DataStringDS();

      connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
      connDS.AcceptChanges();

      stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
      stringDS.AcceptChanges();

    
      OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteLoanSchedule");
      if(cmd==null)
      {
        return UtilDL.GetCommandNotFound();
      }
      foreach(DataStringDS.DataString sValue in stringDS.DataStrings)
      {
        if (sValue.IsStringValueNull()==false)
        {
          cmd.Parameters["Code"].Value = (object) sValue.StringValue;
        }
      
        bool bError = false;
        int nRowAffected = -1;
        nRowAffected = ADOController.Instance.ExecuteNonQuery( cmd, connDS.DBConnections[0].ConnectionID, ref bError );

        if (bError == true )
        {
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
          err.ErrorInfo1 = ActionID.ACTION_LOANISSUE_DEL.ToString();
          errDS.Errors.AddError( err );
          errDS.AcceptChanges();
          return errDS;
        }
      }

      return errDS;  // return empty ErrorDS
    }
    private DataSet _getLoanScheduleList(DataSet inputDS)
    {
      ErrorDS errDS = new ErrorDS();
      /*
            DBConnectionDS connDS = new  DBConnectionDS();      
            DataLongDS longDS = new DataLongDS();
      
            connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
            connDS.AcceptChanges();

   

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetLoanScheduleList");
  
            if(cmd==null)
            {
              return UtilDL.GetCommandNotFound();
            }
    

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            LoanSchedulePO loanSchedulePO = new LoanSchedulePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, 
              loanSchedulePO, loanSchedulePO.LoanSchedules.TableName, 
              connDS.DBConnections[0].ConnectionID, 
              ref bError);
            if (bError == true )
            {
              ErrorDS.Error err = errDS.Errors.NewError();
              err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
              err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
              err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
              err.ErrorInfo1 = ActionID.ACTION_Q_LOANISSUE_ALL.ToString();
              errDS.Errors.AddError( err );
              errDS.AcceptChanges();
              return errDS;

            }

            loanSchedulePO.AcceptChanges();

            LoanScheduleDS loanScheduleDS = new LoanScheduleDS();
            if ( loanSchedulePO.LoanSchedules.Count > 0 )
            {
       

              foreach ( LoanSchedulePO.LoanSchedule poLoanSchedule in loanSchedulePO.LoanSchedules)
              {
                LoanScheduleDS.LoanSchedule loanSchedule = loanScheduleDS.LoanSchedules.NewLoanSchedule();
                if(poLoanSchedule.IsCodeNull()==false)
                {
                  loanSchedule.Code = poLoanSchedule.Code;
                }
                if(poLoanSchedule.IsNameNull()==false)
                {
                  loanSchedule.Name = poLoanSchedule.Name;
                }
                if(poLoanSchedule.IsInterestRateNull()==false)
                {
                  loanSchedule.InterestRate = poLoanSchedule.InterestRate;
                }
                if(poLoanSchedule.IsNoOfInstallmentNull()==false)
                {
                  loanSchedule.NoOfInstallment = poLoanSchedule.NoOfInstallment;
                }
          
                loanScheduleDS.LoanSchedules.AddLoanSchedule( loanSchedule );
                loanScheduleDS.AcceptChanges();
              }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            */
      DataSet returnDS = new DataSet();

      //    returnDS.Merge( loanScheduleDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
    
      return returnDS;
    
    
    }

    private DataSet _updateLoanSchedule(DataSet inputDS)
    {
        ErrorDS errDS = new ErrorDS();
        LoanIssueDS loanIssueDS = new LoanIssueDS();
        DBConnectionDS connDS = new DBConnectionDS();
        long nId = -1;



        //extract dbconnection 
        connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        connDS.AcceptChanges();


        loanIssueDS.Merge(inputDS.Tables[loanIssueDS.LoanIssues.TableName], false, MissingSchemaAction.Error);
        loanIssueDS.Merge(inputDS.Tables[loanIssueDS.LoanSchedules.TableName], false, MissingSchemaAction.Error);
        loanIssueDS.AcceptChanges();

        DataStringDS stringDS = new DataStringDS();
        stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        stringDS.AcceptChanges();


        OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteLoanSchedules");
        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }


        foreach (LoanIssueDS.LoanIssue loanIssue in loanIssueDS.LoanIssues)
        {
            cmd.Parameters["LoanNo"].Value = (object)loanIssue.LoanIssueCode;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LOANSCHEDULE_UPD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
        }




        //create command
        cmd.Dispose();
        cmd = DBCommandProvider.GetDBCommand("CreateLoanSchedule");
        if (cmd == null)
        {
            return UtilDL.GetCommandNotFound();
        }

        if (stringDS.DataStrings[0].IsStringValueNull() == false)
        {
            nId = UtilDL.GetLoanIssueId(connDS, stringDS.DataStrings[0].StringValue);
            if (nId == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }
            cmd.Parameters["Id"].Value = nId;
        }
        foreach (LoanIssueDS.LoanSchedule loanSchedule in loanIssueDS.LoanSchedules)
        {


            if (loanSchedule.IsScheduleNoNull() == false)
            {
                cmd.Parameters["ScheduleNo"].Value = (object)loanSchedule.ScheduleNo;
            }
            else
            {
                cmd.Parameters["ScheduleNo"].Value = null;
            }

            if (loanSchedule.IsInstallmentPrincipalNull() == false)
            {

                cmd.Parameters["InstallmentPrincipal"].Value = (object)loanSchedule.InstallmentPrincipal;
            }
            else
            {
                cmd.Parameters["InstallmentPrincipal"].Value = null;
            }

            if (loanSchedule.IsInstallmentInterestNull() == false)
            {

                cmd.Parameters["InstallmentInterest"].Value = (object)loanSchedule.InstallmentInterest;
            }
            else
            {
                cmd.Parameters["InstallmentInterest"].Value = null;
            }

            if (loanSchedule.IsDueInstallmentDateNull() == false)
            {

                cmd.Parameters["DueInstallmentDate"].Value = (object)loanSchedule.DueInstallmentDate;
            }
            else
            {
                cmd.Parameters["DueInstallmentDate"].Value = System.DBNull.Value;
            }
            if (loanSchedule.IsPaymentDateNull() == false)
            {

                cmd.Parameters["PaymentDate"].Value = (object)loanSchedule.PaymentDate;
            }
            else
            {
                cmd.Parameters["PaymentDate"].Value = System.DBNull.Value;
            }

            if (loanSchedule.IsVoucherNoNull() == false) cmd.Parameters["VoucherNo"].Value = (object)loanSchedule.VoucherNo;
            else cmd.Parameters["VoucherNo"].Value = DBNull.Value;

            if (loanSchedule.IsCashAmountNull() == false)
            {

                cmd.Parameters["CashAmount"].Value = (object)loanSchedule.CashAmount;
            }
            else
            {
                cmd.Parameters["CashAmount"].Value = System.DBNull.Value;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_LOANISSUE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
        }
        errDS.Clear();
        errDS.AcceptChanges();
        return errDS;
    }

    private DataSet _doesLoanScheduleExist( DataSet inputDS )
    {
      DataBoolDS boolDS = new DataBoolDS();
      ErrorDS errDS = new ErrorDS();
      DataSet returnDS = new DataSet();     
      /*
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge( inputDS.Tables[ connDS.DBConnections.TableName ], false, MissingSchemaAction.Error );
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge( inputDS.Tables[ stringDS.DataStrings.TableName ], false, MissingSchemaAction.Error );
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesLoanScheduleExist");
            if(cmd==null)
            {
              return UtilDL.GetCommandNotFound();
            }

            if(stringDS.DataStrings[0].IsStringValueNull()==false)
            {
              cmd.Parameters["Code"].Value = (object) stringDS.DataStrings[0].StringValue ;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32( ADOController.Instance.ExecuteScalar( cmd, connDS.DBConnections[0].ConnectionID, ref bError ));

            if (bError == true )
            {
              errDS.Clear();
              ErrorDS.Error err = errDS.Errors.NewError();
              err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
              err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
              err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString() ;
              err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
              errDS.Errors.AddError( err );
              errDS.AcceptChanges();
              return errDS;
            }      

            boolDS.DataBools.AddDataBool ( nRowAffected == 0 ? false : true );
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge( boolDS );
            returnDS.Merge( errDS );
            returnDS.AcceptChanges();*/
      return returnDS;
    }   
  
  }
}
