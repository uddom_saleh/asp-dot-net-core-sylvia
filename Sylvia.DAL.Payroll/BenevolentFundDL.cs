using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using System.Globalization;

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for AllowDeductDL.
    /// </summary>
    public class BenevolentFundDL
    {
        public BenevolentFundDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.NA_ACTION_GET_BF_DATA:
                    return this._getBF_Data(inputDS);

                case ActionID.ACTION_BF_FUND_TYPE_ADD:
                    return _createBF_Fund_Type(inputDS);
                case ActionID.ACTION_BF_FUND_TYPE_UPP:
                    return _updateBF_Fund_Type(inputDS);
                case ActionID.ACTION_BF_FUND_TYPE_DEL:
                    return _deleteBF_Fund_Type(inputDS);
                case ActionID.ACTION_BF_SHARE_RATE_UPP:
                    return _updateBF_Share_Rate(inputDS);
                case ActionID.ACTION_BF_MEMBER_UPP:
                    return _updateBF_Member(inputDS);
                case ActionID.NA_GET_BF_ABLE_TO_INVEST_AMOUNT:
                    return this._GetAbleToInvestAmount(inputDS);
                case ActionID.ACTION_BF_CREATE_INVESTMENT:
                    return this._CreateInvestment_BF(inputDS);
                case ActionID.ACTION_BF_UPDATE_INVESTMENT:
                    return this._UpdateInvestment_BF(inputDS);
                case ActionID.NA_GET_BF_INVESTMENT_LIST:
                    return this._GetInvestmentList_BF(inputDS);
                case ActionID.NA_GET_BF_INVESTMENT_DETAILS_BY_INVID:
                    return this._GetInvestmentDetailsByInvID_BF(inputDS);
                case ActionID.ACTION_BF_DELETE_LAST_INVESTMENT:
                    return this._CancelLastInvestment_BF(inputDS);
                case ActionID.ACTION_BF_INVESTMENT_SETTLEMENT_CREATE:
                    return this._InvestmentSettlementCreate_BF(inputDS);

                case ActionID.NA_GET_BF_PROVISION_BENEFICIARY_LIST:
                    return this._GetProvisionBeneficiaryList_BF(inputDS);
                case ActionID.NA_GET_BF_INVESTMENT_PROVISION_LIST:
                    return this._GetInvestmentProvisionList_BF(inputDS);
                case ActionID.ACTION_BF_PROVISION_CREATION:
                    return this._ProvisionCreation_BF(inputDS);

                case ActionID.ACTION_BF_TERMS_OF_SEPARATION_SAVE:
                    return this._SaveTermsOfSeperation_BF(inputDS);
                case ActionID.NA_GET_BF_TERMS_OF_SEPARATION:
                    return this._GetTermsOfSeperation_BF(inputDS);

                case ActionID.NA_GET_BF_EMPLOYEE_LIST_FOR_SEPERATION:
                    return this._GetEmployeeListForSeperation_BF(inputDS);
                case ActionID.NA_GET_BF_EMPLOYEE_LIST_FOR_PAYMENT:
                    return this._GetEmployeeListForPayment_BF(inputDS);
                case ActionID.NA_GET_BF_PAYMENT_AMOUNT_FOR_SEPERATION:
                    return this._GetPaymentAmountsForSeperation_BF(inputDS);
                case ActionID.ACTION_BF_EMPLOYEE_SEPARATION:
                    return this._EmployeeSeperation_BF(inputDS);
                case ActionID.NA_GET_BF_PAYMENT_AMOUNT_FOR_PAYMENT:
                    return this._GetPaymentAmountsForPayment_BF(inputDS);
                case ActionID.ACTION_BF_EMPLOYEE_PAYMENT:
                    return this._EmployeePayment_BF(inputDS);

                case ActionID.NA_GET_BF_FUND_HEAD_LIST:
                    return this._GetFundHeadList_BF(inputDS);
                case ActionID.NA_GET_BF_FUND_RECIEVE_DETAILS:
                    return this._GetFundRecieveDetails_BF(inputDS);
                case ActionID.ACTION_BF_FUND_RECIEVE:
                    return this._FundRecieve_BF(inputDS);

                case ActionID.NA_GET_BF_PROVISION_CALCULATED_DATA:
                    return this._GetProvisionCalculatedData_BF(inputDS);
                case ActionID.ACTION_BF_SAVE_PROVISION_CALCULATED_DATA:
                    return this._SaveProvisionCalculatedData_BF(inputDS);
                case ActionID.NA_GET_BF_PROVISIONED_PROFIT_FOR_EMPLOYEE:
                    return this._GetProvisionedProfit_ForEmployee_BF(inputDS);
                case ActionID.ACTION_BF_DISTRIBUTE_PROVISIONED_PROFIT:
                    return this._DistributeProvisionedProfit_BF(inputDS);
                case ActionID.NA_GET_BF_DISTRIBUTED_PROVISION_DATA:
                    return this.GetDistributedProvisionData_BF(inputDS);

                case ActionID.NA_GET_BF_PENDING_DISTRIBUTION:
                    return this._GetPendingDistributions_BF(inputDS);

                case ActionID.ACTION_BF_FUND_JOURNAL_ENTRY:
                    return this._FundJournalEntry_BF(inputDS);
                case ActionID.NA_GET_BF_HEADWISE_AVAILABLE_AMOUNT:
                    return this._GetHeadWiseAvailableAmount_BF(inputDS);
                case ActionID.NA_GET_BF_JOURNAL_EMPLOYEE_BY_BF_MEMBDATE:
                    return this._GetEmployeeList_By_BFMembDate(inputDS);
                case ActionID.ACTION_BF_EMPLOYEE_JOURNAL_ENTRY:
                    return this._EmployeeJournalEntry_BF(inputDS);

                case ActionID.NA_GET_DISTINCT_FUND_YEAR_BF:
                    return this._GetDistinctFundYear_BF(inputDS);
                case ActionID.NA_GET_BF_RECIEVED_FUND_LIST:
                    return this._GetRecievedFundList_BF(inputDS);
                case ActionID.NA_GET_BF_TOTAL_PAYABLE_TO_LSC:
                    return this.GetTotalPayableToLSC_BF(inputDS);

                case ActionID.NA_GET_BF_EMPLOYEE_BALANCE:
                    return this._GetEmployeeBalanceRecord_BF(inputDS);
                case ActionID.ACTION_BF_MODIFY_EMPLOYEE_BALANCE:
                    return this._ModifyEmployeeBalanceRecord_BF(inputDS);
                case ActionID.NA_GET_BF_FUND_BALANCE:
                    return this._GetFundHeadBalanceRecord_BF(inputDS);
                case ActionID.ACTION_BF_MODIFY_FUND_BALANCE:
                    return this._ModifyFundHeadBalanceRecord_BF(inputDS);
                case ActionID.NA_GET_BF_BENEFICIARY_LIST_TO_FUND_RECIEVE:
                    return this._GetBeneficiaryList_To_FundRecieve_BF(inputDS);
                case ActionID.ACTION_BF_FUND_RECIEVE_EMPLOYEE_WISE:
                    return this._Exceptional_FundRecieve_BF(inputDS);
                case ActionID.NA_GET_BF_EMPLOYEE_LIST_FOR_GEN_PAYMENT:
                    return this._GetBF_EmpListForGenaralPayment(inputDS);
                case ActionID.ACTION_BF_PAYMENT_GENERAL:
                    return this._BF_Payment_General(inputDS);
                case ActionID.NA_GET_BF_PAYMENT_AMOUNT_GENERAL:
                    return this._GetBF_PaymentAmountsGeneral(inputDS);
                case ActionID.NA_GET_BF_SEPARATION_INFO:
                    return this._GetBF_SeparationInfo(inputDS);
                case ActionID.ACTION_BF_GRADEWISE_ADD:
                    return this._createBF_Gradewise(inputDS);
                case ActionID.NA_ACTION_GET_BF_DATA_FOR_REPORT:
                    return this._GetBF_Data_ForReport(inputDS);

                case ActionID.ACTION_OCS_SHARE_ADD:
                    return this._createOCS_Share(inputDS);
                case ActionID.ACTION_OCS_SHARE_UPD:
                    return this._updateOCS_Share(inputDS);
                case ActionID.ACTION_OCS_SHARE_DEL:
                    return this._deleteOCS_Share(inputDS);
                case ActionID.NA_GET_BF_EMP_LIST_FOR_FUND_PAYMENT:
                    return this._GetBF_EmpListFor_FundPayment(inputDS);
                case ActionID.ACTION_BF_FUND_PAYMENT:
                    return this._createBF_FundPayment(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    AppLogger.LogError(returnDS.GetXml());
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _getBF_Data(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            bool bError = false;
            int nRowAffected = -1;

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            BenevolentFund bfDS = new BenevolentFund();



            if (stringDS.DataStrings[0].StringValue == "FundTypeExistencyCheck")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFundTypeExistency_BF");
                cmd.Parameters["Code"].Value = stringDS.DataStrings[1].StringValue;

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.Data_Existence.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            else if (stringDS.DataStrings[0].StringValue == "BF_Fund_Type_All")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBF_Fund_Type_All");

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_FundType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            else if (stringDS.DataStrings[0].StringValue == "BF_Share_Rate_by_Fund_Type_ID")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBF_ShareRateByTypeID");
                cmd.Parameters["Fund_Type_ID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_ShareRate.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            else if (stringDS.DataStrings[0].StringValue == "BF_Memeber_List")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBF_Memeber_List");
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;

                cmd.Parameters["BranchID"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["BranchID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);

                cmd.Parameters["DepartmentCode"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;

                cmd.Parameters["DesignationCode"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;

                cmd.Parameters["Fund_Type_ID"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
                cmd.Parameters["Fund_Type_ID2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);

                cmd.Parameters["EmployeeStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
                cmd.Parameters["EmployeeStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_FundMember.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            else if (stringDS.DataStrings[0].StringValue == "OCS_Share_Employee_List")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetOCS_Share_Employee_List");

                cmd.Parameters["Year1"].Value = cmd.Parameters["Year2"].Value = Convert.ToDateTime(stringDS.DataStrings[5].StringValue).Year;
                cmd.Parameters["Month1"].Value = cmd.Parameters["Month2"].Value = Convert.ToDateTime(stringDS.DataStrings[5].StringValue).Month;
                cmd.Parameters["EmployeeCode"].Value = cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["BranchID"].Value = cmd.Parameters["BranchID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["DepartmentCode"].Value = cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["DesignationCode"].Value = cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["EmployeeStatus1"].Value = cmd.Parameters["EmployeeStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.OCS_Share.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }
            
            if (stringDS.DataStrings[0].StringValue == "BF_Gradewise")
            {
                OleDbCommand cmd = DBCommandProvider.GetDBCommand("Get_GradewiseBF");
                cmd.Parameters["FT_ID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);

                OleDbDataAdapter adapter = new OleDbDataAdapter();
                adapter.SelectCommand = cmd;

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Gradewise.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            }

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BF_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            bfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        private DataSet _createBF_Fund_Type(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BenevolentFund bfDS = new BenevolentFund();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BF_FUND_TYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            bfDS.Merge(inputDS.Tables[bfDS.BF_FundType.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            foreach (BenevolentFund.BF_FundTypeRow row in bfDS.BF_FundType)
            {

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) UtilDL.GetDBOperationFailed();


                cmd.Parameters.AddWithValue("p_FT_Id", (object)genPK);
                cmd.Parameters.AddWithValue("p_FT_Code", (object)row.FT_Code);

                if (!row.IsFT_NameNull()) cmd.Parameters.AddWithValue("p_FT_Name", row.FT_Name);
                else cmd.Parameters.AddWithValue("p_FT_Name", DBNull.Value);

                cmd.Parameters.AddWithValue("p_FT_Taxable", (object)row.FT_Taxable);
                cmd.Parameters.AddWithValue("p_FT_IsActive", (object)row.FT_IsActive);
                cmd.Parameters.AddWithValue("p_LoginID", (object)row.LoginID);
                cmd.Parameters.AddWithValue("p_FT_PercentOfBasic", (object)row.FT_PercentOfBasic);
                cmd.Parameters.AddWithValue("p_FT_FlatAmount", (object)row.FT_FlatAmount);
                cmd.Parameters.AddWithValue("p_FT_IsShareBased", (object)row.FT_IsShareBased);



                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_FUND_TYPE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateBF_Fund_Type(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BenevolentFund bfDS = new BenevolentFund();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BF_FUND_TYPE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            bfDS.Merge(inputDS.Tables[bfDS.BF_FundType.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            foreach (BenevolentFund.BF_FundTypeRow row in bfDS.BF_FundType)
            {
                cmd.Parameters.AddWithValue("p_FT_Code", (object)row.FT_Code);

                if (!row.IsFT_NameNull()) cmd.Parameters.AddWithValue("p_FT_Name", row.FT_Name);
                else cmd.Parameters.AddWithValue("p_FT_Name", DBNull.Value);

                cmd.Parameters.AddWithValue("p_FT_Taxable", (object)row.FT_Taxable);
                cmd.Parameters.AddWithValue("p_FT_IsActive", (object)row.FT_IsActive);
                cmd.Parameters.AddWithValue("p_LoginID", (object)row.LoginID);
                cmd.Parameters.AddWithValue("p_FT_PercentOfBasic", (object)row.FT_PercentOfBasic);
                cmd.Parameters.AddWithValue("p_FT_FlatAmount", (object)row.FT_FlatAmount);
                cmd.Parameters.AddWithValue("p_FT_IsShareBased", (object)row.FT_IsShareBased);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_FUND_TYPE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _deleteBF_Fund_Type(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BF_FUND_TYPE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters.AddWithValue("p_FT_Code", (object)stringDS.DataStrings[0].StringValue);


            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_FUND_TYPE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }



            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateBF_Share_Rate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BenevolentFund bfDS = new BenevolentFund();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BF_SHARE_RATE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            bfDS.Merge(inputDS.Tables[bfDS.BF_ShareRate.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            foreach (BenevolentFund.BF_ShareRateRow row in bfDS.BF_ShareRate)
            {
                cmd.Parameters.AddWithValue("p_FT_ID", (object)row.FT_ID);

                if (!row.IsFT_PercentOfBasicNull()) cmd.Parameters.AddWithValue("p_FT_PercentOfBasic", row.FT_PercentOfBasic);
                else cmd.Parameters.AddWithValue("p_FT_PercentOfBasic", DBNull.Value);

                if (!row.IsFT_FlatAmountNull()) cmd.Parameters.AddWithValue("p_FT_FlatAmount", row.FT_FlatAmount);
                else cmd.Parameters.AddWithValue("p_FT_FlatAmount", DBNull.Value);

                if (!row.IsFT_EffectDateNull()) cmd.Parameters.AddWithValue("p_FT_EffectDate", row.FT_EffectDate);
                else cmd.Parameters.AddWithValue("p_FT_EffectDate", DBNull.Value);

                cmd.Parameters.AddWithValue("p_FT_IsActive", row.FT_IsActive);
                cmd.Parameters.AddWithValue("p_LoginID", row.LoginID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_SHARE_RATE_UPP.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateBF_Member(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BenevolentFund bfDS = new BenevolentFund();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();


            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BF_MEMBER_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            bfDS.Merge(inputDS.Tables[bfDS.BF_FundMember.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            foreach (BenevolentFund.BF_FundMemberRow row in bfDS.BF_FundMember)
            {
                cmd.Parameters.AddWithValue("p_FT_ID", (object)row.FT_ID);

                if (!row.IsEmployeeCodeNull()) cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                else cmd.Parameters.AddWithValue("p_EmployeeCode", DBNull.Value);

                if (!row.IsFT_MembershipDateNull()) cmd.Parameters.AddWithValue("p_FT_MembershipDate", row.FT_MembershipDate);
                else cmd.Parameters.AddWithValue("p_FT_MembershipDate", DBNull.Value);

                if (!row.IsFT_NoOfShareNull()) cmd.Parameters.AddWithValue("p_FT_NoOfShare", row.FT_NoOfShare);
                else cmd.Parameters.AddWithValue("p_FT_NoOfShare", DBNull.Value);

                if (!row.IsFT_RemarksNull()) cmd.Parameters.AddWithValue("p_FT_Remarks", row.FT_Remarks);
                else cmd.Parameters.AddWithValue("p_FT_Remarks", DBNull.Value);

                cmd.Parameters.AddWithValue("p_FT_IsActive", row.FT_IsActive);
                cmd.Parameters.AddWithValue("p_LoginID", row.LoginID);

                if(!row.IsFT_PercentOfBasic_SelfNull()) cmd.Parameters.AddWithValue("p_FT_PercentOfBasic_Self", row.FT_PercentOfBasic_Self);
                else cmd.Parameters.AddWithValue("p_FT_PercentOfBasic_Self", DBNull.Value);

                if(!row.IsFT_FlatAmount_SelfNull()) cmd.Parameters.AddWithValue("p_FT_FlatAmount_Self", row.FT_FlatAmount_Self);
                else cmd.Parameters.AddWithValue("p_FT_FlatAmount_Self", DBNull.Value);

                if(!row.IsFT_PercentOfBasic_CompNull()) cmd.Parameters.AddWithValue("p_FT_PercentOfBasic_Comp", row.FT_PercentOfBasic_Comp);
                else cmd.Parameters.AddWithValue("p_FT_PercentOfBasic_Comp", DBNull.Value);

                if(!row.IsFT_FlatAmount_CompNull()) cmd.Parameters.AddWithValue("p_FT_FlatAmount_Comp", row.FT_FlatAmount_Comp);
                else cmd.Parameters.AddWithValue("p_FT_FlatAmount_Comp", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_MEMBER_UPP.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }


            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetAbleToInvestAmount(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAbleToInvestAmount_BF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_ABLE_TO_INVEST_AMOUNT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _CreateInvestment_BF(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            BenevolentFund bfDS = new BenevolentFund();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            bfDS.Merge(inputDS.Tables[bfDS.Investment.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            #region Create New Investment
            cmd.CommandText = "PRO_BF_INVESTMENT_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            long InvID = IDGenerator.GetNextGenericPK();
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", InvID);
            cmd.Parameters.AddWithValue("p_InvestmentDate", DateTime.ParseExact(stringDS.DataStrings[0].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_MaturityDate", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_ProductID", Convert.ToInt32(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_PrincipalAmount", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_Profit", Convert.ToDecimal(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_Duration", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[6].StringValue);
            cmd.Parameters.AddWithValue("p_Tax", Convert.ToDecimal(stringDS.DataStrings[7].StringValue));
            cmd.Parameters.AddWithValue("p_ExciseDuty", Convert.ToDecimal(stringDS.DataStrings[8].StringValue));
            cmd.Parameters.AddWithValue("p_OthersCost", Convert.ToDecimal(stringDS.DataStrings[9].StringValue));
            cmd.Parameters.AddWithValue("p_Buyer", stringDS.DataStrings[10].StringValue);
            cmd.Parameters.AddWithValue("p_Total_Instrument", stringDS.DataStrings[12].StringValue);

            cmd.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[13].StringValue);       // WALI :: 13-Jan-2015

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_CREATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Add Product_Ins_Reference
            cmd.Dispose();
            cmd.CommandText = "PRO_BF_INV_REFERENCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (BenevolentFund.InvestmentRow row in bfDS.Investment.Rows)
            {
                cmd.Parameters.AddWithValue("p_INVID", InvID);
                cmd.Parameters.AddWithValue("p_Product_Ins_Reference", row.Product_Ins_Reference);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_CREATE_INVESTMENT.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _GetInvestmentList_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "NotSettled") cmd = DBCommandProvider.GetDBCommand("GetInvestmentListNotSettled_BF");
            else if (stringDS.DataStrings[0].StringValue == "All") cmd = DBCommandProvider.GetDBCommand("GetInvestmentListAll_BF");

            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_INVESTMENT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetInvestmentDetailsByInvID_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetInvestmentDetailsByInvID_BF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["p_INVID"].Value = (object)Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.Investment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_INVESTMENT_DETAILS_BY_INVID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _UpdateInvestment_BF(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            BenevolentFund bfDS = new BenevolentFund();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            bfDS.Merge(inputDS.Tables[bfDS.Investment.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            #region Update Investment
            cmd.CommandText = "PRO_BF_INVESTMENT_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            Int32 InvID = -1;
            InvID = Convert.ToInt32(stringDS.DataStrings[11].StringValue);
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[11].StringValue));
            cmd.Parameters.AddWithValue("p_InvestmentDate", DateTime.ParseExact(stringDS.DataStrings[0].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_MaturityDate", DateTime.ParseExact(stringDS.DataStrings[1].StringValue, "dd-MM-yyyy", null));
            cmd.Parameters.AddWithValue("p_ProductID", Convert.ToInt32(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_PrincipalAmount", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_Profit", Convert.ToDecimal(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_Duration", Convert.ToInt32(stringDS.DataStrings[5].StringValue));
            cmd.Parameters.AddWithValue("p_Remarks", stringDS.DataStrings[6].StringValue);
            cmd.Parameters.AddWithValue("p_Tax", Convert.ToDecimal(stringDS.DataStrings[7].StringValue));
            cmd.Parameters.AddWithValue("p_ExciseDuty", Convert.ToDecimal(stringDS.DataStrings[8].StringValue));
            cmd.Parameters.AddWithValue("p_OthersCost", Convert.ToDecimal(stringDS.DataStrings[9].StringValue));
            cmd.Parameters.AddWithValue("p_Buyer", stringDS.DataStrings[10].StringValue);
            cmd.Parameters.AddWithValue("p_Total_Instrument", stringDS.DataStrings[12].StringValue);

            cmd.Parameters.AddWithValue("p_Reference", stringDS.DataStrings[13].StringValue);       // WALI :: 13-Jan-2015

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_UPDATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Delete OLD Product_Ins_Reference
            cmd.Dispose();
            cmd.CommandText = "PRO_BF_INV_REFERENCE_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_INVID", InvID);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_UPDATE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Add New Product_Ins_Reference
            cmd.Dispose();
            cmd.CommandText = "PRO_BF_INV_REFERENCE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (BenevolentFund.InvestmentRow row in bfDS.Investment.Rows)
            {
                cmd.Parameters.AddWithValue("p_INVID", InvID);
                cmd.Parameters.AddWithValue("p_Product_Ins_Reference", row.Product_Ins_Reference);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_CREATE_INVESTMENT.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _CancelLastInvestment_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();
            bool bError = false;
            int nRowAffected = -1;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BF_DEL_LAST_INVESTMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_InvID", integerDS.DataIntegers[0].IntegerValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_DELETE_LAST_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _FreezeInvestment_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "PRO_BF_FREEZE_INVESTMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //err.ErrorInfo1 = ActionID.ACTION_BF_FREEZE_INVESTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _InvestmentSettlementCreate_BF(DataSet inputDS)
        {
            int nRowAffected = -1;
            bool bError = false;
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            WPPWFDS wppwfDS = new WPPWFDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DateTime settlementDate = DateTime.ParseExact(stringDS.DataStrings[2].StringValue, "dd-MM-yyyy", null);
            DateTime provisionDate = new DateTime(settlementDate.Year, settlementDate.Month, 2);  // Second day of 'Settlement Month'

            #region Investment Settlement
            cmd.CommandText = "PRO_BF_INV_SETTLEMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            Int32 InvID = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            if (InvID == -1) return UtilDL.GetDBOperationFailed();

            cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_Settlement_Status", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_SettlementDate", settlementDate);  // WALI :: 06-May-2015

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_INVESTMENT_SETTLEMENT_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Provision For Mismatched Profit
            if (Convert.ToDecimal(stringDS.DataStrings[3].StringValue) != 0)
            {
                cmd.Dispose();
                cmd.CommandText = "PRO_BF_PROVISION_CREATE_ST";
                cmd.CommandType = CommandType.StoredProcedure;

                if (cmd == null) return UtilDL.GetCommandNotFound();

                cmd.Parameters.AddWithValue("p_INVID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
                cmd.Parameters.AddWithValue("p_ProvisionDate", provisionDate);
                cmd.Parameters.AddWithValue("p_Day_P", 0);
                cmd.Parameters.AddWithValue("p_Profit_P", Convert.ToDecimal(stringDS.DataStrings[3].StringValue));
                cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[4].StringValue);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_INVESTMENT_SETTLEMENT_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetProvisionBeneficiaryList_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetProvisionBeneficiaryList_BF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_PROVISION_BENEFICIARY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetInvestmentProvisionList_BF(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();


            cmd = DBCommandProvider.GetDBCommand("GetInvestmentProvisionList_BF");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_INVESTMENT_PROVISION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ProvisionCreation_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "PRO_BF_PROVISION_PROCESS";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            string ProvisionDate = stringDS.DataStrings[0].StringValue + "/01/" + stringDS.DataStrings[1].StringValue + " 12:00:01 AM";

            cmd.Parameters.AddWithValue("p_ProvisionDate", (object)Convert.ToDateTime(ProvisionDate));
            cmd.Parameters.AddWithValue("p_LoginID", (object)stringDS.DataStrings[2].StringValue);
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_PROVISION_CREATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }

        private DataSet _SaveTermsOfSeperation_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            bfDS.Merge(inputDS.Tables[bfDS.TermsOfSeparation_BF.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            cmd.CommandText = "PRO_BF_TERM_OF_SEPARATE_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd_Del.CommandText = "PRO_BF_TERM_OF_SEPARATE_DEL";
            cmd_Del.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmd_Del == null) return UtilDL.GetCommandNotFound();

            #region Delete Old Terms
            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_TERMS_OF_SEPARATION_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Create New Terms
            if (bfDS.TermsOfSeparation_BF.Rows.Count > 0)
            {
                foreach (BenevolentFund.TermsOfSeparation_BFRow row in bfDS.TermsOfSeparation_BF.Rows)
                {
                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1) return UtilDL.GetDBOperationFailed();

                    cmd.Parameters.AddWithValue("p_TermsOfSeparationID", genPK);
                    cmd.Parameters.AddWithValue("p_ServiceYear", row.ServiceYear);
                    cmd.Parameters.AddWithValue("p_PaymentPercent_Principal", row.PaymentPercent_Principal);
                    cmd.Parameters.AddWithValue("p_PaymentPercent_Profit", row.PaymentPercent_Profit);
                    cmd.Parameters.AddWithValue("p_DependsOn", bfDS.TermsOfSeparation_BF[0].DependsOn);
                    bError = false;
                    nRowAffected = -1;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BF_TERMS_OF_SEPARATION_SAVE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion
            return errDS;
        }
        private DataSet _GetTermsOfSeperation_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetTermsOfSeparation_BF");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.TermsOfSeparation_BF.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_TERMS_OF_SEPARATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetEmployeeListForSeperation_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_ForSeperation_BF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_EMPLOYEE_LIST_FOR_SEPERATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetEmployeeListForPayment_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_ForPayment_BF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["PaymentStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);   // Added ::WALI :: 19-Apr-2015
            cmd.Parameters["PaymentStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);   // Added ::WALI :: 19-Apr-2015

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_EMPLOYEE_LIST_FOR_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetPaymentAmountsForSeperation_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPaymentAmountsForSeperation_BF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_PAYMENT_AMOUNT_FOR_SEPERATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _EmployeeSeperation_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            OleDbCommand cmd_Sep = new OleDbCommand();
            OleDbCommand cmd_Head = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            bfDS.Merge(inputDS.Tables[bfDS.BF_Employee_Payment.TableName], false, MissingSchemaAction.Error);
            bfDS.Merge(inputDS.Tables[bfDS.BF_Head_GL.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            cmd_Sep.CommandText = "PRO_BF_EMPLOYEE_SEPERATION";
            cmd_Sep.CommandType = CommandType.StoredProcedure;

            cmd_Head.CommandText = "PRO_BF_HEADGL_ADD_FOR_LF";
            cmd_Head.CommandType = CommandType.StoredProcedure;

            if (cmd_Sep == null || cmd_Head == null) return UtilDL.GetCommandNotFound();

            #region Employee Seperation
            cmd_Sep.Parameters.AddWithValue("p_EmployeeID", bfDS.BF_Employee_Payment[0].EmployeeID);
            cmd_Sep.Parameters.AddWithValue("p_Principal", bfDS.BF_Employee_Payment[0].Principal);
            cmd_Sep.Parameters.AddWithValue("p_Profit", bfDS.BF_Employee_Payment[0].Profit);
            cmd_Sep.Parameters.AddWithValue("p_Payment_Date", bfDS.BF_Employee_Payment[0].SeperationDate);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Sep, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd_Sep.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_EMPLOYEE_SEPARATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region For Differences [Forfeirute]
            if (bfDS.BF_Head_GL.Rows.Count > 0)
            {
                foreach (BenevolentFund.BF_Head_GLRow row in bfDS.BF_Head_GL.Rows)
                {
                    cmd_Head.Parameters.AddWithValue("p_RefHeadCode", row.RefHeadCode);
                    cmd_Head.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                    cmd_Head.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                    cmd_Head.Parameters.AddWithValue("p_Ref_EmployeeID", row.Ref_EmployeeID);
                    cmd_Head.Parameters.AddWithValue("p_Calculated_GL_Amount", row.GL_Amount);      // WALI :: 09-Mar-2015

                    bError = false;
                    nRowAffected = -1;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Head, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Head.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BF_EMPLOYEE_SEPARATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion

            #region Fund Journal Add for 'PAYABLE TO LSC'
            if (bfDS.BF_Employee_Payment[0].Payment_Source == "Donor")
            {
                OleDbCommand cmd_Fund = new OleDbCommand();
                cmd_Fund.CommandText = "PRO_BF_FUND_RECIEVE";
                cmd_Fund.CommandType = CommandType.StoredProcedure;
                if (cmd_Fund == null) return UtilDL.GetCommandNotFound();

                decimal gl_Amount = bfDS.BF_Employee_Payment[0].Principal + bfDS.BF_Employee_Payment[0].Profit;

                cmd_Fund.Parameters.AddWithValue("p_HeadID", 108);          // HeadID for 'Payable to LSC'
                cmd_Fund.Parameters.AddWithValue("p_GL_Amount", gl_Amount);
                cmd_Fund.Parameters.AddWithValue("p_Event_Date", bfDS.BF_Employee_Payment[0].SeperationDate);
                cmd_Fund.Parameters.AddWithValue("p_Ref_EmployeeID", bfDS.BF_Employee_Payment[0].EmployeeID);

                if (!bfDS.BF_Employee_Payment[0].IsDonor_ReferenceNull()) cmd_Fund.Parameters.AddWithValue("p_Ref_Other", bfDS.BF_Employee_Payment[0].Donor_Reference);
                else cmd_Fund.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);

                cmd_Fund.Parameters.AddWithValue("p_SalaryDate", DBNull.Value);
                cmd_Fund.Parameters.AddWithValue("p_Calculated_GL_Amount", gl_Amount);

                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Fund, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Fund.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_EMPLOYEE_SEPARATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            return errDS;
        }
        private DataSet _GetPaymentAmountsForPayment_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPaymentAmountsForPayment_BF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_PAYMENT_AMOUNT_FOR_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _EmployeePayment_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            OleDbCommand cmd = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            bfDS.Merge(inputDS.Tables[bfDS.BF_Employee_Payment.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            cmd.CommandText = "PRO_BF_EMPLOYEE_PAYMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_EmployeeID", bfDS.BF_Employee_Payment[0].EmployeeID);
            cmd.Parameters.AddWithValue("p_Payment_Status", true);
            cmd.Parameters.AddWithValue("p_Actual_Payment_Date", bfDS.BF_Employee_Payment[0].Actual_Payment_Date);

            if (!bfDS.BF_Employee_Payment[0].IsPayment_ReferenceNull()) cmd.Parameters.AddWithValue("p_Payment_Reference", bfDS.BF_Employee_Payment[0].Payment_Reference);
            else cmd.Parameters.AddWithValue("p_Payment_Reference", DBNull.Value);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_EMPLOYEE_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }

        private DataSet _GetFundHeadList_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetFundHeadlist_BF");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Fund_Head.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_FUND_HEAD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetFundRecieveDetails_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataDateDS dateDS = new DataDateDS();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetFundRecieveDetails_BF");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["SalaryDate1"].Value = Convert.ToDateTime(dateDS.DataDates[0].DateValue);
            cmd.Parameters["SalaryDate2"].Value = Convert.ToDateTime(dateDS.DataDates[0].DateValue);
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_FUND_RECIEVE_DETAILS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _FundRecieve_BF(DataSet inputDS)
        {


            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "Pro_BF_FUND_RECEIVE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_SalaryDate", Convert.ToDateTime(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_Event_Date", Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_Ref_Other", stringDS.DataStrings[2].StringValue);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_FUND_RECIEVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;

        }

        private DataSet _GetProvisionCalculatedData_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetProvisionCalculatedData_BF");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["ProvisionDate"].Value = stringDS.DataStrings[0].StringValue;
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_PROVISION_CALCULATED_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _SaveProvisionCalculatedData_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            BenevolentFund bfDS = new BenevolentFund();

            bfDS.Merge(inputDS.Tables[bfDS.InvestmentProvision.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd.CommandText = "PRO_BF_PROVISION_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            string ProvisionDate = stringDS.DataStrings[0].StringValue + "/01/" + stringDS.DataStrings[1].StringValue + " 12:00:01 AM";

            foreach (BenevolentFund.InvestmentProvisionRow row in bfDS.InvestmentProvision.Rows)
            {
                cmd.Parameters.AddWithValue("p_INVID", row.InvID);
                cmd.Parameters.AddWithValue("p_ProvisionDate", Convert.ToDateTime(ProvisionDate));
                cmd.Parameters.AddWithValue("p_Day_P", row.Day_P);
                cmd.Parameters.AddWithValue("p_Profit_P", row.Profit_P);
                cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[2].StringValue);
                bool bError = false;
                int nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_SAVE_PROVISION_CALCULATED_DATA.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;
        }
        private DataSet _GetProvisionedProfit_ForEmployee_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetProvisionedProfit_ForEmployee_BF");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_PROVISIONED_PROFIT_FOR_EMPLOYEE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _DistributeProvisionedProfit_BF(DataSet inputDS)
        {


            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Upd = new OleDbCommand();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;


            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "Pro_BF_INV_PROFIT_DIST";
            cmd.CommandType = CommandType.StoredProcedure;


            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_Event_Date", Convert.ToDateTime(stringDS.DataStrings[0].StringValue));


            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_DISTRIBUTE_PROVISIONED_PROFIT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;

        }
        private DataSet GetDistributedProvisionData_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetDistributedProvisionData_BF");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_DISTRIBUTED_PROVISION_DATA.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetPendingDistributions_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetPendingDistributions_BF");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.InvestmentProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_PENDING_DISTRIBUTION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _FundJournalEntry_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_BF_FUND_RECIEVE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters.AddWithValue("p_HeadID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_GL_Amount", Convert.ToDecimal(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_Event_Date", Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture));
            cmd.Parameters.AddWithValue("p_Ref_EmployeeID", DBNull.Value);

            if (stringDS.DataStrings[3].StringValue.Trim() == "") cmd.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);
            else cmd.Parameters.AddWithValue("p_Ref_Other", stringDS.DataStrings[3].StringValue);

            cmd.Parameters.AddWithValue("p_SalaryDate", DBNull.Value);
            cmd.Parameters.AddWithValue("p_Calculated_GL_Amount", Convert.ToDecimal(stringDS.DataStrings[1].StringValue));     // WALI :: 09-Mar-2015

            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_FUND_JOURNAL_ENTRY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;
        }
        private DataSet _GetHeadWiseAvailableAmount_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataIntegerDS integerDS = new DataIntegerDS();

            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetHeadWiseAvailableAmount_ByFundYear_BF");
            adapter.SelectCommand = cmd;

            cmd.Parameters["HeadID1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["HeadID2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["FundYear"].Value = integerDS.DataIntegers[1].IntegerValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Fund_Head.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_HEADWISE_AVAILABLE_AMOUNT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetEmployeeList_By_BFMembDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataDateDS dateDS = new DataDateDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetEmployeeList_By_BFMembDate");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["BFMembDate1"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmpStatus1"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmpStatus2"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["BFMembDate2"].Value = dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmpStatus3"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["EmpStatus4"].Value = integerDS.DataIntegers[0].IntegerValue;

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_JOURNAL_EMPLOYEE_BY_BF_MEMBDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _EmployeeJournalEntry_BF(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;


            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "Pro_BF_EMP_JOURNAL_ENTRY";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();


            cmd.Parameters.AddWithValue("p_SalaryYearMonthDate", Convert.ToDateTime(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_BF_membershipDate", Convert.ToDateTime(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_TransactionDate", Convert.ToDateTime(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_FundHeadID", Convert.ToInt32(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_WithSeperated", Convert.ToBoolean(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_TransactionAmount", Convert.ToDecimal(stringDS.DataStrings[5].StringValue));

            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_EMPLOYEE_JOURNAL_ENTRY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }


            return errDS;

        }

        private DataSet _GetDistinctFundYear_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataIntegerDS integerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetDistinctFundYear_BF");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, integerDS, integerDS.DataIntegers.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_DISTINCT_FUND_YEAR_BF.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            integerDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(integerDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetRecievedFundList_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataDateDS dateDS = new DataDateDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetRecievedFundList_BF");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, dateDS, dateDS.DataDates.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_RECIEVED_FUND_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            dateDS.AcceptChanges();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(dateDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet GetTotalPayableToLSC_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetTotalPayableToLSC_BF");
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_TOTAL_PAYABLE_TO_LSC.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetEmployeeBalanceRecord_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "Principal") cmd = DBCommandProvider.GetDBCommand("GetEmployeePrincipalAmounts_BF");
            else if (stringDS.DataStrings[0].StringValue == "Profit") cmd = DBCommandProvider.GetDBCommand("GetEmployeeProvisionDistributions_BF");
            else if (stringDS.DataStrings[0].StringValue == "Others") cmd = DBCommandProvider.GetDBCommand("GetEmployeeOtherAmounts_BF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[6].StringValue, CultureInfo.InvariantCulture);
            cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[7].StringValue, CultureInfo.InvariantCulture);
            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[2].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[5].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[5].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_EMPLOYEE_BALANCE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ModifyEmployeeBalanceRecord_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            BenevolentFund bfDS = new BenevolentFund();
            MessageDS messageDS = new MessageDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            bfDS.Merge(inputDS.Tables[bfDS.BF_Employee.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "Principal")
            {
                #region Modify Principal Amount(s)
                cmd.CommandText = "PRO_BF_EDIT_EMP_PRINCIPAL";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (BenevolentFund.BF_EmployeeRow row in bfDS.BF_Employee.Rows)
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                    cmd.Parameters.AddWithValue("p_SalaryDate", row.Event_Date);
                    cmd.Parameters.AddWithValue("p_Amount", row.Amount);
                    cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BF_MODIFY_EMPLOYEE_BALANCE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                }
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Profit")
            {
                #region Modify Profit Amount(s)
                cmd.CommandText = "PRO_BF_EDIT_EMP_PROFIT";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (BenevolentFund.BF_EmployeeRow row in bfDS.BF_Employee.Rows)
                {
                    cmd.Parameters.AddWithValue("p_ProfitGLID", row.Row_ID);
                    cmd.Parameters.AddWithValue("p_Amount", row.Amount);
                    cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BF_MODIFY_EMPLOYEE_BALANCE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                }
                #endregion
            }
            else if (stringDS.DataStrings[0].StringValue == "Others")
            {
                #region Modify Other Amount(s)
                cmd.CommandText = "PRO_BF_EDIT_EMP_OTHER_AMNT";
                cmd.CommandType = CommandType.StoredProcedure;
                if (cmd == null) return UtilDL.GetCommandNotFound();

                foreach (BenevolentFund.BF_EmployeeRow row in bfDS.BF_Employee.Rows)
                {
                    cmd.Parameters.AddWithValue("p_ProfitGLID", row.Row_ID);
                    cmd.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                    cmd.Parameters.AddWithValue("p_Amount", row.Amount);
                    cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BF_MODIFY_EMPLOYEE_BALANCE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        //return errDS;
                    }
                    if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                    else messageDS.ErrorMsg.AddErrorMsgRow(row.EmployeeCode + " - " + row.EmployeeName + " [" + row.Event_Date.ToString("MMM, yyyy") + "]");
                }
                #endregion
            }

            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetFundHeadBalanceRecord_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            DataDateDS dateDS = new DataDateDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetFundHeadBalanceRecord_BF");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["HeadID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);
            cmd.Parameters["DateFrom"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue, CultureInfo.InvariantCulture);
            cmd.Parameters["DateTo"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue, CultureInfo.InvariantCulture);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_FUND_BALANCE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _ModifyFundHeadBalanceRecord_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            BenevolentFund bfDS = new BenevolentFund();
            MessageDS messageDS = new MessageDS();
            bool bError = false;
            int nRowAffected = -1;

            bfDS.Merge(inputDS.Tables[bfDS.BF_Head_GL.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_BF_EDIT_FUND_BALANCE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (BenevolentFund.BF_Head_GLRow row in bfDS.BF_Head_GL.Rows)
            {
                cmd.Parameters.AddWithValue("p_HeadGLID", row.Row_ID);
                cmd.Parameters.AddWithValue("p_Changed_GL_Amount", row.GL_Amount);
                cmd.Parameters.AddWithValue("p_Changed_By", row.Changed_By);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_MODIFY_EMPLOYEE_BALANCE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.Event_Date.ToString("dd-MMM-yyyy") + "  [" + row.Description + "]");
                else messageDS.ErrorMsg.AddErrorMsgRow(row.Event_Date.ToString("dd-MMM-yyyy") + "  [" + row.Description + "]");
            }

            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetBeneficiaryList_To_FundRecieve_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetBeneficiaryList_To_FundRecieve_BF");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            long siteID = UtilDL.GetSiteId(connDS, stringDS.DataStrings[2].StringValue);

            //cmd.Parameters["BenevolentFundID"].Value = Convert.ToInt32(stringDS.DataStrings[6].StringValue);
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue.ToUpper() + "%";
            cmd.Parameters["SiteID1"].Value = siteID;
            cmd.Parameters["SiteID2"].Value = siteID;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["EmployeeStatus1"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            cmd.Parameters["EmployeeStatus2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Head_GL.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_BENEFICIARY_LIST_TO_FUND_RECIEVE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _Exceptional_FundRecieve_BF(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            bfDS.Merge(inputDS.Tables[bfDS.BF_Head_GL.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            cmd_Del.CommandText = "PRO_BF_FUND_RECIEVE_DEL_EMP";
            cmd_Del.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "PRO_BF_FUND_RECIEVE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd_Del == null || cmd == null) return UtilDL.GetCommandNotFound();

            foreach (BenevolentFund.BF_Head_GLRow row in bfDS.BF_Head_GL.Rows)
            {
                #region First Delete prior FundRecieve of this (SalaryMonth+Employee)
                cmd_Del.Parameters.AddWithValue("p_SalaryDate", row.SalaryDate);
                cmd_Del.Parameters.AddWithValue("p_EmployeeID", row.Ref_EmployeeID);
                cmd_Del.Parameters.AddWithValue("p_HeadID", row.HeadID);
                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Del.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_FUND_RECIEVE_EMPLOYEE_WISE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion

                #region Then, Create new Fund Recieve
                cmd.Parameters.AddWithValue("p_HeadID", row.HeadID);
                cmd.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                cmd.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                cmd.Parameters.AddWithValue("p_Ref_EmployeeID", row.Ref_EmployeeID);

                if (!row.IsRef_OtherNull()) cmd.Parameters.AddWithValue("p_Ref_Other", row.Ref_Other);
                else cmd.Parameters.AddWithValue("p_Ref_Other", DBNull.Value);

                cmd.Parameters.AddWithValue("p_SalaryDate", row.SalaryDate);
                cmd.Parameters.AddWithValue("p_Calculated_GL_Amount", row.GL_Amount);

                bError = false;
                nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_FUND_RECIEVE_EMPLOYEE_WISE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #endregion
            }
            return errDS;
        }
        private DataSet _GetBF_EmpListForGenaralPayment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBF_EmpListForGenaralPayment");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_EMPLOYEE_LIST_FOR_SEPERATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _BF_Payment_General(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            OleDbCommand cmd_Sep = new OleDbCommand();
            OleDbCommand cmd_Head = new OleDbCommand();
            bool bError;
            int nRowAffected;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            bfDS.Merge(inputDS.Tables[bfDS.BF_Employee_Payment.TableName], false, MissingSchemaAction.Error);
            bfDS.Merge(inputDS.Tables[bfDS.BF_Head_GL.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            cmd_Sep.CommandText = "PRO_BF_PAYMENT_GENERAL";
            cmd_Sep.CommandType = CommandType.StoredProcedure;

            cmd_Head.CommandText = "PRO_BF_HEADGL_ADD_FOR_LF";
            cmd_Head.CommandType = CommandType.StoredProcedure;

            if (cmd_Sep == null || cmd_Head == null) return UtilDL.GetCommandNotFound();

            #region Employee Seperation
            cmd_Sep.Parameters.AddWithValue("p_EmployeeID", bfDS.BF_Employee_Payment[0].EmployeeID);
            cmd_Sep.Parameters.AddWithValue("p_BF_Comp", bfDS.BF_Employee_Payment[0].Principal);
            cmd_Sep.Parameters.AddWithValue("p_BF_Comp_Profit", bfDS.BF_Employee_Payment[0].Profit);
            cmd_Sep.Parameters.AddWithValue("p_Payment_Date", bfDS.BF_Employee_Payment[0].SeperationDate);
            bError = false;
            nRowAffected = -1;

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Sep, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd_Sep.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_EMPLOYEE_SEPARATION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region For Differences [Forfeirute]
            if (bfDS.BF_Head_GL.Rows.Count > 0)
            {
                foreach (BenevolentFund.BF_Head_GLRow row in bfDS.BF_Head_GL.Rows)
                {
                    cmd_Head.Parameters.AddWithValue("p_RefHeadCode", row.RefHeadCode);
                    cmd_Head.Parameters.AddWithValue("p_GL_Amount", row.GL_Amount);
                    cmd_Head.Parameters.AddWithValue("p_Event_Date", row.Event_Date);
                    cmd_Head.Parameters.AddWithValue("p_Ref_EmployeeID", row.Ref_EmployeeID);
                    cmd_Head.Parameters.AddWithValue("p_Calculated_GL_Amount", row.GL_Amount);      // WALI :: 09-Mar-2015

                    bError = false;
                    nRowAffected = -1;

                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Head, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd_Head.Parameters.Clear();
                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_BF_EMPLOYEE_SEPARATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            #endregion


            return errDS;
        }
        private DataSet _GetBF_PaymentAmountsGeneral(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataSet returnDS = new DataSet();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetBF_PaymentAmountsGeneral");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_Employee_Payment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_PAYMENT_AMOUNT_GENERAL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _GetBF_SeparationInfo(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "BF_SeparationInfo")
            {
                cmd = DBCommandProvider.GetDBCommand("GetBF_SeparationInformtion");
                adapter.SelectCommand = cmd;

                cmd.Parameters["Date_From"].Value = Convert.ToDateTime(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["Date_To"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
            }



            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_SeparationInfo.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_SEPARATION_INFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createBF_Gradewise(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BenevolentFund bfDS = new BenevolentFund();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bfDS.Merge(inputDS.Tables[bfDS.BF_Gradewise.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            #region First Delete Old items Records
            OleDbCommand cmdDel = new OleDbCommand();
            cmdDel.CommandText = "PRO_BF_GRADEWISE_DELETE";
            cmdDel.CommandType = CommandType.StoredProcedure;
            cmdDel.Parameters.AddWithValue("p_FT_ID", bfDS.BF_Gradewise[0].Ft_ID);
            
            if (cmdDel == null) return UtilDL.GetCommandNotFound();

            bool bErrorDel = false;
            int nRowAffectedDel = -1;
            nRowAffectedDel = ADOController.Instance.ExecuteNonQuery(cmdDel, connDS.DBConnections[0].ConnectionID, ref bErrorDel);
            cmdDel.Parameters.Clear();
            if (bErrorDel)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_BF_GRADEWISE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
          
            #endregion

            #region Create
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BF_GRADEWISE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (BenevolentFund.BF_GradewiseRow row in bfDS.BF_Gradewise.Rows)
            {
                cmd.Parameters.AddWithValue("p_FT_ID", row.Ft_ID);
                cmd.Parameters.AddWithValue("p_FT_GradeID", row.Ft_GradeID);
                cmd.Parameters.AddWithValue("p_FT_NoOfShare", row.Ft_NoOfShare);
                cmd.Parameters.AddWithValue("p_FT_PercentOfBasic_Self", row.Ft_PercentofBasic_Self);
                cmd.Parameters.AddWithValue("p_FT_FlatAmount_Self", row.Ft_FlatAmount_Self);
                cmd.Parameters.AddWithValue("p_FT_PercentOfBasic_Comp", row.Ft_PercentOfBasic_Comp);
                cmd.Parameters.AddWithValue("p_FT_FlatAmount_Comp", row.Ft_FlatAmount_Comp);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_GRADEWISE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetBF_Data_ForReport(DataSet inputDS)
        {
            DataStringDS stringDS = new DataStringDS();
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool bError = false;
            int nRowAffected = -1;

            if (stringDS.DataStrings[0].StringValue == "BF_StatementReport")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_BFStatementForReport");
                adapter.SelectCommand = cmd;

                cmd.Parameters["BF_FundType"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["Year"].Value = Convert.ToDateTime(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["Month"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["BF_FundType1"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["Year1"].Value = Convert.ToDateTime(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode3"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode4"].Value = stringDS.DataStrings[3].StringValue;

               
                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_StatementReport.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
                
                bfDS.AcceptChanges();
                cmd.Parameters.Clear();
                cmd.Dispose();
            }
            else if (stringDS.DataStrings[0].StringValue == "OCS_StatementReport")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_OCSStatementReport");
                adapter.SelectCommand = cmd;

                cmd.Parameters["MonthYearDate1"].Value =  Convert.ToDateTime(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode8"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode9"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["MonthYearDate2"].Value = Convert.ToDateTime(stringDS.DataStrings[4].StringValue);
                cmd.Parameters["MonthYearDate3"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["Year1"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue).Year;
                cmd.Parameters["EmployeeCode3"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode4"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["MonthYearDate4"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["EmployeeCode5"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode6"].Value = stringDS.DataStrings[3].StringValue;

                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_StatementReport.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

                bfDS.AcceptChanges();
                cmd.Parameters.Clear();
                cmd.Dispose();
            }
            else if (stringDS.DataStrings[0].StringValue == "OCS_StatementReport_BulkEmployee")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_OCSStatementReport_BulkEmployee");
                adapter.SelectCommand = cmd;

                cmd.Parameters["MonthYearDate1"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode8"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode9"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["Year1"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue).Year;
                cmd.Parameters["Year2"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue).Year;
                cmd.Parameters["EmployeeCode3"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode4"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["MonthYearDate2"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["EmployeeCode5"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["EmployeeCode6"].Value = stringDS.DataStrings[3].StringValue;

                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_StatementReport.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

                bfDS.AcceptChanges();
                cmd.Parameters.Clear();
                cmd.Dispose();
            }
            else if (stringDS.DataStrings[0].StringValue == "OCS_StatementReport_Summary")
            {
                cmd = DBCommandProvider.GetDBCommand("Get_OCSStatementReport_Summary");
                adapter.SelectCommand = cmd;

                cmd.Parameters["Year1"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue).Year;
                cmd.Parameters["Year2"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue).Year;
                cmd.Parameters["Year3"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue).Year;
                //cmd.Parameters["Year4"].Value = Convert.ToDateTime(stringDS.DataStrings[2].StringValue).Year;

                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_StatementReport.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

                bfDS.AcceptChanges();
                cmd.Parameters.Clear();
                cmd.Dispose();
            }
            else if (stringDS.DataStrings[0].StringValue == "BF_InvestmentRecovery")
            {
                cmd = DBCommandProvider.GetDBCommand("GetBF_InvestmentRecovery");
                adapter.SelectCommand = cmd;

                cmd.Parameters["FundType"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[2].StringValue;

                nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_InvestmentReport.TableName, connDS.DBConnections[0].ConnectionID, ref bError);

                bfDS.AcceptChanges();
                cmd.Parameters.Clear();
                cmd.Dispose();
            }
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_BF_DATA_FOR_REPORT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        #region RONY :: 09 Jan 2017 :: OCS Share
        private DataSet _createOCS_Share(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_OCS_SHARE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            BenevolentFund bfDS = new BenevolentFund();

            bfDS.Merge(inputDS.Tables[bfDS.OCS_Share.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create OCS Share

            foreach (BenevolentFund.OCS_ShareRow row in bfDS.OCS_Share.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_OCS_ShareID", genPK);
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                cmd.Parameters.AddWithValue("p_PurchaseDate", row.PurchaseDate);
                cmd.Parameters.AddWithValue("p_NoOfShare", row.NoOfShare);
                cmd.Parameters.AddWithValue("p_FaceValue", row.FaceValue);
                cmd.Parameters.AddWithValue("p_TotalPrice", row.TotalPrice);
                cmd.Parameters.AddWithValue("p_SalaryID", DBNull.Value);
                cmd.Parameters.AddWithValue("p_IsCashInHand", row.IsCashInHand);
                cmd.Parameters.AddWithValue("p_FundTypeID", row.FT_ID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID,
                   ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OCS_SHARE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow("[" + row.EmployeeCode + "] " + row.EmployeeName + " --> " + row.PurchaseDate.ToString("MMMM-yyyy") + "[" + row.FT_Name + "]");
                else messageDS.ErrorMsg.AddErrorMsgRow("[" + row.EmployeeCode + "] " + row.EmployeeName + " --> " + row.PurchaseDate.ToString("MMMM-yyyy") + "[" + row.FT_Name + "]");
                messageDS.AcceptChanges();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _updateOCS_Share(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            BenevolentFund bfDS = new BenevolentFund();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            bfDS.Merge(inputDS.Tables[bfDS.OCS_Share.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_OCS_SHARE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (BenevolentFund.OCS_ShareRow row in bfDS.OCS_Share.Rows)
            {
                cmd.Parameters.AddWithValue("p_OCS_ShareID", row.OCS_ShareID);
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                cmd.Parameters.AddWithValue("p_PurchaseDate", row.PurchaseDate);
                cmd.Parameters.AddWithValue("p_NoOfShare", row.NoOfShare);
                cmd.Parameters.AddWithValue("p_FaceValue", row.FaceValue);
                cmd.Parameters.AddWithValue("p_TotalPrice", row.TotalPrice);
                cmd.Parameters.AddWithValue("p_IsCashInHand", row.IsCashInHand);
                cmd.Parameters.AddWithValue("p_FundTypeID", row.FT_ID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OCS_SHARE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow("[" + row.EmployeeCode + "] " + row.EmployeeName + " --> " + row.PurchaseDate.ToString("MMMM-yyyy") + "[" + row.FT_Name + "]");
                else messageDS.ErrorMsg.AddErrorMsgRow("[" + row.EmployeeCode + "] " + row.EmployeeName + " --> " + row.PurchaseDate.ToString("MMMM-yyyy") + "[" + row.FT_Name + "]");
                messageDS.AcceptChanges();
            }

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _deleteOCS_Share(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            MessageDS messageDS = new MessageDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            bfDS.Merge(inputDS.Tables[bfDS.OCS_Share.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_OCS_SHARE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (BenevolentFund.OCS_ShareRow row in bfDS.OCS_Share.Rows)
            {
                cmd.Parameters.AddWithValue("p_OCS_ShareID", row.OCS_ShareID);
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_OCS_SHARE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow("[" + row.EmployeeCode + "] " + row.EmployeeName + " --> " + row.PurchaseDate.ToString("MMMM-yyyy"));
                else messageDS.ErrorMsg.AddErrorMsgRow("[" + row.EmployeeCode + "] " + row.EmployeeName + " --> " + row.PurchaseDate.ToString("MMMM-yyyy"));
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        private DataSet _GetBF_EmpListFor_FundPayment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            BenevolentFund bfDS = new BenevolentFund();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetBF_EmpListFor_FundPayment");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["FT_Code"].Value = stringDS.DataStrings[5].StringValue;
            cmd.Parameters["LoanType1"].Value = stringDS.DataStrings[6].StringValue;
            cmd.Parameters["LoanType2"].Value = stringDS.DataStrings[6].StringValue;
            cmd.Parameters["EmployeeName"].Value = "%" + stringDS.DataStrings[1].StringValue + "%";
            cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteCode1"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            cmd.Parameters["DepartmentCode1"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[3].StringValue;
            cmd.Parameters["DesignationCode1"].Value = stringDS.DataStrings[4].StringValue;
            cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[4].StringValue;

            adapter.SelectCommand = cmd;
            bool bError = false;
            int nRowAffected = -1;

            nRowAffected = ADOController.Instance.Fill(adapter, bfDS, bfDS.BF_FundPayment.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_BF_EMP_LIST_FOR_FUND_PAYMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            bfDS.AcceptChanges();
            cmd.Parameters.Clear();
            cmd.Dispose();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(bfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createBF_FundPayment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            MessageDS messageDS = new MessageDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_BF_FUND_PAYMENT";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            BenevolentFund bfDS = new BenevolentFund();

            bfDS.Merge(inputDS.Tables[bfDS.BF_FundPayment.TableName], false, MissingSchemaAction.Error);
            bfDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create Benevolent Fund Payment

            foreach (BenevolentFund.BF_FundPaymentRow row in bfDS.BF_FundPayment.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_FundPaymentID", genPK);
                cmd.Parameters.AddWithValue("p_EmployeeCode", row.EmployeeCode);
                cmd.Parameters.AddWithValue("p_FT_ID", row.FT_ID);
                cmd.Parameters.AddWithValue("p_PurchaseDate", row.PaymentDate);
                cmd.Parameters.AddWithValue("p_PrincipalAmount", row.PrincipalAmount);
                cmd.Parameters.AddWithValue("p_ProfitAmount", row.ProfitAmount);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_BF_FUND_PAYMENT.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow("[" + row.EmployeeCode + "] " + row.EmployeeName + " --> " + row.PaymentDate.ToString("MMMM-yyyy")) ;
                else messageDS.ErrorMsg.AddErrorMsgRow("[" + row.EmployeeCode + "] " + row.EmployeeName + " --> " + row.PaymentDate.ToString("MMMM-yyyy"));
                messageDS.AcceptChanges();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
    }
}
