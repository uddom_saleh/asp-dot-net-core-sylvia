using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;

namespace Sylvia.DAL.Payroll
{
	/// <summary>
	/// Summary description for IDProviderDL.
	/// </summary>
	public class IDProviderDL
	{
		public IDProviderDL()
		{

		}

    public DataSet Execute(DataSet inputDS)
    {
      DataSet returnDS = new DataSet();
      ErrorDS errDS = new ErrorDS();

      ActionDS actionDS = new  ActionDS();
      actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error );
      actionDS.AcceptChanges();

      ActionID actionID = ActionID.ACTION_UNKOWN_ID ; // just set it to a default
      try
      {
        actionID = (ActionID) Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true );
      }
      catch
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString() ;
        err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString() ;
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString() ;
        err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
        errDS.Errors.AddError(err );
        errDS.AcceptChanges();
        return errDS;
      }

      switch ( actionID )
      {
        case ActionID.ACTION_NEXT_ID_GET:
          return this._getNextID();   
        default:
          Util.LogInfo("Action ID is not supported in this class."); 
          ErrorDS.Error err = errDS.Errors.NewError();
          err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
          err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
          err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
          err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString() ;
          errDS.Errors.AddError( err );
          returnDS.Merge ( errDS );
          returnDS.AcceptChanges();
          return returnDS;
      }

    }

    private DataSet _getNextID()
    {
      DataSet returnDS = new DataSet();
      DataLongDS longDS = new DataLongDS();
      ErrorDS errDS = new ErrorDS();


      long id = IDGenerator.GetNextGenericID(); 
      if ( id == -1 )
      {
        ErrorDS.Error err = errDS.Errors.NewError();
        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        err.ErrorLevel = ErrorLevel.ERR_LEVEL_WARNING.ToString();
        err.ErrorInfo1 = "Could not generater Generic ID";
        errDS.Errors.AddError( err );
        errDS.AcceptChanges();
        return errDS;
      }


      longDS.DataLongs.AddDataLong(id);
      longDS.AcceptChanges();

      returnDS.Merge( longDS );
      returnDS.Merge( errDS );
      returnDS.AcceptChanges();
      return returnDS;

    }




	}  // end of class
}  // end of namespace
