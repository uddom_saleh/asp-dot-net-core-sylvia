using System;
using System.Data;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for TransactionProviderDL.
    /// </summary>
    public class TransactionProviderDL
    {
        public TransactionProviderDL()
        {
        }
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_DB_CONNECTION_GET:
                    return this._getNewConnection();
                case ActionID.ACTION_DB_CONNECTION_CLOSE:
                    return _closeConnection(inputDS);
                case ActionID.ACTION_DB_TRANSACTION_GET:
                    return _getNewTransaction();
                case ActionID.ACTION_DB_TRANSACTION_COMMIT:
                    return _commitTransaction(inputDS);
                case ActionID.ACTION_DB_TRANSACTION_ROLLBACK:
                    return _rollbackTransaction(inputDS);

                case ActionID.ACTION_SQL_DB_CONNECTION_GET:
                    return _getNewConnection_SQL_Att();

                //case ActionID.ACTION_DB_CONNECTION_GET_Ora:
                //    return this._getNewConnection_Ora();
                //case ActionID.ACTION_DB_CONNECTION_CLOSE_Ora:
                //    return _closeConnection_Ora(inputDS);
                //case ActionID.ACTION_DB_TRANSACTION_GET_Ora:
                //    return _getNewTransaction_Ora();
                //case ActionID.ACTION_DB_TRANSACTION_COMMIT_Ora:
                //    return _commitTransaction_Ora(inputDS);
                //case ActionID.ACTION_DB_TRANSACTION_ROLLBACK_Ora:
                //    return _rollbackTransaction_Ora(inputDS);

                //case ActionID.ACTION_DB_CONNECTION_GET_PG:
                //    return this._getNewConnection_PG();
                //case ActionID.ACTION_DB_CONNECTION_CLOSE_PG:
                //    return _closeConnection_PG(inputDS);
                //case ActionID.ACTION_DB_TRANSACTION_GET_PG:
                //    return _getNewTransaction_PG();
                //case ActionID.ACTION_DB_TRANSACTION_COMMIT_PG:
                //    return _commitTransaction_PG(inputDS);
                //case ActionID.ACTION_DB_TRANSACTION_ROLLBACK_PG:
                //    return _rollbackTransaction_PG(inputDS);

                case ActionID.ACTION_DB_TRANSACTION_GET_REC:
                    return _getNewTransaction_Rec();
                case ActionID.ACTION_DB_CONNECTION_GET_REC:
                    return _getNewConnection_Rec();

                case ActionID.ACTION_DB_TRANSACTION_GET_CBS_ORA:
                    return _getNewTransaction_ORA_CBS();


                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }

        }
        private DataSet _getNewTransaction()
        {
            ErrorDS errDS = new ErrorDS();
            int connID = ADOController.Instance.BeginNewTransaction();
            if (connID == -1)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_TRANSACTION_FAILED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorInfo1 = "Should check log file.";
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.DBConnections.AddDBConnection(connID);
            connDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(errDS);
            returnDS.Merge(connDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _commitTransaction(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);

            bool bOk = ADOController.Instance.CommitTransaction(connDS.DBConnections[0].ConnectionID);
            if (bOk == false) // faled
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_COMMIT_FAILED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorInfo1 = "Should check log file.";
                err.ErrorInfo2 = "Connection ID: " + connDS.DBConnections[0].ConnectionID;
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;

        }

        private DataSet _rollbackTransaction(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);

            bool bOk = ADOController.Instance.RollbackTransaction(connDS.DBConnections[0].ConnectionID);
            if (bOk == false) // faled
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_ROLLBACK_FAILED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorInfo1 = "Should check log file.";
                err.ErrorInfo2 = "Connection ID: " + connDS.DBConnections[0].ConnectionID;
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;

        }

        private DataSet _getNewConnection()
        {
            ErrorDS errDS = new ErrorDS();
            int connID = ADOController.Instance.OpenNewConnection();
            if (connID == -1)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_CONNECTION_FAILED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorInfo1 = "Should check log file.";
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.DBConnections.AddDBConnection(connID);
            connDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(errDS);
            returnDS.Merge(connDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _closeConnection(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);

            bool bOk = ADOController.Instance.CloseConnection(connDS.DBConnections[0].ConnectionID);
            if (bOk == false) // faled
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_CONNECTION_CLOSE_FAILED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorInfo1 = "Should check log file.";
                err.ErrorInfo2 = "Connection ID: " + connDS.DBConnections[0].ConnectionID;
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;

        }

        #region Seperate SQL Connection :: WALI :: 28-Jan-2014
        private DataSet _getNewConnection_SQL_Att()
        {
            ErrorDS errDS = new ErrorDS();
            int connID_SQL = ADOController.Instance.OpenNewConnection_SQL_Att();
            if (connID_SQL == -1)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_CONNECTION_FAILED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorInfo1 = "Should check log file.";
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.DBConnections.AddDBConnection(connID_SQL);
            connDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS);
            returnDS.Merge(connDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Seperate Ora Connection
        //private DataSet _getNewConnection_Ora()
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    int connID_Ora = ADOController.Instance.OpenNewConnection_Ora();
        //    if (connID_Ora == -1)
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_CONNECTION_FAILED.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorInfo1 = "Should check log file.";
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }

        //    DBConnectionDS connDS = new DBConnectionDS();
        //    connDS.DBConnections.AddDBConnection(connID_Ora);
        //    connDS.AcceptChanges();

        //    DataSet returnDS = new DataSet();
        //    returnDS.Merge(errDS);
        //    returnDS.Merge(connDS);
        //    returnDS.AcceptChanges();
        //    return returnDS;
        //}
        //private DataSet _closeConnection_Ora(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    DBConnectionDS connDS = new DBConnectionDS();

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);

        //    bool bOk = ADOController.Instance.CloseConnection_Ora(connDS.DBConnections[0].ConnectionID);
        //    if (bOk == false) // faled
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_CONNECTION_CLOSE_FAILED.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorInfo1 = "Should check log file.";
        //        err.ErrorInfo2 = "Connection ID: " + connDS.DBConnections[0].ConnectionID;
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }

        //    return errDS;

        //}
        //private DataSet _getNewTransaction_Ora()
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    int connID = ADOController.Instance.BeginNewTransaction_Ora();
        //    if (connID == -1)
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_TRANSACTION_FAILED.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorInfo1 = "Should check log file.";
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }

        //    DBConnectionDS connDS = new DBConnectionDS();
        //    connDS.DBConnections.AddDBConnection(connID);
        //    connDS.AcceptChanges();

        //    DataSet returnDS = new DataSet();

        //    returnDS.Merge(errDS);
        //    returnDS.Merge(connDS);
        //    returnDS.AcceptChanges();

        //    return returnDS;


        //}
        //private DataSet _commitTransaction_Ora(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    DBConnectionDS connDS = new DBConnectionDS();

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);

        //    bool bOk = ADOController.Instance.CommitTransaction_Ora(connDS.DBConnections[0].ConnectionID);
        //    if (bOk == false) // faled
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_COMMIT_FAILED.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorInfo1 = "Should check log file.";
        //        err.ErrorInfo2 = "Connection ID: " + connDS.DBConnections[0].ConnectionID;
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }

        //    return errDS;

        //}
        //private DataSet _rollbackTransaction_Ora(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    DBConnectionDS connDS = new DBConnectionDS();

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);

        //    bool bOk = ADOController.Instance.RollbackTransaction_Ora(connDS.DBConnections[0].ConnectionID);
        //    if (bOk == false) // faled
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_ROLLBACK_FAILED.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorInfo1 = "Should check log file.";
        //        err.ErrorInfo2 = "Connection ID: " + connDS.DBConnections[0].ConnectionID;
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }

        //    return errDS;

        //}

        #endregion

        #region Seperate PG Connection
        //private DataSet _getNewConnection_PG()
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    int connID_PG = ADOController.Instance.OpenNewConnection_PG();
        //    if (connID_PG == -1)
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_CONNECTION_FAILED.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorInfo1 = "Should check log file.";
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }

        //    DBConnectionDS connDS = new DBConnectionDS();
        //    connDS.DBConnections.AddDBConnection(connID_PG);
        //    connDS.AcceptChanges();

        //    DataSet returnDS = new DataSet();
        //    returnDS.Merge(errDS);
        //    returnDS.Merge(connDS);
        //    returnDS.AcceptChanges();
        //    return returnDS;
        //}
        //private DataSet _closeConnection_PG(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    DBConnectionDS connDS = new DBConnectionDS();

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);

        //    bool bOk = ADOController.Instance.CloseConnection_PG(connDS.DBConnections[0].ConnectionID);
        //    if (bOk == false) // faled
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_CONNECTION_CLOSE_FAILED.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorInfo1 = "Should check log file.";
        //        err.ErrorInfo2 = "Connection ID: " + connDS.DBConnections[0].ConnectionID;
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }

        //    return errDS;

        //}
        //private DataSet _getNewTransaction_PG()
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    int connID = ADOController.Instance.BeginNewTransaction_PG();
        //    if (connID == -1)
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_TRANSACTION_FAILED.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorInfo1 = "Should check log file.";
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }

        //    DBConnectionDS connDS = new DBConnectionDS();
        //    connDS.DBConnections.AddDBConnection(connID);
        //    connDS.AcceptChanges();

        //    DataSet returnDS = new DataSet();

        //    returnDS.Merge(errDS);
        //    returnDS.Merge(connDS);
        //    returnDS.AcceptChanges();

        //    return returnDS;


        //}
        //private DataSet _commitTransaction_PG(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    DBConnectionDS connDS = new DBConnectionDS();

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);

        //    bool bOk = ADOController.Instance.CommitTransaction_PG(connDS.DBConnections[0].ConnectionID);
        //    if (bOk == false) // faled
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_COMMIT_FAILED.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorInfo1 = "Should check log file.";
        //        err.ErrorInfo2 = "Connection ID: " + connDS.DBConnections[0].ConnectionID;
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }

        //    return errDS;

        //}
        //private DataSet _rollbackTransaction_PG(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    DBConnectionDS connDS = new DBConnectionDS();

        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);

        //    bool bOk = ADOController.Instance.RollbackTransaction_PG(connDS.DBConnections[0].ConnectionID);
        //    if (bOk == false) // faled
        //    {
        //        ErrorDS.Error err = errDS.Errors.NewError();
        //        err.ErrorCode = ErrorCode.ERR_DB_ROLLBACK_FAILED.ToString();
        //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //        err.ErrorInfo1 = "Should check log file.";
        //        err.ErrorInfo2 = "Connection ID: " + connDS.DBConnections[0].ConnectionID;
        //        errDS.Errors.AddError(err);
        //        errDS.AcceptChanges();
        //        return errDS;
        //    }

        //    return errDS;

        //}

        #endregion

        #region Recruitment
        private DataSet _getNewTransaction_Rec()
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            int connID = ADOController.Instance.BeginNewTransaction_Rec();
            if (connID == -1)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_TRANSACTION_FAILED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorInfo1 = "Should check log file.";
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.DBConnections.AddDBConnection(connID);
            connDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(connDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getNewConnection_Rec()
        {
            ErrorDS errDS = new ErrorDS();
            int connID = ADOController.Instance.OpenNewConnection_Rec();
            if (connID == -1)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_CONNECTION_FAILED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorInfo1 = "Should check log file.";
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.DBConnections.AddDBConnection(connID);
            connDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(errDS);
            returnDS.Merge(connDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        #endregion

        #region ROny :: 10 Dec 2018 :: New Connection for pushing CBS Data :: ORACLE
        private DataSet _getNewTransaction_ORA_CBS()
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            int connID = ADOController.Instance.BeginNewTransaction_ORA_CBS();
            if (connID == -1)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_TRANSACTION_FAILED.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorInfo1 = "Should check log file.";
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.DBConnections.AddDBConnection(connID);
            connDS.AcceptChanges();

            returnDS.Merge(errDS);
            returnDS.Merge(connDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

    }  // end of class
}  // end of namespace
