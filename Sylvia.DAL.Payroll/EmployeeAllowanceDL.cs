using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    class EmployeeAllowanceDL
    {
        public EmployeeAllowanceDL()
        {
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_EMPLOYEE_ALLOWANCE_ADD:
                    return _createEmployeeAllowance(inputDS);
                case ActionID.NA_ACTION_EMPLOYEE_ALLOWANCE_GET_ALL:
                    return _getEmployeeAllowanceList(inputDS);

                case ActionID.NA_ACTION_GET_EMP_ALLOWANCE_BY_PARAM:
                    return this._getEmpAllowanceListByParam(inputDS);

                case ActionID.NA_ACTION_GET_SUSPECTED_TAXABLE_INCOME:
                    return this._getSuspectedTaxableIncome(inputDS);
                case ActionID.ACTION_SUSPECTED_TAXABLE_INCOME_CREATE:
                    return this._createSuspectedTaxableIncome(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _createEmployeeAllowance(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeAllowanceDS empAllowDS = new EmployeeAllowanceDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            empAllowDS.Merge(inputDS.Tables[empAllowDS.EmpAllowance.TableName], false, MissingSchemaAction.Error);
            empAllowDS.AcceptChanges();

            bool bError;
            int nRowAffected;
            Int32 PayrollSiteID = 0;

            #region Previous Data Deletion...
            foreach (EmployeeAllowanceDS.EmpAllowanceRow Row in empAllowDS.EmpAllowance)
            {
                #region Get ID's from DataBase
                PayrollSiteID = Convert.ToInt32(UtilDL.GetPayrollSiteId(connDS, Row.PayrollSiteCode));
                #endregion

                cmd_Del.CommandText = "PRO_ALLOWANCE_DELETE";
                cmd_Del.CommandType = CommandType.StoredProcedure;

                cmd_Del.Parameters.AddWithValue("p_AllowanceID", Row.Allowance_ID);
                cmd_Del.Parameters.AddWithValue("p_SiteId", Row.SiteID);
                cmd_Del.Parameters.AddWithValue("p_PayrollSiteID", PayrollSiteID);
                cmd_Del.Parameters.AddWithValue("p_DepartmentID", Row.DepartmentID);
                cmd_Del.Parameters.AddWithValue("p_EmployeeType", Row.EmployeeType);
                cmd_Del.Parameters.AddWithValue("p_Religion", Row.Religion);
                cmd_Del.Parameters.AddWithValue("p_Gender", Row.Gender);
                //cmd_Del.Parameters.AddWithValue("p_Delete_User", Row.LogedinUserID);

                if (Row.IsLogedinUserIDNull() == false) cmd_Del.Parameters.AddWithValue("p_Delete_User", Row.LogedinUserID);
                else cmd_Del.Parameters.AddWithValue("p_Delete_User", DBNull.Value);

                cmd_Del.Parameters.AddWithValue("p_DesignationCode", Row.DesignationCode);
                cmd_Del.Parameters.AddWithValue("p_EmployeeCode", Row.EmployeeCode);
                cmd_Del.Parameters.AddWithValue("p_LiabilityCode", Row.LiabilityCode);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd_Del.Parameters.Clear();
                break;
            }
            #endregion


            #region Data Insert...
            cmd.CommandText = "PRO_EMP_ALLOWANCE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeAllowanceDS.EmpAllowanceRow empAllow in empAllowDS.EmpAllowance)
            {
                if (empAllow.IsEmployeeIDNull()) continue;

                cmd.Parameters.AddWithValue("p_ADParamID", empAllow.ADParamID);
                cmd.Parameters.AddWithValue("p_EmployeeID", empAllow.EmployeeID);
                cmd.Parameters.AddWithValue("p_Amount", empAllow.Amount);
                cmd.Parameters.AddWithValue("p_PercentOfBasic", empAllow.PercentOfBasic);
                cmd.Parameters.AddWithValue("p_AppliedStat", empAllow.AppliedStat);
                if (empAllow.IsLogedinUserIDNull() == false) cmd.Parameters.AddWithValue("p_LogedinUserID", empAllow.LogedinUserID);
                else cmd.Parameters.AddWithValue("p_Delete_User", DBNull.Value);

                if (empAllow.IsRemarksNull() == false) cmd.Parameters.AddWithValue("p_Remarks", empAllow.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                if (empAllow.IsEffect_FromNull() == false) cmd.Parameters.AddWithValue("p_EffectFrom", empAllow.Effect_From);
                else cmd.Parameters.AddWithValue("p_EffectFrom", DBNull.Value);

                if (empAllow.IsEffect_ToNull() == false) cmd.Parameters.AddWithValue("p_EffectTo", empAllow.Effect_To);
                else cmd.Parameters.AddWithValue("p_EffectTo", DBNull.Value);

                if (empAllow.IsProcessMonthNull() == false) cmd.Parameters.AddWithValue("p_processMonth", empAllow.ProcessMonth);
                else cmd.Parameters.AddWithValue("p_processMonth", DBNull.Value);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getEmployeeAllowanceList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeAllowanceList");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(26 Sep 11)...
            #region Old...
            //EmployeeAllowancePO empAllowPO = new EmployeeAllowancePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, empAllowPO, empAllowPO.EmployeeAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_CURRENCY_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //empAllowPO.AcceptChanges();

            //EmployeeAllowanceDS empAllowDS = new EmployeeAllowanceDS();
            //if (empAllowPO.EmployeeAllowance.Count > 0)
            //{
            //    foreach (EmployeeAllowancePO.EmployeeAllowanceRow poEmpAllow in empAllowPO.EmployeeAllowance)
            //    {
            //        EmployeeAllowanceDS.EmployeeAllowanceRow empAllow = empAllowDS.EmployeeAllowance.NewEmployeeAllowanceRow();
            //        if (poEmpAllow.IsAdParamCodeNull() == false)
            //        {
            //            empAllow.AdParamCode = poEmpAllow.AdParamCode;
            //        }
            //        if (poEmpAllow.IsEmployeeCodeNull() == false)
            //        {
            //            empAllow.EmployeeCode = poEmpAllow.EmployeeCode;
            //        }
            //        if (poEmpAllow.IsAmountNull() == false)
            //        {
            //            empAllow.Amount = poEmpAllow.Amount;
            //        }
            //        if (poEmpAllow.IsPercentOfBasicNull() == false)
            //        {
            //            empAllow.PercentOfBasic = poEmpAllow.PercentOfBasic;
            //        }
            //        if (poEmpAllow.IsAppliedStatNull() == false)
            //        {
            //            empAllow.AppliedStat = poEmpAllow.AppliedStat;
            //        }

            //        empAllowDS.EmployeeAllowance.AddEmployeeAllowanceRow(empAllow);
            //        empAllowDS.AcceptChanges();
            //    }
            //}
            #endregion

            EmployeeAllowanceDS empAllowDS = new EmployeeAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empAllowDS, empAllowDS.EmployeeAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_EMPLOYEE_ALLOWANCE_GET_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            empAllowDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(empAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getEmpAllowanceListByParam(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            #region Get ID's from DataBase
            Int32 PayrollSiteID = 0;
            PayrollSiteID = Convert.ToInt32(UtilDL.GetPayrollSiteId(connDS, stringDS.DataStrings[0].StringValue));
            #endregion
            if (Convert.ToBoolean(stringDS.DataStrings[4].StringValue))
            {
                if (integerDS.DataIntegers[0].IntegerValue == 1) cmd = DBCommandProvider.GetDBCommand("GetEmpAllowanceByParam_BIN");
                else if (integerDS.DataIntegers[0].IntegerValue == 2) cmd = DBCommandProvider.GetDBCommand("GetEmpAllowanceByParam_Desg_BIN");

                cmd.Parameters["ADID0"].Value = integerDS.DataIntegers[1].IntegerValue;
            }
            else
            {
                if (stringDS.DataStrings[6].StringValue == "All")
                {
                    cmd = DBCommandProvider.GetDBCommand("GetEmpAllowanceByParam_All");

                    cmd.Parameters["ADID"].Value = integerDS.DataIntegers[1].IntegerValue;
                    cmd.Parameters["ADID1"].Value = integerDS.DataIntegers[1].IntegerValue;
                    cmd.Parameters["ADID2"].Value = integerDS.DataIntegers[1].IntegerValue;
                    cmd.Parameters["SortingBy1"].Value = integerDS.DataIntegers[0].IntegerValue;
                    cmd.Parameters["SortingBy2"].Value = integerDS.DataIntegers[0].IntegerValue;
                    cmd.Parameters["SortingBy3"].Value = integerDS.DataIntegers[0].IntegerValue;
                    cmd.Parameters["SortingBy4"].Value = integerDS.DataIntegers[0].IntegerValue;
                    cmd.Parameters["SortingBy5"].Value = integerDS.DataIntegers[0].IntegerValue;
                }
                else
                {
                    if (integerDS.DataIntegers[0].IntegerValue == 1) cmd = DBCommandProvider.GetDBCommand("GetEmpAllowanceByParam");
                    else if (integerDS.DataIntegers[0].IntegerValue == 2) cmd = DBCommandProvider.GetDBCommand("GetEmpAllowanceByParam_Desg");
                }
            }
            if (stringDS.DataStrings[6].StringValue != "All")
            {
                cmd.Parameters["ADID"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["ADID1"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["ADID2"].Value = integerDS.DataIntegers[1].IntegerValue;
                cmd.Parameters["EmployeeCode1"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["EmployeeCode2"].Value = stringDS.DataStrings[5].StringValue;
                cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["SiteID1"].Value = integerDS.DataIntegers[2].IntegerValue;
                cmd.Parameters["PayrollSiteID"].Value = PayrollSiteID;
                cmd.Parameters["PayrollSiteID1"].Value = PayrollSiteID;
                cmd.Parameters["Gender1"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["Gender2"].Value = stringDS.DataStrings[1].StringValue;
                cmd.Parameters["Religion1"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["Religion2"].Value = stringDS.DataStrings[2].StringValue;
                cmd.Parameters["DepartmentID"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["DepartmentID1"].Value = integerDS.DataIntegers[3].IntegerValue;
                cmd.Parameters["EmployeeType"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["EmployeeType1"].Value = integerDS.DataIntegers[4].IntegerValue;
                cmd.Parameters["DesignationCode1"].Value = cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[3].StringValue;
                cmd.Parameters["LiabilityCode1"].Value = cmd.Parameters["LiabilityCode2"].Value = stringDS.DataStrings[7].StringValue;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeAllowanceDS empAllowDS = new EmployeeAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empAllowDS, empAllowDS.EmpAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMP_ALLOWANCE_BY_PARAM.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            empAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(empAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }


        private DataSet _getSuspectedTaxableIncome(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetSuspectedTaxableIncome");

            cmd.Parameters["ADID"].Value = integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["TAXPARAMID"].Value = integerDS.DataIntegers[1].IntegerValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeeAllowanceDS empAllowDS = new EmployeeAllowanceDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empAllowDS, empAllowDS.EmpAllowance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SUSPECTED_TAXABLE_INCOME.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            empAllowDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(empAllowDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _createSuspectedTaxableIncome(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            EmployeeAllowanceDS empAllowDS = new EmployeeAllowanceDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd_Del = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            empAllowDS.Merge(inputDS.Tables[empAllowDS.SuspectedTaxableIncome.TableName], false, MissingSchemaAction.Error);
            empAllowDS.AcceptChanges();

            bool bError;
            int nRowAffected;

            #region Previous Data Deletion...
            foreach (EmployeeAllowanceDS.SuspectedTaxableIncomeRow Row in empAllowDS.SuspectedTaxableIncome)
            {
                cmd_Del.CommandText = "PRO_TAXABLE_INCOME_DELETE";
                cmd_Del.CommandType = CommandType.StoredProcedure;

                cmd_Del.Parameters.AddWithValue("p_ADID", Row.ADID);
                cmd_Del.Parameters.AddWithValue("p_EMPLOYEEID", Row.EMPLOYEEID);
                cmd_Del.Parameters.AddWithValue("p_TAXPARAMID", Row.TAXPARAMID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Del, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd_Del.Parameters.Clear();
                break;
            }
            #endregion

            #region Data Insert...
            cmd.CommandText = "PRO_TAXABLE_INCOME_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeAllowanceDS.SuspectedTaxableIncomeRow empAllow in empAllowDS.SuspectedTaxableIncome)
            {
                //if (empAllow.IsEMPLOYEEIDNull()) continue;

                cmd.Parameters.AddWithValue("p_ADID", empAllow.ADID);
                cmd.Parameters.AddWithValue("p_EMPLOYEEID", empAllow.EMPLOYEEID);
                cmd.Parameters.AddWithValue("p_TAXPARAMID", empAllow.TAXPARAMID);
                cmd.Parameters.AddWithValue("p_AMOUNT", empAllow.AMOUNT);
                cmd.Parameters.AddWithValue("p_CREATE_USER", empAllow.CREATE_USER);
                cmd.Parameters.AddWithValue("p_CREATE_DATE", empAllow.CREATE_DATE);
                cmd.Parameters.AddWithValue("p_SALRAYID", DBNull.Value);
                cmd.Parameters.AddWithValue("p_SELECTED", empAllow.Selected);
                
                //if (empAllow.IsLogedinUserIDNull() == false) cmd.Parameters.AddWithValue("p_CREATE_USER", empAllow.LogedinUserID);
                //else cmd.Parameters.AddWithValue("p_Delete_User", DBNull.Value);


                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_EMPLOYEE_ALLOWANCE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

    }
}
