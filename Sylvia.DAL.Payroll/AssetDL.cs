﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    class AssetDL
    {
        int p_Asset_TypeID;
        string p_UserID;
        DateTime AssetAppDate;

        public AssetDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                #region Asset Type
                case ActionID.ACTION_ASSET_TYPE_CREATE:
                    return this._createAssetType(inputDS);
                case ActionID.ACTION_ASSET_TYPE_UPD:
                    return this._updateAssetType(inputDS);
                case ActionID.ACTION_ASSET_TYPE_DEL:
                    return this._deleteAssetType(inputDS);
                case ActionID.NA_ACTION_GET_ASSET_TYPE_LIST:
                    return this._getAssetTypeList(inputDS);
                case ActionID.NA_ACTION_GET_ASSET_TYPE_LIST_ALL:
                    return this._getAssetTypeListAll(inputDS);
                case ActionID.ACTION_DOES_ASSET_TYPE_EXIST:
                    return this._doesAssetTypeExist(inputDS);
                #endregion


                #region Requisition Category
                case ActionID.ACTION_REQUISITION_CATEGORY_CREATE:

                #endregion

                #region Requisition Input Field
                case ActionID.ACTION_ASSET_INPUT_FIELD_CREATE:
                    return this._createAssetInputField(inputDS);
                case ActionID.ACTION_ASSET_INPUT_FIELD_UPD:
                    return this._updateAssetInputField(inputDS);
                case ActionID.NA_ACTION_GET_ASSET_INPUT_FIELD_LIST:
                    return this._getAssetInputFieldList(inputDS);
                case ActionID.NA_ACTION_GET_ASSET_INPUT_FIELD_LIST_ALL:
                    return this._getAssetInputFieldListAll(inputDS);
                case ActionID.NA_ACTION_GET_ASS_INPUTFIELD_LIST_BY_ATYPEID:
                    return this._getAssetInputFieldListByReqTypeID(inputDS);
                case ActionID.ACTION_DOES_ASSET_INPUT_FIELD_EXIST:
                    return this._doesAssetInputFieldExist(inputDS);
                // Update by Asif, 14-mar-12
                case ActionID.ACTION_ASSET_INPUTFIELD_DEL:
                    return this._deleteAssetInputfield(inputDS);
                case ActionID.NA_ACTION_GET_REQ_INPUTFIELD_LIST_BY_RTYPEID_AND_CURRENT_USER:
                    return this._getRequisitionInputFieldListByReqTypeIDAndCurrentUser(inputDS);
                case ActionID.NA_ACTION_GET_ASSET_TYPE_USING_LOGINID:
                    return this._GetAssetTypeUsingLoginID(inputDS);



                // Update end
                #endregion

                #region RIF Config
                case ActionID.ACTION_AIFCONFIG_CREATE:
                    return this._createAIFConfig(inputDS);
                case ActionID.ACTION_AIFCONFIG_DELETE:
                    return this._deleteRIFConfig(inputDS);
                #endregion

                #region Requisition...
                case ActionID.ACTION_ASSET_APPLY:
                    return this._createAssetApply(inputDS);
                case ActionID.NA_ACTION_REQ_GET_REQUISITION_SUMMERY:
                    return this._GetRequisitionsSummary(inputDS);
                case ActionID.NA_ACTION_REQ_GETTING_EMPLOYEE_HISTORY:
                    return this._GetRequisitionHistData(inputDS);

                #endregion
                case ActionID.NA_ACTION_GET_REQUISITION_ACTIVITY_LIST_ALL:
                    return this._GetReqActivityList(inputDS);
                case ActionID.NA_ACTION_ASSET_APPLIED_LIST:
                    return this._GetAssetAppliedData(inputDS);
                case ActionID.NA_ACTION_GET_ASSET_ACTIVITY_LIST_ALL:
                    return this._GetAssetActivitiyAll(inputDS);
                case ActionID.NA_ACTION_GET_REQUISITION_LIST_BY_ACTIVITYID:
                    return this._GetRequisitionByActivityID(inputDS);
                case ActionID.NA_ACTION_GET_REQ_LIST_BY_ACTIVITYID_CODE:
                    return this._GetReqByActivityIDCode(inputDS);
                case ActionID.NA_ACTION_GET_EMP_CODE_BY_LOGINID:
                    return this._GetEmpCodeByLoginID(inputDS);
                case ActionID.ACTION_ASSET_APPLY_APPROVATION:
                    return this._AssetApplyApprovation(inputDS);
                case ActionID.NA_ACTION_GET_ASS_FIELD_LIST_BY_AEQUESTID:
                    return this._GetAssetFieldListByAssRequestID(inputDS);
                case ActionID.NA_ACTION_GET_FIELD_LIST_BY_ATYPEID_LOGINID:
                    return this._GetAssetInputListByAssTypeIDLoginID(inputDS);
                case ActionID.NA_ACTION_GET_REQ_FIELD_LIST_LIMITMARKABLE:
                    return this._GetReqFieldListLimitMarkable(inputDS);
                case ActionID.NA_ACTION_GET_DEPOSIT_DETAILS:
                    return this._GetDepositDetails(inputDS);
                case ActionID.NA_ACTION_GET_OUTSTANDING_LOAN:
                    return this._GetOutstandingLoan(inputDS);

                case ActionID.ACTION_ASSET_ASSIGN:
                    return this._assignCompanyAsset(inputDS);


                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        #region Requisition Type
        private DataSet _createAssetType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AssetDS assetDS = new AssetDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ASSET_TYPE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            assetDS.Merge(inputDS.Tables[assetDS.AssetType.TableName], false, MissingSchemaAction.Error);
            assetDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AssetDS.AssetTypeRow row in assetDS.AssetType.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_ASSETTYPEID", genPK);

                if (row.IsAssetTypecodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPECODE", row.AssetTypecode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPECODE", DBNull.Value);
                }

                if (row.IsAssetTypenameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPENAME", row.AssetTypename);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_REQUISITIONTYPENAME", DBNull.Value);
                }

                if (row.IsAssetTypeRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPEREMARKS", row.AssetTypeRemarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPEREMARKS", DBNull.Value);
                }

                if (row.IsIsActiveNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ISACTIVE", row.IsActive);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ISACTIVE", DBNull.Value);
                }

                // WALI :: 30-Mar-2015
                if (!row.IsDepriciationRateNull()) cmd.Parameters.AddWithValue("p_DepriciationRate", row.DepriciationRate);
                else cmd.Parameters.AddWithValue("p_DepriciationRate", 0);
                // WALI :: Update :: End

                //if (row.IsCurrentUserIDNull() == false)
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", DBNull.Value);
                //}

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSET_TYPE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateAssetType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AssetDS assetDS = new AssetDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ASSET_TYPE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            assetDS.Merge(inputDS.Tables[assetDS.AssetType.TableName], false, MissingSchemaAction.Error);
            assetDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AssetDS.AssetTypeRow row in assetDS.AssetType.Rows)
            {
                if (row.IsAssetTypenameNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPENAME", row.AssetTypename);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPENAME", DBNull.Value);
                }

                if (row.IsAssetTypeRemarksNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPEREMARKS", row.AssetTypeRemarks);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPEREMARKS", DBNull.Value);
                }

                if (row.IsIsActiveNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ISACTIVE", row.IsActive);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ISACTIVE", DBNull.Value);
                }

                if (row.IsAssetTypecodeNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPECODE", row.AssetTypecode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ASSETTYPECODE", DBNull.Value);
                }

                // WALI :: 30-Mar-2015
                if (!row.IsDepriciationRateNull()) cmd.Parameters.AddWithValue("p_DepriciationRate", row.DepriciationRate);
                else cmd.Parameters.AddWithValue("p_DepriciationRate", 0);
                // WALI :: Update :: End

                //if (row.IsCurrentUserIDNull() == false)
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", DBNull.Value);
                //}

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSET_TYPE_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteAssetType(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ASSET_TYPE__DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_ASSETTYPECODE", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ASSET_TYPE_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        # region Created by Asif, 14-mar-12
        private DataSet _deleteAssetInputfield(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ASS_INPUTFIELD_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_FIELDNAME", (object)stringDS.DataStrings[0].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ASSET_INPUTFIELD_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        # endregion

        private DataSet _getAssetTypeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAssetTypeList");
            cmd.Parameters["Code"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AssetDS assetDS = new AssetDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ASSET_TYPE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            assetDS.AcceptChanges();
            #region Comments
            //RequisitionDS requisitionDS = new RequisitionDS();
            //if (requisitionPO.RequisitionType.Count > 0)
            //{
            //    foreach (RequisitionPO.RequisitionTypeRow poRow in requisitionPO.RequisitionType.Rows)
            //    {
            //        RequisitionDS.RequisitionTypeRow row = requisitionDS.RequisitionType.NewRequisitionTypeRow();
            //        if (poRow.IsREQUISITIONTYPEIDNull() == false)
            //        {
            //            row.REQUISITIONTYPEID = poRow.REQUISITIONTYPEID;
            //        }
            //        if (poRow.IsREQUISITIONTYPECODENull() == false)
            //        {
            //            row.REQUISITIONTYPECODE = poRow.REQUISITIONTYPECODE;
            //        }
            //        if (poRow.IsREQUISITIONTYPENAMENull() == false)
            //        {
            //            row.REQUISITIONTYPENAME = poRow.REQUISITIONTYPENAME;
            //        }
            //        if (poRow.IsREQUISITIONTYPEREMARKSNull() == false)
            //        {
            //            row.REQUISITIONTYPEREMARKS = poRow.REQUISITIONTYPEREMARKS;
            //        }
            //        if (poRow.IsISACTIVENull() == false)
            //        {
            //            row.ISACTIVE = poRow.ISACTIVE;
            //        }

            //        requisitionDS.RequisitionType.AddRequisitionTypeRow(row);
            //        requisitionDS.AcceptChanges();
            //    }
            //}
            #endregion
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAssetTypeListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;
            if (activeStatus.Equals("All"))
            {
                cmd = DBCommandProvider.GetDBCommand("GetAssetTypeListAll");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetActiveAssetTypeList");
                if (activeStatus.Equals("Active")) cmd.Parameters["IsActive"].Value = (object)true;
                else cmd.Parameters["IsActive"].Value = (object)false;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AssetDS assetDS = new AssetDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ASSET_TYPE_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            assetDS.AcceptChanges();

            #region Comments
            //RequisitionDS requisitionDS = new RequisitionDS();
            //if (requisitionPO.RequisitionType.Count > 0)
            //{
            //    foreach (RequisitionPO.RequisitionTypeRow poRow in requisitionPO.RequisitionType.Rows)
            //    {
            //        RequisitionDS.RequisitionTypeRow row = requisitionDS.RequisitionType.NewRequisitionTypeRow();
            //        if (poRow.IsREQUISITIONTYPEIDNull() == false)
            //        {
            //            row.REQUISITIONTYPEID = poRow.REQUISITIONTYPEID;
            //        }
            //        if (poRow.IsREQUISITIONTYPECODENull() == false)
            //        {
            //            row.REQUISITIONTYPECODE = poRow.REQUISITIONTYPECODE;
            //        }
            //        if (poRow.IsREQUISITIONTYPENAMENull() == false)
            //        {
            //            row.REQUISITIONTYPENAME = poRow.REQUISITIONTYPENAME;
            //        }
            //        if (poRow.IsREQUISITIONTYPEREMARKSNull() == false)
            //        {
            //            row.REQUISITIONTYPEREMARKS = poRow.REQUISITIONTYPEREMARKS;
            //        }
            //        if (poRow.IsISACTIVENull() == false)
            //        {
            //            row.ISACTIVE = poRow.ISACTIVE;
            //        }

            //        requisitionDS.RequisitionType.AddRequisitionTypeRow(row);
            //        requisitionDS.AcceptChanges();
            //    }
            //}
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _doesAssetTypeExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesAssetTypeExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ASSET_TYPE_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region Requisition Input Field
        private DataSet _createAssetInputField(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AssetDS assetDS = new AssetDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ASS_INPUTFIELD_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            assetDS.Merge(inputDS.Tables[assetDS.AssetInputField.TableName], false, MissingSchemaAction.Error);
            assetDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AssetDS.AssetInputFieldRow row in assetDS.AssetInputField.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_AIFID", genPK);

                if (row.IsFIELDNAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_FIELDNAME", row.FIELDNAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_FIELDNAME", DBNull.Value);
                }

                if (row.IsDATATYPENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DATATYPE", row.DATATYPE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DATATYPE", DBNull.Value);
                }

                if (row.IsDATATYPENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_AIFREMARKS", row.AIFREMARKS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_AIFREMARKS", DBNull.Value);
                }

                if (row.IsAIFCODENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_AIFCODE", row.AIFCODE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_AIFCODE", DBNull.Value);
                }

                //if (row.IsCurrentUserIDNull() == false)
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", row.CurrentUserID);
                //}
                //else
                //{
                //    cmd.Parameters.AddWithValue("p_CurrentUserId", DBNull.Value);
                //}

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQUISITION_INPUT_FIELD_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _updateAssetInputField(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AssetDS assetDS = new AssetDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ASS_INPUTFIELD_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            assetDS.Merge(inputDS.Tables[assetDS.AssetInputField.TableName], false, MissingSchemaAction.Error);
            assetDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AssetDS.AssetInputFieldRow row in assetDS.AssetInputField.Rows)
            {
                if (row.IsDATATYPENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_DATATYPE", row.DATATYPE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DATATYPE", DBNull.Value);
                }

                if (row.IsAIFREMARKSNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_AIFREMARKS", row.AIFREMARKS);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_AIFREMARKS", DBNull.Value);
                }

                if (row.IsFIELDNAMENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_FIELDNAME", row.FIELDNAME);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_FIELDNAME", DBNull.Value);
                }

                if (row.IsAIFCODENull() == false)
                {
                    cmd.Parameters.AddWithValue("p_AIFCODE", row.AIFCODE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_AIFCODE", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSET_INPUT_FIELD_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getAssetInputFieldList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAssetInputFieldList");
            cmd.Parameters["REQUISITIONINPUTFIELDID"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AssetDS assetDS = new AssetDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ASSET_INPUT_FIELD_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            assetDS.AcceptChanges();

            //RequisitionDS requisitionDS = new RequisitionDS();
            //if (assetDS.RequisitionInputField.Count > 0)
            //{
            //    foreach (RequisitionPO.RequisitionInputFieldRow poRow in assetDS.RequisitionInputField.Rows)
            //    {
            //        RequisitionDS.RequisitionInputFieldRow row = requisitionDS.RequisitionInputField.NewRequisitionInputFieldRow();

            //        if (poRow.IsFIELDNAMENull() == false)
            //        {
            //            row.FIELDNAME = poRow.FIELDNAME;
            //        }
            //        if (poRow.IsDATATYPENull() == false)
            //        {
            //            row.DATATYPE = poRow.DATATYPE;
            //        }
            //        if (poRow.IsRIFREMARKSNull() == false)
            //        {
            //            row.RIFREMARKS = poRow.RIFREMARKS;
            //        }



            //        requisitionDS.RequisitionInputField.AddRequisitionInputFieldRow(row);
            //        requisitionDS.AcceptChanges();
            //    }
            //}

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAssetInputFieldListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetAssetInputFieldListAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AssetDS assetDS = new AssetDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ASSET_INPUT_FIELD_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            assetDS.AcceptChanges();

            //RequisitionDS requisitionDS = new RequisitionDS();
            //if (assetDS.RequisitionInputField.Count > 0)
            //{
            //    foreach (RequisitionPO.RequisitionInputFieldRow poRow in assetDS.RequisitionInputField.Rows)
            //    {
            //        RequisitionDS.RequisitionInputFieldRow row = requisitionDS.RequisitionInputField.NewRequisitionInputFieldRow();

            //        row.RIFID = poRow.RIFID;

            //        if (poRow.IsFIELDNAMENull() == false)
            //        {
            //            row.FIELDNAME = poRow.FIELDNAME;
            //        }
            //        if (poRow.IsDATATYPENull() == false)
            //        {
            //            row.DATATYPE = poRow.DATATYPE;
            //        }
            //        if (poRow.IsRIFREMARKSNull() == false)
            //        {
            //            row.RIFREMARKS = poRow.RIFREMARKS;
            //        }
            //        if (poRow.IsRIFCODENull() == false)
            //        {
            //            row.RIFCODE = poRow.RIFCODE;
            //        }

            //        requisitionDS.RequisitionInputField.AddRequisitionInputFieldRow(row);
            //        requisitionDS.AcceptChanges();
            //    }
            //}

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAssetInputFieldListByReqTypeID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAssetInputFieldListByRTypeID");
            cmd.Parameters["REQUISITIONTYPEID"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AssetDS assetDS = new AssetDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ASS_INPUTFIELD_LIST_BY_ATYPEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            assetDS.AcceptChanges();

            //RequisitionDS requisitionDS = new RequisitionDS();
            //if (assetDS.RequisitionInputField.Count > 0)
            //{
            //    foreach (RequisitionPO.RequisitionInputFieldRow poRow in assetDS.RequisitionInputField.Rows)
            //    {
            //        RequisitionDS.RequisitionInputFieldRow row = requisitionDS.RequisitionInputField.NewRequisitionInputFieldRow();

            //        if (poRow.IsRIFIDNull() == false)
            //        {
            //            row.RIFID = poRow.RIFID;
            //        }
            //        if (poRow.IsFIELDNAMENull() == false)
            //        {
            //            row.FIELDNAME = poRow.FIELDNAME;
            //        }
            //        if (poRow.IsDATATYPENull() == false)
            //        {
            //            row.DATATYPE = poRow.DATATYPE;
            //        }
            //        if (poRow.IsISMANDATORYNull() == false)
            //        {
            //            row.ISMANDATORY = poRow.ISMANDATORY;
            //        }
            //        if (poRow.IsRIFCONFIGIDNull() == false)
            //        {
            //            row.RIFCONFIGID = poRow.RIFCONFIGID;
            //        }
            //        if (poRow.IsLIMIT_MARKABLENull() == false)
            //        {
            //            row.LIMIT_MARKABLE = poRow.LIMIT_MARKABLE;
            //        }


            //        requisitionDS.RequisitionInputField.AddRequisitionInputFieldRow(row);
            //        requisitionDS.AcceptChanges();
            //    }
            //}

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        # region Created by Asif, 14-mar-12
        private DataSet _getRequisitionInputFieldListByReqTypeIDAndCurrentUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRequisitionInputFieldListByRTypeIDAndCurrentUser");
            cmd.Parameters["REQUISITIONTYPEID"].Value = (object)StringDS.DataStrings[1].StringValue;
            cmd.Parameters["CURRENTUSER"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            RequisitionPO requisitionPO = new RequisitionPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionPO, requisitionPO.RequisitionInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_REQ_INPUTFIELD_LIST_BY_RTYPEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            requisitionPO.AcceptChanges();

            RequisitionDS requisitionDS = new RequisitionDS();
            if (requisitionPO.RequisitionInputField.Count > 0)
            {
                foreach (RequisitionPO.RequisitionInputFieldRow poRow in requisitionPO.RequisitionInputField.Rows)
                {
                    RequisitionDS.RequisitionInputFieldRow row = requisitionDS.RequisitionInputField.NewRequisitionInputFieldRow();

                    if (poRow.IsRIFIDNull() == false)
                    {
                        row.RIFID = poRow.RIFID;
                    }
                    if (poRow.IsFIELDNAMENull() == false)
                    {
                        row.FIELDNAME = poRow.FIELDNAME;
                    }
                    if (poRow.IsDATATYPENull() == false)
                    {
                        row.DATATYPE = poRow.DATATYPE;
                    }
                    if (poRow.IsISMANDATORYNull() == false)
                    {
                        row.ISMANDATORY = poRow.ISMANDATORY;
                    }
                    if (poRow.IsORIGINALVALUENull() == false)
                    {
                        row.ORIGINALVALUE = poRow.ORIGINALVALUE;
                    }
                    if (poRow.IsRIFCONFIGIDNull() == false)
                    {
                        row.RIFCONFIGID = poRow.RIFCONFIGID;
                    }

                    requisitionDS.RequisitionInputField.AddRequisitionInputFieldRow(row);
                    requisitionDS.AcceptChanges();
                }
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }
        # endregion

        private DataSet _doesAssetInputFieldExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesAssetInputFieldExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["FieldName"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_REQUISITION_INPUT_FIELD_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        #endregion

        #region RIF Config
        private DataSet _createAIFConfig(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            AssetDS assetDS = new AssetDS();
            assetDS.Merge(inputDS.Tables[assetDS.AIFConfig.TableName], false, MissingSchemaAction.Error);
            assetDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            bool bError = false;
            int nRowAffected = -1;

            #region Delete RIF Config

            cmd.CommandText = "PRO_AIFCONFIG_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (AssetDS.AIFConfigRow row in assetDS.AIFConfig.Rows)
            {
                cmd.Parameters.AddWithValue("p_ASSETTYPEID", row.Assettypeid);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_AIFCONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                break;
            }
            cmd.Dispose();
            #endregion

            #region Create RIF Config

            cmd.CommandText = "PRO_AIFCONFIG_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (AssetDS.AIFConfigRow row in assetDS.AIFConfig.Rows)
            {
                // Update by Asif, 15-mar-12
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }


                //cmd.Parameters.AddWithValue("p_RIFCONFIGID", genPK);
                // Update end
                cmd.Parameters.AddWithValue("p_ASSETTYPEID", row.Assettypeid);
                cmd.Parameters.AddWithValue("p_AIFID", row.Aifid);
                cmd.Parameters.AddWithValue("p_ISMANDATORY", row.Ismandatory);
                cmd.Parameters.AddWithValue("p_AIFCONFIGID", genPK);
                cmd.Parameters.AddWithValue("p_LIMITMARKABLE", row.Limit_Markable);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_AIFCONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteRIFConfig(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_AIFCONFIG_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_ASSETTYPEID", (object)integerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_AIFCONFIG_DELETE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        #endregion

        #region Asset Apply
        private DataSet _createAssetApply(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AssetDS assetDS = new AssetDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ASSET_APPLY";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            //-================
            OleDbCommand cmd2 = new OleDbCommand();
            cmd2.CommandText = "PRO_ASS_REQUEST_HIST_CREATE";
            cmd2.CommandType = CommandType.StoredProcedure;

            if (cmd2 == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            //================
            assetDS.Merge(inputDS.Tables[assetDS.AssetApply.TableName], false, MissingSchemaAction.Error);
            assetDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            DataStringDS stringDS = new DataStringDS();

            long genPK = IDGenerator.GetNextGenericPK();
            if (genPK == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }

            long genPK2 = IDGenerator.GetNextGenericPK();
            if (genPK2 == -1)
            {
                return UtilDL.GetDBOperationFailed();
            }


            foreach (AssetDS.AssetApplyRow row in assetDS.AssetApply.Rows)
            {
                cmd.Parameters.AddWithValue("P_ASS_REQUIESTID", genPK);

                if (row.IsAssetTypeidNull() == false)
                {
                    cmd.Parameters.AddWithValue("P_ASS_TYPEID", row.AssetTypeid);
                    p_Asset_TypeID = row.AssetTypeid;
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_ASS_TYPEID", DBNull.Value);
                }

                if (row.IsCurrentUserIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("P_USERID", row.CurrentUserID);
                    p_UserID = row.CurrentUserID;
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_USERID", DBNull.Value);
                }


                if (row.IsORIGINALVALUENull() == false)
                {
                    cmd.Parameters.AddWithValue("P_ORIGINALVALUE", row.ORIGINALVALUE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_ORIGINALVALUE", DBNull.Value);
                }

                if (row.IsCHANGINGVALUENull() == false)
                {
                    cmd.Parameters.AddWithValue("P_CHANGEVALUE", row.CHANGINGVALUE);
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_CHANGEVALUE", DBNull.Value);
                }


                if (row.IsAifidNull() == false)
                {
                    cmd.Parameters.AddWithValue("P_AIFID", row.Aifid);
                }
                else
                {
                    cmd.Parameters.AddWithValue("P_AIFID", DBNull.Value);
                }

                if (row.IsAppliedDateNull() == false)
                {
                    AssetAppDate = row.AppliedDate;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSET_APPLY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }


            cmd2.Parameters.AddWithValue("P_ASS_REQUIESTID", genPK);
            cmd2.Parameters.AddWithValue("P_ASS_TYPEID", p_Asset_TypeID);
            cmd2.Parameters.AddWithValue("P_USERID", p_UserID);
            cmd2.Parameters.AddWithValue("P_ASS_HISTORYID", genPK2);
            cmd2.Parameters.AddWithValue("p_ACTIVITIESID", Convert.ToInt32(AssetActivities.PendingApproval));
            cmd2.Parameters.AddWithValue("p_AssetApplyDate", AssetAppDate);
            int nRowAffected2 = -1;
            bool bError2 = false;

            nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd2, connDS.DBConnections[0].ConnectionID, ref bError2);
            cmd2.Parameters.Clear();

            errDS.Clear();
            errDS.AcceptChanges();
            //return errDS;

            // Update by Asif, 14-mar-12
            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
            // Update end
        }
        #endregion

        #region Requisition Updated by Shakir 30/08/2012
        private DataSet _GetAssetTypeUsingLoginID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            if (StringDS.DataStrings[0].StringValue != "admin")
            {
                cmd = DBCommandProvider.GetDBCommand("GetAssetTypesByLoginID");
                cmd.Parameters["LOGINID"].Value = (object)StringDS.DataStrings[0].StringValue;
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetAssetTypesByAdmin");
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            AssetDS assetDS = new AssetDS();
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            assetDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        #endregion

        private DataSet _GetRequisitionsSummary(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            cmd = DBCommandProvider.GetDBCommand("GetRequisitionSummary");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;



            //lmsLeaveDS leaveDS = new lmsLeaveDS();
            RequisitionDS RequisitionDS = new RequisitionDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, RequisitionDS, RequisitionDS.Requisition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_LEAVE_SUMMERY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            RequisitionDS.AcceptChanges();



            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(RequisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetRequisitionHistData(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetReqEmployeeHistory");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            //lmsEmployeeHistoryDS lmsEmpHistDS = new lmsEmployeeHistoryDS();
            RequisitionDS RequisitionDS = new RequisitionDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, RequisitionDS, RequisitionDS.Employee.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_LMS_GET_EMPLOYEE_HISTORY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            //lmsEmpHistPO.AcceptChanges();
            RequisitionDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(RequisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetReqActivityList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetActivityListAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetAssetAppliedData(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            if (activeStatus == "All") cmd = DBCommandProvider.GetDBCommand("GetAssetAppliedSummary");
            else
            {
                Int32 assetActivityID = Convert.ToInt32(StringDS.DataStrings[0].StringValue);
                cmd = DBCommandProvider.GetDBCommand("GetAssetAppliedSummary_BYID");
                cmd.Parameters["AssetActivityID"].Value = activeStatus;
            }


            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            AssetDS assetDS = new AssetDS();
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetApply.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            assetDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;




        }

        private DataSet _GetAssetActivitiyAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;
            string LoginID = StringDS.DataStrings[1].StringValue;
            string AssAuthorizationKey = StringDS.DataStrings[2].StringValue;


            if (LoginID != "admin" && AssAuthorizationKey == "False")
            {
                cmd = DBCommandProvider.GetDBCommand("GetAssetActivitiesForGeneral");
            }
            else if (LoginID != "admin" && AssAuthorizationKey == "True")
            {
                cmd = DBCommandProvider.GetDBCommand("GetAssetActForSupperVisor");
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetAssetActivitiesAll");

            }
            LoginID = "";
            AssAuthorizationKey = "";

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            AssetDS AssetDS = new AssetDS();
            nRowAffected = ADOController.Instance.Fill(adapter, AssetDS, AssetDS.Activities.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            AssetDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(AssetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetRequisitionByActivityID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRequisitionByActivityIDDate");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["REQACTIVITYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            cmd.Parameters["DateFrom"].Value = (object)StringDS.DataStrings[1].StringValue;
            cmd.Parameters["DateTo"].Value = (object)StringDS.DataStrings[2].StringValue;


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Requisition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _GetReqByActivityIDCode(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetReqByActivityIDDateCode");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["REQACTIVITYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            cmd.Parameters["DateFrom"].Value = (object)StringDS.DataStrings[1].StringValue;
            cmd.Parameters["DateTo"].Value = (object)StringDS.DataStrings[2].StringValue;
            cmd.Parameters["Code"].Value = (object)StringDS.DataStrings[3].StringValue;


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Requisition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _GetEmpCodeByLoginID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmpCodebyLoginID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["LoginID"].Value = (object)StringDS.DataStrings[0].StringValue;


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.Requisition.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _AssetApplyApprovation(DataSet inputDS)
        {

            string messageBody = "", URL = "";

            #region Get Email Information from UtilDL
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            //string credentialEmailAddress = "shakir@mislbd.com";
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            //string credentialEmailPwd = "*****";
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion


            //--------------------------------------------------------
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            //cmd.CommandText = "PRO_REQUISITION_APPROVATION";
            //cmd.CommandType = CommandType.StoredProcedure;

            //if (cmd == null)
            //{
            //    return UtilDL.GetCommandNotFound();
            //}

            AssetDS assetDS = new AssetDS();
            assetDS.Merge(inputDS.Tables[assetDS.AssetApplyHistory.TableName], false, MissingSchemaAction.Error);
            assetDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();



            foreach (AssetDS.AssetApplyHistoryRow row in assetDS.AssetApplyHistory.Rows)
            {
                //cmd.CommandText = "PRO_REQUISITION_APPROVATION";
                //cmd.CommandType = CommandType.StoredProcedure;

                if (row.ActivitiesID == 101 || row.ActivitiesID == 104 || row.ActivitiesID == 105)
                {
                    cmd.CommandText = "PRO_ASSET_APPROVE_FINISHING";
                    cmd.CommandType = CommandType.StoredProcedure;
                }
                else
                {
                    cmd.CommandText = "PRO_ASSET_APPLY_APPROVE";
                    cmd.CommandType = CommandType.StoredProcedure;
                }


                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_AssHistoryID", genPK);

                if (row.IsAssetrequestidNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_AssRequestID", row.Assetrequestid);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_AssRequestID", DBNull.Value);
                }


                if (row.IsSenderUserIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_SendUserID", row.SenderUserID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_SendUserID", DBNull.Value);
                }

                if (row.IsLoginIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_LoginID", row.LoginID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_LoginID", DBNull.Value);
                }

                if (row.IsActivitiesIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_ActivityID", row.ActivitiesID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_ActivityID", DBNull.Value);
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSET_APPLY_APPROVATION.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();

                #region Email Body Creation...........
                URL = row.Url;
                URL += "?pVal=CompanyAssets";
                messageBody += "<b>Applicant: </b>" + row.APPHOLDER + " [" + row.EmployeeCode + "]<br>";
                messageBody += "<b>Applied Asset Name : </b>" + row.Assettypename + "<br>";
                messageBody += "<br><br><br>";
                messageBody += "Click the following link for details: ";
                messageBody += "<br>";
                messageBody += "<a href='" + URL + "'>" + URL + "</a>";

                #endregion

                #region Email Send............
                Mail.Mail mail = new Mail.Mail();
                if (row.IsMailNotificationNull() == false)
                {
                    string FromEmailAddress = credentialEmailAddress;
                    if (IsEMailSendWithCredential)
                    {
                        string mailDisplayName = System.Configuration.ConfigurationManager.AppSettings["MailDisplayName"].Trim();
                        //mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, emailRow.FromEmailAddress, emailRow.EmailAddress, emailRow.MailingSubject, messageBody);
                        mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, credentialEmailAddress, row.Email, "", "Asset Applied " + row.ACTIVITYFRIENDLYNAME, messageBody, mailDisplayName);

                    }
                }
                #endregion
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetAssetFieldListByAssRequestID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAssetFieldListByRequestID");
            cmd.Parameters["ASSETREQUESTID"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            AssetDS assetDS = new AssetDS();
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            assetDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetAssetInputListByAssTypeIDLoginID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAssetFieldListByATypeIDLoginID");
            cmd.Parameters["ASSETTYPEID"].Value = (object)StringDS.DataStrings[0].StringValue;
            cmd.Parameters["UserLoginID"].Value = (object)StringDS.DataStrings[1].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            AssetDS assetDS = new AssetDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ASS_INPUTFIELD_LIST_BY_ATYPEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            assetDS.AcceptChanges();

            //RequisitionDS requisitionDS = new RequisitionDS();
            //if (requisitionPO.RequisitionInputField.Count > 0)
            //{
            //    foreach (RequisitionPO.RequisitionInputFieldRow poRow in requisitionPO.RequisitionInputField.Rows)
            //    {
            //        RequisitionDS.RequisitionInputFieldRow row = requisitionDS.RequisitionInputField.NewRequisitionInputFieldRow();

            //        if (poRow.IsRIFIDNull() == false)
            //        {
            //            row.RIFID = poRow.RIFID;
            //        }
            //        if (poRow.IsFIELDNAMENull() == false)
            //        {
            //            row.FIELDNAME = poRow.FIELDNAME;
            //        }
            //        if (poRow.IsDATATYPENull() == false)
            //        {
            //            row.DATATYPE = poRow.DATATYPE;
            //        }
            //        if (poRow.IsISMANDATORYNull() == false)
            //        {
            //            row.ISMANDATORY = poRow.ISMANDATORY;
            //        }

            //        if (poRow.IsRIFCONFIGIDNull() == false)
            //        {
            //            row.RIFCONFIGID = poRow.RIFCONFIGID;
            //        }

            //        if (poRow.IsMAXVALUE1Null() == false)
            //        {
            //            row.MAXVALUE1 = poRow.MAXVALUE1;
            //        }

            //        requisitionDS.RequisitionInputField.AddRequisitionInputFieldRow(row);
            //        requisitionDS.AcceptChanges();
            //    }
            //}

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetReqFieldListLimitMarkable(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetFieldListLimtMarkable");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            RequisitionDS requisitionDS = new RequisitionDS();
            nRowAffected = ADOController.Instance.Fill(adapter, requisitionDS, requisitionDS.RequisitionInputField.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            requisitionDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(requisitionDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetDepositDetails(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDepositDetails");
            cmd.Parameters["EmployeeCode"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            FinalSettlementDS FSDS = new FinalSettlementDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, FSDS, FSDS.PayableToEmp.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPOSIT_DETAILS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            FSDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(FSDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetOutstandingLoan(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetOutstandingLoan");
            cmd.Parameters["EmployeeCode"].Value = (object)StringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            FinalSettlementDS FSDS = new FinalSettlementDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, FSDS, FSDS.RecoverableFromEmp.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_OUTSTANDING_LOAN.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            FSDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(FSDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _assignCompanyAsset(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            AssetDS assetDS = new AssetDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            OleDbCommand cmd = new OleDbCommand(), cmd2 = new OleDbCommand(); ;
            cmd.CommandText = "PRO_ASSET_ASSIGN";
            cmd2.CommandText = "PRO_ASSET_ASSIGN_HIST_CREATE";
            cmd.CommandType = cmd2.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmd2 == null) return UtilDL.GetCommandNotFound();

            assetDS.Merge(inputDS.Tables[assetDS.AssetApply.TableName], false, MissingSchemaAction.Error);
            assetDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            long genPK = IDGenerator.GetNextGenericPK();
            long genPK2 = IDGenerator.GetNextGenericPK();
            if (genPK == -1 || genPK2 == -1) return UtilDL.GetDBOperationFailed();

            #region Assign Asset
            long employeeID = UtilDL.GetEmployeeId(connDS, assetDS.AssetApply[0].EmployeeCode);
            foreach (AssetDS.AssetApplyRow row in assetDS.AssetApply.Rows)
            {
                cmd.Parameters.AddWithValue("P_ASS_REQUIESTID", genPK);

                if (!row.IsAssetTypeidNull())
                {
                    cmd.Parameters.AddWithValue("P_ASS_TYPEID", row.AssetTypeid);
                    p_Asset_TypeID = row.AssetTypeid;
                }
                else cmd.Parameters.AddWithValue("P_ASS_TYPEID", DBNull.Value);

                if (!row.IsCurrentUserIDNull())
                {
                    cmd.Parameters.AddWithValue("P_USERID", row.CurrentUserID);
                    p_UserID = row.CurrentUserID;
                }
                else cmd.Parameters.AddWithValue("P_USERID", DBNull.Value);

                if (!row.IsORIGINALVALUENull()) cmd.Parameters.AddWithValue("P_ORIGINALVALUE", row.ORIGINALVALUE);
                else cmd.Parameters.AddWithValue("P_ORIGINALVALUE", DBNull.Value);

                if (!row.IsCHANGINGVALUENull()) cmd.Parameters.AddWithValue("P_CHANGEVALUE", row.CHANGINGVALUE);
                else cmd.Parameters.AddWithValue("P_CHANGEVALUE", DBNull.Value);

                if (!row.IsAifidNull()) cmd.Parameters.AddWithValue("P_AIFID", row.Aifid);
                else cmd.Parameters.AddWithValue("P_AIFID", DBNull.Value);

                cmd.Parameters.AddWithValue("P_EmployeeID", employeeID);

                if (!row.IsAppliedDateNull()) AssetAppDate = row.AppliedDate;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSET_ASSIGN.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                stringDS.DataStrings.AddDataString(genPK.ToString());
            }
            #endregion

            #region Asset Request History
            cmd2.Parameters.AddWithValue("P_ASS_REQUIESTID", genPK);
            cmd2.Parameters.AddWithValue("P_ASS_TYPEID", p_Asset_TypeID);
            cmd2.Parameters.AddWithValue("P_USERID", p_UserID);
            cmd2.Parameters.AddWithValue("P_ASS_HISTORYID", genPK2);
            cmd2.Parameters.AddWithValue("p_ACTIVITIESID", Convert.ToInt32(AssetActivities.Assigned));
            cmd2.Parameters.AddWithValue("p_AssetApplyDate", AssetAppDate);
            cmd2.Parameters.AddWithValue("P_EmployeeID", employeeID);
            cmd2.Parameters.AddWithValue("P_Comments", assetDS.AssetApply[0].Requisitionhistorycomments);

            int nRowAffected2 = -1;
            bool bError2 = false;
            nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd2, connDS.DBConnections[0].ConnectionID, ref bError2);
            cmd2.Parameters.Clear();
            if (bError2)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_ASSET_ASSIGN.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(assetDS);
            returnDS.Merge(stringDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

    }
}