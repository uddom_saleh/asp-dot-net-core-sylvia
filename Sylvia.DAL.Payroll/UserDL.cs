/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: March 29, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;


namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class UserDL
    {
       
        public UserDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_SEC_USER_ADD:
                    return _createUser(inputDS);
                case ActionID.ACTION_SEC_USER_UPD:
                    return this._updateUser(inputDS);
                case ActionID.NA_ACTION_Q_ALL_USER:
                    return _getUserList(inputDS);
                case ActionID.NA_ACTION_SEC_USER_GET:
                    return _getUser(inputDS);
                case ActionID.NA_ACTION_SEC_PWD_CHANGE:
                    return _changeUserPassword(inputDS);
                case ActionID.ACTION_DOES_USER_EXIST:
                    return this._doesUserExist(inputDS);
                case ActionID.ACTION_DOES_EMPLOYEE_EXIST_IN_USER:
                    return this._doesEmployeeExist(inputDS);
                case ActionID.ACTION_SEC_FAILED_LOGIN_GET:
                    return this._getFailedLoginCount(inputDS);
                case ActionID.ACTION_SEC_CONCURRENCE_LOGIN_GET:
                    return this._getConcurrenceLoginCount(inputDS);
                case ActionID.ACTION_SEC_FAILED_LOGIN_SET:
                    return this._setFailedLoginCount(inputDS);
                case ActionID.ACTION_SEC_CONCURRENCE_LOGIN_SET:
                    return this._setConcurrenceLoginCount(inputDS);
                case ActionID.ACTION_SEC_MAXCONCURRENCE_LOGIN_GET:
                    return this._getMaxConcurrenceLogin(inputDS);
                case ActionID.ACTION_SEC_MAXFAILED_LOGIN_GET:
                    return this._getMaxFailedLogin(inputDS);
                case ActionID.ACTION_SEC_USER_LOCKED_GET:
                    return this._getLockedStatus(inputDS);
                case ActionID.ACTION_SEC_USER_LOCKED_SET:
                    return this._setLockedStatus(inputDS);
                case ActionID.NA_ACTION_Q_ALL_PREVILAGE:
                    return this._getPrevilageList(inputDS);
                case ActionID.NA_ACTION_Q_ALL_ROLE_BY_USER:
                    return this._getRoleListByUser(inputDS);
                case ActionID.NA_ACTION_Q_ALL_EMPLOYEE_BY_USER:
                    return this._getEmployeeList(inputDS);
                case ActionID.NA_ACTION_SEC_CONCURRENCE_LOGIN_SET_RM:
                    return this._setConcurrenceLoginCountWithRM(inputDS);
                case ActionID.ACTION_DELEGATE_CREATE:
                    return this._createDelegate(inputDS);
                case ActionID.NA_ACTION_GET_USER_BY_USERID:
                    return this._getUserByID(inputDS);

                case ActionID.NA_ACTION_USER_LIST_BY_COMPANY_CODE:
                    return this._getUserListByCompanyCode(inputDS);

                case ActionID.NA_ACTION_GET_ACTIVE_USER_LIST:
                    return this._getActiveUserList(inputDS);

                case ActionID.NA_ACTION_PLPH_USER_GET:
                    return this._getPrimaryLeavePlanHolderUser(inputDS);
                case ActionID.NA_ACTION_PLV_USER_GET:
                    return this._getLeavePlanVerifierUser(inputDS);

                case ActionID.NA_ACTION_GET_VERIFIER_LIST:
                    return this._getVerifierList(inputDS);

                case ActionID.NA_ACTION_GetAdministrativeRoleAllByLoginID:
                    return this.GetAdministrativeRoleAllByLoginID(inputDS);

                case ActionID.NA_ACTION_GET_USERS_BY_FILTERS:
                    return this.GetUserList_ByFilters(inputDS);

                case ActionID.NA_ACTION_ACC_GET_APPROVER_USERS:
                    return this.GetApproverUsers_Accounting(inputDS);

                case ActionID.ACTION_SEC_LOGIN_FLAG_SET:
                    return this.SetUser_LoginFlag(inputDS);
                case ActionID.ACTION_SEC_USER_LOG_SAVE:
                    return this.SaveUser_Log(inputDS);
                case ActionID.NA_ACTION_GET_SEC_USER_LOG:
                    return this.GetUserLog_ActiveUsers(inputDS);
                case ActionID.ACTION_SEC_USER_DISCONNECT_FORCEFULLY:
                    return this.DisconnectUser_Forcefully(inputDS);
                case ActionID.NA_ACTION_USER_LOGIN_FLAG_CHECK:
                    return this.Check_UserLoginState(inputDS);

                case ActionID.ACTION_SEC_USER_UPDATE_ON_LOGON:
                    return this.UpdateUser_OnLogon(inputDS);

                case ActionID.NA_ACTION_GET_USER_MENU_LIST:
                    return this.GetUserMenuList(inputDS);
                case ActionID.ACTION_DELEGATE_UPDATE:
                    return this._updateDelegate(inputDS);
                case ActionID.ACTION_SYSTEM_MODULE_HIBERNATE_ADD:
                    return this._createModuleHibernatation(inputDS);
                case ActionID.NA_ACTION_ADMINISTRATIVEROLE_BYMODULEID:
                    return this.GetAdministrativeRoleByModuleID(inputDS);
                case ActionID.NA_ACTION_GET_LOGIN_USER:
                    return _getUserForLogin(inputDS);
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }
        private DataSet _getUserForLogin(DataSet inputDS)
        {

            int _conId = ADOController.Instance.OpenNewConnection();

            //int _conId = _controller.OpenNewConnection();

            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();
            bool bError = false;
            int nRowAffected = -1;

            //DBConnectionDS connDS = new DBConnectionDS();
            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            string scode = stringDS.DataStrings[0].StringValue;

            #region Data Load for User
            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUser_For_Login");
            OleDbCommand cmd = DBCommandProvider.GetDBCommandAdvanced("GetUser_For_Login", "User.xml");
            //GetDBCommandAdvanced
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (!stringDS.DataStrings[0].IsStringValueNull())
                cmd.Parameters["LoginID"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bError = false;
            nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, _conId, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LOGIN_USER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            #region Data Load For Additional Administrative Roules...
            //OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("GetAdministrativeRoleByLoginID");
            OleDbCommand cmd1 = DBCommandProvider.GetDBCommandAdvanced("GetAdministrativeRoleByLoginID", "User.xml");

            cmd1.Parameters["LoginID"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter1 = new OleDbDataAdapter();
            adapter1.SelectCommand = cmd1;

            bError = false;
            nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter1, userDS, userDS.UAARole.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            nRowAffected = ADOController.Instance.Fill(adapter1, userDS, userDS.UAARole.TableName, _conId, ref bError);
            userDS.AcceptChanges();
            #endregion

            stringDS.Clear();
            stringDS.AcceptChanges();
            foreach (UserDS.User user in userDS.Users)
            {
                if (!user.IsSaltNull())
                {
                    stringDS.DataStrings.AddDataString(user.Salt);
                }
                stringDS.AcceptChanges();
            }
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(stringDS);
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }
        private DataSet _doesUserExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            int _conId = ADOController.Instance.OpenNewConnection();

            //DBConnectionDS connDS = new DBConnectionDS();
            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommandAdvanced("DoesUserExist", "User.xml");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LogniId"].Value = stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            //nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, _conId, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _doesEmployeeExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesEmployeeExistInUser");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getConcurrenceLoginCount(DataSet inputDS)
        {
            DataLongDS longDS = new DataLongDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("GetConcurrenceLogin");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LoginId"].Value = stringDS.DataStrings[0].StringValue;
            }

            long nFailedLoginCount = UtilDL.GetId(connDS, cmd);
            longDS.DataLongs.AddDataLong(nFailedLoginCount);
            longDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(longDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getFailedLoginCount(DataSet inputDS)
        {
            DataLongDS longDS = new DataLongDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("GetFailedLogin");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LoginId"].Value = stringDS.DataStrings[0].StringValue;
            }

            long nFailedLoginCount = UtilDL.GetId(connDS, cmd);
            longDS.DataLongs.AddDataLong(nFailedLoginCount);
            longDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(longDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createUserRole(DataSet inputDS, string sLoginId)
        {
            DataSet responseDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            RoleDS roleDS = new RoleDS();
            roleDS.Merge(inputDS.Tables[roleDS.Roles.TableName], false, MissingSchemaAction.Error);
            roleDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateUserRole");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (RoleDS.Role role in roleDS.Roles)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                    return UtilDL.GetDBOperationFailed();

                cmd.Parameters["UserRoleId"].Value = genPK;
                cmd.Parameters["LoginId"].Value = sLoginId;

                if (role.IsNameNull() == false)
                {
                    cmd.Parameters["Name"].Value = role.Name;
                }
                else
                {
                    cmd.Parameters["Name"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEC_ROLE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _createUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            UserDS userDS = new UserDS();
            RoleDS roleDS = new RoleDS();
            DataSet responseDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateUser");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();

            userDS.Merge(inputDS.Tables[userDS.Users.TableName], false, MissingSchemaAction.Error);
            userDS.Merge(inputDS.Tables[userDS.AdministrativeRole.TableName], false, MissingSchemaAction.Error);
            userDS.AcceptChanges();

            roleDS.Merge(inputDS.Tables[roleDS.Roles.TableName], false, MissingSchemaAction.Error);
            roleDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            foreach (UserDS.User user in userDS.Users)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters["UserId"].Value = genPK;

                if (user.IsLoginIdNull() == false)
                {
                    cmd.Parameters["LogniId"].Value = user.LoginId;
                }
                else
                {
                    cmd.Parameters["LogniId"].Value = null;
                }
                if (user.IsEmployeeCodeNull() == false)
                {
                    cmd.Parameters["EmployeeCode"].Value = user.EmployeeCode;
                }
                else
                {
                    cmd.Parameters["EmployeeCode"].Value = null;
                }

                if (user.IsPasswordNull() == false)
                {
                    cmd.Parameters["Password"].Value = user.Password;
                }
                else
                {
                    cmd.Parameters["Password"].Value = null;
                }

                if (user.IsMaxConcurrencyLoginNull() == false)
                {
                    cmd.Parameters["MaxConcurrency"].Value = user.MaxConcurrencyLogin;
                }
                else
                {
                    cmd.Parameters["MaxConcurrency"].Value = null;
                }

                if (user.IsConcurrencyCountNull() == false)
                {
                    cmd.Parameters["ConcurrencyCount"].Value = user.ConcurrencyCount;
                }
                else
                {
                    cmd.Parameters["ConcurrencyCount"].Value = null;
                }

                if (user.IsMaxFailedLoginNull() == false)
                {
                    cmd.Parameters["MaxFailedLogin"].Value = user.MaxFailedLogin;
                }
                else
                {
                    cmd.Parameters["MaxFailedLogin"].Value = null;
                }

                if (user.IsEffectDateNull() == false)
                {
                    cmd.Parameters["EffectDate"].Value = user.EffectDate;
                }
                else
                {
                    cmd.Parameters["EffectDate"].Value = System.DBNull.Value;
                }

                if (user.IsExpDateNull() == false)
                {
                    cmd.Parameters["ExpDate"].Value = user.ExpDate;
                }
                else
                {
                    cmd.Parameters["ExpDate"].Value = System.DBNull.Value;
                }

                if (user.IsLockedNull() == false)
                {
                    cmd.Parameters["Locked"].Value = user.Locked;
                }
                else
                {
                    cmd.Parameters["Locked"].Value = null;
                }
                if (user.IsFailedLoginCountNull() == false)
                {
                    cmd.Parameters["FailedLoginCount"].Value = user.FailedLoginCount;
                }
                else
                {
                    cmd.Parameters["FailedLoginCount"].Value = null;
                }

                if (stringDS.DataStrings[0].IsStringValueNull() == false)
                {
                    cmd.Parameters["Salt"].Value = stringDS.DataStrings[0].StringValue;
                }
                else
                {
                    cmd.Parameters["Salt"].Value = null;
                }

                if (!user.IsSystemUrlNull()) cmd.Parameters["SystemUrl"].Value = user.SystemUrl;
                else cmd.Parameters["SystemUrl"].Value = System.DBNull.Value;

                cmd.Parameters["MenuID"].Value = user.MenuID;  //Hasinul

                if (!user.IsTemp_PasswordNull()) cmd.Parameters["Temp_Password"].Value = user.Temp_Password;
                else cmd.Parameters["Temp_Password"].Value = System.DBNull.Value;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                responseDS = _createUserRole(inputDS, user.LoginId);
                errDS.Clear();
                errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                errDS.AcceptChanges();
                if (errDS.Errors.Count > 0)
                {
                    bError = true;
                }

                #region Additional Administrative Role: Jarif, 21.Jun.2015
                if (!bError)
                {
                    OleDbCommand cmd_AARole = new OleDbCommand();
                    cmd_AARole.CommandText = "PRO_ADD_ADMINIS_ROLE_CREATE";
                    cmd_AARole.CommandType = CommandType.StoredProcedure;

                    if (cmd_AARole == null)
                    {
                        return UtilDL.GetCommandNotFound();
                    }

                    foreach (UserDS.AdministrativeRoleRow ARrow in userDS.AdministrativeRole.Rows)
                    {
                        cmd_AARole.Parameters.AddWithValue("p_LoginID", ARrow.LoginID);
                        cmd_AARole.Parameters.AddWithValue("p_AdministrativeRoleID", ARrow.AdministrativeRoleID);
                        cmd_AARole.Parameters.AddWithValue("p_AdministrativeArea", ARrow.AdministrativeArea);
                        cmd_AARole.Parameters.AddWithValue("p_ReportsOnly", ARrow.ReportsOnly);

                        bError = false;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_AARole, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd_AARole.Parameters.Clear();
                    }
                }
                #endregion

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEC_USER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                //Kaysar, 14-Feb-2011
                #region Update Email Address

                if (user.IsEMAILNull() == false && user.EMAIL != "")
                {
                    OleDbCommand cmdEmp = new OleDbCommand();
                    cmdEmp.CommandText = "PRO_EMAIL_ADDRESS_UPDATE";
                    cmdEmp.CommandType = CommandType.StoredProcedure;

                    if (user.IsEmployeeCodeNull() == false)
                    {
                        cmdEmp.Parameters.AddWithValue("p_EmployeeCode", user.EmployeeCode);
                    }
                    else
                    {
                        cmdEmp.Parameters.AddWithValue("p_EmployeeCode", DBNull.Value);
                    }

                    if (user.IsEMAILNull() == false)
                    {
                        cmdEmp.Parameters.AddWithValue("p_Email", user.EMAIL);
                    }
                    else
                    {
                        cmdEmp.Parameters.AddWithValue("p_Email", DBNull.Value);
                    }

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdEmp, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmdEmp.Parameters.Clear();

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_SEC_USER_ADD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                #endregion
                //
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataSet responseDS = new DataSet();
            UserDS userDS = new UserDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            bool bNeedSalt = false;

            userDS.Merge(inputDS.Tables[userDS.Users.TableName], false, MissingSchemaAction.Error);
            userDS.Merge(inputDS.Tables[userDS.AdministrativeRole.TableName], false, MissingSchemaAction.Error);
            userDS.AcceptChanges();

            OleDbCommand cmd;
            if (userDS.Users[0].NeedSalt == true)
            {
                cmd = DBCommandProvider.GetDBCommand("UpdateUserWithSalt");
                bNeedSalt = true;
            }
            else cmd = DBCommandProvider.GetDBCommand("UpdateUserWithoutSalt");

            if (cmd == null) return UtilDL.GetCommandNotFound();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (UserDS.User user in userDS.Users)
            {
                if (user.IsLoginIdNull() == false)
                {
                    cmd.Parameters["LogniId"].Value = user.LoginId;
                }
                else
                {
                    cmd.Parameters["LogniId"].Value = null;
                }

                if (user.IsMaxConcurrencyLoginNull() == false)
                {
                    cmd.Parameters["MaxConcurrency"].Value = user.MaxConcurrencyLogin;
                }
                else
                {
                    cmd.Parameters["MaxConcurrency"].Value = null;
                }

                if (user.IsMaxFailedLoginNull() == false)
                {
                    cmd.Parameters["MaxFailedLogin"].Value = user.MaxFailedLogin;
                }
                else
                {
                    cmd.Parameters["MaxFailedLogin"].Value = null;
                }

                if (user.IsEffectDateNull() == false)
                {
                    cmd.Parameters["EffectDate"].Value = user.EffectDate;
                }
                else
                {
                    cmd.Parameters["EffectDate"].Value = System.DBNull.Value;
                }

                if (user.IsExpDateNull() == false)
                {
                    cmd.Parameters["ExpDate"].Value = user.ExpDate;
                }
                else
                {
                    cmd.Parameters["ExpDate"].Value = System.DBNull.Value;
                }

                if (user.IsLockedNull() == false)
                {
                    cmd.Parameters["Locked"].Value = user.Locked;
                }
                else
                {
                    cmd.Parameters["Locked"].Value = null;
                }
                if (bNeedSalt == true)
                {
                    if (user.IsPasswordNull() == false)
                    {
                        cmd.Parameters["Password"].Value = user.Password;
                    }
                    else
                    {
                        cmd.Parameters["Password"].Value = null;
                    }
                    if (stringDS.DataStrings[0].IsStringValueNull() == false)
                    {
                        cmd.Parameters["Salt"].Value = stringDS.DataStrings[0].StringValue;
                    }
                    else
                    {
                        cmd.Parameters["Salt"].Value = null;
                    }

                    if (!user.IsTemp_PasswordNull()) cmd.Parameters["Temp_Password"].Value = user.Temp_Password;
                    else cmd.Parameters["Temp_Password"].Value = System.DBNull.Value;
                }

                if (!user.IsSystemUrlNull()) cmd.Parameters["SystemUrl"].Value = user.SystemUrl;
                else cmd.Parameters["SystemUrl"].Value = System.DBNull.Value;


                // Jarif, 31.Mar.2015
                if (!user.IsLoginID_OldNull()) cmd.Parameters["LogniId_Old"].Value = user.LoginID_Old;
                else cmd.Parameters["LogniId_Old"].Value = null;
                //Hasinul
                cmd.Parameters["MenuID"].Value = user.MenuID;

                if (!user.IsUpdate_UserNull()) cmd.Parameters["Update_User"].Value = user.Update_User;
                else cmd.Parameters["Update_User"].Value = System.DBNull.Value;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == false)
                {
                    responseDS = _deleteRole(inputDS, user.LoginId);
                    errDS.Clear();
                    errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                    errDS.AcceptChanges();
                    if (errDS.Errors.Count > 0)
                    {
                        bError = true;
                    }
                }

                if (bError == false)
                {
                    responseDS = _createUserRole(inputDS, user.LoginId);
                    errDS.Clear();
                    errDS.Merge(responseDS.Tables[errDS.Errors.TableName], false, MissingSchemaAction.Error);
                    errDS.AcceptChanges();
                    if (errDS.Errors.Count > 0)
                    {
                        bError = true;
                    }
                }

                #region Additional Administrative Role: Jarif, 21.Jun.2015
                if (!bError)
                {
                    OleDbCommand cmd_AARole = new OleDbCommand();
                    cmd_AARole.CommandText = "PRO_ADD_ADMINIS_ROLE_CREATE";
                    cmd_AARole.CommandType = CommandType.StoredProcedure;

                    OleDbCommand cmd_AARole_Del = new OleDbCommand();
                    cmd_AARole_Del.CommandText = "PRO_ADD_ADMINIS_ROLE_DELETE";
                    cmd_AARole_Del.CommandType = CommandType.StoredProcedure;

                    if (cmd_AARole == null || cmd_AARole_Del == null)
                    {
                        return UtilDL.GetCommandNotFound();
                    }

                    cmd_AARole_Del.Parameters.AddWithValue("p_LoginID", user.LoginId);
                    bError = false;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_AARole_Del, connDS.DBConnections[0].ConnectionID, ref bError);

                    foreach (UserDS.AdministrativeRoleRow ARrow in userDS.AdministrativeRole.Rows)
                    {
                        cmd_AARole.Parameters.AddWithValue("p_LoginID", ARrow.LoginID);
                        cmd_AARole.Parameters.AddWithValue("p_AdministrativeRoleID", ARrow.AdministrativeRoleID);
                        cmd_AARole.Parameters.AddWithValue("p_AdministrativeArea", ARrow.AdministrativeArea);
                        cmd_AARole.Parameters.AddWithValue("p_ReportsOnly", ARrow.ReportsOnly);

                        bError = false;
                        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_AARole, connDS.DBConnections[0].ConnectionID, ref bError);
                        cmd_AARole.Parameters.Clear();
                    }
                }
                #endregion

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEC_USER_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                //Kaysar, 14-Feb-2011
                #region Update Email Address

                OleDbCommand cmdEmp = new OleDbCommand();
                cmdEmp.CommandText = "PRO_EMAIL_ADDRESS_UPDATE";
                cmdEmp.CommandType = CommandType.StoredProcedure;

                if (user.IsEmployeeCodeNull() == false)
                {
                    cmdEmp.Parameters.AddWithValue("p_EmployeeCode", user.EmployeeCode);
                }
                else
                {
                    cmdEmp.Parameters.AddWithValue("p_EmployeeCode", DBNull.Value);
                }

                if (user.IsEMAILNull() == false)
                {
                    cmdEmp.Parameters.AddWithValue("p_Email", user.EMAIL);
                }
                else
                {
                    cmdEmp.Parameters.AddWithValue("p_Email", DBNull.Value);
                }

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdEmp, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdEmp.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEC_USER_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

                #endregion
                //
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _changeUserPassword(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserPwdChangeDS pwdDS = new UserPwdChangeDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("ChangePassword");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();

            pwdDS.Merge(inputDS.Tables[pwdDS.PasswordChanges.TableName], false, MissingSchemaAction.Error);
            pwdDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            foreach (UserPwdChangeDS.PasswordChange pwd in pwdDS.PasswordChanges)
            {
                if (!pwd.IsExpDateNull()) cmd.Parameters["ExpiryDate"].Value = pwd.ExpDate;
                else cmd.Parameters["ExpiryDate"].Value = DBNull.Value;

                if (pwd.IsLoginIdNull() == false)
                {
                    cmd.Parameters["LogniId"].Value = pwd.LoginId;
                }
                else
                {
                    cmd.Parameters["LogniId"].Value = null;
                }

                if (pwd.IsNewPasswordNull() == false)
                {
                    cmd.Parameters["Password"].Value = pwd.NewPassword;
                }
                else
                {
                    cmd.Parameters["Password"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_SEC_PWD_CHANGE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteRole(DataSet inputDS, string sLoginId)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteRole");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();

            cmd.Parameters["LoginId"].Value = sLoginId;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                //err.ErrorInfo1 = ActionID.ACTION_USER_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }
        private DataSet _getUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();
            bool bError = false;
            int nRowAffected = -1;

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            string scode = stringDS.DataStrings[0].StringValue;

            #region Data Load for User
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUser_New");

            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                //if (stringDS.DataStrings.Count > 1) // Most of times, SupervisorParam is not provided.
                //{ cmd.Parameters["SupervisorParam"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue); }
                //else
                //{ cmd.Parameters["SupervisorParam"].Value = Convert.ToInt32(Supervisor_SearchParam.General); }

                cmd.Parameters["LoginId1"].Value = stringDS.DataStrings[0].StringValue;

                //cmd.Parameters["LoginId2"].Value = stringDS.DataStrings[0].StringValue;
                //cmd.Parameters["LoginId3"].Value = stringDS.DataStrings[0].StringValue;
                //cmd.Parameters["LoginId4"].Value = stringDS.DataStrings[0].StringValue;
                //cmd.Parameters["LoginId5"].Value = stringDS.DataStrings[0].StringValue;
                //cmd.Parameters["LoginId6"].Value = stringDS.DataStrings[0].StringValue;
                //cmd.Parameters["LoginId7"].Value = stringDS.DataStrings[0].StringValue;
                //cmd.Parameters["LoginId8"].Value = stringDS.DataStrings[0].StringValue;
                //cmd.Parameters["LoginId9"].Value = stringDS.DataStrings[0].StringValue;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_SEC_USER_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            #region Data Load For Additional Administrative Roules...
            OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("GetAdministrativeRoleByLoginID");
            cmd1.Parameters["LoginID"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter1 = new OleDbDataAdapter();
            adapter1.SelectCommand = cmd1;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter1, userDS, userDS.UAARole.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            userDS.AcceptChanges();
            #endregion

            stringDS.Clear();
            stringDS.AcceptChanges();
            foreach (UserDS.User user in userDS.Users)
            {
                if (!user.IsSaltNull())
                {
                    stringDS.DataStrings.AddDataString(user.Salt);
                }
                stringDS.AcceptChanges();
            }
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(stringDS);
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _setFailedLoginCount(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("SetFailedLogin");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();

            longDS.Merge(inputDS.Tables[longDS.DataLongs.TableName], false, MissingSchemaAction.Error);
            longDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (longDS.DataLongs[0].IsLongValueNull() == false)
            {
                cmd.Parameters["FailedLoginCount"].Value = longDS.DataLongs[0].LongValue;

            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LogniId"].Value = stringDS.DataStrings[0].StringValue;

            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEC_FAILED_LOGIN_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _setLockedStatus(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();
            DataBoolDS boolDS = new DataBoolDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("SetLocked");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();

            boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            boolDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (boolDS.DataBools[0].IsBoolValueNull() == false)
            {
                cmd.Parameters["Locked"].Value = boolDS.DataBools[0].BoolValue;

            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LogniId"].Value = stringDS.DataStrings[0].StringValue;

            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEC_FAILED_LOGIN_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _setConcurrenceLoginCount(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("SetConcurrenceLogin");
            if (cmd == null)
                return UtilDL.GetCommandNotFound();

            longDS.Merge(inputDS.Tables[longDS.DataLongs.TableName], false, MissingSchemaAction.Error);
            longDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (longDS.DataLongs[0].IsLongValueNull() == false)
            {
                cmd.Parameters["ConcurrencyCount"].Value = longDS.DataLongs[0].LongValue;

            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LogniId"].Value = stringDS.DataStrings[0].StringValue;

            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEC_FAILED_LOGIN_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _setConcurrenceLoginCountWithRM(DataSet inputDS)
        {
            #region Create By Jarif, 02 Feb 2012...
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();

            DataLongDS longDS = new DataLongDS();
            longDS.Merge(inputDS.Tables[longDS.DataLongs.TableName], false, MissingSchemaAction.Error);
            longDS.AcceptChanges();

            UserDS RememberMeUserDS = new UserDS();
            RememberMeUserDS.Merge(inputDS.Tables[RememberMeUserDS.RememberMe.TableName], false, MissingSchemaAction.Error);
            RememberMeUserDS.AcceptChanges();
            UserDS.RememberMeRow rmUser = (UserDS.RememberMeRow)RememberMeUserDS.RememberMe.Rows[0];

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            if (!rmUser.IsRememberMe) cmd.CommandText = "pro_Concurrence_Login_Delete";
            else if (rmUser.RemembermeID == 0) cmd.CommandText = "pro_Concurrence_Login_Set";
            else cmd.CommandText = "pro_Concurrence_Login_Update";

            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (!longDS.DataLongs[0].IsLongValueNull())
            {
                cmd.Parameters.AddWithValue("p_ConcurrencyCount", longDS.DataLongs[0].LongValue);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_ConcurrencyCount", DBNull.Value);
            }
            if (!stringDS.DataStrings[0].IsStringValueNull())
            {
                cmd.Parameters.AddWithValue("p_LogniId", stringDS.DataStrings[0].StringValue);
            }
            else
            {
                cmd.Parameters.AddWithValue("p_LogniId", DBNull.Value);
            }

            if (rmUser.IsRememberMe && rmUser.RemembermeID == 0)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters.AddWithValue("p_RememberMeID", genPK);
            }
            else cmd.Parameters.AddWithValue("p_RememberMeID", rmUser.RemembermeID);

            cmd.Parameters.AddWithValue("p_IPAddress", rmUser.IPAddress);
            cmd.Parameters.AddWithValue("p_PCName", rmUser.PCName);
            cmd.Parameters.AddWithValue("p_IsRememberMe", rmUser.IsRememberMe);
            cmd.Parameters.AddWithValue("p_RMPWD", rmUser.RMPWD);
            cmd.Parameters.AddWithValue("p_RMSALT", rmUser.RMSalt);
            cmd.Parameters.AddWithValue("p_Remarks", rmUser.Remarks);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEC_CONCURRENCE_LOGIN_SET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
            #endregion
        }

        private DataSet _getUserList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUserList");//Jarif(01 Oct 11)
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUserList_New");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(01 Oct 11)...
            #region Old...
            //UserPO userPO = new UserPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, userPO, userPO.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_USER.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //userPO.AcceptChanges();
            //UserDS userDS = new UserDS();
            ////DataStringDS stringDS = new DataStringDS();
            //if (userPO.Users.Count > 0)
            //{
            //    foreach (UserPO.User poUser in userPO.Users)
            //    {
            //        UserDS.User user = userDS.Users.NewUser();
            //        if (poUser.IsLoginIdNull() == false)
            //        {
            //            user.LoginId = poUser.LoginId;
            //        }
            //        if (poUser.IsPasswordNull() == false)
            //        {
            //            user.Password = poUser.Password;
            //        }
            //        if (poUser.IsMaxConcurrencyLoginNull() == false)
            //        {
            //            user.MaxConcurrencyLogin = poUser.MaxConcurrencyLogin;
            //        }
            //        if (poUser.IsConcurrencyCountNull() == false)
            //        {
            //            user.ConcurrencyCount = poUser.ConcurrencyCount;
            //        }
            //        if (poUser.IsMaxFailedLoginNull() == false)
            //        {
            //            user.MaxFailedLogin = poUser.MaxFailedLogin;
            //        }
            //        if (poUser.IsFailedLoginCountNull() == false)
            //        {
            //            user.FailedLoginCount = poUser.FailedLoginCount;
            //        }
            //        if (poUser.IsEffectDateNull() == false)
            //        {
            //            user.EffectDate = poUser.EffectDate;
            //        }
            //        if (poUser.IsExpDateNull() == false)
            //        {
            //            user.ExpDate = poUser.ExpDate;
            //        }
            //        if (poUser.IsLockedNull() == false)
            //        {
            //            user.Locked = poUser.Locked;
            //        }
            //        if (poUser.IsEmployeeCodeNull() == false)
            //        {
            //            user.EmployeeCode = poUser.EmployeeCode;
            //        }

            //        //Kaysar, 24-Jan-2011
            //        if (poUser.IsDEPARTMENTCODENull() == false)
            //        {
            //            user.DepartmentCode = poUser.DEPARTMENTCODE;
            //        }
            //        if (poUser.IsDELEGATEDUSERIDNull() == false)
            //        {
            //            user.DelegatedUserID = poUser.DELEGATEDUSERID;
            //        }
            //        //

            //        //Kaysar, 15-Feb-2011
            //        if (poUser.IsEMAILNull() == false)
            //        {
            //            user.EMAIL = poUser.EMAIL;
            //        }
            //        //

            //        //Kaysar, 03-May-2011
            //        if (poUser.IsSiteIDNull() == false)
            //        {
            //            user.SiteID = poUser.SiteID;
            //        }
            //        //


            //        //if (poUser.IsSALTNull() == false)
            //        //{
            //        //  stringDS.DataStrings.AddDataString(poUser.SALT);
            //        //}
            //        userDS.Users.AddUser(user);
            //        userDS.AcceptChanges();
            //        // stringDS.AcceptChanges(); 
            //    }
            //}
            #endregion

            UserDS userDS = new UserDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_USER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            //returnDS.Merge(stringDS);
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _getRoleListByUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRoleListByUser");//Jarif(01 Oct 11)
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRoleListByUser_New");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                string st = stringDS.DataStrings[0].StringValue;
                cmd.Parameters["LoginId"].Value = stringDS.DataStrings[0].StringValue;

            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(01 Oct 11)...
            #region Old...
            //RolePO rolePO = new RolePO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, rolePO, rolePO.Roles.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_ROLE_BY_USER.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //rolePO.AcceptChanges();
            //RoleDS roleDS = new RoleDS();
            //if (rolePO.Roles.Count > 0)
            //{
            //    foreach (RolePO.Role poRole in rolePO.Roles)
            //    {
            //        RoleDS.Role role = roleDS.Roles.NewRole();
            //        if (poRole.IsNameNull() == false)
            //        {
            //            role.Name = poRole.Name;
            //        }
            //        if (poRole.IsDescriptionNull() == false)
            //        {
            //            role.Description = poRole.Description;
            //        }
            //        roleDS.Roles.AddRole(role);
            //        roleDS.AcceptChanges();
            //    }
            //}
            #endregion

            RoleDS roleDS = new RoleDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, roleDS, roleDS.Roles.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_ROLE_BY_USER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            roleDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(roleDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _getPrevilageList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPrevilageList");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            PrevilagePO prevPO = new PrevilagePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              prevPO,
              prevPO.Previlages.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_Q_ALL_PREVILAGE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            prevPO.AcceptChanges();
            PrevilageDS prevDS = new PrevilageDS();
            if (prevPO.Previlages.Count > 0)
            {
                foreach (PrevilagePO.Previlage poPrevilage in prevPO.Previlages)
                {
                    PrevilageDS.Previlage prev = prevDS.Previlages.NewPrevilage();
                    if (poPrevilage.IsActionIdNull() == false)
                    {
                        prev.ActionId = poPrevilage.ActionId;
                    }
                    if (poPrevilage.IsFriendlyNameNull() == false)
                    {
                        prev.FriendlyName = poPrevilage.FriendlyName;
                    }
                    prevDS.Previlages.AddPrevilage(prev);
                    prevDS.AcceptChanges();
                }
            }


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(prevDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _getEmployeeList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeForUser");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            EmployeePO employeePO = new EmployeePO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              employeePO,
              employeePO.Employees.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            employeePO.AcceptChanges();
            EmployeeDS employeeDS = new EmployeeDS();
            if (employeePO.Employees.Count > 0)
            {
                foreach (EmployeePO.Employee poEmployee in employeePO.Employees)
                {
                    EmployeeDS.Employee employee = employeeDS.Employees.NewEmployee();
                    if (poEmployee.IsEmployeeCodeNull() == false)
                    {
                        employee.EmployeeCode = poEmployee.EmployeeCode;
                    }
                    if (poEmployee.IsEmployeeNameNull() == false)
                    {
                        employee.EmployeeName = poEmployee.EmployeeName;
                    }
                    employeeDS.Employees.AddEmployee(employee);
                    employeeDS.AcceptChanges();
                }
            }


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(employeeDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _getMaxFailedLogin(DataSet inputDS)
        {
            DataLongDS longDS = new DataLongDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("GetMaxFailedLogin");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LoginId"].Value = stringDS.DataStrings[0].StringValue;
            }

            long nFailedLoginCount = UtilDL.GetId(connDS, cmd);
            longDS.DataLongs.AddDataLong(nFailedLoginCount);
            longDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(longDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getLockedStatus(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("GetLocked");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LoginId"].Value = stringDS.DataStrings[0].StringValue;
            }

            long nLocked = UtilDL.GetId(connDS, cmd);
            bool bLocked;
            if (nLocked == -1)
            {
                bLocked = true;
            }
            else
            {
                bLocked = false;

            }
            boolDS.DataBools.AddDataBool(bLocked);
            boolDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getMaxConcurrenceLogin(DataSet inputDS)
        {
            DataLongDS longDS = new DataLongDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("GetMaxConcurrenceLogin");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["LoginId"].Value = stringDS.DataStrings[0].StringValue;
            }

            long nFailedLoginCount = UtilDL.GetId(connDS, cmd);
            longDS.DataLongs.AddDataLong(nFailedLoginCount);
            longDS.AcceptChanges();


            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(longDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getUserByID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUserByID");

            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            if (!integerDS.DataIntegers[0].IsIntegerValueNull())
            {
                Int32 userID = integerDS.DataIntegers[0].IntegerValue;

                cmd.Parameters["UserID1"].Value = userID;
                cmd.Parameters["UserID2"].Value = userID;
                cmd.Parameters["UserID3"].Value = userID;
                cmd.Parameters["UserID4"].Value = userID;
                cmd.Parameters["UserID5"].Value = userID;
                cmd.Parameters["UserID6"].Value = userID;
                cmd.Parameters["UserID7"].Value = userID;
                cmd.Parameters["UserID8"].Value = userID;
                cmd.Parameters["UserID9"].Value = userID;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            UserDS userDS = new UserDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_USER_BY_USERID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            foreach (UserDS.User user in userDS.Users)
            {
                if (!user.IsSaltNull())
                {
                    stringDS.DataStrings.AddDataString(user.Salt);
                }
                stringDS.AcceptChanges();
            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(stringDS);
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getUserListByCompanyCode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUserListByCompanyCode");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;
            cmd.Parameters["CompanyCode"].Value = stringDS.DataStrings[0].StringValue;

            UserDS userDS = new UserDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_USER_LIST_BY_COMPANY_CODE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            //returnDS.Merge(stringDS);
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getActiveUserList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetActivUserList");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            UserDS userDS = new UserDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACTIVE_USER_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            //returnDS.Merge(stringDS);
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet _getPrimaryLeavePlanHolderUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPLPHUser");
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_PLPH_USER_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getLeavePlanVerifierUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetPLVUser");
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_PLV_USER_GET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getVerifierList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetVerifierList");
            if (cmd == null)
            {

                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["SiteID"].Value = integerDS.DataIntegers[0].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            UserDS userDS = new UserDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_VERIFIER_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            //returnDS.Merge(stringDS);
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;


        }

        private DataSet GetAdministrativeRoleAllByLoginID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAdministrativeRoleAllByLoginID");
            cmd.Parameters["LoginID"].Value = stringDS.DataStrings[0].StringValue;

            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            UserDS userDS = new UserDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.AdministrativeRole.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GetAdministrativeRoleAllByLoginID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet GetUserList_ByFilters(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            UserDS userDS = new UserDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUserList_ByFilter");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["DepartmentCode1"].Value = cmd.Parameters["DepartmentCode2"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["DesignationCode1"].Value = cmd.Parameters["DesignationCode2"].Value = stringDS.DataStrings[1].StringValue;
            cmd.Parameters["SiteCode1"].Value = cmd.Parameters["SiteCode2"].Value = stringDS.DataStrings[2].StringValue;
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_USERS_BY_FILTERS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            userDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet GetApproverUsers_Accounting(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            UserDS userDS = new UserDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetApproverUsers_Accounting");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_ACC_GET_APPROVER_USERS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            userDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet SetUser_LoginFlag(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("SetUser_LoginFlag");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            if (!stringDS.DataStrings[1].IsStringValueNull()) cmd.Parameters["LoginFlag"].Value = Convert.ToBoolean(stringDS.DataStrings[1].StringValue);
            if (!stringDS.DataStrings[0].IsStringValueNull()) cmd.Parameters["LogniId"].Value = stringDS.DataStrings[0].StringValue;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEC_LOGIN_FLAG_SET.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet SaveUser_Log(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();

            userDS.Merge(inputDS.Tables[userDS.Users.TableName], false, MissingSchemaAction.Error);
            userDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_SEC_USER_LOG_SAVE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (!userDS.Users[0].IsLoginIdNull()) cmd.Parameters.AddWithValue("p_LoginId", userDS.Users[0].LoginId);
            if (!userDS.Users[0].IsMachineNameNull()) cmd.Parameters.AddWithValue("p_MachineName", userDS.Users[0].MachineName);
            if (!userDS.Users[0].IsIP_AddressNull()) cmd.Parameters.AddWithValue("p_IP_Address", userDS.Users[0].IP_Address);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            // WALI :: 08-Jan-2017 :: Shakir vai faced prob in UCB
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.ACTION_SEC_USER_LOG_SAVE.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;
            //}

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet GetUserLog_ActiveUsers(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            UserDS userDS = new UserDS();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUserLog_ActiveUsers");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            cmd.Parameters["ThisUser"].Value = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = stringDS.DataStrings[1].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SEC_USER_LOG.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            userDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet DisconnectUser_Forcefully(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            MessageDS messageDS = new MessageDS();

            userDS.Merge(inputDS.Tables[userDS.Users.TableName], false, MissingSchemaAction.Error);
            userDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("SetUser_LoginFlag");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (UserDS.User user in userDS.Users)
            {
                if (!user.IsIs_Logged_InNull()) cmd.Parameters["LoginFlag"].Value = user.Is_Logged_In;
                if (!user.IsLoginIdNull()) cmd.Parameters["LogniId"].Value = user.LoginId;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEC_USER_DISCONNECT_FORCEFULLY.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    //return errDS;

                    messageDS.ErrorMsg.AddErrorMsgRow(user.LoginId + " [" + user.EmployeeName + "]");
                }
                else
                { messageDS.SuccessMsg.AddSuccessMsgRow(user.LoginId + " [" + user.EmployeeName + "]"); }
            }

            //errDS.Clear();
            messageDS.AcceptChanges();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(errDS.Errors);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet Check_UserLoginState(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            UserDS userDS = new UserDS();
            DataBoolDS boolDS = new DataBoolDS();

            userDS.Merge(inputDS.Tables[userDS.Users.TableName], false, MissingSchemaAction.Error);
            userDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("Check_UserLoginState");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["LoginID"].Value = userDS.Users[0].LoginId;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_USER_LOGIN_FLAG_CHECK.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet UpdateUser_OnLogon(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd.CommandText = "PRO_UPDATE_USER_ON_LOGON";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("p_LoginId", stringDS.DataStrings[0].StringValue);
            cmd.Parameters.AddWithValue("p_ConcurrentCount", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_FailedCount", Convert.ToInt32(stringDS.DataStrings[2].StringValue));

            if (stringDS.DataStrings[3].StringValue.Trim() != "")
                cmd.Parameters.AddWithValue("p_LoginFlag", Convert.ToInt32("-1"));
            else cmd.Parameters.AddWithValue("p_LoginFlag", DBNull.Value);

            cmd.Parameters.AddWithValue("p_MachineName", stringDS.DataStrings[4].StringValue);
            cmd.Parameters.AddWithValue("p_IP_Address", stringDS.DataStrings[5].StringValue);

            bool bError = false;
            int nRowAffected = -1;



            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEC_USER_UPDATE_ON_LOGON.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet GetUserMenuList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            UserDS userDS = new UserDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetUserMenuList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.UserMenu.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_USER_MENU_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            userDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(userDS.UserMenu);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createDelegate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_DELEGATE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;

            string messageBody = "", returnMessage = "";

            //DataBoolDS boolDS = new DataBoolDS();
            //boolDS.Merge(inputDS.Tables[boolDS.DataBools.TableName], false, MissingSchemaAction.Error);
            //boolDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            //DataIntegerDS integerDS = new DataIntegerDS();
            //integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            //integerDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Email Information
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            string mailingSubject = UtilDL.GetMailSubForDelegate();

            string senderEmpName = "", SenderEmpCode = "", CcEmailAddress = "", fromEmailAddress = "", emailAddress = "", comments = "";
            if (!stringDS.DataStrings[5].IsStringValueNull()) senderEmpName = stringDS.DataStrings[5].StringValue;
            if (!stringDS.DataStrings[6].IsStringValueNull()) SenderEmpCode = stringDS.DataStrings[6].StringValue;
            if (!stringDS.DataStrings[7].IsStringValueNull()) fromEmailAddress = stringDS.DataStrings[7].StringValue;
            if (!stringDS.DataStrings[8].IsStringValueNull()) emailAddress = stringDS.DataStrings[8].StringValue;
            if (!stringDS.DataStrings[9].IsStringValueNull()) comments = stringDS.DataStrings[9].StringValue;
            #endregion

            cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[1].StringValue);
            cmd.Parameters.AddWithValue("p_dUserID", Convert.ToInt32(stringDS.DataStrings[2].StringValue));
            cmd.Parameters.AddWithValue("p_DateFrom", Convert.ToDateTime(stringDS.DataStrings[3].StringValue));
            cmd.Parameters.AddWithValue("p_DateTo", Convert.ToDateTime(stringDS.DataStrings[4].StringValue));
            cmd.Parameters.AddWithValue("p_LeaveRequestID", DBNull.Value);

            bool bError = false;
            int nRowAffected = -1;

            if (Convert.ToBoolean(stringDS.DataStrings[0].StringValue))
            {
                fromEmailAddress = credentialEmailAddress;
                returnMessage = "";

                messageBody = "<b>Sender: </b>" + senderEmpName + " [" + SenderEmpCode + "]" + "<br>";
                messageBody += "<b>Duration: </b>" + stringDS.DataStrings[3].StringValue + " to " + stringDS.DataStrings[4].StringValue + "<br><br>";
                messageBody += "<br><br><br>";
                messageBody += comments;
                messageBody += "<br>";

                Mail.Mail mail = new Mail.Mail();
                if (IsEMailSendWithCredential)
                {
                    //returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, row.FromEmailAddress, row.EmailAddress, row.MailingSubject, messageBody);
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, fromEmailAddress, emailAddress, CcEmailAddress, mailingSubject, messageBody, senderEmpName);
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, fromEmailAddress, emailAddress, CcEmailAddress, mailingSubject, messageBody, senderEmpName);
                }
                //senderEmpName
                if (returnMessage == "Email successfully sent.")
                {
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                }
                else
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DELEGATE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            else
            {
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            }

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DELEGATE_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateDelegate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_DELEGATE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            Int32 delegatedUserID = Convert.ToInt32(stringDS.DataStrings[2].StringValue);

            cmd.Parameters.AddWithValue("p_Delegate_ID", Convert.ToInt32(stringDS.DataStrings[1].StringValue));
            cmd.Parameters.AddWithValue("p_dUserID", delegatedUserID);

            if (delegatedUserID != 0) cmd.Parameters.AddWithValue("p_DateFrom", Convert.ToDateTime(stringDS.DataStrings[3].StringValue));
            else cmd.Parameters.AddWithValue("p_DateFrom", DBNull.Value);

            if (delegatedUserID != 0) cmd.Parameters.AddWithValue("p_DateTo", Convert.ToDateTime(stringDS.DataStrings[4].StringValue));
            else cmd.Parameters.AddWithValue("p_DateTo", DBNull.Value);

            bool bError = false;
            int nRowAffected = -1;

            #region Send MAIL
            //if (!boolDS.DataBools[0].IsBoolValueNull() && boolDS.DataBools[0].BoolValue)
            //{
            //    returnMessage = "";

            //    messageBody = "<b>Sender: </b>" + senderEmpName + " [" + SenderEmpCode + "]" + "<br><br>";
            //    messageBody += "<br><br><br>";
            //    messageBody += comments;
            //    messageBody += "<br>";

            //    Mail.Mail mail = new Mail.Mail();
            //    if (IsEMailSendWithCredential)
            //    {
            //        //returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, row.FromEmailAddress, row.EmailAddress, row.MailingSubject, messageBody);
            //        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, fromEmailAddress, emailAddress, CcEmailAddress, mailingSubject, messageBody, senderEmpName);
            //    }
            //    else
            //    {
            //        returnMessage = mail.SendEmail(host, port, fromEmailAddress, emailAddress, CcEmailAddress, mailingSubject, messageBody, senderEmpName);
            //    }
            //    //senderEmpName
            //    if (returnMessage == "Email successfully sent.")
            //    {
            //        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            //    }
            //    else
            //    {
            //        ErrorDS.Error err = errDS.Errors.NewError();
            //        err.ErrorCode = ErrorCode.ERR_EMAIL_OPERATION_FAILED.ToString();
            //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //        err.ErrorInfo1 = ActionID.ACTION_DELEGATE_CREATE.ToString();
            //        errDS.Errors.AddError(err);
            //        errDS.AcceptChanges();
            //        return errDS;
            //    }
            //}
            //else
            //{
            //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            //}
            #endregion

            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DELEGATE_UPDATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createModuleHibernatation(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            UserDS userDS = new UserDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SYS_MODULE_HIBERNATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            userDS.Merge(inputDS.Tables[userDS.AdministrativeRole.TableName], false, MissingSchemaAction.Error);
            userDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Add Question...
            long genPK = IDGenerator.GetNextGenericPK();
            foreach (UserDS.AdministrativeRoleRow row in userDS.AdministrativeRole.Rows)
            {
                cmd.Parameters.AddWithValue("p_AdministrativeRoleID", row.AdministrativeRoleID);
                cmd.Parameters.AddWithValue("p_IsHibernate", row.IsHibernate);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SYSTEM_MODULE_HIBERNATE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.AdministrativeRoleName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.AdministrativeRoleName);
                messageDS.AcceptChanges();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet GetAdministrativeRoleByModuleID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAdministrativeRoleByModuleID");
            cmd.Parameters["ModuleID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            UserDS userDS = new UserDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.AdministrativeRole.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_ADMINISTRATIVEROLE_BYMODULEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            userDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(userDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }


    }
}
