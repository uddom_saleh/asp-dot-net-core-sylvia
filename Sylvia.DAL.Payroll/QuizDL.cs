/* *********************************************************************
 *  Compamy: Milllennium Information Solution Limited		 		           *  
 *  Author: Md. Hasinur Rahman Likhon				 				                   *
 *  Comment Date: April 17, 2006									                     *
 ***********************************************************************/

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
	/// <summary>
	/// Summary description for PayrollSiteDL.
	/// </summary>
    public class QuizDL
    {
        public QuizDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {

                case ActionID.ACTION_QUIZQUESTION_ADD:
                    return _createQuizQuestion(inputDS);
                case ActionID.ACTION_QUIZQUESTION_UPD:
                    return _updateQuizQuestion(inputDS);
                case ActionID.NA_ACTION_GET_QUIZQUESTION_LIST:
                    return _getQuizQuestion(inputDS);
                case ActionID.ACTION_QUIZQUESTION_DEL:
                    return _deleteQuizQuestion(inputDS);

                case ActionID.ACTION_SEP_LIABILITY_QUESTIONNAIRE_ADD:
                    return _createSep_LiabilityQuestionnaire(inputDS);
                case ActionID.NA_ACTION_GET_SEP_LIABILITY_QUESTION_LIST:
                    return _getSep_LiabilityQuestionnaire(inputDS);
                case ActionID.NA_ACTION_SEP_LIABILITY_QUESTION_ANSWER:
                    return _createSep_LiabilityQuestionnaireAnswers(inputDS);
                case ActionID.NA_ACTION_ADD_SEP_LIABILITY_COLLECTION:
                    return _createSep_LiabilityCollection(inputDS);
                case ActionID.NA_ACTION_SEP_LIABILITY_COLLECTION_REQ_SEND:
                    return this._sendLiabilityCollectionRequest(inputDS);

                case ActionID.NA_ACTION_EXIT_INTERVIEW_CALL:
                    return this._callForExitInterview(inputDS);
                case ActionID.NA_ACTION_EXIT_INTERVIEW_ADD:
                    return _createExitInterview(inputDS);
                case ActionID.NA_ACTION_GET_EXITINTERVIEWINFO:
                    return _getExitInterviewInfo(inputDS);

                case ActionID.NA_ACTION_ACKNOWLEDGE_EMPLOYEE:
                    return this._AcknowledgeEmployee(inputDS);


                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _createQuizQuestion(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            QuizDS quizDS = new QuizDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_QUIZ_QUESTION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Option = new OleDbCommand();
            cmd_Option.CommandText = "PRO_QUIZ_QUESTION_OPTION_ADD";
            cmd_Option.CommandType = CommandType.StoredProcedure;

            if (cmd == null || cmd_Option == null) return UtilDL.GetCommandNotFound();

            quizDS.Merge(inputDS.Tables[quizDS.Quiz_Question.TableName], false, MissingSchemaAction.Error);
            quizDS.Merge(inputDS.Tables[quizDS.Quiz_QuestionOptions.TableName], false, MissingSchemaAction.Error);
            quizDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Add Question...
            long genPK = IDGenerator.GetNextGenericPK();
            foreach (QuizDS.Quiz_QuestionRow row in quizDS.Quiz_Question.Rows)
            {
                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd.Parameters.AddWithValue("p_QuestionID", (object)genPK);
                cmd.Parameters.AddWithValue("p_QuestionDescription", row.QuestionDescription );
                cmd.Parameters.AddWithValue("p_IsMultipleChoice", row.IsMultipleChoice);
                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("p_IsMandatory", row.IsMandatory);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_QUIZQUESTION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion 

            #region Add Question Options...
            foreach (QuizDS.Quiz_QuestionOptionsRow row_Options in quizDS.Quiz_QuestionOptions.Rows)
            {
                long genPK_Options = IDGenerator.GetNextGenericPK();
                if (genPK_Options == -1) UtilDL.GetDBOperationFailed();
                else cmd_Option.Parameters.AddWithValue("p_QuestionOptionsID", (object)genPK_Options);

                if (genPK == -1) UtilDL.GetDBOperationFailed();
                else cmd_Option.Parameters.AddWithValue("p_QuestionID", (object)genPK);

                cmd_Option.Parameters.AddWithValue("p_QuestionOptions", row_Options.QuestionOptions);
                cmd_Option.Parameters.AddWithValue("p_OptionOrder", row_Options.OptionOrder);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Option, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Option.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_QUIZQUESTION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion 

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _updateQuizQuestion(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            QuizDS quizDS = new QuizDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            quizDS.Merge(inputDS.Tables[quizDS.Quiz_Question.TableName], false, MissingSchemaAction.Error);
            quizDS.Merge(inputDS.Tables[quizDS.Quiz_QuestionOptions.TableName], false, MissingSchemaAction.Error);
            quizDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_QUIZ_QUESTION_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Option_Del = new OleDbCommand();
            cmd_Option_Del.CommandText = "PRO_QUIZ_QUESTIONOPTIONS_DEL";
            cmd_Option_Del.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd_Option = new OleDbCommand();
            cmd_Option.CommandText = "PRO_QUIZ_QUESTION_OPTION_ADD";
            cmd_Option.CommandType = CommandType.StoredProcedure;


            if (cmd == null) return UtilDL.GetCommandNotFound();

            #region Delete Options
            foreach (QuizDS.Quiz_QuestionRow row in quizDS.Quiz_Question.Rows)
            {
                cmd_Option_Del.Parameters.AddWithValue("p_QuestionID", row.QuestionID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Option_Del, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Option_Del.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_QUIZQUESTION_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Update Quiz Question....
            foreach (QuizDS.Quiz_QuestionRow row in quizDS.Quiz_Question.Rows)
            {
                cmd.Parameters.AddWithValue("p_QuestionID", row.QuestionID);
                cmd.Parameters.AddWithValue("p_QuestionDescription", row.QuestionDescription);
                cmd.Parameters.AddWithValue("p_IsMultipleChoice", row.IsMultipleChoice);
                cmd.Parameters.AddWithValue("p_IsActive", row.IsActive);
                cmd.Parameters.AddWithValue("p_IsMandatory", row.IsMandatory);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_QUIZQUESTION_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            #region Update Question Options...
            foreach (QuizDS.Quiz_QuestionOptionsRow row_Options in quizDS.Quiz_QuestionOptions.Rows)
            {
                cmd_Option.Parameters.AddWithValue("p_QuestionOptionsID", row_Options.QuestionOptionsID);
                cmd_Option.Parameters.AddWithValue("p_QuestionID", row_Options.QuestionID);
                cmd_Option.Parameters.AddWithValue("p_QuestionOptions", row_Options.QuestionOptions);
                cmd_Option.Parameters.AddWithValue("p_OptionOrder", row_Options.OptionOrder);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Option, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Option.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_QUIZQUESTION_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion 

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getQuizQuestion(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            QuizDS quizDS = new QuizDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetQuizQuestionList");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, quizDS, quizDS.Quiz_Question.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_QUIZQUESTION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            quizDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(quizDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _deleteQuizQuestion(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            QuizDS quizDS = new QuizDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            quizDS.Merge(inputDS.Tables[quizDS.Quiz_Question.TableName], false, MissingSchemaAction.Error);
            quizDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_QUIZ_QUESTION_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null) return UtilDL.GetCommandNotFound();
            
            foreach (QuizDS.Quiz_QuestionRow row in quizDS.Quiz_Question.Rows)
            {
                cmd.Parameters.AddWithValue("p_QuestionID", row.QuestionID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_QUIZQUESTION_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.QuestionDescription);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.QuestionDescription);
            }

            DataSet returnDS = new DataSet();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _callForExitInterview(DataSet inputDS)
        {
            QuizDS quizDS = new QuizDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            Int32 checklistID = 0;

            quizDS.Merge(inputDS.Tables[quizDS.Quiz_QuestionOptions.TableName], false, MissingSchemaAction.Error);
            quizDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            string messageBody = "", toAddress = "";
            string returnMessage = "Email successfully sent.";
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());

            bool isNotifyByMail = false;
            string mailSubject = "", senderEmail = "";

            #region Set Message
            messageBody = String.Format("<b>Dispatcher :</b> {0}<br><br>", quizDS.Quiz_QuestionOptions[0].Dispatcher);
            messageBody += String.Format("<b>Employee for Separation :</b> {0}<br><br>", quizDS.Quiz_QuestionOptions[0].SeparateEmployee);
            messageBody += quizDS.Quiz_QuestionOptions[0].MailBody;
            //messageBody += "This is to inform you that you have been call for exit interview by " + String.Format("{0}.<br><br>", quizDS.Quiz_QuestionOptions[0].Dispatcher) + "<br>";
            //messageBody += "Please check the HR Management system for details.<br>";
            //messageBody += "<br><br><br>";
            #endregion
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SEP_EXIT_INTERVIEW_CALL";
            cmd.CommandType = CommandType.StoredProcedure;

            foreach (QuizDS.Quiz_QuestionOptionsRow row in quizDS.Quiz_QuestionOptions.Rows)
            {
                cmd.Parameters.AddWithValue("p_ChecklistID", row.ChecklistID);
                cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                cmd.Parameters.AddWithValue("p_CallerUserID", row.CallerUserID);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();
                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_EXIT_INTERVIEW_CALL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                #region Send Notification Mails
                if (!bError)
                {
                    //Email address sent
                    if (row.IsNotificationByMail)
                    {
                        isNotifyByMail = true;
                        senderEmail = row.EmployeeEmail;
                        mailSubject = row.SubjectForEmail;
                        toAddress = row.ReceiverEmail;
                    }
                    if (isNotifyByMail)
                    {
                        senderEmail = credentialEmailAddress;
                        returnMessage = "";
                        if (IsEMailSendWithCredential)
                        {
                            returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, senderEmail, toAddress, "",
                                                           mailSubject, messageBody, "Call for Exit Interview");
                        }
                        else
                        {
                            returnMessage = mail.SendEmail(host, port, senderEmail, toAddress, "", mailSubject, messageBody, "Call for Exit Interview");
                        }
                    }
                    if (returnMessage != "Email successfully sent.")
                    {
                        messageDS.ErrorMsg.AddErrorMsgRow("Email sending failed.");
                        messageDS.AcceptChanges();
                    }
                }
                #endregion
            }

            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        //private DataSet _createExitInterview(DataSet inputDS)
        //{
        //    ErrorDS errDS = new ErrorDS();
        //    QuizDS quizDS = new QuizDS();
        //    DBConnectionDS connDS = new DBConnectionDS();

        //    OleDbCommand cmd_Option = new OleDbCommand();
        //    cmd_Option.CommandText = "PRO_EXIT_INTERVIEW_ADD";
        //    cmd_Option.CommandType = CommandType.StoredProcedure;

        //    if (cmd_Option == null) return UtilDL.GetCommandNotFound();

        //    quizDS.Merge(inputDS.Tables[quizDS.Quiz_QuestionOptions.TableName], false, MissingSchemaAction.Error);
        //    quizDS.AcceptChanges();
        //    connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //    connDS.AcceptChanges();

        //    #region Add Question Options...
        //    foreach (QuizDS.Quiz_QuestionOptionsRow row_Options in quizDS.Quiz_QuestionOptions.Rows)
        //    {
        //        cmd_Option.Parameters.AddWithValue("p_Sep_InterViewID", row_Options.Sep_InterViewID);
        //        cmd_Option.Parameters.AddWithValue("p_QuestionOptionsID", row_Options.QuestionOptionsID);

        //        bool bError = false;
        //        int nRowAffected = -1;
        //        nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Option, connDS.DBConnections[0].ConnectionID, ref bError);
        //        cmd_Option.Parameters.Clear();

        //        if (bError)
        //        {
        //            ErrorDS.Error err = errDS.Errors.NewError();
        //            err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
        //            err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
        //            err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
        //            err.ErrorInfo1 = ActionID.NA_ACTION_EXIT_INTERVIEW_ADD.ToString();
        //            errDS.Errors.AddError(err);
        //            errDS.AcceptChanges();
        //            return errDS;
        //        }
        //    }
        //    #endregion

        //    errDS.Clear();
        //    errDS.AcceptChanges();
        //    return errDS;
        //}

        private DataSet _createExitInterview(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            QuizDS quizDS = new QuizDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();
            bool bError = false;

            OleDbCommand cmd_Option = new OleDbCommand();
            cmd_Option.CommandText = "PRO_EXIT_INTERVIEW_ADD";
            cmd_Option.CommandType = CommandType.StoredProcedure;

            if (cmd_Option == null) return UtilDL.GetCommandNotFound();

            quizDS.Merge(inputDS.Tables[quizDS.Quiz_QuestionOptions.TableName], false, MissingSchemaAction.Error);
            quizDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            string messageBody = "", toAddress = "";
            string returnMessage = "Email successfully sent.";
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());

            bool isNotifyByMail = false;
            string mailSubject = "", senderEmail = "";

            #region Set Message
            messageBody = String.Format("<b>Dispatcher :</b> {0} - {1}, {2}, {3}<br><br>", quizDS.Quiz_QuestionOptions[0].EmployeeCode, quizDS.Quiz_QuestionOptions[0].EmployeeName, quizDS.Quiz_QuestionOptions[0].DesignationName,quizDS.Quiz_QuestionOptions[0].DepartmentName);
            messageBody += String.Format("<b>Employee for Separation :</b> {0} - {1}, {2}, {3}<br><br>", quizDS.Quiz_QuestionOptions[0].EmployeeCode, quizDS.Quiz_QuestionOptions[0].EmployeeName, quizDS.Quiz_QuestionOptions[0].DesignationName, quizDS.Quiz_QuestionOptions[0].DepartmentName);
            messageBody += quizDS.Quiz_QuestionOptions[0].MailBody;            
            //messageBody += "Exit Interview is submitted by " + String.Format("{0} - {1}.<br><br>", quizDS.Quiz_QuestionOptions[0].EmployeeCode, quizDS.Quiz_QuestionOptions[0].EmployeeName) + "<br>";
            //messageBody += "Please check the HR Management system for details.<br>";
            messageBody += "<br><br><br>";
            #endregion
            #endregion

            #region Add Question Options...
            //if (returnMessage == "Email successfully sent.")
            //{
            foreach (QuizDS.Quiz_QuestionOptionsRow row_Options in quizDS.Quiz_QuestionOptions.Rows)
            {
                cmd_Option.Parameters.AddWithValue("p_Sep_InterViewID", row_Options.Sep_InterViewID);
                cmd_Option.Parameters.AddWithValue("p_QuestionOptionsID", row_Options.QuestionOptionsID);

                //Email address sent
                if (row_Options.IsNotificationByMail)
                {
                    isNotifyByMail = true;
                    senderEmail = row_Options.EmployeeEmail;
                    mailSubject = row_Options.SubjectForEmail;
                    toAddress = row_Options.ReceiverEmail;
                }

                //bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd_Option, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd_Option.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_EXIT_INTERVIEW_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }

            }

            #region Send Notification Mails
            if (!bError)
            {
                if (isNotifyByMail)
                {
                    senderEmail = credentialEmailAddress;
                    returnMessage = "";
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, senderEmail, toAddress, "",
                                                       mailSubject, messageBody, "Exit Interview Submission");
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, senderEmail, toAddress, "", mailSubject, messageBody, "Exit Interview Submission");
                    }
                }
                if (returnMessage != "Email successfully sent.")
                {
                    messageDS.ErrorMsg.AddErrorMsgRow("Email sending failed.");
                    messageDS.AcceptChanges();
                }
            }
            #endregion
            #endregion

            errDS.Clear();
            //errDS.AcceptChanges();
            //return errDS;

            errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _getExitInterviewInfo(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            QuizDS quizDS = new QuizDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            if (stringDS.DataStrings[0].StringValue == "ExitInterviewInfoByEmpID")
            {
                cmd = DBCommandProvider.GetDBCommand("GetExitInterviewInfoByEmpID");
                cmd.Parameters["EmployeeID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            }
            if (cmd == null) return UtilDL.GetCommandNotFound();

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, quizDS, quizDS.Quiz_Question.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EXITINTERVIEWINFO.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            quizDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(quizDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createSep_LiabilityQuestionnaire(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            QuizDS quizDS = new QuizDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd_del = new OleDbCommand();
            cmd_del.CommandText = "PRO_SEP_LIABILITY_QUESTION_DEL";
            cmd_del.CommandType = CommandType.StoredProcedure;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SEP_LIABILITY_QUESTION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd_del == null || cmd == null) return UtilDL.GetCommandNotFound();

            quizDS.Merge(inputDS.Tables[quizDS.Sep_Liability_Questionnaire.TableName], false, MissingSchemaAction.Error);
            quizDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region DELETE old setup

            if (!quizDS.Sep_Liability_Questionnaire[0].IsDepartmentIDNull()) cmd_del.Parameters.AddWithValue("p_DepartmentID", quizDS.Sep_Liability_Questionnaire[0].DepartmentID);
            else cmd_del.Parameters.AddWithValue("p_DepartmentID", -100);
            if (!quizDS.Sep_Liability_Questionnaire[0].IsSiteIDNull()) cmd_del.Parameters.AddWithValue("p_SiteID", quizDS.Sep_Liability_Questionnaire[0].SiteID);
            else cmd_del.Parameters.AddWithValue("p_SiteID", -100);

            bool bError_del = false;
            int nRowAffected_del = -1;
            nRowAffected_del = ADOController.Instance.ExecuteNonQuery(cmd_del, connDS.DBConnections[0].ConnectionID, ref bError_del);
            cmd_del.Parameters.Clear();
            if (bError_del == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_SEP_LIABILITY_QUESTIONNAIRE_ADD.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion

            #region Add Question...
            foreach (QuizDS.Sep_Liability_QuestionnaireRow row in quizDS.Sep_Liability_Questionnaire.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_Lia_QuestionnaireID", (object)genPK);
                cmd.Parameters.AddWithValue("p_DiscontinueTypeID", row.DiscontinueTypeID);

                if (!row.IsDepartmentIDNull()) cmd.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                else cmd.Parameters.AddWithValue("p_DepartmentID", DBNull.Value);

                //cmd.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                cmd.Parameters.AddWithValue("p_Description", row.Question_Description);
                cmd.Parameters.AddWithValue("p_InputType", row.InputType);
                cmd.Parameters.AddWithValue("p_DepartmentHead", row.IsDepartmentalHead);

                if (!row.IsCodeNull()) cmd.Parameters.AddWithValue("p_Code", row.Code);
                else cmd.Parameters.AddWithValue("p_Code", DBNull.Value);

                if(!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);

                if (!row.IsLiability_Nature_IDNull()) cmd.Parameters.AddWithValue("p_Liability_Nature", row.Liability_Nature_ID);
                else cmd.Parameters.AddWithValue("p_Liability_Nature", DBNull.Value);

                if (!row.IsCollectionTypeNull()) cmd.Parameters.AddWithValue("p_CollectionType", row.CollectionType);
                else cmd.Parameters.AddWithValue("p_CollectionType", DBNull.Value);

                if (!row.IsHeadIDNull()) cmd.Parameters.AddWithValue("p_HeadID", row.HeadID);
                else cmd.Parameters.AddWithValue("p_HeadID", DBNull.Value);

                if (!row.IsSiteIDNull()) cmd.Parameters.AddWithValue("p_SiteID", row.SiteID);
                else cmd.Parameters.AddWithValue("p_SiteID", DBNull.Value);

                if (!row.IsFunctionIDNull()) cmd.Parameters.AddWithValue("p_FunctionID", row.FunctionID);
                else cmd.Parameters.AddWithValue("p_FunctionID", DBNull.Value);

                if (!row.IsCollectorEmployeeCodeNull()) cmd.Parameters.AddWithValue("p_CollectorEmployeeCode", row.CollectorEmployeeCode);
                else cmd.Parameters.AddWithValue("p_CollectorEmployeeCode", DBNull.Value);
               
                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_SEP_LIABILITY_QUESTIONNAIRE_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _getSep_LiabilityQuestionnaire(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();
            QuizDS quizDS = new QuizDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            OleDbCommand cmd = new OleDbCommand();
            if (stringDS.DataStrings[0].StringValue == "Departmentwise")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSep_LiabilityQuestionnaire");
                cmd.Parameters["DepartmentID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["DiscontinueTypeID"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
            }
            else if (stringDS.DataStrings[0].StringValue == "Sitewise")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSep_LiabilityQuestionnaire_Sitewise");
                cmd.Parameters["SiteID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["DiscontinueTypeID"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
            }
            else if (stringDS.DataStrings[0].StringValue == "Liability_Answers")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSep_LiabilityQuestionnaireAnswers");
                cmd.Parameters["ChecklistID1"].Value = cmd.Parameters["ChecklistID2"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
                cmd.Parameters["DepartmentID1"].Value = cmd.Parameters["DepartmentID2"].Value = Convert.ToInt32(stringDS.DataStrings[2].StringValue);
                cmd.Parameters["SiteID1"].Value = cmd.Parameters["SiteID2"].Value = Convert.ToInt32(stringDS.DataStrings[3].StringValue);
                cmd.Parameters["LoginID1"].Value = cmd.Parameters["LoginID2"].Value = stringDS.DataStrings[4].StringValue;
                cmd.Parameters["IsSubmitted1"].Value = cmd.Parameters["IsSubmitted2"].Value = Convert.ToInt32(stringDS.DataStrings[5].StringValue);
            }
            else if (stringDS.DataStrings[0].StringValue == "Collection_Summary")
            {
                cmd = DBCommandProvider.GetDBCommand("GetSep_Liability_CollectionSummary");
                cmd.Parameters["ChecklistID"].Value = Convert.ToInt32(stringDS.DataStrings[1].StringValue);
            }
            else cmd = DBCommandProvider.GetDBCommand("GetSep_LiabilityQuestionnaire_All");
            
            if (cmd == null) return UtilDL.GetCommandNotFound();
           
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, quizDS, quizDS.Sep_Liability_Questionnaire.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_SEP_LIABILITY_QUESTION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            quizDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(quizDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _createSep_LiabilityQuestionnaireAnswers(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            QuizDS quizDS = new QuizDS();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            bool bError = false;

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SEP_LIABILITY_ANSWER_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            quizDS.Merge(inputDS.Tables[quizDS.Sep_Liability_Questionnaire.TableName], false, MissingSchemaAction.Error);
            quizDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            string messageBody = "", toAddress = "";
            string returnMessage = "Email successfully sent.";
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());

            bool isNotifyByMail = false;
            string mailSubject = "", senderEmail = "";

            #region Set Message
            messageBody = String.Format("<b>Dispatcher :</b> {0} <br><br>", quizDS.Sep_Liability_Questionnaire[0].Dispatcher);
            messageBody += String.Format("<b>Employee for Separation :</b> {0} - {1}, {2}, {3}<br><br>", quizDS.Sep_Liability_Questionnaire[0].EmployeeCode, quizDS.Sep_Liability_Questionnaire[0].EmployeeName, quizDS.Sep_Liability_Questionnaire[0].DesignationName, quizDS.Sep_Liability_Questionnaire[0].DepartmentName) + ".<br>";
            messageBody += quizDS.Sep_Liability_Questionnaire[0].MailBody;
            //messageBody += "This is to inform you that the liability of  " + String.Format("{0} - {1}, {2}, {3} has been collected.<br><br>", quizDS.Sep_Liability_Questionnaire[0].EmployeeCode, quizDS.Sep_Liability_Questionnaire[0].EmployeeName, quizDS.Sep_Liability_Questionnaire[0].DesignationName, quizDS.Sep_Liability_Questionnaire[0].DepartmentName) + ".<br>";
            //messageBody += "Please check the HR Management system for details.<br>";
            //messageBody += "<br><br><br>";
            #endregion
            #endregion

            foreach (QuizDS.Sep_Liability_QuestionnaireRow row in quizDS.Sep_Liability_Questionnaire.Rows)
            {

                cmd.Parameters.AddWithValue("p_CollectionID", row.Lia_CollectionID);
                cmd.Parameters.AddWithValue("p_Given_Text", row.Given_Text);

                if (!row.IsGeneralNull()) cmd.Parameters.AddWithValue("p_General", row.General);
                else cmd.Parameters.AddWithValue("p_General", DBNull.Value);
                if (!row.IsDebitNull()) cmd.Parameters.AddWithValue("p_Debit", row.Debit);
                else cmd.Parameters.AddWithValue("p_Debit", DBNull.Value);
                if (!row.IsCreditNull()) cmd.Parameters.AddWithValue("p_Credit", row.Credit);
                else cmd.Parameters.AddWithValue("p_Credit", DBNull.Value);

                if (!row.IsRemarksNull()) cmd.Parameters.AddWithValue("p_Remarks", row.Remarks);
                else cmd.Parameters.AddWithValue("p_Remarks", DBNull.Value);
                if (!row.IsChanged_TextNull()) cmd.Parameters.AddWithValue("p_Changed_Text", row.Changed_Text);
                else cmd.Parameters.AddWithValue("p_Changed_Text", DBNull.Value);

                //Email address sent
                if (row.IsNotificationByMail)
                {
                    isNotifyByMail = true;
                    senderEmail = row.SenderEmail;
                    mailSubject = row.SubjectForEmail;
                    toAddress = row.RecevierEmail;
                }

                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                cmd.Parameters.Clear();

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.NA_ACTION_SEP_LIABILITY_QUESTION_ANSWER.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }                
            }

            #region Send Notification Mails
            if (!bError)
            {

                if (isNotifyByMail)
                {
                    senderEmail = credentialEmailAddress;
                    returnMessage = "";
                    if (IsEMailSendWithCredential)
                    {
                        returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, senderEmail, toAddress, "",
                                                       mailSubject, messageBody, "Liability Collection");
                    }
                    else
                    {
                        returnMessage = mail.SendEmail(host, port, senderEmail, toAddress, "", mailSubject, messageBody, "Liability Collection");
                    }
                }
                if (returnMessage != "Email successfully sent.")
                {
                    messageDS.ErrorMsg.AddErrorMsgRow("Email sending failed.");
                    messageDS.AcceptChanges();
                }
            }
            #endregion
            errDS.Clear();
            returnDS.Merge(messageDS);
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _createSep_LiabilityCollection(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DataStringDS quizDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_SEP_LIA_COLLECTION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("p_ChecklistID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            cmd.Parameters.AddWithValue("p_LoginID", stringDS.DataStrings[1].StringValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_ADD_SEP_LIABILITY_COLLECTION.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }
        private DataSet _sendLiabilityCollectionRequest(DataSet inputDS)
        {
            QuizDS collectionDS = new QuizDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            MessageDS messageDS = new MessageDS();
            DataStringDS stringDS = new DataStringDS();
            DBConnectionDS connDS = new DBConnectionDS();
            Int32 checklistID = 0;

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            collectionDS.Merge(inputDS.Tables[collectionDS.Sep_Liability_Questionnaire.TableName], false, MissingSchemaAction.Error);
            collectionDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            //string messageBody = "", toAddress = "";
            string returnMessage = "Email successfully sent.";
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());
            #endregion

            OleDbCommand cmdHist = new OleDbCommand();
            cmdHist.CommandText = "PRO_SEP_LIA_COLLECTION_SEND";
            cmdHist.CommandType = CommandType.StoredProcedure;

            if (cmdHist == null) return UtilDL.GetCommandNotFound();

            foreach (QuizDS.Sep_Liability_QuestionnaireRow row in collectionDS.Sep_Liability_Questionnaire.Rows)
            {
                cmdHist.Parameters.AddWithValue("p_ChecklistID", row.ChecklistID);
                cmdHist.Parameters.AddWithValue("p_UserID", row.Collector_UserID);

                if (!row.IsSiteIDNull()) cmdHist.Parameters.AddWithValue("p_SiteID", row.SiteID);
                else cmdHist.Parameters.AddWithValue("p_SiteID", DBNull.Value);

                if (!row.IsDepartmentIDNull()) cmdHist.Parameters.AddWithValue("p_DepartmentID", row.DepartmentID);
                else cmdHist.Parameters.AddWithValue("p_DepartmentID", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdHist, connDS.DBConnections[0].ConnectionID, ref bError);
                cmdHist.Parameters.Clear();
                if (bError)
                {
                    //ErrorDS.Error err = errDS.Errors.NewError();
                    //err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    //err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    //err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    //err.ErrorInfo1 = ActionID.NA_ACTION_SEP_LIABILITY_COLLECTION_REQ_SEND.ToString();
                    //errDS.Errors.AddError(err);
                    //errDS.AcceptChanges();

                    messageDS.ErrorMsg.AddErrorMsgRow(String.Format("{0} => History SAVE ERROR", row.Coll_DepartmentName));
                    continue;
                }

                messageDS.SuccessMsg.AddSuccessMsgRow(String.Format("{0} -> {1}", row.Coll_DepartmentName, row.Collector_EmployeeName));

                #region Send Notification Mails
                if (!bError)
                {
                    string messageBody = "", toAddress = "";
                    bool isNotifyByMail = false;
                    string mailSubject = "", senderEmail = "";
                    #region Set Message
                    messageBody = String.Format("<b>Dispatcher :</b> {0}<br><br>", row.Dispatcher);
                    messageBody += String.Format("<b>Employee for Separation :</b> {0}<br><br>", row.SeparateEmployee);
                    messageBody += row.MailBody;
                    messageBody += "<br><br><br>";
                    #endregion

                    if (row.IsNotificationByMail)
                    {
                        isNotifyByMail = true;
                        senderEmail = row.SenderEmail;
                        mailSubject = row.SubjectForEmail;
                        //toAddress += row.Next_AppEmail + ",";
                        toAddress = row.RecevierEmail;
                    }

                    if (isNotifyByMail)
                    {
                        senderEmail = credentialEmailAddress;
                        returnMessage = "";
                        if (IsEMailSendWithCredential)
                        {
                            returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, senderEmail, toAddress, "", mailSubject, messageBody, "Liability Collection Process");
                        }
                        else
                        {
                            returnMessage = mail.SendEmail(host, port, senderEmail, toAddress, "", mailSubject, messageBody, "Liability Collection Process");
                        }
                    }
                    if (returnMessage != "Email successfully sent.")
                    {
                        messageDS.ErrorMsg.AddErrorMsgRow("Email sending failed.");
                        messageDS.AcceptChanges();
                    }
                }
                #endregion
            }

            errDS.AcceptChanges();
            returnDS.Merge(messageDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _AcknowledgeEmployee(DataSet inputDS)
        {
            FinalSettlementDS fsDS = new FinalSettlementDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();
            DBConnectionDS connDS = new DBConnectionDS();
            MessageDS messageDS = new MessageDS();
            Int32 checklistID = 0;

            fsDS.Merge(inputDS.Tables[fsDS.Separation.TableName], false, MissingSchemaAction.Error);
            fsDS.AcceptChanges();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Get Email Information
            Mail.Mail mail = new Mail.Mail();
            string messageBody = "", toAddress = "";
            string returnMessage = "Email successfully sent.";
            string host = UtilDL.GetHost();
            Int32 port = UtilDL.GetPort();
            bool IsEMailSendWithCredential = Convert.ToBoolean(UtilDL.GetEmailType());
            string credentialEmailAddress = UtilDL.GetCredentialEmailAddress();
            string credentialEmailPwd = UtilDL.base64Decode(UtilDL.GetCredentialEmailPwd());
            bool enableSsl = Convert.ToBoolean(UtilDL.GetEnableSslValue());

            bool isNotifyByMail = false;
            string mailSubject = "", senderEmail = "";

            #region Set Message
            messageBody = String.Format("<b>Dispatcher :</b> {0}<br><br>", fsDS.Separation[0].DispatcherEmployee);
            messageBody += String.Format("<b>Employee for Separation :</b> {0}<br><br>", fsDS.Separation[0].SeparateEmployee);
            messageBody += fsDS.Separation[0].EmailBody;
            //messageBody += "This is to inform you that you have been call for exit interview by " + String.Format("{0}.<br><br>", quizDS.Quiz_QuestionOptions[0].Dispatcher) + "<br>";
            //messageBody += "Please check the HR Management system for details.<br>";
            //messageBody += "<br><br><br>";
            #endregion
            #endregion

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_CHECKLIST_ACKN_HIST_ADD";
            cmd.CommandType = CommandType.StoredProcedure;
            
            #region Send Notification Mails
            foreach (FinalSettlementDS.SeparationRow row in fsDS.Separation.Rows)
            {
                if (row.IsNotifyByMail)
                {
                    isNotifyByMail = true;
                    senderEmail = row.Email;
                    mailSubject = row.SubjectForEmail;
                    //toAddress += row.Next_AppEmail + ",";
                    toAddress = row.RecevierEmail;
                }
            }

            if (isNotifyByMail)
            {
                senderEmail = credentialEmailAddress;
                returnMessage = "";
                if (IsEMailSendWithCredential)
                {
                    returnMessage = mail.SendEmail(host, port, enableSsl, credentialEmailAddress, credentialEmailPwd, senderEmail, toAddress, "",
                                                   mailSubject, messageBody, "Acknowledge Info");
                }
                else
                {
                    returnMessage = mail.SendEmail(host, port, senderEmail, toAddress, "", mailSubject, messageBody, "Acknowledge Info");
                }
            }
            #endregion
            if (returnMessage == "Email successfully sent.")
            {
                foreach (FinalSettlementDS.SeparationRow row in fsDS.Separation.Rows)
                {
                    cmd.Parameters.AddWithValue("p_CheckListID", row.ChecklistId);
                    cmd.Parameters.AddWithValue("p_SenderUserId", row.SenderUserId);
                    cmd.Parameters.AddWithValue("p_ReceiverUserId", row.ReceiverUserId);
                    cmd.Parameters.AddWithValue("p_ActivityId", row.ActivityID);

                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                    cmd.Parameters.Clear();
                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.NA_ACTION_ACKNOWLEDGE_EMPLOYEE.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }
            else
            {
                messageDS.ErrorMsg.AddErrorMsgRow("Email sending failed.");
                messageDS.AcceptChanges();
            }
            errDS.AcceptChanges();
            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
    }
}
