﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
using System.Globalization;
using System.Configuration;


namespace Sylvia.DAL.Payroll
{
    public class ProvidentFundExtendDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {



                case ActionID.NA_ACTION_GET_PF_ACCOUNT_HEADS:
                    return this._GetAccountHead(inputDS);
                case ActionID.ACTION_PF_ACCOUNT_HEAD_CREATION:
                    return this._PfAccountHeadCreation(inputDS);
                case ActionID.ACTION_PF_ACCOUNT_HEAD_DELETE:
                    return this._DeletePFAccountHead(inputDS);
                case ActionID.ACTION_PF_GET_ACCOUT_HEAD_BYID:
                    return this._GetAccountHeadByID(inputDS);


                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        #region Action

        private DataSet _GetAccountHead(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            PFAccountHeadDS pfDS = new PFAccountHeadDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetProvidentFundAccountHeads");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_ACCOUNT_HEAD.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PF_ACCOUNT_HEADS.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _GetAccountHeadByID(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            PFAccountHeadDS pfDS = new PFAccountHeadDS();
            DataSet returnDS = new DataSet();
            DataIntegerDS _DataIntegerDS = new DataIntegerDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            _DataIntegerDS.Merge(inputDS.Tables[_DataIntegerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            _DataIntegerDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetProvidentFundAccountHeadByID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            cmd.Parameters["p_headid"].Value = (object)Convert.ToInt32(_DataIntegerDS.DataIntegers[0].IntegerValue);

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, pfDS, pfDS.PF_ACCOUNT_HEAD.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_GET_PF_INVESTMENT_DETAILS_BY_INVID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            pfDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();
            returnDS.Merge(pfDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _PfAccountHeadCreation(DataSet inputDS)
        {
            int nRowAffected = -1, nRowAffected2 = -1;
            bool bError = false, bError2 = false;

            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            OleDbCommand cmd2 = new OleDbCommand();
            DBConnectionDS connDS = new DBConnectionDS();
            DataSet returnDS = new DataSet();
            PFAccountHeadDS wppwfDS = new PFAccountHeadDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            wppwfDS.Merge(inputDS.Tables[wppwfDS.PF_ACCOUNT_HEAD.TableName], false, MissingSchemaAction.Error);
            wppwfDS.AcceptChanges();

            /////// Using Procedure
            cmd.CommandText = "PRO_PF_FUND_AC_HEAD_CREATION";
            cmd.CommandType = CommandType.StoredProcedure;

            cmd2.CommandText = "PRO_PF_AC_HEAD_UPDATE";
            cmd2.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            if (cmd2 == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            foreach (PFAccountHeadDS.PF_ACCOUNT_HEADRow row in wppwfDS.PF_ACCOUNT_HEAD.Rows)
            {
                if (row.IsHEADIDNull() == true)
                {

                    ///// Procedure Parameters. Should Clear Parameters when use loop.
                    cmd.Parameters.Clear();

                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }

                    cmd.Parameters.AddWithValue("p_HEADID", genPK);
                    cmd.Parameters.AddWithValue("p_HEADCODE", row.HEADCODE);
                    cmd.Parameters.AddWithValue("p_DESCRIPTION", row.DESCRIPTION);
                    cmd.Parameters.AddWithValue("p_HEAD_TYPE", 0);

                    cmd.Parameters.AddWithValue("p_ACCOUNTTYPE", row.ACCOUNTTYPE);
                    cmd.Parameters.AddWithValue("p_ACCOUNTNO", row.ACCOUNTNO);

                    cmd.Parameters.AddWithValue("p_OPENINGBALANCE", 0);
                    cmd.Parameters.AddWithValue("p_HEADOFACCOUNTTYPE", row.HEADOFACCOUNTTYPE);

                    bError = false;
                    nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_PF_ACCOUNT_HEAD_CREATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }


                }
                else
                {
                    ///// Procedure Parameters. Should Clear Parameters when use loop.
                    cmd2.Parameters.Clear();

                    cmd2.Parameters.AddWithValue("p_HEADID", row.HEADID);

                    cmd2.Parameters.AddWithValue("p_HEADCODE", row.HEADCODE);
                    cmd2.Parameters.AddWithValue("p_DESCRIPTION", row.DESCRIPTION);
                    if (row.IsHEAD_TYPENull() == false)
                    {
                        cmd2.Parameters.AddWithValue("p_HEAD_TYPE", row.HEAD_TYPE);
                    }
                    else
                    {
                        cmd2.Parameters.AddWithValue("p_HEAD_TYPE", 0);
                    }

                    if (row.IsACCOUNTTYPENull() == false)
                    {
                        cmd2.Parameters.AddWithValue("p_ACCOUNTTYPE", row.ACCOUNTTYPE);
                    }
                    else
                    {
                        cmd2.Parameters.AddWithValue("p_ACCOUNTTYPE", DBNull.Value);
                    }


                    if (row.IsACCOUNTNONull() == false)
                    {
                        cmd2.Parameters.AddWithValue("p_ACCOUNTNO", row.ACCOUNTNO);
                    }
                    else
                    {
                        cmd2.Parameters.AddWithValue("p_ACCOUNTNO", DBNull.Value);
                    }

                    if (row.IsOPENINGBALANCENull() == false)
                    {
                        cmd2.Parameters.AddWithValue("p_OPENINGBALANCE", row.OPENINGBALANCE); ;
                    }
                    else
                    {
                        cmd2.Parameters.AddWithValue("p_OPENINGBALANCE", DBNull.Value);
                    }

                    if (row.IsHEADOFACCOUNTTYPENull() == false)
                    {
                        cmd2.Parameters.AddWithValue("p_HEADOFACCOUNTTYPE", row.HEADOFACCOUNTTYPE);
                    }
                    else
                    {
                        cmd2.Parameters.AddWithValue("p_HEADOFACCOUNTTYPE", DBNull.Value);
                    }




                    bError = false;
                    nRowAffected2 = -1;
                    nRowAffected2 = ADOController.Instance.ExecuteNonQuery(cmd2, connDS.DBConnections[0].ConnectionID, ref bError2);

                    if (bError2)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_FUND_HEAD_CREATION.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
            }

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _DeletePFAccountHead(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();
            OleDbCommand cmd = new OleDbCommand();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            /////// Using Procedure 
            cmd.CommandText = "PRO_PF_ACCOUNT_HEAD_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            ///// Procedure Parameters. Should Clear Parameters when use loop.
            //cmd.Parameters.Clear();

            cmd.Parameters.AddWithValue("p_FUNDHEADID", Convert.ToInt32(stringDS.DataStrings[0].StringValue));
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        #endregion

    }
}
