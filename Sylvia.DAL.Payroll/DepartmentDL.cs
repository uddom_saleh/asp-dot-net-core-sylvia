/* Compamy: Milllennium Information Solution Limited
 * Author: Zakaria Al Mahmud
 * Comment Date: March 22, 2006
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;
namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for DepartmentDL.
    /// </summary>
    public class DepartmentDL
    {
        public DepartmentDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_DEPARTMENT_ADD:
                    return _createDepartment(inputDS);
                case ActionID.ACTION_DEPARTMENT_UPD:
                    return this._updateDepartment(inputDS);
                case ActionID.NA_ACTION_GET_DEPARTMENT_LIST:
                    return _getDepartmentList(inputDS);
                case ActionID.NA_ACTION_GET_DEPARTMENT:
                    return _getDepartment(inputDS);
                case ActionID.ACTION_DEPARTMENT_DEL:
                    return this._deleteDepartment(inputDS);
                case ActionID.ACTION_DOES_DEPARTMENT_EXIST:
                    return this._doesDepartmentCodeExist(inputDS);

                case ActionID.ACTION_COMPANY_DIVISION_ADD:
                    return this._createCompanyDivision(inputDS);
                case ActionID.NA_ACTION_GET_COMPANY_DIVISION_LIST:
                    return this._getCompanyDivisionList(inputDS);
                case ActionID.ACTION_COMPANY_DIVISION_UPD:
                    return this._updateCompanyDivision(inputDS);
                case ActionID.ACTION_COMPANY_DIVISION_DEL:
                    return this._deleteCompanyDivision(inputDS);
                case ActionID.NA_ACTION_GET_DEPARTMENTLISTFORDELEGATEUSER:
                    return this._getDepartmentListForDelegateUser(inputDS);
                case ActionID.NA_ACTION_GET_DEPARTMENT_LIST_BY_SITEID:
                    return this._getDepartmentListBySiteId(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _doesDepartmentExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = null;
            cmd = DBCommandProvider.GetDBCommand("DoesDepartmentExist");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (stringDS.DataStrings[0].IsStringValueNull() == false)
            {
                cmd.Parameters["Code"].Value = (object)stringDS.DataStrings[0].StringValue;
            }

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _createDepartment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DepartmentDS deptDS = new DepartmentDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("CreateDepartment");
            if (cmd == null)
            {

                return UtilDL.GetCommandNotFound();
            }

            deptDS.Merge(inputDS.Tables[deptDS.Departments.TableName], false, MissingSchemaAction.Error);
            deptDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            foreach (DepartmentDS.Department dep in deptDS.Departments)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }


                cmd.Parameters["DepartmentId"].Value = genPK;

                if (dep.IsDepartmentCodeNull() == false)
                {
                    cmd.Parameters["DepartmentCode"].Value = dep.DepartmentCode;
                }
                else
                {
                    cmd.Parameters["DepartmentCode"].Value = null;
                }

                if (dep.IsDepartmentNameNull() == false)
                {
                    cmd.Parameters["DepartmentName"].Value = dep.DepartmentName;
                }
                else
                {
                    cmd.Parameters["DepartmentName"].Value = null;
                }

                if (dep.IsDepartmentIsActiveNull() == false)
                {
                    cmd.Parameters["IsActive"].Value = dep.DepartmentIsActive;
                }
                else
                {
                    cmd.Parameters["IsActive"].Value = null;
                }

                if (!dep.IsKPIGroupID_DNull())
                {
                    cmd.Parameters["KPIGroupID_D"].Value = dep.KPIGroupID_D;
                }
                else cmd.Parameters["KPIGroupID_D"].Value = DBNull.Value;

                #region Update :: WALI :: 17-Jul-2014
                if (!dep.IsDept_TelephoneNull()) cmd.Parameters["Telephone"].Value = dep.Dept_Telephone;
                else cmd.Parameters["Telephone"].Value = DBNull.Value;
                if (!dep.IsDept_MobileNull()) cmd.Parameters["Mobile"].Value = dep.Dept_Mobile;
                else cmd.Parameters["Mobile"].Value = DBNull.Value;
                if (!dep.IsDept_FaxNull()) cmd.Parameters["Fax"].Value = dep.Dept_Fax;
                else cmd.Parameters["Fax"].Value = DBNull.Value;
                if (!dep.IsDept_EmailNull()) cmd.Parameters["Email"].Value = dep.Dept_Email;
                else cmd.Parameters["Email"].Value = DBNull.Value;
                #endregion

                #region Update :: WALI :: 15-Feb-2015
                if (!dep.IsCompanyDivisionIDNull()) cmd.Parameters["CompanyDivisionID"].Value = dep.CompanyDivisionID;
                else cmd.Parameters["CompanyDivisionID"].Value = DBNull.Value;
                #endregion

                if (dep.IsDepartmentName_NativeLangNull() == false)    // Rony :: 09 June 2016 :: For Native Language
                {
                    cmd.Parameters["Name_NativeLang"].Value = (object)dep.DepartmentName_NativeLang;
                }
                else
                {
                    cmd.Parameters["Name_NativeLang"].Value = null;
                }
                cmd.Parameters["DefaultSelection"].Value = (object)dep.DefaultSelection;

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && dep.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteDepartment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteDepartment");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["DepartmentCode"].Value = (object)stringDS.DataStrings[0].StringValue;
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
            int nRowAffectedRec = -1;
            if (connDS.DBConnections.Rows.Count == 2)
            {
                nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
            }
            cmd.Parameters.Clear();
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_DEL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _getDepartment(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            string scode = stringDS.DataStrings[0].StringValue;
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDepartment");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["DepartmentCode"].Value = scode;//(object) stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            DepartmentPO departmentPO = new DepartmentPO();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter,
              departmentPO,
              departmentPO.Departments.TableName,
              connDS.DBConnections[0].ConnectionID,
              ref bError);
            if (bError == true)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }

            departmentPO.AcceptChanges();
            DepartmentDS departmentDS = new DepartmentDS();
            if (departmentPO.Departments.Count > 0)
            {
                foreach (DepartmentPO.Department poDept in departmentPO.Departments)
                {
                    DepartmentDS.Department dept = departmentDS.Departments.NewDepartment();

                    if (poDept.IsDepartmentCodeNull() == false)
                    {
                        dept.DepartmentCode = poDept.DepartmentCode;
                    }
                    if (poDept.IsDepartmentNameNull() == false)
                    {
                        dept.DepartmentName = poDept.DepartmentName;
                    }
                    if (poDept.IsIsActiveNull() == false)
                    {

                        dept.DepartmentIsActive = poDept.IsActive;
                    }
                    departmentDS.Departments.AddDepartment(dept);
                    departmentDS.AcceptChanges();
                }


            }

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(departmentDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _getDepartmentList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDepartmentList_New");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(28 Sep 11)...
            #region Old...
            //DepartmentPO deptPO = new DepartmentPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, deptPO, deptPO.Departments.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENT_LIST.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //deptPO.AcceptChanges();

            //DepartmentDS deptDS = new DepartmentDS();
            //if (deptPO.Departments.Count > 0)
            //{
            //    // custPO.WriteXml(@"c:\custPO.txt");

            //    foreach (DepartmentPO.Department poDept in deptPO.Departments)
            //    {
            //        DepartmentDS.Department dept = deptDS.Departments.NewDepartment();
            //        if (poDept.IsDepartmentCodeNull() == false)
            //        {
            //            dept.DepartmentCode = poDept.DepartmentCode;
            //        }
            //        if (poDept.IsDepartmentNameNull() == false)
            //        {
            //            dept.DepartmentName = poDept.DepartmentName;
            //        }
            //        if (poDept.IsIsActiveNull() == false)
            //        {

            //            dept.DepartmentIsActive = poDept.IsActive;
            //        }
            //        deptDS.Departments.AddDepartment(dept);
            //        deptDS.AcceptChanges();
            //    }
            //}
            #endregion

            DepartmentDS deptDS = new DepartmentDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, deptDS, deptDS.Departments.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENT_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            deptDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(deptDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _updateDepartment(DataSet inputDS)
        {
            #region OLD :: WALI :: 16-Feb-2015
            //ErrorDS errDS = new ErrorDS();

            //DBConnectionDS connDS = new DBConnectionDS();

            //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            //connDS.AcceptChanges();

            //DepartmentDS deptDS = new DepartmentDS();
            //deptDS.Merge(inputDS.Tables[deptDS.Departments.TableName], false, MissingSchemaAction.Error);
            //deptDS.AcceptChanges();


            //OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateDepartment");
            //if (cmd == null)
            //{
            //    return UtilDL.GetCommandNotFound();

            //}

            //foreach (DepartmentDS.Department dep in deptDS.Departments)
            //{

            //    if (dep.IsDepartmentCodeNull() == false)
            //    {
            //        cmd.Parameters["DepartmentCode"].Value = dep.DepartmentCode;
            //    }
            //    else
            //    {
            //        cmd.Parameters["DepartmentCode"].Value = null;
            //    }

            //    if (dep.IsDepartmentNameNull() == false)
            //    {
            //        cmd.Parameters["DepartmentName"].Value = dep.DepartmentName;
            //    }
            //    else
            //    {
            //        cmd.Parameters["DepartmentName"].Value = null;
            //    }

            //    if (dep.IsDepartmentIsActiveNull() == false)
            //    {
            //        cmd.Parameters["IsActive"].Value = dep.DepartmentIsActive;
            //    }
            //    else
            //    {
            //        cmd.Parameters["IsActive"].Value = null;
            //    }

            //    if (!dep.IsKPIGroupID_DNull())
            //    {
            //        cmd.Parameters["KPIGroupID_D"].Value = dep.KPIGroupID_D;
            //    }
            //    else cmd.Parameters["KPIGroupID_D"].Value = DBNull.Value;

            //    #region Update :: WALI :: 17-Jul-2014
            //    if (!dep.IsDept_TelephoneNull()) cmd.Parameters["Telephone"].Value = dep.Dept_Telephone;
            //    else cmd.Parameters["Telephone"].Value = DBNull.Value;
            //    if (!dep.IsDept_MobileNull()) cmd.Parameters["Mobile"].Value = dep.Dept_Mobile;
            //    else cmd.Parameters["Mobile"].Value = DBNull.Value;
            //    if (!dep.IsDept_FaxNull()) cmd.Parameters["Fax"].Value = dep.Dept_Fax;
            //    else cmd.Parameters["Fax"].Value = DBNull.Value;
            //    if (!dep.IsDept_EmailNull()) cmd.Parameters["Email"].Value = dep.Dept_Email;
            //    else cmd.Parameters["Email"].Value = DBNull.Value;
            //    #endregion
            //    #region Update :: WALI :: 15-Feb-2015
            //    if (!dep.IsCompanyDivisionIDNull()) cmd.Parameters["CompanyDivisionID"].Value = dep.CompanyDivisionID;
            //    else cmd.Parameters["CompanyDivisionID"].Value = DBNull.Value;
            //    #endregion

            //    bool bError = false;
            //    int nRowAffected = -1;
            //    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            //    if (bError == true)
            //    {
            //        ErrorDS.Error err = errDS.Errors.NewError();
            //        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //        err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_ADD.ToString();
            //        errDS.Errors.AddError(err);
            //        errDS.AcceptChanges();
            //        return errDS;
            //    }
            //}

            //return errDS;  // return empty ErrorDS
            #endregion

            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DepartmentDS deptDS = new DepartmentDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            deptDS.Merge(inputDS.Tables[deptDS.Departments.TableName], false, MissingSchemaAction.Error);
            deptDS.AcceptChanges();



            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_DEPT";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (DepartmentDS.Department dep in deptDS.Departments)
            {
                if (dep.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion



            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_DEPARTMENT_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (DepartmentDS.Department dep in deptDS.Departments)
            {
                if (!dep.IsDepartmentNameNull()) cmd.Parameters.AddWithValue("p_DepartmentName", dep.DepartmentName);
                else cmd.Parameters.AddWithValue("p_DepartmentName", DBNull.Value);
                if (!dep.IsDepartmentIsActiveNull()) cmd.Parameters.AddWithValue("p_IsActive", dep.DepartmentIsActive);
                else cmd.Parameters.AddWithValue("p_IsActive", DBNull.Value);
                if (!dep.IsKPIGroupID_DNull()) cmd.Parameters.AddWithValue("p_KPIGroupID_D", dep.KPIGroupID_D);
                else cmd.Parameters.AddWithValue("p_KPIGroupID_D", DBNull.Value);
                if (!dep.IsDept_TelephoneNull()) cmd.Parameters.AddWithValue("p_Dept_Telephone", dep.Dept_Telephone);
                else cmd.Parameters.AddWithValue("p_Dept_Telephone", DBNull.Value);
                if (!dep.IsDept_MobileNull()) cmd.Parameters.AddWithValue("p_Dept_Mobile", dep.Dept_Mobile);
                else cmd.Parameters.AddWithValue("p_Dept_Mobile", DBNull.Value);
                if (!dep.IsDept_FaxNull()) cmd.Parameters.AddWithValue("p_Dept_Fax", dep.Dept_Fax);
                else cmd.Parameters.AddWithValue("p_Dept_Fax", DBNull.Value);
                if (!dep.IsDept_EmailNull()) cmd.Parameters.AddWithValue("p_Dept_Email", dep.Dept_Email);
                else cmd.Parameters.AddWithValue("p_Dept_Email", DBNull.Value);
                if (!dep.IsCompanyDivisionIDNull()) cmd.Parameters.AddWithValue("p_CompanyDivisionID", dep.CompanyDivisionID);
                else cmd.Parameters.AddWithValue("p_CompanyDivisionID", DBNull.Value);
                if (!dep.IsCompanyDivisionID_OldNull()) cmd.Parameters.AddWithValue("p_CompanyDivisionID_Old", dep.CompanyDivisionID_Old);
                else cmd.Parameters.AddWithValue("p_CompanyDivisionID_Old", DBNull.Value);

                // Rony :: 09 June 2016 :: For Native Language
                if (!dep.IsDepartmentName_NativeLangNull()) cmd.Parameters.AddWithValue("p_Name_NativeLang", dep.DepartmentName_NativeLang);
                else cmd.Parameters.AddWithValue("p_Name_NativeLang", DBNull.Value);

                if (!dep.IsDepartmentCodeNull()) cmd.Parameters.AddWithValue("p_DepartmentCode", dep.DepartmentCode);
                else cmd.Parameters.AddWithValue("p_DepartmentCode", DBNull.Value);

                if (!dep.IsModifyEmployeeNull()) cmd.Parameters.AddWithValue("p_ModifyAllEmployee", dep.ModifyEmployee);
                else cmd.Parameters.AddWithValue("p_ModifyAllEmployee", DBNull.Value);

                cmd.Parameters.AddWithValue("p_DefaultSelection", dep.DefaultSelection);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                int nRowAffectedRec = -1;
                if (connDS.DBConnections.Rows.Count == 2 && dep.PostToErecruitment)
                {
                    nRowAffectedRec = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[1].ConnectionID, ref bError);
                }
                cmd.Parameters.Clear();
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_DEPARTMENT_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _doesDepartmentCodeExist(DataSet inputDS)
        {
            DataBoolDS boolDS = new DataBoolDS();
            ErrorDS errDS = new ErrorDS();
            DataSet returnDS = new DataSet();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DoesDepartmentExist");
            string s = stringDS.DataStrings[0].StringValue;
            cmd.Parameters["Code"].Value = (object)s;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = Convert.ToInt32(ADOController.Instance.ExecuteScalar(cmd, connDS.DBConnections[0].ConnectionID, ref bError));

            if (bError == true)
            {
                errDS.Clear();
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_DOES_ID_EXIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }

            boolDS.DataBools.AddDataBool(nRowAffected == 0 ? false : true);
            boolDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(boolDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        #region Company Division :: WALI :: 15-Feb-2015
        private DataSet _createCompanyDivision(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DepartmentDS departmentDS = new DepartmentDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            departmentDS.Merge(inputDS.Tables[departmentDS.CompanyDivision.TableName], false, MissingSchemaAction.Error);
            departmentDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COMPANYDIVISION_ADD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (DepartmentDS.CompanyDivisionRow row in departmentDS.CompanyDivision.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1) return UtilDL.GetDBOperationFailed();

                cmd.Parameters.AddWithValue("p_CompanyDivisionID", genPK);
                cmd.Parameters.AddWithValue("p_CompanyDivisionCode", row.CompanyDivisionCode);
                cmd.Parameters.AddWithValue("p_CompanyDivisionName", row.CompanyDivisionName);
                cmd.Parameters.AddWithValue("p_DefaultSelection", row.DefaultSelection);
                
                if(!row.IsSiteCodeNull()) cmd.Parameters.AddWithValue("p_SiteCode", row.SiteCode);
                else cmd.Parameters.AddWithValue("p_SiteCode", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMPANY_DIVISION_ADD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();
            }

            returnDS.Merge(errDS);
            return returnDS;
        }
        private DataSet _getCompanyDivisionList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            DepartmentDS departmentDS = new DepartmentDS();
            DataSet returnDS = new DataSet();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetCompanyDivisionList");
            if (cmd == null) return UtilDL.GetCommandNotFound();
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, departmentDS, departmentDS.CompanyDivision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_COMPANY_DIVISION_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                //return errDS;
            }

            departmentDS.AcceptChanges();
            errDS.Clear();
            errDS.AcceptChanges();

            returnDS.Merge(departmentDS.CompanyDivision);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }
        private DataSet _updateCompanyDivision(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DepartmentDS departmentDS = new DepartmentDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            departmentDS.Merge(inputDS.Tables[departmentDS.CompanyDivision.TableName], false, MissingSchemaAction.Error);
            departmentDS.AcceptChanges();


            #region Update Default Selection to Zero if New Selection found
            OleDbCommand cmdUpdateDefaultSelection = new OleDbCommand();
            cmdUpdateDefaultSelection.CommandText = "PRO_DEFAULT_SELECT_COMDIV";
            cmdUpdateDefaultSelection.CommandType = CommandType.StoredProcedure;

            foreach (DepartmentDS.CompanyDivisionRow row in departmentDS.CompanyDivision.Rows)
            {
                if (row.DefaultSelection)
                {
                    bool bError = false;
                    int nRowAffected = -1;
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmdUpdateDefaultSelection, connDS.DBConnections[0].ConnectionID, ref bError);

                    if (bError == true)
                    {
                        ErrorDS.Error err = errDS.Errors.NewError();
                        err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                        err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                        err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                        err.ErrorInfo1 = ActionID.ACTION_COMPANY_DIVISION_UPD.ToString();
                        errDS.Errors.AddError(err);
                        errDS.AcceptChanges();
                        return errDS;
                    }
                }
                break;
            }
            #endregion



            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COMPANYDIVISION_UPD";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (DepartmentDS.CompanyDivisionRow row in departmentDS.CompanyDivision.Rows)
            {
                cmd.Parameters.AddWithValue("p_CompanyDivisionCode", row.CompanyDivisionCode);
                cmd.Parameters.AddWithValue("p_CompanyDivisionName", row.CompanyDivisionName);
                cmd.Parameters.AddWithValue("p_DefaultSelection", row.DefaultSelection);

                if (!row.IsSiteCodeNull()) cmd.Parameters.AddWithValue("p_SiteCode", row.SiteCode);
                else cmd.Parameters.AddWithValue("p_SiteCode", DBNull.Value);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMPANY_DIVISION_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.CompanyDivisionCode + " - " + row.CompanyDivisionName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.CompanyDivisionCode + " - " + row.CompanyDivisionName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        private DataSet _deleteCompanyDivision(DataSet inputDS)
        {
            #region Local Variable Declaration..........
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            DepartmentDS departmentDS = new DepartmentDS();
            MessageDS messageDS = new MessageDS();
            DataSet returnDS = new DataSet();
            #endregion

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            departmentDS.Merge(inputDS.Tables[departmentDS.CompanyDivision.TableName], false, MissingSchemaAction.Error);
            departmentDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_COMPANYDIVISION_DEL";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null) return UtilDL.GetCommandNotFound();

            foreach (DepartmentDS.CompanyDivisionRow row in departmentDS.CompanyDivision.Rows)
            {
                cmd.Parameters.AddWithValue("p_CompanyDivisionCode", row.CompanyDivisionCode);

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);
                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_COMPANY_DIVISION_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                }
                cmd.Parameters.Clear();

                if (nRowAffected > 0) messageDS.SuccessMsg.AddSuccessMsgRow(row.CompanyDivisionCode + " - " + row.CompanyDivisionName);
                else messageDS.ErrorMsg.AddErrorMsgRow(row.CompanyDivisionCode + " - " + row.CompanyDivisionName);
            }

            returnDS.Merge(errDS);
            returnDS.Merge(messageDS);
            return returnDS;
        }
        #endregion

        private DataSet _getDepartmentListForDelegateUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();            

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDepartmentListForDelegateUser");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["pSiteID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            DepartmentDS deptDS = new DepartmentDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, deptDS, deptDS.Departments.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENTLISTFORDELEGATEUSER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            deptDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(deptDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getDepartmentListBySiteId(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            DataStringDS stringDS = new DataStringDS();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetDepartmentListBySiteID");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["pSiteID"].Value = Convert.ToInt32(stringDS.DataStrings[0].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            DepartmentDS deptDS = new DepartmentDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, deptDS, deptDS.Departments.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_DEPARTMENT_LIST_BY_SITEID.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            deptDS.AcceptChanges();


            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(deptDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

    }
}
