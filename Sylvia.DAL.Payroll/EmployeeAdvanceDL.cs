using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

namespace Sylvia.DAL.Payroll
{
    public class EmployeeAdvanceDL
    {
        public EmployeeAdvanceDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_MAIL_USERS_CREATE:
                    return this._createMailUsers(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_LIST_FROM_MAILUSER:
                    return this._getEmployeeListFromMailUser(inputDS);
                case ActionID.NA_ACTION_GET_RULECONFIGURATION_LIST_ALL:
                    return this._getRuleConfigListAll(inputDS);
                case ActionID.NA_ACTION_GET_MAIL_DEPARTMENT_LIST_ALL:
                    return this._getMailDepartmentListAll(inputDS);
                case ActionID.ACTION_MAIL_RULE_CONFIGURATION_CREATE:
                    return this._createMailRuleConfiguration(inputDS);
                case ActionID.ACTION_MAIL_RULE_CONFIGURATION_DEL:
                    return this._deleteMailRuleConfiguration(inputDS);
                case ActionID.NA_ACTION_GET_MAIL_SEND_DATE_LIST_ALL:
                    return this._getMailSendDateListAll(inputDS);
                case ActionID.ACTION_MAIL_SEND_DATE_CREATE:
                    return this._createMailSendDate(inputDS);
                case ActionID.ACTION_MAIL_SEND_DATE_DEL:
                    return this._deleteMailSendDate(inputDS);
                case ActionID.NA_ACTION_GET_MAIL_BODY:
                    return this._getMailBody(inputDS);
                case ActionID.ACTION_MAIL_BODY_CREATE:
                    return this._createMailBody(inputDS);
                case ActionID.NA_ACTION_GET_MAILUSER_ALL:
                    return this._getMailUserListAll(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_ADVANCE_LIST_ALL:
                    return this._getEmployeeAdvanceListAll(inputDS);
                case ActionID.NA_ACTION_GET_EMPLOYEE_ADVANCE_LIST:
                    return this._getEmployeeAdvanceList(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _createMailUsers(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;
                        
            EmployeeAdvanceDS empAdDS = new EmployeeAdvanceDS();
            empAdDS.Merge(inputDS.Tables[empAdDS.MailUsers.TableName], false, MissingSchemaAction.Error);
            empAdDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();
            
            #region Delete Mail Users...

            cmd.CommandText = "PRO_EA_MAIL_USERS_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters.AddWithValue("p_DepartmentCode", stringDS.DataStrings[0].StringValue);

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.ACTION_MAIL_USERS_CREATE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            #endregion
            
            #region Create Mail Users...

            cmd.Parameters.Clear();
            cmd.CommandText = "PRO_EA_MAIL_USERS_CREATE";
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeAdvanceDS.MailUsersRow row in empAdDS.MailUsers.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_MailUsersID", genPK);

                if (!row.IsDepartmentCodeNull())
                {
                    cmd.Parameters.AddWithValue("p_DepartmentCode", row.DepartmentCode);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_DepartmentCode", DBNull.Value);
                }

                if (!row.IsEmployeeIDNull())
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", row.EmployeeID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_EmployeeID", DBNull.Value);
                }
                                
                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAIL_USERS_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getEmployeeListFromMailUser(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeListFromMailUser");
            cmd.Parameters["DeptCode"].Value = stringDS.DataStrings[0].StringValue;
            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            #region Data Load...
            EmployeeDS empDS = new EmployeeDS();
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_LIST_FROM_MAILUSER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empDS.AcceptChanges();
            #endregion
            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }

        private DataSet _getRuleConfigListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRuleConfigListAll");
            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            #region Data Load...
            EmployeeAdvanceDS empAdvDS = new EmployeeAdvanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, empAdvDS, empAdvDS.RuleConfiguration.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_RULECONFIGURATION_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empAdvDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empAdvDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }

        private DataSet _getMailDepartmentListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetMailDepartmentListAll");
            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            #region Data Load...
            EmployeeAdvanceDS empAdvDS = new EmployeeAdvanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, empAdvDS, empAdvDS.MailUsers.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_MAIL_DEPARTMENT_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empAdvDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empAdvDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }

        private DataSet _createMailRuleConfiguration(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;

            EmployeeAdvanceDS empAdDS = new EmployeeAdvanceDS();
            empAdDS.Merge(inputDS.Tables[empAdDS.RuleConfiguration.TableName], false, MissingSchemaAction.Error);
            empAdDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create Rule Configuration...
            cmd.CommandText = "PRO_EA_MAIL_RULECONFIG_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeAdvanceDS.RuleConfigurationRow row in empAdDS.RuleConfiguration.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_RuleConfigurationID", genPK);
                cmd.Parameters.AddWithValue("p_Task", row.Task);
                cmd.Parameters.AddWithValue("p_Operand", row.Operand);
                cmd.Parameters.AddWithValue("p_Days", row.Days);
                cmd.Parameters.AddWithValue("p_MailTo", row.MailTo);
                cmd.Parameters.AddWithValue("p_MailCC", row.MailCC);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAIL_USERS_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteMailRuleConfiguration(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;
            Int32 ruleConfigID = 0;

            EmployeeAdvanceDS empAdDS = new EmployeeAdvanceDS();
            empAdDS.Merge(inputDS.Tables[empAdDS.RuleConfiguration.TableName], false, MissingSchemaAction.Error);
            empAdDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Delete Mail Rule Configuration...

            cmd.CommandText = "PRO_EA_MAIL_RULECONFIG_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeAdvanceDS.RuleConfigurationRow row in empAdDS.RuleConfiguration.Rows)
            {
                cmd.Parameters.AddWithValue("p_RuleConfigurationID", row.RuleConfigurationID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAIL_RULE_CONFIGURATION_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getMailSendDateListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetMailSendDateListAll");
            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            #region Data Load...
            EmployeeAdvanceDS empAdvDS = new EmployeeAdvanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, empAdvDS, empAdvDS.MailSendDate.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_MAIL_SEND_DATE_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empAdvDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empAdvDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }

        private DataSet _createMailSendDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;

            EmployeeAdvanceDS empAdDS = new EmployeeAdvanceDS();
            empAdDS.Merge(inputDS.Tables[empAdDS.MailSendDate.TableName], false, MissingSchemaAction.Error);
            empAdDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create Mail send Date...
            cmd.CommandText = "PRO_EA_MAIL_SEND_DATE_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeAdvanceDS.MailSendDateRow row in empAdDS.MailSendDate.Rows)
            {
                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    return UtilDL.GetDBOperationFailed();
                }

                cmd.Parameters.AddWithValue("p_SendDateConfigID", genPK);
                cmd.Parameters.AddWithValue("p_SendDate", row.SendDate);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAIL_SEND_DATE_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _deleteMailSendDate(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;
            Int32 ruleConfigID = 0;

            EmployeeAdvanceDS empAdDS = new EmployeeAdvanceDS();
            empAdDS.Merge(inputDS.Tables[empAdDS.MailSendDate.TableName], false, MissingSchemaAction.Error);
            empAdDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Delete Mail Rule Configuration...

            cmd.CommandText = "PRO_EA_MAIL_SEND_DATE_DELETE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (EmployeeAdvanceDS.MailSendDateRow row in empAdDS.MailSendDate.Rows)
            {
                cmd.Parameters.AddWithValue("p_SendDateConfigID", row.SendDateConfigID);

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAIL_SEND_DATE_DEL.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getMailBody(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetMailBody");
            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            #region Data Load...
            EmployeeAdvanceDS empAdvDS = new EmployeeAdvanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, empAdvDS, empAdvDS.MailBody.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_MAIL_BODY.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empAdvDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empAdvDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }

        private DataSet _createMailBody(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            OleDbCommand cmd = new OleDbCommand();
            bool bError = false;
            int nRowAffected = -1;

            EmployeeAdvanceDS empAdDS = new EmployeeAdvanceDS();
            empAdDS.Merge(inputDS.Tables[empAdDS.MailBody.TableName], false, MissingSchemaAction.Error);
            empAdDS.AcceptChanges();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Create/ Update Mail send Date...
            cmd.CommandText = "PRO_EA_MAIL_BODY_CREATE";
            cmd.CommandType = CommandType.StoredProcedure;
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            if (empAdDS.MailBody.Rows.Count > 0)
            {
                EmployeeAdvanceDS.MailBodyRow row = (EmployeeAdvanceDS.MailBodyRow)empAdDS.MailBody.Rows[0];
                if (row.BodyConfigurationID == -1)
                {
                    long genPK = IDGenerator.GetNextGenericPK();
                    if (genPK == -1)
                    {
                        return UtilDL.GetDBOperationFailed();
                    }

                    cmd.Parameters.AddWithValue("p_BodyConfigurationID", genPK);
                }
                else
                {
                    cmd.CommandText = "PRO_EA_MAIL_BODY_UPDATE";
                    cmd.Parameters.AddWithValue("p_BodyConfigurationID", row.BodyConfigurationID);
                }
                cmd.Parameters.AddWithValue("p_Subject", row.Subject);
                if (!row.IsBodyPart1Null())
                    cmd.Parameters.AddWithValue("p_BodyPart1", row.BodyPart1);
                else cmd.Parameters.AddWithValue("p_BodyPart1", " ");
                if (!row.IsBodyPart3Null())
                    cmd.Parameters.AddWithValue("p_BodyPart3", row.BodyPart3);
                else cmd.Parameters.AddWithValue("p_BodyPart3", " ");

                bError = false;
                nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_MAIL_BODY_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getMailUserListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetMailUserListAll");
            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            #region Data Load...
            EmployeeAdvanceDS empAdvDS = new EmployeeAdvanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, empAdvDS, empAdvDS.MailUsers.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_MAILUSER_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empAdvDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empAdvDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }

        private DataSet _getEmployeeAdvanceListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeAdvanceListAll");
            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            #region Data Load...
            EmployeeAdvanceDS empAdvDS = new EmployeeAdvanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, empAdvDS, empAdvDS.EmployeeAdvance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_ADVANCE_LIST_ALL.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empAdvDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empAdvDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }

        private DataSet _getEmployeeAdvanceList(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetEmployeeAdvanceList");
            Debug.Assert(cmd != null);
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            #region Data Load...
            EmployeeAdvanceDS empAdvDS = new EmployeeAdvanceDS();
            nRowAffected = ADOController.Instance.Fill(adapter, empAdvDS, empAdvDS.EmployeeAdvance.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_EMPLOYEE_ADVANCE_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            empAdvDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(empAdvDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            //    
            return returnDS;
            //    
        }
    }    
}
