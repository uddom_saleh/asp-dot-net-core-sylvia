/* Compamy: Milllennium Information Solution Limited
 * Author: Km Jarif
 * Comment Date: October, 23, 2009
 * */

using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;

//
using Sylvia.Common.Reports;
//

namespace Sylvia.DAL.Payroll
{
    /// <summary>
    /// Summary description for GratuityProvisionDL.
    /// </summary>
    
    class GratuityProvisionDL
    {
        public GratuityProvisionDL()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                //For Gratuity Provision
                case ActionID.NA_ACTION_GET_GRATUITYPROVISION_LIST_LAST_MONTH:
                    return this._getGratuityProvisionLM(inputDS);
                case ActionID.NA_ACTION_GET_GRATUITYPROVISION_LIST_DATE_RANGE:
                    return this._getGratuityProvisionDR(inputDS);
                case ActionID.ACTION_GRATUITYPROVISION_UPD:
                    return this._updateGratuityProvision(inputDS);
                case ActionID.NA_ACTION_GET_PROVISIONEDGRATUITY_ANALYSIS_RPT:
                    return this._getProvisionedGratuityAnalysisReport(inputDS);
                case ActionID.NA_ACTION_GET_ACCUMULATEDGRATUITY_ANALYSIS_RPT:
                    return this._getAccumulatedGratuityAnalysisReport(inputDS);

                case ActionID.NA_ACTION_GET_GRATUITYSLIP:
                    return this._getGratuitySlip(inputDS);

                case ActionID.NA_ACTION_GET_GRATUITYPROVISION_LIST_SP:
                    return this._getGratuityProvisionLM_SP(inputDS);
                case ActionID.NA_ACTION_GET_GRATUITYPROVISION_BY_CODE:
                    return this._getGratuityProvisionBalanceByEmpCode(inputDS);    
                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement
        }

        private DataSet _getGratuityProvisionLM(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGratuityProvisionListOfLastMonth");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(27 Sep 11)...
            #region Old...
            //GratuityPO gPO = new GratuityPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, gPO, gPO.GratuityMonthlyProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRATUITYPROVISION_LIST_LAST_MONTH.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //gPO.AcceptChanges();

            //GratuityProvisionDS gpDS = new GratuityProvisionDS();

            //if (gPO.GratuityMonthlyProvision.Rows.Count > 0)
            //{
            //    foreach (GratuityPO.GratuityMonthlyProvisionRow poGP in gPO.GratuityMonthlyProvision.Rows)
            //    {
            //        GratuityProvisionDS.GratuityMonthlyProvisionRow gp = gpDS.GratuityMonthlyProvision.NewGratuityMonthlyProvisionRow();
            //        if (poGP.IsSalaryIdNull() == false)
            //        {
            //            gp.SalaryId = poGP.SalaryId;
            //        }
            //        if (poGP.IsEmployeeIdNull() == false)
            //        {
            //            gp.EmployeeId = poGP.EmployeeId;
            //        }
            //        if (poGP.IsEmployeeCodeNull() == false)
            //        {
            //            gp.EmployeeCode = poGP.EmployeeCode;
            //        }
            //        if (poGP.IsEmployeeStatusNull() == false)
            //        {
            //            gp.EmployeeStatus = poGP.EmployeeStatus;
            //        }
            //        if (poGP.IsMonthYearDateNull() == false)
            //        {
            //            gp.MonthYearDate = poGP.MonthYearDate;
            //        }
            //        if (poGP.IsAccumulatedCalculationAmountNull() == false)
            //        {
            //            gp.AccumulatedCalculationAmount = poGP.AccumulatedCalculationAmount;
            //        }
            //        if (poGP.IsAccumulatedChangingAmountNull() == false)
            //        {
            //            gp.AccumulatedChangingAmount = poGP.AccumulatedChangingAmount;
            //        }
            //        if (poGP.IsProvisionedAmountNull() == false)
            //        {
            //            gp.ProvisionedAmount = poGP.ProvisionedAmount;
            //        }

            //        gpDS.GratuityMonthlyProvision.AddGratuityMonthlyProvisionRow(gp);
            //        gpDS.AcceptChanges();
            //    }
            //}
            #endregion

            GratuityProvisionDS gpDS = new GratuityProvisionDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, gpDS, gpDS.GratuityMonthlyProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRATUITYPROVISION_LIST_LAST_MONTH.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            gpDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(gpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _getGratuityProvisionDR(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGratuityProvisionListOnDateRange");

            //take the dates into the parameters of the command
            if ((dateDS.DataDates[0].IsDateValueNull() == false) && (dateDS.DataDates[1].IsDateValueNull() == false))
            {
                cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
                cmd.Parameters["ToDate"].Value = dateDS.DataDates[1].DateValue;
            }

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(27 Sep 11)...
            #region Old...
            //GratuityPO gPO = new GratuityPO();

            //bool bError = false;
            //int nRowAffected = -1;
            //nRowAffected = ADOController.Instance.Fill(adapter, gPO, gPO.GratuityMonthlyProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRATUITYPROVISION_LIST_DATE_RANGE.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //gPO.AcceptChanges();

            //GratuityProvisionDS gpDS = new GratuityProvisionDS();

            //if (gPO.GratuityMonthlyProvision.Rows.Count > 0)
            //{
            //    foreach (GratuityPO.GratuityMonthlyProvisionRow poGP in gPO.GratuityMonthlyProvision.Rows)
            //    {
            //        GratuityProvisionDS.GratuityMonthlyProvisionRow gp = gpDS.GratuityMonthlyProvision.NewGratuityMonthlyProvisionRow();
            //        if (poGP.IsSalaryIdNull() == false)
            //        {
            //            gp.SalaryId = poGP.SalaryId;
            //        }
            //        if (poGP.IsEmployeeIdNull() == false)
            //        {
            //            gp.EmployeeId = poGP.EmployeeId;
            //        }
            //        if (poGP.IsEmployeeCodeNull() == false)
            //        {
            //            gp.EmployeeCode = poGP.EmployeeCode;
            //        }
            //        if (poGP.IsEmployeeNameNull() == false)
            //        {
            //            gp.EmployeeName = poGP.EmployeeName;
            //        }
            //        if (poGP.IsEmployeeStatusNull() == false)
            //        {
            //            gp.EmployeeStatus = poGP.EmployeeStatus;
            //        }
            //        if (poGP.IsMonthYearDateNull() == false)
            //        {
            //            gp.MonthYearDate = poGP.MonthYearDate;
            //        }
            //        if (poGP.IsAccumulatedCalculationAmountNull() == false)
            //        {
            //            gp.AccumulatedCalculationAmount = poGP.AccumulatedCalculationAmount;
            //        }
            //        if (poGP.IsAccumulatedChangingAmountNull() == false)
            //        {
            //            gp.AccumulatedChangingAmount = poGP.AccumulatedChangingAmount;
            //        }
            //        if (poGP.IsProvisionedAmountNull() == false)
            //        {
            //            gp.ProvisionedAmount = poGP.ProvisionedAmount;
            //        }
            //        if (poGP.IsRemarksNull() == false)
            //        {
            //            gp.Remarks = poGP.Remarks;
            //        }

            //        gpDS.GratuityMonthlyProvision.AddGratuityMonthlyProvisionRow(gp);
            //        gpDS.AcceptChanges();
            //    }
            //}
            #endregion

            GratuityProvisionDS gpDS = new GratuityProvisionDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, gpDS, gpDS.GratuityMonthlyProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRATUITYPROVISION_LIST_DATE_RANGE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            gpDS.AcceptChanges();
            #endregion

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(gpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }

        private DataSet _updateGratuityProvision(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            GratuityProvisionDS gpDS = new GratuityProvisionDS();
            DBConnectionDS connDS = new DBConnectionDS();

            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateGetGratuityProvision");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            gpDS.Merge(inputDS.Tables[gpDS.GratuityMonthlyProvision.TableName], false, MissingSchemaAction.Error);
            gpDS.AcceptChanges();


            foreach (GratuityProvisionDS.GratuityMonthlyProvisionRow gp in gpDS.GratuityMonthlyProvision.Rows)
            {
                // SalaryId
                if (gp.IsSalaryIdNull() == false)
                {
                    cmd.Parameters["SalaryId"].Value = (object)gp.SalaryId;
                }
                else
                {
                    cmd.Parameters["SalaryId"].Value = null;
                }
                //Accumulated Changing Amount
                if (gp.IsAccumulatedChangingAmountNull() == false)
                {
                    cmd.Parameters["AccumulatedChangingAmount"].Value = (object)gp.AccumulatedChangingAmount;
                }
                else
                {
                    cmd.Parameters["AccumulatedChangingAmount"].Value = null;
                }
                //Provisioned Amount
                if (gp.IsProvisionedAmountNull() == false)
                {
                    cmd.Parameters["ProvisionedAmount"].Value = (object)gp.ProvisionedAmount;
                }
                else
                {
                    cmd.Parameters["ProvisionedAmount"].Value = null;
                }
                //Remarks
                if (gp.IsRemarksNull() == false)
                {
                    cmd.Parameters["Remarks"].Value = (object)gp.Remarks;
                }
                else
                {
                    cmd.Parameters["Remarks"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_GRATUITYPROVISION_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getProvisionedGratuityAnalysisReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetProvisionedGratuityAnalysisData");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            //take the dates into the parameters of the command
            if (dateDS.DataDates[0].IsDateValueNull() == false)
            {
                cmd.Parameters["FromDate"].Value = dateDS.DataDates[0].DateValue;
            }
            if (dateDS.DataDates[1].IsDateValueNull() == false)
            {
                cmd.Parameters["ToDate"].Value = dateDS.DataDates[1].DateValue;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(27 Sep 11)...
            #region Old...
            ////-----------------------Declare the DataSets DS-----------------------
            //GratuityPO gratuityPO = new GratuityPO();

            ////------------------------------get employee detail------------------------
            //bool bError = false;
            //int nRowAffected = -1;

            //nRowAffected = ADOController.Instance.Fill(adapter,
            //  gratuityPO, gratuityPO.GratuityAnalysisDetail.TableName,
            //  connDS.DBConnections[0].ConnectionID,
            //  ref bError);

            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_PROVISIONEDGRATUITY_ANALYSIS_RPT.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //gratuityPO.AcceptChanges();

            //GratuityProvisionDS gratuityAnalysisDS = new GratuityProvisionDS();
            //// now create the packet

            //if (gratuityPO.GratuityAnalysisDetail.Count > 0)
            //{
            //    decimal nAmount = 0;
            //    foreach (GratuityPO.GratuityAnalysisDetailRow gratuityPo in gratuityPO.GratuityAnalysisDetail.Rows)
            //    {
            //        GratuityProvisionDS.GratuityAnalysisDetailRow gratuityAnalysis = gratuityAnalysisDS.GratuityAnalysisDetail.NewGratuityAnalysisDetailRow();
                    
            //        if (gratuityPo.IsEmployeeCodeNull() == false)
            //        {
            //            gratuityAnalysis.EmployeeCode = gratuityPo.EmployeeCode;
            //        }
            //        if (gratuityPo.IsEmployeeNameNull() == false)
            //        {
            //            gratuityAnalysis.EmployeeName = gratuityPo.EmployeeName;
            //        }
            //        if (gratuityPo.IsFunctionCodeNull() == false)
            //        {
            //            gratuityAnalysis.FunctionCode = gratuityPo.FunctionCode;
            //        }
            //        if (gratuityPo.IsFunctionNameNull() == false)
            //        {
            //            gratuityAnalysis.FunctionName = gratuityPo.FunctionName;
            //        }

            //        if (gratuityPo.IsAmountNull() == false)
            //        {
            //            gratuityAnalysis.Amount = gratuityPo.Amount;
            //        }

            //        gratuityAnalysisDS.GratuityAnalysisDetail.AddGratuityAnalysisDetailRow(gratuityAnalysis);
            //    }
            //    gratuityAnalysisDS.AcceptChanges();
            //}
            #endregion

            GratuityProvisionDS gratuityAnalysisDS = new GratuityProvisionDS();
            
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, gratuityAnalysisDS, gratuityAnalysisDS.GratuityAnalysisDetail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_PROVISIONEDGRATUITY_ANALYSIS_RPT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            gratuityAnalysisDS.AcceptChanges();
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(gratuityAnalysisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _getAccumulatedGratuityAnalysisReport(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAccumulatedGratuityAnalysisData");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            //take the dates into the parameters of the command
            if (dateDS.DataDates[0].IsDateValueNull() == false)
            {
                cmd.Parameters["MonthYearDate"].Value = dateDS.DataDates[0].DateValue;
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            #region Update By Jarif(27 Sep 11)...
            #region Old...
            ////-----------------------Declare the DataSets DS-----------------------
            //GratuityPO gratuityPO = new GratuityPO();

            ////------------------------------get employee detail------------------------
            //bool bError = false;
            //int nRowAffected = -1;

            //nRowAffected = ADOController.Instance.Fill(adapter,
            //  gratuityPO, gratuityPO.GratuityAnalysisDetail.TableName,
            //  connDS.DBConnections[0].ConnectionID,
            //  ref bError);

            //if (bError == true)
            //{
            //    ErrorDS.Error err = errDS.Errors.NewError();
            //    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
            //    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
            //    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
            //    err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACCUMULATEDGRATUITY_ANALYSIS_RPT.ToString();
            //    errDS.Errors.AddError(err);
            //    errDS.AcceptChanges();
            //    return errDS;

            //}

            //gratuityPO.AcceptChanges();

            //GratuityProvisionDS gratuityAnalysisDS = new GratuityProvisionDS();
            //// now create the packet

            //if (gratuityPO.GratuityAnalysisDetail.Count > 0)
            //{
            //    decimal nAmount = 0;
            //    foreach (GratuityPO.GratuityAnalysisDetailRow gratuityPo in gratuityPO.GratuityAnalysisDetail.Rows)
            //    {
            //        GratuityProvisionDS.GratuityAnalysisDetailRow gratuityAnalysis = gratuityAnalysisDS.GratuityAnalysisDetail.NewGratuityAnalysisDetailRow();

            //        if (gratuityPo.IsEmployeeCodeNull() == false)
            //        {
            //            gratuityAnalysis.EmployeeCode = gratuityPo.EmployeeCode;
            //        }
            //        if (gratuityPo.IsEmployeeNameNull() == false)
            //        {
            //            gratuityAnalysis.EmployeeName = gratuityPo.EmployeeName;
            //        }
            //        if (gratuityPo.IsFunctionCodeNull() == false)
            //        {
            //            gratuityAnalysis.FunctionCode = gratuityPo.FunctionCode;
            //        }
            //        if (gratuityPo.IsFunctionNameNull() == false)
            //        {
            //            gratuityAnalysis.FunctionName = gratuityPo.FunctionName;
            //        }

            //        if (gratuityPo.IsAmountNull() == false)
            //        {
            //            gratuityAnalysis.Amount = gratuityPo.Amount;
            //        }
            //        if (gratuityPo.IsRemarksNull() == false)
            //        {
            //            gratuityAnalysis.Remarks = gratuityPo.Remarks;
            //        }

            //        gratuityAnalysisDS.GratuityAnalysisDetail.AddGratuityAnalysisDetailRow(gratuityAnalysis);
            //    }
            //    gratuityAnalysisDS.AcceptChanges();
            //}
            #endregion

            GratuityProvisionDS gratuityAnalysisDS = new GratuityProvisionDS();
            
            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, gratuityAnalysisDS, gratuityAnalysisDS.GratuityAnalysisDetail.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ACCUMULATEDGRATUITY_ANALYSIS_RPT.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            gratuityAnalysisDS.AcceptChanges();
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(gratuityAnalysisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        // WALI :: 16-Aug-2015
        private DataSet _getGratuitySlip(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();
            OleDbCommand cmd = new OleDbCommand();
            GratuityProvisionDS gratuityAnalysisDS = new GratuityProvisionDS();
            OleDbDataAdapter adapter = new OleDbDataAdapter();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            cmd = DBCommandProvider.GetDBCommand("GetGratuitySlip");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["SeparationDate"].Value = dateDS.DataDates[0].DateValue.ToString("MM/dd/yyyy");
            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, gratuityAnalysisDS, gratuityAnalysisDS.GratuityProvisions.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRATUITYSLIP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;
            }
            gratuityAnalysisDS.AcceptChanges();

            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();
            returnDS.Merge(gratuityAnalysisDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;
        }

        private DataSet _getGratuityProvisionLM_SP(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataIntegerDS integerDS = new DataIntegerDS();
            integerDS.Merge(inputDS.Tables[integerDS.DataIntegers.TableName], false, MissingSchemaAction.Error);
            integerDS.AcceptChanges();

            DataDateDS dateDS = new DataDateDS();
            dateDS.Merge(inputDS.Tables[dateDS.DataDates.TableName], false, MissingSchemaAction.Error);
            dateDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGratuityProvisionList_SP");
            if (cmd == null)  return UtilDL.GetCommandNotFound();

            cmd.Parameters["SalaryDate"].Value = (object)dateDS.DataDates[0].DateValue;
            cmd.Parameters["EmployeeTypeID"].Value = (object)integerDS.DataIntegers[0].IntegerValue;
            cmd.Parameters["PayrollSiteID"].Value = (object)integerDS.DataIntegers[1].IntegerValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            GratuityProvisionDS gpDS = new GratuityProvisionDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, gpDS, gpDS.GratuityMonthlyProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRATUITYPROVISION_LIST_SP.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            gpDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(gpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
        private DataSet _getGratuityProvisionBalanceByEmpCode(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS stringDS = new DataStringDS();
            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetGratuityBalanceByEmpCode");
            if (cmd == null) return UtilDL.GetCommandNotFound();

            cmd.Parameters["EmployeeCode"].Value = stringDS.DataStrings[0].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            GratuityProvisionDS gpDS = new GratuityProvisionDS();

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, gpDS, gpDS.GratuityMonthlyProvision.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_GRATUITYPROVISION_BY_CODE.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            gpDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(gpDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;

        }
    }
}
