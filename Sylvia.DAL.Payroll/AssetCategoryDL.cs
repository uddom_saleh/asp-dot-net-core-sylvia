﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using Sylvia.Common;
using Sylvia.DAL.Database;
using Sylvia.DAL.Payroll.PO;


namespace Sylvia.DAL.Payroll
{
    class AssetCategoryDL
    {
        public DataSet Execute(DataSet inputDS)
        {
            DataSet returnDS = new DataSet();
            ErrorDS errDS = new ErrorDS();

            ActionDS actionDS = new ActionDS();
            actionDS.Merge(inputDS.Tables[actionDS.Actions.TableName], false, MissingSchemaAction.Error);
            actionDS.AcceptChanges();

            ActionID actionID = ActionID.ACTION_UNKOWN_ID; // just set it to a default
            try
            {
                actionID = (ActionID)Enum.Parse(typeof(ActionID), actionDS.Actions[0].ActionID, true);
            }
            catch
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_ACTION_ID_INVALID.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_BL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                err.ErrorInfo1 = "Invalid Action ID: " + actionDS.Actions[0];
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                returnDS.Merge(errDS);
                returnDS.AcceptChanges();
                return returnDS;
            }

            switch (actionID)
            {
                case ActionID.ACTION_ASSET_CATEGORY_CREATE:
                    return this._AssetCategoryEntry(inputDS);
                case ActionID.NA_ACTION_GET_ASSET_CATEGORY_LIST:
                    return this._getAssetCategoryList(inputDS);
                case ActionID.ACTION_ASSET_CATEGORY_UPD:
                    return this._updateAssetCategories(inputDS);
                case ActionID.ACTION_ASSET_CATEGORY_DELETE:
                    return this._deleteAssetCategories(inputDS);
                case ActionID.NA_ACTION_GET_ASSET_CATEGORY_LIST_ALL:
                    return this._GetAssetCategoryListAll(inputDS);
                case ActionID.NA_ACTION_GET_ASSET_TYPES_LIST_ALL:
                    return this._GetAssetTypesListAll(inputDS);
                case ActionID.NA_ACTION_GET_ASS_TYPE_LIST_BY_CATEGORYEID:
                    return this._GetAssetTypeByAssCategoryID(inputDS);
                case ActionID.NA_ACTION_GET_ASS_FIELD_LIST_BY_TYPEID:
                    return this._GetAssetFieldByAssTypeID(inputDS);
                case ActionID.ACTION_ASS_CATEGORY_CONFIG_CREATE:
                    return this._AssetCategoryConfigEntry(inputDS);
                case ActionID.ACTION_ASS_CATEGORY_CONFIG_DELETE:
                    return this._AssetCategoryConfigDelete(inputDS);
                //case ActionID.NA_ActionGetFieldBy_TypeCodeCategoryID:
                //    return this._GetRequisitionFieldBTypeCodeCategoryID(inputDS);
                case ActionID.NA_ACTION_ASSET_GET_LCGRADE_LIST:
                    return this._GetLCGradeListAll_AssCateConfig(inputDS);
                case ActionID.ACTION_ASSET_CATEGORY_ASSIGN:
                    return this._assignGradeToAssCategory(inputDS);
                case ActionID.ACTION_ASSET_FIELD_LIMIT_CONFIG:
                    return this._AssetLimitMarkableConfigEntry(inputDS);
                case ActionID.NA_ACTION_GET_ASS_FIELD_VALUE:
                    return this._GetAssetFieldValue(inputDS);
                case ActionID.NA_ACTION_GET_REQ_LIMIT_FIELD_LIST_BY_TYPEID:
                //    return this._GetReqLimitFieldListByReqTypeID(inputDS);
                //case ActionID.NA_ACTION_GET_REQ_LIMIT_FIELD_LIST_BY_TYPEID2:
                //    return this._GetReqLimitFieldListByReqTypeID2(inputDS);
                //case ActionID.NA_ACTION_GET_REQUISITION_HISTORY:
                //    return this._GetRequisitionDataForReport(inputDS);
                //case ActionID.NA_ACTION_GET_REQUISITION_HEALTH_SAFTY:
                //    return this._GetReqHealthSafetyForReport(inputDS);

                default:
                    Util.LogInfo("Action ID is not supported in this class.");
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_ACTION_NOT_SUPPORTED.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_NORMAL.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorInfo1 = "Unsupported Action ID: " + actionID.ToString();
                    errDS.Errors.AddError(err);
                    returnDS.Merge(errDS);
                    returnDS.AcceptChanges();
                    return returnDS;
            }  // end of switch statement

        }

        private DataSet _AssetCategoryEntry(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("AssetCategoryEntry");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            AssetDS assetDS = new AssetDS();
            assetDS.Merge(inputDS.Tables[assetDS.AssetCategory.TableName], false, MissingSchemaAction.Error);
            assetDS.AcceptChanges();

            foreach (AssetDS.AssetCategoryRow CategoryRow in assetDS.AssetCategory.Rows)
            {

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters["ReqID"].Value = (object)genPK;


                if (CategoryRow.IsASSETCATEGORYCODENull() == false)
                {
                    cmd.Parameters["ReqCode"].Value = (object)CategoryRow.ASSETCATEGORYCODE;
                }
                else
                {
                    cmd.Parameters["ReqCode"].Value = null;
                }

                if (CategoryRow.IsASSETCATEGORYNAMENull() == false)
                {
                    cmd.Parameters["ReqName"].Value = (object)CategoryRow.ASSETCATEGORYNAME;
                }
                else
                {
                    cmd.Parameters["ReqName"].Value = null;
                }

                if (CategoryRow.IsASSETCATEGORYREMARKSNull() == false)
                {
                    cmd.Parameters["ReqCateRemarks"].Value = (object)CategoryRow.ASSETCATEGORYREMARKS;
                }
                else
                {
                    cmd.Parameters["ReqCateRemarks"].Value = null;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSET_CATEGORY_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _getAssetCategoryList(DataSet inputDS)
        {

            OleDbCommand cmd = new OleDbCommand();
            //Product_MngtDS ProductDS = new Product_MngtDS();
            AssetDS ReqDS = new AssetDS();

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            DataStringDS stringDS = new DataStringDS();
            string AssCategoryCode = "";

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();
            AssCategoryCode = stringDS.DataStrings[0].StringValue;


            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //Get sql command from commandXML folder
            if (AssCategoryCode != "")
            {
                cmd = DBCommandProvider.GetDBCommand("GetAssetCategoryList");
                cmd.Parameters["ReqCateCode"].Value = AssCategoryCode;

                if (cmd == null)
                {
                    return UtilDL.GetCommandNotFound();
                }
            }
            else
            {
                cmd = DBCommandProvider.GetDBCommand("GetAssetCategoryAll");
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, ReqDS, ReqDS.AssetCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_ASSET_CATEGORY_LIST.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();
                return errDS;

            }
            ReqDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();
            DataSet returnDS = new DataSet();
            returnDS.Merge(ReqDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();
            return returnDS;

        }

        private DataSet _deleteAssetCategories(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataStringDS stringDS = new DataStringDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            stringDS.Merge(inputDS.Tables[stringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            stringDS.AcceptChanges();


            OleDbCommand cmd = DBCommandProvider.GetDBCommand("DeleteAssetCategories");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            foreach (DataStringDS.DataString sValue in stringDS.DataStrings)
            {
                if (sValue.IsStringValueNull() == false)
                {
                    cmd.Parameters["REQCATEGORYCODE"].Value = (object)sValue.StringValue;
                }

                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_CATEGORY_DELETE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }

            return errDS;  // return empty ErrorDS
        }

        private DataSet _updateAssetCategories(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            AssetDS assetDS = new AssetDS();
            DBConnectionDS connDS = new DBConnectionDS();



            //extract dbconnection 
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("UpdateAssetCategories");
            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            assetDS.Merge(inputDS.Tables[assetDS.AssetCategory.TableName], false, MissingSchemaAction.Error);
            assetDS.AcceptChanges();



            foreach (AssetDS.AssetCategoryRow AssCategoryRow in assetDS.AssetCategory.Rows)
            {

                if (AssCategoryRow.IsASSETCATEGORYNAMENull() == false)
                {
                    cmd.Parameters["REQCATEGORYNAME"].Value = (object)AssCategoryRow.ASSETCATEGORYNAME;
                }
                else
                {
                    cmd.Parameters["REQCATEGORYNAME"].Value = null;
                }


                if (AssCategoryRow.IsASSETCATEGORYREMARKSNull() == false)
                {
                    cmd.Parameters["REQCATEGORYREMARKS"].Value = (object)AssCategoryRow.ASSETCATEGORYREMARKS;
                }
                else
                {
                    cmd.Parameters["REQCATEGORYREMARKS"].Value = null;
                }


                if (AssCategoryRow.IsASSETCATEGORYCODENull() == false)
                {
                    cmd.Parameters["REQCATEGORYCODE"].Value = (object)AssCategoryRow.ASSETCATEGORYCODE;
                }
                else
                {
                    cmd.Parameters["REQCATEGORYCODE"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSET_CATEGORY_UPD.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetAssetCategoryListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetAssetCategoryAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            AssetDS assetDS = new AssetDS();
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetCategory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            assetDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetAssetTypesListAll(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetAssetTypesAll");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            AssetDS assetDS = new AssetDS();
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssetType.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            assetDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;
        }

        private DataSet _GetAssetTypeByAssCategoryID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAssTypeByAssCateID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["REQCATEGORYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            AssetDS assetDS = new AssetDS();
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AssCateConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            assetDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _GetAssetFieldByAssTypeID(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAFieldListLimitByTypeID");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }
            cmd.Parameters["REQUISITIONTYPEID1"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            cmd.Parameters["REQUISITIONTYPEID2"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            cmd.Parameters["REQCATEGORYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[1].StringValue);

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            AssetDS assetDS = new AssetDS();
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AIFConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            assetDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        private DataSet _AssetCategoryConfigEntry(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Data Delete from "AssetCateConfig" Table by gridCode wise

            //create command
            OleDbCommand cmd1 = DBCommandProvider.GetDBCommand("AssetCategoryConfigDelete");

            if (cmd1 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            AssetDS AssetDS = new AssetDS();
            AssetDS.Merge(inputDS.Tables[AssetDS.AssCateConfig.TableName], false, MissingSchemaAction.Error);
            AssetDS.AcceptChanges();

            foreach (AssetDS.AssCateConfigRow ReqCateConfigRow1 in AssetDS.AssCateConfig.Rows)
            {


                if (ReqCateConfigRow1.IsAssCategoryIDNull() == false)
                {
                    cmd1.Parameters["REQCATEGORYID"].Value = (object)ReqCateConfigRow1.AssCategoryID;
                }
                else
                {
                    cmd1.Parameters["REQCATEGORYID"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd1, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASS_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion


            #region Data Insert to "AssetCateConfig" from grdTypeList
            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("AssetCategoryConfigEntry");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            AssetDS AssetDS2 = new AssetDS();
            AssetDS2.Merge(inputDS.Tables[AssetDS2.AssCateConfig.TableName], false, MissingSchemaAction.Error);
            AssetDS2.AcceptChanges();

            foreach (AssetDS.AssCateConfigRow AssCateConfigRow in AssetDS2.AssCateConfig.Rows)
            {

                long genPK = IDGenerator.GetNextGenericPK();
                if (genPK == -1)
                {
                    UtilDL.GetDBOperationFailed();
                }
                cmd.Parameters["RCCID"].Value = (object)genPK;

                if (AssCateConfigRow.IsAssCategoryIDNull() == false)
                {
                    cmd.Parameters["REQCATEGORYID"].Value = (object)AssCateConfigRow.AssCategoryID;
                }
                else
                {
                    cmd.Parameters["REQCATEGORYID"].Value = null;
                }


                if (AssCateConfigRow.IsAssetTypeCodeNull() == false)
                {
                    cmd.Parameters["REQUISITIONTYPECODe"].Value = (object)AssCateConfigRow.AssetTypeCode;
                }
                else
                {
                    cmd.Parameters["REQUISITIONTYPECODe"].Value = null;
                }



                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);


                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASS_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion



            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _AssetCategoryConfigDelete(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            #region Data Delete from database grid code wise
            //create command
            OleDbCommand cmd4 = DBCommandProvider.GetDBCommand("AssetCateConfigDelete");

            if (cmd4 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            AssetDS AssetDS = new AssetDS();
            AssetDS.Merge(inputDS.Tables[AssetDS.AssCateConfig.TableName], false, MissingSchemaAction.Error);
            AssetDS.AcceptChanges();

            foreach (AssetDS.AssCateConfigRow AssCateConfigRow1 in AssetDS.AssCateConfig.Rows)
            {


                if (AssCateConfigRow1.IsAssCategoryIDNull() == false)
                {
                    cmd4.Parameters["REQCATEGORYID"].Value = (object)AssCateConfigRow1.AssCategoryID;
                }
                else
                {
                    cmd4.Parameters["REQCATEGORYID"].Value = null;
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd4, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASS_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }
            #endregion

            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        // private DataSet _GetRequisitionFieldBTypeCodeCategoryID(DataSet inputDS)
        //{

        //ErrorDS errDS = new ErrorDS();

        //DBConnectionDS connDS = new DBConnectionDS();
        //DataLongDS longDS = new DataLongDS();

        //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //connDS.AcceptChanges();

        //DataStringDS StringDS = new DataStringDS();
        //StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        //StringDS.AcceptChanges();

        //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetRFieldListByTypeCodeCateID");

        //if (cmd == null)
        //{
        //    return UtilDL.GetCommandNotFound();
        //}

        //cmd.Parameters["REQUISITIONTYPECODE"].Value = (object)StringDS.DataStrings[0].StringValue;
        //cmd.Parameters["REQCATEGORYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[1].StringValue);

        //OleDbDataAdapter adapter = new OleDbDataAdapter();
        //adapter.SelectCommand = cmd;

        //bool bError = false;
        //int nRowAffected = -1;

        //AssetDS assetDS = new AssetDS();
        //nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AIFConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        //assetDS.AcceptChanges();

        //// now create the packet
        //errDS.Clear();
        //errDS.AcceptChanges();

        //DataSet returnDS = new DataSet();

        //returnDS.Merge(assetDS);
        //returnDS.Merge(errDS);
        //returnDS.AcceptChanges();

        //return returnDS;


        // }

        private DataSet _GetLCGradeListAll_AssCateConfig(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            OleDbCommand cmd = new OleDbCommand();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            string activeStatus = StringDS.DataStrings[0].StringValue;

            cmd = DBCommandProvider.GetDBCommand("GetAssGradeListAll_AssCategory");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            AssetDS assetDS = new AssetDS();
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.Grade.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            assetDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;




        }

        private DataSet _assignGradeToAssCategory(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();
            DBConnectionDS connDS = new DBConnectionDS();

            OleDbCommand cmd = new OleDbCommand();
            cmd.CommandText = "PRO_ASSET_GRADE_UPDATE";
            cmd.CommandType = CommandType.StoredProcedure;

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            //lmsLeaveCategoryDS AssetDS  = new lmsLeaveCategoryDS();
            AssetDS AssetDS = new AssetDS();
            AssetDS.Merge(inputDS.Tables[AssetDS.Grade.TableName], false, MissingSchemaAction.Error);
            AssetDS.AcceptChanges(); ;

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            foreach (AssetDS.GradeRow row in AssetDS.Grade.Rows)
            {
                cmd.Parameters.AddWithValue("p_GradeID", row.GRADEID);
                if (row.IsASSCATEGORYIDNull() == false)
                {
                    cmd.Parameters.AddWithValue("p_AssCategoryID", row.ASSCATEGORYID);
                }
                else
                {
                    cmd.Parameters.AddWithValue("p_AssCategoryID", DBNull.Value);
                }


                bool bError = false;
                int nRowAffected = -1;
                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);

                if (bError)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASSET_CATEGORY_ASSIGN.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
                cmd.Parameters.Clear();
            }
            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _AssetLimitMarkableConfigEntry(DataSet inputDS)
        {
            ErrorDS errDS = new ErrorDS();

            //extract dbconnection
            DBConnectionDS connDS = new DBConnectionDS();
            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();


            #region Data Delete from "ASSCATECONFIG_WITH_FIELDLIMIT" Database
            //create command
            OleDbCommand cmd = DBCommandProvider.GetDBCommand("AssCateConfigFieldLimitDelete");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            AssetDS AssetDS = new AssetDS();
            AssetDS.Merge(inputDS.Tables[AssetDS.ASSCATECONFIG_WITH_FIELDLIMIT.TableName], false, MissingSchemaAction.Error);
            AssetDS.Merge(inputDS.Tables[AssetDS.AssCateConfig.TableName], false, MissingSchemaAction.Error);

            AssetDS.AcceptChanges();

            foreach (AssetDS.AssCateConfigRow AssCateConfigRow in AssetDS.AssCateConfig.Rows)
            {
                if (AssCateConfigRow.IsAssCategoryIDNull() == false)
                {
                    cmd.Parameters["REQCATEGORYID"].Value = (object)AssCateConfigRow.AssCategoryID;
                }
                else
                {
                    cmd.Parameters["REQCATEGORYID"].Value = null;
                }


                if (AssCateConfigRow.IsAssetTypeIDNull() == false)
                {
                    cmd.Parameters["REQUISITIONTYPEid"].Value = (object)AssCateConfigRow.AssetTypeID;
                }
                else
                {
                    cmd.Parameters["REQUISITIONTYPEid"].Value = null;
                }
            }


            foreach (AssetDS.ASSCATECONFIG_WITH_FIELDLIMITRow AssCateConfigRow_ in AssetDS.ASSCATECONFIG_WITH_FIELDLIMIT.Rows)
            {

                if (AssCateConfigRow_.IsfieldNameNull() == false)
                {
                    cmd.Parameters["fieldName"].Value = (object)AssCateConfigRow_.fieldName;
                }
                else
                {
                    cmd.Parameters["fieldName"].Value = null;
                }



                bool bError = false;
                int nRowAffected = -1;

                nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd, connDS.DBConnections[0].ConnectionID, ref bError);


                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_REQ_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }




            #endregion


            #region Data Insert to "REQCATECONFIG_WITH_FIELDLIMIT" from grdLimitValueList
            //create command
            OleDbCommand cmd3 = DBCommandProvider.GetDBCommand("AssCateConfigFieldLimitEntry");

            if (cmd3 == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            AssetDS AssetDS2 = new AssetDS();
            AssetDS2.Merge(inputDS.Tables[AssetDS2.ASSCATECONFIG_WITH_FIELDLIMIT.TableName], false, MissingSchemaAction.Error);
            AssetDS2.Merge(inputDS.Tables[AssetDS2.AssCateConfig.TableName], false, MissingSchemaAction.Error);

            AssetDS2.AcceptChanges();

            foreach (AssetDS.AssCateConfigRow AssCateConfigRow in AssetDS2.AssCateConfig.Rows)
            {
                if (AssCateConfigRow.IsAssCategoryIDNull() == false)
                {
                    cmd3.Parameters["REQCATEGORYID"].Value = (object)AssCateConfigRow.AssCategoryID;
                }
                else
                {
                    cmd3.Parameters["REQCATEGORYID"].Value = null;
                }


                if (AssCateConfigRow.IsAssetTypeIDNull() == false)
                {
                    cmd3.Parameters["REQUISITIONTYPEid"].Value = (object)AssCateConfigRow.AssetTypeID;
                }
                else
                {
                    cmd3.Parameters["REQUISITIONTYPEid"].Value = null;
                }
            }


            foreach (AssetDS.ASSCATECONFIG_WITH_FIELDLIMITRow AssCateConfigRow_ in AssetDS.ASSCATECONFIG_WITH_FIELDLIMIT.Rows)
            {

                if (AssCateConfigRow_.IsfieldNameNull() == false)
                {
                    cmd3.Parameters["fieldName"].Value = (object)AssCateConfigRow_.fieldName;
                }
                else
                {
                    cmd3.Parameters["fieldName"].Value = null;
                }


                if (AssCateConfigRow_.IsMinValueNull() == false)
                {
                    cmd3.Parameters["MinValue"].Value = (object)AssCateConfigRow_.MinValue;
                }
                else
                {
                    cmd3.Parameters["MinValue"].Value = null;
                }


                if (AssCateConfigRow_.IsMaxValueNull() == false)
                {
                    cmd3.Parameters["MaxValue"].Value = (object)AssCateConfigRow_.MaxValue;
                }
                else
                {
                    cmd3.Parameters["MaxValue"].Value = null;
                }



                bool bError = false;
                int nRowAffected = -1;

                if (cmd3.Parameters["MinValue"].Value != null && cmd3.Parameters["MaxValue"].Value != null)
                {
                    nRowAffected = ADOController.Instance.ExecuteNonQuery(cmd3, connDS.DBConnections[0].ConnectionID, ref bError);
                }

                if (bError == true)
                {
                    ErrorDS.Error err = errDS.Errors.NewError();
                    err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                    err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                    err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                    err.ErrorInfo1 = ActionID.ACTION_ASS_CATEGORY_CONFIG_CREATE.ToString();
                    errDS.Errors.AddError(err);
                    errDS.AcceptChanges();
                    return errDS;
                }
            }




            #endregion


            errDS.Clear();
            errDS.AcceptChanges();
            return errDS;
        }

        private DataSet _GetAssetFieldValue(DataSet inputDS)
        {

            ErrorDS errDS = new ErrorDS();

            DBConnectionDS connDS = new DBConnectionDS();
            DataLongDS longDS = new DataLongDS();

            connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
            connDS.AcceptChanges();

            DataStringDS StringDS = new DataStringDS();
            StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
            StringDS.AcceptChanges();

            OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetAFieldvalue");

            if (cmd == null)
            {
                return UtilDL.GetCommandNotFound();
            }

            cmd.Parameters["REQUISITIONTYPEID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
            cmd.Parameters["REQCATEGORYID"].Value = Convert.ToInt32((object)StringDS.DataStrings[1].StringValue);
            cmd.Parameters["FIELDNAME"].Value = (object)StringDS.DataStrings[2].StringValue;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;

            bool bError = false;
            int nRowAffected = -1;

            AssetDS assetDS = new AssetDS();
            nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AIFConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
            assetDS.AcceptChanges();

            // now create the packet
            errDS.Clear();
            errDS.AcceptChanges();

            DataSet returnDS = new DataSet();

            returnDS.Merge(assetDS);
            returnDS.Merge(errDS);
            returnDS.AcceptChanges();

            return returnDS;


        }

        //private DataSet _GetReqLimitFieldListByReqTypeID(DataSet inputDS)
        //{

        //ErrorDS errDS = new ErrorDS();

        //DBConnectionDS connDS = new DBConnectionDS();
        //DataLongDS longDS = new DataLongDS();

        //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //connDS.AcceptChanges();

        //DataStringDS StringDS = new DataStringDS();
        //StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        //StringDS.AcceptChanges();

        //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetReqListLimitFieldByTypeID");

        //if (cmd == null)
        //{
        //    return UtilDL.GetCommandNotFound();
        //}
        //cmd.Parameters["REQUISITIONTYPEID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
        //OleDbDataAdapter adapter = new OleDbDataAdapter();
        //adapter.SelectCommand = cmd;

        //bool bError = false;
        //int nRowAffected = -1;

        //AssetDS assetDS = new AssetDS();
        //nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AIFConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        //assetDS.AcceptChanges();

        //// now create the packet
        //errDS.Clear();
        //errDS.AcceptChanges();

        //DataSet returnDS = new DataSet();

        //returnDS.Merge(assetDS);
        //returnDS.Merge(errDS);
        //returnDS.AcceptChanges();
        //return returnDS;


        // }

        //private DataSet _GetReqLimitFieldListByReqTypeID2(DataSet inputDS)
        //{

        //ErrorDS errDS = new ErrorDS();

        //DBConnectionDS connDS = new DBConnectionDS();
        //DataLongDS longDS = new DataLongDS();

        //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //connDS.AcceptChanges();

        //DataStringDS StringDS = new DataStringDS();
        //StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        //StringDS.AcceptChanges();

        //OleDbCommand cmd = DBCommandProvider.GetDBCommand("GetReqListLimitFieldByTypeID2");

        //if (cmd == null)
        //{
        //    return UtilDL.GetCommandNotFound();
        //}
        ////cmd.Parameters["REQUISITIONTYPEID"].Value = Convert.ToInt32((object)StringDS.DataStrings[0].StringValue);
        //OleDbDataAdapter adapter = new OleDbDataAdapter();
        //adapter.SelectCommand = cmd;

        //bool bError = false;
        //int nRowAffected = -1;

        //AssetDS assetDS = new AssetDS();
        //nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.AIFConfig.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        //assetDS.AcceptChanges();

        //// now create the packet
        //errDS.Clear();
        //errDS.AcceptChanges();

        //DataSet returnDS = new DataSet();

        //returnDS.Merge(assetDS);
        //returnDS.Merge(errDS);
        //returnDS.AcceptChanges();
        //return returnDS;


        // }

        //private DataSet _GetRequisitionDataForReport(DataSet inputDS)
        //{
        //ErrorDS errDS = new ErrorDS();

        //DBConnectionDS connDS = new DBConnectionDS();
        //DataLongDS longDS = new DataLongDS();

        //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //connDS.AcceptChanges();

        //OleDbCommand cmd = new OleDbCommand();

        //DataStringDS StringDS = new DataStringDS();
        //StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        //StringDS.AcceptChanges();

        //string activeStatus = StringDS.DataStrings[0].StringValue;

        //cmd = DBCommandProvider.GetDBCommand("GetRequisitionHistory");

        //if (cmd == null)
        //{
        //    return UtilDL.GetCommandNotFound();
        //}

        //OleDbDataAdapter adapter = new OleDbDataAdapter();
        //adapter.SelectCommand = cmd;

        //bool bError = false;
        //int nRowAffected = -1;

        //AssetDS assetDS = new AssetDS();
        //nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.ReqHistory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        //assetDS.AcceptChanges();

        //// now create the packet
        //errDS.Clear();
        //errDS.AcceptChanges();

        //DataSet returnDS = new DataSet();

        //returnDS.Merge(assetDS);
        //returnDS.Merge(errDS);
        //returnDS.AcceptChanges();

        //return returnDS;
        // }

        //private DataSet _GetReqHealthSafetyForReport(DataSet inputDS)
        //{
        //ErrorDS errDS = new ErrorDS();

        //DBConnectionDS connDS = new DBConnectionDS();
        //DataLongDS longDS = new DataLongDS();

        //connDS.Merge(inputDS.Tables[connDS.DBConnections.TableName], false, MissingSchemaAction.Error);
        //connDS.AcceptChanges();

        //OleDbCommand cmd = new OleDbCommand();

        //DataStringDS StringDS = new DataStringDS();
        //StringDS.Merge(inputDS.Tables[StringDS.DataStrings.TableName], false, MissingSchemaAction.Error);
        //StringDS.AcceptChanges();

        //string activeStatus = StringDS.DataStrings[0].StringValue;

        //cmd = DBCommandProvider.GetDBCommand("GetReqHealthSafety");

        //if (cmd == null)
        //{
        //    return UtilDL.GetCommandNotFound();
        //}

        //OleDbDataAdapter adapter = new OleDbDataAdapter();
        //adapter.SelectCommand = cmd;

        //bool bError = false;
        //int nRowAffected = -1;

        //AssetDS assetDS = new AssetDS();
        //nRowAffected = ADOController.Instance.Fill(adapter, assetDS, assetDS.ReqHistory.TableName, connDS.DBConnections[0].ConnectionID, ref bError);
        //assetDS.AcceptChanges();

        //// now create the packet
        //errDS.Clear();
        //errDS.AcceptChanges();

        //DataSet returnDS = new DataSet();

        //returnDS.Merge(assetDS);
        //returnDS.Merge(errDS);
        //returnDS.AcceptChanges();

        //return returnDS;
        //}
    }


}



