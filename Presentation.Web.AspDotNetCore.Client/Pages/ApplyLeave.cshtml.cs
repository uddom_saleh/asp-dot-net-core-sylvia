﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Sylvia.Common;

namespace Presentation.Web.AspDotNetCore.Client.Pages
{


    public class ApplyLeaveModel : PageModel
    {
        DBCommandProvider dBCommandProvider = new DBCommandProvider();
        public string Address { get; set; }
        public UserDS userDS { get; set; }
        public CountryDS countryDS { get; set; }
        public CountryDS divisionLs { get; set; }
        public int CountryId { get; set; }
        public int DivisionId { get; set; }
        public string CountrySelectedText { get; set; }


        public void OnGetGetRdlcReport()
        {
            
        }

        public void OnGet()
        {
            int _conId = ADOController.Instance.OpenNewConnection();
            ErrorDS errDS = new ErrorDS();
            userDS = new UserDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;

            OleDbCommand cmd = dBCommandProvider.GetDBCommandAdvanced("GetUser_For_Login", "User.xml");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return;
            }
            cmd.Parameters["LoginID"].Value = "admin";

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, userDS, userDS.Users.TableName, _conId, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LOGIN_USER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

            }
            userDS.AcceptChanges();


            OleDbCommand cmd1 = dBCommandProvider.GetDBCommandAdvanced("GetAdministrativeRoleByLoginID", "User.xml");

            cmd1.Parameters["LoginID"].Value = "admin";

            OleDbDataAdapter adapter1 = new OleDbDataAdapter();
            adapter1.SelectCommand = cmd1;

            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter1, userDS, userDS.UAARole.TableName, _conId, ref bError);
            userDS.AcceptChanges();

            stringDS.Clear();
            stringDS.AcceptChanges();
            foreach (UserDS.User user in userDS.Users)
            {
                if (!user.IsSaltNull())
                {
                    stringDS.DataStrings.AddDataString(user.Salt);
                }
                stringDS.AcceptChanges();
            }
            ViewData.Add("loggedInName", userDS.Users[0].EmployeeName);
            this.LoadCountry();
        }

        private void LoadCountry()
        {
            CountryId = 16;
            CountrySelectedText = "BANGLADESH";
            this.LoadDivisionByCountryName(CountrySelectedText);
            countryDS = UtilUI.getCountryList(); // code to populate the grades list box
        }
        public void OnPostGetLoginUserName()
        {
            

           
        }

        private void LoadDivisionByCountryName(string countryName)
        {
            divisionLs = UtilUI.getDivisionList();
            if (divisionLs.Division != null)
            {
                DataRow[] foundRows = divisionLs.Division.Select("COUNTRYNAME='" + countryName + "'");
                divisionLs = new CountryDS();
                foreach (DataRow divRow in foundRows)
                { divisionLs.Division.ImportRow(divRow); }
                divisionLs.AcceptChanges();
            }
        }

        public JsonResult OnGetDivisionForAssignLeaveByCountryName(string countryName)
        {
            try
            {
                CountryDS divisionLs = UtilUI.getDivisionList();
                if (divisionLs.Division != null)
                {
                    DataRow[] foundRows = divisionLs.Division.Select("COUNTRYNAME='" + countryName + "'");
                    divisionLs = new CountryDS();
                    foreach (DataRow divRow in foundRows)
                    { divisionLs.Division.ImportRow(divRow); }
                    divisionLs.AcceptChanges();
                }


                StringWriter builder = new StringWriter();
                builder.WriteLine("[");
                if (divisionLs.Division != null)
                {
                    builder.WriteLine("{\"optionDisplay\":\"Choose..\",");
                    builder.WriteLine("\"optionValue\":\"0\"},");
                    for (int i = 0; i <= divisionLs.Division.Rows.Count - 1; i++)
                    {
                        builder.WriteLine("{\"optionDisplay\":\"" + divisionLs.Division.Rows[i]["Divisionname"] + "\",");
                        builder.WriteLine("\"optionValue\":\"" + divisionLs.Division.Rows[i]["Divisionid"] + "\"},");
                    }
                }
                else
                {
                    builder.WriteLine("{\"optionDisplay\":\"Select\",");
                    builder.WriteLine("\"optionValue\":\"0\"},");
                }


                string returnjson = builder.ToString().Substring(0, builder.ToString().Length - 3);
                returnjson = returnjson + "]";
                //return returnjson.Replace("\r", "").Replace("\n", "");
                return new JsonResult(returnjson.Replace("\r", "").Replace("\n", ""));

            }
            catch (Exception)
            {
                return new JsonResult(string.Empty);

            }

        }
    }

}