﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Sylvia.Common;
using Sylvia.Common.Reports;
using System.Data;
using System.IO;
using Sylvia.Common.RDLCReport;

namespace Presentation.Web.AspDotNetCore.Client
{
    public class LeaveReportModel : PageModel
    {
        DBCommandProvider dBCommandProvider = new DBCommandProvider();
        public EmployeeDS empDS { get; set; }
        public rptLeaveDS leaveDS { get; set; }
        public void OnGet()
        {
            int _conId = ADOController.Instance.OpenNewConnection();
            ErrorDS errDS = new ErrorDS();
            empDS = new EmployeeDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;

            OleDbCommand cmd = dBCommandProvider.GetDBCommandAdvanced("GetEmployeeList_ForLeaveReport_SortByCode", "Employee.xml");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return;
            }

            cmd.Parameters["EmployeeCode1"].Value = "6105";
            cmd.Parameters["EmployeeCode2"].Value = "6105";
            cmd.Parameters["EmployeeTypeID1"].Value = -1;
            cmd.Parameters["EmployeeTypeID2"].Value = -1;
            cmd.Parameters["CompanyCode1"].Value = "0";
            cmd.Parameters["CompanyCode2"].Value = "0";
            cmd.Parameters["LocationCode1"].Value = "0";
            cmd.Parameters["LocationCode2"].Value = "0";
            cmd.Parameters["SiteID1"].Value = 0;
            cmd.Parameters["SiteID2"].Value = 0;
            cmd.Parameters["CostCenterCode1"].Value = "0";
            cmd.Parameters["CostCenterCode2"].Value = "0";
            cmd.Parameters["CompanyDivisionCode1"].Value = "0";
            cmd.Parameters["CompanyDivisionCode2"].Value = "0";
            cmd.Parameters["DepartmentCode1"].Value = "0";
            cmd.Parameters["DepartmentCode2"].Value = "0";
            cmd.Parameters["FunctionCode1"].Value = "0";
            cmd.Parameters["FunctionCode2"].Value = "0";
            cmd.Parameters["LiabilityCode1"].Value = "0";
            cmd.Parameters["LiabilityCode2"].Value = "0";
            cmd.Parameters["GradeCode1"].Value = "0";
            cmd.Parameters["GradeCode2"].Value = "0";
            cmd.Parameters["DesignationCode1"].Value = "0";
            cmd.Parameters["DesignationCode2"].Value = "0";
            cmd.Parameters["EmployeeStatus1"].Value = -1;
            cmd.Parameters["EmployeeStatus2"].Value = -1;

            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, empDS, empDS.Employees.TableName, _conId, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LOGIN_USER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

            }
            empDS.AcceptChanges();

        }
        public IActionResult OnPostRdlcReport()
        {
            int _conId = ADOController.Instance.OpenNewConnection();
            ErrorDS errDS = new ErrorDS();
            leaveDS = new rptLeaveDS();
            DataStringDS stringDS = new DataStringDS();
            bool bError = false;
            int nRowAffected = -1;
            OleDbCommand cmd = dBCommandProvider.GetDBCommandAdvanced("GetLeaveSummary", "rptLeave.xml");

          //  OleDbCommand cmd = DBCommandProvider.GetDBCommandAdvanced("GetLeaveSummary", "rptLeave.xml");
            if (cmd == null)
            {
                Util.LogInfo("Command is null.");
                return Page();
            }

            DateTime frmdate = new DateTime(2020, 01, 01);
            DateTime todate = new DateTime(2020, 12, 12);
            cmd.Parameters["FromDate"].Value = (object)frmdate;
            cmd.Parameters["ToDate"].Value = (object)todate;
            cmd.Parameters["ActivitiesID"].Value = "101";
            cmd.Parameters["ELQID"].Value = 18532;
            cmd.Parameters["LeaveTypeID1"].Value = cmd.Parameters["LeaveTypeID2"].Value = 0;
            cmd.Parameters["EmployeeCodes1"].Value = cmd.Parameters["EmployeeCodes2"].Value = "6105";
            cmd.Parameters["EmpStatus1"].Value = -1;
            cmd.Parameters["EmpStatus2"].Value = -1;


            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = cmd;


            bError = false;
            nRowAffected = -1;
            nRowAffected = ADOController.Instance.Fill(adapter, leaveDS, leaveDS.Leave.TableName, _conId, ref bError);
            if (bError)
            {
                ErrorDS.Error err = errDS.Errors.NewError();
                err.ErrorCode = ErrorCode.ERR_DB_OPERATION_FAILED.ToString();
                err.ErrorTier = ErrorTier.ERR_TIER_DL.ToString();
                err.ErrorLevel = ErrorLevel.ERR_LEVEL_SEVER.ToString();
                err.ErrorInfo1 = ActionID.NA_ACTION_GET_LOGIN_USER.ToString();
                errDS.Errors.AddError(err);
                errDS.AcceptChanges();

            }
            leaveDS.AcceptChanges();
            leaveDS.Merge((DataTable)GetLogo());
            leaveDS.AcceptChanges();


            ReportByteData reporteByteData = new ReportByteData();
            var returnString = reporteByteData.ReportData(leaveDS);
            return File(returnString, System.Net.Mime.MediaTypeNames.Application.Octet, "LeaveReport" + ".pdf");
        }

        public DataTable GetLogo()
        {
            string logoPath = @"D:\SYLVIA-RND-ASP-DOT-DOT-CORE\asp-dot-net-core-sylvia\Sylvia\Presentation.Web.AspDotNetCore.Client\wwwroot\img\JTI_Logo.jpg";

            FileStream fs;
            BinaryReader br;
            Byte[] imgbyte;
            fs = new FileStream(logoPath, FileMode.Open, FileAccess.Read);

            br = new BinaryReader(fs);
            imgbyte = br.ReadBytes(Convert.ToInt32((fs.Length)));
            DataTable dt = new DataTable();
            dt.TableName = "Image";
            DataRow drow;
            dt.Columns.Add("img", System.Type.GetType("System.Byte[]"));
            drow = dt.NewRow();
            drow[0] = imgbyte;
            dt.Rows.Add(drow);
            br.Close();
            fs.Close();
            dt.TableName = "Image";
            return dt;
        }
    }
}