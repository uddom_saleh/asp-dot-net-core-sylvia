using System;

namespace Presentation.Web.AspDotNetCore.Client
{
    /// <summary>
    /// Summary description for DictionaryUI.
    /// </summary>
    /// 


    public class DictionaryUI
    {
        // Generic messages
        public static readonly string MSG_UNDER_PROCESSING = "Your previous request is under processing. Please make contact with administrator..";
        public static readonly string MSG_ITEMS_NOT_FOUND = "There is no items to show.";
        public static readonly string MSG_ITEMS_NOT_FOUND_FOR_ACTION = "There is no selected item for given action.";

        // Data type format
        public static readonly string DATA_FORMAT_ORA_DATE = "dd-MMM-yyyy";
        public static readonly string DATA_FORMAT_ORA_MONTH = "MMM-yyyy";

        // Specific error messages
        public static readonly string ERR_MSG_NOT_FOUND_DEPARTMENT = "Department not avilable.";
        public static readonly string ERR_MSG_DUPLICATE_CODE_GRADE = "Some selected grades  already allocated.";
        public static readonly string ERR_MSG_CONFIRM_PWD_MISMATCH = "Confirm password does not match.";
        public static readonly string ERR_MSG_CURRENT_PWD_MISMATCH = "Current password does not match your login password.";
        public static readonly string ERR_MSG_EMPLOYEE_NOT_FOUND = "Employee list is empty.";
        public static readonly string ERR_MSG_DATA_CONFLICT = "Conflict with system uses.";
        public static readonly string ERR_MSG_DATE = "To date should be greater than from date.";

        // Generic error messages
        public static readonly string ERR_MSG_DATA_TYPE_MISMATCH = "Data type does not match.";
        public static readonly string ERR_MSG_DATA_MISSING = "Input is required.";
        public static readonly string ERR_MSG_STRING_TOO_LONG = "Input string is too long.";
        public static readonly string ERR_MSG_STRING_TOO_SHORT = "Input string is too short.";
        public static readonly string ERR_MSG_DUPLICATE_CODE = "Code already exist in the system.";
        public static readonly string ERR_MSG_DUPLICATE_NAME = "Name already exist in the system.";
        public static readonly string ERR_MSG_DUPLICATE = "Already exist in the system.";
        public static readonly string ERR_MSG_DATA_NOT_FOUND = "Specified data does not exist in the system.";
        public static readonly string ERR_MSG_CODE_NOT_FOUND = "Specified code does not exist in the system.";
        public static readonly string ERR_MSG_INVALID_FORMAT = "Input format is invalid.";
        public static readonly string ERR_MSG_NUMBER_OUT_OF_RANGE = "Input number is out of range.";
        public static readonly string ERR_MSG_DATE_OUT_OF_RANGE = "Input date is out of range.";
        public static readonly string ERR_MSG_DB_FAILD = "<p style=\"margin-top: 0; margin-bottom: 0\" align=\"center\"><b> " +
                                                                    "<font face=\"Verdana\" color=\"#FF0000\">There was an error during database " +
                                                                    "operation.</font></b></p>" +
                                                                    "<p style=\"margin-top: 0; margin-bottom: 0\" align=\"center\"><b>" +
                                                                    "<font face=\"Verdana\" color=\"#FF0000\">The detail of the error can be found in log " +
                                                                    "file.</font></b></p>" +
                                                                    "<p style=\"margin-top: 0; margin-bottom: 0\" align=\"center\"><b>" +
                                                                    "<font face=\"Verdana\" color=\"#FF0000\">You may contact your <i>System " +
                                                                    "Administrator</i> for help.</font></b></p>";

        public static readonly string ERR_MSG_SALARY_REVIEW = "<p align=\"center\"><font color=\"#FF0000\" face=\"Verdana\" size=\"2\"><i>Salary Review</i>" +
                                                              "could not be processed.&nbsp; Because required information could not be found in" +
                                                              "the system. Please check the help file for details.</font></p>" +
                                                               "<p align=\"center\"><font face=\"Verdana\" size=\"2\"><a href=\"javascript:history.back()\">Back</a></font></p>";

        public static readonly string ERR_MSG_INVALID_PRIVILEGE = "<p align=\"center\"><font color=\"#FF0000\" face=\"Verdana\" size=\"2\"><i>Sorry</i>" +
                                                                  "Access denied. You have no permission to execute the action.</font></p>" +
                                                                  "<p align=\"center\"><font face=\"Verdana\" size=\"2\"><a href=\"javascript:history.back()\">Back</a></font></p>" +
                                                                  "<p align=\"center\"></p>";



        public static readonly string ERR_MSG_SESSION_ENDED = "<p align=\"center\"><font color=\"#FF0000\" face=\"Verdana\" size=\"2\"><i>Sorry</i>" +
                                                                  " You are no longer logged into system. Please login again.</font></p>" +
                                                                  "<p align=\"center\"><font face=\"Verdana\" size=\"2\"><a href=\"javascript:history.back()\">Back</a></font></p>" +
                                                                  "<p align=\"center\"></p>";

        public static readonly string ERR_MSG_MODULE_HIBERNATE = "<p align=\"center\"><font color=\"#FF0000\" face=\"Verdana\" size=\"2\">" +
                                                              "System is in <i><b>No Transaction</b></i> mode. <br/>Please try again later or contact with <i>System Administrator</i>.</font></p>" +
                                                              "<p align=\"center\"><font face=\"Verdana\" size=\"2\"><a href=\"javascript:history.back()\">Back</a></font></p>" +
                                                              "<p align=\"center\"></p>";

        public enum SessionKey
        {
            KEY_PREV_PAGE,
            KEY_MSG,
            KEY_FORWARD,
            KEY_CODE,
            KEY_SEPARATION_END_SERVICE_INFO,
            KEY_EXISTING_EMP_FILTER,
        }


    }



}
